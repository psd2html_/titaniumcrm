<?php

/**
 * This is the model class for table 'manufacture2coreinstrumentclass'.
 *
 * The followings are the available columns in table 'manufacture2coreinstrumentclass':
 * @property integer $idManufacture
* @property integer $idCoreInstrumentClass
                                 
 */
class manufacture2coreinstrumentclass extends ActiveRecord
{
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'manufacture2coreinstrumentclass';
    }

    public function rules() {
        return array(
            array('idManufacture, idCoreInstrumentClass', 'required'),
            array('idManufacture, idCoreInstrumentClass', 'numeric'),            
            array('idManufacture, idCoreInstrumentClass', 'safe', 'on'=>'search'),
        );
    }
    
    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idManufacture' => 'idManufacture',
'idCoreInstrumentClass' => 'idCoreInstrumentClass',

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idManufacture',$this->idManufacture);
$criteria->compare('idCoreInstrumentClass',$this->idCoreInstrumentClass);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
