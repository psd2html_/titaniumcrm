<?php

/**
 * This is the model class for table 'attachement2logisticfragment'.
 *
 * The followings are the available columns in table 'attachement2logisticfragment':
 * @property integer $idLogisticFragment
* @property integer $idAttachment
                                 
 */
class attachement2logisticfragment extends ActiveRecord
{
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'attachement2logisticfragment';
    }

    public function rules() {
        return array(
            array('idLogisticFragment, idAttachment', 'required'),
            array('idLogisticFragment, idAttachment', 'numeric'),            
            array('idLogisticFragment, idAttachment', 'safe', 'on'=>'search'),
        );
    }
    
    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idLogisticFragment' => 'idLogisticFragment',
'idAttachment' => 'idAttachment',

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idLogisticFragment',$this->idLogisticFragment);
$criteria->compare('idAttachment',$this->idAttachment);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
