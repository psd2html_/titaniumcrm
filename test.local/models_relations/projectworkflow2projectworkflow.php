<?php

/**
 * This is the model class for table 'projectworkflow2projectworkflow'.
 *
 * The followings are the available columns in table 'projectworkflow2projectworkflow':
 * @property integer $idFrom
* @property integer $idTo
                                 
 */
class projectworkflow2projectworkflow extends ActiveRecord
{
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'projectworkflow2projectworkflow';
    }

    public function rules() {
        return array(
            array('idFrom, idTo', 'required'),
            array('idFrom, idTo', 'numeric'),            
            array('idFrom, idTo', 'safe', 'on'=>'search'),
        );
    }
    
    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idFrom' => 'idFrom',
'idTo' => 'idTo',

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idFrom',$this->idFrom);
$criteria->compare('idTo',$this->idTo);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
