<?php

/**
 * This is the model class for table 'orderungroupedfeature'.
 *
 * The followings are the available columns in table 'orderungroupedfeature':
 * @property integer $idFeature
* @property integer $idOrder
                                 
 */
class orderungroupedfeature extends ActiveRecord
{
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'orderungroupedfeature';
    }

    public function rules() {
        return array(
            array('idFeature, idOrder', 'required'),
            array('idFeature, idOrder', 'numeric'),            
            array('idFeature, idOrder', 'safe', 'on'=>'search'),
        );
    }
    
    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idFeature' => 'idFeature',
'idOrder' => 'idOrder',

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idFeature',$this->idFeature);
$criteria->compare('idOrder',$this->idOrder);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
