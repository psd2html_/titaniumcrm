INSERT INTO `coreinstrumentclass` (`idCoreinstrumentclass`, `name`, `abbr`) VALUES
  (1, 'test_class', 1);

INSERT INTO `manufacturer` (`idManufacturer`, `name`, `abbr`, `coreInstrumentClasses`, `motto`) VALUES
  (1, 'Addidas', 'test', 'test', 'test');

INSERT INTO `permission` (`idPermission`, `enumPermissionType`, `idUser`, `idManufacturer`, `idCoreInstrumentClass`) VALUES
  (1, '2', 1, 1, 1);