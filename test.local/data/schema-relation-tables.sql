DROP TABLE IF EXISTS `attachement2logisticfragment`;
CREATE TABLE IF NOT EXISTS `attachement2logisticfragment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idLogisticFragment` int (11) NOT NULL ,
  `idAttachment` int (11) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `attachment2contest`;
CREATE TABLE IF NOT EXISTS `attachment2contest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idContest` int (11) NOT NULL ,
  `idAttachment` int (11) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `attachment2contragent`;
CREATE TABLE IF NOT EXISTS `attachment2contragent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idContragent` int (11) NOT NULL ,
  `idAttachment` int (11) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `attachment2coreinstrument`;
CREATE TABLE IF NOT EXISTS `attachment2coreinstrument` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idCoreInstrument` int (11) NOT NULL ,
  `idAttachment` int (11) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `attachment2event`;
CREATE TABLE IF NOT EXISTS `attachment2event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idEvent` int (11) NOT NULL ,
  `idAttachment` int (11) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `attachment2logistic`;
CREATE TABLE IF NOT EXISTS `attachment2logistic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idLogistic` int (11) NOT NULL ,
  `idAttachment` int (11) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `attachment2order`;
CREATE TABLE IF NOT EXISTS `attachment2order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idOrder` int (11) NOT NULL ,
  `idAttachment` int (11) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `attachment2project`;
CREATE TABLE IF NOT EXISTS `attachment2project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idProject` int (11) NOT NULL ,
  `idAttachment` int (11) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `attachment2sample`;
CREATE TABLE IF NOT EXISTS `attachment2sample` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idSample` int (11) NOT NULL ,
  `idAttachment` int (11) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `centre2manufacturer`;
CREATE TABLE IF NOT EXISTS `centre2manufacturer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idManufacturer` int (11) NOT NULL ,
  `idCentre` int (11) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `customer2project`;
CREATE TABLE IF NOT EXISTS `customer2project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idProject` int (11) NOT NULL ,
  `idCustomer` int (11) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `event2logistic`;
CREATE TABLE IF NOT EXISTS `event2logistic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idLogistic` int (11) NOT NULL ,
  `idEvent` int (11) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `managedblockfeature`;
CREATE TABLE IF NOT EXISTS `managedblockfeature` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idFeature` int (11) NOT NULL ,
  `idManagedBlock` int (11) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `manufacture2coreinstrumentclass`;
CREATE TABLE IF NOT EXISTS `manufacture2coreinstrumentclass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idManufacture` int (11) NOT NULL ,
  `idCoreInstrumentClass` int (11) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `notice2link`;
CREATE TABLE IF NOT EXISTS `notice2link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idLink` int (11) NOT NULL ,
  `idNotice` int (11) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `orderungroupedfeature`;
CREATE TABLE IF NOT EXISTS `orderungroupedfeature` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idFeature` int (11) NOT NULL ,
  `idOrder` int (11) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `projectworkflow2projectworkflow`;
CREATE TABLE IF NOT EXISTS `projectworkflow2projectworkflow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idFrom` int (11) NOT NULL ,
  `idTo` int (11) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `supplier2project`;
CREATE TABLE IF NOT EXISTS `supplier2project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idSupplier` int (11) NOT NULL ,
  `idProject` int (11) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;