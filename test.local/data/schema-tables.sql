DROP TABLE IF EXISTS `ActiveRecordLog`;
CREATE TABLE IF NOT EXISTS `ActiveRecordLog` (
  `idActiveRecordLog` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar (255) DEFAULT NULL ,
  `action` varchar (20) DEFAULT NULL ,
  `model` varchar (45) DEFAULT NULL ,
  `idModel` varchar (10) DEFAULT NULL ,
  `field` varchar (45) DEFAULT NULL ,
  `creationDate` varchar (255) NOT NULL ,
  `idUser` varchar (45) DEFAULT NULL ,
  `newValue` varchar (255) DEFAULT NULL ,
  PRIMARY KEY (`idActiveRecordLog`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `atomicRbac`;
CREATE TABLE IF NOT EXISTS `atomicRbac` (
  `idAtomicRbac` int(11) NOT NULL AUTO_INCREMENT,
  `enumRuleClass` int (11) NOT NULL ,
  `enumRuleValue` int (11) NOT NULL ,
  `idProject` int (11) DEFAULT NULL ,
  `idUser` int (11) NOT NULL ,
  PRIMARY KEY (`idAtomicRbac`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `attachment`;
CREATE TABLE IF NOT EXISTS `attachment` (
  `idAttachment` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar (255) NOT NULL ,
  `title` varchar (255) DEFAULT NULL ,
  `originalFilename` varchar (255) NOT NULL ,
  PRIMARY KEY (`idAttachment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `centre`;
CREATE TABLE IF NOT EXISTS `centre` (
  `idCentre` int(11) NOT NULL AUTO_INCREMENT,
  `idManufacturer` int (11) DEFAULT NULL ,
  `name` varchar (255) NOT NULL ,
  `manufacturers` varchar (255) DEFAULT NULL ,
  PRIMARY KEY (`idCentre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `centre2manufacturer`;
CREATE TABLE IF NOT EXISTS `centre2manufacturer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idCentre` int (11) NOT NULL ,
  `idManufacturer` int (11) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `checkpoint`;
CREATE TABLE IF NOT EXISTS `checkpoint` (
  `idCheckpoint` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar (255) NOT NULL ,
  `description` varchar (255) DEFAULT NULL ,
  `order` int (11) DEFAULT NULL ,
  `idCheckpointGroup` int (11) DEFAULT NULL ,
  `readyState` int (11) DEFAULT NULL ,
  `startWith` int (11) NOT NULL ,
  PRIMARY KEY (`idCheckpoint`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `checkpointGroup`;
CREATE TABLE IF NOT EXISTS `checkpointGroup` (
  `idCheckpointGroup` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar (99) NOT NULL ,
  PRIMARY KEY (`idCheckpointGroup`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `city`;
CREATE TABLE IF NOT EXISTS `city` (
  `idCity` int(11) NOT NULL AUTO_INCREMENT,
  `idCountry` varchar (11) DEFAULT NULL ,
  `idRegion` varchar (10) DEFAULT NULL ,
  `name` varchar (128) DEFAULT NULL ,
  PRIMARY KEY (`idCity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `company`;
CREATE TABLE IF NOT EXISTS `company` (
  `idCompany` int(11) NOT NULL AUTO_INCREMENT,
  `idManufacturer` int (11) ,
  `idCompanyClass` int (11) NOT NULL ,
  `fullName` varchar (255) NOT NULL ,
  `fullNameEng` varchar (255) DEFAULT NULL ,
  `shortName` varchar (64) NOT NULL ,
  `city` varchar (45) NOT NULL ,
  `cityGenetivus` varchar (255) DEFAULT NULL ,
  `physicalAddress` varchar (255) DEFAULT NULL ,
  `physicalAddressEng` varchar (255) DEFAULT NULL ,
  `legalAddress` varchar (255) DEFAULT NULL ,
  `url` varchar (255) DEFAULT NULL ,
  `email` varchar (64) DEFAULT NULL ,
  `phone` varchar (45) DEFAULT NULL ,
  `addPhone` varchar (45) DEFAULT NULL ,
  `comment` varchar (255) DEFAULT NULL ,
  `enumFinanceType` int (11) DEFAULT NULL ,
  `inn` varchar (64) DEFAULT NULL ,
  `financeType` varchar (255) DEFAULT NULL ,
  `ogrn` varchar (64) DEFAULT NULL ,
  `bik` varchar (64) DEFAULT NULL ,
  `bankName` varchar (64) DEFAULT NULL ,
  `account` varchar (64) DEFAULT NULL ,
  `correspondentAccount` varchar (64) DEFAULT NULL ,
  `abbr` int (10) NOT NULL ,
  `requisites` varchar (255) DEFAULT NULL ,
  PRIMARY KEY (`idCompany`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `companyclass`;
CREATE TABLE IF NOT EXISTS `companyclass` (
  `idCompanyclass` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar (99) NOT NULL ,
  PRIMARY KEY (`idCompanyclass`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `contest`;
CREATE TABLE IF NOT EXISTS `contest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idProject` int (11) NOT NULL ,
  `contestUrl` varchar (255) DEFAULT NULL ,
  `contestSiteUrl` varchar (255) DEFAULT NULL ,
  `enumStatus` int (11) DEFAULT NULL ,
  `datePost` varchar (255) DEFAULT NULL ,
  `dateRequest` varchar (255) DEFAULT NULL ,
  `dateResponse` varchar (255) DEFAULT NULL ,
  `dateContest` varchar (255) DEFAULT NULL ,
  `notification` varchar (80) DEFAULT NULL ,
  `costLotCurrency` int (11) DEFAULT NULL ,
  `costAskCurrency` int (11) DEFAULT NULL ,
  `costContractCurrency` int (11) DEFAULT NULL ,
  `costLot` int (11) DEFAULT NULL ,
  `costAsk` int (11) DEFAULT NULL ,
  `costContract` int (11) DEFAULT NULL ,
  `number` int (255) DEFAULT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `contragent`;
CREATE TABLE IF NOT EXISTS `contragent` (
  `idContragent` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar (255) NOT NULL ,
  `director` varchar (255) DEFAULT NULL ,
  `requisites` varchar (255) DEFAULT NULL ,
  `phone` varchar (255) DEFAULT NULL ,
  `email` varchar (255) DEFAULT NULL ,
  `address` varchar (255) DEFAULT NULL ,
  PRIMARY KEY (`idContragent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `coreinstrument`;
CREATE TABLE IF NOT EXISTS `coreinstrument` (
  `idCoreinstrument` int(11) NOT NULL AUTO_INCREMENT,
  `idCoreInstrumentClass` int (11) NOT NULL ,
  `idManufacturer` int (11) NOT NULL ,
  `name` varchar (255) NOT NULL ,
  `sid` varchar (45) NOT NULL ,
  `description` varchar (255) NOT NULL ,
  `price` int (11) DEFAULT NULL ,
  `enumCurrency` int (4) DEFAULT NULL ,
  `isPriceSettled` varchar (255) DEFAULT NULL ,
  PRIMARY KEY (`idCoreinstrument`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `coreinstrumentclass`;
CREATE TABLE IF NOT EXISTS `coreinstrumentclass` (
  `idCoreinstrumentclass` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar (90) NOT NULL ,
  `abbr` int (10) NOT NULL ,
  PRIMARY KEY (`idCoreinstrumentclass`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `country`;
CREATE TABLE IF NOT EXISTS `country` (
  `idCountry` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar (128) DEFAULT NULL ,
  PRIMARY KEY (`idCountry`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `custom`;
CREATE TABLE IF NOT EXISTS `custom` (
  `idCustom` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar (80) NOT NULL ,
  PRIMARY KEY (`idCustom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `customer`;
CREATE TABLE IF NOT EXISTS `customer` (
  `idCustomer` int(11) NOT NULL AUTO_INCREMENT,
  `idCompany` int (11) DEFAULT NULL ,
  `firstName` varchar (90) NOT NULL ,
  `lastName` varchar (90) NOT NULL ,
  `middleName` varchar (90) DEFAULT NULL ,
  `address` varchar (255) DEFAULT NULL ,
  `email` varchar (255) DEFAULT NULL ,
  `email2` varchar (255) DEFAULT NULL ,
  `skype` varchar (255) DEFAULT NULL ,
  `icq` varchar (255) DEFAULT NULL ,
  `corporation` varchar (90) DEFAULT NULL ,
  `phone` varchar (90) DEFAULT NULL ,
  `phone2` varchar (90) DEFAULT NULL ,
  `mobilePhone` varchar (255) DEFAULT NULL ,
  `customerComment` varchar (255) DEFAULT NULL ,
  `job` varchar (255) DEFAULT NULL ,
  `comment` varchar (255) DEFAULT NULL ,
  `bothName` varchar (255) DEFAULT NULL ,
  PRIMARY KEY (`idCustomer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `event`;
CREATE TABLE IF NOT EXISTS `event` (
  `idEvent` int(11) NOT NULL AUTO_INCREMENT,
  `idProject` int (11) NOT NULL ,
  `enumType` int (11) NOT NULL ,
  `eventClass` int (11) DEFAULT NULL ,
  `title` int (11) DEFAULT NULL ,
  `date` date NOT NULL ,
  `comment` varchar (255) DEFAULT NULL ,
  `idUser` int (11) DEFAULT NULL ,
  `searchEnumClass` varchar (255) DEFAULT NULL ,
  `idCheckpoint` varchar (255) DEFAULT NULL ,
  PRIMARY KEY (`idEvent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `feature`;
CREATE TABLE IF NOT EXISTS `feature` (
  `idFeature` int(11) NOT NULL AUTO_INCREMENT,
  `sid` varchar (45) NOT NULL ,
  `name` varchar (255) DEFAULT NULL ,
  `count` int (11) DEFAULT NULL ,
  `amount` int (11) DEFAULT NULL ,
  `isPriceSettled` int (11) DEFAULT NULL ,
  `ruName` varchar (255) DEFAULT NULL ,
  `description` varchar (255) DEFAULT NULL ,
  `isFreeOfCharge` varchar (255) DEFAULT NULL ,
  `ruDescription` varchar (255) DEFAULT NULL ,
  `idManufacturer` varchar (255) NOT NULL ,
  `price` int (11) DEFAULT NULL ,
  `enumCurrency` int (4) DEFAULT NULL ,
  PRIMARY KEY (`idFeature`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `featuregroup`;
CREATE TABLE IF NOT EXISTS `featuregroup` (
  `idFeaturegroup` int(11) NOT NULL AUTO_INCREMENT,
  `idCoreInstrument` int (11) NOT NULL ,
  `name` varchar (255) NOT NULL ,
  `parent` int (11) DEFAULT NULL ,
  PRIMARY KEY (`idFeaturegroup`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `folder`;
CREATE TABLE IF NOT EXISTS `folder` (
  `idFolder` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar (255) NOT NULL ,
  `idProject` int (11) NOT NULL ,
  `parent` int (11) DEFAULT NULL ,
  `enumSpecialFolder` int (11) DEFAULT NULL ,
  `link` int (11) DEFAULT NULL ,
  PRIMARY KEY (`idFolder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `group`;
CREATE TABLE IF NOT EXISTS `group` (
  `idGroup` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar (255) NOT NULL ,
  `jsonEncodedRules` varchar (255) DEFAULT NULL ,
  `system` int (11) DEFAULT NULL ,
  `isLogist` tinyint (1) DEFAULT NULL ,
  `startingTab` int (11) DEFAULT NULL ,
  PRIMARY KEY (`idGroup`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `insuranceCompany`;
CREATE TABLE IF NOT EXISTS `insuranceCompany` (
  `idInsuranceCompany` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar (255) NOT NULL ,
  `director` varchar (255) DEFAULT NULL ,
  `phone` varchar (255) DEFAULT NULL ,
  `email` varchar (255) DEFAULT NULL ,
  `requisites` varchar (255) DEFAULT NULL ,
  `address` varchar (255) DEFAULT NULL ,
  PRIMARY KEY (`idInsuranceCompany`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `orderfeature`;
CREATE TABLE IF NOT EXISTS `orderfeature` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idOrder` int (11) NOT NULL ,
  `idFeature` int (11) NOT NULL ,
  `idManagedBlock` int (11) NOT NULL ,
  `idLink` int (11) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `logistic`;
CREATE TABLE IF NOT EXISTS `logistic` (
  `idLogistic` int(11) NOT NULL AUTO_INCREMENT,
  `idProject` int (11) NOT NULL ,
  `enumStatus` varchar (45) DEFAULT NULL ,
  `lastChange` varchar (255) DEFAULT NULL ,
  `currentLocation` varchar (128) DEFAULT NULL ,
  `comment` varchar (255) DEFAULT NULL ,
  `shipmentDeadline` varchar (255) DEFAULT NULL ,
  `productionDeadline` varchar (255) DEFAULT NULL ,
  `idContragent` int (11) DEFAULT NULL ,
  `enumTransportBeforeBorder` int (11) DEFAULT NULL ,
  `enumTransportAfterBorder` int (11) DEFAULT NULL ,
  `idCustom` int (11) DEFAULT NULL ,
  PRIMARY KEY (`idLogistic`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `logisticFragment`;
CREATE TABLE IF NOT EXISTS `logisticFragment` (
  `idLogisticFragment` int(11) NOT NULL AUTO_INCREMENT,
  `cityFrom` varchar (255) NOT NULL ,
  `cityTo` varchar (255) NOT NULL ,
  `dateUnload` varchar (255) DEFAULT NULL ,
  `dateUpload` varchar (255) DEFAULT NULL ,
  `comment` varchar (255) DEFAULT NULL ,
  `enumTransport` int (11) NOT NULL ,
  `idProject` int (11) DEFAULT NULL ,
  PRIMARY KEY (`idLogisticFragment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `logisticPosition`;
CREATE TABLE IF NOT EXISTS `logisticPosition` (
  `idLogisticPosition` int(11) NOT NULL AUTO_INCREMENT,
  `innerNumber` int (11) DEFAULT NULL ,
  `idInsuranceCompany` int (11) DEFAULT NULL ,
  `idCoreInstrument` int (11) DEFAULT NULL ,
  `idFeature` int (11) DEFAULT NULL ,
  `idProject` int (11) NOT NULL ,
  `productionDeadline` varchar (255) DEFAULT NULL ,
  `poNumber` varchar (255) DEFAULT NULL ,
  `goodsNameRus` varchar (255) DEFAULT NULL ,
  `goodsOrigin` varchar (255) DEFAULT NULL ,
  `goodsNameEng` varchar (255) DEFAULT NULL ,
  `isPurchaseViaManufacturer` int (11) DEFAULT NULL ,
  `isTechnoinfoUnloadsGoods` int (11) DEFAULT NULL ,
  `isExportDeclarationMade` int (11) DEFAULT NULL ,
  `idPackingSheetUpload` int (11) DEFAULT NULL ,
  `idDescriptionUpload` int (11) DEFAULT NULL ,
  `isSpecialDeliveryRequired` int (11) DEFAULT NULL ,
  `isExportDoubleApplicationLicenseRequired` int (11) DEFAULT NULL ,
  `isInsurancePayed` int (11) DEFAULT NULL ,
  `packagingSpecification` varchar (255) DEFAULT NULL ,
  `buyIncotermsCondition` varchar (255) DEFAULT NULL ,
  `sellIncotermsCondition` varchar (255) DEFAULT NULL ,
  `takeoutAddress` varchar (255) DEFAULT NULL ,
  `tnvedCod` varchar (255) DEFAULT NULL ,
  PRIMARY KEY (`idLogisticPosition`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `managedblock`;
CREATE TABLE IF NOT EXISTS `managedblock` (
  `idManagedblock` int(11) NOT NULL AUTO_INCREMENT,
  `comment` varchar (255) DEFAULT NULL ,
  `notice` varchar (255) DEFAULT NULL ,
  `enumRule` int (11) NOT NULL ,
  `idCoreInstrument` varchar (255) NOT NULL ,
  `idFeatureGroup` varchar (255) DEFAULT NULL ,
  PRIMARY KEY (`idManagedblock`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `managedblockfeature`;
CREATE TABLE IF NOT EXISTS `managedblockfeature` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idManagedBlock` int (11) NOT NULL ,
  `idFeature` int (11) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `manufacturer`;
CREATE TABLE IF NOT EXISTS `manufacturer` (
  `idManufacturer` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar (99) NOT NULL ,
  `abbr` varchar (10) NOT NULL ,
  `coreInstrumentClasses` varchar (255) DEFAULT NULL ,
  `motto` varchar (255) DEFAULT NULL ,
  PRIMARY KEY (`idManufacturer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `notice`;
CREATE TABLE IF NOT EXISTS `notice` (
  `idNotice` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar (255) NOT NULL ,
  `idFeatureGroup` int (11) NOT NULL ,
  `mapId` int (11) NOT NULL ,
  PRIMARY KEY (`idNotice`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `order`;
CREATE TABLE IF NOT EXISTS `order` (
  `idOrder` int(11) NOT NULL AUTO_INCREMENT,
  `enumCalculationAlgorithm` int (11) DEFAULT NULL ,
  `idCoreInstrument` int (11) DEFAULT NULL ,
  `idProject` int (11) NOT NULL ,
  `comment` varchar (255) DEFAULT NULL ,
  `date` varchar (255) NOT NULL ,
  `name` varchar (255) NOT NULL ,
  `enumOrderType` int (11) DEFAULT NULL ,
  `enumPricePolicy` int (11) DEFAULT NULL ,
  `idUser` int (11) NOT NULL ,
  `idOrderInProject` int (11) NOT NULL ,
  `idCustomer` int (11) DEFAULT NULL ,
  `showCIP` int (11) DEFAULT NULL ,
  `showDDP` int (11) DEFAULT NULL ,
  `showEXW` int (11) DEFAULT NULL ,
  `isFeaturePriceShown` tinyint (1) DEFAULT NULL ,
  `isRussianDescriptionLoaded` tinyint (1) DEFAULT NULL ,
  `isDeliverySpreadBetweenFeatures` tinyint (1) DEFAULT NULL ,
  `destinationCity` varchar (255) DEFAULT NULL ,
  `destinationCityGenetivus` varchar (255) DEFAULT NULL ,
  `idCompany` int (11) DEFAULT NULL ,
  `guaranteeInMonth` int (11) DEFAULT NULL ,
  `guaranteeLessThan` int (11) DEFAULT NULL ,
  `deliver` varchar (255) DEFAULT NULL ,
  `enumPaymentCondition` int (11) DEFAULT NULL ,
  `lastDate` varchar (255) DEFAULT NULL ,
  `cipCurrency` int (11) DEFAULT NULL ,
  `doThreeMonth` int (11) DEFAULT NULL ,
  `outCurrency` int (11) DEFAULT NULL ,
  `idCoreInstrumentClass` varchar (255) DEFAULT NULL ,
  `idCoreInstrumentManufacturer` varchar (255) DEFAULT NULL ,
  `entryId` varchar (255) DEFAULT NULL ,
  `price` varchar (255) DEFAULT NULL ,
  `cipPayment` int (11) DEFAULT NULL ,
  `ddpCoeff` int (11) DEFAULT NULL ,
  `saleCoeff` int (11) DEFAULT NULL ,
  `overridenPrice` int (11) DEFAULT NULL ,
  `eurRub` int (11) DEFAULT NULL ,
  `usdRub` int (11) DEFAULT NULL ,
  `gbpRub` int (11) DEFAULT NULL ,
  `yenRub` int (11) DEFAULT NULL ,
  PRIMARY KEY (`idOrder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `permission`;
CREATE TABLE IF NOT EXISTS `permission` (
  `idPermission` int(11) NOT NULL AUTO_INCREMENT,
  `enumPermissionType` varchar (255) NOT NULL ,
  `idUser` int (11) NOT NULL ,
  `idManufacturer` int (11) DEFAULT NULL ,
  `idCoreInstrumentClass` int (11) DEFAULT NULL ,
  PRIMARY KEY (`idPermission`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `pm`;
CREATE TABLE IF NOT EXISTS `pm` (
  `idPm` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int (11) DEFAULT NULL ,
  `subject` varchar (255) DEFAULT NULL ,
  `idRecipient` int (11) NOT NULL ,
  `body` varchar (255) DEFAULT NULL ,
  `status` int (11) DEFAULT NULL ,
  `created` varchar (255) NOT NULL ,
  `idProject` int (11) DEFAULT NULL ,
  PRIMARY KEY (`idPm`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `project`;
CREATE TABLE IF NOT EXISTS `project` (
  `idProject` int(11) NOT NULL AUTO_INCREMENT,
  `overrideFilter` tinyint (1) ,
  `lastChange` timestamp ,
  `lastChangeId` int (11) ,
  `contractTotalCurrency` int (11) ,
  `logisticPositionCounter` int (11) ,
  `idInsuranceCompany` int (11) ,
  `poCounter` int (11) ,
  `kpCounter` int (11) ,
  `sampleCounter` int (11) ,
  `enumStatus` int (11) ,
  `buyIncotermsCondition` int (11) ,
  `sellIncotermsCondition` int (11) ,
  `enumFinanceStatus` int (11) ,
  `priority` int (11) ,
  `year` varchar (255) ,
  `payedByUser` varchar (255) ,
  `quartal` int (11) ,
  `innerName` varchar (255) ,
  `name` varchar (255) ,
  `takeoutAddress` text DEFAULT NULL ,
  `parent` int (11) DEFAULT NULL ,
  `comment` varchar (255) DEFAULT NULL ,
  `idCustomer` int (11) DEFAULT NULL ,
  `enumProvision` int (11) DEFAULT NULL ,
  `idResponsible` int (11) DEFAULT NULL ,
  `idCompanyConsumer` int (11) DEFAULT NULL ,
  `idCompanyBuyer` int (11) DEFAULT NULL ,
  `tnvedCod` varchar (255) DEFAULT NULL ,
  `idManufacturer` varchar (255) DEFAULT NULL ,
  `idCoreInstrumentClass` varchar (255) DEFAULT NULL ,
  `earliestSampleStatus` varchar (255) DEFAULT NULL ,
  `countOfOrders` varchar (255) DEFAULT NULL ,
  `isInsurancePayed` varchar (255) DEFAULT NULL ,
  `isExportDeclarationMade` varchar (255) DEFAULT NULL ,
  `isExportDoubleApplicationLicenseRequired` varchar (255) DEFAULT NULL ,
  `isSpecialDeliveryRequired` varchar (255) DEFAULT NULL ,
  `isTechnoinfoUnloadsGoods` varchar (255) DEFAULT NULL ,
  `projectName` varchar (255) DEFAULT NULL ,
  `packagingSpecification` varchar (255) DEFAULT NULL ,
  `idCoreInstrument` varchar (255) DEFAULT NULL ,
  `idContractUpload` int (11) DEFAULT NULL ,
  PRIMARY KEY (`idProject`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `projectWorkflow`;
CREATE TABLE IF NOT EXISTS `projectWorkflow` (
  `idProjectWorkflow` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar (90) NOT NULL ,
  `description` varchar (255) DEFAULT NULL ,
  `enumState` int (11) NOT NULL ,
  `nextStages` varchar (255) DEFAULT NULL ,
  PRIMARY KEY (`idProjectWorkflow`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `region`;
CREATE TABLE IF NOT EXISTS `region` (
  `idRegion` int(11) NOT NULL AUTO_INCREMENT,
  `idCountry` varchar (10) DEFAULT NULL ,
  `name` varchar (64) DEFAULT NULL ,
  PRIMARY KEY (`idRegion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `sample`;
CREATE TABLE IF NOT EXISTS `sample` (
  `idSample` int(11) NOT NULL AUTO_INCREMENT,
  `idProject` int (11) NOT NULL ,
  `recievedDate` varchar (255) NOT NULL ,
  `lastChanged` varchar (255) DEFAULT NULL ,
  `task` varchar (255) NOT NULL ,
  `enumStatus` int (11) NOT NULL ,
  `comment` varchar (255) DEFAULT NULL ,
  `location` varchar (128) DEFAULT NULL ,
  `name` varchar (255) DEFAULT NULL ,
  `idManufacturer` int (11) NOT NULL ,
  `idSampleInProject` int (11) NOT NULL ,
  `idCoreInstrument` int (11) NOT NULL ,
  `idCoreInstrumentClass` int (11) NOT NULL ,
  `enumDeliverType` int (11) NOT NULL ,
  `centre` varchar (255) NOT NULL ,
  `idCentre` varchar (255) NOT NULL ,
  `title` int (64) DEFAULT NULL ,
  `innerName` int (255) DEFAULT NULL ,
  PRIMARY KEY (`idSample`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `spent`;
CREATE TABLE IF NOT EXISTS `spent` (
  `idSpent` int(11) NOT NULL AUTO_INCREMENT,
  `enumCurrency` int (11) NOT NULL ,
  `enumPaymentGoal` int (11) NOT NULL ,
  `isCashPayment` int (11) NOT NULL ,
  `paymentType` int (11) NOT NULL ,
  `idUser` varchar (255) NOT NULL ,
  `reason` varchar (255) DEFAULT NULL ,
  `date` varchar (255) NOT NULL ,
  `idProject` int (11) NOT NULL ,
  `amount` int (11) NOT NULL ,
  `whoPayed` int (255) DEFAULT NULL ,
  PRIMARY KEY (`idSpent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `sticky`;
CREATE TABLE IF NOT EXISTS `sticky` (
  `idSticky` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int (11) NOT NULL ,
  `isTask` int (11) DEFAULT NULL ,
  `idResponsible` int (11) DEFAULT NULL ,
  `date` varchar (255) DEFAULT NULL ,
  `calendarDate` varchar (255) DEFAULT NULL ,
  `text` varchar (255) NOT NULL ,
  `postCommitComment` varchar (255) DEFAULT NULL ,
  `idProject` int (11) NOT NULL ,
  `showInCalendar` tinyint (1) DEFAULT NULL ,
  `template` int (11) DEFAULT NULL ,
  `colour` varchar (45) DEFAULT NULL ,
  `isDone` varchar (255) DEFAULT NULL ,
  PRIMARY KEY (`idSticky`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `supplier`;
CREATE TABLE IF NOT EXISTS `supplier` (
  `idSupplier` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar (45) NOT NULL ,
  PRIMARY KEY (`idSupplier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `upload`;
CREATE TABLE IF NOT EXISTS `upload` (
  `idUpload` int(11) NOT NULL AUTO_INCREMENT,
  `idFolder` int (11) NOT NULL ,
  `name` varchar (255) NOT NULL ,
  `size` int (11) DEFAULT NULL ,
  `idProject` int (11) NOT NULL ,
  `comment` varchar (255) DEFAULT NULL ,
  `date` varchar (255) DEFAULT NULL ,
  `idUser` int (11) NOT NULL ,
  `token` varchar (255) NOT NULL ,
  PRIMARY KEY (`idUpload`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `manufacture2coreinstrumentclass`;
CREATE TABLE IF NOT EXISTS `manufacture2coreinstrumentclass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idManufacture` int (11) DEFAULT NULL ,
  `idCoreInstrumentClass` int (11) DEFAULT NULL ,
  `name` varchar (255) DEFAULT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;