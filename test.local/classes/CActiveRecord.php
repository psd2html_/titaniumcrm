<?php

/**
 * @file ActiveRecord.php.
 * @author Artem Stepochkin <psd2html@mail.ru>
 */
class CActiveRecord
{
    const BELONGS_TO    = 'CBelongsToRelation';
    const HAS_ONE       = 'CHasOneRelation';
    const HAS_MANY      = 'CHasManyRelation';
    const MANY_MANY     = 'CManyManyRelation';
    const STAT          = 'CStatRelation';
}