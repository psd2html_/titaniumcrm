<?php

/**
 * @file AutoloaderFiles.php.
 * @author Artem Stepochkin <psd2html@mail.ru>
 */
class AutoloaderFiles
{
    public $currentObjClass;
    public $validator;
    public $attr3;
    public $finalModelList = array(); // array new data list tables-fields
    public $finalModelListRelations = array(); //
    public $primaryKey = '';

    public function __construct()
    {
        $dir = 'models';
        $files = scandir($dir);
        foreach ($files as $file)
        {
            if (($file != "..") AND ($file != ".")) {
                if (file_exists($dir. '/' . $file)) {
                    require_once $dir. '/' . $file;
                    $explode = explode('.', $file);
                    $className = $explode[0];

                    // Формируем первый основной список таблиц с полями в ПАПКЕ  /models
                    $returnDataList = $this->dataList($className, $dir. '/' . $file);
                    array_push($this->finalModelList, $returnDataList);

                    // создаем модели по relations в отдельную папку
                    $this->createModelsRelations($className);
                }
            }
        }
        // создаем схему
        echo '==============================================================================='  . "<br>";
        echo '================================================================= БАЗОВЫЕ СХЕМЫ'  . "<br>";
        echo '==============================================================================='  . "<br>";
        $getScheme = $this->getCreateScheme();
        echo $getScheme;
        echo "<br>";


        // Проходимся по новым промежуточным моделям и таблицам
        $this->finalModelList = array();
        $dir = 'models_relations';
        $files = scandir($dir);
        foreach ($files as $file)
        {
            if (($file != "..") AND ($file != ".")) {
                if (file_exists($dir. '/' . $file)) {
                    require_once $dir. '/' . $file;
                    $explode = explode('.', $file);
                    $className = $explode[0];

                    // Формируем первый основной список таблиц с полями в ПАПКЕ /models_relations
                    $returnDataListRelations = $this->dataList($className, $dir. '/' . $file);
                    array_push($this->finalModelList, $returnDataListRelations);
                }
            }
        }

//        echo '<pre>';
//        print_r($this->finalModelList);
//        echo '</pre>';
//        die('STOP');


        // создаем схему
        echo '=============================================================================================================='  . "<br>";
        echo '=============================================================== ВТОРИЧНЫЕ СХЕМЫ  ============================='  . "<br>";
        echo '=============================================================================================================='  . "<br>";
        $getScheme = $this->getCreateScheme();
        echo $getScheme;
        echo "<br>";

//        echo '<pre>';
//        print_r($this->finalModelList);
//        echo '</pre>';



    }


    public function createModelsRelations($className)
    {
        $class = new $className();
        if (method_exists($className, 'relations') and $className != 'Users') {
            $relations = $class->relations();

            //$fileLine = $array();

            //$allowRelation = array('CManyManyRelation');
            if (!empty($relations)) {
                foreach ($relations as $key => $array)
                {
                    $relationType  = $array[0];
                    $relationTable = $array[1];
                    $relationField = $array[2];

                    if ($relationType == 'CManyManyRelation') {
                        $pattern = "/(\w+)[(](\w.+)[)]/";
                        preg_match_all($pattern, $relationField, $match);
                        if (count($match) == 3) {
                            $refTable  = trim($match[1][0]);
                            $relFields = $match[2][0];
                            $fields    = explode(',', $relFields);

                            $propertyLine   = '';
                            $labelLine      = '';
                            $criteriaLine   = '';
                            foreach ($fields as $value)
                            {
                                $value        = trim($value);
                                // Property
                                $propertyLine .= "* @property integer $$value" . "\r\n";
                                // label
                                $labelLine .= "'$value' => '$value'," . "\r\n";
                                // Criteria
                                $_criteria = '$criteria';
                                $_this = '$this';
                                $criteriaLine .= "$_criteria"."->compare('$value',$_this->$value);" . "\r\n";
                            }

                            $filename = 'models_relations/' . $refTable . '.php';
                            $_className = '$className';
                            $fileLine   =
                                "<?php

/**
 * This is the model class for table '$refTable'.
 *
 * The followings are the available columns in table '$refTable':
 $propertyLine                                 
 */
class $refTable extends ActiveRecord
{
    public static function model($_className = __CLASS__) {
        return parent::model($_className);
    }

    public function tableName() {
        return '$refTable';
    }

    public function rules() {
        return array(
            array('$relFields', 'required'),
            array('$relFields', 'numeric'),            
            array('$relFields', 'safe', 'on'=>'search'),
        );
    }
    
    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			$labelLine
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$_criteria=new CDbCriteria;

		$criteriaLine

		return new CActiveDataProvider($_this, array(
			'criteria'=>$_criteria,
		));
	}
}
";
                            file_put_contents($filename, $fileLine);
                        }
                    }
                }
            }
        }
    }



    /**
     * Формируем первый основной список таблиц с полями
     *
     * @param $className
     * @param $file
     */
    public function dataList($className, $file)
    {
        $dataList = array();
        $fileContent = file_get_contents($file);
        $pattern = '/@property\s(integer|string|date|datetime|boolean|bool|int|timestamp|text|primary_key)\s[$](\w+)/';
        preg_match_all($pattern, $fileContent, $match);

        $class = new $className();
        if (method_exists($className, 'tableName') and method_exists($className, 'rules'))
        {
            $tableName  = trim($class->tableName());
            $rules      = $class->rules();

            // записываем в массив все наши таблицы с полями из свойств методов (по @property комментарию)
            foreach ($match[2] as $key1 => $fieldName) {
                foreach ($match[1] as $key2 => $fieldType) {
                    if ($key1 == $key2) {
                        $dataList[$tableName][$fieldName]['field_type'] = $fieldType;
                    }
                }
            }

            foreach ($rules as $array)
            {
                if (!array_key_exists('on', $array))
                {
                    foreach ($array as $value)
                    {
                        $fields     = $array[0]; // поле или массив полей
                        $listFields = explode(',', $fields);
                        // если массив полей
                        if (count($listFields) > 1)
                        {
                            foreach ($listFields as $fieldName)
                            {
                                $fieldName = trim($fieldName);
                                // ищем несоответствие
                                //foreach ($dataList as $table)
                                //{
                                    // Если не было такого ключа в массиве
                                    if (!array_key_exists($fieldName, $dataList[$tableName])) {
                                        // но если есть публичное свойство
                                        if (property_exists($class, $fieldName)) {
                                            $dataList[$tableName][$fieldName]['is_public_property'] = 'true';
                                        // иначе это вообще добавленный новый аттрибут в rules
                                        } else {
                                            $dataList[$tableName][$fieldName]['new_rules'] = 'true';
                                        }
                                    }

                                    $validator = $array[1];
                                    if (isset($dataList[$tableName][$fieldName]['validator']) AND !empty($dataList[$tableName][$fieldName]['validator'])) {
                                        $dataList[$tableName][$fieldName]['validator'] = $dataList[$tableName][$fieldName]['validator'] . ',' . $validator;
                                    } else {
                                        $dataList[$tableName][$fieldName]['validator'] = $validator;
                                    }
                                    $dataList[$tableName][$fieldName]['field_size']  = isset($array['max']) ? $array['max'] : $dataList[$tableName][$fieldName]['field_size'];

//                                    // required
//                                    if ((isset($dataList[$tableName][$fieldName]['field_required'])) AND (!empty($dataList[$tableName][$fieldName]['field_required']))) {
//                                        if ($dataList[$tableName][$fieldName]['field_required'] !== $array[1]) {
//                                            $dataList[$tableName][$fieldName]['field_required'] += ' + ' . $array[1];
//                                        } else {
//                                            $dataList[$tableName][$fieldName]['field_required'] = $array[1];
//                                        }
//                                    } else {
//                                        if ($array[1] == 'required') {
//                                            $dataList[$tableName][$fieldName]['field_required'] = 'yes';
//                                        }
//                                    }
//
//                                    // validator
//                                    if ((isset($dataList[$tableName][$fieldName]['validator'])) AND (!empty($dataList[$tableName][$fieldName]['validator']))) {
//                                        if ($dataList[$tableName][$fieldName]['validator'] != $array[1]) {
//                                            $dataList[$tableName][$fieldName]['validator'] += ' + ' . $array[1];
//                                        }
//                                    } else {
//                                        $dataList[$tableName][$fieldName]['validator'] = $array[1];
//                                    }
//
//                                    // field_type
//                                    $dataList[$tableName][$fieldName]['field_type']  = ($array[1] != 'length') ? $array[1] : $dataList[$tableName][$fieldName]['field_type'];
//                                    $dataList[$tableName][$fieldName]['attr3']       = ($dataList[$tableName][$fieldName]['attr3']) ? : $array[2];
//                                    $dataList[$tableName][$fieldName]['field_size']  = isset($array['max']) ? $array['max'] : $dataList[$tableName][$fieldName]['field_size'];
                                //}
                            }
                        // если 1 поле
                        } else {
                            $fieldName = trim($listFields[0]);
                            // ищем несоответствие в сформированном ранее массиве
                            // если ищем в той таблице что нам нужно
                            // Если не было такого ключа в массиве
                            if (!array_key_exists($fieldName, $dataList[$tableName])) {
                                if (property_exists($class, $fieldName)) {
                                    $dataList[$tableName][$fieldName]['is_public_property'] = 'true';
                                } else {
                                    $dataList[$tableName][$fieldName]['new_rules'] = 'true';
                                }
                            }

                            //$dataList[$tableName][$fieldName]['field_type']     = ($array[1] != 'length') ? $array[1] : $dataList[$tableName][$fieldName]['field_type'];
                            $dataList[$tableName][$fieldName]['field_size']     = isset($array['max']) ? $array['max'] : $dataList[$tableName][$fieldName]['field_size'];
                            //$dataList[$tableName][$fieldName]['field_required'] = ($array[1] == 'required') ? 'yes' : $dataList[$tableName][$fieldName]['field_required'];
                            //$dataList[$tableName][$fieldName]['validator'] = $array[1];
                            //$dataList[$tableName][$fieldName]['attr3']     = $array[2];

                            // validator
                            $validator = $array[1];
                            if (isset($dataList[$tableName][$fieldName]['validator']) AND !empty($dataList[$tableName][$fieldName]['validator'])) {
                                $dataList[$tableName][$fieldName]['validator'] = $dataList[$tableName][$fieldName]['validator'] . ',' . $validator;
                            } else {
                                $dataList[$tableName][$fieldName]['validator'] = $validator;
                            }
                        }
                    }
                }
            }

        // Если нет RULES
        } elseif (!method_exists($className, 'rules')) {
            if (method_exists($className, 'tableName')) {
                $tableName  = trim($class->tableName());
                // записываем в массив все наши таблицы с полями из свойств методов (по @property комментарию)
                foreach ($match[2] as $key1 => $fieldName) {
                    foreach ($match[1] as $key2 => $fieldType) {
                        if ($key1 == $key2) {
                            $dataList[$tableName][$fieldName]['field_type'] = $fieldType;
                        }
                    }
                }
            }
        }

        return $dataList;
    }


    public function getCreateScheme()
    {
        // integer|string|date|boolean|bool|int|timestamp
        $allowTypes = array(
            'string'        => 'varchar',
            'integer'       => 'int',
            'date'          => 'date',
            'datetime'      => 'timestamp',
            'boolean'       => 'tinyint (1)',
            'bool'          => 'tinyint (1)',
            'int'           => 'int',
            'timestamp'     => 'timestamp',
            'numerical'     => 'int',
            'DateTime'      => 'timestamp',
            'enum'          => 'string',
            'text'          => 'text'
        );
        $exludeTables = array('users');
        $schema = '';
        $br = "<br>";

        foreach ($this->finalModelList as $_key => $_value)
        {
            foreach ($_value as $keyTableName => $fields)
            {
                // исключаем таблицы
                if (!in_array($keyTableName, $exludeTables))
                {
                    $schema .= "DROP TABLE IF EXISTS `$keyTableName`;" . "<br>";
                    $schema .= "CREATE TABLE IF NOT EXISTS `$keyTableName` (". "<br>";

                    $schemaPrimaryKey = "`id` int(11) NOT NULL AUTO_INCREMENT,". "<br>";

                    $x=0;
                    $this->primaryKey = '';
                    foreach ($fields as $keyNameField => $field)
                    {
                        // Добавляем PRIMARY KEY по первым свойствам
                        $x++;
                        if ($x == 1) {
                            $added = "id" . strtolower($keyTableName);
                            if ($added == strtolower($keyNameField)) {
                                $schemaPrimaryKey = "`$keyNameField` int(11) NOT NULL AUTO_INCREMENT,". "<br>";
                                $schema .= $schemaPrimaryKey;
                                $this->primaryKey = $keyNameField;
                                continue;
                            }
                        } else {
                            $schemaPrimaryKey = "";
                        }
                        $schema .=  $schemaPrimaryKey;

                        $br = "<br \>";

                        $nameField = "`$keyNameField`";
                        $fieldType = $field['field_type'] ? : '';
                        $fieldType = $allowTypes[$fieldType];
                        $fieldSize = $field['field_size'] ? "(" . $field['field_size'] . ")" : '';

                        //if ()
                        if (isset($field['validator']) and !empty($field['validator'])) {
                            $validators = explode(',', $field['validator']);
                            $validators = array_unique($validators);
                        }
                        if (is_array($validators))
                        {
                            if (in_array('required', $validators)) {
                                $fieldNotNull = "NOT NULL";
                            } else {
                                $fieldNotNull = "DEFAULT NULL";
                            }
                        }

                        // если нет размера
                        if (!empty($fieldType) and empty($fieldSize)) {
                            if ($fieldType == 'int') {
                                $fieldSize = "(". 11 . ")";
                            }
                            if ($fieldType == 'varchar') {
                                $fieldSize = "(". 255 . ")";
                            }
                        }
                        // если нет типа
                        if (empty($fieldType) and !empty($fieldSize)) {
                            if ($fieldSize < 255) {
                                $fieldType = 'int';
                            }
                            if ($fieldSize == 255) {
                                $fieldType = 'varchar';
                            }
                        }

                        if ($fieldType == 'text') {
                            $fieldNotNull = "DEFAULT NULL";
                        }

                        $coma = ',';

                        if ($keyNameField == 'idManufacturer' and $keyTableName == 'feature') {
                            $c=0;
                        }

                        // НЕ исключаем чистые safe трибуты
                        if (is_array($validators)) {
                            //if (in_array('safe', $validators) and (isset($field['new_rules']))) {
                            if (isset($field['new_rules'])) {
//                        $nameField = '';
//                        $fieldType = '';
//                        $fieldSize = '';
//                        $fieldNotNull = '';
//                        $br = '';
//                        $coma = '';
//                        $fieldType = 'varchar';
//                        $fieldSize = "(". 255 . ")";
//                        $fieldNotNull = "DEFAULT NULL";

                                $fieldSize = (isset($field['field_size'])) ? "(".$field['field_size'].")" : '';
                                foreach ($validators as $value)
                                {
                                    if (array_key_exists($value, $allowTypes)) {
                                        $fieldType = $allowTypes[$value];
                                        if ($value == 'numerical') {
                                            $fieldSize = $fieldSize ?  $fieldSize  : "(". 11 . ")";
                                        } elseif ($value == 'string') {
                                            $fieldSize = $fieldSize ?  $fieldSize  : "(". 255 . ")";
                                        }
                                    } elseif($value == 'safe' and (!isset($field['field_type']))) {
                                        if (!$fieldSize) {
                                            $fieldType = "varchar";
                                            $fieldSize = "(". 255 . ")";
                                        }
                                    }

                                }
                                // если всё вообще пусто
                                if (empty($fieldType) and empty($fieldSize)) {
                                    $fieldType = "varchar";
                                    $fieldSize = "(". 255 . ")";
                                }

                                $fieldNotNull = "DEFAULT NULL";
                                if (in_array('required', $validators)) {
                                    $fieldNotNull = "NOT NULL";
                                }
                            }
                        }

                        // пока что исключаем публичные свойства
                        if (is_array($validators)) {
                            if (isset($field['is_public_property'])) {
                                $nameField = '';
                                $fieldType = '';
                                $fieldSize = '';
                                $fieldNotNull = '';
                                $br = '';
                                $coma = '';
                            }
                        }

//                    if ($keyNameField == 'idAtomicRbac')
//                    {
//                        if ($fieldType == 'primary_key') {
//                            $schema = str_replace('`id`', "$keyNameField", $schema);
//                        }
//                    }


                        $schema .= "$nameField $fieldType $fieldSize $fieldNotNull $coma".$br;
                    }

                    if (!empty($this->primaryKey)) {
                        $schema .= "PRIMARY KEY (`$this->primaryKey`)" . "<br>";
                    } else {
                        $schema .= "PRIMARY KEY (`id`)" . "<br>";
                    }

                    $schema .= ") ENGINE=InnoDB DEFAULT CHARSET=utf8;". "<br><br>";
                }
            }
        }

        return $schema;
    }


    public function checkFieldFinalModelList($fieldName)
    {
        foreach ($this->finalModelList as $keyTableName => $fields)
        {
            // если ищем в нужной таблице
            if ($keyTableName == $this->currentTableName) {
                // если нет ключа
                if (!array_key_exists($this->currentFieldName, $fields)) {
                    if (property_exists($this->currentObjClass, $fieldName)) {
                        $this->finalModelList[$keyTableName][$fieldName]['is_public_property'] = 'true';
                    } else {
                        $this->finalModelList[$keyTableName][$fieldName]['new_rules'] = 'true';
                    }
                    return false;
                } else {
                    $this->finalModelList[$keyTableName][$fieldName]['validator']   = $this->validator;
                    $this->finalModelList[$keyTableName][$fieldName]['attr3']       = $this->attr3;
                    return true;
                }
            }
        }
        return false;
    }
}