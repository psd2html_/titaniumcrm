<?php

/**
 * This is the model class for table "group".
 *
 * The followings are the available columns in table 'group':
 * @property integer $idGroup
 * @property string $name
 * @property text $jsonEncodedRules
 * @property integer $system
 * @property boolean $isLogist
 * @property integer $startingTab
 *
 * The followings are the available model relations:
 * @property Users[] $users
 */
class Group extends CActiveRecord
{
    /*
        Per-group auth scheme
        zone+modification+param+action = profit???
        mods, if any checks in config order. so, they should go in right descending order

        1). case: check permission on action. returns True/False
        2). case: find what partial to load - ReadOnly, Form, Nothing
        3). case: get filtering @ugly sql
    */

    const CLASS_ZONE = 0;
    const CLASS_PROJECT_SUBZONE = 1;

    /* partial form recommendations */
    const PARTIAL_READONLY = "_ro";
    const PARTIAL_FORM = "_f";
    const PARTIAL_NONE = "_x"; // ????

    /* operations */
    const ACTION_ADMIN = 8;
    const ACTION_DELETE = 3;
    const ACTION_CREATE = 2;
    const ACTION_UPDATE = 7;
    const ACTION_VIEW = 1;
    const ACTION_EDIT = 4;
    const ACTION_READONLY = 5;
    const ACTION_NONE = 6;

    /* subzone modificators */
    const MOD_HAS_MANUFACTURER = 1;
    const MOD_HAS_MANUFACTURER_AND_CLASS = 2;
    const MOD_DEFAULT = 0;
    const MOD_PROJECT_OWNER = 5;
    const MOD_PROJECT_DELEGATED = 6;

    /* zone stage has params (source == project stages const MOD_PROJECT_STAGE_BASE+shift ) */
    const MOD_PROJECT_STAGE_BASE = 100;

    /* Zones */
    /* no modificators */
    const ZONE_CONTRAGENT = 1;
    const ZONE_CUSTOM = 2;
    const ZONE_CLIENT = 3;
    const ZONE_COMPANY = 4;
    const ZONE_USER_MANAGEMENT = 5;
    const ZONE_INSURANCE_COMPANY = 6;
    const ZONE_COREINTRUMENT_CLASS = 7;
    const ZONE_COMPANY_CLASS = 30;

    /* manufacturer (yes/no) */
    const ZONE_MANUFACTURER = 7;
    const ZONE_MEASUREMENT_CENTRE = 8;
    const ZONE_FEATURE = 9;
    /* manufacturer+class (yes/no) */
    const ZONE_COREINSTRUMENT = 10;
    const ZONE_PROJECT = 11;
    /* project subzones */
    const SUBZONE_PROJECT_TASK = 13;
    const SUBZONE_PROJECT_ORDER = 14;
    const SUBZONE_PROJECT_LOG = 15;
    const SUBZONE_PROJECT_ARCHIVE = 16;
    const SUBZONE_PROJECT_FILES = 17;
    const SUBZONE_PROJECT_SAMPLES = 18;
    const SUBZONE_PROJECT_CONTEST = 19;
    const SUBZONE_PROJECT_LOGISTIC = 20;
    const SUBZONE_PROJECT_SPENTS = 21;
    const SUBZONE_PROJECT_INSTALLATION = 22;
    const ACTION_FORBIDDEN = 666;

    public static function getPermissionsUponZone($zone, $action, $model)
    {
        if (Yii::app()->user->isGuest) {
            return false;
        }

        if (Yii::app()->user->isAdmin()) {
            if ($action == self::ACTION_NONE) return false;
            return true;
        }

        $conf = self::config();
        $zoneConf = $conf[$zone];

        if ($zoneConf['class'] == self::CLASS_PROJECT_SUBZONE) {
            $projection = array(
                self::ACTION_UPDATE => self::ACTION_EDIT,
                self::ACTION_VIEW => self::ACTION_READONLY,
                self::ACTION_ADMIN => self::ACTION_EDIT,
                self::ACTION_CREATE => self::ACTION_EDIT,
                self::ACTION_DELETE => self::ACTION_EDIT,
            );
            $good = array(self::ACTION_READONLY, self::ACTION_EDIT, self::ACTION_NONE);
            if (!in_array($action, $good)) {
                $action = $projection[$action];
            }
        }

        $ac = self::getAllowedActions($model, $zone, $model === False);

        if (is_array($ac)) {
            if ($action == self::ACTION_READONLY) {
                return in_array($action, $ac) || in_array(self::ACTION_EDIT, $ac);
            } else {
                return in_array($action, $ac);
            }
        } else {
            return false;
        }
    }

    public static function config()
    {

        $mods_0 = array(self::MOD_DEFAULT);
        $mods_1 = array(self::MOD_HAS_MANUFACTURER, self::MOD_DEFAULT);
        $mods_2 = array(self::MOD_HAS_MANUFACTURER_AND_CLASS, self::MOD_DEFAULT);
        $mods_3 = array(self::MOD_PROJECT_OWNER, self::MOD_PROJECT_DELEGATED, self::MOD_HAS_MANUFACTURER_AND_CLASS);

        $p = array_keys(Project::getEnumStatusList());

        foreach ($p as $stage) {
            $mods_3[] = $stage + self::MOD_PROJECT_STAGE_BASE;
        }

        $mods_3[] = self::MOD_DEFAULT;

        return array(
            self::ZONE_CONTRAGENT => self::_config_for_simple_zones("Контрагент", $mods_0),
            self::ZONE_CUSTOM => self::_config_for_simple_zones("Таможни", $mods_0),
            self::ZONE_CLIENT => self::_config_for_simple_zones("Клиенты", $mods_0),
            self::ZONE_COMPANY => self::_config_for_simple_zones("Организации с которыми работаем", $mods_0),
            self::ZONE_USER_MANAGEMENT => self::_config_for_simple_zones("Пользователи и права", $mods_0),
            self::ZONE_INSURANCE_COMPANY => self::_config_for_simple_zones("Страховые компании", $mods_0),
            self::ZONE_COREINTRUMENT_CLASS => self::_config_for_simple_zones("Классы оборудования", $mods_0),
            self::ZONE_COMPANY_CLASS => self::_config_for_simple_zones("Типы организаций", $mods_0),
            /* manufacturer (yes/no) */
            self::ZONE_MANUFACTURER => self::_config_for_simple_zones("Производители", $mods_1),
            self::ZONE_MEASUREMENT_CENTRE => self::_config_for_simple_zones("Измерительный центр", $mods_0),
            self::ZONE_FEATURE => self::_config_for_simple_zones("Позиция", $mods_1),
            /* manufacturer+class (yes/no) */
            self::ZONE_COREINSTRUMENT => self::_config_for_simple_zones("Базовый прибор", $mods_2),
            self::ZONE_PROJECT => self::_config_for_simple_zones("Проект (первичная проверка)", $mods_3, array(), True),
            /* project subzones */
            //self::SUBZONE_PROJECT_EDIT => self::_config_for_project_subzones("Редактирование проекта", $mods_3),
            self::SUBZONE_PROJECT_TASK => self::_config_for_project_subzones("Задачи и комментарии", $mods_3),
            self::SUBZONE_PROJECT_ORDER => self::_config_for_project_subzones("Коммерческие предложения", $mods_3),
            self::SUBZONE_PROJECT_LOG => self::_config_for_project_subzones("История изменений", $mods_3),
            self::SUBZONE_PROJECT_ARCHIVE => self::_config_for_project_subzones("Архив задач", $mods_3),
            self::SUBZONE_PROJECT_FILES => self::_config_for_project_subzones("Файлы", $mods_3),
            self::SUBZONE_PROJECT_SAMPLES => self::_config_for_project_subzones("Образцы", $mods_3),
            self::SUBZONE_PROJECT_CONTEST => self::_config_for_project_subzones("Конкурс", $mods_3),
            self::SUBZONE_PROJECT_LOGISTIC => self::_config_for_project_subzones("Логистика", $mods_3),
            self::SUBZONE_PROJECT_SPENTS => self::_config_for_project_subzones("Доп. траты", $mods_3),
            self::SUBZONE_PROJECT_INSTALLATION => self::_config_for_project_subzones("Монтаж", $mods_3)
        );
    }

    private static function _config_for_simple_zones($name, $mod = array(), $default = array(), $strictPolicy = False)
    {
        $CRUD_Actons = array(self::ACTION_CREATE, self::ACTION_UPDATE, self::ACTION_DELETE, self::ACTION_VIEW, self::ACTION_ADMIN);

        foreach ($mod as $mod_id) {
            switch ($mod_id) {
                case self::MOD_HAS_MANUFACTURER_AND_CLASS:
                case self::MOD_HAS_MANUFACTURER:
                case self::MOD_PROJECT_OWNER:
                case self::MOD_PROJECT_DELEGATED:
                    $default[$mod_id] = array(self::ACTION_CREATE, self::ACTION_UPDATE, self::ACTION_DELETE, self::ACTION_VIEW, self::ACTION_ADMIN);
                    break;

                default:
                    $default[$mod_id] = $strictPolicy ? array(self::ACTION_NONE) : array(self::ACTION_VIEW, self::ACTION_ADMIN);
            }
        }

        return self::_config_entry($name, $mod, $CRUD_Actons, self::CLASS_ZONE, $default);
    }

    private static function _config_entry($name, $mod, $actions, $class, $default)
    {
        return array(
            'name' => $name,
            'mod' => $mod,
            'actions' => $actions,
            'class' => $class,
            'default' => $default,
        );
    }

    private static function _config_for_project_subzones($name, $mod = array(), $default = array())
    {
        $MVVC = array(self::ACTION_EDIT, self::ACTION_READONLY, self::ACTION_NONE);

        foreach ($mod as $mod_id) {
            if ($mod_id == self::MOD_DEFAULT) {
                $default[$mod_id] = array(self::ACTION_NONE);
            } else if ($mod_id <= self::MOD_PROJECT_DELEGATED) {
                $default[$mod_id] = array(self::ACTION_EDIT);
            } else {
                $default[$mod_id] = array(self::ACTION_NONE);
            }
        }

        return self::_config_entry($name, $mod, $MVVC, self::CLASS_PROJECT_SUBZONE, $default);
    }

    public static function getAllowedActions($model, $zone, $treatModelOptimistically = False)
    {
        $conf = self::config();
        $setId = Yii::app()->user->idGroup;
        if (!$setId) {
            return array();
        }
        $rules = self::getGroupRules($setId);
        $zoneRules = array_key_exists($zone, $rules) ? $rules[$zone] : array();
        $zoneConf = $conf[$zone];


        $user = Yii::app()->user->user();
//        var_dump($zoneRules);
        /*---------------------------*/
        $helpers = self::modsList();
        foreach ($zoneConf['mod'] as $modId) {
//            echo($helpers[$modId] . ":" . self::checkMod($modId, $model, $user) . "<br/>");
            if (!array_key_exists($modId, $zoneRules))
                continue;
            if (self::checkMod($modId, $model, $user, $treatModelOptimistically)) {
                return $zoneRules[$modId];
            }
        }
        return array();

    }

    public static function getGroupRules($id)
    {
        $array = Yii::app()->cache->get("RULES_GROUP_" . $id);
        if ($array === false) {
            $grp = Group::model()->findByPk($id);
            if (!$grp) {
                throw new CHttpException(500, "User is in bad group...");
            }
            $array = $grp->getRules();
        }
        return $array;
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Group the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function modsList()
    {
        $mods_3 = array(
            self::MOD_HAS_MANUFACTURER => 'Право на производителя',
            self::MOD_HAS_MANUFACTURER_AND_CLASS => 'Право на производителя+класс',
            self::MOD_DEFAULT => 'По-умолчанию',

            self::MOD_PROJECT_OWNER => 'Владелец проекта',
            self::MOD_PROJECT_DELEGATED => 'Персональное разрешение',
        );

        $p = (Project::getEnumStatusList());

        foreach ($p as $stage => $label) {
            $mods_3[$stage + self::MOD_PROJECT_STAGE_BASE] = "Этап " . $label;
        }
        return $mods_3;
    }

    /**
     * @param $modId
     * @param $model
     * @param User $user
     * @return array|bool
     */
    private static function checkMod($modId, $model, $user, $treatModelOptimistically = False)
    {
        if ($modId == self::MOD_DEFAULT) {
            return True;
        }


        if ($modId >= self::MOD_PROJECT_STAGE_BASE && $modId <= self::MOD_PROJECT_STAGE_BASE + 100) {
            if ($treatModelOptimistically) return true;
            return $model->enumStatus == $modId - self::MOD_PROJECT_STAGE_BASE;
        } else {
            switch ($modId) {
                case self::MOD_PROJECT_OWNER:
                    if ($treatModelOptimistically) return true;
                    return $model->idResponsible == $user->id;
                case self::MOD_PROJECT_DELEGATED:
                    if ($treatModelOptimistically) return true;
                    return $model->isProjectDelegatedToUser($user->id);
                case self::MOD_HAS_MANUFACTURER_AND_CLASS:
                    if ($treatModelOptimistically) return true;
                    return Permission::isManufacturerAndClassAllowed($model->idManufacturer, $model->idCoreInstrumentClass);
                case self::MOD_HAS_MANUFACTURER:
                    if ($treatModelOptimistically) return true;
                    return Permission::isManufacturerAllowed($model->idManufacturer);
                default:
                    return False;
            }
        }


        return false;
    }

    public static function getCheckboxName($zoneId, $modId, $actionId)
    {
        return ("rbac[" . $zoneId . "][" . $modId . "][]");
    }

    public static function getRadioName($zoneId, $modId)
    {
        return ("rbac[" . $zoneId . "][" . $modId . "][]");
    }

    public static function actionsList()
    {
        return array(
            self::ACTION_ADMIN => 'Таблица и меню',
            self::ACTION_DELETE => 'Удаление',
            self::ACTION_CREATE => 'Создание',
            self::ACTION_UPDATE => 'Изменение',
            self::ACTION_VIEW => 'Просмотр',
            self::ACTION_EDIT => 'Полный доступ',
            self::ACTION_READONLY => 'Только чтение',
            self::ACTION_NONE => 'Запрет',
        );
    }

    public static function getAllGroups()
    {
        $v = CHtml::listData(Group::model()->findAll(), "idGroup", "name");
        $v[] = "Без группы!";
        return $v;

    }

    /**
     * @param int $zone
     * @param bool $strictPolicy
     * @param string $tableAlias
     * @param bool $personalProjectsOnly skip Project Stages during compilation, if possible
     * @return array
     */
    public static function getUglySql($zone = self::ZONE_PROJECT, $strictPolicy = False, $tableAlias = 't', $personalProjectsOnly = false)
    {
        //@todo CACHING!!!

        $myId = Yii::app()->user->id;
        $myGroup = Yii::app()->user->idGroup;

        $permissions = Permission::model()->findAllByAttributes(array('idUser' => $myId));

        $rules = self::getGroupRules($myGroup);
        $projectRules = $rules[$zone];
        $additionalWithes = [];
        $condition = [];
        $stages = [];

        foreach ($projectRules as $modId => $allowedActions) {
            if (in_array(self::ACTION_ADMIN, $allowedActions)) {
                /* ----------------------------- */

                if ($modId >= self::MOD_PROJECT_STAGE_BASE) {
                    $stages[] = $modId - self::MOD_PROJECT_STAGE_BASE;
                } else {
                    switch ($modId) {
                        case self::MOD_DEFAULT:
                            # if VIEW is activated on ALL CASES - we have nothing to add here...
                            if (!$strictPolicy)
                                return array("True", array());
                            break;
                        case self::MOD_PROJECT_OWNER:
                            $condition[] = "($tableAlias.idResponsible = $myId)";
                            break;
                        case self::MOD_PROJECT_DELEGATED:
                            $condition[] = "(atomicRbacs.idUser = $myId)";
                            $additionalWithes[] = 'atomicRbacs';
                            break;
                        case self::MOD_HAS_MANUFACTURER:
                            $simpleManufacturers = array();

                            foreach ($permissions as $p) {
                                $simpleManufacturers[] = $p->idManufacturer;
                            }
                            if (count($simpleManufacturers)) {
                                $condition[] = "($tableAlias.idManufacturer IN (" . implode(', ', $simpleManufacturers) . "))";
                            }

                            break;
                        case self::MOD_HAS_MANUFACTURER_AND_CLASS:
                            $simpleManufacturers = array();

                            foreach ($permissions as $p) {

                                if ($p->idCoreInstrumentClass != null) {
                                    $condition[] = '(t.idManufacturer = ' . $p->idManufacturer . ' and t.idCoreInstrumentClass = ' . $p->idCoreInstrumentClass . ")";
                                } else {
                                    $simpleManufacturers[] = $p->idManufacturer;
                                }
                            }
                            if (count($simpleManufacturers)) {
                                $condition[] = "($tableAlias.idManufacturer IN (" . implode(', ', $simpleManufacturers) . "))";
                            }

                            break;
                    }
                }
                /* ----------------------------- */
            }
        }
        if (count($stages)) {
            if (!$personalProjectsOnly || count($condition) == 0) {
                $condition[] = "($tableAlias.enumStatus IN (" . implode(', ', $stages) . "))";
            }
        }

        $sql = (implode(" OR ", $condition));
        return array($sql ? $sql : "True", $additionalWithes);

    }

    public static function checkAccessToAction($z, $a)
    {
    }

    public static function getTopMenu()
    {
        $conf = self::config();

        $allTopMenu = array(
            self::ZONE_CLIENT => False,
            self::ZONE_COMPANY_CLASS => False,
            self::ZONE_COMPANY => False,
            self::ZONE_COREINSTRUMENT => False,
            self::ZONE_USER_MANAGEMENT => False,
            self::ZONE_INSURANCE_COMPANY => False,
            self::ZONE_FEATURE => False,
            self::ZONE_CUSTOM => False,
            self::ZONE_MANUFACTURER => False,
            self::ZONE_CONTRAGENT => False,
            self::ZONE_PROJECT => False,
            self::ZONE_MEASUREMENT_CENTRE => False,
        );
	return $allTopMenu;
        $setId = Yii::app()->user->idGroup;
	
	if (!$setId) {
            return $allTopMenu;
        }
	
        $rules = self::getGroupRules($setId);
        $helpers = self::modsList();


        foreach ($allTopMenu as $zoneId => $meep) {
            $zoneConf = $conf[$zoneId];
            $zoneRules = array_key_exists($zoneId, $rules) ? $rules[$zoneId] : array();

            $user = Yii::app()->user->user();
            /*---------------------------*/
            foreach ($zoneConf['mod'] as $modId) {
                if (!array_key_exists($modId, $zoneRules))
                    continue;
                if (in_array(self::ACTION_ADMIN, $zoneRules[$modId])) {
                    $allTopMenu[$zoneId] = True;
                }
            }
        }

        return $allTopMenu;

    }

    public static function getStartingTab()
    {
        $gid = Yii::app()->user->idGroup;
        $name = "TAB_GROUP_" . $gid;
        $m = Yii::app()->cache->get($name);
        if (!$m) {
            if (Yii::app()->user->user()->relatedGroup) {
                $m = Yii::app()->user->user()->relatedGroup->startingTab;
                Yii::app()->cache->set($name, $m);
            } else {
                return Null;
            }
        }
        return $m;
    }

    public function beforeSave()
    {
        if ($this->isNewRecord && !$this->jsonEncodedRules) {
            $this->setRules($this->_getDefaultRules());
        }
        return parent::beforeSave();
    }

    public function setRules($array)
    {
        $this->jsonEncodedRules = CJSON::encode($array);
        $this->dropCache();
    }

    private function dropCache()
    {
        Yii::app()->cache->delete("RULES_GROUP_" . $this->idGroup);
        Yii::app()->cache->delete("TAB_GROUP_" . $this->idGroup);
    }

    private static function _getDefaultRules()
    {
        $config = self::config();
        $def = array();
        foreach ($config as $zoneId => $zone) {
            $def[$zoneId] = $zone['default'];
        }
        return $def;
    }

    public function beforeDelete()
    {
        if ($this->idGroup <= 3) {
            throw new CHttpException(403, "Нельзя удалить системную группу.");
            return false;
        }
        return parent::beforeDelete();
    }

    public function getRules($force = False)
    {
        $array = Yii::app()->cache->get("RULES_GROUP_" . $this->idGroup);
        if ($force || $array === false) {
            $var = $this->jsonEncodedRules;

            $array = Null;

            try {
                $array = CJSON::decode($var);
            } catch (Exception $e) {
                $array = Null;
            }
            if (!$array) {
                $array = self::_getDefaultRules();
            }

            $this->saveRules($array);

            Yii::app()->cache->set("RULES_GROUP_" . $this->idGroup, $array, 24 * 60 * 60);
        }
        return $array;
    }

    private function saveRules($array)
    {
        $this->setRules($array);
        $this->update("jsonEncodedRules");
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'group';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required'),
            array('system', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
            array('jsonEncodedRules, isLogist, startingTab', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idGroup, name, jsonEncodedRules, system', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'users' => array(self::HAS_MANY, 'User', 'idGroup'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'idGroup' => 'Id Group',
            'name' => 'Name',
            'jsonEncodedRules' => 'Json Encoded Rules',
            'system' => 'System',
            'isLogist' => 'Это группа для логистов?',
            'startingTab' => 'Стартовая вкладка',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('name', $this->name, true);
        $criteria->compare('isLogist', $this->isLogist);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}
