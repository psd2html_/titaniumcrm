<?php

/**
 * This is the model class for table "sticky".
 *
 * The followings are the available columns in table 'sticky':
 * @property integer $idSticky
 * @property integer $idUser
 * @property integer $isTask
 * @property integer $idResponsible
 * @property string $date
 * @property string $calendarDate
 * @property string $text
 * @property string $postCommitComment
 * @property integer $idProject
 * @property bool $showInCalendar
 * @property int $template
 * @property string $colour
 *
 * The followings are the available model relations:
 * @property Project $project
 */
class Sticky extends ActiveRecord
{
    public $searchUser;
    private $_isTaskOld;
    private $_isDoneOld;

    public static function getTemplateList($includeIcons = false)
    {
        $a = array(0 => 'Прочее',
            1 => 'Встреча',
            2 => 'Телефонный звонок',
            3 => 'Email',
            4 => 'Экскурсия на завод',
        );
        if ($includeIcons) {
            foreach ($a as $k => $v) {
                $a[$k] = CHtml::image(self::getTemplateIcons($k)) . ' ' . $v;
            }
        }
        return $a;
    }

    public static function getTemplateIcons($type)
    {

        $k =
            array(
                0 => '/images/actions/note.png',
                1 => '/images/actions/meeting.png',
                2 => '/images/actions/call.png',
                3 => '/images/actions/email.png',
                4 => '/images/actions/factory.png',
            );
        return (isset($k[$type])) ? $k[$type] : null;


    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Sticky the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getIsOutdated()
    {
        if ($this->isDone) return false;
        $ts = strtotime($this->calendarDate);
        if ($ts <= strtotime("midnight", time()))
            return true;
        return false;
    }

    public function getCustomClass()
    {
        return $this->isOutdated ? 'outdatedRow' : null;
    }

    function behaviors()
    {
        return array(
            'loggable' => array(
                'class' => 'ActiveRecordLoggable',
                'logCreation' => true,
                'shouldBubbleToMaster' => true,
                'hostAfterSaveAction' => 'updateLastChange',
            )
        );
    }

    public function afterSave()
    {

        if ($this->isNewRecord) {
            if ($this->isTask) {
                PersonalMessageService::notifyAboutNewTask($this);
            }
        } else {
            if (!$this->_isTaskOld && $this->isTask) {
                PersonalMessageService::notifyAboutNewTask($this);
            }
            if (!$this->_isDoneOld &&  $this->isDone) {
                if ($this->idUser!=$this->idResponsible) {
                    PersonalMessageService::notifyAboutFinishedTask($this);
                }
            }
        }

        parent::afterSave();
    }

    public function afterFind()
    {
        $this->_isTaskOld = $this->isTask;
        $this->_isDoneOld = $this->isDone;
        parent::afterFind();
    }

    public function getName()
    {
        return Helpers::more(strip_tags($this->text), 255);
    }

    public function init()
    {
        $this->idUser = Yii::app()->user->id;
        $this->idResponsible = Yii::app()->user->id;
        $this->date = Yii::app()->dateFormatter->format("yyyy-M-d", time());
        $this->calendarDate = Yii::app()->dateFormatter->format("yyyy-M-d", time());
    }

    public function getStickyHtml() {
        if ($this->isTask) {
            return $this->getTaskHtmlFormatted();
        }  else {
            return $this->getTextWithBottom();
        }
    }

    public function getTaskHtmlFormatted()
    {
        $isOutdated = $this->isOutdated;
        $out = '';

        $out .=
            CHtml::link($this->getTemplateIcon() . ' ' . $this->text, Yii::app()->createUrl("sticky/update", array("id" => $this->getPrimaryKey())), array('class' => 'foundationModalTrigger'));

        if ($isOutdated)
            $out .= '<span class="outdated">';
        $out .= '<small>';

        if ($this->responsible) {
            $out .= '<br/> Отв.' . $this->responsible->fullName;
        }

        if ($this->calendarDate) {
            $out .= '<br/> Deadline ' . Yii::app()->dateFormatter->formatDateTime($this->calendarDate, 'medium', false);
            if ($isOutdated) {
                $out .= ' <span class="alert label round">!</span>';
            }
        }
        $out .= '</small>';
        if ($isOutdated)
            $out .= '</span>';
        return $out;
    }

    public function getTemplateIcon()
    {
        return CHtml::image(self::getTemplateIcons($this->template));
    }

    public function getTextWithBottom()
    {
        $out = '';
        $out .= CHtml::link("<i class='fa fa-pencil'></i>", Yii::app()->createUrl("sticky/update", array("id" => $this->getPrimaryKey())), array('class' => 'right button secondary tiny update foundationModalTrigger'));
        $out .=   $this->text."<br/>";
        $out .= '<small>';
        if ($this->user) {
            $out .= $this->user->fullName . ', ';
        }

        if ($this->date) {
            $out .= Yii::app()->dateFormatter->formatDateTime($this->date, 'medium', false);
        }
        $out .= '</small>';

        return $out;
    }

    public function beforeDelete()
    {

        if (Yii::app()->user->isGuest) {
            return false;
        }
        if (Yii::app()->user->isAdmin()) {
            return true;
        }

        if (!$this->project->isSubzoneWritable(Group::SUBZONE_PROJECT_TASK)) {
            $this->addError('', 'У вас нет необходимых прав для совершения данного действия');
            return false;
        }


        return parent::beforeDelete();
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'sticky';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array(' idUser, text, idProject', 'required'),
            array(' idUser, idProject', 'numerical', 'integerOnly' => true),
            array('colour', 'length', 'max' => 45),
            array('postCommitComment, date, isTask, text, template, calendarDate, isDone, idResponsible, showInCalendar', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('searchUser, idSticky, idUser, text, idProject, colour', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'project' => array(self::BELONGS_TO, 'Project', 'idProject'),
            'user' => array(self::BELONGS_TO, 'User', 'idUser'),
            'responsible' => array(self::BELONGS_TO, 'User', 'idResponsible'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'checkColumn'=>'',
            'idSticky' => 'Id Sticky',
            'idUser' => 'Автор',
            'date' => 'Дата создания',
            'template' => 'Шаблон',
            'showInCalendar' => 'Отображать в календаре',
            'text' => 'Заметка',
            'idProject' => 'Проект',
            'colour' => 'Colour',
            'isDone' => 'Выполнен',
            'isTask' => 'Задание или нет',
            'calendarDate' => 'Сделать к дате',
            'idResponsible' => 'Назначено для',
            'postCommitComment' => 'Комментарий',
        );
    }
    public $notAssignedToAuthor=false;
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idSticky', $this->idSticky);
        $criteria->compare('idUser', $this->idUser);

        if ($this->searchUser) {
            $criteria->addCondition("t.idUser = :su OR t.idResponsible = :su");
            $criteria->params['su'] = $this->searchUser;

        }

        $criteria->compare('date', $this->date, true);
        $criteria->compare('calendarDate', $this->calendarDate, true);
        $criteria->compare('isDone', $this->isDone);
        $criteria->compare('isTask', $this->isTask);
        $criteria->compare('idResponsible', $this->idResponsible);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('idProject', $this->idProject);
        $criteria->compare('colour', $this->colour, true);

        if ($this->notAssignedToAuthor) {
            $criteria->addCondition("t.idUser!=t.idResponsible");
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}