<?php

class ProtoProject extends ActiveRecord
{


    const ACTIVE_PROJECT_FILTER = 1;
    const ENDED_PROJECT_FILTER = 2;
    const STATUS_INIT = 0;
//    const STATUS_COMMUNICATION = 10;
    const STATUS_CONTEST = 20;
    const STATUS_BUY = 30;
    const STATUS_SUPPLY = 30;
    //   const STATUS_LOGISTIC = 40;
    //   const STATUS_SETUP = 50;
    const STATUS_SUPPORT = 60;
    const STATUS_FINAL = 70;
    const STATUS_FAILED = 100;

    public static function getEnumProvisionList()
    {
        return array(
            0 => 'Неизвестно',
            1 => 'Внесено',
            2 => 'Не внесено',
            3 => 'Не требуется',);
    }

    public static function getFuzzyBooleanList($o = null)
    {
        $s =
            array(0 => 'Нет', '1' => 'Да', '2' => 'Неизвестно');

        return ($o === null) ? $s : $s[$o];
    }

    public static function getEnumStatusList($includeBoxes = false, $context = 0)
    {
        if ($context == 0) {
            $list = array(
                self::STATUS_INIT    => 'Подготовка',
                self::STATUS_CONTEST => 'Конкурс',
                self::STATUS_SUPPLY  => 'Поставка',
                self::STATUS_SUPPORT => 'Сервис',
                self::STATUS_FINAL   => 'Установлен',
                self::STATUS_FAILED  => 'Проигран',
            );
        } else if ($context == 1) {
            $list = array(
                self::STATUS_INIT    => 'Подготовка',
                self::STATUS_CONTEST => 'Конкурс',
                self::STATUS_SUPPLY  => 'Поставка',
                self::STATUS_SUPPORT => 'Сервис',
            );
        } else if ($context == 2) {
            $list = array(
                self::STATUS_FINAL  => 'Установлен',
                self::STATUS_FAILED => 'Проигран',
            );
        }

        if (!$includeBoxes) {
            return $list;
        } else {
            $colors = self::getEnumStatusColours();

            foreach ($list as $key => $value) {
                $list[$key] = '<div class="cbox-fixed" style="background-color:' . $colors[$key] . ' "></div> ' . $value;
            }

            return $list;
        }
    }

    public static function getEnumFinanceStatusList()
    {
        return array(0 => 'Нет оплаты',
                     1 => 'Оплата проведена',
        );
    }

    public function getStatusBox()
    {
        return '<div class="cbox-fixed" style="background-color:' . $this->statusColor . ' "></div>';
    }

    public function getStatusColor()
    {
        $a = Project::getEnumStatusColours();

        return $a[$this->enumStatus];
    }

    public static function getEnumStatusColours()
    {
        return array(
            self::STATUS_INIT    => "#FFFFFF",
            //self::STATUS_COMMUNICATION => "#9966CC",
            self::STATUS_CONTEST => "#FF0000",
            self::STATUS_SUPPLY  => "#00FF00",
            //self::STATUS_LOGISTIC => "#3399CC",
            //self::STATUS_SETUP => "#CCFF00",
            self::STATUS_SUPPORT => "#0000FF",
            self::STATUS_FINAL   => "#000000",
            self::STATUS_FAILED  => "#000000"
        );
    }

    public function getPriorityColumn()
    {
        $a = self::getPriorityList();

        return $a[$this->priority];
    }

    public static function getPriorityList()
    {
        return array(
            0 => 'Обычный приоритет',
            1 => 'Повышенный приоритет'
        );
    }

    /*
          * 1) Подготовка (белый)
     ранее: Инициализация, заявка
     2) Конкурс (красный)
     ранее: конкурс
     3) Поставка (зелёный)
     ранее: Финансы, закупка, логистика
     4) Сервис (синий)
     ранее: обслуживание
     5) Закрыт (чёрный)
     ранее: завершено
     6) Проигран (чёрный)
     ранее: завершено-проиграно/потерян интерес
         */

    // 10 -> 0
    // 50,40 -> 30

    public function getBuyIncotermsConditionColumn()
    {
        $a = self::getIncotermsList();

        return $a[$this->buyIncotermsCondition];
    }

    public static function getIncotermsList()
    {
        return array(
            0 => 'Неизвестно',
            1 => 'DDP',
            2 => 'CIP',
            3 => 'FOB',
            4 => 'EXW',
            5 => 'FCA',
            6 => 'CIF',
        );
    }

    public function getSellIncotermsConditionColumn()
    {
        return $this->sellIncotermsCondition;
    }

    /**
     * @param User $user
     */
    public function canChangeResponsible($user)
    {

        return (($this->idResponsible == $user->id) || ($user->superuser));
    }

    public function rules()
    {
        $rules = array(

            array('idResponsible', 'required'),
            array('idCompanyConsumer, idManufacturer, idCoreInstrumentClass', 'required', 'on' => 'insert'),
            array('contractTotalCurrency, enumFinanceStatus, idCustomer, idResponsible', 'numerical', 'integerOnly' => true),
            array('year, quartal', 'numerical', 'allowEmpty' => true, 'integerOnly' => true),
            array('innerName', 'length', 'max' => 64),
            array('virtualCoreInstrument, isInsurancePayed, isExportDeclarationMade, isExportDoubleApplicationLicenseRequired, isSpecialDeliveryRequired, isTechnoinfoUnloadsGoods, packagingSpecification, consigneeAddress, goodsNameRus, goodsOrigin, goodsNameEng, poNumber, contractDate, contractTotal, contractNumber', 'safe'),
            array('setupDate, setupComment, actualDeliveryAddress, isPurchaseViaManufacturer, takeoutAddress, tnvedCod, priority, buyIncotermsCondition, sellIncotermsCondition, enumProvision, idInsuranceCompany, yearQuartal, pageSize, parent, searchManufacturer, enumStatus, searchTownCompany,searchCompany, searchCoreInstrument, comment, isBuyerAndConsumerDiffers, idCoreInstrument,idCustomer, idCompanyBuyer', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('actualDeliveryAddress, isPurchaseViaManufacturer, idProject, searchContragent, enumStatus, idCoreInstrumentClass, enumFinanceStatus, year, quartal, innerName, name, comment, idCustomer, idResponsible', 'safe', 'on' => 'search'),
            // Костыль для установки NULL в дату, если она пуста - иначе она станет 1899 годом :c
            array('contractDate, setupDate', 'default', 'setOnEmpty' => 'true', 'value' => null),
        );
        if (!$this->canChangeResponsible(Yii::app()->user->user())) {
            $rules[] = array('idResponsible', "unsafe", 'on' => 'update');
        }

        return $rules;
    }

    public function searchableFields()
    {
        return array(
            array('year', 'dummy', 'sort' => '(quartal>0) DESC, year, quartal', 'sortDesc' => '(quartal>0) DESC, year DESC, quartal DESC', 'name' => 'year'),
            array('quartal', 'dummy', 'sort' => '(quartal>0) DESC, year, quartal', 'sortDesc' => '(quartal>0) DESC, year DESC, quartal DESC', 'name' => 'quartal'),
            array('earliestSampleStatus', 'dummy', 'sort' => 'ISNULL(earliestSampleStatus) ASC, earliestSampleStatus', 'sortDesc' => 'ISNULL(earliestSampleStatus) ASC, earliestSampleStatus DESC', 'name' => 'earliestSampleStatus'),
            array('productionDeadline', 'dummy', 'from' => 'logisticRelated', 'sort' => 'ISNULL(logisticRelated.productionDeadline) ASC, logisticRelated.productionDeadline', 'sortDesc' => 'ISNULL(logisticRelated.productionDeadline) ASC, logisticRelated.productionDeadline DESC', 'name' => 'productionDeadline'),
            array('shipmentDeadline', 'dummy', 'from' => 'logisticRelated', 'sort' => 'ISNULL(logisticRelated.shipmentDeadline) ASC, logisticRelated.shipmentDeadline', 'sortDesc' => 'ISNULL(logisticRelated.shipmentDeadline) ASC, logisticRelated.shipmentDeadline DESC', 'name' => 'shipmentDeadline'),
            array('contestDeadline', 'dummy', 'from' => 'contestRelated', 'sort' => 'ISNULL(contestRelated.dateRequest) ASC, contestRelated.dateRequest', 'sortDesc' => 'ISNULL(contestRelated.dateRequest) ASC, contestRelated.dateRequest DESC', 'name' => 'contestDeadline'),

            array('manufacturer.name', 'relation', 'from' => 'manufacturer', 'name' => 'searchManufacturer', 'label' => 'Производитель'),
            array('coreInstrument.name', 'relation', 'from' => 'coreInstrument', 'name' => 'searchCoreInstrument', 'label' => 'Базовый прибор'),
            array('companyConsumer.physicalAddressEng', 'relation', 'from' => 'companyConsumer', 'name' => 'companyFullEnglishAddress', 'label' => 'Адрес организации-потребителя (англ)'),
            array('companyConsumer.fullNameEng', 'relation', 'from' => 'companyConsumer', 'name' => 'companyFullEnglishName', 'label' => 'Организация-потребитель (англ)'),
            array('companyBuyer.fullName', 'relation', 'from' => 'companyBuyer',
                                                       'sort' => 'ISNULL(idCompanyBuyer) ASC, companyBuyer.fullName', 'sortDesc' => 'ISNULL(idCompanyBuyer) ASC, companyBuyer.fullName DESC',
                                                       'name' => 'searchCompanyBuyerByName', 'label' => 'Компания-поставщик'),


            //array('coreInstrument.idCoreInstrumentClass', 'relation', 'exact' => true, 'from' => 'coreInstrument', 'name' => 'idCoreInstrumentClass', 'label' => 'Базовый прибор'),
            array('logisticRelated.idContragent', 'relation', 'exact' => true, 'from' => 'logisticRelated', 'name' => 'searchContragent', 'label' => 'Контрагент'),
            array('companyConsumer.idCompanyClass', 'relation', 'exact' => true, 'from' => 'companyConsumer', 'name' => 'searchCompanyClass', 'label' => 'Тип организации'),

            array('idCompanyBuyer, idCompanyConsumer', 'aggregate', 'exact' => true, 'name' => 'searchIdCompany', 'label' => 'Компания'),
            array('parent, idProject', 'aggregate', 'exact' => true, 'name' => 'searchAllLinkedProjects', 'label' => 'Компания'),

            array('companyBuyer.fullName, companyBuyer.shortName, companyConsumer.fullName, companyConsumer.shortName', 'aggregate', 'from' => 'companyConsumer, companyBuyer', 'name' => 'searchCompany', 'label' => 'Компания'),
            array('companyBuyer.city, companyConsumer.city', 'aggregate', 'from' => 'companyConsumer, companyBuyer', 'name' => 'searchTownCompany', 'label' => 'Компания'),
            array('companyConsumer.fullName, companyConsumer.shortName, companyConsumer.fullNameEng', 'aggregate', 'from' => 'companyConsumer', 'name' => 'searchCompanyConsumer', 'label' => 'Организация-потребитель'),

            array('tnvedCod, year, innerName, name, comment, takeoutAddress', 'partial'),
            array('closestTask', 'relation', 'from' => 'closestTaskSorter', 'sort' => 'isTaskFree, closestTaskDate', 'sortDesc' => 'isTaskFree, closestTaskDate DESC', 'name' => 'closestTask'),
            array('idManufacturer, enumStatus, idInsuranceCompany, idCoreInstrumentClass, idResponsible, quartal, idCompanyConsumer', 'exact'),

            array('earliestSampleStatus, countOfOrders, priority, enumProvision, isInsurancePayed, isExportDeclarationMade, isExportDoubleApplicationLicenseRequired, isSpecialDeliveryRequired, isTechnoinfoUnloadsGoods', 'exact'),
            array('projectName, packagingSpecification, consigneeAddress, goodsNameRus, goodsOrigin, goodsNameEng, poNumber, contractDate, contractTotal, contractNumber', 'partial'),
            array('year', 'aggregate', 'exact' => false, 'name' => 'searchYear', 'label' => 'Год/квартал', 'sort' => 'year, quartal', 'sortDesc' => 'year DESC, quartal DESC'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {

        return array(
            'atomicRbacs'           => array(self::HAS_MANY, 'AtomicRbac', 'idProject', 'on' => 'atomicRbacs.enumRuleClass = ' . AtomicRbac::RBAC_PROJECT),
            'contractUpload'        => array(self::BELONGS_TO, 'Upload', 'idContractUpload'),
            'roomMeasurementUpload' => array(self::BELONGS_TO, 'Upload', 'idRoomMeasurementUpload'),
            'setupReportUpload'     => array(self::BELONGS_TO, 'Upload', 'idSetupReportUpload'),

            'closestTask2' => array(self::HAS_ONE, 'Sticky', 'idProject', 'condition' => '(closestTask2.isTask=1) AND (closestTask2.date >= CURDATE() OR closestTask2.isDone =0 )', 'order' => 'closestTask2.calendarDate ASC'),

            'closestTaskSorter'     => array(self::HAS_ONE, 'Sticky', 'idProject',
                'select' => 'MIN(closestTaskSorter.calendarDate) as closestTaskDate, (MIN(closestTaskSorter.calendarDate) IS NULL) as isTaskFree',
                'on'     => 'closestTaskSorter.isTask=1 AND (closestTaskSorter.calendarDate >= CURDATE() OR closestTaskSorter.isDone =0 )',
                'group'  => 't.idProject'),
            'spents'                => array(self::HAS_MANY, 'Spent', 'idProject'),
            'childrenProjectsCount' => array(self::STAT, 'Project', 'parent'),
            'childrenProjects'      => array(self::HAS_MANY, 'Project', 'parent'),
            'sticky'                => array(self::HAS_MANY, 'Sticky', 'idProject'),
            'folders'               => array(self::HAS_MANY, 'Folder', 'idProject'),
            'topLevelFolders'       => array(self::HAS_MANY, 'Folder', 'idProject', 'on' => 'parent IS NULL'),
            'logisticFragments'     => array(self::HAS_MANY, 'LogisticFragment', 'idProject'),
            'logisticPositions'     => array(self::HAS_MANY, 'LogisticPosition', 'idProject'),
            'parentProject'         => array(self::BELONGS_TO, 'Project', 'parent'),
            'coreInstrument'        => array(self::BELONGS_TO, 'CoreInstrument', 'idCoreInstrument'),
            'insuranceCompany'      => array(self::BELONGS_TO, 'InsuranceCompany', 'idInsuranceCompany'),
            'coreInstrumentClass'   => array(self::BELONGS_TO, 'CoreInstrumentClass', 'idCoreInstrumentClass'),
            'lastActionLog'         => array(self::BELONGS_TO, 'ActiveRecordLog', 'lastChangeId'),
            'companyBuyer'          => array(self::BELONGS_TO, 'Company', 'idCompanyBuyer'),
            'companyConsumer'       => array(self::BELONGS_TO, 'Company', 'idCompanyConsumer'),
            'manufacturer'          => array(self::BELONGS_TO, 'Manufacturer', 'idManufacturer'),
            'customersRaw'          => array(self::MANY_MANY, 'Customer', 'customer2project(idProject, idCustomer)'),
            'attachments'           => array(self::MANY_MANY, 'Attachment', 'attachment2project(idProject, idAttachment)'),
            'contestRelated'        => array(self::HAS_ONE, 'Contest', 'idProject'),

            'tasksActive'    => array(self::HAS_MANY, 'Sticky', 'idProject', 'condition' => '(tasksActive.isDone=0 AND tasksActive.isTask=1)'),
            'tasksFinished'  => array(self::HAS_MANY, 'Sticky', 'idProject', 'condition' => '(tasksFinished.isDone=1 AND tasksFinished.isTask=1)'),
            'commentsSticky' => array(self::HAS_MANY, 'Sticky', 'idProject', 'condition' => 'commentsSticky.isTask=0'),


            'stickyClosest'   => array(self::HAS_ONE, 'Sticky', 'idProject', 'condition' => '(showInCalendar=1 OR isTask=1) AND `calendarDate`>=NOW()', 'order' => 'date asc'),
            'logisticRelated' => array(self::HAS_ONE, 'Logistic', 'idProject'),
            'responsible'     => array(self::BELONGS_TO, 'User', 'idResponsible'),
            'samples'         => array(self::HAS_MANY, 'Sample', 'idProject'),
            'orders'          => array(self::HAS_MANY, 'Order', 'idProject'),

            'uploads'      => array(self::HAS_MANY, 'Upload', 'idProject'),
            'uploadsCount' => array(self::STAT, 'Upload', 'idProject'),

        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        $p = parent::attributeLabels();

        return $p + array(
                'engPreview'                => 'Карточка проекта (англ.)',
                'setupDate'                 => 'Дата Установки',
                'setupComment'              => 'Комментарий по установке',
                // --------------- deprecated --------------------------------
                'productionDeadline'        => 'Дедлайн производства',
                'poNumber'                  => 'Номер  PO',
                'goodsNameRus'              => 'Название на русском для страхования',
                'goodsOrigin'               => 'Производитель/страна происхождения ',
                'goodsNameEng'              => 'Название на англ. согласно контракту с поставщиком',
                'idInsuranceCompany'        => 'Страховая компания',
                'isPurchaseViaManufacturer' => 'Закупка через производителя или посредника',
                'packagingSpecification'    => 'Количество мест и вид тары, измерения, вес нетто/брутто',
                'isInsurancePayed'          => 'Страховка',
                'buyIncotermsCondition'     => 'Условия закупки у поставщика',

                'takeoutAddress'                           => 'Адрес забора и контактные данные',
                'isTechnoinfoUnloadsGoods'                 => 'Кто разгружает – заказчик или Техноинфо',
                'isExportDeclarationMade'                  => 'Делает ли производитель экспортную декларацию ',
                'isSpecialDeliveryRequired'                => 'Нужны ли особые условия транспортировки  ',
                'isExportDoubleApplicationLicenseRequired' => 'Необходима ли экспортная лицензия как на товар двойного назначения ',
                'tnvedCod'                                 => 'Код ТНВЭД',
                // --------------- deprecated --------------------------------
                'sellIncotermsCondition'                   => 'Условия продажи покупателю',

                'contractTotal'         => 'Стоимость по контракту',
                'payedByUser'           => 'Оплачено пользователем',
                'contractTotalCurrency' => 'Валюта',
                'virtualCoreInstrument' => 'Позиция для отображения вместо базового инструмента',


                'priority'              => 'Приоритет',
                'projectName'           => 'Название проекта',
                'actualDeliveryAddress' => 'Фактический адрес доставки',
                'created'               => 'Дата создания проекта',
                'consigneeAddress'      => 'Адрес грузополучателя по контракту ',

                'contractDate'   => 'Дата контракта',
                'contractNumber' => 'Номер контракта',
                'enumProvision'  => 'Обеспечение',

                'fileCoreInstrumentDescription' => 'Описание прибора на русском с фотографией(ями)',
                'filePackingList'               => 'Упаковочный от производителя  ',
                'fileContract'                  => 'Контракт с покупателем ',

                'lastActionLog'    => 'Последнее изменение',
                'contestDeadline'  => 'Заявка - deadline',
                'searchContragent' => 'Контрагент',
                'yearQuartal'      => 'Год/квартал',

                'samples'              => 'Измерения',
                'earliestSampleStatus' => 'Измерения',

                'eventClosest'              => 'Ближайшее событие',
                'customersRaw'              => 'Клиенты',
                'lastChange'                => 'Дата последнего изменения',
                'attachments'               => 'Файлы',
                'events'                    => 'События',
                'logisticRelated'           => '',
                'contestRelated'            => '',
                'idProject'                 => 'Id Project',
                'enumStatus'                => 'Текущая группа этапов',
                'shipmentDeadline'          => 'Дедлайн поставки',
                'idProject'                 => 'Проект',
                'searchCompanyConsumer'     => 'Огранизация-потребитель',
                'customersColumn'           => 'Конечный пользователь',
                'enumFinanceStatus'         => 'Статус Финансирования',
                'year'                      => 'Год закупки',
                'quartal'                   => 'Квартал закупки',
                'innerName'                 => 'Внутреннее имя производителя',
                'name'                      => 'Имя проекта',
                'comment'                   => 'Комментарий',
                'isBuyerAndConsumerDiffers' => 'Организация потребитель и организация-поставщик отличаются',

                'orders'        => 'Коммер. предложения',
                'countOfOrders' => 'Коммер. предложения',

                'idCompanyBuyer'        => 'Организация, что закупает оборудование',
                'idCompanyConsumer'     => 'Конечный потребитель',
                'companyBuyer'          => 'Организация, что закупает оборудование',
                'companyConsumer'       => 'Конечный потребитель',
                'searchCompany'         => 'Организация-потребитель',
                'searchTownCompany'     => 'Город',
                'searchCoreInstrument'  => 'Базовый прибор',
                'idCoreInstrument'      => 'Базовый прибор',
                'coreInstrument'        => 'Базовый прибор',
                'CompanyBuyer'          => 'Организация-покупатель',
                'CompanyConsumer'       => 'Организация-потребитель',
                'idResponsible'         => 'Ответственный',
                'responsible'           => 'Ответственный',
                'suppliers'             => 'Поставищики оборудования',
                'enumProjectClass'      => 'Тип проекта',
                'idCoreInstrumentClass' => 'Класс оборудования',
                'coreInstrumentClass'   => 'Класс оборудования',
                'idManufacturer'        => 'Производитель оборудования',
                'manufacturer'          => 'Производитель оборудования',
                'searchManufacturer'    => 'Производитель',
                'idContragent'          => 'Контрагент',
                'samplesStatus'         => 'Образцы/стадия',
                'closestTask'           => 'Ближайшая задача',
                'companyClass'          => 'Тип организации',
            );
    }
}
