<?php

/**
 * This is the model class for table "logisticFragment".
 *
 * The followings are the available columns in table 'logisticFragment':
 * @property integer $idLogisticFragment
 * @property string $cityFrom
 * @property string $cityTo
 * @property string $dateUnload
 * @property string $dateUpload
 * @property string $comment
 * @property integer $enumTransport
 * @property integer $idProject
 *
 * The followings are the available model relations:
 * @property Attachment[] $attachments
 * @property Project $project
 */
class LogisticFragment extends CActiveRecord
{
    public $attachment1, $attachment2, $attachment3;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return LogisticFragment the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getReadableName()
    {
        return 'Этап логистики ' . $this->cityFrom . ' > ' . $this->cityTo;
    }

    public function  getAttachmentColumn()
    {
        $out = array_map(function ($a) {
            /* @var Attachment $a */
            return $a->getDownloadLink();
        }, $this->attachments);

        return $out ? implode(', ', $out) : ' - ';
    }

    public function getEnumTransportText()
    {
        $a = Logistic::getEnumTransportList();
        return isset($a[$this->enumTransport]) ? ($a[$this->enumTransport]) : ("N/A");
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'logisticFragment';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('attachment1, attachment2, attachment3', 'file', 'types' => 'jpg, jpeg, gif, png, txt, doc, docx, pdf, xls, xlsx, zip, rar, ppt, pptx', 'allowEmpty' => 'true'),
            array('cityFrom, cityTo, enumTransport', 'required'),
            array('enumTransport, idProject', 'numerical', 'integerOnly' => true),
            array('cityFrom, cityTo', 'length', 'max' => 255),
            array('dateUnload, dateUpload, comment', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idLogisticFragment, cityFrom, cityTo, dateUnload, dateUpload, enumTransport, idProject', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'attachments' => array(self::MANY_MANY, 'Attachment', 'attachement2logisticfragment(idLogisticFragment, idAttachment)'),
            'project' => array(self::BELONGS_TO, 'Project', 'idProject'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'idLogisticFragment' => 'Id Logistic Fragment',
            'cityFrom' => 'Отправлен из',
            'cityTo' => 'Прибыл в',
            'dateUnload' => 'Дата отправления',
            'dateUpload' => 'Дата прибытия',
            'enumTransport' => 'Тип транспорта',
            'idProject' => 'Проект',
            'comment' => 'Комментарий',
        );
    }

    function behaviors()
    {
        return array(
            'CAdvancedArBehavior' => array(
                'class' => 'ext.Many2Many.CAdvancedArBehavior'),
            'loggable' => array(
                'class' => 'ActiveRecordLoggable',
                'logCreation' => true,
                'shouldBubbleToMaster' => true,
                'hostAfterSaveAction' => 'updateLastChange',
            )
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idLogisticFragment', $this->idLogisticFragment);
        $criteria->compare('cityFrom', $this->cityFrom, true);
        $criteria->compare('cityTo', $this->cityTo, true);
        $criteria->compare('dateUnload', $this->dateUnload, true);
        $criteria->compare('dateUpload', $this->dateUpload, true);
        $criteria->compare('enumTransport', $this->enumTransport);
        $criteria->compare('idProject', $this->idProject);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}