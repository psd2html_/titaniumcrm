<?php

/**
 * This is the model class for table "ActiveRecordLog".
 *
 * The followings are the available columns in table 'ActiveRecordLog':
 * @property string $idActiveRecordLog
 * @property string $description
 * @property string $action
 * @property string $model
 * @property string $idModel
 * @property string $field
 * @property string $creationDate
 * @property string $idUser
 *
 * // todo добавил потому как http://joxi.ru/LmGVOdVse8ZOjr
 * @property string $slaveModelId
 * @property string $slaveModel
 */
class ActiveRecordLog extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ActiveRecordLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ActiveRecordLog';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('creationDate', 'required'),
			array('description, newValue', 'safe'),
			array('action', 'length', 'max'=>20),
			array('model, field, idUser', 'length', 'max'=>45),
			array('idModel', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idActiveRecordLog, description, action, model, idModel, field, creationDate, idUser', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'user' => array(self::BELONGS_TO, 'User', 'idUser'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idActiveRecordLog' => 'Id Active Record Log',
			'description' => 'Действие',
			'action' => 'Action',
			'model' => 'Model',
			'idModel' => 'Id Model',
			'field' => 'Field',
			'creationDate' => 'Дата',
			'idUser' => 'Пользователь',
			'newValue' => 'Новое значение',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idActiveRecordLog',$this->idActiveRecordLog,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('action',$this->action,true);
		$criteria->compare('model',$this->model,true);
		$criteria->compare('idModel',$this->idModel);
		$criteria->compare('field',$this->field);
		$criteria->compare('newValue',$this->newValue,true);
		$criteria->compare('creationDate',$this->creationDate,true);
		$criteria->compare('idUser',$this->idUser,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}