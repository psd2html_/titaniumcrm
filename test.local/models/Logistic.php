<?php

/**
 * This is the model class for table "logistic".
 *
 * The followings are the available columns in table 'logistic':
 * @property integer $idLogistic
 * @property integer $idProject
 * @property string $enumStatus
 * @property string $lastChange
 * @property string $currentLocation
 * @property string $comment
 * @property string $shipmentDeadline
 * @property string $productionDeadline
 * @property integer $idContragent
 * @property integer $enumTransportBeforeBorder
 * @property integer $enumTransportAfterBorder
 * @property integer $idCustom
 *
 * The followings are the available model relations:
 * @property Project $project
 * @property Attachment[] $attachments
 * @property Event[] $events
 */
class Logistic extends ActiveRecord {
    public function beforeValidate() {
        
      //set null, if nothing is chosen
      if ($this->idContragent<=0)
          $this->idContragent=null;
      
      if ($this->idCustom<=0)
          $this->idCustom=null;
      
        return parent::beforeValidate();
    }
    
    public function getName()
    {
        return $this->project->name;
    }
    public function getTransportAfterBorder()
    {
        $a=self::getEnumTransportList();
        return $a[$this->enumTransportAfterBorder];
    }
    
    public function getTransportBeforeBorder()
    {
        $a=self::getEnumTransportList();
        return $a[$this->enumTransportBeforeBorder];
    }
           
    public static function getEnumTransportList() {
        return array(0 => 'Авиа',
            1 => 'Море',
            2 => 'ЖД',
            3 => 'Авто',
            "" => 'Не установлено'
        );
    }
    public static function getEnumStatusList() {
        return array(0 => 'Получена договоренность',
            1 => 'Телефонный звонок',
            2 => 'Проведена первоначальная встреча',
            3 => 'Экскурсия на завод',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Logistic the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'logistic';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('idProject ', 'required'),
            array('lastChange', 'default',
                'value' => 'CURRENT_TIMESTAMP',
                'setOnEmpty' => true, 'on' => 'create, update'),
            array('idProject, enumStatus, idContragent, enumTransportBeforeBorder, enumTransportAfterBorder, idCustom', 'numerical', 'integerOnly' => true),
            array('enumStatus', 'length', 'max' => 45),
            array('currentLocation', 'length', 'max' => 128),
            array('comment, shipmentDeadline, productionDeadline', 'safe'),
            // Костыль для установки NULL в дату, если она пуста - иначе она станет 1899 годом :c
            array('shipmentDeadline, productionDeadline', 'default','setOnEmpty'=>'true','value'=>null),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idProject, enumStatus, lastChange, currentLocation, comment, shipmentDeadline, productionDeadline, idContragent, enumTransportBeforeBorder, enumTransportAfterBorder, idCustom', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'project' => array(self::BELONGS_TO, 'Project', 'idProject'),
            'contragent' => array(self::BELONGS_TO, 'Contragent', 'idContragent'),
            'custom' => array(self::BELONGS_TO, 'Custom', 'idCustom'),
            'attachments' => array(self::MANY_MANY, 'Attachment', 'attachment2logistic(idLogistic, idAttachment)'),
            'events' => array(self::MANY_MANY, 'Event', 'event2logistic(idLogistic, idEvent)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'idLogistic' => 'Id Logistic',
            'idProject' => 'Id Project',
            'enumStatus' => 'Статус',
            'idContragent' => 'Контрагент',
            'idCustom' => 'Растаможка',
            'lastChange' => 'Дата последнего изменения',
            'currentLocation' => 'Текущее местонахождение',
            'comment' => 'Комментарий',
            'shipmentDeadline' => 'Deadline Поставки',
            'productionDeadline' => 'Deadline Производства',
            'contragent' => 'Контрагент',
            'enumTransportBeforeBorder' => 'Тип транспорта до границы РФ',
            'enumTransportAfterBorder' => 'Тип транспорта после границы РФ',
            'custom' => 'Через кого растаможено',
        );
    }

    function behaviors()
    {
        return array(
            'CAdvancedArBehavior' => array(
                'class' => 'ext.Many2Many.CAdvancedArBehavior'),
            'loggable' => array(
                'class' => 'ActiveRecordLoggable',
                'shouldBubbleToMaster' => true,
                'logCreation' => false,
                'hostAfterSaveAction'=>'updateLastChange',
            )
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->compare('idProject', $this->idProject);
        $criteria->compare('enumStatus', $this->enumStatus);
        $criteria->compare('lastChange', $this->lastChange, true);
        $criteria->compare('currentLocation', $this->currentLocation, true);
        $criteria->compare('comment', $this->comment, true);
        $criteria->compare('shipmentDeadline', $this->shipmentDeadline, true);
        $criteria->compare('productionDeadline', $this->productionDeadline, true);
        $criteria->compare('idContragent', $this->idContragent);
        $criteria->compare('enumTransportBeforeBorder', $this->enumTransportBeforeBorder);
        $criteria->compare('enumTransportAfterBorder', $this->enumTransportAfterBorder);
        $criteria->compare('idCustom', $this->idCustom);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

}