<?php

/**
 * This is the model class for table "upload".
 *
 * The followings are the available columns in table 'upload':
 * @property integer $idUpload
 * @property integer $idFolder
 * @property string $name
 * @property integer $size
 * @property integer $idProject
 * @property string $comment
 * @property string $date
 * @property integer $idUser
 *
 * The followings are the available model relations:
 * @property User $user
 * @property Project $project
 * @property Folder $folder
 */
class Upload extends CActiveRecord
{

    public static function getArchivePath($t)
    {
        $root = Yii::app()->getBasePath() . '/async/processed/';
        return realpath($root.$t);
    }


    public function beforeDelete() {
        try {
            if (file_exists($this->getFilePath()))
            { unlink($this->getFilePath()); }
        }
        catch (Exception $e)
        {
            return false;
        }

        return parent::beforeDelete();
    }

    const UPLOADS_DIRECTORY = "projectUploads/";
    const MAX_SIZE=10000000;

    /**
     * @param Project $project
     * @param Folder $folder
     * @param string $file
     */
    public static function useUploadedFileAndSave($project, $folder, $file, $originalFilename)
    {
        $root = Yii::app()->getBasePath() . '/../';


        self::_prepareFolderStructure($project, $folder);

        $id = Yii::app()->db->createCommand('select MAX(idUpload) as id from upload')->queryScalar();
        $id++;

        $filename = md5(basename($file). "//". $id);

        $dest=self::UPLOADS_DIRECTORY . $project->idProject . "/" . $folder->idFolder . "/" . $filename;
        copy($file,$dest);

        $model = new Upload();
        $model->idProject = $project->idProject;
        $model->idFolder = $folder->idFolder;
        $model->name = $originalFilename;
        $model->token = $filename;
        $model->size = filesize($file);
        $model->date = Yii::app()->dateFormatter->format("yyyy-M-d", time());
        $model->idUser = Yii::app()->user->id;
        $model->save();
        return $model;
    }


    /**
     * @param Project $project
     * @param Folder $folder
     * @param CUploadedFile $file
     */
    public static function useFileAndSave($project, $folder, $file)
    {
        $root = Yii::app()->getBasePath() . '/../';

        if ($file->size > self::MAX_SIZE) {
            return false;
        }

        self::_prepareFolderStructure($project, $folder);

        $id = Yii::app()->db->createCommand('select MAX(idUpload) as id from upload')->queryScalar();
        $id++;

        $filename = md5($file->name . "//" . $id);
        $file->saveAs($root . self::UPLOADS_DIRECTORY . $project->idProject . "/" . $folder->idFolder . "/" . $filename);
        $model = new Upload();
        $model->idProject = $project->idProject;
        $model->idFolder = $folder->idFolder;
        $model->name = $file->name;
        $model->token = $filename;
        $model->size = $file->size;
        $model->date = Yii::app()->dateFormatter->format("yyyy-M-d", time());
        $model->idUser = Yii::app()->user->id;
        $model->save();
        return $model;
    }

    public static function getAllowedMimes()
    {
        //'jpg, jpeg, gif, png, txt, doc, docx, pdf, xls, xlsx, zip, rar, ppt, pptx',
        return
            array(
                'image/png', 'image/jpg', 'image/jpeg', 'image/gif','application/pdf','text/plain','application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'text/html','application/msword'
            );
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Upload the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @param $project
     * @param $folder
     * @param $root
     */
    public static function _prepareFolderStructure($project, $folder)
    {
        $root = Yii::app()->getBasePath() . '/../';

        if (!is_dir($root . self::UPLOADS_DIRECTORY)) {
            mkdir($root . self::UPLOADS_DIRECTORY);
        }

        if (!is_dir($root . self::UPLOADS_DIRECTORY . $project->idProject)) {
            mkdir($root . self::UPLOADS_DIRECTORY . $project->idProject);
        }
        if (!is_dir($root . self::UPLOADS_DIRECTORY . $project->idProject . "/" . $folder->idFolder)) {
            mkdir($root . self::UPLOADS_DIRECTORY . $project->idProject . "/" . $folder->idFolder);
        }
    }

    public function getFilePath()
    {
        $root = Yii::app()->getBasePath() . '/../';
        return $root . self::UPLOADS_DIRECTORY . $this->idProject . "/" . $this->idFolder . "/" . $this->token;
    }

    public function init()
    {
        $this->idUser = Yii::app()->user->id;
        $this->date = Yii::app()->dateFormatter->format("yyyy-M-d", time());
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'upload';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('idFolder, name, idProject, idUser, token', 'required'),
            array('idFolder, size, idProject, idUser', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
            array('comment, date', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idUpload, idFolder, name, size, idProject, comment, date, idUser', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'idUser'),
            'project' => array(self::BELONGS_TO, 'Project', 'idProject'),
            'folder' => array(self::BELONGS_TO, 'Folder', 'idFolder'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'idUpload' => 'Id Upload',
            'idFolder' => 'Id Folder',
            'name' => 'Name',
            'size' => 'Size',
            'idProject' => 'Id Project',
            'comment' => 'Comment',
            'date' => 'Date',
            'idUser' => 'Id User',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idUpload', $this->idUpload);
        $criteria->compare('idFolder', $this->idFolder);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('size', $this->size);
        $criteria->compare('idProject', $this->idProject);
        $criteria->compare('comment', $this->comment, true);
        $criteria->compare('date', $this->date, true);
        $criteria->compare('idUser', $this->idUser);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getDownloadUrl()
    {
    /** upload/download?p=<?= $idProject ?>&f={{currentDirId}}&h={{f.uid}}&i={{f.id}}
       **/
        return Yii::app()->createUrl("upload/download",array('f'=>$this->idFolder,'h'=>substr($this->token, 0, 10), 'i'=>$this->idUpload,'p'=>$this->idProject));
    }
}