<?php

/**
 * This is the model class for table "notice".
 *
 * The followings are the available columns in table 'notice':
 * @property integer $idNotice
 * @property string $text
 * @property integer $idFeatureGroup
 * @property integer $mapId
 *
 * The followings are the available model relations:
 * @property FeatureGroup $featureGroup
 * @property Link[] $links
 */
class Notice extends ActiveRecord {

    public $save_features;
    public $update_features=false;
    
    public function getName() {
        return $this->text;
    }
            
    public function getFeatures() {
        if (!$this->isNewRecord) {
            $raw = Yii::app()->db->createCommand('select idFeature, idManagedBlock from `notice2link` where idNotice=' . $this->idNotice . ' order by `idManagedBlock`')->queryAll();
            $out = array();
            foreach ($raw as $entry) {
                $out[$entry['idManagedBlock']][] = $entry['idFeature'];
            }
            return $out;
        } else {
            return null;
        }
    }

    public function getFeaturesWithData() {
        if (!$this->isNewRecord) {
            $raw = Yii::app()->db->createCommand('select o.idFeature, o.idManagedBlock as idManagedBlock, f.name as name, f.sid as sid, f.description as description from `notice2link` as o join `feature` as f on o.idFeature=f.idFeature join `managedblock` as m on m.idManagedBlock=o.idManagedBlock where o.idNotice=' . $this->idNotice . ' order by length(m.`comment`)')->queryAll();
            $out = array();
            foreach ($raw as $entry) {
                $out[$entry['idManagedBlock']][] = $entry;
            }
            return $out;
        } else {
            return null;
        }
    }

    public function afterSave() {
        parent::afterSave();

        if ($this->update_features) {
            if (!$this->isNewRecord) {
                Yii::app()->db->createCommand('delete from `notice2link` where idNotice=' . $this->idNotice)->execute();
            }

            if ($this->save_features) {

                foreach ($this->save_features as $idManagedBlock => $keys) {


                    // add realtions
                    foreach ($keys as $key) {
                        Yii::app()->db->createCommand(
                                        'insert into `notice2link` (idNotice, idManagedBlock, idFeature) values (' . $this->idNotice . ', :imb,:if)')
                                ->bindParam('imb', $idManagedBlock)
                                ->bindParam('if', $key)
                                ->execute();
                    }
                }
            }
        }
        return true;
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Notice the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'notice';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('text, idFeatureGroup,mapId', 'required'),
            array('idFeatureGroup,mapId', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idNotice, text, idFeatureGroup', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'featureGroup' => array(self::BELONGS_TO, 'FeatureGroup', 'idFeatureGroup'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'idNotice' => 'Id Notice',
            'text' => 'Подпись',
            'mapId' => 'Номер в кружочке для примечания. ',
            'idFeatureGroup' => 'Id Feature Group',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idNotice', $this->idNotice);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('idFeatureGroup', $this->idFeatureGroup);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function behaviors() {
        return array(
            'CAdvancedArBehavior' => array(
                'class' => 'ext.Many2Many.CAdvancedArBehavior')
        );
    }

}