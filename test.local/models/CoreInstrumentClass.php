<?php

/**
 * This is the model class for table "CoreInstrumentClass".
 *
 * The followings are the available columns in table 'coreinstrumentclass':
 * @property integer $idCoreInstrumentClass
 * @property string $name
 *
 * The followings are the available model relations:
 * @property Coreinstrument[] $coreinstruments
 */
class CoreInstrumentClass extends ActiveRecord {
    function behaviors()
    {
        return array(
            'loggable' => array(
                'class' => 'ActiveRecordLoggable',
                'logCreation' => true,
                'shouldBubbleToMaster' => !true
            )
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CoreInstrumentClass the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'coreinstrumentclass';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name,abbr', 'required'),
            array('abbr', 'length', 'max' => 10),
            array('abbr', 'unique', 'allowEmpty' => false),
            array('name', 'length', 'max' => 90),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idCoreInstrumentClass, name', 'safe', 'on' => 'search'),
        );
    }
    public function beforeDelete() {

        if (Yii::app()->user->isGuest) {
            return false;
        }
        
        if (count($this->projects)) 
        {
            $this->addError("", "Нельзя удалить класс оборудования, так как он участвует в проектах!");
            return false;
        }
        
        if (count($this->orders)) 
        {
            $this->addError("", "Нельзя удалить класс оборудования, так как он участвует в КП!");
            return false;
        }

        if (!Group::getPermissionsUponZone(Group::ZONE_COREINTRUMENT_CLASS,Group::ACTION_DELETE,$this)) {
            return !true;
        }
        return parent::beforeDelete();
    }
    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'coreInstruments' => array(self::HAS_MANY, 'CoreInstrument', 'idCoreInstrumentClass'),
            'manufacturers' => array(self::MANY_MANY, 'Manufacturer', 'manufacture2coreinstrumentclass(idCoreInstrumentClass, idManufacture)'),
            'orders' => array(self::HAS_MANY, 'Order', 'idCoreInstrumentClass'),
            'projects' => array(self::HAS_MANY, 'Project', 'idCoreInstrumentClass'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'abbr'=>'Аббревиатура',
            'idCoreInstrumentClass' => 'Id Core Instrument Class',
            'name' => 'Название',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idCoreInstrumentClass', $this->idCoreInstrumentClass);
        $criteria->compare('name', $this->name, true);
         $criteria->compare('abbr', $this->abbr, true);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}