<?php

/**
 * This is the model class for table "orderfeature".
 *
 * The followings are the available columns in table 'orderfeature':
 * @property integer $idOrder
 * @property integer $idFeature
 * @property integer $idManagedBlock
 * @property integer $idLink
 *
 * The followings are the available model relations:
 * @property Notice[] $notices
 * @property Feature $idFeature0
 * @property Order $idOrder0
 * @property Managedblock $idManagedBlock0
 */
class orderfeature extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Link the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orderfeature';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idOrder, idFeature, idManagedBlock', 'required'),
			array('idOrder, idFeature, idManagedBlock', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idOrder, idFeature, idManagedBlock, idLink', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'notices' => array(self::MANY_MANY, 'Notice', 'notice2link(idLink, idNotice)'),
			'idFeature0' => array(self::BELONGS_TO, 'Feature', 'idFeature'),
			'idOrder0' => array(self::BELONGS_TO, 'Order', 'idOrder'),
			'idManagedBlock0' => array(self::BELONGS_TO, 'Managedblock', 'idManagedBlock'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idOrder' => 'Id Order',
			'idFeature' => 'Id Feature',
			'idManagedBlock' => 'Id Managed Block',
			'idLink' => 'Id Link',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idOrder',$this->idOrder);
		$criteria->compare('idFeature',$this->idFeature);
		$criteria->compare('idManagedBlock',$this->idManagedBlock);
		$criteria->compare('idLink',$this->idLink);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}