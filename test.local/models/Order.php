<?php

/**
 * This is the model class for table "order".
 *
 * The followings are the available columns in table 'order':
 * @property integer $idOrder
 * @property integer $enumCalculationAlgorithm
 * @property integer $idCoreInstrument
 * @property integer $idProject
 * @property string $comment
 * @property string $date
 * @property string $name
 * @property integer $enumOrderType
 * @property integer $enumPricePolicy
 * @property integer $idUser
 * @property integer $idOrderInProject
 * @property integer $idCustomer
 * @property integer $showCIP
 * @property integer $showDDP
 * @property integer $showEXW
 * @property boolean $isFeaturePriceShown
 * @property boolean $isRussianDescriptionLoaded
 * @property boolean $isDeliverySpreadBetweenFeatures
 * @property string $destinationCity
 * @property string $destinationCityGenetivus
 * @property integer $idCompany
 * @property double $cipPayment
 * @property double $ddpCoeff
 * @property double $saleCoeff
 * @property double $overridenPrice
 * @property integer $guaranteeInMonth
 * @property integer $guaranteeLessThan
 * @property string $deliver
 * @property integer $enumPaymentCondition
 * @property string $lastDate
 * @property double $eurRub
 * @property double $usdRub
 * @property double $gbpRub
 * @property double $yenRub
 * @property integer $cipCurrency
 * @property integer $doThreeMonth
 * @property integer $outCurrency
 * @property Int[][] $save_features
 *
 * The followings are the available model relations:
 * @property Project $project
 * @property User $user
 * @property Coreinstrument $coreInstrument
 * @property Feature[] $features
 * @property Attachment[] $attachments
 */
class Order extends ActiveRecord
{

    const PRICEPOLICY_NOSALE = 0;
    const PRICEPOLICY_SALE = 1;
    const PRICEPOLICY_OVERRIDENPRICE = 2;
    const PAYMENTCONDITION_FIRST = 0;
    const PAYMENTCONDITION_BEFOREUNLOAD = 1;
    const PAYMENTCONDITION_AFTERSETUP = 2;
    const PAYMENTCONDITION_FIFTYFIFTY = 3;
    const PAYMENTCONDITION_THIRYEIGHTY = 4;
    const PAYMENTCONDITION_CUSTOM = 5;
    const CURRENCY_RUB = 0;
    const CURRENCY_EUR = 1;
    const CURRENCY_USD = 2;
    const CURRENCY_GBP = 3;
    const CURRENCY_YEN = 4;
    const CURRENCY_EMPTY = 10;
    const TYPE_ROUBLE = 0;
    const TYPE_EURO = 1;
    const TYPE_SCIENCE = 2;

    const ALGORITHM_EXW = 0;
    const ALGORITHM_CIP = 1;
    const ALGORITHM_DDP = 2;

    public $update_features = false;
    public $save_features;
    public $save_ungrouped_features;

    public static function resolveCurrencyToText($currency)
    {
        $a=self::getCurrencies();
        return $a[$currency];
    }

    public function getCalculationAlgorithmText() {
        $a=self::getCalculationAlgorithms();
        return $a[$this->enumCalculationAlgorithm];
    }
    public static function getCalculationAlgorithms()
    {
        return array(
            self::ALGORITHM_EXW  => 'EXW',
            self::ALGORITHM_CIP => 'CIP',
            self::ALGORITHM_DDP => 'DDP',
        );
    }

    public  function  scopes() {
        return array(
          'dateAndName'=>array(
              'select'=>'date, '
          )
        );
    }
    public static function getPricePolicies()
    {
        return array(
            self::PRICEPOLICY_NOSALE => 'Без скидки',
            self::PRICEPOLICY_SALE => 'Скидка (%)',
            self::PRICEPOLICY_OVERRIDENPRICE => 'Скидка до стоимости',
        );
    }

    public static function getPaymentConditionList()
    {
        return array(
            self::PAYMENTCONDITION_FIRST => 'Предоплата',
            self::PAYMENTCONDITION_AFTERSETUP => 'После установки',
            self::PAYMENTCONDITION_BEFOREUNLOAD => 'Перед отгрузкой',
            self::PAYMENTCONDITION_FIFTYFIFTY => '50%/50%',
            self::PAYMENTCONDITION_THIRYEIGHTY => '30%/70%',
            self::PAYMENTCONDITION_CUSTOM => 'По договоренности',
        );
    }

    public static function getCurrencies()
    {
        return array(
            self::CURRENCY_RUB => 'RUB',
            self::CURRENCY_EUR => 'EUR',
            self::CURRENCY_USD => 'USD',
            self::CURRENCY_GBP => 'GBP',
            self::CURRENCY_YEN => 'YEN',
        );
    }

    public static function getRusCurrenciesLong()
    {
        return array(
            self::CURRENCY_RUB => 'рубль',
            self::CURRENCY_EUR => 'евро',
            self::CURRENCY_USD => 'доллар США',
            self::CURRENCY_GBP => 'британский фунт',
            self::CURRENCY_YEN => 'йена',
        );
    }

    public static function getRusCurrencies()
    {
        return array(
            self::CURRENCY_RUB => 'руб.',
            self::CURRENCY_EUR => 'евр.',
            self::CURRENCY_USD => 'долл.',
            self::CURRENCY_GBP => 'фунт.',
            self::CURRENCY_YEN => 'йен.',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Order the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeValidate()
    {
        //check permissions

        if (!$this->project->isSubzoneWritable(Group::SUBZONE_PROJECT_ORDER)) {
            $this->addError('', 'У вас нет необходимых прав для совершения данного действия');
            return false;
        }

        if ($this->idCoreInstrument == 0) {
            $this->idCoreInstrument = null;
        }

        if ($this->idCustomer == 0) {
            $this->idCustomer  = null;
        }

        return parent::beforeValidate();
    }

    public function init()
    {
        $this->idUser = Yii::app()->user->id;
        $this->date = Yii::app()->dateFormatter->format("yyyy-M-d", time());
        $this->ddpCoeff=1.3;
    }

    public function getOrderType()
    {
        $o = self::getEnumOrderTypeList();
        return $o[$this->enumOrderType];
    }

    public static function getEnumOrderTypeList()
    {
        return array(self::TYPE_ROUBLE => 'Рублевый',
            self::TYPE_EURO => 'Валют',
            self::TYPE_SCIENCE => 'Science and Research',
        );
    }

    public function getName()
    {
        $name = '';
        if (!$this->project) return 'ORPHANED-KP';
        $name .= "TI" . str_pad(1000 + $this->idProject, 4, "0", STR_PAD_LEFT);

        if ($this->idOrderInProject) {
            $name .= '-Q' . str_pad($this->idOrderInProject, 2, "0", STR_PAD_LEFT);
        } else {
            $name .= '-QF' . $this->idOrder;
        }
        if ($this->user) {
            $name .= $this->user->abbr . '-';
        } else {
            $name .= '*** -';
        }
        $name .= $this->coreInstrument?$this->coreInstrument->abbr:"Accessoires" ;
        $name .= '-' . $this->project->companyConsumer->abbr;

        return $name;
    }

    /**
     * returns array of both grouped and ungrouped features together
     * @return array|CActiveRecord[]|null
     */
    public function getDistinctFeatures()
    {
        $distinct = array();
        $fs = $this->getSetOfFeatures();
        $fs = array_merge($fs, $this->getUngroupedFeatures());

        foreach ($fs as $feature) {
            if (key_exists($feature->idFeature, $distinct)) {
                $distinct[$feature->idFeature]->amount += $feature->amount;
            } else {
                $distinct[$feature->idFeature] = $feature;
            }
        }

        return $distinct;
    }

    /**
     * Returns array of Features from CoreInstument
     * @return array|CActiveRecord[]|null
     */
    public function getSetOfFeatures()
    {
        if (!$this->isNewRecord) {
            $out = Feature::model()->
                findAllBySql('select *, (m.amount) as amount FROM feature as t JOIN `orderfeature` as m ON m.idFeature=t.idFeature WHERE m.idOrder=' . $this->idOrder . ' order by `idManagedBlock`');

            return $out;

            $raw = Yii::app()->db->createCommand('select idFeature, idManagedBlock, amount from `orderfeature` where idOrder=' . $this->idOrder . ' order by `idManagedBlock`')->queryAll();

            $out = array();
            foreach ($raw as $entry) {
                if (array_key_exists($entry['idFeature'], $out)) {
                    $out[$entry['idFeature']]->count = $out[$entry['idFeature']]->count + $entry['amount'];
                } else {
                    $f = Feature::model()->findByPk($entry['idFeature']);
                    $f->count = $entry['amount'];
                    $out[$entry['idFeature']] = $f;
                }
            }
            return $out;
        } else {
            return null;
        }
    }

    /**
     * Return Features that won't belong to CoreInstrument
     * @return array|CActiveRecord[]
     */

    public function getUngroupedFeatures()
    {
        if (!$this->isNewRecord) {

            $out = Feature::model()->
                findAllBySql('select *, (m.amount) as amount FROM feature as t JOIN orderungroupedfeature as m ON m.idFeature=t.idFeature WHERE m.idOrder=' . $this->idOrder . ' ORDER BY m.`order` ASC');
            return $out;
        } else {
            return array();
        }
    }

    /**
     * @todo refactor: find out, what is to for. And remove it.
     * ????? I DUNNO ????
     * @return array|null
     */
    public function getFeatures()
    {
        if (!$this->isNewRecord) {

            $raw = Yii::app()->db->createCommand('select idFeature, idManagedBlock, amount from `orderfeature` where idOrder=' . $this->idOrder . ' order by `idManagedBlock`')->queryAll();

            $out = array();
            foreach ($raw as $entry) {
                $out[$entry['idManagedBlock']][$entry['idFeature']] = $entry['amount'];
            }
            return $out;
        } else {
            return null;
        }
    }

    public function getFeaturesWithData()
    {
        if (!$this->isNewRecord) {
            $raw = Yii::app()->db->createCommand('select o.idFeature, o.idManagedBlock as idManagedBlock, f.name as name, f.sid as sid, f.description as description from `orderfeature` as o join `feature` as f on o.idFeature=f.idFeature join `managedblock` as m on m.idManagedBlock=o.idManagedBlock where o.idOrder=' . $this->idOrder . ' order by length(m.`comment`)')->queryAll();
            $out = array();
            foreach ($raw as $entry) {
                $out[$entry['idManagedBlock']][] = $entry;
            }
            return $out;
        } else {
            return null;
        }
    }

    public function saveFeatures($features, $amount)
    {

        Yii::app()->db->createCommand('delete from `orderfeature` where idOrder=' . $this->idOrder)->execute();


        foreach ($features as $idManagedBlock => $keys) {
            // add realtions
            foreach ($keys as $key) {

                $a = 1;
                if (key_exists($key, $amount)) {
                    $a = $amount[$key];
                }

                Yii::app()->db->createCommand(
                    'insert into `orderfeature` (idOrder, idManagedBlock, idFeature, amount) values (' . $this->idOrder . ', :imb,:if, :am)')
                    ->bindParam('imb', $idManagedBlock)
                    ->bindParam('if', $key)
                    ->bindParam('am', $a)
                    ->execute();
            }
        }
    }

    public function saveUngroupedFeatures($features, $amount)
    {
        Yii::app()->db->createCommand('delete from `orderungroupedfeature` where idOrder=' . $this->idOrder)->execute();

        $i = 0;

        foreach ($features as $key) {

            $a = 1;

            if (key_exists($key, $amount)) {
                $a = $amount[$key];
            }

            $i++;
            Yii::app()->db->createCommand(
                'insert into `orderungroupedfeature` (`order`, idOrder, idFeature, amount) values (' . $i . ', ' . $this->idOrder . ', :if, :am)')
                ->bindParam('if', $key)
                ->bindParam('am', $a)
                ->execute();
        }
    }

    public  function  afterDelete() {
        if ($this->project) {
            $this->project->recacheOrderCount();
        }

        parent::afterDelete();
    }
    public function afterSave()
    {
        if ($this->project) {
            $this->project->recacheOrderCount();
        }

        parent::afterSave();


        return true;
    }

    public function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->setKpCounter(false);
        }
        parent::beforeSave();
        return true;
    }

    public function setKpCounter($doSave)
    {
        if (($this->project) && (!($this->idOrderInProject))) {
            $this->idOrderInProject = $this->project->kpCounter + 1;
            $this->project->saveCounters(array('kpCounter' => 1));
            if ($doSave) {
                $this->save();

            }
        }
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'order';
    }

    public function beforeDelete()
    {

        if (Yii::app()->user->isGuest) {
            return false;
        }
        if (Yii::app()->user->isAdmin()) {
            return true;
        }

        if (!$this->project->isSubzoneWritable(Group::SUBZONE_PROJECT_ORDER)) {
            $this->addError('', 'У вас нет необходимых прав для совершения данного действия');
            return false;
        }


        return parent::beforeDelete();
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('idProject, date, idUser', 'required'),
            array('destinationCityGenetivus, destinationCity, isFeaturePriceShown, idCustomer, comment, deliver, lastDate, idCoreInstrumentClass,idCoreInstrumentManufacturer, enumOrderType , entryId, date, price', 'safe'),
            array('idCoreInstrument, guaranteeLessThan, idCoreInstrument, enumOrderType, idProject, idUser, showCIP, showDDP, showEXW, idCompany, guaranteeInMonth, enumPaymentCondition, cipCurrency, doThreeMonth, outCurrency', 'numerical', 'integerOnly' => true),
            array('isDeliverySpreadBetweenFeatures, isRussianDescriptionLoaded, enumCalculationAlgorithm, guaranteeLessThan, cipPayment, ddpCoeff, saleCoeff, enumPricePolicy, overridenPrice, eurRub, usdRub, gbpRub, yenRub', 'numerical'),
            // array('lastDate', 'date', 'allowEmpty'=>true,'format'=>'yyyy-M-d'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idOrder, idCoreInstrument, enumOrderType, comment, date, idCustomer', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'attachments' => array(self::MANY_MANY, 'Attachment', 'attachment2order(idOrder, idAttachment)'),
            'user' => array(self::BELONGS_TO, 'User', 'idUser'),
            'coreInstrument' => array(self::BELONGS_TO, 'CoreInstrument', 'idCoreInstrument'),
            'attachments' => array(self::MANY_MANY, 'Attachment', 'attachment2order(idOrder, idAttachment)'),
            'project' => array(self::BELONGS_TO, 'Project', 'idProject'),
            'coreInstrumentClass' => array(self::BELONGS_TO, 'CoreInstrumentClass', 'idCoreInstrumentClass'),
            'manufacturer' => array(self::BELONGS_TO, 'Manufacturer', 'idManufacturer'),
            'customer' => array(self::BELONGS_TO, 'Customer', 'idCustomer'),
            //   'features' => array(self::MANY_MANY, 'Feature', 'orderfeature(idOrder, idFeature)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'isRussianDescriptionLoaded'=>'Русские описания и названия',
            'isDeliverySpreadBetweenFeatures'=>'"Включать" доставку в стоимость позиций',
            'isFeaturePriceShown' => 'Показывать разрядку по ценам',
            'enumPricePolicy' => 'Тип скидки',
            'manufacturer' => '',
            'coreInstrumentClass' => '',
            'user' => 'КП составил',
            'project' => 'КП к проекту',
            'idCoreInstrumentClass' => 'Класс Оборудования',
            'idManufacturer' => 'Производитель',
            'enumOrderType' => 'Тип P.O.',
            'idOrder' => 'Id Order',
            'idCustomer' => 'На кого оформляем КП (введите первые буквы фамилии)',
            'coreInstrument' => 'Заказ',
            'idCoreInstrument' => 'Базовый прибор',
            'comment' => 'Комментарий',
            'date' => 'Дата',
            'customer' => 'Клиент',
            'idUser' => 'Коммерческое предложение составил',
            'name' => 'Код',
            'showCIP' => 'Отображать CIP',
            'showDDP' => 'Отображать DDP',
            'showEXW' => 'Отображать EXW',

            'enumCalculationAlgorithm' => 'Тип КП',

            'city' => 'Город доставки CIP',
            'destinationCityGenetivus' => 'Город доставки, род. падеж (Доставка до Москвы)',
            'destinationCity' => 'Город доставки',

            'idCompany' => 'Компания',
            'cipPayment' => 'Стоимость доставки и страховки',
            'ddpCoeff' => 'Коэффициент DDP',
            'saleCoeff' => '% Скидки',
            'overridenPrice' => 'Ручное задание итоговой стоимости',
            'deliver' => 'Срок поставки, месяцев',
            'enumPaymentCondition' => 'Условия оплаты',
            'lastDate' => 'КП действительно до',
            'eurRub' => 'Ручной курс EUR-Руб',
            'usdRub' => 'Ручной курс USD-Руб',
            'gbpRub' => 'Ручной курс GBP-Руб',
            'yenRub' => 'Ручной курс YEN-Руб',
            'cipCurrency' => 'Валюта',
            'doThreeMonth' => 'КП действительно 3 месяца',
            'outCurrency' => 'Итоговая валюта',
            'guaranteeInMonth' => 'Гарантия, месяцев',
            'guaranteeLessThan' => 'Но не более, месяцев',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idOrder', $this->idOrder);
        $criteria->compare('idCoreInstrument', $this->idCoreInstrument);
        $criteria->compare('comment', $this->comment, true);
        $criteria->compare('date', $this->date, true);
        $criteria->compare('enumOrderType', $this->enumOrderType);
        $criteria->compare('idProject', $this->idProject);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    function behaviors()
    {
        return array(
            'CAdvancedArBehavior' => array(
                'class' => 'ext.Many2Many.CAdvancedArBehavior'),
            'loggable' => array(
                'class' => 'ActiveRecordLoggable',
                'logCreation' => true,
                'shouldBubbleToMaster' => true,
                'hostAfterSaveAction' => 'updateLastChange',
            ));
    }

    private function normalizePrice($val, $from, $to, $curr)
    {
        return $val * $curr[$from] / $curr[$to];
    }

    public function getOutputPrice() {
        list($sum, $exw, $cip, $ddp, $discount)=  $this->calculateOrder();
        $a=self::getRusCurrencies();
        return Helpers::formatPrice($sum).' '.$a[$this->outCurrency];
    }
    /**
     * @return array array($sum, $exw, $cip, $ddp, $discount)
     */
    public  function calculateOrder() {

        $conversion = $this->getMoneyConversionArray();
        $distinct = $this->getDistinctFeatures();

        $exw = 0;

        $coreInstrument = $this->coreInstrument;

        if ($coreInstrument) {
            if ($coreInstrument->price) {
                $exw += $this->normalizePrice($coreInstrument->price, $coreInstrument->enumCurrency, $this->outCurrency, $conversion);
            }
        }

        foreach ($distinct as $feature) {
            if ($feature->price) {
                $exw += $this->normalizePrice($feature->price, $feature->enumCurrency, $this->outCurrency, $conversion) * $feature->amount;
            }
        }

        $sum = $exw;
        $cip = $sum;
        $ddp = $sum;
        $discount = 0;

        if ($this->enumCalculationAlgorithm>=self::ALGORITHM_CIP) {
            if ($this->cipPayment) {
                $cip += $this->normalizePrice($this->cipPayment, $this->cipCurrency, $this->outCurrency, $conversion);
                $ddp = $cip;
                $sum = $cip;
            }
        }

        if ($this->enumCalculationAlgorithm>=self::ALGORITHM_DDP) {
            if ($this->ddpCoeff) {
            $ddp = $cip * $this->ddpCoeff;
            $sum = $ddp;
            }
        }

        if ($this->enumPricePolicy == Order::PRICEPOLICY_SALE) {
            if ($this->saleCoeff > 0) {
                $discount = $sum * $this->saleCoeff / 100;
                $sum -= $discount;
            }
        } elseif ($this->enumPricePolicy == Order::PRICEPOLICY_OVERRIDENPRICE) {
            $discount = $this->overridenPrice-$sum;
            $sum = $this->overridenPrice;
        }

        return array($sum, $exw, $cip, $ddp, $discount);
    }

    /**
     * @return mixed
     */
    public function getMoneyConversionArray()
    {
        $conversion=array();
        $conversion[Order::CURRENCY_RUB] = 1;
        $conversion[Order::CURRENCY_EUR] = $this->eurRub;
        $conversion[Order::CURRENCY_USD] = $this->usdRub;
        $conversion[Order::CURRENCY_YEN] = $this->yenRub;
        $conversion[Order::CURRENCY_GBP] = $this->gbpRub;
        return $conversion;
    }
}