<?php

/**
 * This is the model class for table "checkpointGroup".
 *
 * The followings are the available columns in table 'checkpointGroup':
 * @property integer $idCheckpointGroup
 * @property string $name

 *
 * The followings are the available model relations:
 * @property Checkpoint[] $checkpoints
 */
class CheckpointGroup extends ActiveRecord {
    function behaviors()
    {
        return array(
            'loggable' => array(
                'class' => 'ActiveRecordLoggable',
                'logCreation' => true,
                'shouldBubbleToMaster' => !true
            )
        );
    }

    /**
     * Returns models that applyes to enumProjectClasses
     * 

     * @param type $includeEmpty
     * @param type $getRaw
     * @return array
     */
    public static function getGroups($enumProjectClass, $includeEmpty = false, $getRaw = false) {
        $m = CheckpointGroup::model()->findAll();
        if ($getRaw) {
            return $m;
        }
        $out = array();
        $out = CHtml::listData($m, "idCheckpointGroup", "name");

        if ($includeEmpty) {
            $out[0] = "Без группы";
        }


        return $out;
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CheckpointGroup the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'checkpointGroup';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required'),
            array('idCheckpointGroup', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 99),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idCheckpointGroup, name', 'safe', 'on' => 'search'),
        );
    }

    public function beforeDelete() {

        if (Yii::app()->user->isGuest) {
            return false;
        }


        if (count($this->checkpoints)>0)
        {
             $this->addError("", "Данная группа содержит контрольные точки. Необходимо их удалить для начала.");
            return !true;
        }
        return parent::beforeDelete();
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'checkpoints' => array(self::HAS_MANY, 'Checkpoint', 'idCheckpointGroup', 'order' => '`order`'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'idCheckpointGroup' => 'Id Checkpoint Group',
            'name' => 'Name',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idCheckpointGroup', $this->idCheckpointGroup);
        $criteria->compare('name', $this->name, true);


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}