<?php

/**
 * This is the model class for table "managedblock".
 *
 * The followings are the available columns in table 'managedblock':
 * @property integer $idManagedBlock
 * @property string $comment
 * @property string $notice
 * @property integer $enumRule
 * @property Array[] $save
 *
 * The followings are the available model relations:
 * @property Feature[] $features
 * @property FeatureGroup $featureGroup
 * @property CoreInstrument $coreInstrument
 */
class ManagedBlock extends ActiveRecord
{
    const ANY_COMBINATION = 0;
    const SELECT_ONE_OR_MORE = 1;
    const SELECT_ONE = 2;
    public $save_features;

    public static function getEnumRules()
    {
        return array(
            self::ANY_COMBINATION => 'Сколько угодно из списка',
            self::SELECT_ONE => 'Только один из списка',
            self::SELECT_ONE_OR_MORE => 'Не менее одного из списка',
        );
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getName()
    {
        return $this->comment ? $this->comment : "Нет названия";
    }

    public function afterSave()
    {
        parent::afterSave();
        if ($this->save_features) {

            $ready = Yii::app()->db->createCommand("SELECT idFeature FROM managedblockfeature WHERE idManagedBlock=" . $this->idManagedBlock)
                ->queryColumn();

            if (is_array($this->save_features) && (array_diff($this->save_features,$ready)||array_diff($ready,$this->save_features))) {

                $joined = Feature::model()->findAllByPk($this->save_features);
                $joined = array_map(function($o) {return $o->readableName;}, $joined );
                $joined = implode(', ',$joined);
////  public function logEvent($description, $action, $model, $field, $userId = -1, $slaveModel = null, $newValue = null, $invokeAfterSave = false)

                $this->logEvent(Yii::app()->user->name
                . ' изменил опции в ' . get_class($this)
                . '[' . $this->readableName . '].', ActiveRecordLoggable::ACTION_CHANGE, $this, 'features', -1, $this, $joined, true);

                if (!$this->isNewRecord) {
                    // clean relations
                    Yii::app()->db->createCommand('delete from `managedblockfeature` where idManagedBlock=' . $this->idManagedBlock)->execute();
                }
                // add realtions
                $i = 0;
                foreach ($this->save_features as $key) {
                    $i++;
                    Yii::app()->db->createCommand(
                        'insert into `managedblockfeature` (`order`, idManagedBlock, idFeature) values (' . $i . ', ' . $this->idManagedBlock . ', :if)')

                        ->bindParam('if', $key)
                        ->execute();
                }
            }
        }
        return true;
    }

    public function getFeatures()
    {
        if (!$this->isNewRecord) {
            $out = Feature::model()->
                findAllBySql('select * FROM feature as t JOIN managedblockfeature as m ON m.idFeature=t.idFeature WHERE m.idManagedBlock=' . $this->idManagedBlock . ' ORDER BY m.`order` ASC');
            return $out;
        } else {
            return array();
        }
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ManagedBlock the static model class
     */
    public function getNotices()
    {
        if (!$this->isNewRecord) {
            $raw = Yii::app()->db->createCommand('select l.idFeature as idFeature, l.idNotice as idNotice, n.mapId as mapId, n.text as text from `notice2link` as l join `notice` as n on n.idNotice=l.idNotice where idManagedBlock=' . $this->idManagedBlock . ' order by `idFeature`')->queryAll();
            $out = array();

            foreach ($raw as $entry) {
                $out[$entry['idFeature']][] = $entry;
            }

            return $out;
        } else {
            return array();
        }
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'managedblock';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('idCoreInstrument, enumRule', 'required'),
            array('idManagedblock, enumRule', 'numerical', 'integerOnly' => true),
            array('comment, save_features, notice, idFeatureGroup', 'safe'),
            // array('notice,comment', 'filter', 'filter' => array($obj = new CHtmlPurifier(), 'purify')),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idManagedblock, idCoreInstrument, comment, enumRule', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            //   'features' => array(self::MANY_MANY, 'Feature', 'managedblockfeature(idManagedBlock, idFeature)'),
            //  'managedblockfeatures' => array(self::HAS_MANY, 'ManagedBlockFeature', 'idManagedBlock','order'=>'`idFeature` desc'),
            // 'featuresOrdered' => array(self::HAS_MANY, 'Feature',  array('idFeature'=>'idFeature'),'through'=>'managedblockfeatures'),

            'featureGroup' => array(self::BELONGS_TO, 'FeatureGroup', 'idFeatureGroup'),
            'coreInstrument' => array(self::BELONGS_TO, 'CoreInstrument', 'idCoreInstrument'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(

            'notice' => 'Примечание к блоку',
            'idManagedBlock' => 'Id Managed Block',
            'comment' => 'Название блока позиций',
            'enumRule' => 'Правило для блока позиций',
            'idFeatureGroup' => 'Группа блоков',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->compare('idCoreInstrument', $this->idCoreInstrument);
        $criteria->compare('idManagedBlock', $this->idManagedBlock);
        $criteria->compare('comment', $this->comment, true);
        $criteria->compare('enumRule', $this->enumRule);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    function behaviors()
    {
        return array(
            'CAdvancedArBehavior' => array(
                'class' => 'ext.Many2Many.CAdvancedArBehavior'),
            'loggable' => array(
                'class' => 'ActiveRecordLoggable',
                'logCreation' => true,
                'shouldBubbleToMaster' => true,
                'masterRelation' => 'coreInstrument'
            ));
    }
}