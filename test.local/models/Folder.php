<?php

/**
 * This is the model class for table "folder".
 *
 * The followings are the available columns in table 'folder':
 * @property integer $idFolder
 * @property string $name
 * @property integer $idProject
 * @property integer $parent
 * @property integer $enumSpecialFolder
 * @property integer $link
 *
 * The followings are the available model relations:
 * @property Folder $parentFolder
 * @property Folder[] $folders
 * @property Project $project
 * @property Upload[] $uploads
 */
class Folder extends CActiveRecord
{
    public function beforeDelete() {
        if ($this->uploads) {
            foreach ($this->uploads as $upload) {
                if (!$upload->delete()) {
                    return false;
                }
            }
        }
        return parent::beforeDelete();
    }
    /**
     * User-initiated recursive folder deletion
     * @return bool|void
     */

    public function gracefullyDelete() {

        if ($this->enumSpecialFolder!=self::USER_FOLDER) {
            return false;
        }


        if ($this->folders) {

            foreach($this->folders as $folder) {

                if (!$folder->gracefullyDelete()) {
                     return false;
                 }
            }
        }
        return $this->delete();
        //return false;//$this->delete();
    }

    const SAMPLES_FOLDER = 1;
    const SAMPLE_FOLDER = 2;
    const SYSTEM_FOLDER = 3;
    const USER_FOLDER = 10;

    /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Folder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'folder';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, idProject', 'required'),
			array('idFolder, idProject, parent', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idFolder, name, idProject, parent', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parentFolder' => array(self::BELONGS_TO, 'Folder', 'parent'),
			'folders' => array(self::HAS_MANY, 'Folder', 'parent'),
			'project' => array(self::BELONGS_TO, 'Project', 'idProject'),
			'uploads' => array(self::HAS_MANY, 'Upload', 'idFolder'),
			'uploadsCount' => array(self::STAT, 'Upload', 'idFolder'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idFolder' => 'Id Folder',
			'name' => 'Name',
			'idProject' => 'Id Project',
			'parent' => 'Parent',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idFolder',$this->idFolder);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('idProject',$this->idProject);
		$criteria->compare('parent',$this->parent);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}