<?php

/**
 * This is the model class for table "attachment".
 *
 * The followings are the available columns in table 'attachment':
 * @property integer $idAttachment
 * @property string $filename
 * @property string $title
 * @property string $originalFilename
 *
  todo Не определено свойство "Attachment.original_filename".
 * @property string $original_filename
 *
 *
 * @property Event[] $events
 * @property Logistic[] $logistics
 * @property Order[] $orders
 * @property Project[] $projects
 * @property Sample[] $samples
 */
class Attachment extends ActiveRecord
{

    const folder = '/attachments/';
    const BELONGS_TO_LOGISTICFRAGMENT = 9;
    const BELONGS_TO_CONTRAGENT = 8;
    const BELONGS_TO_CONTEST = 7;
    const BELONGS_TO_COREINSTRUMENT = 6;
    const BELONGS_TO_EVENT = 5;
    const BELONGS_TO_ORDER = 4;
    const BELONGS_TO_LOGISTIC = 3;
    const BELONGS_TO_PROJECT = 2;
    const BELONGS_TO_SAMPLE = 1;
    public $attached_file;
    public $belongType;
    public $belongId;

    public function getDownloadLink() {
        return CHtml::link($this->name,Yii::app()->createUrl("/attachment/download/",array('id'=>$this->getPrimaryKey())));
    }

    public static function bindUploadedFileToModelAttribute($uploadName, $model, $attribute, $pkColumnName = null)
    {
        $aId = self::bindUploadedFileToModel($model, $pkColumnName, $uploadName);
        if ($aId) {
            $model->{$attribute}=$aId;
            return true;
        }
        return false;
    }

    /**
     * Uploads attachment1, attachment2, attachment3 fields and binds them to the model
     * @param CActiveRecord $model
     * @throws CException
     */

    public static function bindAttachmentsToModel($model, $pkColumnName =null)
    {

        for ($i = 1; $i <= 3; $i++) {
            $name='attachment' . $i;
            self::bindUploadedFileToModel($model, $pkColumnName, $name);
        }
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Attachment the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @param CActiveRecord $model
     * @param $pkColumnName
     * @param $name
     * @throws CException
     */
    public static function bindUploadedFileToModel($model, $pkColumnName, $name)
    {
        list($belongType, $linkTableName) = self::resolveTypeFromModelClass($model);

        if (!$pkColumnName) {
            $table = $model->getMetaData()->tableSchema;

            if (!is_string($table->primaryKey))
                throw new CException("Model has multiple primary keys! This is not guessing game, man");
            $pkColumnName = $table->primaryKey;
        }

        $file = CUploadedFile::getInstance($model, $name);
        if ($file) {
            $k = new Attachment();

            $k->attached_file = $file;
            $k->belongId = $model->getPrimaryKey();
            $k->belongType = $belongType;

            if (!$k->save()) {
                //var_dump($k->errors);
                //die();;
                $model->addErrors($k->errors);
                return null;
                //throw new CException("Attachment couldn't be saved!");
            }

            Yii::app()->db->createCommand('insert into ' . $linkTableName . ' (' . $pkColumnName . ', idAttachment) values (' . $model->getPrimaryKey() . ', ' . $k->getPrimaryKey() . ')')
                ->execute();
            return $k->idAttachment;

        }
        return null;
    }

    public function getName()
    {
        return $this->originalFilename . ' ' . ($this->title ? '(' . $this->title . ')' : '');
    }

    /**
     * returns title or filename, if no title
     * @return string
     */
    public function getTitleSafe()
    {
        return $this->title ? $this->title : $this->originalFilename;
    }

    public function getOriginalFilenameWithLink($addTitle = true)
    {
        $text = CHtml::link(
                CHtml::image(Yii::app()->getBaseUrl() . '/images/download.gif', 'Загрузить')
                , Yii::app()->createUrl('Attachment/download', array('id' => $this->idAttachment))
                , array('class' => 'button secondary small'))
            . '&nbsp;' . $this->originalFilename;
        if ($addTitle)
            $text .= '&nbsp;' . $this->title;
        return $text;
    }

    public function beforeValidate()
    {


        if ($this->isNewRecord) {
            $id = Yii::app()->db->createCommand('select MAX(idAttachment) as id from attachment')->queryScalar();

            $this->idAttachment = ++$id;
            $this->filename = md5($this->attached_file->name . '+' . $this->idAttachment);
            $this->originalFilename = $this->attached_file->name;
        }
        return parent::beforeValidate();
    }

    public function afterSave()
    {
        parent::afterSave();

        if ($this->isNewRecord) {
            $this->attached_file->saveAs(
                $this->getAttachmentsFolder()
                . $this->filename);
        }
    }

    public function getAttachmentsFolder()
    {
        return $_SERVER['DOCUMENT_ROOT'] .
        Yii::app()->request->baseUrl . self::folder;
    }
    public function isAlive() {
        return file_exists($this->getAttachmentsFolder()
        . $this->filename);
    }
    public function getPath() {
        return $this->getAttachmentsFolder()
        . $this->filename;
    }
    public function delete()
    {
        if (file_exists($this->getAttachmentsFolder()
        . $this->filename)
        ) {
            unlink($this->getAttachmentsFolder()
            . $this->filename);
        }
        parent::delete();
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'attachment';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('filename, originalFilename', 'required'),
            array('filename, originalFilename', 'length', 'max' => 255),
            array('belongType, belongId, title', 'safe'),
            array('attached_file', 'file', 'types' => 'jpg, jpeg, gif, png, txt, doc, docx, pdf, xls, xlsx, zip, rar, ppt, pptx', 'allowEmpty' => 'false'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idAttachment, filename, original_filename', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'events' => array(self::MANY_MANY, 'Event', 'attachment2event(idAttachment, idEvent)'),
            'logistics' => array(self::MANY_MANY, 'Logistic', 'attachment2logistic(idAttachment, idLogistic)'),
            'orders' => array(self::MANY_MANY, 'Order', 'attachment2order(idAttachment, idOrder)'),
            'projects' => array(self::MANY_MANY, 'Project', 'attachment2project(idAttachment, idProject)'),
            'contests' => array(self::MANY_MANY, 'Contest', 'attachment2contest(idAttachment, idContest)'),
            'samples' => array(self::MANY_MANY, 'Sample', 'attachment2sample(idAttachment, idSample)'),
            'coreinstruments' => array(self::MANY_MANY, 'CoreInstrument', 'attachment2coreinstrument(idAttachment, idCoreInstrument)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'idAttachment' => 'Id Attachment',
            'filename' => 'Filename',
            'title' => 'Описание (необязательно)',
            'original_filename' => 'Original Filename',
            'attached_file' => 'Файл для загрузки (jpg, gif, png, txt, doc, docx, pdf, xls, xlsx, zip, rar, ppt, pptx)'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idAttachment', $this->idAttachment);
        $criteria->compare('filename', $this->filename, true);
        $criteria->compare('original_filename', $this->original_filename, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    private static  function resolveTypeFromModelClass($model)
    {
        switch (strtolower(get_class($model))) {
            case "logisticfragment":
                return array(self::BELONGS_TO_LOGISTICFRAGMENT, "attachement2logisticfragment"); //sic! @todo - rename typo in db
                break;
            case "contragent":
                return array(self::BELONGS_TO_CONTRAGENT, "attachment2contragent");
                break;
            case "contest":
                return array(self::BELONGS_TO_CONTEST, "attachment2contest");
                break;
            case "coreinstrument":
                return array(self::BELONGS_TO_COREINSTRUMENT, "attachment2coreinstrument");
                break;
            case "event":
                return array(self::BELONGS_TO_EVENT, "attachment2event");
                break;
            case "order":
                return array(self::BELONGS_TO_ORDER, "attachment2order");
                break;
            case "project":
                return array(self::BELONGS_TO_PROJECT, "attachment2project");
                break;
            case "sample":
                return array(self::BELONGS_TO_SAMPLE, "attachment2sample");
                break;
            case "logistic":
                break;
            default:
                throw new CException("Model is not associated with any attachment types. Quitting");
        }
    }

}