<?php

/**
 * This is the model class for table "atomicRbac".
 *
 * The followings are the available columns in table 'atomicRbac':
 * todo Удалил потому как нужно перебить как
 * @property integer $idAtomicRbac
 * @property integer $enumRuleClass
 * @property integer $enumRuleValue
 * @property integer $idProject
 * @property integer $idUser
 *
 * The followings are the available model relations:
 * @property Users $user
 * @property Project $project
 */


class AtomicRbac extends CActiveRecord
{
    const RBAC_PROJECT=0;
    const ALLOW = 0;

    private $_idUser=0;

    public function afterSave() {
        if ($this->enumRuleClass==self::RBAC_PROJECT) {
          if ($this->_idUser!=$this->idUser) {
               PersonalMessageService::notifyAboutCoassignedProject($this->project,$this->user);
         }
        }
        parent::afterSave();
    }

    public function afterFind() {
        $this->_idUser=$this->idUser;
        parent::afterFind();
    }
    /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AtomicRbac the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'atomicRbac';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('enumRuleClass, enumRuleValue, idUser', 'required'),
			array('enumRuleClass, enumRuleValue, idProject, idUser', 'numerical', 'integerOnly'=>true),
			array('enumRuleClass, enumRuleValue, idProject', 'unsafe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idAtomicRbac, enumRuleClass, enumRuleValue, idProject, idUser', 'safe', 'on'=>'search'),

            // todo из миграции
            //array('idAtomicRbac', 'primary_key')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'idUser'),
			'project' => array(self::BELONGS_TO, 'Project', 'idProject'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idAtomicRbac' => 'Id Atomic Rbac',
			'enumRuleClass' => 'Enum Rule Class',
			'enumRuleValue' => 'Enum Rule Value',
			'idProject' => 'Id Project',
			'idUser' => 'Пользователь',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idAtomicRbac',$this->idAtomicRbac);
		$criteria->compare('enumRuleClass',$this->enumRuleClass);
		$criteria->compare('enumRuleValue',$this->enumRuleValue);
		$criteria->compare('idProject',$this->idProject);
		$criteria->compare('idUser',$this->idUser);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}