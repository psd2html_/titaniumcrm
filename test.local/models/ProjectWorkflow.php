<?php

/**
 * This is the model class for table "projectWorkflow".
 *
 * The followings are the available columns in table 'projectWorkflow':
 * @property integer $idProjectWorkflow
 * @property string $name
 * @property string $description
 * @property integer $enumState
 *
 * The followings are the available model relations:
 * @property Projectworkflow2projectworkflow[] $projectworkflow2projectworkflows
 * @property Projectworkflow2projectworkflow[] $projectworkflow2projectworkflows1
 */
class ProjectWorkflow extends ActiveRecord
{
    function getCBOX()
    {
         return '<div class="cbox-fixed" style="background-color:'.$this->colour.' "></div>';
    }
    function getNameFull() {
        $a=  Project::getEnumStatusList();
        return $this->name.' ('.$a[$this->enumState].')';
    }
    function getState() {
        $a=  Project::getEnumStatusList();
        return $a[$this->enumState];
    }
    function getColour() {
        $a=  Project::getEnumStatusColours();
        return $a[$this->enumState];
    }

    function behaviors()
    {
        return array(
            'CAdvancedArBehavior' => array(
                'class' => 'ext.Many2Many.CAdvancedArBehavior'),
            'loggable' => array(
                'class' => 'ActiveRecordLoggable',
                'logCreation' => true,
                'shouldBubbleToMaster' => !true

            ));
    }
    
    public function beforeDelete() {

        if (Yii::app()->user->isGuest) {
            return false;
        }
        

        return parent::beforeDelete();
    }
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProjectWorkflow the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'projectWorkflow';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, enumState', 'required'),
			array('enumState', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>90),
			array('description, nextStages', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idProjectWorkflow, name, description, enumState', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'nextStages' => array(self::MANY_MANY, 'ProjectWorkflow', 'projectworkflow2projectworkflow(idFrom, idTo)'),
			//'projectworkflow2projectworkflows1' => array(self::HAS_MANY, 'Projectworkflow2projectworkflow', 'idTo'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idProjectWorkflow' => 'Id Project Workflow',
			'name' => 'Название этапа',
			'description' => 'Описание этапа',
			'enumState' => 'Группа этапов',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idProjectWorkflow',$this->idProjectWorkflow);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('enumState',$this->enumState);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}