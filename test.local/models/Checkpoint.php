<?php

/**
 * This is the model class for table "Checkpoint".
 *
 * The followings are the available columns in table 'Checkpoint':
 * @property integer $idCheckpoint

 * @property string $name
 * @property string $description
 * @property integer $order
 * @property integer $idCheckpointGroup
 * @property integer $readyState
 * @property integer $startWith
 */
class Checkpoint extends ActiveRecord {
    function behaviors()
    {
        return array(
            'loggable' => array(
                'class' => 'ActiveRecordLoggable',
                'logCreation' => true,
                'shouldBubbleToMaster' => !true
            )
        );
    }

    public function getStartWithName() {
        $types = Project::getEnumStatusList();
        //$types[1000] = 'Без проверки';
        return ($types[$this->startWith]);
    }

    public function getReadyStateName() {
        $types = Project::getEnumStatusList();
        $types[1000] = 'Без проверки';
        return ($types[$this->readyState]);
    }
    public function beforeSave() {
        if (($this->idCheckpointGroup==-1))
            $this->idCheckpointGroup=null;
        
        return parent::beforeSave();
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Checkpoint the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'checkpoint';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, startWith', 'required'),
            array('readyState', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
            array('idCheckpointGroup, order, description', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idCheckpoint, name, description, readyState', 'safe', 'on' => 'search'),
        );
    }
    public function beforeDelete() {

        if (Yii::app()->user->isGuest) {
            return false;
        }
          
        // ?????
        return parent::beforeDelete();
    }
    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'checkpointGroup' => array(self::BELONGS_TO, 'CheckpointGroup', 'idCheckpointGroup'),
            'events' => array(self::HAS_MANY, 'Event', 'idCheckpoint'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'idCheckpoint' => 'Id Checkpoint',
            'idCheckpointGroup' => 'Группа контрольных точек',
            'name' => 'Название проверки',
            'description' => 'Комментарий',
            'readyState' => 'Должен быть готов к след. этапу',
            'startWith' => 'Начать с этапа',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idCheckpoint', $this->idCheckpoint);
     //   $criteria->compare('enumProjectClass', $this->enumProjectClass);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('readyState', $this->readyState);
        $criteria->order = 'readyState asc';
        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

}