<?php
/* @todo replace count to amount! */
/**
 * This is the model class for table "feature".
 *
 * The followings are the available columns in table 'feature':
 * @property integer $idFeature
 * @property string $sid
 * @property string $name
 * @property integer $count
 * @property integer $amount
 * @property integer $isPriceSettled
 * @property string $ruName
 * @property string $description
 * @property string $isFreeOfCharge
 * @property string $ruDescription
 *
 * The followings are the available model relations:
 * @property Ownage[] $ownages
 */
class Feature extends ActiveRecord
{
///usr/share/unoconv/unoconv -vvv -i FilterOptions=76 -e FilterOptions=76 -e RestrictPermissions=true -e PermissionPassword=test -e EnableCopyingOfContent=false -f doc index.html
    const descPath = "/../descriptions/";
    const HAS_DESCRIPTION = 2;
    const NO_DESCRIPTION = 1;
    public $amount = 1;
    public $count = 1;
    public $addToPrice = 0;
    public $multiplyPrice = 1;

    public $hasText;
    public $hasPrice;
    public $attached_image;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Feature the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getFullName()
    {
        return $this->sid . ' - ' . $this->name;
    }

    public function getHasPrice()
    { // виртуальный property, важно!

        if (($this->isPriceSettled)) {
            return self::HAS_DESCRIPTION;
        }

        return self::NO_DESCRIPTION;
    }

    public function getHasPriceLiteral()
    {
        if (($this->price))
            return CHtml::image(Yii::app()->getBaseUrl() . "/images/yes_icon.gif");
        return CHtml::image(Yii::app()->getBaseUrl() . "/images/no_icon.png");;
    }

    public function getHasText()
    { // виртуальный property, важно!
        if (($this->description) && (strlen($this->description) > 5)) {
            return self::HAS_DESCRIPTION;
        }

        return self::NO_DESCRIPTION;
    }

    public function getHasTextLiteral()
    {
        if (($this->description))
            return CHtml::image(Yii::app()->getBaseUrl() . "/images/yes_icon.gif");
        return CHtml::image(Yii::app()->getBaseUrl() . "/images/no_icon.png");;
    }

    /**
     *  Get FileName that stored in folder
     * @return string filename
     */
    public function getSanitizedFilename()
    {
        $str = $this->sid;
        $tr = array("/" => "-", '…' => '', "'" => '', '"' => '', '.' => '', '*' => '', '?' => '');
        $str = strtr($str, $tr) . '.txt';

        return $str;
    }

    public function beforeSave()
    {
        if (!strip_tags($this->description)) {
            $this->description = null;
        }
        if (!strip_tags($this->ruDescription)) {
            $this->ruDescription = null;
        }
        return parent::beforeSave();
    }

    /**
     * has Description of any means - in folder or in db
     * @return boolean hasDescription
     */
    public function hasDescription()
    {
        //todo: store in db
        return $this->description != null; //file_exists(Yii::app()->getBasePath() . self::descPath . $this->getSanitizedFilename());
    }

    public function obtainDescription()
    {

    }

    /**
     * @return string Description
     */
    public function getDescription()
    {
        //todo: store in db
        return $this->description;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'feature';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        $rules= array(
            array('sid, idManufacturer', 'required'),
            array('sid', 'length', 'max' => 45),
            array('name', 'length', 'max' => 255),
            array('sid', 'unique', 'allowEmpty' => false),
            array('price', 'numerical', 'min' => 0),
            array('enumCurrency', 'numerical', 'min' => 0, 'max' => 4, 'integerOnly' => true),

            array('isPriceSettled, hasText, hasPrice', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idFeature, idManufacturer, sid, name, enumCurrency, price, priceLastUpdated', 'safe', 'on' => 'search'),
        );

//        if (!Permission::isLoggedUserSuperadmin() and !$this->isNewRecord) {
//            $rules[]=array('idManufacturer', 'unsafe', 'on'=>'update');
//            if (Permission::isManufacturerAllowed($this->idManufacturer)) {
//                $rules[]=array('name, description, ruDescription, ruName', 'safe', 'on'=>'update');
//            } else {
//                $rules[]=array('name, description, ruDescription, ruName', 'unsafe', 'on'=>'update');
//            }
//        } else {
//            $rules[]=array('name, idManufacturer, description, ruDescription, ruName', 'safe');
//        }


        return $rules;
    }

    public function beforeDelete()
    {

        if (Yii::app()->user->isGuest) {
            return false;
        }

        if ((count($this->managedblocks)) || (count($this->orders))) {
            $this->addError("", "Нельзя удалить опцию, так как она используется");
            return false;
        }

        if (!Group::getPermissionsUponZone(Group::ZONE_FEATURE,Group::ACTION_DELETE,$this)) {
            return !true;
        }


        return parent::beforeDelete();
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'managedblocks' => array(self::MANY_MANY, 'ManagedBlock', 'managedblockfeature(idFeature, idManagedBlock)'),
            //'orderfeatures' => array(self::HAS_MANY, 'Orderfeature', 'idFeature'),
            'manufacturer' => array(self::BELONGS_TO, 'Manufacturer', 'idManufacturer'),
            'orders' => array(self::MANY_MANY, 'Order', 'orderungroupedfeature(idFeature, idOrder)'),

            //'ownages' => array(self::HAS_MANY, 'Ownage', 'idFeature'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'idFeature' => 'Id Feature',
            'price' => 'Цена',
            'enumCurrency' => 'Валюта',
            'idManufacturer' => 'Производитель',
            'sid' => 'Артикул',
            'name' => 'Наименование',
            'ruName' => 'Перевод наименования на русский',
            'description' => 'Описание',
            'ruDescription' => 'Перевод описания на русский',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idFeature', $this->idFeature);
        $criteria->compare('idManufacturer', $this->idManufacturer);
        $criteria->compare('sid', $this->sid, true);
        $criteria->compare('name', $this->name, true);

        if ($this->hasPrice !== NULL) {
            if ($this->hasPrice == self::HAS_DESCRIPTION) {
                $criteria->addCondition('price > 0');
            } else if ($this->hasPrice == self::NO_DESCRIPTION) {
                $criteria->addCondition('price = 0 or price IS null');
            }
        }

        if ($this->hasText !== NULL) {
            if ($this->hasText == self::HAS_DESCRIPTION) {
                $criteria->addCondition('description is not null');
            } else if ($this->hasText == self::NO_DESCRIPTION) {
                $criteria->addCondition('description is null');
            }
        }

        //INVERSE
        list($outUglySql, $additionalWithes) = Group::getUglySql(Group::ZONE_FEATURE, True);
        $criteria->addCondition($outUglySql);

        foreach ($additionalWithes as $aw) {
            $criteria->with[]=$aw;
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    function behaviors()
    {
        return array(
            'img' => array(
                'class' => 'ext.hasImage.hasImage',
                'file_prefix' => "feature", 
                'thumbnail_width' => 10000,
                'thumbnail_height' => 150,
                'doResize' => true,
            ),
            'loggable' => array(
                'class' => 'ActiveRecordLoggable',
                'logCreation' => true,
                'shouldBubbleToMaster' => false,
            ));
    }

}