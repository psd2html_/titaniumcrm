<?php

/**
 * This is the model class for table "logisticPosition".
 *
 * The followings are the available columns in table 'logisticPosition':
 * @property integer $idLogisticPosition
 * @property integer $innerNumber
 * @property integer $idInsuranceCompany
 * @property integer $idCoreInstrument
 * @property integer $idFeature
 * @property integer $idProject
 * @property string $productionDeadline
 * @property string $poNumber
 * @property string $goodsNameRus
 * @property string $goodsOrigin
 * @property string $goodsNameEng
 * @property integer $isPurchaseViaManufacturer
 * @property integer $isTechnoinfoUnloadsGoods
 * @property integer $isExportDeclarationMade
 * @property integer $idPackingSheetUpload
 * @property integer $idDescriptionUpload
 * @property integer $isSpecialDeliveryRequired
 * @property integer $isExportDoubleApplicationLicenseRequired
 * @property integer $isInsurancePayed
 * @property string $packagingSpecification
 * @property string $buyIncotermsCondition
 * @property string $sellIncotermsCondition
 * @property string $takeoutAddress
 * @property string $tnvedCod
 *
 * todo добавлено по миграции ф4
 * @property string $
 *
 * The followings are the available model relations:
 * @property Project $project
 * @property Coreinstrument $coreInstrument
 * @property Feature $feature
 * @property Upload $packingSheetUpload
 * @property Upload $descriptionUpload
 * @property InsuranceCompany $insuranceCompany
 */
class LogisticPosition extends CActiveRecord
{
    public function beforeValidate() {

        //set null, if nothing is chosen
        if ($this->idInsuranceCompany<=0)
            $this->idInsuranceCompany=null;

        return parent::beforeValidate();
    }

    public function beforeSave() {

        if ($this->isNewRecord) {
            $innerNumber=$this->project->logisticPositionCounter;
            if ($innerNumber==0) {$innerNumber=1;}
            $this->innerNumber=$innerNumber;
            $this->project->logisticPositionCounter=$innerNumber+1;
            $this->project->update(array('logisticPositionCounter'));
        }
        return parent::beforeSave();
    }

    public function beforeDelete()
    {

        if (!$this->project->isSubzoneWritable(Group::SUBZONE_PROJECT_LOGISTIC)) {
            $this->addError('', 'У вас нет необходимых прав для совершения данного действия');
            return false;
        }

        return parent::beforeDelete();
    }

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LogisticPosition the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'logisticPosition';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('idProject','required'),
			array('innerNumber, idInsuranceCompany, idCoreInstrument, idFeature, idProject, isPurchaseViaManufacturer, isTechnoinfoUnloadsGoods, isExportDeclarationMade, isSpecialDeliveryRequired, isExportDoubleApplicationLicenseRequired, isInsurancePayed', 'numerical', 'integerOnly'=>true),
			array('poNumber, goodsNameRus, goodsOrigin, goodsNameEng, buyIncotermsCondition, sellIncotermsCondition, tnvedCod', 'length', 'max'=>255),
			array('productionDeadline, packagingSpecification, takeoutAddress', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idLogisticPosition, innerNumber, idInsuranceCompany, idCoreInstrument, idFeature, idProject, productionDeadline, poNumber, goodsNameRus, goodsOrigin, goodsNameEng, isPurchaseViaManufacturer, isTechnoinfoUnloadsGoods, isExportDeclarationMade, isSpecialDeliveryRequired, isExportDoubleApplicationLicenseRequired, isInsurancePayed, packagingSpecification, buyIncotermsCondition, sellIncotermsCondition, takeoutAddress, tnvedCod', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'packingSheetUpload' => array(self::BELONGS_TO, 'Upload', 'idPackingSheetUpload'),
            'descriptionUpload' => array(self::BELONGS_TO, 'Upload', 'idDescriptionUpload'),
			'project' => array(self::BELONGS_TO, 'Project', 'idProject'),
			'coreInstrument' => array(self::BELONGS_TO, 'CoreInstrument', 'idCoreInstrument'),
			'feature' => array(self::BELONGS_TO, 'Feature', 'idFeature'),
			'insuranceCompany' => array(self::BELONGS_TO, 'InsuranceCompany', 'idInsuranceCompany'),
		);
	}
    public function getName() {
        return $this->goodsNameEng?$this->goodsNameEng:"Название не заполнено";
//        return $this->feature?$this->feature->name:
//            ($this->coreInstrument?$this->coreInstrument->name:"Позиция не выставлена");
    }
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'productionDeadline' => 'Дедлайн производства',
            'poNumber' => 'Номер  PO',
            'goodsNameRus' => 'Название на русском для страхования',
            'goodsOrigin' => 'Производитель/страна происхождения ',
            'goodsNameEng' => 'Название на англ. согласно контракту с поставщиком',
            'idInsuranceCompany' => 'Страховая компания',
            'isPurchaseViaManufacturer' => 'Закупка через производителя или посредника',
            'packagingSpecification' => 'Количество мест и вид тары, измерения, вес нетто/брутто',
            'isInsurancePayed' => 'Страховка',
            'buyIncotermsCondition' => 'Условия закупки у поставщика',
            'sellIncotermsCondition' => 'Условия продажи покупателю',
            'takeoutAddress'=>'Адрес забора и контактные данные',
            'isTechnoinfoUnloadsGoods' => 'Кто разгружает – заказчик или Техноинфо',
            'isExportDeclarationMade' => 'Делает ли производитель экспортную декларацию ',
            'isSpecialDeliveryRequired' => 'Нужны ли особые условия транспортировки  ',
            'isExportDoubleApplicationLicenseRequired' => 'Необходима ли экспортная лицензия как на товар двойного назначения ',
            'tnvedCod'=>'Код ТНВЭД',
            'innerNumber'=>'Порядковый номер'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idLogisticPosition',$this->idLogisticPosition);
		$criteria->compare('innerNumber',$this->innerNumber);
		$criteria->compare('idInsuranceCompany',$this->idInsuranceCompany);
		$criteria->compare('idCoreInstrument',$this->idCoreInstrument);
		$criteria->compare('idFeature',$this->idFeature);
		$criteria->compare('idProject',$this->idProject);
		$criteria->compare('productionDeadline',$this->productionDeadline,true);
		$criteria->compare('poNumber',$this->poNumber,true);
		$criteria->compare('goodsNameRus',$this->goodsNameRus,true);
		$criteria->compare('goodsOrigin',$this->goodsOrigin,true);
		$criteria->compare('goodsNameEng',$this->goodsNameEng,true);
		$criteria->compare('isPurchaseViaManufacturer',$this->isPurchaseViaManufacturer);
		$criteria->compare('isTechnoinfoUnloadsGoods',$this->isTechnoinfoUnloadsGoods);
		$criteria->compare('isExportDeclarationMade',$this->isExportDeclarationMade);
		$criteria->compare('isSpecialDeliveryRequired',$this->isSpecialDeliveryRequired);
		$criteria->compare('isExportDoubleApplicationLicenseRequired',$this->isExportDoubleApplicationLicenseRequired);
		$criteria->compare('isInsurancePayed',$this->isInsurancePayed);
		$criteria->compare('packagingSpecification',$this->packagingSpecification,true);
		$criteria->compare('buyIncotermsCondition',$this->buyIncotermsCondition,true);
		$criteria->compare('sellIncotermsCondition',$this->sellIncotermsCondition,true);
		$criteria->compare('takeoutAddress',$this->takeoutAddress,true);
		$criteria->compare('tnvedCod',$this->tnvedCod,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}