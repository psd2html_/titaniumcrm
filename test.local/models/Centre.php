<?php

/**
 * This is the model class for table "centre".
 *
 * The followings are the available columns in table 'centre':
 * @property integer $idCentre
 * @property integer $idManufacturer
 * @property string $name
 *
 * The followings are the available model relations:
 * @property Manufacturer $idCentre0
 */
class Centre extends ActiveRecord
{


    public $searchManufacturer;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Centre the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeDelete()
    {

        if (Yii::app()->user->isGuest) {
            return false;
        }

        if (!Group::getPermissionsUponZone(Group::ZONE_MEASUREMENT_CENTRE,Group::ACTION_DELETE,$this)) {
            return !true;
        }

        return parent::beforeDelete();
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'centre';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array(' name', 'required'),
            array('idManufacturer', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
            array('manufacturers', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idCentre, searchManufacturer, idManufacturer, name', 'safe', 'on' => 'search'),
        );
    }

    public function getManufacturerNames()
    {

        if (($this->manufacturers)) {
            $out = array();
            foreach ($this->manufacturers as $m) {
                $out[] = $m->name;
            }
            return implode($out, ', ');
        }
        return 'Нету...';
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(

            'manufacturers' => array(self::MANY_MANY, 'Manufacturer', 'centre2manufacturer(idCentre, idManufacturer)'),
            'centre2manufacturers' => array(self::HAS_MANY, 'Centre2manufacturer', 'idCentre'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'idCentre' => 'Центр',
            'idManufacturer' => 'Фирма',
            'manufacturers' => 'В центре есть оборудование от следующих производителей',
            'name' => 'Название',
        );
    }

    function behaviors()
    {
        return array(
            'CAdvancedArBehavior' => array(
                'class' => 'ext.Many2Many.CAdvancedArBehavior'),
            'loggable' => array(
                'class' => 'ActiveRecordLoggable',
                'logCreation' => true,
            ));
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        if ($this->searchManufacturer) {
            $criteria->join = 'JOIN centre2manufacturer as c2m on c2m.idCentre=t.idCentre';
            $criteria->addCondition('c2m.idManufacturer = :ma');
            $criteria->params['ma'] = $this->searchManufacturer;
        }

        $criteria->compare('idCentre', $this->idCentre);
        $criteria->compare('idManufacturer', $this->idManufacturer);
        $criteria->compare('name', $this->name, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}