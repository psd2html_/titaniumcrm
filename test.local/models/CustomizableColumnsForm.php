<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sdd
 * Date: 8/26/13
 * Time: 8:56 PM
 * To change this template use File | Settings | File Templates.
 */
class CustomizableColumnsForm extends CFormModel {
    public $columns;
    public $presetName;
    public $currentPreset;
    public $changePresetTo=null;


    public function attributeLabels() {
        return array("presetName" => "Название текущего пресета");
    }
}