<?php

/**
 * This is the model class for table "pm".
 *
 * The followings are the available columns in table 'pm':
 * @property integer $idPm
 * @property integer $idUser
 * @property string $subject
 * @property integer $idRecipient
 * @property string $body
 * @property integer $status
 * @property string $created
 * @property integer $idProject
 *
 * The followings are the available model relations:
 * @property Project $project
 * @property User $recipient
 */
class Pm extends CActiveRecord implements IProvidesCustomizableColumns
{
    const SUBJECT_CUSTOM=0;
    const SUBJECT_NEW_TASK_ASSIGNED=1;
    const SUBJECT_TASK_DONE=2;
    const SUBJECT_PROJECT_ASSIGNED=3;
    const SUBJECT_PROJECT_LOGISTIC_STATUS = 4;

    public function getSubjectColumn() {
        $a=self::getSubjectList();
        return isset($a[$this->subject])?$a[$this->subject]:$a[0];
    }

    public static function getSubjectList() {
        return array(
            self::SUBJECT_NEW_TASK_ASSIGNED=>"Вам назначена задача",
            self::SUBJECT_TASK_DONE=>"Ваша задача выполнена",
            self::SUBJECT_PROJECT_ASSIGNED=>"Вам назначен проект",
            self::SUBJECT_PROJECT_LOGISTIC_STATUS=>"Проект перешел на этап, связанный с логистикой",
            self::SUBJECT_CUSTOM=>"Другое",
        );
    }

    const PM_UNREAD=0;

    const PM_READ=1;
    const PM_ARCHIVED=2;

    public function init() {
        $this->created=Yii::app()->dateFormatter->format("yyyy-M-d H:m", time());
    }

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Pm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pm';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idRecipient, created', 'required'),
			array('subject, idUser, idRecipient, status, idProject', 'numerical', 'integerOnly'=>true),
			array('body', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idPm, idUser, subject, idRecipient, body, status, created, idProject', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'project' => array(self::BELONGS_TO, 'Project', 'idProject'),
			'user' => array(self::BELONGS_TO, 'User', 'idUser'),
			'recipient' => array(self::BELONGS_TO, 'User', 'idRecipient'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idPm' => 'Id Pm',
			'idUser' => 'Отправитель',
			'subject' => 'Тема',
			'idRecipient' => 'Получатель',
			'body' => 'Сообщение',
			'status' => 'Status',
			'created' => 'Дата',
			'idProject' => 'Проект',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idPm',$this->idPm);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('idRecipient',$this->idRecipient);
		$criteria->compare('body',$this->body,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('idProject',$this->idProject);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * @return ARCustomizableColumnsBase
     */
    public function getCustomizableColumns()
    {
        return new PmCustomizableColumns();
    }
}