<?php

/**
 * This is the model class for table "spent".
 *
 * The followings are the available columns in table 'spent':
 * @property integer $idSpent
 * @property double $amount
 * @property integer $enumCurrency
 * @property integer $enumPaymentGoal
 * @property integer $isCashPayment
 * @property integer $paymentType
 * @property string $idUser
 * @property string $reason
 * @property string $date
 * @property integer $idProject
 *
 * The followings are the available model relations:
 * @property Project $idProject0
 */
class Spent extends ActiveRecord {

    public function getName() {
        return $this->getAmountText() . ' ' . $this->reason;
    }

    public function init() {
        $this->idUser = Yii::app()->user->id;
        $this->date = Yii::app()->dateFormatter->format("yyyy-M-d", time());
    }

    public function beforeDelete() {

        if (Yii::app()->user->isGuest) {
            return false;
        }

       
        return parent::beforeDelete();
    }

    public static function getPaymentTypeList() {
        return array(0 => 'Нал.',
            1 => 'Карта',
            2 => 'Безнал');
    }
    public static function getPaymentGoalList() {
        return array(0 => 'Неизвестно',
            1 => 'Цель',
            2 => 'Еще цель');
    }

    public function getPaymentGoal() {
        $a= self::getPaymentGoalList();
        return $a[$this->enumPaymentGoal];
    }

    public function getPaymentType() {
        $a=$this->getPaymentTypeList();       
        return $a[$this->isCashPayment];
    }

    public function getAmountText() {
        return $this->amount . ' ' . $this->getCurrency();
    }

    public function getCurrency() {
        $o = Order::getCurrencies();
        return $o[$this->enumCurrency];
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Spent the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'spent';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('amount, enumCurrency, isCashPayment, idUser, enumPaymentGoal, date, idProject', 'required'),
            array('enumCurrency, isCashPayment, idProject', 'numerical', 'integerOnly' => true),
            array('amount', 'numerical'),
            array('idUser, reason, enumPaymentGoal', 'safe'),
            array('whoPayed', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idSpent, amount, enumCurrency, isCashPayment, whoPayed, reason, date, idProject', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'project' => array(self::BELONGS_TO, 'Project', 'idProject'),
            'user' => array(self::BELONGS_TO, 'User', 'idUser'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'idSpent' => 'Id Spent',
            'amount' => 'Размер оплаты',
            'enumCurrency' => 'Валюта',
            'isCashPayment' => 'Тип оплаты',
            'enumPaymentGoal' => 'На что пошла оплата',
            'reason' => 'Описание',
            'idUser' => 'Кто оплатил',
            'date' => 'Дата оплаты',
            'idProject' => 'Id Project',
        );
    }

    function behaviors()
    {
        return array(
            'CAdvancedArBehavior' => array(
                'class' => 'ext.Many2Many.CAdvancedArBehavior'),
            'loggable' => array(
                'class' => 'ActiveRecordLoggable',
                'logCreation' => true,
                'shouldBubbleToMaster' => true,
                'hostAfterSaveAction'=>'updateLastChange',
            )
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idSpent', $this->idSpent);
        $criteria->compare('amount', $this->amount);
        $criteria->compare('enumCurrency', $this->enumCurrency);
        $criteria->compare('isCashPayment', $this->isCashPayment);
        $criteria->compare('idUser', $this->idUser);
        $criteria->compare('paymentGoal', $this->enumPaymentGoal);
        $criteria->compare('reason', $this->reason, true);
        $criteria->compare('date', $this->date, true);
        $criteria->compare('idProject', $this->idProject);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}