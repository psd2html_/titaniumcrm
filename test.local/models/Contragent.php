<?php

/**
 * This is the model class for table "contragent".
 *
 * The followings are the available columns in table 'contragent':
 * @property integer $idContragent
 * @property string $name
 * @property string $director
 * @property string $requisites
 *
 * The followings are the available model relations:
 * @property Logistic[] $logistics
 * @property Attachment[] $attachments
 */
class Contragent extends ActiveRecord
{
    function behaviors()
    {
        return array(
            'loggable' => array(
                'class' => 'ActiveRecordLoggable',
                'logCreation' => true,
                'shouldBubbleToMaster' => !true
            )
        );
    }
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Contragent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'contragent';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('name, phone, email, address, director, requisites', 'safe'),
			array('name', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idContragent, name', 'safe', 'on'=>'search'),
		);
	}
        public function beforeDelete() {

        if (Yii::app()->user->isGuest) {
            return false;
        }

        if (count($this->logistics)) {
            $this->addError("", "Нельзя удалить контрагента, так как он участвует в проектах!");
            return false;
        }


        if (!Group::getPermissionsUponZone(Group::ZONE_CONTRAGENT,Group::ACTION_DELETE,$this)) {
           return !true;
        }

        return parent::beforeDelete();
    }
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'attachments' => array(self::MANY_MANY, 'Attachment', 'attachment2contragent(idContragent, idAttachment)'),
			'logistics' => array(self::HAS_MANY, 'Logistic', 'idContragent'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
                    'logistics'=>'Проекты',
            'phone'=>'Телефон',
            'email'=>'Email',
            'address'=>'Адрес',
			'idContragent' => 'Id Contragent',
			'name' => 'Фирма контрагент',
			'director' => 'ФИО Директора',
			'requisites' => 'Реквизиты',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idContragent',$this->idContragent);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('director',$this->director,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}