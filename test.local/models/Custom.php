<?php

/**
 * This is the model class for table "custom".
 *
 * The followings are the available columns in table 'custom':
 * @property integer $idCustom
 * @property string $name
 *
 * The followings are the available model relations:
 * @property Logistic[] $logistics
 */
class Custom extends ActiveRecord {
    function behaviors()
    {
        return array(
            'loggable' => array(
                'class' => 'ActiveRecordLoggable',
                'logCreation' => true,
                'shouldBubbleToMaster' => !true
            )
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Custom the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'custom';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required'),
            array('name', 'length', 'max' => 80),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idCustom, name', 'safe', 'on' => 'search'),
        );
    }
     public function beforeDelete() {

        if (Yii::app()->user->isGuest) {
            return false;
        }
        
        if (count($this->logistics)) 
        {
            $this->addError("", "Нельзя удалить таможню, так как она используется в проектах");
            return false;
        }

         if (!Group::getPermissionsUponZone(Group::ZONE_CUSTOM,Group::ACTION_DELETE,$this)) {
             return !true;
         }
        return parent::beforeDelete();
    }
    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'logistics' => array(self::HAS_MANY, 'Logistic', 'idCustom'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'idCustom' => 'Id Custom',
            'name' => 'Кто занимается растаможкой',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idCustom', $this->idCustom);
        $criteria->compare('name', $this->name, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}