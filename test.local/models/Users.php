<?php
/**
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $activkey
 * @property integer $createtime
 * @property integer $lastvisit
 * @property integer $superuser
 * @property integer $status
 * @property integer $personalFooter
 *
 * @property Array[] permissions
 * @property Array[] permissionRules
 *
 * @property timestamp $create_at
 * @property timestamp $lastvisit_at
 */

class Users extends CActiveRecord
{

    const GROUP_MANAGER = 0;
    const GROUP_SUPERADMIN = 1;
    const GROUP_LOGIST = 2;

    const STATUS_NOACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_BANNED = -1;
    const STATUS_BANED = -1;

    /**
     * @return CActiveRecord[]
     */
    public static function getAllLogists()
    {
        $logistsList=Group::model()->findAll("isLogist=1");
        $out=array();
        foreach ($logistsList as $list) {
            $out=$out+    $list->users;
        }
        return  $out;
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function itemAlias($type, $code = NULL)
    {
        $_items = array(
            'UserStatus' => array(
                self::STATUS_NOACTIVE => UserModule::t('Not active'),
                self::STATUS_ACTIVE => UserModule::t('Active'),
                self::STATUS_BANNED => UserModule::t('Banned'),
            ),
            'AdminStatus' => array(
                '0' => UserModule::t('No'),
                '1' => UserModule::t('Yes'),
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    //TODO: Delete for next version (backward compatibility)

    public function getGroupName()
    {
        $a = self::getAllGroups();
        return $a[$this->group];
    }

    public static function getAllGroups()
    {
        return array(self::GROUP_MANAGER => "Сотрудник",
            self::GROUP_SUPERADMIN => "Суперадмин",
            self::GROUP_LOGIST => "Логист");
    }

    function behaviors()
    {
        return array(
            'img' => array(
                'class' => 'ext.hasImage.hasImage',
                'file_prefix' => "userSign",
                'thumbnail_width' => 469,
                'thumbnail_height' => 251,
                'crop_width' => 234,
                'crop_height' => 125,
                'doResize' => true,
                'no_image' => 'empty.png'
            ),
        );
    }


    public function  getName()
    {
        return $this->profile->firstname . ' ' . $this->profile->lastname;
    }

    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public function getEditUrl()
    {
        return "#";
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'users';
    }

    public function setPersonalFilter($value)
    {
        if (is_array($value)) {
            $this->serializedPersonalFilter = CJSON::encode($value);
        }
    }

    public function getPersonalFilterListData()
    {
        if ($m = $this->getPersonalFilter()) {
            $out = array();
            // manufacturer => class;
            foreach ($m as $u) {
                $key = $u['m'];
                $value = $u['c'];
                if (is_numeric($key) && is_numeric($value)) {
                    $name = Yii::app()->db->createCommand("SELECT name FROM manufacturer where idManufacturer=:m")->bindParam("m", $key)->queryColumn();
                    $class = Yii::app()->db->createCommand("SELECT name FROM coreinstrumentclass where idCoreInstrumentClass=:m")->bindParam("m", $value)->queryColumn();
                    $out[] = array('name' => $name[0], 'class' => $class[0], "idManufacturer" => $key, "idCoreInstrumentClass" => $value);
                }
            }
            return $out;
        } else {
            return array();
        }
    }

    public function getPersonalFilter()
    {
        return CJSON::decode($this->serializedPersonalFilter);
    }

    public function setPermissions($value)
    {

        if (is_array($value)) {
            if (Yii::app()->getModule('user')->isAdmin()) {
                $this->serializedPermissions = $value;
            }
        }

    }

    public function beforeSave()
    {
        if ($this->idGroup == 0) {
            $this->idGroup = null;
        }

        return parent::beforeSave();
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.CConsoleApplication
        return array(
            array('username', 'length', 'max' => 20, 'min' => 3, 'message' => "Incorrect username (length between 3 and 20 characters)."),
            array('password', 'length', 'max' => 128, 'min' => 4, 'message' => "Incorrect password (minimal length 4 symbols)."),
            array('email', 'email'),
            array('abbr', 'length', 'max' => 10),
            array('abbr', 'unique', 'allowEmpty' => false),
            array('username', 'unique', 'message' => "This user's name already exists."),
            array('email', 'unique', 'message' => "This user's email address already exists."),
            array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u', 'message' => "Incorrect symbols (A-z0-9)."),
            array('status', 'in', 'range' => array(self::STATUS_NOACTIVE, self::STATUS_ACTIVE, self::STATUS_BANNED)),
            array('superuser', 'in', 'range' => array(0, 1)),
            array('permissions, group, idGroup, columns, personalFooter,personalFooter2,personalFooter3', 'safe'),
            array('create_at', 'default', 'value' => date('Y-m-d H:i:s'), 'setOnEmpty' => true, 'on' => 'insert'),
            array('lastvisit_at', 'default', 'value' => '0000-00-00 00:00:00', 'setOnEmpty' => true, 'on' => 'insert'),
            array('username, email, superuser, status', 'required'),
            array('superuser, status', 'numerical', 'integerOnly' => true),
            array('group, idGroup, id, username, password, email, activkey, create_at, lastvisit_at, superuser, status', 'safe', 'on' => 'search'),
        ) ;
//            :
//            ((Yii::app()->user->id == $this->id) ? array(
//            array('username, email', 'required'),
//            array('username', 'length', 'max' => 20, 'min' => 3, 'message' => UserModule::t("Incorrect username (length between 3 and 20 characters).")),
//            array('email', 'email'),
//            array('columns, personalFooter,personalFooter2,personalFooter3', 'safe'),
//            array('username', 'unique', 'message' => UserModule::t("This user's name already exists.")),
//            array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u', 'message' => UserModule::t("Incorrect symbols (A-z0-9).")),
//            array('email', 'unique', 'message' => UserModule::t("This user's email address already exists.")),
//            ) : array());

    }

    public function getFullName()
    {
        return $this->profile->firstname . ' ' . $this->profile->lastname;
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        $relations = Yii::app()->getModule('user')->relations;
        if (!isset($relations['profile']))
            $relations['profile'] = array(self::HAS_ONE, 'Profile', 'user_id');

        $relations['permissionRules'] = array(self::HAS_MANY, 'Permission', 'idUser');
        $relations['relatedGroup'] = array(self::BELONGS_TO, 'Group', 'idGroup');

        return $relations;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'abbr' => 'Аббревиатура',
            'personalFooter' => 'Должность',
            'personalFooter2' => 'Контактный телефон и факс',
            'personalFooter3' => 'Email',
            'id' => UserModule::t("Id"),
            'username' => UserModule::t("username"),
            'password' => UserModule::t("password"),
            'verifyPassword' => UserModule::t("Retype Password"),
            'email' => UserModule::t("E-mail"),
            'verifyCode' => UserModule::t("Verification Code"),
            'activkey' => UserModule::t("activation key"),
            'createtime' => UserModule::t("Registration date"),
            'create_at' => UserModule::t("Registration date"),
            'lastvisit_at' => UserModule::t("Last visit"),
            'superuser' => UserModule::t("Superuser"),
            'status' => UserModule::t("Status"),
            'group' => "Группа",
            'idGroup' => "Группа (новый механизм)",
        );
    }

    public function scopes()
    {
        return array(
            'active' => array(
                'condition' => 'status=' . self::STATUS_ACTIVE,
            ),
            'notactive' => array(
                'condition' => 'status=' . self::STATUS_NOACTIVE,
            ),
            'banned' => array(
                'condition' => 'status=' . self::STATUS_BANNED,
            ),
            'superuser' => array(
                'condition' => 'superuser=1',
            ),
            'notsafe' => array(
                'select' => 'id, username, password, email, activkey, create_at, lastvisit_at, superuser, status',
            ),
        );
    }

    public function defaultScope()
    {
        return CMap::mergeArray(Yii::app()->getModule('user')->defaultScope, array(
            'alias' => 'user',
            'select' => 'user.group, user.idGroup, user.id, user.username, user.abbr, user.personalFooter, user.personalFooter2,user.personalFooter3, user.email, user.serializedPreferences, user.serializedPersonalFilter, user.serializedPermissions, user.serializedColumns, user.create_at, user.lastvisit_at, user.superuser, user.status',
        ));
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('password', $this->password);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('activkey', $this->activkey);
        $criteria->compare('create_at', $this->create_at);
        $criteria->compare('lastvisit_at', $this->lastvisit_at);
        $criteria->compare('superuser', $this->superuser);
        $criteria->compare('status', $this->status);
        $criteria->compare('`group`', $this->group);

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->getModule('user')->user_page_size,
            ),
        ));
    }

    public function getCreatetime()
    {
        return strtotime($this->create_at);
    }

    public function setCreatetime($value)
    {
        $this->create_at = date('Y-m-d H:i:s', $value);
    }

    public function getLastvisit()
    {
        return strtotime($this->lastvisit_at);
    }

    public function setLastvisit($value)
    {
        $this->lastvisit_at = date('Y-m-d H:i:s', $value);
    }

    public function afterSave() {
        Yii::app()->user->purgeStoredIdGroupForUser($this->id);
        return parent::afterSave();
    }

}