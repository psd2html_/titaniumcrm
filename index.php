<?php

//phpinfo();
//

error_reporting(0);

// change the following paths if necessary
$yii=dirname(__FILE__).'/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main-local.php';

// remove the following line when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);

// LOCAL STORAGE
if ($_SERVER['SERVER_ADDR'] == "127.0.0.1") {
    defined('YII_LOCAL') or define('YII_LOCAL', false);
} else {
    defined('YII_LOCAL') or define('YII_LOCAL', false);
}

require_once($yii);
Yii::createWebApplication($config)->run();