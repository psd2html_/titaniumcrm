if (typeof window.dwDialog == 'undefined') {
    window.dwDialog = {};
    window.dwDialog.modalDiv = ('#dwModal');
    window.dwDialog.popupDiv = ('#dwModalPopup');
    window.dwDialog.headerDiv = ('#dwModalPopupHeader');
    window.dwDialog.innerDiv = ('#dwModalPopupInner');
    window.dwDialog.location = '';
    window.dwDialog.onSuccess = {};
    window.dwDialog.onCancel = {};
    window.dwDialog.allowClickToClose = true;
    window.dwDialog.forceReload = false;

    window.dwDialog.triggerFilterLock = function (modelName, mGridViewName, button) {
        var isOff=$(button).attr('mode');

        if (isOff == 0) {
            $.ajax({
                url: "/personal/triggerFilterLocking",
                data: {'m': modelName},
                success: function (result) {

                    $(mGridViewName).find("input").first().trigger({
                        type: 'keydown',
                        keyCode: 13
                    });
                },
                error: function (e) {
                    console.log(e);
                    alert('Не удалось зафиксировать панель.');
                }
            });
        } else {
            $.ajax({
                url: "/personal/triggerFilterLocking",
                data: {'m': modelName},
                success: function (result) {
                    $(mGridViewName).find("input").removeAttr("disabled");
                    $(mGridViewName).find("select").removeAttr("disabled");
                    $(button).attr('mode',0);
                    $(button).addClass("filterUnlocked").removeClass("filterLocked");
                },
                error: function (e) {
                    console.log(e);
                    alert('Не удалось зафиксировать панель.');
                }
            });
        }
    };

    window.dwDialog.onAfterShowDialog=null;
    window.dwDialog.callbackDispatcher = {};
    window.dwDialog.callbackDispatcher.registerCallback = function (action, result, callback) {
        if (result)
            window.dwDialog.onSuccess[action] = callback;
        else
            window.dwDialog.onCancel[action] = callback;
    }

    window.dwDialog.callbackDispatcher.invokeCallback = function (action, result, argument) {
        var array = result ? window.dwDialog.onSuccess : window.dwDialog.onCancel;

        for (var a in array) {
            if (action.indexOf(a) >= 0) {
                array[a](argument);
                return true;
            }
        }

        return false;
    }

    window.dwDialog.adjustPosition = function () {
        $(window.dwDialog.popupDiv).css("top", Math.max(0, (($(window).height() - $(window.dwDialog.popupDiv).outerHeight()) / 2) +
            $(window).scrollTop()) + "px");
    }
    window.dwDialog.showDialog = function (data) {
        $(window.dwDialog.modalDiv).show();
        $(window.dwDialog.popupDiv).show();
        $(window.dwDialog.innerDiv).html(data);
        window.dwDialog.adjustPosition();
        if (window.dwDialog.onAfterShowDialog!=null) {
            console.log("meep");
            window.dwDialog.onAfterShowDialog();
            window.dwDialog.onAfterShowDialog=null;
        }
    }

    window.dwDialog.uploadMultipartForm = function (form, url, reload) {

        if (window.dwDialog.forceReload) {
            reload = true;
        }
        if (typeof window.nicEditors !== 'undefined') {
            for (var i = 0; i < nicEditors.editors.length; i++) {
                for (var j = 0; j < nicEditors.editors[i].nicInstances.length; j++) {
                    nicEditors.editors[i].nicInstances[j].saveContent();
                }
            }
        }

        var formElement = document.getElementById(form.replace("#", ""));
        var formData = new FormData(formElement);

        var xhr = new XMLHttpRequest();

        xhr.open("POST", url, true);

        xhr.onreadystatechange = function (e) {
            if (4 == this.readyState) {
                if (this.status == 200) {
                    var data = JSON.parse(this.response);
                    if (data.result == 'success') {
                        if (reload) {
                            location.reload();
                        } else {
                            window.dwDialog.closeDialog(true, data);
                        }
                    } else {
                        window.dwDialog.setInnerHtml(data.msg);
                    }
                } else {
                    alert('Сохранение не удалось. Код ошибки - ' + this.status);
                }
            }
        };

        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        xhr.addEventListener("error", function (e) {
            alert('Сохранение не удалось');
        }, false);

        xhr.send(formData);
    }

    window.dwDialog.uploadForm = function (form, url, reload) {
        window.dwDialog.uploadMultipartForm(form, url, reload);
        return;
    }

    window.dwDialog.loadAjax = function (url, allowClickToClose, forceReload) {
        window.dwDialog.location = url;
        window.dwDialog.allowClickToClose = allowClickToClose;
        window.dwDialog.forceReload = forceReload;
        window.dwDialog.showDialog('<img src="/images/loading.gif" alt="загрузка..." /> Загрузка диалога...');
        $.ajax({
            url: url,
            success: function (result) {
                window.dwDialog.setInnerHtml(result);
            },
            error: function (e) {
                if (e.status == 403) {
                    window.dwDialog.setInnerHtml("<h5>Действие запрещено групповой политикой</h5><p>Сожалеем об этом</p>");
                } else {
                console.log(e);
                alert('Не удалось открыть диалоговое окно. Детали занесены в консоль.');
                }
            }
        });
    }

    window.dwDialog.closeDialog = function (success, argument) {
        $(window.dwDialog.modalDiv).hide();
        $(window.dwDialog.popupDiv).hide();
        window.dwDialog.callbackDispatcher.invokeCallback(window.dwDialog.location, success, argument);
        window.dwDialog.location = null;
    }

    window.dwDialog.setInnerHtml = function (data) {
        try {
            $(window.dwDialog.innerDiv).html(data);
        }
        catch (e) {
            console.log(e);
            console.trace();
        }
        window.dwDialog.adjustPosition();

    }

    window.dwDialog.bindTriggers = function () {
        $("a.dwDialogTrigger").unbind("click").click(function (e) {
            var a = $(this).attr('clickToClose') ? true : false;
            var fr = $(this).attr('forceReload') ? true : false;
            window.dwDialog.loadAjax($(this).attr('href'), a, fr);
            e.preventDefault();
        });
        $("a.foundationModalTrigger").unbind("click").click(function (e) {
            var a = $(this).attr('clickToClose') ? true : false;
            var fr = $(this).attr('forceReload') ? true : false;
            window.dwDialog.loadAjax($(this).attr('href'), a, fr);
            e.preventDefault();
        });
    }
}
$(document).ready(function () {
    var closeButton = function (e) {
        if (window.dwDialog.allowClickToClose ||
            confirm("Вы действительно хотите закрыть диалоговое окно без сохранения?")) {
            window.dwDialog.closeDialog(false, []);
            return;
        }
    };

    window.dwDialog.bindTriggers();
    $(window.dwDialog.modalDiv).unbind("click").click(closeButton);
    $(window.dwDialog.headerDiv).find("button").unbind("click").click(closeButton);

})

if (typeof window.mGridView == 'undefined') {
    window.mGridView = {};

    window.mGridView.bindClearInput=function (id) {
        $("#"+id).find('input[type="text"]').after('<i class="fa fa-times clearInput"></i>');

        $('.clearInput').click(function(){
            var g=$(this).parent().find('input');
            if ( g.val()==='') return;
            if ( g.attr("disabled")!==undefined) return;
            g.val('');
            $(g).trigger({
                type: 'keydown',
                keyCode: 13
            });
        });
    }

    window.mGridView.switchPreset = function (gid, preset, id) {
        $('.grid-view').addClass('grid-view-loading');
        $.ajax({
            url: "/personal/switchPreset/?gid=" + gid + "&preset=" + preset,
            success: function (result) {
                location.reload();
            },
            error: function (e) {
                console.log(e);
                alert('Выполнение смена пресета упало. Детали занесены в консоль.');
            }
        });
    };

    window.mGridView.clickableKeyValueFilter = {};
    window.mGridView.clickableKeyValueFilter.init = function (id) {
        $('.mgvClKVF').unbind("click").click(
            function (e) {
                var g = 'select[name="' + $(this).attr('g') + '"]';
                var v = $(this).attr('v');
                $(g).val(v);
                $(g).trigger({
                    type: 'keydown',
                    keyCode: 13
                });

            }
        )
        //class=\"mgvClF\" g=\"'.$column['name'].'\"
    };

    window.mGridView.clickableFilter = {};
    window.mGridView.clickableFilter.init = function (id) {
        $('.mgvClF').unbind("click").click(
            function (e) {

                var g = 'input[name="' + $(this).attr('g') + '"]';
                $(g).val($(this).html());
                e.preventDefault();
                $(g).trigger({
                    type: 'keydown',
                    keyCode: 13
                });

            }
        )
        //class=\"mgvClF\" g=\"'.$column['name'].'\"
    };
}