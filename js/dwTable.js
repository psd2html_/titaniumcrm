function updateNode(data,id)
{
      $('#table_related_'+id).html(data);
}
function dwTable(container_name,colCount,tableId,baseUrl)
{
    var container=$(container_name);

    var activeRow=-1;
    var activeRowId=-1;
    var sortActive=false;

    init();
    function toggleSort() {
        sortActive=!sortActive;
        if (sortActive)
        {
            unbindListeners();
            $(container).find('tr').unbind().hover(function() {
                $(this.cells[0]).addClass('showDragHandle');
            }, function() {
                $(this.cells[0]).removeClass('showDragHandle');
            });

            $(container).find("table").tableDnD({
                dragHandle: ".dragHandle"
            });

            $(this).attr('value','Сохранить порядок строк');
        }    
        else
        {
            //   $(container).find("table").tableDnD();
            var sendData= new Object();
            var order=new Array();
            $(container).find('tr.edit_tr').each( function() {
                var ri=$(this).attr('rowId');
                if (ri!=0) {
                order.push(ri);
                }
            });
            
            $.ajax({
                url: baseUrl+"/Table/ChangeOrder",
                data: {
                    tableId: tableId, 
                    newOrder: order
                },
                type: 'POST',
            });
            
            bindListeners();
            $(this).attr('value','Изменить порядок строк');
        }       
    }

    //----------------------------------------------------------

    function init() {
        activeRow=-1;
        bindListeners();
        $(container).find('.add-new-btn').unbind().click(addRow);
        $(container).find('.refresh-btn').unbind().click(changeCol);       
        $(container).find('.sort-btn').unbind().click(toggleSort);
        $(container).find('.delete-table-btn').unbind().click(deleteMe);
        
    }
    function deleteMe()
    {
         if(!confirm('Таблица будет удалена безвозвратно! Вы уверены?')) return false;
      
         $.ajax({
            url: baseUrl+"/Table/PurgeTable",
            data: {
                tableId: tableId
            },
            type: 'POST'
        });
         $(container).html('<div class="flash-success">Удалено!</div>');
        
    }
    function bindListeners()
    {
        $(container).find('.create-pump-lnk').unbind().click(createDescription);
        $(container).find('.prevent_default').unbind().click(sp);
        $(container).find('.edit_tr').unbind();
        $(container).find('.dataFul').unbind().click(toggleEdit);
        $(container).find('textarea').unbind().keyup(testKey)
       // $(container).find('.cancel-lnk').unbind().click(cancelEdit);
        $(container).find('.delete-lnk').unbind().click(deleteEdit);
        //$(container).find('.apply-lnk').unbind().click(okEdit);
    }
    function testKey(e)  
    {
        if (e.keyCode==13)
            {
                okEdit();
            }
    }
    function unbindListeners()
    {
     //   $(container).find('.edit_tr').unbind();
        $(container).find('.dataFul').unbind();
       // $(container).find('.cancel-lnk').unbind();
        $(container).find('.delete-lnk').unbind();
      //  $(container).find('.apply-lnk').unbind();
        $(container).find('.create-pump-lnk').unbind();
    }
    function changeCol()
    {
        var cc=$(container).find('input[name=colCount]').attr('value');
        colCount=cc;
        $.ajax({
            url: baseUrl+"/Table/ChangeColumnsCount",
            data: {
                tableId: tableId, 
                colCount: cc
            },
            type: 'POST',
            success: jsonChangeCol,
            datatype: "html"
        });
    }
    function jsonChangeCol(data)
    {
        $(container).html(data);
        init();
    }
    function sp(e)
    {
     e.stopPropagation();   
    }
    function createDescription(e)
    {
        $.ajax({
            url: baseUrl+"/Table/CreateDescription",
            data: {
                tableId: tableId,
                rowId: $(this).attr('rowid')
            },
            type: 'POST',
            success: jsonCreateDescription,
            datatype: "json"
        });
        e.stopPropagation();
    
            e.preventDefault();
    }
    function jsonCreateDescription(json)
    {
          var obj = $.parseJSON(json);
          var max=obj.id;
     //     alert(max);
        $(container).find('.edit_tr[rowId='+obj.rowId+'] .link-box').html('<a href="'+baseUrl+'/Pump/Index?idSE='+max+'" class="prevent_default" target="_blank">Редактировать описание</a>');
        $(container).find('.prevent_default').unbind().click(sp);
    }
    function addRow(e)
    {
        $.ajax({
            url: baseUrl+"/TableRow/CreateNewRow",
            data: {
                tableId: tableId
            },
            type: 'POST',
            success: jsonAddRow,
            datatype: "json"
        });
    }
    function jsonAddRow(json)
    {
        var obj = $.parseJSON(json);
       
       
        var max=obj.id;
        var text="";
        
        text=text+'<tr class="edit_tr"  rowId="'+max+ '">';
        text=text+'<td class="dragHandle"></td>';                

        for (var i=0;i<colCount;i++)
        {
            text=text+'<td class="dataFul">';
            text=text+'<span class="text" rowId="' +max+'"><i>Пусто</i></span>';
            text=text+'<textarea rowId="' +max+ '" style="display: none"></textarea>';
            text=text+("</td>");
        }
                  
        text=text+'<td>'+
            '<div class="link-box">'+
            '<a href="#" rowId="' +max+ '" class="create-pump-lnk">Создать описание</a>'+
            '</div>'+
            '<div class="edit-box" ><a href="#" rowId="'+max+'" class="delete-lnk">Удалить</a></div></td>';
        text=text+"</tr>";
        $(container).find('table > tbody:last').append(text);
        bindListeners();

    }        
    function toggleEdit(e)
    {
 
        if (activeRow!=-1)
        {
            okEdit();
        }
        activeRow=$(this).parent().attr('rowId');
         
       
        $(this).find('span').hide();
         $(this).find('textarea').show().focus();
       // $(container).find('.edit_tr[rowId='+activeRow.toString()+'] textarea').show();
        //$(container).find('.edit_tr[rowId='+activeRow.toString()+'] .edit-box').show();
        //$(container).find('.edit_tr[rowId='+activeRow.toString()+'] .link-box').hide();
        //$(container).find('.edit_tr[rowId='+activeRow.toString()+'] span').hide();
    }
    function deleteEdit(e)
    {
        $.ajax({
            url: baseUrl+"/TableRow/DeleteRow",
            data: {
                rowId: $(this).attr('rowId')
            },
            type: 'POST',
            success: jsonDeleteSuccess,
            datatype: "json"
        });
  
        deactivate(e);     
        e.preventDefault();
    }
    function jsonDeleteSuccess(json)
    {
        var obj = $.parseJSON(json);
        $(container).find('.edit_tr[rowId='+obj.id.toString()+']').remove();
    }
    function okEdit()
    {
        if (activeRow==-1) {return;}
        
        var dat=new Array();
        var sendData= new Object();
        $(container).find('.edit_tr[rowId='+activeRow.toString()+'] .dataFul').each( function() {
            var spn=$(this).find('span');
            
            var tb=$(this).find('textarea');
            
            var text=tb.val();
           // alert(text);
            spn.html((text!='')?text:"<i>Пусто</i>");
            dat.push(text);
        });
    
        sendData.row=dat;
        $.ajax({
            url: baseUrl+"/TableRow/UpdateRowAjax",
            data: {
                row: dat, 
                rowId: activeRow, 
                tableId: tableId
            },
            type: 'POST'
        });
    
        $(container).find('.edit_tr[rowId='+activeRow.toString()+'] span').show();
        $(container).find('.edit_tr[rowId='+activeRow.toString()+'] textarea').hide();
       // $(container).find('.edit_tr[rowId='+activeRow.toString()+'] .edit-box').hide();
        //$(container).find('.edit_tr[rowId='+activeRow.toString()+'] .link-box').show();
            
        activeRow=-1;
        activeRowId=-1;
            
        
    
    }
    function cancelEdit(e)
    {
        $(container).find('.edit_tr[rowId='+activeRow.toString()+'] td, .edit_tr[rowId='+activeRow.toString()+'] th').each( function() {
    
            var spn=$(this).find('span');
            var tb=$(this).find('textarea');
            var text=spn.html();
            tb.val((text!='<i>Пусто</i>')?text:"");
        //spn.html((text!='')?text:"<i>Пусто</i>");
     
        });
        deactivate(e);   
    }
    function deactivate(e)
    {
        if (activeRow!=-1) {
    
    
            $(container).find('.edit_tr[rowId='+activeRow.toString()+'] span').show();
            $(container).find('.edit_tr[rowId='+activeRow.toString()+'] textarea').hide();
            $(container).find('.edit_tr[rowId='+activeRow.toString()+'] .edit-box').hide();
            $(container).find('.edit_tr[rowId='+activeRow.toString()+'] .link-box').show();
            
            activeRow=-1;
            activeRowId=-1;
            e.stopPropagation();
    
            e.preventDefault();
        }
    }
}