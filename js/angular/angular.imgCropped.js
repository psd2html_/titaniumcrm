angular.module('dw.imgCropped', []).directive('imgCropped', function () {
    return {
        restrict: 'E',
        replace: true,
        scope: { settings: '@settings',
            selected: '&' },
        link: function (scope, element, attr) {
            var myImg;


            var clear = function () {
                if (myImg) {
                    myImg.next().remove();
                    myImg.remove();
                    myImg = undefined;
                }
            };

            scope.$watch('settings', function (nv) {
                clear();
                nv = JSON.parse(nv);
                if (nv.src) {
                    var options = {
                        boxWidth: 1000,
                        boxHeight: 500,
                        trackDocument: true,
                        onSelect: function (x) {
                            scope.selected({cords: x});
                        }
                    };
                    if (nv.minW && nv.minH) {
                        options.aspectRatio = nv.minW / nv.minH;
                        options.setSelect = [0, 0, nv.minW, nv.minH];
                        options.minSize = [nv.minW, nv.minH];
                    } else {

                    }
                    element.after('<img />');
                    myImg = element.next();
                    myImg.attr('src', nv.src);
                    $(myImg).Jcrop(options);
                }
            });
            scope.$on('$destroy', clear);
        }
    };
});