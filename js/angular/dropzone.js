var module = angular.module('dropzone', []);

module.filter('bytes', function () {
    return function (bytes, precision) {
        if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';
        if (typeof precision === 'undefined') precision = 1;
        var units = ['байт', 'кБ', 'МБ', 'ГБ', 'ТБ', 'ПБ'],
            number = Math.floor(Math.log(bytes) / Math.log(1024));
        return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) + ' ' + units[number];
    }
});

module.directive("dropzone", function () {
    return {
        templateUrl: "/angularStatic/upload_template.html",
        transclude: true,
        restrict: "EA",
        scope: {
            csrfName: "@",
            csrfToken: "@",
            uploadAction: "@",
            projectId: "@",
            location: "=",
            successCallback: "&"
        },
        link: function ($scope, elem) {
            $scope.files = [];
            $scope.progressVisible = false;
            $scope.uploadFile = function () {
                var fd = new FormData()
                for (var i in $scope.files) {
                    fd.append("file", $scope.files[i])
                }
                fd.append("p", $scope.projectId);
                fd.append("l", $scope.location);
                fd.append($scope.csrfName, $scope.csrfToken);
                xhr = new XMLHttpRequest()
                xhr.upload.addEventListener("progress", uploadProgress, false)
                xhr.addEventListener("load", uploadComplete, false)
                xhr.open("POST", $scope.uploadAction)
                $scope.progressVisible = true
                xhr.send(fd)
            }

            function uploadComplete(evt) {
                $scope.$apply(function () {
                    $scope.progressVisible = false;
                    if (xhr.status == 200) {
                        var result = JSON.parse(xhr.response);
                        if (result.r > 0) {
                            $scope.successCallback();
                        } else {
                            alert("Не удалось загрузить. " + result.m);
                        }
                    }
                });
            }


            $scope.setFiles = function (element) {
                $scope.$apply(function () {
                    $scope.files = []
                    for (var i = 0; i < element.files.length; i++) {
                        $scope.files.push(element.files[i])
                    }
                    $scope.progressVisible = false
                });
            };

            function uploadProgress(evt) {
                $scope.$apply(function () {
                    if (evt.lengthComputable) {
                        $scope.progress = Math.round(evt.loaded * 100 / evt.total);
                    } else {
                        $scope.progress = 'unable to compute'
                    }
                })
            }

            elem.bind('dragover', function (evt) {
                elem.addClass("drop-hover");
                evt.stopPropagation();
                evt.preventDefault();
            })

            elem.bind('dragexit', function (evt) {
                evt.preventDefault();
                evt.stopPropagation();
                elem.removeClass("drop-hover");
            })

            elem.bind('dragleave', function (evt) {
                evt.preventDefault();
                evt.stopPropagation();
                elem.removeClass("drop-hover");
            })

            elem.bind('drop', function (evt) {
                evt.preventDefault();
                evt.stopPropagation();
                elem.removeClass("drop-hover");
                var files = evt.originalEvent.dataTransfer.files;
                $scope.files = []
                for (var i = 0, f; f = files[i]; i++) {
                    $scope.files.push(f)
                }
                $scope.uploadFile();
            });
        }
    }
});