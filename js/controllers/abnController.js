function AbnTestController($scope, $http) {
    var apple_selected;
    $scope.movableFile = null;
    $scope.files = [];
    $scope.updatingFiles = false;

    function deselectCurrentFolder() {
        $scope.currentDirId = -1;
        $scope.selectedFolderData = null;
        $scope.selectedFolder = "";
    }
    deselectCurrentFolder();
    $scope.treeControl = {};

    $scope.renameFile = function (f) {
        var fileName=window.prompt("Комментарий", (f.c)? f.c:'');
        if (fileName!=null) {
            var opt = {n:fileName, h: f.uid, i: f.id, f: $scope.currentDirId};
            opt[$scope.csrfName] = $scope.csrfToken;

            $http.post("/upload/renameFile", opt).then(
                function (data) {
                    if (data.data.r < 0) {
                        alert("Переименование не удалось. " + $scope.data.e);
                    } else {
                        $scope.updateFiles();
                    }
                },
                function (err) {
                    alert("Переименование не удалось");
                }
            );
        }
    }

    $scope.deleteFolder = function(id) {
        if (confirm("Вы действительно хотите удалить папку и все файлы в ней?\n Отмена данной операции невозможна.")) {
            var opt = {p: $scope.idProject, f: id};

            opt[$scope.csrfName] = $scope.csrfToken;

            $http.post("/upload/deleteFolder", opt).then(
                function (data) {
                    if (data.r < 0) {
                        alert("Не удалось удалить. " + $scope.data.e);
                    } else {
                        deselectCurrentFolder();
                        $scope.updateTree();
                    }
                },
                function (err) {
                    alert("Не удалось удалить");
                }
            );
        }
    }

    $scope.renameFolder = function(id) {
        var folderName=window.prompt("Название папки",$scope.selectedFolder);
        if (folderName!=null) {
            var opt = {p: $scope.idProject, f: id, n: folderName};
            opt[$scope.csrfName] = $scope.csrfToken;

            $http.post("/upload/renameFolder", opt).then(
                function (data) {
                    if (data.r < 0) {
                        alert("Не удалось переименовать. " + $scope.data.e);
                    } else {
                        $scope.selectedFolder=folderName;
                        $scope.updateTree();
                    }
                },
                function (err) {
                    alert("Создание не удалось");
                }
            );
        }
    }

    $scope.newFolder = function(isSubfolder, parentId) {
        var folderName=window.prompt("Название папки","Новая папка");
        if (folderName!=null) {
            var opt = {p: $scope.idProject, n: folderName};
            opt[$scope.csrfName] = $scope.csrfToken;

            if (isSubfolder) {
                opt['parent']=parentId;
            }

            $http.post("/upload/createFolder", opt).then(
                function (data) {
                    if (data.r < 0) {
                        alert("Создание не удалось. " + $scope.data.e);
                    } else {
                        $scope.updateTree();
                    }
                },
                function (err) {
                    alert("Создание не удалось");
                }
            );
        }
    }

    $scope.moveFile = function (f) {
        $scope.movableFile = f;
    }

    $scope.deleteFile = function (f) {
        $scope.movableFile = null;
        if (confirm("Вы действительно хотите удалить " + f.n + "?")) {
            var opt = {h: f.uid, i: f.id, f: $scope.currentDirId};
            opt[$scope.csrfName] = $scope.csrfToken;

            $http.post("/upload/deleteFile", opt).then(
                function (data) {
                    if (data.data.r < 0) {
                        alert("Удаление не удалось. " + $scope.data.e);
                    } else {
                        $scope.updateTree();
                        $scope.updateFiles();
                    }
                },
                function (err) {
                    alert("Удаление не удалось");
                }
            );
        }
    }

    $scope.updateFiles = function () {
        $scope.movableFile = null;
        if ($scope.currentDirId == -1) return;
        $scope.updatingFiles = true;
        $http.get("/upload/getFiles/" + $scope.currentDirId).success(function (struct) {

            $scope.files = struct.f;
            $scope.updatingFiles = false;
        })
    }

    $scope.updateTree = function () {

        $scope.movableFile = null;
        $http.get("/upload/getFolders/" + $scope.idProject).success(function (struct) {
            var datBranch=null;

            $scope.fileTree = struct.f;

            if (datBranch) {
                datBranch.selected=true;
            }

            $scope.folderCount = struct.c;
        })
    }

    $scope.my_tree_handler = function (branch) {
        if ($scope.movableFile) {
            var opt = {h: $scope.movableFile.uid, i: $scope.movableFile.id, f: branch.data.id, p: $scope.idProject};
            opt[$scope.csrfName] = $scope.csrfToken;

            $http.post("/upload/moveFile", opt).then(
                function (data) {
                    if (data.data.r < 0) {
                        alert("Перемещение не удалось. " + $scope.data.e);
                    } else {
                        $scope.updateTree();
                        $scope.updateFiles();
                    }
                },
                function (err) {
                    alert("Перемещение не удалось");
                }
            );

            $scope.movableFile = null;
        }

        $scope.movableFile = null;
        var _ref;
        $scope.selectedFolder = branch.label
        $scope.selectedFolderData = branch.data;
        $scope.currentDirId = branch.data.id;
        $scope.updateFiles();

    };
    $scope.fileTree = [];
}