/*
Name:       More/Less Text Collapser
Version:    1.0 (February 22th, 2010)
Author:     Adam C. Miller

Licence:    More/Less Text Collapser is licensed under the MIT license
http://www.opensource.org/licenses/mit-license.php
 
*/
var $j = jQuery.noConflict();
jQuery.fn.moreLess = function(options) {
    var textID = this;
    var opts = $j.extend({}, $j.fn.moreLess.defaults, options);
    $j(".moar_txt",this).click(ControlClick);
    $j(".m_icon",this).click(ControlClick);
    $j(".m_icon_off",this).click(ControlClick);
	return $j(this);
	
    function ControlClick() {
        var content = $j(".hid",this.parentNode);
		var img = $j(".m_icon",this.parentNode);
		var img2 = $j(".m_icon_off",this.parentNode);
        var collapsed =  (content[0].style.display == 'none');
		
		

            content.slideToggle(200, testOnOverSized);
			ToggleControls(collapsed, $j(this));
			img.toggleClass( "hide");
			img2.toggleClass( "hide");
    }
	function testOnOverSized() {
		var arr = $j(this).find('.rtable');
		
						   jQuery.each(arr, function() {
						   if ($j(this).width()>750) {
						   
						        arr=$j(this).find('nobr');
								 jQuery.each(arr, function() {
								$j(this).parent().html($j(this).html());
								});
						   }
							});
	}
    function ToggleControls(collapsed, control) {
        //control.html(collapsed ? opts.expandedText : opts.collapsedText);
    }
    function ToggleControls2(collapsed, control) {
		control.src(collapsed ? "img/minus.png" : "img/plus.png");
    }
   
    function InsertControls(expanded) {
        $j(textID).append("<span class='moreLessControls'>" + (opts.startExpanded ? opts.expandedText : opts.collapsedText) + "</span>");
    }
};

// Default Options
$j.fn.moreLess.defaults = {
startExpanded: false,
collapsedText: 'Полное описание',
expandedText: 'Скрыть полное описание',
truncateIndex: 150,
maximumTruncateIndex: 200,
truncateChar: ' ',
minimumTextLength: 300,
speed: 'fast',
callback: null
};