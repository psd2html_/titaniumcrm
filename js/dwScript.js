/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery(document).ready(function() {
    /* init */
    
    
    /* pGallery */
    var preventStageHoverEffect = function(self){
        self.wrap.unbind('mouseenter').unbind('mouseleave');
        self.imgNav.append('<a class="tray"></a>');
        self.imgNav.show();
        self.hiddenTray = true;
        self.imgNav.find('.tray').bind('click',function(){
            if(self.hiddenTray){
                self.list.parents('.jcarousel-container').animate({
                    height:"80px"
                });
            }else{
                self.list.parents('.jcarousel-container').animate({
                    height:"1px"
                });
            }
            self.hiddenTray = !self.hiddenTray;
        });
    }
    jQuery("#pGalleryUl").PikaChoose({
      
        carousel:true,
        autoPlay:false
    });
    
    /* tooltip */
    return;
    jQuery(".gallery-item").each(function() {
        if (jQuery(this).find('.description')!='') {
            jQuery(this).tooltip({ 
                bodyHandler: function() { 
                    return jQuery(this).find('.description').html(); 
                }, 
            
                track: true, 
    
                showURL: false 
            });
        }
    });
    /* left navigation */
    
})
