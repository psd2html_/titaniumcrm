<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>
<div class="form">
    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span
            class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>


    <!--    ------------- paste here-->

    <div class="row full-width">
        <div class="twelve columns">
            <h4><?= $model->isNewRecord ? 'Создать ' : 'Редактировать' ?> <?= $model->isTask ? 'задачу' : 'комментарий' ?> </h4>

            <?php
            $form_name = 'sticky-ajax-form';
            $form = $this->beginWidget('ext.foundation.widgets.FounActiveForm', array(
                'id' => $form_name,
                'htmlOptions' => array(
                    'onsubmit' => "sendFormAjax('#$form_name'); return false;",
                ),
                'enableAjaxValidation' => false,
                'enableClientValidation' => true,
            ));

            echo $form->errorSummary($model);
            ?>

            <script>
                function updateVisibility() {
                    var taskDone = $('#Sticky_isDone').attr('checked') == 'checked';
                    if (taskDone) {
                        $('#commentBox').show();
                    } else {
                        $('#commentBox').hide();
                    }
                }
                function templateChanged(e) {
                    if ($(e).val() == 1) {
                        // Type set to Task
                        $('.templatePart').show('fast');
                        $('#Sticky_showInCalendar').attr('disabled', 'disabled');
                        $('#Sticky_showInCalendar').attr('checked', '1');
                    } else {
                        // Type set to Note
                        $('.templatePart').hide();
                        $('#Sticky_showInCalendar').removeAttr('disabled');
                    }
                }

                function sendFormAjax(form) {
                    window.dwDialog.uploadForm(form, "<?= Yii::app()->request->getRequestUri() ?>", true);
                }
            </script>



            <?php echo $form->hiddenField($model, 'idUser'); ?>
            <?php echo $form->hiddenField($model, 'date'); ?>
            <?php echo $form->hiddenField($model, 'idProject'); ?>
            <fieldset>
                <div class="row">

                    <div class="six columns indent-bot-15">
                        <label>Прикрепить к дате</label>
                        <?php
                        echo $form->hiddenField($model, 'isTask');
                        $this->widget('ext.proDate.proDate', array(
                            'model' => $model,
                            'attribute' => 'calendarDate',
                            'htmlOptions' => array('class' => 'foundation-date-picker')
                        ));; ?>

                        <?php
                        if ($model->isTask) {
                            $model->showInCalendar = 1;
                            echo $form->checkBoxRow($model, 'showInCalendar', array('disabled' => 'disabled'));
                        } else {
                            echo $form->checkBoxRow($model, 'showInCalendar');
                        }
                        ?>

                    </div>

                    <div class="six columns templatePart">
                        <?php
                        if ($model->isTask) {
                            ?>
                            <div class="float-right">
                                <?php $list = CHtml::listData(User::model()->findAll(), 'id', 'fullname');
                                $list[] = "Никому не назначено";
                                echo $form->dropDownListRow($model, 'idResponsible', $list); ?>
                            </div>
                        <?php } ?>
                    </div>


                </div>
            </fieldset>

            <div class="row">
                <div class="nine columns">
                    <label>Текст</label>
                    <?php
                    $this->widget('ext.niceditor.nicEditorWidget', array(
                        "model" => $model, // Data-Model
                        "attribute" => 'text',
                        //"config" => array(),
                    ));
                    ?>
                    <?php echo $form->error($model, 'text'); ?>
                </div>
                <div class="three columns">
                    <fieldset>
                        <legend>Шаблоны</legend>
                        <?php echo $form->radioButtonListRow($model, 'template', Sticky::getTemplateList(true)); ?>
                        <br/>
                    </fieldset>

                </div>

            </div>
            <?php if ($model->isTask) { ?>
                <hr/>
                <?php echo $form->checkBoxRow($model, 'isDone', array('onchange' => 'updateVisibility();')); ?>
                <div class="row" id="commentBox">
                    <div class="twelve columns">
                        <label>Комментарий</label>
                        <?php
                        $this->widget('ext.niceditor.nicEditorWidget', array(
                            "model" => $model, // Data-Model
                            "attribute" => 'postCommitComment',
                            //"config" => array(),
                        ));
                        ?>
                    </div>
                </div>
            <?php } ?>

            <br/><br/>
            <script>
                setTimeout(
                    function () {
                        console.log("mmm");
                        updateVisibility();
                    }, 100);

            </script>
            <!--    ------------- /paste here-->



            <?php
            echo CHtml::submitButton(Yii::t('app', 'Save'), array('class' => 'button'));

            if (!$model->isNewRecord) {
                echo "&nbsp;" . CHtml::link('Удалить', Yii::app()->createUrl("system/confirmDeletion", array(
                            "mid" => deletionHelper::D_STICKY,
                            "id" => $model->getPrimaryKey()))
                        , array('class' => 'alert button del-btn'));

            }
            $this->endWidget();
            ?>        </div>
        <!-- form -->

