<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Stickies') 
    
); ?><header> <h3> Управление  </h3> </header>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'sticky-grid',
	'dataProvider' => $model->search(),
        'cssFile' => false,
	'template' => '{items} {pager}',
	'columns' => array(
        'idSticky',
        'idUser',
        'created',
        'text',
        array(
                			'name' => 'idProject',
                                        'value' => 'isset($data->idProject0->enumStatus)?$data->idProject0->enumStatus:"N/A"'
                ),
        'colour',
array('class' => 'CButtonColumn',
            'template' => '{update}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options'=>array('class'=>'button secondary tiny delete')
                ),
                'update' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options'=>array('class'=>'button secondary tiny update')
                ),
            ),
        ),
        
	),
)); ?>

 <div class="footer_content">
        <a class="button" href='<?= $this->createUrl("Sticky/create")?> '>Создать новый</a>
 </div>