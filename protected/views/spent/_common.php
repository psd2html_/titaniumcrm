<div class="row">
    <div class="two columns">
        <?php echo $form->labelEx($model, 'amount'); ?>
        <?php echo $form->textField($model, 'amount'); ?>
        <?php echo $form->error($model, 'amount'); ?>
    </div>
    <div class="one column">
        <?php echo $form->labelEx($model, 'enumCurrency'); ?>
        <?php echo $form->dropDownList($model, 'enumCurrency', Order::getCurrencies()); ?>
        <?php echo $form->error($model, 'enumCurrency'); ?>
    </div>
    <div class="two columns">
        <?php echo $form->labelEx($model, 'isCashPayment'); ?>
        <?php echo $form->dropDownList($model, 'isCashPayment', Spent::getPaymentTypeList()); ?>
        <?php echo $form->error($model, 'isCashPayment'); ?>
    </div>
    <div class="two columns">
        <?php echo $form->labelEx($model, 'idUser'); ?>
        <?php echo $form->dropDownList($model, 'idUser', CHtml::listData(User::model()->findAll(), 'id', 'fullname')); ?>
        <?php echo $form->error($model, 'idUser'); ?>
    </div>
    <div class="two columns">
        <?php echo $form->labelEx($model, 'enumPaymentGoal'); ?>
        <?php echo $form->dropDownList($model, 'enumPaymentGoal', Spent::getPaymentGoalList()); ?>
        <?php echo $form->error($model, 'enumPaymentGoal'); ?>
    </div>
    <div class="three columns">
        <?php echo $form->labelEx($model, 'date'); ?>
        <?php
        $this->widget('ext.proDate.proDate', array(
            'model' => $model,
            'attribute' => 'date',
            'htmlOptions' => array('class' => 'foundation-date-picker')
        ));;
        ?>
        <?php echo $form->error($model, 'date'); ?>
    </div>
</div>


<?php echo $form->labelEx($model, 'reason'); ?>
<?php echo $form->textArea($model, 'reason', array('rows' => 6, 'cols' => 50)); ?>
<?php echo $form->error($model, 'reason'); ?>



<?php echo $form->hiddenField($model, 'idProject'); ?>
