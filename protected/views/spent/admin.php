<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Spents') 
    
); ?><header> <h3> Управление  </h3> </header>


<?php $this->widget('MGridView', array(
	'id' => 'spent-grid',
	'dataProvider' => $model->search(),
        'cssFile' => false,
	'template' => '{items} {pager}',
	'columns' => array(
        'idSpent',
        'amount',
        'enumCurrency',
        'isCashPayment',
        'whoPayed',
        'reason',
        'date',
        array(
                			'name' => 'idProject',
                                        'value' => 'isset($data->idProject0->enumStatus)?$data->idProject0->enumStatus:"N/A"'
                ),
array('class' => 'CButtonColumn',
            'template' => '{update}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options'=>array('class'=>'button secondary tiny delete')
                ),
                'update' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options'=>array('class'=>'button secondary tiny update')
                ),
            ),
        ),
        
	),
)); ?>

 <div class="footer_content">
        <a class="button" href='<?= $this->createUrl("Spent/create")?> '>Создать новый</a>
 </div>