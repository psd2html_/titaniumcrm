<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>
<div class="row full-width">
    <div class="twelve columns">
        <h4><?= $model->isNewRecord ? 'Создать новую трату' : 'Редактировать трату' ?></h4>

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'ajax-form',
            'htmlOptions' => array(
                'onsubmit' => "
                window.dwDialog.uploadForm('#ajax-form', '". Yii::app()->request->getRequestUri() ."', true);
                return false;
                ",
            ),
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
        ));

        echo $form->errorSummary($model);
        ?>


        <?php $this->renderPartial('_common', array('model' => $model, 'form' => $form)); ?>

        <?php
        echo CHtml::submitButton(Yii::t('app', 'Save'), array('class' => 'button'));
        $this->endWidget();
        ?>

    </div>
</div> <!-- form -->

