<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>
<div class="form">
    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <?php
    $form = $this->beginWidget('ext.foundation.widgets.FounActiveForm', array(
        'id' => 'spent-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
    ));

    echo $form->errorSummary($model);
    ?>

    
    <?php $this->renderPartial('_common',array('model'=>$model,'form'=>$form)); ?>


    <?php
    echo CHtml::submitButton(Yii::t('app', 'Save'), array('class' => 'button'));

    if (!$model->isNewRecord) {
        echo "&nbsp;" . CHtml::link('Удалить', Yii::app()->createUrl("system/confirmDeletion", array(
                    "mid" => deletionHelper::D_SPENT,
                    "id" => $model->getPrimaryKey()))
                , array('class' => 'alert button del-btn'));
    }
    $this->endWidget();
    ?>        </div> <!-- form -->

