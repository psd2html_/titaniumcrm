<?php Yii::app()->clientScript->registerScriptFile("/js/revealActivate.js",  CClientScript::POS_END); ?>
<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Orders')
);
?><header> <h3> КП </h3> </header>


<?php
$dataProvider=$model->search();//
$dataProvider->sort=array('defaultOrder'=>'date DESC');

$this->widget('MGridView', array(
    'id' => 'order-grid',
    
    'dataProvider' => $dataProvider,    
    'cssFile' => false,
    'template' => '{items} {pager}',
    'columns' => array(
        array(
            'name' => 'idProject',
            'value' => 'isset($data->project->nameUrl)?$data->project->nameUrl:"N/A"',
            'type'=>'html'
        ),
        'name',
        array(
            'name' => 'idCoreInstrument',
            'value' => 'isset($data->coreInstrument->name)?$data->coreInstrument->name:"N/A"'
        ),
        array(
            'name' => 'date',
            'value' => 'Yii::app()->dateFormatter->formatDateTime($data->date, "medium", null)'
        ),
        
        'comment',
        array('class' => 'CButtonColumn',
            'template' => '{update}{delete}{loaddocx}{loadgoogle}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options' => array('class' => 'button secondary tiny delete')
                ),
                'update' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options' => array('class' => 'button secondary tiny update')
                ),
                'loaddocx' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/Word-icon.png',
                    'options' => array('class' => 'button secondary tiny'),
                    'url'=>'Yii::app()->createUrl("/googleDocs/download/",array("id"=>$data->idOrder))',
                    'label'=>'Загрузить DOCX',
                ),
                'loadgoogle' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/google-icon.png',
                    'options' => array('class' => 'button secondary tiny'),
                    'url'=>'Yii::app()->createUrl("/googleDocs/uploadToGoogleDocs/",array("id"=>$data->idOrder))',
                    'label'=>'Загрузить из GoogleDocs',
                ),
            ),
        ),
    ),
));


?>

