<?php Yii::app()->clientScript->registerScriptFile("/js/revealActivate.js",  CClientScript::POS_END); ?>
<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Projects') => array('/project/index'),
    $model->project->name => $model->project->getViewUrl(array( 'noLinkedProjects'=>1)),
    Yii::t('app', 'Update'),
); ?>
<?php
echo CHtml::link('<i class="indented fa fa-download" ng-class="row.tree_icon"></i> Загрузить <br/>сохраненную версию КП', Yii::app()->createUrl("googleDocs/download",array("id"=>$model->idOrder)),array("class"=>'button right secondary'));
?>
<header> <h3> Редактировать коммерческое предложение <?= $model->name ?> </h3> </header>

<div class="module_content">
<?php
$this->renderPartial('_form', array(
			'model'=>$model));
?>
</div>


