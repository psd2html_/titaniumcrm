<?php
/* @var $this OrderController */
if (Yii::app()->user->hasFlash('success')):
    ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'order-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
));

echo $form->errorSummary($model);
?>
<dl class="tabs">
    <dd class="active"><a href="#main">Общие настройки</a></dd>
    <dd><a id="techId" href="#tech">Технические настройки</a></dd>
    <dd><a id="ungroupedId" href="#ungrouped">Дополнительные позиции</a></dd>
    <dd><a id="lawId" href="#law">Расчетная часть</a></dd>

</dl>

<ul class="tabs-content">

    <li class="active" id="mainTab">
        <?php echo $form->hiddenField($model, 'idProject'); ?>

        <script>
            function selectTab(a) {
                $.fn.foundationTabs.set_tab(a);
            }
            /**
             * refresh
             */
            function refreshManufacturers() {
                $.ajax({
                    url: "<?= Yii::app()->createUrl('ajax/getManufacturers') ?>",
                    data: {'idCoreInstrumentClass': $('#Order_idCoreInstrumentClass').val()},
                    type: "POST",
                    success: function (e) {
                        //console.log(e);
                        $('#Order_idManufacturer').html(e.manufacturers);
                        $('#Order_idCoreInstrument').html(e.coreinstruments);
                    }
                });
            }

            function refreshList() {
                $.ajax({
                    url: "<?= Yii::app()->createUrl('ajax/getCoreInstruments') ?>",
                    data: {'idManufacturer': $('#Order_idManufacturer').val(),
                        'idCoreInstrumentClass': $('#Order_idCoreInstrumentClass').val()},
                    type: "POST",
                    success: function (e) {
                        $('#Order_idCoreInstrument').html(e);
                        reloadCoreInstrument();
                    }
                });
            }
        </script>

        <div class="row">
            <div class="four columns indent-bot-15 ">
                <?php echo $form->labelEx($model, 'idCoreInstrumentClass'); ?>
                <?php echo $form->dropDownList($model, 'idCoreInstrumentClass', CHtml::listData(CoreInstrumentClass::model()->findAll(), "idCoreInstrumentClass", "name"), array('disabled' => 'disabled', 'onchange' => 'refreshManufacturers();', 'onkeyup' => 'refreshManufacturers();')); ?>
                <?php echo $form->error($model, 'idCoreInstrumentClass'); ?>
            </div>
            <div class="four columns">
                <?php $manufacturers = $model->idCoreInstrumentClass ? Manufacturer::findAllWhoHasCoreInstrumentClass($model->idCoreInstrumentClass) : Manufacturer::model()->findAll();
                ?>
                <?php echo $form->labelEx($model, 'idManufacturer'); ?>
                <?php echo $form->dropDownList($model, 'idManufacturer', CHtml::listData($manufacturers, "idManufacturer", "name"), array('onchange' => 'refreshList();', 'onkeyup' => 'refreshList();')); ?>
                <?php echo $form->error($model, 'idManufacturer'); ?>
            </div>
            <div class="four columns ">
                <?php

                $coreInstrument = new CoreInstrument();
                $coreInstrument->unsetAttributes();

                if ($model->idManufacturer) {
                    $coreInstrument->idManufacturer = $model->idManufacturer;
                } else {
                    $coreInstrument->idManufacturer = $manufacturers[0]->idManufacturer;
                }

                if ($model->idCoreInstrumentClass) {
                    $coreInstrument->idCoreInstrumentClass = $model->idCoreInstrumentClass;
                }

                $aa = $coreInstrument->search();
                $aa->pagination = false;
                $coreInstrumentList = $aa->getData();
                $coreInstrumentDataList = CHtml::listData($coreInstrumentList, 'idCoreInstrument', 'name');
                $coreInstrumentDataList[0] = "Без базового инструмента";
                ?>
                <?php echo $form->labelEx($model, 'idCoreInstrument'); ?>
                <?php echo $form->dropDownList($model, 'idCoreInstrument', $coreInstrumentDataList); ?>

            </div>
        </div>

        <div class="row">

            <div class="three columns">
                <?php echo $form->labelEx($model, 'idUser'); ?>
                <?php echo CHtml::textField("userOlolo", $model->user->fullname, array('enabled' => 'false', 'disabled' => 'disabled')) ?>
                <?php echo $form->hiddenField($model, 'idUser'); ?>
                <?php echo $form->error($model, 'idUser'); ?>
            </div>

            <div class="three columns">
                <?php echo $form->labelEx($model, 'date'); ?>
                <?php
                $this->widget('ext.proDate.proDate', array(
                    'model' => $model,
                    'attribute' => 'date',
                    'htmlOptions' => array('class' => 'foundation-date-picker')
                ));
                ?>
                <?php echo $form->error($model, 'date'); ?>
            </div>

            <div class="six columns">
                <?php echo $form->labelEx($model, 'idCustomer'); ?>
                <?php
                $linkedCustomers=$model->project->customers;
                $linkedCustomers=array(0=>"Без клиента")+CHtml::listData($linkedCustomers,"idCustomer","name");
                echo $form->dropDownList($model, 'idCustomer', $linkedCustomers)
                ?>
            </div>
        </div>

        <div class="row">
            <div class="six columns">
                <?php echo $form->labelEx($model, 'destinationCity'); ?>
                <?php echo $form->textField($model, 'destinationCity'); ?>
                <?php echo $form->error($model, 'destinationCity'); ?>
            </div>
            <div class="six columns">
                <?php echo $form->labelEx($model, 'destinationCityGenetivus'); ?>
                <?php echo $form->textField($model, 'destinationCityGenetivus'); ?>
                <?php echo $form->error($model, 'destinationCityGenetivus'); ?>
            </div>
        </div>

        <?php echo $form->labelEx($model, 'comment'); ?>
        <?php echo $form->textArea($model, 'comment', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'comment'); ?>
        <?php
        if ($model->isNewRecord) {
            echo CHtml::link("Далее...", "#", array('class' => 'button', 'onclick' => '$("#techId").click(); return false;'));
        }
        ?>
    </li>
    <li id="ungroupedTab">
        <?php
        $this->widget('ext.autocompleter.autocompleter', array(
            'verifyUrl' => 'ajax/doesFeatureExist',
            'autocompleteUrl' => 'ajax/featureAjax',
            'createUrl' => 'ajax/fetchFeatureAjax',
            'hiddenInput' => '#PersonId',
            'defaultValue' => '',
            'multiselect' => true,
            'multiselect_input' => "ungroupedFeatures[]",
            'data' => $model->ungroupedFeatures,
            'wouldCreateNew' => "Артикул не найден.",
            'found' => "Нажмите на Добавить.",
            'badFormat' => "Выберите позицию из предложенных.",
            'promt' => "Введите первые буквы артикула.",
            'allowCreate' => true,
            'showAmount' => true,
            //'amountInput'=>"ungroupedFeatures"
        ));
        ?>
        <?php
        if ($model->isNewRecord) {
            echo CHtml::link("Далее...", "#", array('class' => 'button', 'onclick' => '$("#lawId").click(); return false;'));
        }
        ?>
    </li>
    <li id="techTab">

        <div class="ajax-updated-part">
            <?php
            if ($model->features) {
                $features = $model->features;
            } else {
                $features = array();
            }
            if ($model->idCoreInstrument===0||$model->idCoreInstrument===null)
            {
                $this->renderPartial('_featuresForNoCoreInstrument', array());
            }
                else
                {
            $this->renderPartial('_features', array('model' => $model->coreInstrument ? $model->coreInstrument : $coreInstrumentList[0],
                'features' => $features));
                }
            ?>
        </div>
        <?php
        if ($model->isNewRecord) {
            echo CHtml::link("Далее...", "#", array('class' => 'button', 'onclick' => '$("#ungroupedId").click(); return false;'));
        }
        ?>
    </li>

    <li id="lawTab">
        <?php
        $this->renderPartial('_legal', array(
            'model' => $model, 'form' => $form));
        if ($model->isNewRecord) {
            echo CHtml::submitButton('Создать', array('class' => 'button'));
        }
        ?>

    </li>
</ul>




<?php
if (!$model->isNewRecord) {
    echo CHtml::submitButton("Сохранить как новую версию КП", array('name' => 'saveAsReversion', 'class' => 'button'));
    echo "&nbsp;" . CHtml::submitButton(Yii::t('app', 'Save'), array('name' => 'saveAsCurrent', 'class' => 'button secondary'));

    echo "&nbsp;" . CHtml::link('Удалить', Yii::app()->createUrl("system/confirmDeletion", array(
                "mid" => deletionHelper::D_ORDER,
                "id" => $model->getPrimaryKey()))
            , array('class' => 'alert button del-btn'));
    echo "<br/> <small><span class='required'>*</span> При сохранении под новой версией, у КП сменится номер, дата составления и автор</small>";
}
$this->endWidget();
?>





<script>
    function reloadCoreInstrument() {
        $(".ajax-updated-part").html('<?= Helpers::getSpinnerGif() ?><br/>Загрузка<br/><br/>');
        var mid = $('#Order_idCoreInstrument').val();
        if (mid >= 0) {
            $.ajax({url: "<?= Yii::app()->createUrl("order/getFeaturesAjax") ?>",
                    data: {idCoreInstrument: mid},
                    success: function (data) {
                        $(".ajax-updated-part").html(data);
                    },
                    method: "get"
                }
            );
        }
        else {
            $(".ajax-updated-part").html("Выберите сначала Базовый прибор на предыдущей странице");
        }
    }

    $(document).ready(function () {

        $('.reload-options').click(function (e) {
            reloadCoreInstrument();
            e.preventDefault();
        });
        $('#Order_idCoreInstrument').change(function (e) {
            reloadCoreInstrument();
            e.preventDefault();
        });
    });
</script>

