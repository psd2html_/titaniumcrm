<?php Yii::app()->clientScript->registerScriptFile("/js/revealActivate.js",  CClientScript::POS_END); ?>
<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Projects') => array('/project/index'),
    $model->project->name => $model->project->getViewUrl(array( 'noLinkedProjects'=>1)),
    Yii::t('app', 'Create'),
);?>
<header> <h3> Создать коммерческое предложение </h3> </header>

<div class="module_content">
    
<?php
$this->renderPartial('_form', array(
			'model' => $model,
			'buttons' => 'create'));

?>
</div>