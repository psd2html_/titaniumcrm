<script>

function show_or_hide(id, val) {
    if (val) {
        $("#" + id).show('fast');
    } else {
        $("#" + id).hide();
    }
}

var curr = null;
var currLabel = {
<?= Order::CURRENCY_RUB ?>:
'RUB',
<?= Order::CURRENCY_EUR ?>:
'EUR',
<?= Order::CURRENCY_USD ?>:
'USD',
<?= Order::CURRENCY_GBP ?>:
'GBP',
<?= Order::CURRENCY_YEN ?>:
'YEN'
}
;
function updateCurrencies() {
    curr = {
    <?= Order::CURRENCY_RUB ?>:
    1,
    <?= Order::CURRENCY_EUR ?>:
    parseFloat($('#Order_eurRub').val()),
    <?= Order::CURRENCY_USD ?>:
    parseFloat($('#Order_usdRub').val()),
    <?= Order::CURRENCY_GBP ?>:
    parseFloat($('#Order_gbpRub').val()),
    <?= Order::CURRENCY_YEN ?>:
    parseFloat($('#Order_yenRub').val())
}
;
}

/**
 * Comment
 */


function formatMoney(n) {
    var
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? " " : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}
;

function normalizePrice(sum, source, dest) {
    if (curr == null) {
        updateCurrencies();
    }
    if (isNaN(sum)) {
        sum = 0;
    }
    return sum * curr[source] / curr[dest];
}


function updatePrice() {
    updateCurrencies();
    var ACCType = ($('#Order_enumCalculationAlgorithm').val());
    var CIP = ACCType >= 1;//($('#Order_showCIP').attr('checked') == 'checked');
    var DDP = ACCType >= 2;//($('#Order_showDDP').attr('checked') == 'checked');
    var CIP_amount = parseFloat($('#Order_cipPayment').val());
    var CIP_currency = $('#Order_cipCurrency').val();
    var DDP_coeff = parseFloat($('#Order_ddpCoeff').val());
    var OUT_currency = $('#Order_outCurrency').val();
    var SALE_coeff = $('#Order_saleCoeff').val();

    var sum = 0;
    var out = "";
    var intermediateSum = [0, 0, 0, 0];
    $(".editable_price").each(function () {
        var value = parseFloat($(this).attr('value'));
        var currency = $(this).attr('currency');
        var amount = $(this).attr('amount');
        if (!isNaN(value)) {
            intermediateSum[currency] += value * amount;
            sum += normalizePrice(value, currency, OUT_currency) * amount;
        }
    });

    out += "Сумма по позициям в оригинальной валюте:"
    for (var i = 0; i <= 4; i++) {
        if (intermediateSum[i] > 0) {
            out += " " + formatMoney(intermediateSum[i]) + " " + currLabel[i] + ";"
        }
    }
    out += "<br/>";
    out += "EXW = " + formatMoney(sum) + " " + currLabel[OUT_currency] + "<br/>";

    if (CIP || DDP) {
        sum += normalizePrice(CIP_amount, CIP_currency, OUT_currency);
    }

    if (CIP) {
        out += "CIP = " + formatMoney(sum) + " " + currLabel[OUT_currency] + "<br/>";
        ;
    }
    if (DDP) {
        sum = sum * (isNaN(DDP_coeff) ? 1 : DDP_coeff);
        out += "DDP = " + formatMoney(sum) + " " + currLabel[OUT_currency] + "<br/>";
        ;
    }
    var PP = parseInt($('#Order_enumPricePolicy').val());
    if ((PP === 1) && (!isNaN(SALE_coeff)) && (SALE_coeff < 100) && (SALE_coeff > 0)) {
        out += "Скидка " + SALE_coeff + "% (" + formatMoney(sum * (SALE_coeff) / 100) + " " + currLabel[OUT_currency] + ")<br/>";
        ;
        sum = sum * (100 - SALE_coeff) / 100;
        out += "C учетом скидки = " + formatMoney(sum) + " " + currLabel[OUT_currency] + "<br/>";
        ;
    }
    if ((PP === 2)) {
        var stk = parseFloat($("#Order_overridenPrice").val());
        if ((!isNaN(stk))) {
            out += "Стоимость для клиента " + formatMoney(stk) + " " + currLabel[OUT_currency] + "<br/>";
            var prc = (1 - stk / sum) * 100;
            out += "Процент скидки для клиента " + prc.toFixed(2) + "%" + "<br/>";
            ;
        } else {
            out += "Пожалуйста, укажите стоимость для клиента в окошке<br/>";
            ;
        }

    }


    $("#outVal").html(out);
}

function reloadValutes() {
    $.ajax({url: '<?= Yii::app()->createUrl("ajax/CurrencyCourseBriefAjaxFallback") ?>',
        type: "post",
        success: function (v) {
            var out = v;

            if (out.R == 1) {
                alert("Сайт ЦБ РФ недоступен. Курс валют взят с сайта европейского банка.");
            } else if (out.R < 0) {
                alert("Невозможно обновить курс валют - все источники недоступны.");
                return;
            }
            var mul = 1.03;

            $('#Order_eurRub').val((mul * out.V[<?= Order::CURRENCY_EUR ?>]).toFixed(3));
            $('#Order_usdRub').val((mul * out.V[<?= Order::CURRENCY_USD ?>]).toFixed(3));
            $('#Order_gbpRub').val((mul * out.V[<?= Order::CURRENCY_GBP ?>]).toFixed(3));
            $('#Order_yenRub').val((mul * out.V[<?= Order::CURRENCY_YEN ?>]).toFixed(3));
            updateCurrencies();
        }
    });
}

function reloadAccountingSection() {
    var chosenGroupedOptions = [];


    var chosenUnGroupedOptions = [];


    $("#techTab").find("input:checked").each(function (e) {
        var id = $(this).val();
        var amount = $("#Order_featuresAmount_" + id).val();
        chosenGroupedOptions.push({i: id, a: amount });
    });
    $("#ungroupedTab").find(".multiselect").find("input[type='hidden']").each(function (e) {
        var id = $(this).val();
        var amount = $("#Amount_" + id).val();
        chosenUnGroupedOptions.push({i: id, a: amount });
    });
    var idCI = $("#Order_idCoreInstrument").val();
    var out = (JSON.stringify({ci: idCI,
        fg: chosenGroupedOptions,
        fug: chosenUnGroupedOptions}));

    $.ajax({url: '<?= Yii::app()->createUrl("ajax/getAccounting") ?>',
        type: "post",
        success: updateAccounting,
        data: {json: out}
    });
}

var activeEditor = null;
var oldInlineValue = null;
function declineEditing() {
    $(activeEditor).html(oldInlineValue);
    disableInlineEditor();
}

function acceptEditing() {
    var value = parseFloat($(activeEditor).find("input[name=newPrice]").val());

    var currency = $(activeEditor).find("select[name=newPriceCurrency]").val();

    var mid = $(activeEditor).attr('mid');
    var kind = $(activeEditor).attr('kind');

    if (!isNaN(value)) {
        $(activeEditor).html(value + ' ' + currLabel[currency]);
        $(activeEditor).attr('value', value);
        $(activeEditor).attr('currency', currency);

        $.ajax({url: '<?= Yii::app()->createUrl("system/updatePriceAjax") ?>',
            type: "post",
            error: function (request, status, error) {
                alert(request.responseText);
            },
            data: {kind: kind,
                id: mid,
                value: value,
                currency: currency,
                '<?= Yii::app()->request->csrfTokenName ?>': '<?= Yii::app()->request->csrfToken ?>'}
        });
        disableInlineEditor();
    } else {
        declineEditing();
    }
}

function enableInlineEditor(e) {
    if (activeEditor != null) {
        //disableInlineEditor();
        declineEditing();
        updatePrice();
    }

    var inlineEditor = $(".inline_editor").html();

    oldInlineValue = $(this).html();
    $(this).html(inlineEditor);

    activeEditor = this;

    var value = parseFloat($(activeEditor).attr('value'));
    var currency = $(activeEditor).attr('currency');


    if (!isNaN(value)) {
        $(activeEditor).find("input[name=newPrice]").val(value);
        $(activeEditor).find("select[name=newPriceCurrency]").val(currency);
    } else {
        $(activeEditor).find("input[name=newPrice]").val(0);
        $(activeEditor).find("select[name=newPriceCurrency]").val("1");
    }

    $(this).find("input").keydown(keyUpEditor);
    $(this).unbind().click(doNotBubbleBro);
    $(document).click(declineEditing);
    $(activeEditor).find("input[name=newPrice]").focus().select();
    $(activeEditor).find('.inline_accept').click(function (e) {
            e.preventDefault();
            e.cancelBubble = true;
            if (e.stopPropagation) e.stopPropagation();
            acceptEditing();
        }
    );
    $(activeEditor).find('.inline_decline').click(function (e) {
            e.preventDefault();
            e.cancelBubble = true;
            if (e.stopPropagation) e.stopPropagation();
            declineEditing();
        }
    );
    e.cancelBubble = true;
    if (e.stopPropagation) e.stopPropagation();
}

function doNotBubbleBro(e) {
    e.cancelBubble = true;
    if (e.stopPropagation) e.stopPropagation();
}

function disableInlineEditor() {
    if (activeEditor != null) {

        //$(activeEditor).html("1111");
        $(activeEditor).unbind().click(enableInlineEditor);
        $(document).unbind("click", declineEditing);

        activeEditor = null;
        updatePrice();
    }
}

function keyUpEditor(e) {
    if (e.keyCode == 27) {
        e.preventDefault();
        declineEditing();

        return false;
    }
    /*
     if (e.keyCode == 9) {
     e.preventDefault();
     var nextOne = $(activeEditor).parents('tr').next().find(".editable_price");
     acceptEditing();
     if (nextOne!=null) {
     $(nextOne).click();
     }
     return false;
     }*/

    if (e.keyCode == 13) {
        e.preventDefault();
        acceptEditing();
        return false;
    }
}

function updateAccounting(p) {
    $("#outAccounting").html(p.html);
    $(".editable_price").unbind().click(enableInlineEditor);
    updatePrice();
}

function updateAccessibility() {
    var SpreadDelivery = ($('#Order_isDeliverySpreadBetweenFeatures').attr('checked') == 'checked');
    if (SpreadDelivery) {
        $('#Order_showEXW').removeAttr("checked");
        $('#Order_showEXW').attr("disabled","disabled");
    } else {
        $('#Order_showEXW').removeAttr("disabled");
    }
    updateVisibility();
}

function updateVisibility() {
    var type = ($('#Order_enumCalculationAlgorithm').val());
    var CIP = ($('#Order_showCIP').attr('checked') == 'checked');
    var DDP = ($('#Order_showDDP').attr('checked') == 'checked');
    var TM = !($('#Order_doThreeMonth').attr('checked') == 'checked');
    var PP = parseInt($('#Order_enumPricePolicy').val());

    show_or_hide('cip-fieldset', type >= 1);
    show_or_hide('cip-checkbox', type >= 1);
    show_or_hide('ddp-fieldset', type >= 2);
    show_or_hide('ddp-checkbox', type >= 2);

    show_or_hide('kp-three-month', TM);
    show_or_hide('saleCoeff', PP === 1);
    show_or_hide('manualPrice', PP === 2);
    updatePrice();
}

function init() {
    updateAccessibility();
    updateVisibility();
    reloadAccountingSection();
}

$(document).ready(init);
$("#lawId").click(reloadAccountingSection);
</script>


<div class="row">
    <div class="eight columns">
        <fieldset>
            <legend>Расчет</legend>
            <div id="outAccounting"></div>

            <br/>
        </fieldset>

        <fieldset>
            <legend>Условия КП</legend>


            <div class="row">
                <div class="three columns">
                    <?php echo $form->labelEx($model, 'enumPaymentCondition'); ?>
                    <?php echo $form->dropDownList($model, 'enumPaymentCondition', Order::getPaymentConditionList()); ?>
                    <?php echo $form->error($model, 'enumPaymentCondition'); ?>
                </div>

                <div class="three columns">
                    <?php echo $form->labelEx($model, 'deliver'); ?>
                    <?php echo $form->textField($model, 'deliver', array('size' => 60, 'maxlength' => 256)); ?>
                    <?php echo $form->error($model, 'deliver'); ?>
                </div>
                <div class="three columns">
                    <?php echo $form->labelEx($model, 'guaranteeInMonth'); ?>
                    <?php echo $form->textField($model, 'guaranteeInMonth', array('size' => 60, 'maxlength' => 256)); ?>
                    <?php echo $form->error($model, 'guaranteeInMonth'); ?>
                </div>
                <div class="three columns">
                    <?php echo $form->labelEx($model, 'guaranteeLessThan'); ?>
                    <?php echo $form->textField($model, 'guaranteeLessThan', array('size' => 60, 'maxlength' => 256)); ?>
                    <?php echo $form->error($model, 'guaranteeLessThan'); ?>
                </div>
            </div>


            <div class="row">
                <div class="six columns indent-bot-20">
                    <?php echo $form->labelEx($model, 'doThreeMonth'); ?>
                    <?php echo $form->checkBox($model, 'doThreeMonth', array('onclick' => 'updateVisibility()')); ?>
                    <?php echo $form->error($model, 'doThreeMonth'); ?>
                </div>
                <div class="six columns" id="kp-three-month">
                    <?php echo $form->labelEx($model, 'lastDate'); ?>
                    <?php
                    $this->widget('ext.proDate.proDate', array(
                        'model' => $model,
                        'attribute' => 'lastDate',
                        'htmlOptions' => array('class' => 'foundation-date-picker')
                    ));;
                    ?>
                    <?php echo $form->error($model, 'lastDate'); ?>
                </div>
            </div>

        </fieldset>
    </div>
    <div class="four columns">

        <fieldset>
            <legend>Настрйка</legend>
            <?php echo $form->labelEx($model, 'enumCalculationAlgorithm'); ?>
            <?php echo $form->dropDownList($model, 'enumCalculationAlgorithm', Order::getCalculationAlgorithms(), array('onchange' => 'updateVisibility(); updatePrice();', "onkeyup" => "updateVisibility(); updatePrice();")); ?>
            <?php echo $form->error($model, 'enumCalculationAlgorithm'); ?>
            <br/><br/>

            <div class="row">
                <div class="three columns indent-bot-15">
                    <?php echo $form->labelEx($model, 'isFeaturePriceShown'); ?>
                    <?php echo $form->checkBox($model, 'isFeaturePriceShown'); ?>
                    <?php echo $form->error($model, 'isFeaturePriceShown'); ?>
                </div>

                <div class="three columns">
                    <?php echo $form->labelEx($model, 'showEXW'); ?>
                    <?php echo $form->checkBox($model, 'showEXW', array('onclick' => 'updateVisibility()')); ?>
                    <?php echo $form->error($model, 'showEXW'); ?>

                </div>
                <div class="three columns" id="cip-checkbox">
                    <?php echo $form->labelEx($model, 'showCIP'); ?>
                    <?php echo $form->checkBox($model, 'showCIP', array('onclick' => 'updateVisibility()')); ?>
                    <?php echo $form->error($model, 'showCIP'); ?>

                </div>
                <div class="three columns indent-bot-20" id="ddp-checkbox">
                    <?php echo $form->labelEx($model, 'showDDP'); ?>
                    <?php echo $form->checkBox($model, 'showDDP', array('onclick' => 'updateVisibility()')); ?>
                    <?php echo $form->error($model, 'showDDP'); ?>
                </div>
            </div>

            <div class="row">
                <div class="six columns indent-bot-20">  <?php echo $form->labelEx($model, 'isDeliverySpreadBetweenFeatures'); ?>
                    <?php echo $form->checkBox($model, 'isDeliverySpreadBetweenFeatures', array('onclick'=>'updateAccessibility()')); ?>
                    <?php echo $form->error($model, 'isDeliverySpreadBetweenFeatures'); ?>
                </div>
                <div class="six columns">
                    <?php echo $form->labelEx($model, 'isRussianDescriptionLoaded'); ?>
                    <?php echo $form->checkBox($model, 'isRussianDescriptionLoaded'); ?>
                    <?php echo $form->error($model, 'isRussianDescriptionLoaded'); ?>

                </div>
            </div>

        </fieldset>
        <fieldset id="cip-fieldset">
            <legend>Доставка</legend>
            <div class="row">
                <div class="nine columns"><?php echo $form->labelEx($model, 'cipPayment'); ?>
                    <?php echo $form->textField($model, 'cipPayment', array('onchange' => 'updatePrice()', "onkeyup" => "updatePrice()")); ?>
                    <?php echo $form->error($model, 'cipPayment'); ?>
                </div>
                <div class="three columns"><?php echo $form->labelEx($model, 'cipCurrency'); ?>
                    <?php echo $form->dropDownList($model, 'cipCurrency', Order::getCurrencies(), array('onchange' => 'updatePrice()', "onkeyup" => "updatePrice()")); ?>
                    <?php echo $form->error($model, 'cipCurrency'); ?>
                </div>
            </div>
        </fieldset>
        <fieldset id="ddp-fieldset">
            <legend>DDP</legend>
            <?php echo $form->labelEx($model, 'ddpCoeff'); ?>
            <?php echo $form->textField($model, 'ddpCoeff', array('onchange' => 'updatePrice()', "onkeyup" => "updatePrice()")); ?>
            <?php echo $form->error($model, 'ddpCoeff'); ?>

        </fieldset>
        <fieldset>
            <legend>Валюты</legend>

            <div class="row">
                <div class="six columns">
                    <?php echo $form->labelEx($model, 'eurRub'); ?>
                    <?php echo $form->textField($model, 'eurRub', array('onchange' => 'updatePrice()', "onkeyup" => "updatePrice()")); ?>
                    <?php echo $form->error($model, 'eurRub'); ?>
                    <?php echo $form->labelEx($model, 'usdRub'); ?>
                    <?php echo $form->textField($model, 'usdRub', array('onchange' => 'updatePrice()', "onkeyup" => "updatePrice()")); ?>
                    <?php echo $form->error($model, 'usdRub'); ?>

                </div>
                <div class="six columns">
                    <?php echo $form->labelEx($model, 'gbpRub'); ?>
                    <?php echo $form->textField($model, 'gbpRub', array('onchange' => 'updatePrice()', "onkeyup" => "updatePrice()")); ?>
                    <?php echo $form->error($model, 'gbpRub'); ?>
                    <?php echo $form->labelEx($model, 'yenRub'); ?>
                    <?php echo $form->textField($model, 'yenRub', array('onchange' => 'updatePrice()', "onkeyup" => "updatePrice()")); ?>
                    <?php echo $form->error($model, 'yenRub'); ?>

                </div>
            </div>
            <button class="button secondary small indent-bot-20" onclick="reloadValutes(); return false;">Взять с ЦБ
                +3%
            </button>
            <br/><br/>
            <?php echo $form->labelEx($model, 'enumPricePolicy'); ?>
            <?php echo $form->dropDownList($model, 'enumPricePolicy', Order::getPricePolicies(), array('onchange' => 'updateVisibility(); updatePrice();', "onkeyup" => "updateVisibility(); updatePrice();")); ?>
            <?php echo $form->error($model, 'enumPricePolicy'); ?>
            <br/><br/>

            <div id='saleCoeff'>
                <?php echo $form->labelEx($model, 'saleCoeff'); ?>
                <?php echo $form->textField($model, 'saleCoeff', array('onchange' => 'updatePrice()', "onkeyup" => "updatePrice()")); ?>
                <?php echo $form->error($model, 'saleCoeff'); ?>
            </div>
            <div id='manualPrice'>
                <?php echo $form->labelEx($model, 'overridenPrice'); ?>
                <?php echo $form->textField($model, 'overridenPrice', array('onchange' => 'updatePrice()', "onkeyup" => "updatePrice()")); ?>
                <?php echo $form->error($model, 'overridenPrice '); ?>
            </div>

            <?php echo $form->labelEx($model, 'outCurrency'); ?>
            <?php echo $form->dropDownList($model, 'outCurrency', Order::getCurrencies(), array('onchange' => 'updatePrice()', "onkeyup" => "updatePrice()")); ?>
            <?php echo $form->error($model, 'outCurrency'); ?> <br/><br/>
        </fieldset>
        <fieldset>
            <legend>Расчет</legend>
            <code id="outVal"></code>
            <br/>
        </fieldset>
    </div>
</div>





















