<h4><?= $model->name ?> </h4>

<?php
$description = ManagedBlock::getEnumRules();

echo('<dl class="tabs pill">');
$first = true;
foreach ($model->featureGroups as $f) {
    if (count($f->managedBlocks) > 0) {
        echo($first ? '<dd class="active">' : '<dd>');
        $first = false;
        echo('<a href="#fg' . $f->idFeatureGroup . '">' . $f->name . '</a></dd>');
    }
}
echo('</dl>');


echo('<ul class="tabs-content">');
$first = true;
foreach ($model->featureGroups as $f) {
    if (count($f->managedBlocks) > 0) {
        echo($first ? '<li class="active"' : '<li');
        $first = false;
        echo(" id='fg" . $f->idFeatureGroup . "Tab'>");


        $lookup = array();
        if (count($f->notices) > 0) {


            echo("<div class='more'>");
            echo("<h6>Примечания</h6>");

            echo('<table><tbody>');
            $nId = 1;
            foreach ($f->notices as $n) {

                $lookup[$n->idNotice] = 0;
                echo('<tr id="notice-' . $n->idNotice . '">');
                echo('<td width="1%"><span class="round label">' . $n->mapId . '</span></td>');
                echo('<td>' . ($n->text) . '</td>');
                echo('</tr>');
                $nId++;
            }
            echo('</tbody></table>');
            echo("</div>");
        }

        $img = $f->getImage();

        if ($img) {
            ?>
            <div class="row">

            <div class="six columns">
        <?
        }

        echo('<table>');
        echo('<thead>');
        echo('<tr><th>Позиция</th><th width="50%">Количество</th></tr>');
        echo('</thead>');
        echo('<tbody>');

        foreach ($f->managedBlocks as $m) {
            $notices = $m->getNotices();

            echo("<tr><td colspan='2' class='subheader'>");
            echo('<h6>' . ($m->comment) . '</h6>');
            echo strip_tags($m->notice) ? $m->notice.'<br/>' : '';
            echo('<span class="required">*</span> ' . $description[$m->enumRule].'<br/><br/>');

            if ($m->enumRule==ManagedBlock::SELECT_ONE) {
                echo (" <a href='#' onClick='setOffRadioBlock({$m->idManagedBlock}); return false;' class='button secondary small'>Снять выбор</a>'");
            }
            echo("</td></tr>");

            foreach ($m->features as $x) {
                echo('<tr><td>');
                $v = $x->sid . " - " . $x->nameUrl;
                $optionChecked = array_key_exists($m->idManagedBlock, $features) ? key_exists($x->idFeature, $features[$m->idManagedBlock]) : false;
                switch ($m->enumRule) {
                    case ManagedBlock::SELECT_ONE:
                        echo(CHtml::radioButton("Order[features][" . $m->idManagedBlock . "][]", $optionChecked, array('onChange' => 'toggleAmountView(this,true,' . $m->idManagedBlock . ');', 'value' => $x->idFeature)) . $v);
                        break;
                    case ManagedBlock::SELECT_ONE_OR_MORE:
                    case ManagedBlock::ANY_COMBINATION:
                        echo(CHtml::checkBox("Order[features][" . $m->idManagedBlock . "][]", $optionChecked, array('onChange' => 'toggleAmountView(this,false,' . $m->idManagedBlock . ');', 'value' => $x->idFeature)) . $v);
                        break;
                }

                if (key_exists((string)($x->idFeature), $notices)) {
                    echo('&nbsp;');
                    foreach ($notices[$x->idFeature] as $notice) {
                        echo('<span class="round label" title="' . $notice['text'] . '">' . ($notice['mapId']) . '</span>');
                    }
                }

                if (!$x->hasDescription()) {
                    echo(' <span class="alert round label" title="Нет файла с описанием для данной позиции">!</span>');
                }
                echo('</td><td>');
                $style = $optionChecked ? "" : "style='display:none'";
                echo('<span class="featuresAmountCounter" fid="' . $x->idFeature . '"  mid="' . $m->idManagedBlock . '" ' . $style . '>');

                echo(CHtml::textField("Order[featuresAmount][" . $x->idFeature . "]", $optionChecked ? $features[$m->idManagedBlock][$x->idFeature] : 1, array('class' => 'input-inline')));
                echo('</span>');
                echo('</td></tr>');
            }


        }
        echo('</tbody></table>');

        if ($img) {
            ?>
            </div>
            <div class="six columns">
                <a class="th indent-bot-15" href="<?= $img ?>" style="">
                    <?= CHtml::image($img, "Дерево"); ?>
                </a>
            </div>
            </div>
        <?
        }
        echo("</li>");
    }
}
echo('</ul>');
?>

<?php
$this->widget('ext.slidetoggle.ESlidetoggle', array(
    'itemSelector' => 'div.more',
    'titleSelector' => 'div.more h6',
    'collapsed' => 'div.more',
    'duration' => 'fast'
));
?>

<script>
    function setOffRadioBlock(val) {
        $('input[name="Order[features]['+val+'][]"]').prop('checked', false);
        $(".featuresAmountCounter[mid=" + val + "]").hide();
    }

    function toggleAmountView(e, radio, mid) {
    //    alert("EEE");
        var fid = $(e).val();

        if (radio) {
            $(".featuresAmountCounter[mid=" + mid + "]").hide();
        }

        if (($(e).attr('checked') == 'checked')) {
            $(".featuresAmountCounter[mid=" + mid + "][fid=" + fid + "]").show();
        } else {
            $(".featuresAmountCounter[mid=" + mid + "][fid=" + fid + "]").hide();
        }
    }
</script>
