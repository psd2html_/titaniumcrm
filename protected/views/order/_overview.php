<?php
$Curr = Order::getCurrencies();
$features = $model->coreInstrument ? array($model->coreInstrument) : array();
$features += $model->distinctFeatures;
list($sum, $exw, $cip, $ddp, $discount) = $model->calculateOrder();

?>
<h3><?= $model->name ?></h3>
<?= Helpers::returnTitleAndValueIfNotNull('Тип:', ($model->calculationAlgorithmText)); ?>
<?php if ($model->enumCalculationAlgorithm >= Order::ALGORITHM_EXW)
    echo Helpers::returnTitleAndValueIfNotNull('EXW', (Helpers::formatPrice($exw) . ' ' . $Curr[$model->outCurrency])); ?>
<?php if ($model->enumCalculationAlgorithm >= Order::ALGORITHM_CIP)
    echo Helpers::returnTitleAndValueIfNotNull('CIP', (Helpers::formatPrice($cip) . ' ' . $Curr[$model->outCurrency])); ?>
<?php if ($model->enumCalculationAlgorithm >= Order::ALGORITHM_DDP)
    echo Helpers::returnTitleAndValueIfNotNull('DDP', (Helpers::formatPrice($ddp) . ' ' . $Curr[$model->outCurrency])); ?>
<?= Helpers::returnTitleAndValueIfNotNull('Скидка для клиента', (Helpers::formatPrice($discount) . ' ' . $Curr[$model->outCurrency]), ($model->enumPricePolicy == Order::PRICEPOLICY_SALE)); ?>
<?= Helpers::returnTitleAndValueIfNotNull('Стоимость, выставленная заказчику', (Helpers::formatPrice($sum) . ' ' . $Curr[$model->outCurrency])); ?>
<h5>Позиции</h5>
<table>
    <thead>
    <tr>
        <th>SID</th>
        <th>Имя</th>
        <th>Кол-во</th>
        <th>Стоимость (за ед.)</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($features as $feature) { ?>

        <tr>
            <td><?= $feature->sid ?></td>
            <td><?= $feature->nameUrl ?></td>
            <td><?= $feature->amount ?></td>
            <td><?= $feature->price ? (Helpers::formatPrice($feature->price) . ' ' . $Curr[$model->outCurrency]) : '<i>Нет цены</i>' ?></td>
        </tr>
    <? } ?>
    </tbody>
</table>





