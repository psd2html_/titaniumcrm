<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Feature Groups') 
    
); ?><header> <h3> Управление  </h3> </header>


<?php $this->widget('MGridView', array(
	'id' => 'feature-group-grid',
	'dataProvider' => $model->search(),
        'cssFile' => false,
	'template' => '{items} {pager}',
	'columns' => array(
        'idFeatureGroup',
        'name',
        array(
                			'name' => 'parent',
                                        'value' => 'isset($data->featuregroups->name)?$data->featuregroups->name:"N/A"'
                ),
array('class' => 'CButtonColumn',
            'template' => '{update}{delete}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options'=>array('class'=>'button secondary tiny delete')
                ),
                'update' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options'=>array('class'=>'button secondary tiny update')
                ),
            ),
        ),
        
	),
)); ?>

 <div class="footer_content">
        <a class="button" href='<?= $this->createUrl("FeatureGroup/create")?> '>Создать новый</a>
 </div>