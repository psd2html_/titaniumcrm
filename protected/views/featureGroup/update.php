<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Core Instruments') => array('index'),
    $model->coreInstrument->name=>array('coreInstrument/update','id'=>$model->coreInstrument->idCoreInstrument,'#'=>'grp'),
    Yii::t('app', 'Update'),
); ?>
<header> <h3> <?= $model->name; ?> </h3> </header>
<div class="module_content">
<?php
$this->renderPartial('_form', array(
			'model'=>$model));
?>
</div>