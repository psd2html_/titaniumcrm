<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>
<div class="form">
    <p class="note">
        <?php echo Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required');?>.
    </p>

    <?php
    $form=$this->beginWidget('CActiveForm', array(
    'id'=>'feature-group-form',
    'enableAjaxValidation'=>false,
    'enableClientValidation'=>true,
    ));

    echo $form->errorSummary($model);
    ?>
    
        <fieldset>
            <?php echo $form->labelEx($model,'name'); ?>
            <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->error($model,'name'); ?>
         </fieldset>
            <? $form->hiddenField($model,'idCoreInstrument'); ?>




            <?php
            echo CHtml::submitButton(Yii::t('app', 'Save'),array('class'=>'button'));
             if (!$model->isNewRecord) {
                        echo "&nbsp;" . CHtml::link('Удалить', Yii::app()->createUrl("system/confirmDeletion", array(
                        "mid" => deletionHelper::D_FEATUREGROUP,
                        "id" => $model->getPrimaryKey()))
                        , array('class' => 'alert button del-btn'));
            }
            $this->endWidget(); 
            ?>
    
</div> <!-- form -->

                    