<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Feature Groups') => array('index'),
    Yii::t('app', 'Create'),
);?>
<header> <h6> Добавить группу позиций </h6> </header>

<div class="module_content">
    
<?php
$this->renderPartial('_form', array(
			'model' => $model,
			'buttons' => 'create'));

?>
</div>