<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Core Instrument Classes')
);
?><header> <h3> Управление классами оборудования</h3> </header>


<?php
$this->widget('MGridView', array(
    'id' => 'core-instrument-class-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'cssFile' => false,
    'template' => '{items} {pager}',
    'columns' => array(
        'name',
        'abbr',
        array('class' => 'CButtonColumn',
            'template' => '{update}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options' => array('class' => 'button secondary tiny delete')
                ),
                'update' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options' => array('class' => 'button secondary tiny update')
                ),
            ),
        ),
    ),
));
?>

<div class="footer_content">
    <a class="button" href='<?= $this->createUrl("CoreInstrumentClass/create") ?> '>Создать новый класс оборудования</a>
</div>