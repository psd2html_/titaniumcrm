<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Contragents') 
    
); ?><header> <h3> Управление  </h3> </header>


<?php $this->widget('MGridView', array(
	'id' => 'contragent-grid',
	'dataProvider' => $model->search(),
        'cssFile' => false,
	'template' => '{items} {pager}',
	'columns' => array(

        'name',
        'director',

array('class' => 'CButtonColumn',
            'template' => '{update}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options'=>array('class'=>'button secondary tiny delete')
                ),
                'update' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options'=>array('class'=>'button secondary tiny update')
                ),
            ),
        ),
        
	),
)); ?>

 <div class="footer_content">
        <a class="button" href='<?= $this->createUrl("Contragent/create")?> '>Создать новый</a>
 </div>