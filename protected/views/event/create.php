<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Projects') => array('project/index'),
    $model->project->name => $model->getViewUrl(),
    Yii::t('app', 'Создать событие'),
);?>
<header> <h3> Создать </h3> </header>

<div class="module_content">
    
<?php
$this->renderPartial('_form', array(
			'model' => $model,
			'buttons' => 'create'));

?>
</div>