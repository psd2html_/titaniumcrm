<?php /* @var $model Event */ ?>
<div class="indent-bot-10">
    <?= $model->comment ? $model->comment : '<i>Нет комментария</i>';
    ;
    ?>  
</div>

    <?php if ($model->attachments) { ?>
    <div class="indent-bot-10">
        <?php foreach ($model->attachments as $a) {
            ?>
            <?= CHtml::image(Yii::app()->getBaseUrl() . '/images/icon_attachment.gif', '', array('align' => 'bottom')) ?> <?= $a->title ? $a->title : $a->originalFilename ?> <br/>
    <?php } ?>
    </div>
<?php } ?>

<?= Helpers::reutrnTitleAndValueIfNotNull('Отв.', $model->user ? $model->user->fullname : '') ?>  