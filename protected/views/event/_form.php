<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>

<p class="note">
    <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
</p>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'event-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
    'enableClientValidation' => true,
        ));

echo $form->errorSummary($model);
?>
<dl class="tabs">
    <dd class="active"><a href="#main">Настройки</a></dd>
    <?php if (!$model->isNewRecord) { ?>
    <dd><a href="#attachments">Файлы <?= $model->attachments ? '(' . count($model->attachments) . ')' : '' ?></a></dd>
    <?php } ?>
</dl>
<ul class="tabs-content">
    <li class="active"  id="mainTab">
        <?php
        echo $form->hiddenField($model, 'idProject');
        echo $form->hiddenField($model, 'idCheckpoint');

        echo $form->hiddenField($model, 'eventClass');
//echo $form->dropDownList($model, 'idProject', CHtml::listData(Project::model()->findAll(), 'idProject', 'name')); 
        ?>

        <div class="row">
            <div class="four columns indent-bot-15">
                <?php echo $form->labelEx($model, 'idUser'); ?>
                <?php echo $form->dropDownList($model, 'idUser', CHtml::listData(User::model()->findAll(), 'id', 'fullname'), array('prompt' => 'None')); ?>
                <?php echo $form->error($model, 'idUser'); ?>
            </div>

            <div class="four columns">
                <?php echo $form->labelEx($model, 'enumType'); ?>
                <?php echo $form->dropDownList($model, 'enumType', $model->getEnumTypeList()); ?>
                <?php echo $form->error($model, 'enumType'); ?>
            </div>


            <div class="four columns">
                <?php echo $form->labelEx($model, 'date'); ?>
                <?php
                $this->widget('ext.proDate.proDate', array(
            'model' => $model,
            'attribute' => 'date',
            'htmlOptions'=>array('class'=>'foundation-date-picker')
            
            
            
            )
        );
               
                ?>
                <?php echo $form->error($model, 'date'); ?>
            </div>
        </div>



        <?php echo $form->labelEx($model, 'title'); ?>
        <?php echo $form->textField($model, 'title'); ?>
        <?php echo $form->error($model, 'title'); ?>


        <?php echo $form->labelEx($model, 'comment'); ?>
        <?php $this->widget('ext.niceditor.nicEditorWidget', array(
            "model" => $model, // Data-Model
            "attribute" => 'comment', // Attribute in the Data-Model
            
        )); 
        /*
        $this->widget('ext.ckeditor.CKEditorWidget', array(
            "model" => $model, # Data-Model
            "attribute" => 'comment', # Attribute in the Data-Model
            //"defaultValue" => "Test Text", # Optional
            # Additional Parameter (Check http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.config.html)
            "config" => array(
                "height" => "200px",
                "width" => "100%",
                "toolbar" => "Basic",
            ),
        ));*/

//echo $form->textArea($model, 'comment', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'comment'); ?>

        <?php
        if ($model->isNewRecord) {
            echo $form->labelEx($model, 'attachments');
            for ($i = 1; $i <= 3; $i++) {
                ?>

                <?php echo $form->fileField($model, 'attachment' . $i); ?>
                <?php echo $form->error($model, 'attachment' . $i); ?>
                <br/>
            <?php }
        }
        ?>
        <br/>
<?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('class' => 'button')); ?> &nbsp;
<?php if (!$model->isNewRecord) { echo CHtml::link('Удалить cобытие', '#', array('class' => 'alert button del-btn'));} ?>
    </li>
    <li id="attachmentsTab">
        <?php
        if (!$model->isNewRecord) {
            $this->widget('ext.relation.relation', array(
                'model' => $model,
                'widgetType' => 2,
                'relation' => 'attachments',
                'controllerName'=>'attachment',
                'fields' => 'originalFilenameWithLink',
                'actionParams' => array('Attachment[belongId]' => $model->idEvent,
                    'Attachment[belongType]' => Attachment::BELONGS_TO_EVENT,
                    'returnUrl' => Yii::app()->createUrl('event/update', array('id' => $model->idEvent, '#' => 'attachments'))),
            ));
        } else {
            ?>
            <h6>Редактирование файлов возможно только к уже созданным и сохраненным событиям</h6>
            <p>Сперва сохрание событие, а потом тут появяться файлы</p>
<?php } ?>
    </li>
</ul>









<?php
$this->endWidget();
?>


