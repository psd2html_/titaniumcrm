<?php
$this->breadcrumbs = array(
   Yii::t('app', 'Projects') => array('project/index'),
    $model->project->name => $model->getViewUrl(array( 'noLinkedProjects'=>1)),
    Yii::t('app', 'Редактировать событие'),
);
?>

<header> <h3> <?= $model->title; ?> </h3> </header>
<div class="module_content">
    <?php
    $this->renderPartial('_form', array(
        'model' => $model));
    ?>
  
</div>

