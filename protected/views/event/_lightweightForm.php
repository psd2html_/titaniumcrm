<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'event-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
    'enableClientValidation' => true,
        ));

echo $form->errorSummary($model);
?>
<?php
echo $form->hiddenField($model, 'idProject');
echo $form->hiddenField($model, 'idCheckpoint');

echo $form->hiddenField($model, 'eventClass');
//echo $form->dropDownList($model, 'idProject', CHtml::listData(Project::model()->findAll(), 'idProject', 'name')); 
?>

<div class="row full-width">
    <div class="four columns indent-bot-15">
        <?php echo $form->labelEx($model, 'idUser'); ?>
        <?php echo $form->dropDownList($model, 'idUser', CHtml::listData(User::model()->findAll(), 'id', 'fullname'), array('prompt' => 'None')); ?>
        <?php echo $form->error($model, 'idUser'); ?>
    </div>

    <div class="four columns">
        <?php echo $form->labelEx($model, 'enumType'); ?>
        <?php echo $form->dropDownList($model, 'enumType', $model->getEnumTypeList()); ?>
        <?php echo $form->error($model, 'enumType'); ?>
    </div>


    <div class="four columns">
        <?php echo $form->labelEx($model, 'date'); ?>
        <?php 
        $this->widget('ext.proDate.proDate', array(
            'model' => $model,
            'attribute' => 'date',
            'htmlOptions'=>array('class'=>'foundation-date-picker')
            ));
        
        ?>
        <?php echo $form->error($model, 'date'); ?>
    </div>
</div>

<div class="row full-width">
    <div class="twelve columns">
        <?php echo $form->labelEx($model, 'title'); ?>
        <?php echo $form->textField($model, 'title'); ?>
        <?php echo $form->error($model, 'title'); ?>


        <?php echo $form->labelEx($model, 'comment'); ?>
        <?php 
        $this->widget('ext.niceditor.nicEditorWidget', array(
            "model" => $model, // Data-Model
            "attribute" => 'comment', // Attribute in the Data-Model
            "config" => array("maxHeight" => "400"),
        )); 
        /*      $this->widget('ext.ckeditor.CKEditorWidget', array(
          "model" => $model, # Data-Model
          "attribute" => 'comment', # Attribute in the Data-Model
          //"defaultValue" => "Test Text", # Optional
          # Additional Parameter (Check http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.config.html)
          "config" => array(
          "height" => "200px",
          "width" => "100%",
          "toolbar" => "Basic",
          ),
          ));
         */
       // echo $form->textArea($model, 'comment', array('rows' => 6, 'cols' => 50));
        ?>
        <?php echo $form->error($model, 'comment'); ?>

        <?php
        if ($model->isNewRecord) {
            echo $form->labelEx($model, 'attachments');
            for ($i = 1; $i <= 3; $i++) {
                ?>

                <?php echo $form->fileField($model, 'attachment' . $i); ?>
                <?php echo $form->error($model, 'attachment' . $i); ?>
                <br/>
                <?php
            }
        }
        ?>

        <br/>
        <?php echo CHtml::submitButton(Yii::t('app', 'Save'), array('class' => 'button')); ?> &nbsp;

        <?php
        if (!$model->isNewRecord) {
            echo CHtml::link('Удалить cобытие', '#', array('class' => 'alert button del-btn'));
        }
        ?>
        <?php $this->endWidget(); ?>

        <?php if ($model->attachments) { ?>
            <hr/>

            <?php foreach ($model->attachments as $a) {
                ?>
                <?=
                CHtml::link(CHtml::image(Yii::app()->getBaseUrl() . '/images/icon_attachment.gif', '', array('align' => 'bottom')) . ' ' . $a->titleSafe, Yii::app()->createUrl('Attachment/download', array('id' => $a->idAttachment)), array('target' => '_blank'));
                ?>            
                <br/>
            <?php } ?>

        <?php } ?>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.del-btn').click(function(e){
            if (window.confirm("Вы уверены?")) {
                $.ajax({
                    url: "<?= Yii::app()->createUrl('event/delete',array('id'=>$model->idEvent )) ?>",
                    type: "post",
                    data: {'id2': <?= $model->idEvent ?>,
                '<?=Yii::app()->request->csrfTokenName ?>': '<?=Yii::app()->request->csrfToken?>'
                            },
                    success: function() {
                        window.location='<?= isset($_GET['returnUrl']) ? $_GET['returnUrl'] : '/' ?>';
                    }
                
                });
            }
            e.preventDefault();
        });
    });


</script>
