<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Events') 
    
); ?><header> <h3> Управление  </h3> </header>


<?php $this->widget('MGridView', array(
	'id' => 'event-grid',
	'dataProvider' => $model->search(),
        'cssFile' => false,
	'template' => '{items} {pager}',
	'columns' => array(
        'idEvent',
        array(
                			'name' => 'idProject',
                                        'value' => 'isset($data->project->name)?$data->project->name:"N/A"'
                ),
        'enumType',
        'comment',
        array(
                			'name' => 'idUser',
                                        'value' => 'isset($data->user->fullname)?$data->user->fullname:"N/A"'
                ),
        'date',
array('class' => 'CButtonColumn',
            'template' => '{update}{delete}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options'=>array('class'=>'button secondary tiny delete')
                ),
                'update' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options'=>array('class'=>'button secondary tiny update')
                ),
            ),
        ),
        
	),
)); ?>

 <div class="footer_content">
        <a class="button" href='<?= $this->createUrl("Event/create")?> '>Создать новый</a>
 </div>