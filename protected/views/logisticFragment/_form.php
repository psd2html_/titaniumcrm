<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>
<div class="form">
    <p class="note">
        <?php echo Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required');?>.
    </p>

    <?php
    $form=$this->beginWidget('CActiveForm', array(
    'id'=>'logistic-fragment-form',
    'enableAjaxValidation'=>false,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
    'enableClientValidation'=>true,
    ));

    echo $form->errorSummary($model);
    ?>

    <?php echo $form->hiddenField($model, 'idProject'); ?>

    <div class="row">
        <div class="four columns"> <?php echo $form->labelEx($model, 'cityFrom'); ?>
            <?php echo $form->textField($model, 'cityFrom', array('size' => 60, 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'cityFrom'); ?>
            <?php echo $form->labelEx($model, 'dateUnload'); ?>
            <?php
            $this->widget('ext.proDate.proDate', array(
                'model' => $model,
                'attribute' => 'dateUnload',
                'htmlOptions' => array('class' => 'foundation-date-picker')
            ));; ?>
            <?php echo $form->error($model, 'dateUnload'); ?>
        </div>
        <div class="four columns">
            <?php echo $form->labelEx($model, 'cityTo'); ?>
            <?php echo $form->textField($model, 'cityTo', array('size' => 60, 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'cityTo'); ?>
            <?php echo $form->labelEx($model, 'dateUpload'); ?>
            <?php
            $this->widget('ext.proDate.proDate', array(
                'model' => $model,
                'attribute' => 'dateUpload',
                'htmlOptions' => array('class' => 'foundation-date-picker')
            ));; ?>
            <?php echo $form->error($model, 'dateUpload'); ?>

        </div>
        <div class="four columns">
            <?php echo $form->labelEx($model, 'enumTransport'); ?>
            <?php echo $form->dropDownList($model, 'enumTransport', Logistic::getEnumTransportList()); ?>
            <?php echo $form->error($model, 'enumTransport'); ?>

        </div>
    </div>


    <?php echo $form->labelEx($model, 'comment'); ?>
    <?php echo $form->textArea($model, 'comment'); ?>
    <?php echo $form->error($model, 'comment'); ?>
    <fieldset>
        <legend>Прикрепленные файлы</legend>
        <?php
        $this->widget('ext.relation.relation', array(
            'model' => $model,
            'widgetType' => 2,
            'relation' => 'attachments',
            'controllerName' => 'attachment',
            'fields' => 'originalFilenameWithLink',
            'showCreate'=>false,
            'actionParams' => array('Attachment[belongId]' => $model->idProject,
                'Attachment[belongType]' => Attachment::BELONGS_TO_LOGISTICFRAGMENT,
                'returnUrl' => $model->project->getViewUrl(array( 'noLinkedProjects'=>1, '#' => 'logistic'))),
        ));
        ?>

        <?php
        for ($i = 1; $i <= 3; $i++) {
            ?>

            <?php echo $form->fileField($model, 'attachment' . $i); ?>
            <?php echo $form->error($model, 'attachment' . $i); ?>
            <br/>
        <?php
        }
        ?>
        <br/>
    </fieldset>
    <br/>

    <?php
        echo CHtml::submitButton(Yii::t('app', 'Save'),array('class'=>'button'));
         
        if (!$model->isNewRecord) {
                    echo "&nbsp;" . CHtml::link('Удалить', Yii::app()->createUrl("system/confirmDeletion", array(
                    "mid" => deletionHelper::D_LOGISTICFRAGMENT,
                    "id" => $model->getPrimaryKey()))
                    , array('class' => 'alert button del-btn'));
        
        }
    $this->endWidget();
        ?>        </div> <!-- form -->

