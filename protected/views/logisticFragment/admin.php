<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Logistic Fragments') 
    
); ?><header> <h3> Управление  </h3> </header>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'logistic-fragment-grid',
	'dataProvider' => $model->search(),
        'cssFile' => false,
	'template' => '{items} {pager}',
	'columns' => array(
        'cityFrom',
        'cityTo',
        'dateUnload',
        'dateUpload',
        'enumTransport',

array('class' => 'CButtonColumn',
            'template' => '{update}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options'=>array('class'=>'button secondary tiny delete')
                ),
                'update' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options'=>array('class'=>'button secondary tiny update')
                ),
            ),
        ),
        
	),
)); ?>

 <div class="footer_content">
        <a class="button" href='<?= $this->createUrl("LogisticFragment/create")?> '>Создать новый</a>
 </div>