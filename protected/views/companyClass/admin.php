<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Company Classes') 
    
); ?><header> <h3> Управление типами организации </h3> </header>


<?php $this->widget('MGridView', array(
	'id' => 'company-class-grid',
	'dataProvider' => $model->search(),
    'filter'=>$model,
        'cssFile' => false,
	'template' => '{items} {pager}',
	'columns' => array(
        'name',
array('class' => 'CButtonColumn',
            'template' => '{update}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options'=>array('class'=>'button secondary tiny delete')
                ),
                'update' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options'=>array('class'=>'button secondary tiny update')
                ),
            ),
        ),
        
	),
)); ?>

 <div class="footer_content">
        <a class="button" href='<?= $this->createUrl("CompanyClass/create")?> '>Создать новый тип организации</a>
 </div>