<?php Yii::app()->clientScript->registerScriptFile("/js/revealActivate.js", CClientScript::POS_END); ?>


<h3> <?= $model->fullName ?>  </h3>
<?php
$cols = array(
    array('lastName', 'firstName', 'middleName', 'phone', 'phone2', 'mobilePhone', 'skype', 'icq', 'abbr'),
    array('address', 'legalAddress'),
);
?>
<div class="row full-width">
    <div class="four columns">
        <?php
        foreach ($cols[0] as $attribute) {
            echo Helpers::reutrnTitleAndValueIfNotNull($model->getAttributeLabel($attribute), $model->getAttribute($attribute));
        }
        ?>
    </div>
    <div class="four columns">

        <?php
        echo Helpers::reutrnTitleAndValueIfNotNull($model->getAttributeLabel("company"), $model->idCompany ? (CHtml::link($model->company->name, Yii::app()->createUrl('company/view', array('id' => $model->idCompany)))) : '');
        foreach ($cols[1] as $attribute) {
            echo Helpers::reutrnTitleAndValueIfNotNull($model->getAttributeLabel($attribute), $model->getAttribute($attribute));
        }

        ?>

    </div>
    <div class="four columns">
        <?php
        $img = $model->getThumbnail(false, true);
        if ($img) {
            echo CHtml::image($img, 'Картинка', array('class' => 'indent-bot-10'));
            echo '<br/>';
        }
        ?>
    </div>
</div>

<?php
if ((strip_tags($model->comment))) {
    ?>
    <fieldset>
        <legend>Комментарий</legend>
        <?= ($model->comment) ?><br/>
    </fieldset>
<?php } ?>



<h5> Проекты c участием данного клиента </h5>

<?php
$this->mGridViewDataProvider->renderMGridView('project-grid',$model,'customer');
?>
