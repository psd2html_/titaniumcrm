<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>
    <div class="form">
        <p class="note">
            <?php echo Yii::t('app', 'Fields with'); ?> <span
                class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
        </p>

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'customer-form',
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
        ));

        echo $form->errorSummary($model);
        ?>

        <fieldset>
            <legend>Персональная карточка</legend>
            <div class="row">
                <div class="three columns">
                    <?php echo $form->labelEx($model, 'firstName'); ?>
                    <?php echo $form->textField($model, 'firstName', array('size' => 60, 'maxlength' => 90)); ?>
                    <?php echo $form->error($model, 'firstName'); ?>

                    <?php echo $form->labelEx($model, 'phone'); ?>
                    <?php echo $form->textField($model, 'phone', array('size' => 60, 'maxlength' => 90)); ?>
                    <?php echo $form->error($model, 'phone'); ?>

                    <?php echo $form->labelEx($model, 'email'); ?>
                    <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 90)); ?>
                    <?php echo $form->error($model, 'email'); ?>
                </div>
                <div class="three columns">
                    <?php echo $form->labelEx($model, 'lastName'); ?>
                    <?php echo $form->textField($model, 'lastName', array('size' => 60, 'maxlength' => 90)); ?>
                    <?php echo $form->error($model, 'lastName'); ?>

                    <?php echo $form->labelEx($model, 'mobilePhone'); ?>
                    <?php echo $form->textField($model, 'mobilePhone', array('size' => 60, 'maxlength' => 90)); ?>
                    <?php echo $form->error($model, 'mobilePhone'); ?>

                    <?php echo $form->labelEx($model, 'email2'); ?>
                    <?php echo $form->textField($model, 'email2', array('size' => 60, 'maxlength' => 90)); ?>
                    <?php echo $form->error($model, 'email2'); ?>
                </div>
                <div class="three columns">
                    <?php echo $form->labelEx($model, 'middleName'); ?>
                    <?php echo $form->textField($model, 'middleName', array('size' => 60, 'maxlength' => 90)); ?>
                    <?php echo $form->error($model, 'middleName'); ?>

                    <?php echo $form->labelEx($model, 'phone2'); ?>
                    <?php echo $form->textField($model, 'phone2', array('size' => 60, 'maxlength' => 90)); ?>
                    <?php echo $form->error($model, 'phone2'); ?>

                    <?php echo $form->labelEx($model, 'skype'); ?>
                    <?php echo $form->textField($model, 'skype', array('size' => 60, 'maxlength' => 90)); ?>
                    <?php echo $form->error($model, 'skype'); ?>

                </div>

                <div class="three columns">
                    <div class="indent-bot-15">
                        <?php echo CHtml::label('Фотография', ''); ?>
                        <?php
                        if (!$model->isNewRecord) {
                            $img = $model->getThumbnail(false, true);
                            if ($img) {
                                echo CHtml::image($img, 'Картинка', array('class' => 'indent-bot-10'));
                                echo '<br/>';
                            }
                        }
                        ?>
                        <?php
                        echo $form->fileField($model, 'attached_image');
                        echo $form->error($model, 'attached_image');
                        ?>
                    </div>
                </div>
            </div>


            <?php echo $form->labelEx($model, 'address'); ?>
            <?php echo $form->textField($model, 'address', array('size' => 60, 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'address'); ?>

        </fieldset>
        <fieldset>
        <div class="row">
            <div class="six columns indent-bot-15"> <?php echo $form->labelEx($model, 'idCompany'); ?>
                <?php
                $attribute = 'idCompany';

                echo $form->hiddenField($model, $attribute);
                $v = $model->getRelated('company');
                $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                    'name' => ('autocomplete_' . get_class($model)),
                    'value' => $v ? $v->fullName : '',
                    'source' => Yii::app()->createUrl('ajax/companyAjax'),
                    'options' => array(
                        'select' => "js:function(event, ui) {
                                          $('#" . get_class($model) . "_" . $attribute . "').val(ui.item.id);
                                        }",
                        'change' => "js:function(event, ui) {
                                          if (!ui.item) {
                                             $('#" . get_class($model) . "_" . $attribute . "').val('');
                                          }
                                        }",
                        'size' => 120,
                        'showAnim' => 'fold',
                    ),
                ));
                ?>
                <?php echo $form->error($model, 'idCompany'); ?>
                <?php echo CHtml::link("Создать новую компанию",array('company/create'),array('class'=>'dwDialogTrigger button secondary small')); ?>
            </div>
            <div class="six columns">
                <?php echo $form->labelEx($model, 'job'); ?>
                <?php echo $form->textField($model, 'job', array('size' => 60, 'maxlength' => 255)); ?>
                <?php echo $form->error($model, 'job'); ?></div>
            </div>
        </div>



        </fieldset>

        <?php echo $form->labelEx($model, 'comment'); ?>
        <?php
        $this->widget('ext.niceditor.nicEditorWidget', array(
            "model" => $model, // Data-Model
            "attribute" => 'comment',
            //"config" => array(),
        ));
        ?>

        <?php
        echo CHtml::submitButton(Yii::t('app', 'Save'), array('class' => 'button'));
        if (!$model->isNewRecord) {
            echo "&nbsp;" . CHtml::link('Удалить', Yii::app()->createUrl("system/confirmDeletion", array(
                        "mid" => deletionHelper::D_CUSTOMER,
                        "id" => $model->getPrimaryKey()))
                    , array('class' => 'alert button del-btn'));
        }

        $this->endWidget();
        ?>
    </div> <!-- form -->




<?php
$this->widget('ext.slidetoggle.ESlidetoggle', array(
    'itemSelector' => 'div.more',
    'titleSelector' => 'div.more h6',
    'collapsed' => 'div.more',
    'duration' => 'fast'
));
?>