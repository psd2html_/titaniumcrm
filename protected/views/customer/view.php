<?php Yii::app()->clientScript->registerScriptFile("/js/revealActivate.js", CClientScript::POS_END); ?>

<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Customers') => array('index'),
    $model->shortName,
);
/* @var $model Customer */


$cols = array(
    array('shortName', 'city', 'phone', 'addPhone', 'abbr'),
    array('physicalAddress', 'legalAddress'),
);
?>


<?php
$cols = array(
    array('lastName', 'firstName', 'middleName', 'phone', 'phone2', 'mobilePhone', 'skype', 'icq', 'abbr'),
    array('address', 'legalAddress'),
);
?>

<h3> <?= (Group::getPermissionsUponZone(Group::ZONE_CLIENT,Group::ACTION_UPDATE,$model)) ? CHtml::link($model->fullName . ' ' . CHtml::image(Yii::app()->getBaseUrl() . '/images/pencil.png', 'Редактировать'), array('customer/update', 'id' => $model->idCustomer)) : $model->fullName ?>  </h3>

<div class="row">
    <div class="four columns">
        <?php
        foreach ($cols[0] as $attribute) {
            echo Helpers::reutrnTitleAndValueIfNotNull($model->getAttributeLabel($attribute), $model->getAttribute($attribute));
        }
        ?>
    </div>
    <div class="four columns">

        <?php
        echo Helpers::reutrnTitleAndValueIfNotNull($model->getAttributeLabel("company"), $model->idCompany ? (CHtml::link($model->company->name, Yii::app()->createUrl('company/view', array('id' => $model->idCompany)))) : '');
        foreach ($cols[1] as $attribute) {
            echo Helpers::reutrnTitleAndValueIfNotNull($model->getAttributeLabel($attribute), $model->getAttribute($attribute));
        }

        ?>

    </div>
    <div class="four columns">
        <?php
        $img = $model->getThumbnail(false, true);
        if ($img) {
            echo CHtml::image($img, 'Картинка', array('class' => 'indent-bot-10'));
            echo '<br/>';
        }
        ?>
    </div>
</div>

<?php
if ((strip_tags($model->comment))) {
    ?>
    <fieldset>
        <legend>Комментарий</legend>
        <?= ($model->comment) ?><br/>
    </fieldset>
<?php } ?>

<?php
if (false) {
    $cols = array(
        array('shortName', 'city', 'phone', 'addPhone', 'abbr'),
        array('physicalAddress', 'legalAddress'),
    );
    ?>
    <h6>  <?= $model->company->fullName ?>  </h6>
    <div class="row">
        <div class="six columns">
            <?php
            foreach ($cols[0] as $attribute) {
                echo Helpers::reutrnTitleAndValueIfNotNull($model->company->getAttributeLabel($attribute), $model->company->getAttribute($attribute));
            }
            ?>
        </div>
        <div class="six columns">
            <?php
            echo Helpers::reutrnTitleAndValueIfNotNull($model->company->getAttributeLabel("idCompanyClass"), $model->company->idCompanyClass ? ($model->company->companyClass->name) : '');
            foreach ($cols[1] as $attribute) {
                echo Helpers::reutrnTitleAndValueIfNotNull($model->company->getAttributeLabel($attribute), $model->company->getAttribute($attribute));
            }
            echo Helpers::reutrnTitleAndValueIfNotNull($model->company->getAttributeLabel("url"), $model->company->url ? (CHtml::link($model->company->url, CHtml::normalizeUrl($model->company->url))) : '');
            ?>

        </div>
    </div>


<?php } ?>
<br/>


<h5> Проекты c участием данного клиента </h5>

<?php
$this->mGridViewDataProvider->renderMGridView('project-grid',$model);

?>
