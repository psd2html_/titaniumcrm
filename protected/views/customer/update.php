<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Customers') => array('index'),
    $model->shortName=> array('view','id'=>$model->idCustomer),
    Yii::t('app', 'Update'),
); ?>
<div class="right"><?= CHtml::link("Назад к сводке &raquo;",array('view','id'=>$model->getPrimaryKey()),array('class'=>'button ')) ?></div>
<header> <h3> <?= $model->fullName; ?>  </h3> </header>
<div class="module_content">
<?php
$this->renderPartial('_form', array(
			'model'=>$model));
?>
</div>