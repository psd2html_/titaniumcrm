<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Customers')

); ?>
<header><h3> Клиенты </h3></header>
<div class="indent-bot-20">
    <a class="button " href='<?= $this->createUrl("Customer/create") ?> '>Создать нового клиента</a>
</div>

<?php
$this->mGridViewDataProvider->renderMGridView('customer-admin',false);
?>

