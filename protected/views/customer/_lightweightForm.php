<?php
$form_name='ajax-form';
$form = $this->beginWidget('CActiveForm', array(
    'id' => $form_name,
    'htmlOptions'=>array(
        'onsubmit'=>"sendFormAjax('#$form_name'); return false;",
    ),
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
));

echo $form->errorSummary($model);
?>

<script>
    function sendFormAjax(form) {
        window.dwDialog.uploadForm(form, "<?= Yii::app()->request->getRequestUri() ?>", false);
    }
</script>

<fieldset>
    <legend>Персональная карточка</legend>
    <div class="row full-width">
        <div class="four columns">
            <?php echo $form->labelEx($model, 'firstName'); ?>
            <?php echo $form->textField($model, 'firstName', array('size' => 60, 'maxlength' => 90)); ?>
            <?php echo $form->error($model, 'firstName'); ?>
        </div>
        <div class="four columns">
            <?php echo $form->labelEx($model, 'lastName'); ?>
            <?php echo $form->textField($model, 'lastName', array('size' => 60, 'maxlength' => 90)); ?>
            <?php echo $form->error($model, 'lastName'); ?>
        </div>
        <div class="four columns">
            <?php echo $form->labelEx($model, 'middleName'); ?>
            <?php echo $form->textField($model, 'middleName', array('size' => 60, 'maxlength' => 90)); ?>
            <?php echo $form->error($model, 'middleName'); ?>
        </div>
    </div>


    <div class="row full-width">
        <div class="four columns">
            <?php echo $form->labelEx($model, 'phone'); ?>
            <?php echo $form->textField($model, 'phone', array('size' => 60, 'maxlength' => 90)); ?>
            <?php echo $form->error($model, 'phone'); ?>
        </div>
        <div class="four columns">
            <?php echo $form->labelEx($model, 'mobilePhone'); ?>
            <?php echo $form->textField($model, 'mobilePhone', array('size' => 60, 'maxlength' => 90)); ?>
            <?php echo $form->error($model, 'mobilePhone'); ?>
        </div>
        <div class="four columns">
            <?php echo $form->labelEx($model, 'phone2'); ?>
            <?php echo $form->textField($model, 'phone2', array('size' => 60, 'maxlength' => 90)); ?>
            <?php echo $form->error($model, 'phone2'); ?>
        </div>
    </div>

    <div class="row  full-width">
        <div class="four columns">
            <?php echo $form->labelEx($model, 'email'); ?>
            <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 90)); ?>
            <?php echo $form->error($model, 'email'); ?>
        </div>
        <div class="four columns">
            <?php echo $form->labelEx($model, 'email2'); ?>
            <?php echo $form->textField($model, 'email2', array('size' => 60, 'maxlength' => 32)); ?>
            <?php echo $form->error($model, 'email2'); ?>
        </div>
        <div class="four columns">
            <?php echo $form->labelEx($model, 'skype'); ?>
            <?php echo $form->textField($model, 'skype', array('size' => 60, 'maxlength' => 90)); ?>
            <?php echo $form->error($model, 'skype'); ?>
        </div>
    </div>
    <div class="row  full-width">
        <div class="twelve columns">
            <?php echo $form->labelEx($model, 'address'); ?>
            <?php echo $form->textField($model, 'address', array('size' => 60, 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'address'); ?>
        </div>
    </div>
</fieldset>
<fieldset>

    <?php echo $form->labelEx($model, 'idCompany'); ?>
    <?php
    $attribute = 'idCompany';

    echo $form->hiddenField($model, $attribute);
    $v = $model->getRelated('company');
    $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
        'name' => ('autocomplete_' . get_class($model)),
        'value' => $v ? $v->fullName : '',
        'source' => Yii::app()->createUrl('ajax/companyAjax'),
        'options' => array(
            'select' => "js:function(event, ui) {
                                          $('#" . get_class($model) . "_" . $attribute . "').val(ui.item.id);
                                        }",
            'change' => "js:function(event, ui) {
                                          if (!ui.item) {
                                             $('#" . get_class($model) . "_" . $attribute . "').val('');
                                          }
                                        }",
            'size' => 120,
            'showAnim' => 'fold',
        ),
    ));
    ?>
    <?php echo $form->error($model, 'idCompany'); ?>

    <?php echo $form->labelEx($model, 'job'); ?>
    <?php echo $form->textField($model, 'job', array('size' => 60, 'maxlength' => 255)); ?>
    <?php echo $form->error($model, 'job'); ?>
</fieldset>

<br/>
<?php
echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Cохранить',array('class'=>'button'));
if (!$model->isNewRecord) {
    echo "&nbsp;" . CHtml::link('Удалить', Yii::app()->createUrl("system/confirmDeletion", array(
                "mid" => deletionHelper::D_CUSTOMER,
                "id" => $model->getPrimaryKey()))
            , array('class' => 'alert button del-btn'));
}
$this->endWidget();
?>




