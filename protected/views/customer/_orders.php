<?php if (!$model->isNewRecord) { ?>

        <fieldset>
            <legend>Заказы</legend>
            <div class="indent-bot-15">
                <?php if ($model->orders) { ?>
                    <table>
                        <?php foreach ($model->orders as $o) {
                            ?>
                            <tr>
                                <td>
                                    <h5><?= CHtml::link($o->coreInstrument->name . '-' . $o->coreInstrument->sid, array('order/update', 'id' => $o->idOrder)) ?></h5>
                                    <strong>Дата:</strong>  <?= Yii::app()->dateFormatter->formatDateTime($o->date, 'full', null) ?> <br/>
                                    <?php if ($o->comment) { ?> 
                                        <strong>Комментарий:</strong>  <?= $o->comment ?> <br/>
                                    <?php } ?>
                                    <br/>
                                    <?= CHtml::link('Загрузить DOCX', array("/googleDocs/download/", 'id' => $o->idOrder), array('target' => '_blank')) ?> | <?= CHtml::link('Выгрузить на Google', array("/googleDocs/uploadToGoogleDocs/", 'id' => $o->idOrder), array('target' => '_blank')) ?>
                                </td>
                                <td width="50%">
                                    <?php
                                    $groups = array();
                                    foreach ($o->featuresWithData as $idManagedBlock => $keys) {
                                        $managedBlock = ManagedBlock::model()->findByPk($idManagedBlock);
                                        if ($managedBlock) {
                                            $out = '<strong>' . $managedBlock->comment . '</strong>';
                                            $out.='<ul>';
                                            foreach ($keys as $key) {
                                                $out.='<li>' . $key['name'] . ' - ' . $key['sid'];
                                                if (!$key['description']) {
                                                    $out.=(' <span class="alert round label" title="Нет файла с описанием для данной позиции">!</span>');
                                                }
                                                $out.='</li>';
                                            }
                                            $out.='</ul>';
                                            $groups[$managedBlock->featureGroup->name][] = $out;
                                        }
                                    }
                                    foreach ($groups as $name => $keys) {
                                        ?>
                                        <div class="more">
                                            <h6><?= $name ?></h6>
                                            <?= implode(' ', $keys); ?>
                                        </div>
                                    <? }
                                    ?>

                                </td>
                            </tr>
                        <?php } ?> 
                    </table>
                <?php } ?>
                <a class="button create-new-featuregroup small" href="<?= Yii::app()->createUrl("order/create", array("Order[idCustomer]" => $model->idCustomer)) ?>" >Добавить заказ</a>
            </div>
        </fieldset>

    <?php } ?>