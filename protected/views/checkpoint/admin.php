<?php

$this->breadcrumbs = array(
    Yii::t('app', 'Checkpoints')
); ?>
<header> <h3> Управление  </h3> </header>


<?php $this->widget('MGridView', array(
	'id' => 'checkpoint-grid',
	'dataProvider' => $model->search(),
        'cssFile' => false,
	'template' => '{items} {pager}',
	'columns' => array(
        'name',
        'description',
            array('name'=>'readyState',
                  'value'=>'$data->getReadyStateName()'),
        array('name'=>'startWith',
                  'value'=>'$data->getStartWithName()'),
        array('class' => 'CButtonColumn',
            'template' => '{update}{delete}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options'=>array('class'=>'button secondary tiny delete')
                ),
                'update' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options'=>array('class'=>'button secondary tiny update')
                ),
            ),
        ),
        
	),
)); ?>

 <div class="footer_content">
        <a class="button" href='<?= $this->createUrl("Checkpoint/create")?> '>Создать новый</a>
 </div>