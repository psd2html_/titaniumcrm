<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>
<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'ajax-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
    ));

    echo $form->errorSummary($model);


    $types = Project::getEnumStatusList();
    $types[1000] = 'Без проверки';
    ?>

  

    <?php echo $form->labelEx($model, 'idCheckpointGroup'); ?>
    <?php
    $aa = CheckpointGroup::getGroups(0, true);

    echo $form->dropDownList($model, 'idCheckpointGroup', $aa);
    ?>
    <?php echo $form->error($model, 'idCheckpointGroup'); ?>

    <?php echo $form->labelEx($model, 'name'); ?>
    <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 255)); ?>
    <?php echo $form->error($model, 'name'); ?>

    <?php echo $form->labelEx($model, 'description'); ?>
    <?php echo $form->textArea($model, 'description', array('rows' => 6, 'cols' => 50)); ?>
    <?php echo $form->error($model, 'description'); ?>

    <?php echo $form->labelEx($model, 'startWith'); ?>
    <?php echo $form->dropDownList($model, 'startWith', $types); ?>
    <?php echo $form->error($model, 'startWith'); ?>

    <?php echo $form->labelEx($model, 'readyState'); ?>
    <?php echo $form->dropDownList($model, 'readyState', $types); ?>
<?php echo $form->error($model, 'readyState'); ?>
    <br/><br/>

    <?php
    /* @var $model Checkpoint */
    $model_name = get_class($model);

    echo CHtml::ajaxSubmitButton($model->isNewRecord ? 'Создать' : 'Cохранить', Yii::app()->createUrl(
                    $model->isNewRecord ? $model_name . '/create' : $model_name . '/update/' . $model->getPrimaryKey()
            ), array(
        'type' => 'POST',
        'dataType' => 'json',
        'data' => 'js:$("#ajax-form").serialize()', //this one
        'success' => "js:function(data){
       if(data.result==='success'){
          location.reload();
       }else{
         $('#foundationModal').html(data.msg);
       }
   }",
            ), array('class' => 'button'));
    ?>

    <?php
    if (!$model->isNewRecord) {
        echo "&nbsp;" . CHtml::link('Удалить', Yii::app()->createUrl("system/confirmDeletion", array(
                    "mid" => deletionHelper::D_CHECKPOINT,
                    "id" => $model->getPrimaryKey()))
                , array('class' => 'alert button del-btn'));
    }
    
    $this->endWidget();
    ?>