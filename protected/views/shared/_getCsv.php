<?php
$this->widget('CSVGridView', array(
        'id' => 'csv-grid',
        'dataProvider' => $dataProvider,
        'filter' => $model,
        'readOnly'=>$readOnly,
        'cssFile' => false,
        'template' => '{items} {pager}',
        //'columns' => $columns
    )
);
