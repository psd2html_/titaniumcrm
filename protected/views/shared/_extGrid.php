<?php

$this->widget('MGridView', array(
        'id' => $gridViewId,
        'dataProvider' => $dataProvider,
        'readOnly'=>$readOnly,
        'filter' => $model,
        'cssFile' => false,
        'template' => '{items} {pager}',
        'extended'=>true
        //'columns' => $columns
    )
);
?>
