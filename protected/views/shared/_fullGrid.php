<?php

$this->widget('MGridView', array(
        'id' => $gridViewId,
        'dataProvider' => $dataProvider,
        'filter' => $model,
        'cssFile' => false,
        'template' => '{items} {pager}',
        'extended'=>true,
        'readOnly'=>$readOnly,
        'downloadCSV'=>true,
        'lockableFilters'=>true,
        'context'=>$context,
        //'columns' => $columns

//        'columns' => array(
//            'projectName',
//        ),
    )
);
?>
