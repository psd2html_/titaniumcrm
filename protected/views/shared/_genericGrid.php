<?php
$this->widget('MGridView', array(
    'id' => $gridViewId,
    'dataProvider' => $dataProvider,
    'filter' => $model,
    'readOnly'=>$readOnly,
    'cssFile' => false,
    'template' => '{items} {pager}',
    'columns' => array()
));