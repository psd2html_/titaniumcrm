<?php
$this->widget('CSVGridView', array(
        'id' => 'csv-grid',
        'dataProvider' => $dataProvider,
        'filter' => $model,
        'cssFile' => false,
        'template' => '{items} {pager}',
        'separator'=>';',
        //'columns' => $columns
    )
);
