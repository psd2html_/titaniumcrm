<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Features')
);
?><header> <h3> Управление позициями </h3> </header>
<?php
$p = Permission::getAllowedManufacturers();

if (!$p) {
    ?>
    К сожалению, похоже, что у вас нет разрешенных к просмотру или редактированию позиций.
    <?php
} else {
    
$this->widget('MGridView', array(
    'id' => 'feature-grid',
    'dataProvider' => $model->search(),
    'filter' => $model, // важно!
    'cssFile' => false,
    'template' => '{items} {pager}',
    'columns' => array(
        'sid',
        'name',
        array(
            'name' => 'idManufacturer',
            'value' => '$data->manufacturer->name',
            'filter' => CHtml::listData( $p, "idManufacturer","name"),
        ),
        array(
            'name' => 'hasPrice',
            'header' => Yii::t('app', 'Есть цена'),
            'type' => 'raw',
            'filter' => (array(Feature::NO_DESCRIPTION => Yii::t('app', 'Нет'), Feature::HAS_DESCRIPTION => Yii::t('app', 'Есть'))),
            'value' => '$data->hasPriceLiteral'
        ),
        array(
            'name' => 'hasText',
            'header' => Yii::t('app', 'Есть описание'),
            'type' => 'raw',
            'filter' => (array(Feature::NO_DESCRIPTION => Yii::t('app', 'Нет'), Feature::HAS_DESCRIPTION => Yii::t('app', 'Есть'))),
            'value' => '$data->hasTextLiteral'
        ),
        array('class' => 'CButtonColumn', // блок не важный, настройка колонки с кнопками, к задаче не относится
            'template' => '{update}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options' => array('class' => 'button secondary tiny delete')
                ),
                'update' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options' => array('class' => 'button secondary tiny update')
                ),
            ),
        ),
    ),
));
?>

<div class="footer_content">
    <a class="button" href='<?= $this->createUrl("Feature/create") ?> '>Создать новую позицию</a>
</div>
<?php } ?>
