<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>
<div class="form">
    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span
            class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'feature-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
        'enableClientValidation' => true,
    ));

    if (Permission::isLoggedUserSuperadmin()||$model->isNewRecord)
    { $rights=true; $strict_rights=true; }
    else
    {
        $strict_rights=false;
    $rights=Permission::isManufacturerAllowed($model->idManufacturer);
    }

    echo $form->errorSummary($model);
    ?>

    <div class="row">
        <div class="nine columns">


            <div class="row">
                <div class="six columns">
                    <?php echo $form->labelEx($model, 'sid'); ?>
                    <?php echo $form->textField($model, 'sid', $model->isNewRecord ? array() : array('disabled' => 'true')); ?>
                    <?php echo $form->error($model, 'sid'); ?>


                </div>
                <div class="six columns">

                    <?php echo $form->labelEx($model, 'idManufacturer'); ?>
                    <?php echo $form->dropDownList($model, 'idManufacturer', CHtml::listData(Manufacturer::model()->findAll(), "idManufacturer", "name"),
                        ($strict_rights)?array():array('disabled'=>'disabled')); ?>
                    <?php echo $form->error($model, 'idManufacturer'); ?>


                </div>
            </div>
            <div class="row">
                <div class="six columns">

                    <?php echo $form->labelEx($model, 'name'); ?>
                    <?php echo $form->textField($model, 'name', ($rights)?array():array('disabled'=>'disabled')); ?>
                    <?php echo $form->error($model, 'name'); ?>
                </div>
                <div class="six columns">


                    <?php echo $form->labelEx($model, 'ruName'); ?>
                    <?php echo $form->textField($model, 'ruName', ($rights)?array():array('disabled'=>'disabled')); ?>
                    <?php echo $form->error($model, 'ruName'); ?>
                </div>
            </div>
            <fieldset>
                <legend>Цена</legend>
                <div class="indent-bot-15">
                    <?php echo $form->dropDownList($model, 'isPriceSettled', array(0 => 'Цена неопределена', '1' => 'Использовать следующую цену для позиции')); ?>
                    <?php echo $form->error($model, 'isPriceSettled'); ?>
                </div>
                <div class="row">

                    <div class="four columns">
                        <?php echo $form->labelEx($model, 'price'); ?>
                        <?php echo $form->textField($model, 'price', array('size' => 60, 'maxlength' => 255, 'onkeypress' => '$("#Feature_isPriceSettled").val(1);')); ?>
                        <?php echo $form->error($model, 'price'); ?>
                    </div>
                    <div class="two columns">
                        <?php echo $form->labelEx($model, 'enumCurrency'); ?>
                        <?php echo $form->dropDownList($model, 'enumCurrency', Order::getCurrencies()); ?>
                        <?php echo $form->error($model, 'enumCurrency'); ?>
                    </div>
                    <div class="six columns">
                    </div>

                </div>

            </fieldset>
        </div>
        <div class="three columns">
            <div class="indent-bot-15">
                <?php echo CHtml::label('Фотография', ''); ?>
                <?php
                if (!$model->isNewRecord) {
                    $img = $model->getThumbnail(false, true);
                    if ($img) {
                        echo CHtml::image($img, 'Картинка', array('class' => 'indent-bot-10'));
                    }
                }
                ?>
                <?php
                echo $form->fileField($model, 'attached_image');
                echo $form->error($model, 'attached_image');
                ?>
            </div>
        </div>
    </div>

    <?php echo $form->labelEx($model, 'description'); ?>
    <?php
        $this->widget('ext.niceditor.nicEditorWidget', array(
        "model" => $model, // Data-Model
        "attribute" => 'description', // Attribute in the Data-Model
        "config" => array("maxHeight" => "400"),
    ));
    ?>
    <?php echo $form->error($model, 'description'); ?>
    <br/>
    <?php echo $form->labelEx($model, 'ruDescription'); ?>
    <?php
    $this->widget('ext.niceditor.nicEditorWidget', array(
        "model" => $model, // Data-Model
        "attribute" => 'ruDescription', // Attribute in the Data-Model
        "config" => array("maxHeight" => "400"),
    ));
    ?>
    <?php echo $form->error($model, 'ruDescription'); ?>
    <br/>
    <?php if (!$rights) { ?>
        <script>
            $(document).ready(
                function() {
                    jQuery('.nicEdit-main').attr('contenteditable','false');
                    jQuery('.nicEdit-panel').hide();
                }
            )
        </script>
    <?php } ?>
    <?php
    echo CHtml::submitButton(Yii::t('app', 'Save'), array('class' => 'button'));

    if (!$model->isNewRecord) {
        echo "&nbsp;" . CHtml::link('Удалить', Yii::app()->createUrl("system/confirmDeletion", array(
                    "mid" => deletionHelper::D_FEATURE,
                    "id" => $model->getPrimaryKey()))
                , array('class' => 'alert button del-btn'));
    }
    $this->endWidget();
    ?>
</div> <!-- form -->

