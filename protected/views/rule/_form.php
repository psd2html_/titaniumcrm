<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>
<div class="form">
    <p class="note">
        <?php echo Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required');?>.
    </p>

    <?php
    $form=$this->beginWidget('CActiveForm', array(
    'id'=>'permission-form',
    'enableAjaxValidation'=>false,
    'enableClientValidation'=>true,
    ));

    echo $form->errorSummary($model);
    ?>
    
        
            <?php echo $form->labelEx($model,'idPermission'); ?>
            <?php echo $form->textField($model,'idPermission'); ?>
            <?php echo $form->error($model,'idPermission'); ?>
         
        
        
            <?php echo $form->labelEx($model,'enumPermissionType'); ?>
            <?php echo $form->textField($model,'enumPermissionType',array('size'=>45,'maxlength'=>45)); ?>
            <?php echo $form->error($model,'enumPermissionType'); ?>
         
        
        
            <?php echo $form->labelEx($model,'idUser'); ?>
            <?php echo $form->textField($model,'idUser'); ?>
            <?php echo $form->error($model,'idUser'); ?>
         
        
        
            <?php echo $form->labelEx($model,'idManufacturer'); ?>
            <?php echo $form->textField($model,'idManufacturer'); ?>
            <?php echo $form->error($model,'idManufacturer'); ?>
         
        
        
            <?php echo $form->labelEx($model,'idCoreInstrumentClass'); ?>
            <?php echo $form->textField($model,'idCoreInstrumentClass'); ?>
            <?php echo $form->error($model,'idCoreInstrumentClass'); ?>
         
            <?php
        echo CHtml::submitButton(Yii::t('app', 'Save'),array('class'=>'button'));
$this->endWidget(); ?>
</div> <!-- form -->

