<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Permissions') 
    
); ?><header> <h3> Управление  </h3> </header>


<?php $this->widget('MGridView', array(
	'id' => 'permission-grid',
	'dataProvider' => $model->search(),
        'cssFile' => false,
	'template' => '{items} {pager}',
	'columns' => array(
        'idPermission',
        'enumPermissionType',
        array(
                			'name' => 'idUser',
                                  ),
        array(
                			'name' => 'idManufacturer',
                                  ),
        array(
                			'name' => 'idCoreInstrumentClass',
                                  ),
array('class' => 'CButtonColumn',
            'template' => '{update}{delete}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options'=>array('class'=>'button secondary tiny delete')
                ),
                'update' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options'=>array('class'=>'button secondary tiny update')
                ),
            ),
        ),
        
	),
)); ?>

 <div class="footer_content">
        <a class="button" href='<?= $this->createUrl("Permission/create")?> '>Создать новый</a>
 </div>