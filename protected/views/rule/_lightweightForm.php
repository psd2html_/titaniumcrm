<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>

<div class="row full-width">
    <div class="twelve columns">
        <h4><?= $model->isNewRecord ? 'Создать новое правило' : 'Редактировать правило' ?></h4>

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'ajax-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
        ));

        echo $form->errorSummary($model);
        ?>

        <?php echo $form->hiddenField($model, 'idUser'); ?>
        <script>
            /**
             * refresh
             */


            function refreshList() {
                $.ajax({
                    url: "<?= Yii::app()->createUrl('ajax/getCoreInstrumentClasses') ?>",
                    data: {'idManufacturer': $('#Permission_idManufacturer').val(), 'all':1},
                    type: "POST",
                    success: function(e) {
                        $('#Permission_idCoreInstrumentClass').html(e);
                    }
                });
            }
        </script>

        <div class="row full-width">

            <div class="six columns">
                <?php
                $manufacturers = Manufacturer::model()->findAll();
                $cclRaw = $model->idManufacturer ? $model->manufacturer->coreInstrumentClasses : $manufacturers[0]->coreInstrumentClasses;
                
                $ccl[0] = "Все";
                $ccl = $ccl+ CHtml::listData($cclRaw, "idCoreInstrumentClass", "name");
                
                ?>
                <?php echo $form->labelEx($model, 'idManufacturer'); ?>
                <?php echo $form->dropDownList($model, 'idManufacturer', CHtml::listData($manufacturers, "idManufacturer", "name"), array('onchange' => 'refreshList();', 'onkeyup' => 'refreshList();')); ?>
                <?php echo $form->error($model, 'idManufacturer'); ?>
            </div>
            <div class="six columns indent-bot-15 ">
                <?php echo $form->labelEx($model, 'idCoreInstrumentClass'); ?>
                <?php echo $form->dropDownList($model, 'idCoreInstrumentClass', $ccl); ?>
                <?php echo $form->error($model, 'idCoreInstrumentClass'); ?>
            </div>
        </div>

        <?php
        echo CHtml::ajaxSubmitButton($model->isNewRecord ? 'Создать' : 'Cохранить', Yii::app()->createUrl(
                        $model->isNewRecord ? 'rule/create' : 'rule/update/' . $model->getPrimaryKey()
                ), array(
            'type' => 'POST',
            'dataType' => 'json',
            'data' => 'js:$("#ajax-form").serialize()', //this one
            'beforeSend'=>"js:function() {  $('#foundationModal input[type=submit]').hide() }",
            'success' => "js:function(data){
            if(data.result==='success'){
                location.reload();
            }else{
                $('#foundationModal').html(data.msg);
            }
        }",
                ), array('class' => 'button'));

        $this->endWidget();
        ?>

    </div>
</div> <!-- form -->

