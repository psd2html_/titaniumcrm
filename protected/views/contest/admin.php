<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Contests') 
    
); ?><header> <h3> Управление  </h3> </header>


<?php $this->widget('MGridView', array(
	'id' => 'contest-grid',
	'dataProvider' => $model->search(),
        'cssFile' => false,
	'template' => '{items} {pager}',
	'columns' => array(
        array(
                			'name' => 'idProject',
                                        'value' => 'isset($data->idProject0->name)?$data->idProject0->name:"N/A"'
                ),
        'contestUrl',
        'contestSiteUrl',
        'costLot',
        'costAsk',
        'costContract',
array('class' => 'CButtonColumn',
            'template' => '{update}{delete}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options'=>array('class'=>'button secondary tiny delete')
                ),
                'update' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options'=>array('class'=>'button secondary tiny update')
                ),
            ),
        ),
        
	),
)); ?>

 <div class="footer_content">
        <a class="button" href='<?= $this->createUrl("Contest/create")?> '>Создать новый</a>
 </div>