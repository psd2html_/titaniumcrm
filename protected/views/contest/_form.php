<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>
<div class="form">
    <p class="note">
        <?php echo Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required');?>.
    </p>

    <?php
    $form=$this->beginWidget('CActiveForm', array(
    'id'=>'contest-form',
    'enableAjaxValidation'=>false,
    'enableClientValidation'=>true,
    ));

    echo $form->errorSummary($model);
    ?>
    
        <fieldset>
            <?php echo $form->labelEx($model,'idProject'); ?>
            <?php echo $form->dropDownList($model, 'idProject0', CHtml::listData(Project::model()->findAll(),'idProject', 'name')); ?>
            <?php echo $form->error($model,'idProject'); ?>
         </fieldset>
        
        <fieldset>
            <?php echo $form->labelEx($model,'contestUrl'); ?>
            <?php echo $form->textField($model,'contestUrl',array('size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->error($model,'contestUrl'); ?>
         </fieldset>
        
        <fieldset>
            <?php echo $form->labelEx($model,'contestSiteUrl'); ?>
            <?php echo $form->textField($model,'contestSiteUrl',array('size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->error($model,'contestSiteUrl'); ?>
         </fieldset>
        
        <fieldset>
            <?php echo $form->labelEx($model,'costLot'); ?>
            <?php echo $form->textField($model,'costLot'); ?>
            <?php echo $form->error($model,'costLot'); ?>
         </fieldset>
        
        <fieldset>
            <?php echo $form->labelEx($model,'costAsk'); ?>
            <?php echo $form->textField($model,'costAsk'); ?>
            <?php echo $form->error($model,'costAsk'); ?>
         </fieldset>
        
        <fieldset>
            <?php echo $form->labelEx($model,'costContract'); ?>
            <?php echo $form->textField($model,'costContract'); ?>
            <?php echo $form->error($model,'costContract'); ?>
         </fieldset>
            <?php
        echo CHtml::submitButton(Yii::t('app', 'Save'),array('class'=>'button'));
$this->endWidget(); ?>
</div> <!-- form -->

