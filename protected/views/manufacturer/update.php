<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Manufacturers') => array('index'),
    Yii::t('app', 'Update'),
); ?>
<?php if ($model->company) {?>
    <div class="right">
        <?= CHtml::link("Перейти к контактной информации",array('company/view','id'=>$model->company->idCompany),array('class'=>'button ')) ?>
    </div>
<?php } ?>
<header> <h3> <?= $model->name; ?> </h3> </header>


<div class="module_content">
<?php
$this->renderPartial('_form', array(
			'model'=>$model));
?>
</div>