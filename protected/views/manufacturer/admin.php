<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Manufacturers')
);
?><header> <h3> Управление производителями оборудования  </h3> </header>


<?php
$this->widget('MGridView', array(
    'id' => 'manufacturer-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'cssFile' => false,
    'template' => '{items} {pager}',
    'columns' => array(
        'name',
        'abbr',
        array('class' => 'CButtonColumn',
            'template' => '{update}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options' => array('class' => 'button secondary tiny delete')
                ),
                'update' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options' => array('class' => 'button secondary tiny update')
                ),
            ),
        ),
    ),
));
?>

<div class="footer_content">
    <a class="button" href='<?= $this->createUrl("Manufacturer/create") ?> '>Создать нового производителя оборудования</a>
</div>