<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>

<dl class="tabs">
    <dd class="active"><a href="#main">Настройки</a></dd>
    <?php if (!$model->isNewRecord) { ?>
        <dd><a href="#logo">Редактор логотипа</a></dd>
    <?php } ?>
</dl>

<ul class="tabs-content">
    <?php if (!$model->isNewRecord) { ?>
        <li id="logoTab">
            <fieldset>
                <legend>Картинка</legend>

                <?php
                $this->widget("ext.hasImage.processingTool", array('model' => $model));
                ?>
                <br/>
            </fieldset>
        </li>
    <?php } ?>
    <li class="active" id="mainTab">
        <div class="form">
            <p class="note">
                <?php echo Yii::t('app', 'Fields with'); ?> <span
                    class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
            </p>

            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'manufacturer-form',
                'enableAjaxValidation' => false,
                'enableClientValidation' => true,
            ));

            echo $form->errorSummary($model);
            ?>


            <?php echo $form->labelEx($model, 'name'); ?>
            <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 99)); ?>
            <?php echo $form->error($model, 'name'); ?>

            <?php echo $form->labelEx($model, 'abbr'); ?>
            <?php echo $form->textField($model, 'abbr', array('size' => 45, 'maxlength' => 10)); ?>
            <?php echo $form->error($model, 'abbr'); ?>

            <?php echo $form->labelEx($model, 'motto'); ?>
            <?php echo $form->textArea($model, 'motto', array('row' => 8)); ?>
            <?php echo $form->error($model, 'motto'); ?>


            <?php if (!$model->isNewRecord) { ?>
                <fieldset>
                    <legend>Классы приборов от данного производителя</legend>

                    <?php
                    $this->widget('ext.relation.relation', array(
                        'model' => $model,
                        'widgetType' => 0,
                        'relation' => 'coreInstrumentClasses',
                        'fields' => 'name',
                        'createUrl' => Yii::app()->createUrl("coreInstrumentClass/create", array(
                            'actionParams' => array('returnUrl' => Yii::app()->request->getRequestUri()))),
                    ));
                    ?>
                </fieldset>
            <?php } ?>

            <?php
            echo CHtml::submitButton(Yii::t('app', 'Save'), array('class' => 'button'));
            if (!$model->isNewRecord) {
                echo "&nbsp;" . CHtml::link('Удалить', Yii::app()->createUrl("system/confirmDeletion", array(
                            "mid" => deletionHelper::D_MANUFACTURER,
                            "id" => $model->getPrimaryKey()))
                        , array('class' => 'alert button del-btn'));
            }
            $this->endWidget();
            ?>
        </div>
        <!-- form -->

    </li>
</ul>