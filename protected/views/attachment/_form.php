<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>
<div class="form">
    <p class="note">
        <?php echo Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required');?>.
    </p>

    <?php
    $form=$this->beginWidget('CActiveForm', array(
    'id'=>'attachment-form',
    'enableAjaxValidation'=>false,
    'enableClientValidation'=>true,
         'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));

    echo $form->errorSummary($model);
    ?>
    
        <fieldset>
            
            <?php echo $form->hiddenField($model, 'belongType'); ?>
            <?php echo $form->hiddenField($model, 'belongId'); ?>
        
            <?php if ($model->isNewRecord) { ?>
            
            <?php echo $form->labelEx($model,'attached_file'); ?>
            <?php echo $form->fileField($model, 'attached_file'); ?>
            <?php echo $form->error($model,'attached_file'); ?>
            
            
            <?php } else { 
            echo $model->getOriginalFilenameWithLink(false)    ;
            }
            ?>
            
            <br/><br/>
            
            <?php echo $form->labelEx($model,'title'); ?>
            <?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->error($model,'title'); ?>
            
         </fieldset>
            <?php
        echo CHtml::submitButton(Yii::t('app', 'Save'),array('class'=>'button'));
$this->endWidget(); ?>
</div> <!-- form -->

