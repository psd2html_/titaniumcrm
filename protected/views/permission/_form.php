<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>
<?php Yii::app()->clientScript->registerScriptFile("/js/revealActivate.js", CClientScript::POS_END); ?>
<div class="form">
    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span
            class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <?php
    $form = $this->beginWidget('ext.foundation.widgets.FounActiveForm', array(
        'id' => 'user-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
    ));

    echo $form->errorSummary($model);
    ?>

    <fieldset>
        <legend>Права пользователя (<?= $model->group ?>)</legend>
        <div class="row">
            <div class="four columns">

                <?php echo $form->checkBoxRow($model, 'superuser'); ?>

                <div class="row">
                    <div class="six columns">
                        <?php echo $form->dropDownListRow($model, 'idGroup', Group::getAllGroups()); ?>
                        <?php echo $form->textFieldRow($model, 'abbr', array('size' => 45, 'maxlength' => 10)); ?>
                    </div>
                </div>



                <?php echo $form->error($model, 'abbr'); ?>
                <br/>
                <br/>

            </div>
            <div class="eight columns indent-bot-15">


                <?php
                $m = new Permission();
                $m->unsetAttributes();
                $m->idUser = $model->id;

                $dp = $m->search();

                $dp->pagination->route = Yii::app()->request->getPathInfo();
                $this->renderPartial('_rules', array('dataProvider' => $dp, 'model' => $m));
                ?></div>
        </div>
    </fieldset>


    <?php
    echo CHtml::submitButton(Yii::t('app', 'Save'), array('class' => 'button'));
    $this->endWidget();
    ?>
</div> <!-- form -->

