<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Users') 
    
); ?><header> <h3> Управление пользователями </h3> </header>


<?php $this->widget('MGridView', array(
	'id' => 'user-grid',

    'filter'=>$model,
	'dataProvider' => $model->search(),
    'cssFile' => false,

	'template' => '{items} {pager}',
	'columns' => array(
        'username',
         'abbr',
        'email',
        'create_at',
        'lastvisit_at',
        array(
            'filter'=>User::getAllGroups(),
            'name'=>'group',
            'value'=>'$data->groupName',
            ),
        array(
            'filter'=>User::getAllGroups(),
            'name'=>'idGroup',
            'value'=>'$data->relatedGroup?$data->relatedGroup->name:"-"',
            ),
        /*
        array('name'=>'status',
            'value'=>'CHtml::image(Helpers::getSemaphoreImage($data->status))',
            'type' => 'raw',
            ),    */
        
array('class' => 'CButtonColumn',
            'template' => '{update}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options'=>array('class'=>'button secondary tiny delete')
                ),
                'update' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options'=>array('class'=>'button secondary tiny update')
                ),
            ),
        ),
        
	),
)); ?>

 