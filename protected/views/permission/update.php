<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Users') => array('index'),
    Yii::t('app', 'Update'),
); ?>
<header> <h3> Редактировать правило </h3> </header>
<div class="module_content">
<?php
$this->renderPartial('_form', array(
			'model'=>$model));
?>
</div>