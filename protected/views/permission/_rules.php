<h4>Разрешения на фирмы и классы</h4>
<p>Внимание, с глобального обновления системы аутенитфикации в f8-f9, правила только разрешающие. </p>
<?php

$this->widget('ext.relation.relation', array(
    'dataProvider' => $dataProvider,
    'model' => $model,
    //'filter' => $model,
    'widgetType' => 5,
    'relation' => 'permissionRules',
    'fields' => array(
        array(
            'name' => 'idManufacturer',
            'value' => '$data->manufacturer?$data->manufacturer->name:"N/A"',
        ),
        array(
            'name' => 'idCoreInstrumentClass',
            'value' => '$data->coreInstrumentClass?$data->coreInstrumentClass->name:"Все"',
            'filter' =>''
        ),
        
    ),
    'controllerName'=>'rule',
    'foundationModalTrigger'=>true,
    'showCreate' => false,
    'actionParams' => array('Permission[idUser]' => $model->idUser,
        'returnUrl' => Yii::app()->request->getRequestUri()),
));
?>

<?=

CHtml::link('Создать новое правило', array('rule/create', 'Permission[idUser]' => $model->idUser,
    'returnUrl' => Yii::app()->request->getRequestUri()), array('class' => ' button small secondary foundationModalTrigger'))
?>


