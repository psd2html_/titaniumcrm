<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>
<div class="form">
    <p class="note">
        <?php echo Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required');?>.
    </p>

    <?php
    $form=$this->beginWidget('CActiveForm', array(
    'id'=>'user-form',
    'enableAjaxValidation'=>false,
    'enableClientValidation'=>true,
    ));

    echo $form->errorSummary($model);
    ?>
    
        
            <?php echo $form->labelEx($model,'username'); ?>
            <?php echo $form->textField($model,'username',array('size'=>20,'maxlength'=>20)); ?>
            <?php echo $form->error($model,'username'); ?>
         
        
        
            <?php echo $form->labelEx($model,'password'); ?>
            <?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>128)); ?>
            <?php echo $form->error($model,'password'); ?>
         
        
        
            <?php echo $form->labelEx($model,'email'); ?>
            <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
            <?php echo $form->error($model,'email'); ?>
         
        
        
            <?php echo $form->labelEx($model,'activkey'); ?>
            <?php echo $form->textField($model,'activkey',array('size'=>60,'maxlength'=>128)); ?>
            <?php echo $form->error($model,'activkey'); ?>
         
        
        
            <?php echo $form->labelEx($model,'create_at'); ?>
            <?php 
                    $this->widget('ext.proDate.proDate', array(
                    'model' => $model,
                    'attribute' => 'create_at',
                    'htmlOptions' => array('class' => 'foundation-date-picker')
                ));; ?>
            <?php echo $form->error($model,'create_at'); ?>
         
        
        
            <?php echo $form->labelEx($model,'lastvisit_at'); ?>
            <?php 
                    $this->widget('ext.proDate.proDate', array(
                    'model' => $model,
                    'attribute' => 'lastvisit_at',
                    'htmlOptions' => array('class' => 'foundation-date-picker')
                ));; ?>
            <?php echo $form->error($model,'lastvisit_at'); ?>
         
        
        
            <?php echo $form->labelEx($model,'superuser'); ?>
            <?php echo $form->textField($model,'superuser'); ?>
            <?php echo $form->error($model,'superuser'); ?>
         
        
        
            <?php echo $form->labelEx($model,'status'); ?>
            <?php echo $form->textField($model,'status'); ?>
            <?php echo $form->error($model,'status'); ?>
         
        
        
            <?php echo $form->labelEx($model,'serializedPermissions'); ?>
            <?php echo $form->textField($model,'serializedPermissions',array('size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->error($model,'serializedPermissions'); ?>
         
        <?php  echo CHtml::ajaxSubmitButton($model->isNewRecord ? 'Создать' : 'Cохранить', Yii::app()->createUrl(
                    $model->isNewRecord ? $model_name . '/create' : $model_name . '/update/' . $model->getPrimaryKey()
            ), array(
        'type' => 'POST',
        'dataType' => 'json',
        'data' => 'js:$("#ajax-form").serialize()', //this one
        'success' => "js:function(data){
            if(data.result==='success'){
                location.reload();
            }else{
                $('#foundationModal').html(data.msg);
            }
        }",
            ), array('class' => 'button'));
    
    $this->endWidget();
    ?>
    
    
</div> <!-- form -->

