<?php
if ($model->isManufacturerDescription) {
    $this->breadcrumbs = array(
        Yii::t('app', 'Companies')=>"/company/index",
        "Производители оборудования"=>"/company/indexManufacturers",
        $model->shortName=> array('view','id'=>$model->idCompany),
        Yii::t('app', 'Update'),
    );
} else {
$this->breadcrumbs = array(
    Yii::t('app', 'Companies') => array('index'),
        $model->shortName=> array('view','id'=>$model->idCompany),
    Yii::t('app', 'Update'),
);
}?>
<div class="right"><?= CHtml::link("Назад к сводке &raquo;",array('view','id'=>$model->idCompany),array('class'=>'button ')) ?></div>
<header> <h3> <?= $model->fullName; ?> </h3> </header>


<div class="module_content">
<?php
$this->renderPartial('_form', array(
			'model'=>$model));
?>
</div>