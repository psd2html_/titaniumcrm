<?php
$form_name = 'sticky-ajax-form';
$form = $this->beginWidget('ext.foundation.widgets.FounActiveForm', array(
    'id' => $form_name,
    'htmlOptions' => array(
        'onsubmit' => "sendFormAjax('#$form_name'); return false;",
    ),
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
));

?>

<script>
    function sendFormAjax(form) {
        window.dwDialog.uploadForm(form, "<?= Yii::app()->request->getRequestUri() ?>", false);
    }
</script>

<fieldset>
    <?php if ($model->isNewRecord) { ?>
        <legend>Создать компанию</legend>
    <?php } ?>
    <?php echo $form->errorSummary($model); ?>
    <div class="row full-width">
        <div class="six columns">
            <?php echo $form->labelEx($model, 'fullName'); ?>
            <?php echo $form->textField($model, 'fullName', array('size' => 60, 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'fullName'); ?>
            <?php echo $form->labelEx($model, 'shortName'); ?>
            <?php echo $form->textField($model, 'shortName', array('size' => 45, 'maxlength' => 64)); ?>
            <?php echo $form->error($model, 'shortName'); ?>
            <?php echo $form->labelEx($model, 'abbr'); ?>
                <?php echo $form->textField($model, 'abbr', array('size' => 45, 'maxlength' => 10)); ?>
                <?php echo $form->error($model, 'abbr'); ?>
        </div>
        <div class="six columns">

            <?php echo $form->labelEx($model, 'city'); ?>
            <?php echo $form->textField($model, 'city', array('size' => 45, 'maxlength' => 45)); ?>
            <?php echo $form->error($model, 'city'); ?>

            <div class="indent-bot-15">
                <?php echo $form->labelEx($model, 'idCompanyClass'); ?>
                <?php echo $form->dropDownList($model, 'idCompanyClass', CHtml::listData(CompanyClass::model()->findAll(), "idCompanyClass", "name")); ?>
                <?php echo $form->error($model, 'idCompanyClass'); ?>
            </div>

        </div>
    </div>
    <div class="row full-width">
        <div class="twelve columns">
            <?php echo $form->labelEx($model, 'physicalAddress'); ?>
            <?php echo $form->textField($model, 'physicalAddress', array('size' => 60, 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'physicalAddress'); ?>

            <?php echo $form->labelEx($model, 'legalAddress'); ?>
            <?php echo $form->textField($model, 'legalAddress', array('size' => 60, 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'legalAddress'); ?>
        </div>
    </div>
    <div class="row full-width">
        <div class="six columns"><?php echo $form->labelEx($model, 'url'); ?>
            <?php echo $form->textField($model, 'url', array('size' => 60, 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'url'); ?>
            <?php echo $form->labelEx($model, 'email'); ?>
            <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 64)); ?>
            <?php echo $form->error($model, 'email'); ?>
        </div>
        <div class="six columns">
            <?php echo $form->labelEx($model, 'phone'); ?>
            <?php echo $form->textField($model, 'phone', array('size' => 45, 'maxlength' => 45)); ?>
            <?php echo $form->error($model, 'phone'); ?>

            <?php echo $form->labelEx($model, 'addPhone'); ?>
            <?php echo $form->textField($model, 'addPhone', array('size' => 45, 'maxlength' => 45)); ?>
            <?php echo $form->error($model, 'addPhone'); ?>
        </div>
    </div>

</fieldset>

<?php
echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Cохранить'
    , array('class' => 'button'));?>

<?php
$this->endWidget();
?>


