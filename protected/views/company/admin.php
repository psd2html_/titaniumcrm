<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Companies')
);
?><header> <h3> Управление организациями </h3> </header>
<div class="indent-bot-20">
    <a class="button" href='<?= $this->createUrl("Company/create") ?> '>Создать новую организацию</a>
</div>

<?php
$this->widget('MGridView', array(
    'id' => 'company-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'cssFile' => false,
    'template' => '{items} {pager}',
    'columns' => array(

        array('class' => 'CButtonColumn',
            'template' => '{view}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options' => array('class' => 'button secondary tiny delete')
                ),
                'view' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options' => array('class' => 'button secondary tiny view')
                ),
            ),
        ),
    ),
));
?>

