<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Companies')=>"/company/index",
    "Производители оборудования"
);
?><header> <h3> Контакты производителей оборудования</h3> </header>


<?php
$this->widget('MGridView', array(
    'id' => 'company-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'cssFile' => false,
    'template' => '{items} {pager}',
    'columns' => array(

        array('class' => 'CButtonColumn',
            'template' => '{view}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options' => array('class' => 'button secondary tiny delete')
                ),
                'view' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options' => array('class' => 'button secondary tiny view')
                ),
            ),
        ),
    ),
));
?>

