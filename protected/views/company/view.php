<?php Yii::app()->clientScript->registerScriptFile("/js/revealActivate.js", CClientScript::POS_END); ?>

<?php
if ($model->isManufacturerDescription) {
    $this->breadcrumbs = array(
        Yii::t('app', 'Companies')=>"/company/index",
        "Производители оборудования"=>"/company/indexManufacturers",
        $model->shortName,
    );
} else {
$this->breadcrumbs = array(
    Yii::t('app', 'Companies') => array('index'),
    $model->shortName,
);
}
/* @var $model Customer */


$cols = array(
    array('shortName', 'city', 'phone', 'addPhone', 'abbr'),
    array('physicalAddress', 'legalAddress'),
);
?>

<h3> <?= (Group::getPermissionsUponZone(Group::ZONE_COMPANY,Group::ACTION_UPDATE,$model)) ? CHtml::link($model->fullName . ' ' . CHtml::image(Yii::app()->getBaseUrl() . '/images/pencil.png', 'Редактировать'), array('company/update', 'id' => $model->idCompany)) : $model->fullName ?>  </h3>

<div class="row">
    <div class="six columns">
        <?php
        foreach ($cols[0] as $attribute) {
            echo Helpers::reutrnTitleAndValueIfNotNull($model->getAttributeLabel($attribute), $model->getAttribute($attribute));
        }
        ?>
    </div>
    <div class="six columns">
        <?php
        echo Helpers::reutrnTitleAndValueIfNotNull($model->getAttributeLabel("idCompanyClass"), $model->idCompanyClass ? ($model->companyClass->name) : '');
        foreach ($cols[1] as $attribute) {
            echo Helpers::reutrnTitleAndValueIfNotNull($model->getAttributeLabel($attribute), $model->getAttribute($attribute));
        }
        echo Helpers::reutrnTitleAndValueIfNotNull($model->getAttributeLabel("url"), $model->url ? (CHtml::link($model->url, CHtml::normalizeUrl($model->url))) : '');
        ?>

    </div>
</div>
<?php if (($model->comment)) { ?>
    <fieldset>
        <legend>Комментарий</legend>
        <?= Helpers::getProcessedText($model->comment) ?><br/><br/>
    </fieldset>
<?php } ?>
<?php if (!$model->isManufacturerDescription) { ?>
<br/>
<div class="indent-bot-15">
    <?= CHtml::link('Создать проект для ' . $model->shortName, array('/project/create', 'Project[idCompanyConsumer]' => $model->idCompany), array('class' => 'button secondary')) ?>
</div>
<?php } ?>
<br/>
<h5> Сотрудники данной организации </h5>

<div class="indent-bot-20">
    <a class="button dwDialogTrigger" forceReload="true" target="_blank" href='<?= $this->createUrl("Customer/create",array('Customer[idCompany]'=>$model->idCompany)) ?> '>Создать нового сотрудника</a>
</div>

<?php
$this->mGridViewDataProvider->renderMGridView('customer-grid',$model);
?>

<?php if (!$model->isManufacturerDescription) { ?>
<h5> Проекты c участием данной организации </h5>

<?php
$this->mGridViewDataProvider->renderMGridView('company-view-project-grid',$model);
?>
<?php } else { ?>
    <h5> Проекты c участием данного производителя</h5>
    <?php
    $this->mGridViewDataProvider->renderMGridView('project-grid-manufacturer',$model);
    ?>
<?php } ?>