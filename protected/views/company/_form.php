<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>
    <div class="form">
        <p class="note">
            <?php echo Yii::t('app', 'Fields with'); ?> <span
                class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
        </p>

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'company-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
        ));

        echo $form->errorSummary($model);
        ?>


        <fieldset>

            <div class="row">
                <div class="six columns">
                    <?php echo $form->labelEx($model, 'fullName'); ?>
                    <?php echo $form->textField($model, 'fullName', array('size' => 60, 'maxlength' => 255)); ?>
                    <?php echo $form->error($model, 'fullName'); ?>


                    <?php echo $form->labelEx($model, 'shortName'); ?>
                    <?php echo $form->textField($model, 'shortName', array('size' => 45, 'maxlength' => 64)); ?>
                    <?php echo $form->error($model, 'shortName'); ?>

                    <?php echo $form->labelEx($model, 'abbr'); ?>
                    <?php echo $form->textField($model, 'abbr', array('size' => 45, 'maxlength' => 10)); ?>
                    <?php echo $form->error($model, 'abbr'); ?>
                </div>
                <div class="six columns">
                    <?php echo $form->labelEx($model, 'fullNameEng'); ?>
                    <?php echo $form->textField($model, 'fullNameEng', array('size' => 60, 'maxlength' => 255)); ?>
                    <?php echo $form->error($model, 'fullNameEng'); ?>

                    <?php echo $form->labelEx($model, 'city'); ?>
                    <?php echo $form->textField($model, 'city', array('size' => 45, 'maxlength' => 45)); ?>
                    <?php echo $form->error($model, 'city'); ?>

                    <?php echo $form->labelEx($model, 'cityGenetivus'); ?>
                    <?php echo $form->textField($model, 'cityGenetivus', array('size' => 45, 'maxlength' => 45)); ?>
                    <?php echo $form->error($model, 'cityGenetivus'); ?>

                    <div class="indent-bot-15">
                        <?php echo $form->labelEx($model, 'idCompanyClass'); ?>
                        <?php echo $form->dropDownList($model, 'idCompanyClass', CHtml::listData(CompanyClass::model()->findAll(), "idCompanyClass", "name")); ?>
                        <?php echo $form->error($model, 'idCompanyClass'); ?>
                    </div>
                </div>
            </div>

        </fieldset>
        <fieldset>
            <div class="row">
                <div class="six columns"><?php echo $form->labelEx($model, 'url'); ?>
                    <?php echo $form->textField($model, 'url', array('size' => 60, 'maxlength' => 255)); ?>
                    <?php echo $form->error($model, 'url'); ?>
                    <?php echo $form->labelEx($model, 'email'); ?>
                    <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 64)); ?>
                    <?php echo $form->error($model, 'email'); ?>
                </div>
                <div class="six columns">
                    <?php echo $form->labelEx($model, 'phone'); ?>
                    <?php echo $form->textField($model, 'phone', array('size' => 45, 'maxlength' => 45)); ?>
                    <?php echo $form->error($model, 'phone'); ?>

                    <?php echo $form->labelEx($model, 'addPhone'); ?>
                    <?php echo $form->textField($model, 'addPhone', array('size' => 45, 'maxlength' => 45)); ?>
                    <?php echo $form->error($model, 'addPhone'); ?>
                </div>
            </div>
            <?php echo $form->labelEx($model, 'physicalAddress'); ?>
            <?php echo $form->textField($model, 'physicalAddress', array('size' => 60, 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'physicalAddress'); ?>

            <?php echo $form->labelEx($model, 'physicalAddressEng'); ?>
            <?php echo $form->textField($model, 'physicalAddressEng', array('size' => 60, 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'physicalAddressEng'); ?>


            <?php echo $form->labelEx($model, 'legalAddress'); ?>
            <?php echo $form->textField($model, 'legalAddress', array('size' => 60, 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'legalAddress'); ?>
        </fieldset>


        <?php echo $form->labelEx($model, 'requisites'); ?>
        <?php echo $form->textArea($model, 'requisites', array('rows' => 5)); ?>
        <?php echo $form->error($model, 'requisites'); ?>

        <?php echo $form->labelEx($model, 'comment'); ?>
        <?php echo $form->textArea($model, 'comment', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'comment'); ?>




        <?php
        echo CHtml::submitButton(Yii::t('app', 'Save'), array('class' => 'button'));

        if (!$model->isNewRecord) {
            echo "&nbsp;" . CHtml::link('Удалить', Yii::app()->createUrl("system/confirmDeletion", array(
                        "mid" => deletionHelper::D_COMPANY,
                        "id" => $model->getPrimaryKey()))
                    , array('class' => 'alert button del-btn'));
        }

        $this->endWidget();
        ?>
    </div> <!-- form -->

<?php
$this->widget('ext.slidetoggle.ESlidetoggle', array(
    'itemSelector' => 'div.more',
    'titleSelector' => 'div.more h6',
    'collapsed' => 'div.more',
    'duration' => 'fast'
));
?>