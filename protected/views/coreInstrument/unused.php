
<script type="text/javascript">
    function reloadPage()
    {
        location.reload();
    }
    function showModal(html)
    {
        $('#modalDialog').html(html);
        $('#modalDialog').reveal();
        
    }
    $(".create-new-featuregroup").click(function(e) {
        $.post("<?=$this->createUrl("managedBlock/createAjax")?>",{id:<?=$model->getPrimaryKey()?>},showModal);
        e.preventDefault();
    })
    
    $(".update-featuregroup").click(function(e) {
        var myId=$(this).attr('mid');
        $.post("<?=$this->createUrl("managedBlock/updateAjax")?>",{id:<?=$model->getPrimaryKey()?>},showModal);
        e.preventDefault();
    })
    
    $(".delete-featuregroup").click(function(e) {
        var myId=$(this).attr('mid');
        var myCId=$(this).attr('cid');
        $.post("<?=$this->createUrl("managedBlock/deleteAjax")?>",{id:<?=$model->getPrimaryKey()?>});
        $('[mid='+myId+'][cid='+myCId+']').hide();
        e.preventDefault();
    })
    
 
</script>

<div id="modalDialog" class="reveal-modal expand">
</div>
