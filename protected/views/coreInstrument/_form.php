<?php
$this->angularJsDispatcher->injectDependency("ui.sortable");
if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>


<dl class="tabs">
    <dd class="active"><a href="#main">Основные настройки</a></dd>
    <?php if (!$model->isNewRecord) { ?>
        <dd><a href="#form">Конструктор формы для позиций</a></dd>
        <dd><a href="#logo">Редактор фотографии</a></dd>
    <?php } ?>
</dl>
<ul class="tabs-content">
    <li id="mainTab" class="active">
        <p class="note">
            <?php echo Yii::t('app', 'Fields with'); ?> <span
                class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
        </p>

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'core-instrument-form',
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
        ));
        if (Permission::isLoggedUserSuperadmin() || $model->isNewRecord) {
            $rights = true;
            $strict_rights = true;
        } else {
            $strict_rights = false;
            $rights = Permission::isManufacturerAllowed($model->idManufacturer);
        }

        echo $form->errorSummary($model);
        ?>

        <div class="row">
            <div class="four columns">
                <?php echo $form->labelEx($model, 'name'); ?>
                <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 255)); ?>
                <?php echo $form->error($model, 'name'); ?>

            </div>
            <div class="four columns">
                <?php echo $form->labelEx($model, 'ruName'); ?>
                <?php echo $form->textField($model, 'ruName', array('size' => 60, 'maxlength' => 255)); ?>
                <?php echo $form->error($model, 'ruName'); ?>

            </div>
            <div class="four columns">
                <?php echo $form->labelEx($model, 'sid'); ?>
                <?php echo $form->textField($model, 'sid', $model->isNewRecord ? array() : array('disabled' => 'true')); ?>
                <?php echo $form->error($model, 'sid'); ?>

            </div>
        </div>


        <div class="row">
            <div class="twelve columns">
                <div class="row">
                    <div class="six columns">
                        <div class="indent-bot-15">
                            <?php echo $form->labelEx($model, 'idCoreInstrumentClass'); ?>
                            <?php echo $form->dropDownList($model, 'idCoreInstrumentClass', CHtml::listData(CoreInstrumentClass::model()->findAll(), "idCoreInstrumentClass", "name")); ?>
                            <?php echo $form->error($model, 'idCoreInstrumentClass'); ?>

                        </div>
                    </div>
                    <div class="six columns">

                        <div class="indent-bot-15">
                            <?php echo $form->labelEx($model, 'idManufacturer'); ?>
                            <?php echo $form->dropDownList($model, 'idManufacturer', CHtml::listData(Manufacturer::model()->findAll(), "idManufacturer", "name")
                                , (Permission::isLoggedUserSuperadmin() || $model->isNewRecord) ? array() : array('disabled' => 'disabled')
                            ); ?>
                            <?php echo $form->error($model, 'idManufacturer'); ?>

                        </div>
                    </div>
                </div>
                <fieldset>
                    <legend>Цена</legend>
                    <div class="indent-bot-15">
                        <?php echo $form->dropDownList($model, 'isPriceSettled', array(0 => 'Цена неопределена', '1' => 'Использовать следующую цену для позиции')); ?>
                        <?php echo $form->error($model, 'isPriceSettled'); ?>
                    </div>
                    <div class="row">

                        <div class="four columns">
                            <?php echo $form->labelEx($model, 'price'); ?>
                            <?php echo $form->textField($model, 'price', array('size' => 60, 'maxlength' => 255, 'onkeypress' => '$("#Feature_isPriceSettled").val(1);')); ?>
                            <?php echo $form->error($model, 'price'); ?>
                        </div>
                        <div class="two columns">
                            <?php echo $form->labelEx($model, 'enumCurrency'); ?>
                            <?php echo $form->dropDownList($model, 'enumCurrency', Order::getCurrencies()); ?>
                            <?php echo $form->error($model, 'enumCurrency'); ?>
                        </div>
                        <div class="six columns">
                        </div>
                    </div>
                </fieldset>
            </div>

        </div>

        <fieldset>
            <legend>Файлы</legend>
            <?php
            if (!$model->isNewRecord) {
                $this->widget('ext.relation.relation', array(
                    'model' => $model,
                    'widgetType' => 2,
                    'showCreate' => false,
                    'relation' => 'attachments',
                    'controllerName' => 'attachment',
                    'fields' => 'originalFilenameWithLink',
                    'actionParams' => array('Attachment[belongId]' => $model->idCoreInstrument,
                        'Attachment[belongType]' => Attachment::BELONGS_TO_COREINSTRUMENT,
                        'returnUrl' => Yii::app()->request->getRequestUri()),
                ));
            }
            ?>
            <?php
            echo $form->labelEx($model, 'attachments');
            for ($i = 1; $i <= 3; $i++) {
                ?>

                <?php echo $form->fileField($model, 'attachment' . $i); ?>
                <?php echo $form->error($model, 'attachment' . $i); ?>
                <br/>
            <?php
            }
            ?>

            <br/>
        </fieldset>
        <?php
        echo CHtml::submitButton(Yii::t('app', 'Save'), array('class' => 'button'));
        if (!$model->isNewRecord) {
            echo "&nbsp;" . CHtml::link('Удалить', Yii::app()->createUrl("system/confirmDeletion", array(
                        "mid" => deletionHelper::D_COREINSTRUMENT,
                        "id" => $model->getPrimaryKey()))
                    , array('class' => 'alert button del-btn'));
        }
        ?>
        <?php if (!$model->isNewRecord) { ?>
            <?php
            if ($rights)
                echo CHtml::link("Редактировать описания", Yii::app()->createUrl("/coreInstrument/descriptionUpdate", array('id' => $model->idCoreInstrument)),
                    array('class' => 'button right small secondary dwDialogTrigger')) ?>
            <div class="more">
                <h6>Описание</h6>

                <p>
                    <?php echo $model->description; ?>
                </p>
            </div>
            <br/>
            <div class="more">
                <h6>Перевод описания</h6>

                <p>
                    <?php echo $model->ruDescription; ?>
                </p>
            </div>

        <?php } ?>

        <?php
        $this->endWidget();
        ?>

    </li>
    <?php if (!$model->isNewRecord) { ?>
        <li id="logoTab">
            <fieldset>
                <legend>Картинка</legend>

                <?php
                $this->widget("ext.hasImage.processingTool", array('model' => $model));
                ?>
                <br/>
            </fieldset>
        </li>

        <li id="formTab">
            <?

            $this->renderPartial('_sortableForm', array(
                'model' => $model));
            ?>
        </li>
    <?php } ?>
</ul>

<?php
$this->widget('ext.slidetoggle.ESlidetoggle', array(
    'itemSelector' => 'div.more',
    'titleSelector' => 'div.more h6',
    //  'collapsed' => 'div.more',
    'duration' => 'fast'
));
?>
