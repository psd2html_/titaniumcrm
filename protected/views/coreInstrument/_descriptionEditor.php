<?php
$form_name = 'sticky-ajax-form';
$form = $this->beginWidget('ext.foundation.widgets.FounActiveForm', array(
    'id' => $form_name,
    'htmlOptions' => array(
        'onsubmit' => "sendFormAjax('#$form_name'); return false;",
    ),
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
));


echo $form->errorSummary($model);
?>
<script>

    function sendFormAjax(form) {
        window.dwDialog.uploadForm(form, "<?= Yii::app()->request->getRequestUri() ?>", true);
    }
</script>

<?php echo $form->labelEx($model, 'description'); ?>
<?php
$this->widget('ext.niceditor.nicEditorWidget', array(
    "model" => $model, // Data-Model
    "attribute" => 'description', // Attribute in the Data-Model
    "config" => array("maxHeight" => "400"),
));
?>
<?php echo $form->error($model, 'description'); ?>
<br/>
<?php echo $form->labelEx($model, 'ruDescription'); ?>
<?php
$this->widget('ext.niceditor.nicEditorWidget', array(
    "model" => $model, // Data-Model
    "attribute" => 'ruDescription', // Attribute in the Data-Model
    "config" => array("maxHeight" => "400"),
));
?>
<?php echo $form->error($model, 'ruDescription'); ?>
<br/>
<br/>
<?php
echo CHtml::submitButton(Yii::t('app', 'Save'), array('class' => 'button'));
?>
<?php
$this->endWidget();
?>
