<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Core Instruments')
);
?><header> <h3> Управление базовыми приборами</h3> </header>


<?php
$p = Permission::getAllowedManufacturers();

if (!$p) {
    ?>
    К сожалению, похоже, что у вас нет разрешенных к просмотру или редактированию базовых приборов.
    <?php
} else {
    $this->widget('MGridView', array(
        'id' => 'core-instrument-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'cssFile' => false,
        'template' => '{items} {pager}',
        'columns' => array(
            array('name' => 'idManufacturer',
                'value' => '$data->manufacturer->name',
                'filter' => CHtml::listData($p, "idManufacturer", "name"),
            ),
            'name',
            array('name' => 'idCoreInstrumentClass',
                'value' => '$data->coreInstrumentClass->name',
                'filter' => CHtml::listData(CoreInstrumentClass::model()->findAll(), "idCoreInstrumentClass", "name"),
            ),
            'sid',
            array(
                'name' => 'hasPrice',
                'header' => Yii::t('app', 'Есть цена'),
                'type' => 'raw',
                'filter' => (array(Feature::NO_DESCRIPTION => Yii::t('app', 'Нет'), Feature::HAS_DESCRIPTION => Yii::t('app', 'Есть'))),
                'value' => '$data->hasPriceLiteral'
            ),
            array('class' => 'CButtonColumn',
                'template' => '{update}',
                'buttons' => array(
                    /* 'delete' => array(
                      'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                      'options'=>array('class'=>'button secondary tiny delete')
                      ), */
                    'update' => array(
                        'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                        'options' => array('class' => 'button secondary tiny update')
                    ),
                ),
            ),
        ),
    ));
    ?>
    <div class="footer_content">
        <a class="button" href='<?= $this->createUrl("CoreInstrument/create") ?> '>Создать новый</a>
    </div>

    <?php
}
?>

