<?php
Yii::app()->getClientScript()->registerCoreScript('jquery.ui');
Yii::app()->getClientScript()->registerScriptFile('/js/angular/angular.sortable.js', CClientScript::POS_BEGIN);
Yii::app()->getClientScript()->registerScriptFile('/js/angular/ng-upload.js', CClientScript::POS_BEGIN);
$description = ManagedBlock::getEnumRules();
?>

<script>
function CoreInstrunmentController($http, $scope, $timeout, $window) {
    $scope.groups =<?= CJavaScript::encode($model->getSerializedOptionsSettings())?>;
    $scope.copyPasteShown = false;
    $scope.files = [];
    $scope.reload = function () {
        $scope.saving = true;
        $http.get('/coreInstrument/getFeatureGroupConfig/<?= $model->idCoreInstrument ?>').then(function (data) {

            $scope.selectedFeatureGroup = null;
            $scope.selectedManagedBlock = null;
            console.log(data.data);
            $scope.groups = data.data;
            $scope.saving = false;
            $scope.dirty = false;

        });
    }

    $window.onbeforeunload = function () {
        return  $scope.dirty ? "Форма содержит несохраненные данные" : null;
    };
    $scope.saving = false;
    $scope.dirty = false;
    $scope.enumRulesDescription = <?= CJavaScript::encode($description) ?>;
    $scope.deletedGroups = [];
    $scope.selectedFeatureGroup = null;
    $scope.selectedManagedBlock = null;

    $scope.spoil = function () {
        $scope.dirty = true;
    }


    $scope.ajaxUrl = '/api/getFeature';

    $scope.copyToClipboardSelectedGroup = function () {
        if ($scope.selectedFeatureGroup==null) return;
        $scope.copyPasteShown=!$scope.copyPasteShown;
        $scope.copyPasteData=JSON.stringify($scope.selectedFeatureGroup);
    }

    function validateFeature(obj) {
        var fields=["id","sid"];

        if (obj.hasOwnProperty("$$hashKey")) delete obj.$$hashKey;
        for (var field in fields) {

            if (!obj.hasOwnProperty(fields[field])) {
                alert("Нет поля в фиче - "+fields[field]);
                return false;
            }
        }
        return true;
    }

    function validateBlock(obj) {
        var fields=["features","enumRule","comment","name","id"];
        obj.id=-1;
        obj.deleted=[];
        if (obj.hasOwnProperty("$$hashKey")) delete obj.$$hashKey;
        for (var field in fields) {
            if (!obj.hasOwnProperty(fields[field]))
            {
                alert("Нет поля в блоке - "+fields[field]);
                return false;
            }
        }
        for (var feature in obj.features) {
            if (!validateFeature(obj.features[feature])) return false;
        }
        return true;
    }

    function validateGroup(obj) {
        var fields=["managedBlocks","img","opened","name"];
        obj.id=-1;
        obj.deleted=[];
        if (obj.hasOwnProperty("$$hashKey")) delete obj.$$hashKey;
        for (var field in fields) {
            if (!obj.hasOwnProperty(fields[field]))
            {
                alert("Нет поля в группе - "+fields[field]);
                return false;
            }
        }
        for (var block in obj.managedBlocks) {
            if (!validateBlock(obj.managedBlocks[block])) return false;
        }
        return true;
    }

    $scope.pasteFromClipboardGroup = function () {
        data=window.prompt ("Вставить описание группы из буфер обмена, нажмите Ctrl+V, Enter \n");

        try {
            var obj=JSON.parse(data);
        } catch (e)
        {
            alert("Данные повреждены или плохие; Вставка группы невозможна; Ошибка парсинга;"); return
        }
       // console.log(obj); return;
        if (!validateGroup(obj)) {alert("Данные повреждены или плохие; Вставка группы невозможна;"); return}

        $scope.groups.unshift(obj);
        $scope.selectedFeatureGroup=obj;
        $scope.spoil();


    }
    $scope.createGroup = function () {
        $scope.groups.unshift(
            {id: -1,
                name: $scope.newGroupName,
                managedBlocks: [],
                deleted: []}
        );
        $scope.spoil();
        $scope.newGroupName = '';
    }

    $scope.createBlock = function (f) {
        if (!$scope.selectedFeatureGroup) return;
        $scope.selectedFeatureGroup.managedBlocks.unshift(
            {   id: -1,
                name: $scope.newBlockName ? $scope.newBlockName : '',
                features: [],
                comment: '',
                enumRule: 0,
                deleted: []}
        );
        $scope.spoil();
        $scope.newBlockName = '';
    }

    $scope.addFeature = function (f) {
        if (!$scope.selectedManagedBlock) return;
        for (var i = 0; i < $scope.selectedManagedBlock.features.length; i++) {
            if ($scope.selectedManagedBlock.features[i].id == f.id) {
                alert('Данная позиция уже присутствует в списке. Добавление невозможно.');
                return;
            }
        }
        $scope.spoil();
        $scope.selectedManagedBlock.features.unshift({
            'id': f.id,
            'name': f.name
        });
    }

    $scope.variants = [];
    $scope.loading = !true;
    $scope.loadingCreate = !true;
    $scope.sm = 0; // 0 - idle, 1 - waiting for deferred, 2 - loading

    var deferred;
    var deferredCreate;

    function ajaxConvert(inputObject) {
        var o = [];
        for (var key in inputObject) {
            o.unshift({id: key, name: inputObject[key]});
        }
        return o;
    }

    $scope.createFeature = function () {
        $http.post("/api/putFeature", {
            '<?= Yii::app()->request->csrfTokenName ?>': '<?= Yii::app()->request->csrfToken ?>',
            'sid': $scope.createsid, 'n': $scope.createname, 'm': $scope.createmanufacturer}).then(
            function (data) {
                if (data.data.r <= 0) {
                    alert(data.data.m);
                    return;
                }
                $scope.spoil();
                $scope.addFeature(data.data.v);
            },
            function (err) {
                console.log(err);
            }
        );
    }

    $scope.saveAll = function () {
        if ($scope.saving) return;

        var post = $http.post("/coreInstrument/Savegroups/<?= $model->idCoreInstrument?>", {
            '<?= Yii::app()->request->csrfTokenName ?>': '<?= Yii::app()->request->csrfToken ?>',
            'd': $scope.deletedGroups, 'g': $scope.groups});
        $scope.saving = true;

        post.then(
            function (data) {
                var oldSDId = $scope.selectedFeatureGroup ? $scope.selectedFeatureGroup.id : null;
                var oldMBId = $scope.selectedManagedBlock ? $scope.selectedManagedBlock.id : null;

                $scope.groups = data.data;
                $scope.selectedFeatureGroup = null;
                $scope.selectedManagedBlock = null;

                for (var i = 0; i < $scope.groups.length; i++) {
                    if (oldSDId == $scope.groups[i].id) {
                        $scope.selectedFeatureGroup = $scope.groups[i];
                    }
                }
                $scope.saving = false;
                $scope.dirty = false;
            },
            function (err) {
                alert('Не удалось сохранить.');
                $scope.saving = false;
                console.log(err);
            }
        );
    }
    $scope.$watch('filter', function () {
        if (($scope.filter) && (!$scope.loading)) {

            if (deferred) {
                $timeout.cancel(deferred);
            }

            $scope.sm = 1;

            deferred = $timeout(function () {
                $scope.sm = 2;
                $scope.loading = true;
                $http.get($scope.ajaxUrl + "?q=" + $scope.filter).then(
                    function (data) {
                        $scope.sm = 0;
                        // console.log(data);
                        $scope.loading = false;
                        $scope.variants = ajaxConvert(data.data);
                    },
                    function (err) {
                        $scope.sm = 0;
                    }
                );
            }, 500);
        }
    });


    $scope.deleteItem = function (needle, haystack, deleteLog) {
        var v_id = haystack.indexOf(needle);
        if (v_id >= -1) {
            deleteLog.unshift(needle.id);
            haystack.splice(v_id, 1);
        }
    }

    $scope.unsetSelectedManagedBlock = function () {
        $scope.selectedManagedBlock = null;
    }
    $scope.editBlock = function (f) {
        $scope.selectedManagedBlock = f;
    }
    $scope.deleteSelectedManagedBlock = function () {
        if (!$scope.selectedFeatureGroup) {
            return;
        }
        if (!$scope.selectedManagedBlock) {
            return;
        }
        $scope.deleteItem($scope.selectedManagedBlock, $scope.selectedFeatureGroup.managedBlocks, $scope.selectedFeatureGroup.deleted);
        $scope.spoil();
        $scope.selectedManagedBlock = null;
    }
    $scope.deleteSelectedGroup = function () {
        if (!$scope.selectedFeatureGroup) {
            return;
        }
        if (confirm("Вы уверены, что хотите удалить данную группу и все её блоки?")) {

            var v = $scope.selectedFeatureGroup;
            var v_id = $scope.groups.indexOf(v);
            if (v_id >= -1) {
                $scope.deletedGroups.unshift(v.id);
                $scope.groups.splice(v_id, 1);
                $scope.selectedFeatureGroup = null;
                $scope.spoil();
            }
        }
    }
    $scope.toggleSort = !false;

    $scope.selectFeatureGroup = function (t) {
        $scope.copyPasteShown=false;
        $scope.selectedFeatureGroup = t;
        $scope.selectedManagedBlock = null;
    }
    $scope.lol = function () {
        console.log($scope.ororo.type);
    }
    $scope.triggerGroup = function (d) {
        d.opened = !d.opened;
    }
    var xhr;
    $scope.uploadFile = function () {

        if (!$scope.selectedFeatureGroup) {
            return;
        }

        var fd = new FormData()
        for (var i in $scope.files) {
            fd.append("image", $scope.files[i])
        }
        fd.append("<?= Yii::app()->request->csrfTokenName ?>", "<?= Yii::app()->request->csrfToken ?>");
        fd.append("gid", $scope.selectedFeatureGroup.id);
        xhr = new XMLHttpRequest()
        xhr.upload.addEventListener("progress", uploadProgress, false)
        xhr.addEventListener("load", uploadComplete, false)
        // xhr.addEventListener("error", uploadFailed, false)
        // xhr.addEventListener("abort", uploadCanceled, false)
        xhr.open("POST", "/coreInstrument/uploadFeatureGroupImage")
        $scope.progressVisible = true
        xhr.send(fd)
    }

    function uploadComplete(evt) {
        $scope.$apply(function () {
            $scope.progressVisible = false;
            if (xhr.status == 200) {
                var result = JSON.parse(xhr.response);
                if (result.r > 0) {
                    $scope.selectedFeatureGroup.img = result.fn + '?_t=' + Math.floor((Math.random() * 10000) + 1);
                    ;
                } else if (result.r < 0) {
                    alert("Загрузка не удалась. " + result.msg);
                }
            }
        });
    }

    $scope.deleteImage = function () {
        if (($scope.selectedFeatureGroup) && ($scope.selectedFeatureGroup.img)) {
            $http.post("/coreInstrument/deleteFeatureGroupImage", {
                '<?= Yii::app()->request->csrfTokenName ?>': '<?= Yii::app()->request->csrfToken ?>',
                'gid': $scope.selectedFeatureGroup.id}).then(
                function (data) {
                    if (data.data.r <= 0) {
                        alert(data.data.m);
                        return;
                    }
                    $scope.selectedFeatureGroup.img = null;

                },
                function (err) {
                    console.log(err);
                }
            );
        }
    }

    $scope.setFiles = function (element) {
        $scope.$apply(function () {
            console.log('files:', element.files);
            // Turn the FileList object into an Array
            $scope.files = []
            for (var i = 0; i < element.files.length; i++) {
                $scope.files.push(element.files[i])
            }
            $scope.progressVisible = false
        });
    };
    function uploadProgress(evt) {
        $scope.$apply(function () {
            if (evt.lengthComputable) {
                $scope.progress = Math.round(evt.loaded * 100 / evt.total);
            } else {
                $scope.progress = 'unable to compute'
            }
        })
    }

}
</script>


<div ng-controller='CoreInstrunmentController'>

<a ng-show="saving" href="#x" class="savingoverlay"></a>
<div ng-show="saving" class="savingpopup"><?= Helpers::getSpinnerGif() ?> Идет сохранение...</div>

<div class="row">
    <div class="twelve columns panel">
        <input ng-disabled="!dirty || saving" type="button" class="button "
               value="Сохранить"
               ng-click="saveAll()"/>
        <input ng-show="dirty" ng-disabled="!dirty || saving" type="button" class="button secondary"
               value="Отменить"
               ng-click="reload()"/>
    </div>
</div>

<div class="row">

<div class="four columns">
    <h4>Существующие группы позций</h4>
    <i ng-show="groups.length==0">Групп нет.</i>

    <ul ui-sortable="{handle:'.sortable-tag', update:spoil}" ng-model="groups">
        <li ng-repeat='d in groups'>
                    <span class="sortable-tag" ng-show="toggleSort">
                        <img src="/images/icon_sort.gif">
                    </span>

            <input type="button" class="button small" ng-class='d==selectedFeatureGroup?"":"secondary"'
                   ng-click='selectFeatureGroup(d)'
                   value="{{ d.name }} ({{ d.managedBlocks.length }})"/>

        </li>
    </ul>
    <hr/>

    <div class="row collapse">
        <div class="eight columns">
            <input type="text"  placeholder="Введите название группы" ng-model="newGroupName"/>
        </div>
        <div class="four columns">
            <input type="button" class="small button secondary postfix" value="Создать новую группу"
                   ng-click="createGroup()"/>
        </div>
    </div>
    <a type="button right" class="small button secondary"
           ng-click="pasteFromClipboardGroup()"> Вставить из буфер обмена</a>
</div>
<div class="eight columns">
<div class="selMEdit" ng-show='selectedManagedBlock'>
    <div class="right">
        <a href ng-click="unsetSelectedManagedBlock() "> Назад &raquo; </a>
    </div>
    <h5>Редактирование блока</h5>
    <label>Название блока</label>

    <div class="row">
        <div class="eight columns">
            <input type="text" ng-change='spoil();' ng-model="selectedManagedBlock.name"/>
        </div>
        <div class="four columns">
            <input type="button" class="small button alert" value="Удалить блок"
                   ng-click="deleteSelectedManagedBlock()"/>
        </div>
    </div>
    <label>Комментарий к блоку</label>
    <textarea ng-change='spoil()' ng-model="selectedManagedBlock.comment">
    </textarea>

    <label>Тип валидации</label>
    <?php echo CHtml::dropDownList('enumRule', 0, ManagedBlock::getEnumRules(), array("ng-change" => 'spoil()', 'ng-model' => 'selectedManagedBlock.enumRule')); ?>
    <br/>

    <hr/>

    <h6>Позиции</h6>

    <div class="row">
        <div class="six columns">
            <div class="row collapse">
                <div class="eleven columns"><input type="text" ng-model="filter" autocomplete=false
                                                   name="search-filter"
                                                   placeholder="Введите первые буквы для поиска.."/></div>
                <div class="one column"><span class="postfix">&#128269;</span></div>
            </div>

            <div class="indent-bot-10" ng-show="!filter&&(sm==0)">
                Введите первые буквы названия или SID, и здесь будут варианты для добавления
            </div>

            <div class="indent-bot-10" ng-show="filter&&(sm==1)">
                Поиск "{{ filter }}" ...
            </div>

            <div class="indent-bot-10" ng-show="filter&&(sm==2)">
                Получение ответа ...
            </div>

            <div class="indent-bot-10" ng-show="(variants.length<1)&&(filter)&&(sm==0)">
                По запросу ничего не найдено
            </div>

            <ul class="indent-bot-10">
                <li ng-repeat="d in variants">

                    <input type="button" value="+" ng-click="addFeature( d )">
                    <span ng-bind-html="d.name"></span>
                </li>
            </ul>

        </div>
        <div class="six columns">
            <ul ui-sortable="{ update:spoil }" ng-model="selectedManagedBlock.features">
                <li ng-repeat='f in selectedManagedBlock.features'>
                    <input type="button" class="" value="X"
                           ng-click="deleteItem(f, selectedManagedBlock.features, selectedManagedBlock.deleted)"/>
                    <span ng-bind-html='f.name'></span>
                                <span class="sortable-tag"
                                      ng-show="toggleSort">
                                 <img src="/images/icon_sort.gif">
                                </span>
                </li>
            </ul>
        </div>
    </div>

    <hr/>

    <h6>Создать позицию и добавить в список</h6>

    <form name="createFeatureForm" ng-model="createFeatureForm">
        <div class="row">
            <div class="four columns">
                <label>SID</label>
                <input type="text" ng-model="createsid" autocomplete=false
                       name="create-sid"
                       placeholder="Введите SID" required/>
            </div>

            <div class="four columns">
                <label>Название</label>
                <input type="text" ng-model="createname" autocomplete=false
                       name="create-name"
                       placeholder="Введите название позиции" required/>
            </div>
            <div class="four columns">
                <label>Производитель</label>
                <?php
                $allManufacturers = Manufacturer::model()->findAll();
                $allManufacturers = CHtml::listData($allManufacturers, 'idManufacturer', 'name');

                echo CHtml::dropDownList('create-manufacturer', 0, $allManufacturers, array('ng-model' => 'createmanufacturer', 'required' => 'required')); ?>
            </div>
        </div>

        <input type="button" class="button small" value="Создать позицию и добавить"
               ng-disabled='!createFeatureForm.$dirty||!createFeatureForm.$valid'
               ng-click="createFeature()"/>
    </form>
</div>

<!-- = Group widget = -->

<div class="selEdit" ng-show='selectedFeatureGroup&&!selectedManagedBlock'>
    <h5>Редактирование группы</h5>
    <fieldset ng-show="copyPasteShown">
        <textarea ng-bind="copyPasteData">
        </textarea>
        <small>Чтобы скопировать описание данной группы в буфер обмена, нажмите на текстовое поле, затем Ctrl+A, Ctrl+C</small>
    </fieldset>
    <input type="button right" class="small button secondary" value="В буфер обмена"
           ng-click="copyToClipboardSelectedGroup()"/>

    <br/> <br/>
    <label>Название группы</label>

    <div class="row">

        <div class="eight columns">
            <input ng-change='spoil()' type="text" ng-model="selectedFeatureGroup.name"/>
        </div>
        <div class="four columns">
            <input type="button" class="small button alert" value="Удалить группу"
                   ng-click="deleteSelectedGroup()"/>
        </div>
    </div>

    <fieldset>
        <legend>Позиции</legend>



        <div class="row collapse">
            <div class="eight columns">
                <input type="text" placeholder="Введите название блока позиций" ng-model="newBlockName"/>
            </div>
            <div class="four columns">
                <input type="button" class="small button secondary postfix" value="Создать блок позиций"
                       ng-click="createBlock()"/>
            </div>
        </div>

        <hr/>

        <ul ui-sortable="{handle:'.sortable-tag', update:spoil}" ng-model="selectedFeatureGroup.managedBlocks">
            <li ng-repeat='m in selectedFeatureGroup.managedBlocks'>
                    <span class="sortable-tag" ng-show="toggleSort">
                        <img src="/images/icon_sort.gif">
                    </span>

                <div class="right">
                    <a type="button" class="small button secondary"
                           ng-click="editBlock(m)">
                        <img src="/images/icn_edit.png" alt="Ред."/>
                    </a>
                </div>
                <b> {{ m.name?m.name:'Без названия' }} </b> <br/>

                <div class="indent-bot-15">
                    <span class="required">*</span>
                    {{ enumRulesDescription[m.enumRule] }}
                    <div ng-show="m.comment">
                        <span class="required">**</span>
                                <span ng-bind-html='m.comment'>
                                </span>
                    </div>
                </div>
                <i ng-show="m.features.length==0">Позиций нет.</i>
                <ul class="circle">
                    <li ng-repeat='f in m.features'>
                        <span ng-bind-html='f.name'></span>
                    </li>
                </ul>


                <br/>
            </li>
        </ul>
    </fieldset>

    <fieldset>
        <legend>Фрагмент дерева</legend>


        <form class="indent-bot-15" ng-show="selectedFeatureGroup.id>0">
            <label ng-show="progressVisible"><?= Helpers::getSpinnerGif() ?> Загрузка {{progress}}%...</label>
            <input type="hidden" name='<?= Yii::app()->request->csrfTokenName ?>'
                   value="<?= Yii::app()->request->csrfToken ?>"/>
            <input type="file" name="image" ng-model="photo" accept="image/*"
                   onchange="angular.element(this).scope().setFiles(this)"/>
            <input type="hidden" name='gid' value='{{selectedFeatureGroup.id}}'/>
            <input type="submit" value="Загрузить" ng-disabled="files.length==0" ng-click="uploadFile()"/>
            <input type="submit" ng-show="selectedFeatureGroup.img" value="Удалить картинку" ng-click="deleteImage()"/>
        </form>
        <i ng-show="selectedFeatureGroup.id<0">Загрузка картинок невозможна для созданных, но не сохраненных групп.
            Пожалуйста сохраните данные.</i>
        <a href="#" class="th indent-bot-15" ng-show="selectedFeatureGroup.img">
            <img width="500px" ng-src="{{selectedFeatureGroup.img}}"/> </a>
    </fieldset>
</div>
</div>
</div>
</div>


