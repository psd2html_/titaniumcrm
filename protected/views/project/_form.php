<?php if (Yii::app()->user->hasFlash('success')): ?>
    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>


<?php
$this->widget('ext.slidetoggle.ESlidetoggle', array(
    'itemSelector' => 'div.more',
    'titleSelector' => 'div.more h6',
    'collapsed' => 'div.more',
    'duration' => 'fast'
));
?>



<div class="form">

    <dl class="tabs">
        <dd class="active"><a href="#main">Общие Настройки</a></dd>
        <dd><a href="#access">Доступ</a></dd>
       </dl>
    <?php
    $form = $this->beginWidget('ext.foundation.widgets.FounActiveForm', array(
        'id' => 'project-form',
        'enableAjaxValidation' => !true,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
        'enableClientValidation' => true,
    ));
    echo $form->errorSummary($model);
    ?>
    <ul class="tabs-content">


        <li class="active" id="mainTab">
            <?php $this->renderPartial('tabs/_main', array('model' => $model, 'form' => $form, 'logistic' => $logistic)); ?>
        </li>
        <li id="accessTab">
            <?php $this->renderPartial('tabs/atomicRbac', array('model' => $model, 'form' => $form, 'logistic' => $logistic)); ?>
        </li>

    </ul>
    <?php
    echo CHtml::submitButton(Yii::t('app', 'Save'), array('class' => 'button'));
    if (!$model->isNewRecord) {
        echo "&nbsp;" . CHtml::link('Удалить', Yii::app()->createUrl("system/confirmDeletion", array(
                    "mid" => deletionHelper::D_PROJECT,
                    "id" => $model->getPrimaryKey()))
                , array('class' => 'alert button del-btn'));
    }
    ?>

    <?php
    $this->endWidget();
    ?>


</div> <!-- form -->

