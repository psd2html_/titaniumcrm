<div id="archiveLink" style="text-align: center"> <?= Helpers::getSpinnerGif() ?> Подготовка вашего архива... </div>

<script>
    var attempts = 5;
    function getLink() {
        $.getJSON(
            "<?= Yii::app()->createUrl("/project/getZip",array('id'=>$id))?>",
            {},
            function (data) {
                console.log(data)
                attempts = attempts - 1;
                if (data.r > 0) {
                    $("#archiveLink").html(data.c);
                } else {
                    if (attempts < 0) {
                        $("#archiveLink").html("Произошла ошибка. Попробуйте позже.");
                    } else {
                        setTimeout(getLink(), 1000);
                    }
                }
            }
        )
    }
    setTimeout(getLink(), 500);

</script>