<?php
/**
 * @var $model UploadForm
 */
if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>


<div class="row full-width">
    <div class="twelve columns">
        <h4><?= $model->getTitle() ?></h4>

        <?php
        $form_name = 'sticky-ajax-form';
        $form = $this->beginWidget('ext.foundation.widgets.FounActiveForm', array(
            'id' => $form_name,
            'htmlOptions' => array(
                'enctype' => 'multipart/form-data',
                'onsubmit' => "sendFormAjax('#$form_name'); return false;",
            ),
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
        ));

        echo $form->errorSummary($model);
        ?>

        <script>
            function sendFormAjax(form) {
                window.dwDialog.uploadForm(form, "<?= Yii::app()->request->getRequestUri() ?>", true);
            }
        </script>


        <div class="row">
            <div class="six columns">
                <?php echo $form->fileFieldRow($model, 'attachment'); ?>
                <br/><br/>
                <?php echo $form->checkBoxRow($model, 'doRemove'); ?></div>
            <div class="six columns">
                <b>Текущий
                    файл: </b> <?= $model->oldUpload ? CHtml::link($model->oldUpload->name, $model->oldUpload->getDownloadUrl()) : "<i>Не задан</i>" ?>
            </div>
        </div>


        <?php echo $form->hiddenField($model, 'enumField'); ?>
        <?php echo $form->hiddenField($model, 'idProject'); ?>
        <?php echo $form->hiddenField($model, 'idLogisticPosition'); ?>

        <br/><br/>

        <?php
        echo CHtml::submitButton('Cохранить'
            , array('class' => 'button'));

        $this->endWidget();
        ?>

    </div>
</div> <!-- form -->





