<div class="row ">
    <div class="two columns indent-bot-15 ">
        <?php echo $form->dropDownListRow($model, 'isExportDoubleApplicationLicenseRequired', array(0 => 'Нет', '1' => 'Да', '2' => 'Неизвестно')); ?>
    </div>
    <div class="two columns indent-bot-15">
        <?php echo $form->dropDownListRow($model, 'isExportDeclarationMade', array(0 => 'Нет', '1' => 'Да', '2' => 'Неизвестно')); ?>
    </div>
    <div class="two columns">
        <?php echo $form->dropDownListRow($model, 'isSpecialDeliveryRequired', array(0 => 'Нет', '1' => 'Да', '2' => 'Неизвестно')); ?>
    </div>
    <div class="two columns">
        <?php echo $form->dropDownListRow($model, 'isTechnoinfoUnloadsGoods', array(0 => 'Заказчик', '1' => 'Техноинфо', '2' => 'Неизвестно')); ?>
    </div>
    <div class="two columns">
        <?php echo $form->dropDownListRow($model, 'isPurchaseViaManufacturer', array(0 => 'Посредник', '1' => 'Производитель', '2' => 'Неизвестно')); ?>
    </div>
    <div class="one columns">
        <?php echo $form->dropDownListRow($model, 'isInsurancePayed', array(0 => 'Нет', '1' => 'Есть', '2' => 'Неизвестно')); ?>
    </div>
    <div class="one columns">
        <?php echo $form->dropDownListRow($model, 'enumProvision', Project::getEnumProvisionList()); ?>
    </div>
</div>
<div class="row">
    <div
        class="four columns"><?php echo $form->textAreaRow($model, 'packagingSpecification', array('rows' => 4)); ?></div>
    <div
        class="four columns"><?php echo $form->textAreaRow($model, 'consigneeAddress', array('rows' => 4)); ?></div>
    <div
        class="four columns"><?php echo $form->textAreaRow($model, 'actualDeliveryAddress', array('rows' => 4)); ?></div>
</div>

<div class="row">
    <div class="six columns"><?php echo $form->textFieldRow($model, 'goodsNameRus'); ?>
        <?php echo $form->textFieldRow($model, 'goodsOrigin'); ?>
        <?php echo $form->textFieldRow($model, 'contractTotal'); ?>


    </div>
    <div class="six columns"><?php echo $form->textFieldRow($model, 'goodsNameEng'); ?>
        <?php echo $form->textFieldRow($model, 'poNumber'); ?>
        <?php echo $form->textFieldRow($model, 'contractNumber'); ?>


    </div>
</div>

<div class="row">
    <div class="three columns">
        <?php echo CHtml::label("Код ТН ВЭД ". CHtml::link("(<i class='fa fa-book'></i> Справочник кодов)","http://www.tks.ru/db/tnved/tree",array("target"=>'_blank')),false);
              echo $form->textField($model, 'tnvedCod');
              echo $form->error($model, 'tnvedCod');
        ?>

    </div>
    <div class="three columns indent-bot-15">
        <?php echo $form->labelEx($model, 'contractDate'); ?>
        <?php
        $this->widget('ext.proDate.proDate', array(
            'model' => $model,
            'attribute' => 'contractDate',
            'htmlOptions' => array('class' => 'foundation-date-picker')
        ));
        ?>
    </div>

    <div class="three columns">
        <?php echo $form->dropDownListRow($model, 'buyIncotermsCondition', Project::getIncotermsList()); ?>
    </div>
    <div class="three columns">
        <?php echo $form->dropDownListRow($model, 'sellIncotermsCondition', Project::getIncotermsList()); ?>
    </div>
    <div class="three columns"></div>
</div>
<?php echo $form->textAreaRow($model, 'takeoutAddress'); ?>

<?php $files = array('fileCoreInstrumentDescription', 'filePackingList', 'fileContract') ?>
<table class="twelve">
    <thead>
    <tr>
        <th width="50%">Загрузить документ</th>
        <th>Текущий документ</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($files as $att) { ?>
        <tr>
            <td><?php echo $form->fileFieldRow($model, $att); ?> </td>
            <td><?php echo $model->{$att . 'Attachment'} ? $model->{$att . 'Attachment'}->downloadLink : '<i>Нет</i>' ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>




