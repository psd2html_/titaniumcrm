<?php /* @var $model Contest */ ?>



    <div class="row">
        <div class="four columns"> <?php echo $form->labelEx($model, 'contestUrl'); ?>
            <?php echo $form->textField($model, 'contestUrl', array('size' => 60, 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'contestUrl'); ?>
        </div>
        <div class="four columns">
            <?php echo $form->labelEx($model, 'contestSiteUrl'); ?>
            <?php echo $form->textField($model, 'contestSiteUrl', array('size' => 60, 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'contestSiteUrl'); ?>
        </div>
        <div class="four columns">
            <?php echo $form->labelEx($model, 'number'); ?>
            <?php echo $form->textField($model, 'number', array('size' => 60, 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'number'); ?>
        </div>
    </div>

    <div class="row">

        <div class="three columns">
            <?php echo $form->labelEx($model, 'datePost'); ?>
            <?php
            $this->widget('ext.proDate.proDate', array(
                'model' => $model,
                'attribute' => 'datePost',
                'htmlOptions' => array('class' => 'foundation-date-picker')
            ));
            ?>
            <?php echo $form->error($model, 'datePost'); ?>
        </div>
        <div class="three columns">
            <?php echo $form->labelEx($model, 'dateRequest'); ?>
            <?php
            $this->widget('ext.proDate.proDate', array(
                'model' => $model,
                'attribute' => 'dateRequest',
                'htmlOptions' => array('class' => 'foundation-date-picker')
            ));
            ?>
            <?php echo $form->error($model, 'dateRequest'); ?>
        </div>
        <div class="three columns">
            <?php echo $form->labelEx($model, 'dateResponse'); ?>
            <?php
            $this->widget('ext.proDate.proDate', array(
                'model' => $model,
                'attribute' => 'dateResponse',
                'htmlOptions' => array('class' => 'foundation-date-picker')
            ));
            ?>
            <?php echo $form->error($model, 'dateResponse'); ?>
        </div>
        <div class="three columns">
            <?php echo $form->labelEx($model, 'dateContest'); ?>
            <?php
            $this->widget('ext.proDate.proDate', array(
                'model' => $model,
                'attribute' => 'dateContest',
                'htmlOptions' => array('class' => 'foundation-date-picker')
            ));
            ?>
            <?php echo $form->error($model, 'dateContest'); ?>
        </div>
    </div>
    <div class="row">
        <div class="four columns"> <?php echo $form->labelEx($model, 'costLot'); ?>
            <?php echo $form->textField($model, 'costLot'); ?>
            <?php echo $form->error($model, 'costLot'); ?></div>
        <div class="four columns">
            <?php echo $form->labelEx($model, 'costAsk'); ?>
            <?php echo $form->textField($model, 'costAsk'); ?>
            <?php echo $form->error($model, 'costAsk'); ?>
        </div>
        <div class="four columns">
            <?php echo $form->labelEx($model, 'costContract'); ?>
            <?php echo $form->textField($model, 'costContract'); ?>
            <?php echo $form->error($model, 'costContract'); ?>
        </div>
    </div>

    <fieldset>
        <legend>Файлы</legend>
        <span>Файлы переместились во вкладку файлы</span>

        <br/>
    </fieldset>



