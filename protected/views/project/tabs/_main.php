<?php Yii::app()->clientScript->registerScriptFile("/js/revealActivate.js", CClientScript::POS_END); ?>

<?php
$coreInstrument = new CoreInstrument();
$coreInstrument->unsetAttributes();

if ($model->idManufacturer) {
    $coreInstrument->idManufacturer = $model->idManufacturer;
}

if ($model->idCoreInstrumentClass) {
    $coreInstrument->idCoreInstrumentClass = $model->idCoreInstrumentClass;
}

$aa = $coreInstrument->search();
$aa->pagination = false;
$coreInstrumentList = $aa->getData();
$coreInstrumentDataList = CHtml::listData($coreInstrumentList, 'idCoreInstrument', 'name');
$coreInstrumentDataList[0] = "Без базового инструмента";
?>
<script>
    /**
     * refresh
     */
    function refreshManufacturers() {
        $.ajax({
            url: "<?= Yii::app()->createUrl('ajax/getManufacturers') ?>",
            data: {'idCoreInstrumentClass': $('#Project_idCoreInstrumentClass').val()},
            type: "POST",
            success: function (e) {
                //console.log(e);
                $('#Project_idManufacturer').html(e.manufacturers);
                $('#Project_idCoreInstrument').html(e.coreinstruments);
            }
        });
    }

    function refreshVirtual() {
        var m = $('#Project_idCoreInstrument').val();
        if (m != 0) {
            $('#virtualCoreInstrumentBox').hide();
        } else {
            $('#virtualCoreInstrumentBox').show();
        }
    }

    function refreshList() {
        $.ajax({
            url: "<?= Yii::app()->createUrl('ajax/getCoreInstruments') ?>",
            data: {'idManufacturer': $('#Project_idManufacturer').val(),
                'idCoreInstrumentClass': $('#Project_idCoreInstrumentClass').val()},
            type: "POST",
            success: function (e) {
                $('#Project_idCoreInstrument').html(e);
            }
        });
    }
    $(document).ready(function() {refreshVirtual();})
</script>

<!--!!!!!-->
<!--!!!!-->

<div class="row">
    <div class="three columns indent-bot-15 ">
        <?php echo $form->labelEx($model, 'idCoreInstrumentClass'); ?>
        <?php echo $form->dropDownList($model, 'idCoreInstrumentClass', CHtml::listData(CoreInstrumentClass::model()->findAll(), "idCoreInstrumentClass", "name"), array('disabled' => 'disabled', 'onchange' => 'refreshManufacturers();', 'onkeyup' => 'refreshManufacturers();')); ?>
        <?php echo $form->error($model, 'idCoreInstrumentClass'); ?>
    </div>
    <div class="three columns">
        <?php $manufacturers = $model->idCoreInstrumentClass ? Manufacturer::findAllWhoHasCoreInstrumentClass($model->idCoreInstrumentClass) : Manufacturer::model()->findAll();
        ?>
        <?php echo $form->labelEx($model, 'idManufacturer'); ?>
        <?php echo $form->dropDownList($model, 'idManufacturer', CHtml::listData($manufacturers, "idManufacturer", "name"), array('disabled' => 'disabled', 'onchange' => 'refreshList();', 'onkeyup' => 'refreshList();')); ?>
        <?php echo $form->error($model, 'idManufacturer'); ?>
    </div>
    <div class="three columns ">
        <?php echo $form->labelEx($model, 'idCoreInstrument'); ?>
        <?php echo $form->dropDownList($model, 'idCoreInstrument', $coreInstrumentDataList, array('onchange' => 'refreshVirtual();', 'onkeyup' => 'refreshVirtual();')); ?>
        <?php echo $form->error($model, 'idCoreInstrument'); ?>
    </div>
    <div class="three columns">
        <div id="virtualCoreInstrumentBox">
            <?php echo $form->textFieldRow($model, 'virtualCoreInstrument', $model->getYearQuartalVariants()); ?>
        </div>
    </div>
</div>
<div class="row">


    <div class="three columns indent-bot-15 ">

        <?php echo $form->labelEx($model, 'idResponsible'); ?>

        <?php

        if ($model->canChangeResponsible(Yii::app()->user->user())) {
            echo $form->dropDownList($model, 'idResponsible', CHtml::listData(User::model()->findAll(), 'id', 'fullname'));
        } else {
            echo $form->dropDownList($model, 'idResponsible', CHtml::listData(User::model()->findAll(), 'id', 'fullname'), array('disabled' => 'disabled'));
        }

        ?>
        <?php echo $form->error($model, 'idResponsible'); ?>
    </div>

    <div class="three columns">
        <?php echo $form->dropDownListRow($model, 'priority', Project::getPriorityList()); ?>
    </div>
    <div class="three columns">

        <?php echo $form->labelEx($model, 'yearQuartal'); ?>
        <?php echo $form->dropDownList($model, 'yearQuartal', $model->getYearQuartalVariants()); ?>
        <?php echo $form->error($model, 'yearQuartal'); ?>
    </div>
    <div class="three columns">

    </div>

</div>


<div class="row">
    <div class="three columns">
        <?php echo $form->labelEx($model, 'idCompanyConsumer'); ?>
        <?php echo CHtml::textField('companyConsumerText', $model->companyConsumer->fullName, array('disabled' => 'disabled')); ?>
    </div>

    <div class="three columns">
        <?php echo $form->labelEx($model, 'innerName'); ?>
        <?php echo $form->textField($model, 'innerName', array('size' => 60, 'maxlength' => 64)); ?>
        <?php echo $form->error($model, 'innerName'); ?>
    </div>
    <div class="three columns">

    </div>
    <div class="three columns">

    </div>
</div>

<?php $this->sidebarToolbox = $this->renderPartial('tabs/_mainToolbox', array(), true); ?>

<?php
$this->widget('ext.nrelation.single', array(
    'model' => $model,
    'attribute' => 'idCompanyBuyer',
    'relation' => 'companyBuyer',
    'fields' => 'fullNameUrl',
    'allowEmpty' => true,
    'emptyText' => 'Отсутствует',
    'ajaxUrl' => Yii::app()->createUrl('api/getCompany'),
)); ?>


<?php $this->renderPartial("localWidgets/_consumers", array('form' => $form, 'model' => $model)); ?>






