<?php if (Yii::app()->user->id==$model->idResponsible || Permission::isLoggedUserSuperadmin()) { ?>
<h5>Дополнительный список пользователей, кто имеет доступ к проекту</h5>

<?php $this->mGridViewDataProvider->renderMGridView('atomic-project-permissions', $model); ?>

<?=
CHtml::link('Дать разрешение', array('project/createRbac','id'=>$model->idProject), array('class' => ' button small secondary foundationModalTrigger')) ?>

<?php } else { ?>
    <i> Нет прав на редактирование данной информации </i>
<?php } ?>
