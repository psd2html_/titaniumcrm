<?php
/**
 * @var Project $model
 */
?>

<?php
$form = $this->beginWidget('ext.foundation.widgets.ReadonlyForm', array(
    'action' => Yii::app()->createUrl("/project/setAttrJson"),
    'id' => 'editable-logistic-form',
    'enableAjaxValidation' => !true,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
));
$uncertainColouring = array('emptyOption' => 2);
?>

<table>
    <tbody>
    <tr>
        <td width="33%">
            <?php echo $form->textFieldRow($model, 'contractNumber'); ?>
        </td>
        <td width="33%">
            <?php
            $contragents = array();
            $contragents[] = "Не определен";
            $contragents = $contragents + CHtml::listData(Contragent::model()->findAll(), 'idContragent', 'name');
            ?>
            <?php echo $form->dropDownListRow($model->logistic, 'idContragent', $contragents); ?>
        </td>
        <td>
            <?php echo $form->textFieldRow($model, 'sellIncotermsCondition'); ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php echo $form->label($model, 'contractDate'); ?>
            <?php echo $form->dateField($model, 'contractDate'); ?>
        </td>
        <td>
            <?php echo $form->label($model->logistic, 'shipmentDeadline'); ?>
            <?php echo $form->dateField($model->logistic, 'shipmentDeadline'); ?>
        </td>
        <td>
            <?php echo $form->textAreaRow($model, 'actualDeliveryAddress', array('rows' => 4)); ?>
        </td>
    </tr>

    <tr>
        <td>
            <div class="row">
                <div class="eight columns">
                    <?php echo $form->textFieldRow($model, 'contractTotal', null, "currency:''"); ?>
                </div>
                <div class="four columns">
                    <?php echo $form->dropDownListRow($model, 'contractTotalCurrency', Order::getCurrencies()); ?>
                </div>
            </div>
        </td>

        <td>
            <?php echo $form->textFieldRow($model, 'payedByUser'); ?>
        </td>
        <td>
            <?php echo $form->textAreaRow($model, 'consigneeAddress'); ?>
        </td>
    </tr>
    </tbody>
</table>


<h6>Контракт с покупателем</h6>
<?php echo $form->uploadField($model, 'idContractUpload','contractUpload',Yii::app()->createUrl("/project/manageLogisticUpload",array('id'=>$model->idProject, 'idL'=>0, 'type'=>UploadForm::CONTRACT))); ?>

<?php
$this->endWidget();
?>
<?php $this->renderPartial("briefViewTabs/_ro/_logisticPosition", array("model" => $model)) ?>

<hr/>

<h5>Продвижение</h5>
<?php $this->mGridViewDataProvider->renderMGridViewReadOnlyMode('lf-grid', $model, false); ?>
