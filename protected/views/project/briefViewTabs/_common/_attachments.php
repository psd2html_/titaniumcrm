<?php
/**
 * @var Project $model
 */
?>

<?php
$this->angularJsDispatcher->injectDependency("angularBootstrapNavTree");
$this->angularJsDispatcher->injectDependency("dropzone");
Yii::app()->clientScript->registerScriptFile(Yii::app()->getBaseUrl() . "/js/controllers/abnController.js", CClientScript::POS_BEGIN);
$idProject = $model->idProject;

$currentUser = Yii::app()->user->id;
$totalRights = !$isReadOnly;
?>
<div
    ng-init="csrfToken='<?= Yii::app()->request->csrfToken ?>'; csrfName='<?= Yii::app()->request->csrfTokenName ?>'; idProject=<?= $idProject ?>; updateTree();"
    ng-controller="AbnTestController">

    <div class="alert-box" ng-show="movableFile">Нажмите на папку, что переместить файл {{movableFile.n}} в нее</div>
    <hr>
    <div class="row">
        <div class="three columns">
            <div class="panel">
                <abn-tree tree-data="fileTree" control="treeControl" on-select="my_tree_handler(branch)"
                          expand-level="2"></abn-tree>
                <?php if ($totalRights) { ?>
                <a class="small secondary button" ng-click="newFolder(false)"><i class="fa fa-folder-o"></i> Создать новую
                    папку</a>
                <?php } ?>
                <a class="small secondary button" href="<?= Yii::app()->createUrl("/project/downloadArchive", array('id'=>$model->idProject)) ?>"><i class="fa fa-download"></i> Загрузить архив</a>
            </div>
        </div>
        <div class="nine columns">
            <span ng-show="updatingFiles"><?= Helpers::getSpinnerGif() ?></span>

            <div ng-show="currentDirId>-1&&!updatingFiles">
                <div <?php if ($totalRights) { ?>dropzone<?php } ?> project-id="<?= $idProject ?>" upload-action="/upload/store"
                     success-callback="updateFiles()"
                     location="currentDirId"
                     csrf-name="<?= Yii::app()->request->csrfTokenName ?>"
                     csrf-token="<?= Yii::app()->request->csrfToken ?>">
                    <div class="right">
                        <?php if ($totalRights) { ?>
                            <a class="small secondary button"
                               ng-click="newFolder(true, currentDirId)"><i class="fa fa-folder-o"></i> Создать подпапку</a>
                            &nbsp;&nbsp;
                            <a class="button small secondary small" ng-click="renameFolder(currentDirId)"
                               ng-show="selectedFolderData.type>=10"><i class="fa fa-pencil"></i> Переименовать</a>
                            <a class="button small alert small" ng-click="deleteFolder(currentDirId)"
                               ng-show="selectedFolderData.type>=10"><i class="fa fa-trash-o"></i> Удалить</a>

                        <?php } ?>
                    </div>
                    <h4>{{selectedFolder}}</h4>

                    <table class="table">
                        <thead>
                        <tr>
                            <th>Файл</th>
                            <th>Автор</th>
                            <th>Размер</th>
                            <th>Дата</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-show="files.length==0">
                            <td colspan="6">
                                <i>Файлов нет</i>
                            </td>
                        </tr>
                        <tr ng-repeat="f in files">
                            <td>{{f.c?(f.c+' ('+f.n+')'):f.n}}</td>
                            <td>{{f.a}}</td>
                            <td>{{f.s | bytes}}</td>
                            <td>{{f.date | date}}</td>
                            <td>

                                <a class="small secondary button" title="Скачать"
                                   ng-href="/upload/download?p=<?= $idProject ?>&f={{currentDirId}}&h={{f.uid}}&i={{f.id}}"><i
                                        class="fa fa-download"></i></a>
                                <?php if ($totalRights) { ?>
                                <a class="secondary small button"
                                   ng-show="f.u==<?= $currentUser ?>||<?= $totalRights ?>" title="Переместить"
                                   ng-click="moveFile(f)"><i class="fa fa-arrows-h"></i></a>
                                <a class="secondary small button"
                                   ng-show="f.u==<?= $currentUser ?>||<?= $totalRights ?>" title="Переименовать"
                                   ng-click="renameFile(f)"><i class="fa fa-pencil"></i></a>
                                &nbsp;&nbsp;
                                <a class="alert small button" title="Удалить"
                                   ng-show="f.u==<?= $currentUser ?>||<?= $totalRights ?>"
                                   ng-click="deleteFile(f)"><i class="fa fa-trash-o"></i></a>
                                <?php } ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
