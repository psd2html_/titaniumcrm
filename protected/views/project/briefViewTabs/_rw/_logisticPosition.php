<?php
/**
 * @var Project $model
 */
?>

<h5>Список позиций к поставке:</h5>
<?php $this->mGridViewDataProvider->renderMGridView('logisticPosition-brief-grid', $model, false); ?>
<div class="footer_content">
    <a class="button secondary small dwDialogTrigger" href='<?= $this->createUrl("project/createLogisticPosition/",array('id'=>$model->idProject ))?> '>Добавить позицию</a>
</div>
<br/>
<dl class="tabs">
    <?php $first = true;
    foreach ($model->logisticPositions as $lmodel) {
        ?>
        <dd <?= $first ? 'class="active"' : '' ?>><a
                href="#logisticPosition<?= $lmodel->idLogisticPosition ?>"><?= $lmodel->name ?></a></dd>
        <?php $first = false;
    } ?>
</dl>

<ul class="tabs-content">
    <?php $first = true;
    foreach ($model->logisticPositions as $lmodel) {
        ?>
        <li <?= $first ? 'class="active"' : '' ?> id="logisticPosition<?= $lmodel->idLogisticPosition ?>Tab">
            <?php
            $form = $this->beginWidget('ext.foundation.widgets.NGActiveForm', array(
                'action' => Yii::app()->createUrl("/project/setAttrJson"),
                'id' => 'editable-logisticPosition-' . $lmodel->idLogisticPosition . '-form',
                'enableAjaxValidation' => !true,
                'htmlOptions' => array('enctype' => 'multipart/form-data'),
            ));
            $uncertainColouring = array('emptyOption' => 2);
            ?>

            <table>
                <tbody>
                <tr>
                    <td width="33%"> <?php echo $form->textFieldRow($lmodel, 'poNumber'); ?></td>
                    <td width="33%"><?php echo $form->textFieldRow($lmodel, 'goodsNameEng'); ?></td>
                    <td width="33%"><?php echo $form->textFieldRow($lmodel, 'goodsNameRus'); ?></td>
                </tr>
                <tr>
                    <td>   <?php echo $form->dropDownListRow($lmodel, 'isPurchaseViaManufacturer', array(0 => 'Посредник', '1' => 'Производитель', '2' => 'Неизвестно'), $uncertainColouring); ?> </td>
                    <td>
                        <?php echo $form->label($lmodel, 'productionDeadline'); ?>
                        <?php echo $form->dateField($lmodel, 'productionDeadline'); ?>
                    </td>
                    <td>
                        <?php
                        $insuranceCompanies = array();
                        $insuranceCompanies[] = "Не определена";
                        $insuranceCompanies = $insuranceCompanies + CHtml::listData(InsuranceCompany::model()->findAll(), 'idInsuranceCompany', 'name');
                        ?>
                        <?php echo $form->dropDownListRow($lmodel, 'idInsuranceCompany', $insuranceCompanies); ?>

                    </td>
                </tr>

                <tr>
                    <td><?php echo $form->textFieldRow($lmodel, 'goodsOrigin'); ?></td>
                    <td><?php echo $form->textAreaRow($lmodel, 'packagingSpecification', array('rows' => 4)); ?></td>
                    <td><?php echo $form->dropDownListRow($lmodel, 'isInsurancePayed', array(0 => 'Нет', '1' => 'Есть', '2' => 'Неизвестно'), $uncertainColouring); ?> </td>
                </tr>
                <tr>
                    <td><?php echo $form->textFieldRow($lmodel, 'buyIncotermsCondition'); ?></td>
                    <td><?php echo $form->textAreaRow($lmodel, 'takeoutAddress'); ?> </td>
                    <td><?php echo $form->dropDownListRow($lmodel, 'isTechnoinfoUnloadsGoods', array(0 => 'Заказчик', '1' => 'Техноинфо', '2' => 'Неизвестно'), $uncertainColouring); ?>
                    </td>
                </tr>
                <tr>
                    <td width="33%">
                        <?php echo CHtml::label('<span class="hinted" title="Не делает: Если в цену производителя входил VAT (налог на внутреннем рынке), то он подлежит возврату Technoinfo Ltd. Для этого мы выступаем экспортерами товара за пределы ЕС/США/etc. и сами декларируем товар к вывозу.

Делает: Если производитель изначально отпустил товар по цене экспортного - он обычно выступает за то, чтобы открыть эскпортную декларацию на себя, чтобы отчитаться в налоговой и с его duty-free прибыли не было удержаний.

Либо: Он потребует доказательство экспорта от Technoinfo Ltd., что нежелательно.">Делает ли производитель экспортную декларацию  <i class="fa fa-question-circle"></i></span>', "Project_isExportDeclarationMade"); ?>


                        <?php echo $form->dropDownList($lmodel, 'isExportDeclarationMade', array(0 => 'Нет', '1' => 'Да', '2' => 'Неизвестно'), $uncertainColouring); ?>
                    </td>
                    <td></td>
                    <td><?php echo $form->dropDownListRow($lmodel, 'isSpecialDeliveryRequired', array(0 => 'Нет', '1' => 'Да', '2' => 'Неизвестно'), $uncertainColouring); ?>
                    </td>
                </tr>
                <tr>
                    <td>

                        <?php echo $form->dropDownListRow($lmodel, 'isExportDoubleApplicationLicenseRequired', array(0 => 'Нет', '1' => 'Да', '2' => 'Неизвестно'), $uncertainColouring); ?>
                    </td>
                    <td>

                    </td>
                    <td>
                        <?php echo CHtml::label("Код ТН ВЭД " . CHtml::link("(<i class='fa fa-book'></i> Справочник кодов)", "http://www.tks.ru/db/tnved/tree", array("target" => '_blank')), false);
                        echo $form->textField($lmodel, 'tnvedCod');
                        echo $form->error($lmodel, 'tnvedCod');
                        ?>
                    </td>
                </tr>
                <tr>
                    <td><label>Упаковочный</label>
                        <?php echo $form->uploadField($lmodel, 'idPackingSheetUpload', 'packingSheetUpload', Yii::app()->createUrl("/project/manageLogisticUpload", array('id' => $model->idProject, 'idL' => $lmodel->idLogisticPosition, 'type' => UploadForm::PACKING_SHEET))); ?>
                    </td>
                    <td><label>Описание</label>
                        <?php echo $form->uploadField($lmodel, 'idDescriptionUpload', 'descriptionUpload', Yii::app()->createUrl("/project/manageLogisticUpload", array('id' => $model->idProject, 'idL' => $lmodel->idLogisticPosition, 'type' => UploadForm::DESCRIPTION))); ?>
                    </td>
                    <td></td>
                </tr>
                </tbody>
            </table>

            <?php
            $this->endWidget();
            ?>
        </li>
        <?php $first = false;
    } ?>
</ul>