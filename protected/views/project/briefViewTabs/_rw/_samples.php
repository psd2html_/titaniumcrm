<?php
/**
 * @var Project $model
 */
?>

<?php $this->mGridViewDataProvider->renderMGridView('samples-grid', $model, false); ?>

<?=
CHtml::link("Создать новый образец", Yii::app()->createUrl('sample/create', array('Sample[idProject]' => $model->idProject,
    'returnUrl' => Yii::app()->request->getRequestUri()
)), array('class' => 'small secondary button'));
?>




