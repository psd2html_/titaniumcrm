<?php
/**
 * @var Project $model
 */
?>

<?php $this->mGridViewDataProvider->renderMGridView('spent-grid', $model, false); ?>
<div class="footer_content">
    <a class="button secondary dwDialogTrigger small" href='<?= $this->createUrl("spent/create",array('Spent[idProject]'=>$model->idProject ))?> '>Создать новую трату</a>
</div>