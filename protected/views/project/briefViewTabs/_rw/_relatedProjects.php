<?php
/**
 * @var Project $model
 */
?>

<?php
if ($model->parent)
    $this->mGridViewDataProvider->renderMGridView('project-grid', $model, false, false, "linked-projects-slave");
else
    $this->mGridViewDataProvider->renderMGridView('project-grid', $model, false, false, "linked-projects");
?>
<?php
if ($model->childrenProjects) {
    echo '&nbsp' . CHtml::link('Редактировать группу', Yii::app()->createUrl('project/masterAndLinkedProjects', array('id' => $model->idProject)), array('class' => 'button'));
}
if ($model->parent) {
    echo '&nbsp' . CHtml::link('Редактировать группу', Yii::app()->createUrl('project/masterAndLinkedProjects', array('id' => $model->parent)), array('class' => 'button'));
} else {
    if ($model->permission == Permission::PERMISSION_READWRITE) {
        echo '&nbsp' . CHtml::link('Добавить проекты в группу к данному проекту', Yii::app()->createUrl('project/linkProjects', array('id' => $model->idProject)), array('class' => 'button'));
    }
}
?>