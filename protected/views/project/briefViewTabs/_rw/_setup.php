<?php
/**
 * @var Project $model
 */
?>

<?php
$form = $this->beginWidget('ext.foundation.widgets.NGActiveForm', array(
    'action' => Yii::app()->createUrl("/project/setAttrJson"),
    'id' => 'editable-logistic-form',
    'enableAjaxValidation' => !true,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
));
$uncertainColouring = array('emptyOption' => 2);
?>

<table>
    <tbody>
    <tr>
        <td width="33%">
            <label>Замер помещения</label>
            <?php echo $form->uploadField($model, 'idRoomMeasurementUpload','roomMeasurementUpload',Yii::app()->createUrl("/project/manageLogisticUpload",array('id'=>$model->idProject, 'idL'=>0, 'type'=>UploadForm::ROOM))); ?>
        </td>
        <td width="33%">
            <label>АКТ об установке</label>
            <?php echo $form->uploadField($model, 'idSetupReportUpload','setupReportUpload',Yii::app()->createUrl("/project/manageLogisticUpload",array('id'=>$model->idProject, 'idL'=>0, 'type'=>UploadForm::SETUP))); ?>
        </td>
        <td>
            <?php echo $form->label($model, 'setupDate'); ?>
            <?php echo $form->dateField($model, 'setupDate'); ?>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <?php echo $form->textAreaRow($model, 'setupComment', array('rows' => 4)); ?>
        </td>
    </tr>

    </tbody>
</table>

<?php
$this->endWidget();
?>
