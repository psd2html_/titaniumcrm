<?php
/**
 * @var Project $model
 */
?>

<?php

$m = new Order();
$m->unsetAttributes();
$m->idProject = $model->idProject;

$dataProvider = $m->search();
$dataProvider->pagination->route = $model->getViewUrl();

$this->widget('ext.relation.relation', array(
    'dataProvider' => $dataProvider,
    'model' => $m,
    'widgetType' => 5,
    'relation' => 'orders',
    'fields' => array(
        array(
            'name' => 'date',
            'value' => 'Yii::app()->dateFormatter->formatDateTime($data->date,"medium",null)',
        ),
        array(
            'name' => 'name',
            'value' => '$data->name',
            'type' => 'html'
        ),
        'comment',
        array(
            'name' => 'outputPrice',
            'value' => '$data->outputPrice',
            'header' => 'Итого к оплате',
            'filter' => '',
            'type' => 'html'
        ),
        array('class' => 'CButtonColumn',
            // 'template' => '{downloadPdf}{downloadDoc}{preview}',
            'template' => '{downloadDoc}{preview}',
            'htmlOptions' => array('style' => 'width:140px'),
            'buttons' => array(
//                'downloadPdf' => array(
//                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_pdf.png',
//                    'options' => array('class' => 'button secondary tiny '),
//                    'url'=>'Yii::app()->createUrl("/googleDocs/downloadPdf/",array("id"=>$data->idOrder))',
//                ),
                'downloadDoc' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/Word-icon.png',
                    'options' => array('class' => 'button secondary tiny '),
                    'title' => 'Скачать DOCX',

                    'url' => 'Yii::app()->createUrl("/googleDocs/download/",array("id"=>$data->idOrder))',
                ),
                'preview' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_preview.png',
                    'options' => array('class' => 'button secondary tiny update foundationModalTrigger', 'clickToClose' => 'true'),
                    'url' => 'Yii::app()->createUrl("order/overview",array("id"=>$data->idOrder))',
                ),
            ),
        ),
    ),
    'actionParams' => array('Order[idProject]' => $model->idProject,
        'returnUrl' => $model->getViewUrl(array ('#' => 'orders'))),
));




