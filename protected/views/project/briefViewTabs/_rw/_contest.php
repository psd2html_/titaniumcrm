<?php
/**
 * @var Project $model
 */
?>

<?php
$form = $this->beginWidget('ext.foundation.widgets.NGActiveForm', array(
    'action' => Yii::app()->createUrl("/project/setAttrJson"),
    'id' => 'editable-contest-form',
    'enableAjaxValidation' => !true,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
));

?>
<table>
    <tr>
        <td colspan="4">
            <h5>Информация о конкурсе</h5>
        </td>
    </tr>
    <tr>
        <td> <?php echo $form->textFieldRow($model->contest, 'contestUrl', array('size' => 60, 'maxlength' => 255)); ?></td>
        <td> <?php echo $form->textFieldRow($model->contest, 'contestSiteUrl', array('size' => 60, 'maxlength' => 255)); ?></td>
        <td>  <?php echo $form->textFieldRow($model->contest, 'number', array('size' => 60, 'maxlength' => 255)); ?> </td>
        <td>  <?php echo $form->dropDownListRow($model, 'enumProvision', Project::getEnumProvisionList(),array('emptyOption'=>0)); ?></td>
    </tr>
    <tr>
        <td>
            <div class="row">
                <div class="eight columns">
                    <?php echo $form->textFieldRow($model->contest, 'costLot', null, "currency:''"); ?>
                </div>
                <div class="four columns">
                    <?php echo $form->dropDownListRow($model->contest, 'costLotCurrency', Order::getCurrencies()); ?>
                </div>
            </div>
        </td>
        <td>
            <div class="row">
                <div class="eight columns">
                    <?php echo $form->textFieldRow($model->contest, 'costContract', null, "currency:''"); ?>
                </div>
                <div class="four columns">
                    <?php echo $form->dropDownListRow($model->contest, 'costContractCurrency', Order::getCurrencies()); ?>
                </div>
            </div>
        </td>
        <td colspan="1">
            <div class="row">
                <div class="eight columns">
                    <?php echo $form->textFieldRow($model->contest, 'costAsk', null, "currency:''"); ?>
                </div>
                <div class="four columns">
                    <?php echo $form->dropDownListRow($model->contest, 'costAskCurrency', Order::getCurrencies()); ?>
                </div>
            </div>
            </td>
        <td colspan="1">

        </td>
    </tr>
    <tr>
        <td colspan="4">
            <h5>Ключевые даты</h5>
        </td>
    </tr>
    <tr>
        <td width="25%">
            <?php echo $form->dateFieldRow($model->contest, 'datePost'); ?>
        </td>
        <td width="25%">
            <?php echo $form->dateFieldRow($model->contest, 'dateRequest'); ?>
        </td>
        <td width="25%">
            <?php echo $form->dateFieldRow($model->contest, 'dateResponse'); ?>
        </td>
        <td width="25%">
            <?php echo $form->dateFieldRow($model->contest, 'dateContest'); ?>
        </td>
    </tr>
</table>


<?php
$this->endWidget();
?>
