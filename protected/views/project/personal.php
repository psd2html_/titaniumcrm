<?php

if ($model->parent) {
    $this->breadcrumbs = array(
        Yii::t('app', 'Projects') => array('index'),
        $model->parentProject->name => $model->parentProject->getViewUrl(),
        $model->name,
    );
} else {
    $this->breadcrumbs = array(
        Yii::t('app', 'Projects') => array('index'),
        $model->name,
    );
}
$this->angularJsDispatcher->injectDependency('ui.bootstrap');
$isLogist = Permission::isLoggedUserLogist();
$writable = $model->isWritable();
?>


<?php Yii::app()->clientScript->registerScriptFile("/js/revealActivate.js", CClientScript::POS_END); ?>

<?php /* @var $model Project */ ?>


<div class="row">
<div class="seven columns">
    <div class="row">
        <div class="four columns">
            <h4>Сводка
                по <?= $writable ? CHtml::link($model->projectName . ' ' . CHtml::image(Yii::app()->getBaseUrl() . '/images/pencil.png', 'Редактировать'), $model->getUpdateUrl()) : $model->projectName ?></h4>

            <?php
            $data = $model->getCBOX() . ' ' . $model->getStatus() . '; ' . $model->getPriorityColumn();
            echo $writable ?
                CHtml::link($data . ' ' . CHtml::image(Yii::app()->getBaseUrl() . '/images/pencil.png', 'Редактировать'), array('project/chooseNextWorkflow', 'id' => $model->idProject), array('class' => 'foundationModalTrigger'))
                :
                $data; ?> <br/><br/>

            <?php
            $form = $this->beginWidget(!$writable ? 'ext.foundation.widgets.ReadonlyForm' : 'ext.foundation.widgets.NGActiveForm', array(
                'action' => Yii::app()->createUrl("/project/setAttrJson"),
                'id' => 'editable-brief-project-form',
                'enableAjaxValidation' => !true,
                'htmlOptions' => array('enctype' => 'multipart/form-data'),
            ));
            ?>

            <strong>Ответственный: </strong> <?= CHtml::link(Helpers::getUserIcon() . ' ' . $model->responsible->fullname, "#") ?>
            <br/>
            <?= Helpers::reutrnTitleAndValueIfNotNull('Класс прибора', $model->coreInstrumentClass->name); ?>
            <?php
            if (($model->manufacturer)) {
                echo Helpers::reutrnTitleAndValueIfNotNull('Производитель', $model->manufacturer->name);
            }
            ?>
            <?php
            if (($model->coreInstrument)) {
                echo Helpers::reutrnTitleAndValueIfNotNull('Базовый прибор', $model->coreInstrument->name);
            } elseif ($model->virtualCoreInstrument) {
                echo Helpers::reutrnTitleAndValueIfNotNull('Позиция', $model->virtualCoreInstrument);
            }
            ?>
            <strong>Год/квартал закупки</strong>
            <?php echo $form->dropDownList($model, 'yearQuartal', $model->getYearQuartalVariants()); ?>
            <?php $this->endWidget(); ?>
        </div>
        <div class="seven columns">
            <?php
            $showPrefix = false;
            if (($model->companyBuyer) && ($model->companyConsumer)) {
                $showPrefix = true;
            }
            foreach (array('companyBuyer', 'companyConsumer') as $name) {
                if ($c = $model->getRelated($name)) {
                    ?>
                    <div class="more">
                        <h6><?= $showPrefix ? ($model->getAttributeLabel($name) . ': ') : '' ?> <?= ($c->fullName . ', ' . $c->city /* , array('company/update', 'id' => $c->idCompany) */) ?>  </h6>
                    <span>
                        <?= CHtml::link("Перейти к организации", $c->viewUrl) ?> <br/>
                        <?= Helpers::reutrnTitleAndValueIfNotNull('Телефон', $c->phone); ?>
                        <?= Helpers::reutrnTitleAndValueIfNotNull('Доп.Телефон', $c->addPhone); ?>
                        <?= Helpers::reutrnTitleAndValueIfNotNull('Физ.Адрес', $c->physicalAddress); ?>
                        <?= Helpers::reutrnTitleAndValueIfNotNull('Юр.Адрес', $c->legalAddress); ?>
                        <?= Helpers::reutrnTitleAndValueIfNotNull('Тип организации', $c->companyClass ? $c->companyClass->name : 'N/A'); ?>
                        <?= Helpers::reutrnTitleAndValueIfNotNull('Email', $c->email); ?>

                    </span>
                    </div>
                <?php
                }
            }
            ?>
            <?php
            $customers = $model->getCustomers();

            foreach ($customers as $c) {
                $label = $c->customerComment;
                ?>
                <div class="more">
                    <h6> <?= ($c->lastName . ' ' . $c->firstName . ' ' . $c->middleName . ' ') /* , array('customer/update', 'id' => $c->idCustomer) */ ?> <?= $label ? '(' . $label . ')' : '' ?>  </h6>
                <span>
                    <?= CHtml::link("Перейти к клиенту", $c->viewUrl) ?> <br/>
                    <?= $c->getContactInfo(); ?>

                </span>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
    <?php if ($model->isVisibleSubzone(Group::SUBZONE_PROJECT_TASK)) { ?>

        <script>
            function ProjectBriefViewController($scope, $http) {
                $scope.updatingData = false;
                $scope.updateNotes = function () {
                    $scope.updatingData = true;
                    $http.get("/project/getProjectComments/" + $scope.projectId).success(function (struct) {
                        $scope.projectNotes = struct.p;
                        $scope.updatingData = false;
                    })
                }
                $scope.projectNotes = [];

                $scope.limittingAmount = 3;
                $scope.increaseLimittingAmount = function () {
                    $scope.limittingAmount = $scope.limittingAmount + 3;
                }
                $scope.status = "3";
                $scope.showStatus = function () {
                    var selected = $filter('filter')($scope.statuses, {value: $scope.user.status});
                    return ($scope.user.status && selected.length) ? selected[0].text : 'Not set';
                };

                $scope.init = function () {
                    $scope.updateNotes();
                }
                $scope.editComment = function (id) {
                    window.dwDialog.loadAjax("/sticky/update/" + id, true, true);
                }
            }


        </script>
        <hr/>

        <div class="row">
            <div class="six columns">
                <?php $this->mGridViewDataProvider->renderMGridView('sticky-grid', $model); ?>

            </div>
            <div class="six columns">

                <div ng-controller="ProjectBriefViewController" ng-init="projectId=<?= $model->idProject ?>; init()">

                    <span ng-show="updatingData"> <?= Helpers::getSpinnerGif() ?> </span>
                    <table>
                        <thead>
                        <tr>
                            <th>Комментарии</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="note in projectNotes| limitTo:limittingAmount">
                            <td>
                                <small>{{ note.d|date:'longDate' }}</small>
                                <?php if ($model->isSubzoneWritable(Group::SUBZONE_PROJECT_TASK)) { ?>
                                    <button ng-click="editComment(note.id)"><i class="fa fa-pencil"></i></button>
                                <?php } ?>
                                <div ng-bind-html="note.t"></div>
                            </td>
                        </tr>
                        <tr ng-show="projectNotes.length>limittingAmount">
                            <td>
                                <a href="#" ng-click="increaseLimittingAmount()">...</a>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>

        </div>

    <?php } ?>
    <hr/>


    <?=
    $model->isSubzoneWritable(Group::SUBZONE_PROJECT_ORDER) ? CHtml::link('Создать коммерческое предложение', array('order/create', 'Order[idProject]' => $model->idProject,
        'returnUrl' => $model->getViewUrl(array('#' => 'orders'))), array('class' => 'button small secondary')) : "";
    ?>

    &nbsp;
    <?php if ($model->isSubzoneWritable(Group::SUBZONE_PROJECT_TASK)) { ?>
        <a class="button secondary small foundationModalTrigger"
           href='<?= $this->createUrl("sticky/create", array('Sticky[idProject]' => $model->idProject, 'Sticky[isTask]' => 1)) ?> '>Создать
            задачу</a>
        <a class="button secondary small foundationModalTrigger"
           href='<?= $this->createUrl("sticky/create", array('Sticky[idProject]' => $model->idProject, 'Sticky[isTask]' => 0)) ?> '>Создать
            комментарий</a>
    <?php } ?>

    <?php if (false) { ?>
        <span href="#" class="small secondary button dropdown ">
                Создать событие
                <ul>
                    <?php
                    $lis = Event::getEventClassList();
                    //$lis = array_slice($lis, 0, 2);
                    foreach ($lis as $key => $value) {
                        ?>
                        <li><?=
                            CHtml::link($value, Yii::app()->createUrl('event/create', array('Event[idProject]' => $model->idProject,
                                'returnUrl' => Yii::app()->request->getRequestUri(),
                                'Event[eventClass]' => $key
                            )));
                            ?></li>
                    <?php } ?>
                </ul>
            </span>
    <?php } ?>
    <br/><br/><br/>
</div>
<div class="five columns">
    <?php
    $dates = $model->getCalendarEvents(false, Yii::app()->request->getRequestUri());

    /* @var $model project */
    $this->widget('application.extensions.profullcalendar.ProFullcalendarGraphWidget', array(
            'data' => $dates,
            'allowQtip' => true,
            'template' => 'vertical',
            'options' => array(
                'editable' => !true,
                'aspectRatio' => 2,
                'contentHeight' => '400',
            ),
            'htmlOptions' => array(
                'style' => 'margin: 0 auto;',
            ),
        )
    );
    ?>

</div>
</div>

<?php
$tabs = $model->getTabs();
$defaultActive = false;


$defaultActive = Group::getStartingTab();


if ($defaultActive === Null) {
    $defaultActive = key($tabs);
}

?>

<dl class="tabs">
    <?php foreach ($tabs as $key => $tab) {
        if (($tab['visible'])) continue;
        ?>
        <dd  <?= $defaultActive == $key ? 'class="active"' : '' ?>>
            <a href="#<?= $key ?>"><?= $tab['readonly'] ? "<i class='fa fa-lock'></i> " : "" ?><?= $tab['label'] ?></a>
        </dd>
    <?php } ?>
</dl>

<ul class="tabs-content">
    <?php foreach ($tabs as $key => $tab) {
        if ($tab['visible']) continue; ?>
        <li <?= $defaultActive == $key ? 'class="active"' : '' ?> id="<?= $key ?>Tab">
            <?php $this->renderPartial("briefViewTabs/" . $tab['mod'] . "/_" . $key, array("model" => $model, "isReadOnly"=> $tab['readonly'])); ?>
        </li>
    <?php } ?>
</ul>

<?php
$this->widget('ext.slidetoggle.ESlidetoggle', array(
    'itemSelector' => 'div.more',
    'titleSelector' => 'div.more h6',
    'collapsed' => 'div.more',
    'duration' => 'fast'
));

Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/jquery.qtip.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.qtip.js', CClientScript::POS_END);
?>
