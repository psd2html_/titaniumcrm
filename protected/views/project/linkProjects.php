<?php Yii::app()->clientScript->registerScriptFile("/js/revealActivate.js", CClientScript::POS_END); ?>
<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Projects') => array('index'),
    $masterModel->name => array('view', 'id' => $masterModel->idProject),
    Yii::t('app', 'Projects')
);
?>



<header> <h3> Добавить дочерние проекты </h3> </header>

<?php $form=$this->beginWidget('CActiveForm', array(
    'enableAjaxValidation'=>true,
)); ?>

<?php


$this->widget('MGridView', array(
    'id' => 'project-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'cssFile' => false,
    'template' => '{items} {pager}',
    'columns' => array(),
    'insertColumns'=> array(array(
        'id' => 'selectedProjectsId',
        'class' => 'CCheckBoxColumn',
        'selectableRows' => '50',
    ))
));
?>

<?php echo CHtml::submitButton('Добавить отмеченные проекты к данному проекту',array('class'=>'button')); ?>

<?php $this->endWidget(); ?>

