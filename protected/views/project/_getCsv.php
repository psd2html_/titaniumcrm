<?php
$this->widget('CSVGridView', array(
        'id' => 'project-grid',
        'dataProvider' => $projectDataProvider,
        'filter' => $model,
        'cssFile' => false,
        'template' => '{items} {pager}',
        //'columns' => $columns
    )
);
