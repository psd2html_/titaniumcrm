<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Projects') => array('index'),
    Yii::t('app', 'Create'),
);?>
<header> <h3> Создать </h3> </header>



<div class="module_content">
    
<?php
$a=Permission::getAllowedManufacturers();
if (count($a)) {
$this->renderPartial('_formCreate', array(
			'model' => $model,
			'buttons' => 'create',
                        'manufacturers'=>$a));
}
else
{ ?>
К сожалению, у вас нет прав создавать проекты! :c
 <?php } ?>
</div>

