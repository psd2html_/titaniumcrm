<?php
$eventsType=  Event::getEventClassList();

$this->widget('MGridView', array(
    'id' => 'log-grid',
    'dataProvider' => $dataProvider,
    'readOnly'=>$readOnly,
    'filter' => $model,
    'cssFile' => false,
    'template' => '{items} {pager}',
    'columns' => array(
        array(
            'name' => 'creationDate',
            'value' => 'Yii::app()->dateFormatter->formatDateTime($data->creationDate, "full", "medium")',
            'filter' => '',
        ),
        array(
            'name' => 'idUser',
            'value' => '$data->user->fullname',
            'filter' => CHtml::listData(User::model()->findAll(),"id","fullname"),
        ),
        array(
            'name' => 'description',
        ),
        array(
            'name' => 'newValue',
            'type'=>'html'
        ),
    ),
));
