
<?php


$this->widget('MGridView', array(
    'id' => 'sticky-grid',
    'noPermalink'=>true,
    'dataProvider' => $dataProvider,
    'readOnly'=>$readOnly,
    'cssFile' => false,
    'template' => '{items} {pager}',
    'columns' => array(
        array('name' => 'date',
            'value' => '$data->getStickyHtml()',
            'type'=>'html',
            'header'=>'Задачи'
        ),
         /*array('name' => 'idUser',
            'value' => '$data->user->fullName',
            
        ),*/
        array(
            'updateButtonUrl' => 'Yii::app()->createUrl("sticky/update", array("id" => $data->getPrimaryKey()))',
            'deleteButtonUrl' => 'Yii::app()->createUrl("sticky/delete", array("id" => $data->getPrimaryKey()))',

            'class' => 'MButtonColumn',
            'template' => '{update}{delete}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options' => array('class' => 'button secondary tiny delete'),
//                    'url'=>'',
                ),
                'update' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options' => array('class' => 'button secondary tiny update foundationModalTrigger')
                ),
            ),
        ),
    ),
));
?>


