<?php

$this->widget('MGridView', array(
    'id' => 'lf-grid',
    'dataProvider' => $dataProvider,
    'readOnly'=>$readOnly,
    'cssFile' => false,
    'template' => '{items} {pager}',
    'columns' => array(
        'cityFrom',
        'cityTo',

        array('name' => 'dateUnload',
                'value'=>'Yii::app()->dateFormatter->formatDateTime($data->dateUnload,"medium", null)',
            ),
        array('name' => 'dateUpload',
            'value'=>'Yii::app()->dateFormatter->formatDateTime($data->dateUpload,"medium", null)'
        ),
        array('name' => 'enumTransport',
            'value' => '$data->enumTransportText',
            'filter'=>Logistic::getEnumTransportList()
        ),
        'comment',
        array('name' => 'attachments',
            'header' => 'Файлы',
            'value' => '$data->getAttachmentColumn()',
            'filter'=> '',
            'type' => 'html'
        ),
        array(
            'updateButtonUrl' => 'Yii::app()->createUrl("logisticFragment/update", array("id" => $data->getPrimaryKey()))',
            'deleteButtonUrl' => 'Yii::app()->createUrl("logisticFragment/delete", array("id" => $data->getPrimaryKey()))',

            'class' => 'MButtonColumn',
            'template' => '{update}{delete}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options' => array('class' => 'button secondary tiny delete')
                ),
                'update' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options' => array('class' => 'button secondary tiny dwDialogTrigger update')
                ),
            ),
        ),
    ),
));
?>
