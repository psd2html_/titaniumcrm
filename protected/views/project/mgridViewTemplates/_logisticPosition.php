<?php

$this->widget('MGridView', array(
    'id' => 'logisticPosition-brief-grid',
    'dataProvider' => $dataProvider,
    'readOnly'=>$readOnly,
    'cssFile' => false,
    'template' => '{items} {pager}',
    'columns' => array(

        array('name' => 'innerNumber',
        ),
        array('name' => 'goodsNameEng',
        ),
        array('name' => 'poNumber',
        ),
        array(
            'updateButtonUrl' => 'Yii::app()->createUrl("logisticFragment/update", array("id" => $data->getPrimaryKey()))',
            'deleteButtonUrl' => 'Yii::app()->createUrl("project/deleteLogisticPosition", array("id" => $data->getPrimaryKey()))',

            'class' => 'MButtonColumn',
            'template' => '{delete}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options' => array('class' => 'button secondary tiny delete')
                ),
                'update' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options' => array('class' => 'button secondary tiny dwDialogTrigger update')
                ),
            ),
        ),
    ),
));
?>

