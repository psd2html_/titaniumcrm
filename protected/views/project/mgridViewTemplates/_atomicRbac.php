<?php

$this->widget('MGridView', array(
    'id' => 'sticky-grid',
    'noPermalink'=>true,
    'dataProvider' => $dataProvider,
    'readOnly'=>$readOnly,
    'cssFile' => false,
    'template' => '{items} {pager}',
    'columns' => array(
        array(
            'name' => 'idUser',
            'value' => '$data->user?$data->user->fullname:"N/A"',
        ),

        array(
            'updateButtonUrl' => 'Yii::app()->createUrl("project/updateRbac", array("id" => $data->getPrimaryKey()))',
            'deleteButtonUrl' => 'Yii::app()->createUrl("project/deleteRbac", array("id" => $data->getPrimaryKey()))',

            'class' => 'MButtonColumn',
            'template' => '{update}{delete}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options' => array('class' => 'button secondary tiny delete'),
                ),
                'update' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options' => array('class' => 'button secondary tiny update foundationModalTrigger')
                ),
            ),
        ),
    ),
));
?>


