<?php

$this->widget('MGridView', array(
    'id' => 'spent-grid',
    'dataProvider' => $dataProvider,
    'cssFile' => false,
    'readOnly'=>$readOnly,
    'filter'=>$model,
    'template' => '{items} {pager}',
    'columns' => array(
        array('name' => 'amount',
            'value' => '$data->amountText'
        ),
        array('name' => 'isCashPayment',
            'value' => '$data->paymentType'),
        array('name' => 'enumPaymentGoal',
            'value' => '$data->paymentGoal',
            'filter'=>Spent::getPaymentGoalList()),
        array('name' => 'idUser',
            'value' => '$data->user->fullname',
            'filter'=>CHtml::listData(User::model()->findAll(),'id','fullname')),

        'reason',
        'date',
        array(
            'updateButtonUrl' => 'Yii::app()->createUrl("spent/update", array("id" => $data->getPrimaryKey()))',
            'deleteButtonUrl' => 'Yii::app()->createUrl("spent/delete", array("id" => $data->getPrimaryKey()))',
            
            'class' => 'MButtonColumn',
            'template' => '{update}{delete}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options' => array('class' => 'button secondary tiny delete')
                ),
                'update' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options' => array('class' => 'button dwDialogTrigger secondary tiny update')
                ),
            ),
        ),
    ),
));
?>

