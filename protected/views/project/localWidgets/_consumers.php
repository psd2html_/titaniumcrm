<fieldset>
    <legend>Контакты с заказчиком</legend>
<script>
    var autocompleteCustomer=null;
    function removeCustomer(e) {
        $(e).parent('').parent('').remove(); return false; 
    }

    function addCustomerToListFunction(val, link) {
        var Template = '<tr><td><input type="hidden" value="' + val + '" name="Customers[]" id="Customers">' + link + '</td><td><input type="text" value="" name="customerComment[]" id="customerComment"></td><td><a href="#" class="remove button secondary small" onclick="removeCustomer(this); return false;"><img alt="Удалить" src="/images/icn_trash.png"></a></td></tr>';
        $('#customerAutocompleteTable>tbody').append(Template);
    }

    window.afterCreation = function (arg)
    {
        addCustomerToListFunction(arg.idCustomer, arg.name);
    }

    window.dwDialog.callbackDispatcher.registerCallback("customer/create",true,window.afterCreation);

    function addCustomerFunction()
    {
        if (autocompleteCustomer!=null) {

            var val=autocompleteCustomer.id;
            var link='<?= CHtml::link("##TEMPLATE_NAME##", array('customer/update', 'id' => '##ID##')) ?>';
            link=link.replace("##TEMPLATE_NAME##",autocompleteCustomer.value).replace("##ID##", val.toString());
            addCustomerToListFunction(val, link);
        }

        return false;
    }

</script>


<table id="customerAutocompleteTable">

    <thead>

    <th width="40%">
        Контактное лицо
    </th>
    <th>
        Комментарий
    </th>
    <th width="70px"></th>
</thead>

<tbody>

    <?php
    $k = 0;
    $consumers = ($model->customers);
    foreach ($consumers as $customer) {
        ?>
        <tr> 
            <td >
                <?php
                $k++;
                echo CHtml::hiddenField("Customers[]", $customer->idCustomer);
                echo CHtml::link($customer->fullName, array('customer/update', 'id' => $customer->idCustomer));
                ?>
            </td>
            <td>
    <?php echo CHtml::textField('customerComment[]', $customer->customerComment); ?>
            </td>
            <td >
                <a href="#" class="remove button secondary small" onclick="removeCustomer(this); return false;">
                    <img alt="Удалить" src="<?= Yii::app()->getBaseUrl() ?>/images/icn_trash.png">    
                </a>
            </td>
        </tr>
<?php } ?>
</tbody>
</table>

<div class="row">
    
    <div class="five columns">
        Введите первые буквы фамилии
        <?php
        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            // 'model' => $model,
            'name' => ('autocomplete_id_' . $k),
            'value' => "",
            'source' => Yii::app()->createUrl('ajax/customerAjax'),
            // additional javascript options for the autocomplete plugin
            'options' => array(
                'select' => "js:function(event, ui) {
                                          //$('#Customer_id_" . $k . "').val(ui.item.id);
                                              window.autocompleteCustomer=ui.item;
                                        }",
                'change' => "js:function(event, ui) {
                                          if (!ui.item) {
                                            window.autocompleteCustomer=null;
                                          }
                                        }",
                'size' => 120,
                'showAnim' => 'fold',
            ),
        ));
        ?>
    </div>
    <div class="four columns">
        <br/><a href="#" class="remove button small" onclick="addCustomerFunction(); return false;"> Добавить в список </a>
        <a href="<?= Yii::app()->createUrl("customer/create", array('Customer[idCompany]'=>$model->idCompanyConsumer,'returnUrl' => Yii::app()->request->getRequestUri())) ?>" class="add button secondary small foundationModalTrigger" >Создать нового клиента</a>
    </div>    
    <div class="two columns">
        
    </div>
</div>
</fieldset>