<?php
/* @var $model Project  */
$readonly = $model->permission == Permission::PERMISSION_READ;
$groupedCheckpoint = $model->getGrouppedCheckpoints();
$ready = $model->checkpointEvents;
$checkpointGroups = array();

echo('<dl class="tabs pill">');
$first = true;
$tabs = array();

foreach ($groupedCheckpoint as $key => $value) {
    echo($first ? '<dd class="active">' : '<dd>');
    //$first = false;
    if ($key == 0) {
        $groupName = 'Без группы';
    } else {
        if ($checkpointGroups[$key] = CheckpointGroup::model()->findByPk($key)) {
            $groupName = $checkpointGroups[$key]->name;
        } else {
            $groupName = ",,?";
        }
    }

    $finished = 0;
    //----------------------
    $cout = '';
    $cout.=($first ? '<li class="active"' : '<li');
    $first = false;

    $cout.=(" id='cg" . $key . "Tab'>");

    $ch = $value;

    if ($ch) {
        $cout.='<table><tbody>';
        foreach ($ch as $c) {
            $readyBool = array_key_exists($c->idCheckpoint, $ready);
            if ($readyBool)
            {
                $finished++;
            }
            $boo = (' <span class="alert round label" title="Должен быть готов к этапу ' . $c->readyStateName . ' ">!</span> ');
            $late= !$readyBool&&($c->readyState < $model->enumStatus);
            if ($readonly) {
                $url = "";
            } else {
                $url = $readyBool ? (Yii::app()->createUrl('event/update', array('id' => $ready[$c->idCheckpoint]->idEvent, 'returnUrl' => Yii::app()->request->getRequestUri()))) :
                        (Yii::app()->createUrl('event/create', array('returnUrl' => Yii::app()->request->getRequestUri(), 'Event[title]' => $c->name, 'Event[idUser]' => Yii::app()->user->id,
                            'Event[idProject]' => $model->idProject,
                            'Event[eventClass]' => Event::CHECKPOINT_EVENT, 'Event[idCheckpoint]' => $c->idCheckpoint)));
            }

            $cout.='<tr ' . (($late) ? 'class="toolate"' : '') . '>';
            $cout.='<td>' . (($late) ? $boo : '') . ($readonly ? $c->name : CHtml::link($c->name, $url)) . ($readyBool ? ' (Выполнено ' . $ready[$c->idCheckpoint]->user->fullname . ', ' . Yii::app()->dateFormatter->formatDateTime($ready[$c->idCheckpoint]->date, 'medium', null) . ')' : '') . '</td>';
            $cout.='<td>' . ($readyBool ? CHtml::image(Yii::app()->getBaseUrl() . '/images/yes_icon.gif') : '') . '</td>';
            $cout.=' </tr>';
        }

        $cout.='</tbody> </table> </li>';
    }
    $tabs[$key] = $cout;

    //----------------------

    echo('<a href="#cg' . $key . '">' . $groupName . ' (' . $finished . '/' . count($checkpointGroups[$key]->checkpoints) . ')' . '</a></dd>');
}

echo('</dl>');
?>


<?php
$first = true;
echo('<ul class="tabs-content">');
foreach ($groupedCheckpoint as $key => $value) {
    echo($tabs[$key]);
    ?>
<?php } ?>
</ul>