<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>


<div class="row full-width">
    <div class="twelve columns">
        <h4><?= 'Разрешение на проект' ?></h4>

        <?php
        $form_name = 'sticky-ajax-form';
        $form = $this->beginWidget('ext.foundation.widgets.FounActiveForm', array(
            'id' => $form_name,
            'htmlOptions' => array(
                'onsubmit' => "sendFormAjax('#$form_name'); return false;",
            ),
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
        ));

        echo $form->errorSummary($model);
        ?>

        <script>
            function sendFormAjax(form) {
                window.dwDialog.uploadForm(form, "<?= Yii::app()->request->getRequestUri() ?>", true);
            }
        </script>
            <?php echo $form->hiddenField($model, 'idProject'); ?>
            <?php echo $form->hiddenField($model, 'enumRuleValue'); ?>
            <?php echo $form->dropDownList($model, 'idUser', CHtml::listData(User::model()->findAll(),"id", "fullname")); ?>
        <br/><br/>

        <?php

        echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Cохранить'
            , array('class' => 'button'));
        $this->endWidget();

        ?>

    </div>
</div> <!-- form -->





