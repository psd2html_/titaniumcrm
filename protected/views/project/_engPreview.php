<?php /** @var Project $model */ ?>
<h3><?= $model->name ?></h3>
<?php
if (($model->manufacturer)) {
    echo Helpers::reutrnTitleAndValueIfNotNull('Manufacturer', $model->manufacturer->name);
}
if (($model->coreInstrument)) {
    echo Helpers::reutrnTitleAndValueIfNotNull('Core Instrument', $model->coreInstrument->name);
} elseif ($model->virtualCoreInstrument) {
    echo Helpers::reutrnTitleAndValueIfNotNull('Feature', $model->virtualCoreInstrument);
}
echo Helpers::reutrnTitleAndValueIfNotNull('Manufacturer ID ', $model->innerName);

if (($model->companyConsumer)) {
    echo Helpers::reutrnTitleAndValueIfNotNull('Customer', $model->companyConsumer->fullNameEng?$model->companyConsumer->fullNameEng:TranslitHelper::translitGost($model->companyConsumer->fullName));
    echo Helpers::reutrnTitleAndValueIfNotNull('Addr.', $model->companyConsumer->physicalAddressEng?$model->companyConsumer->physicalAddressEng:TranslitHelper::translitGost($model->companyConsumer->physicalAddress));
}

echo Helpers::reutrnTitleAndValueIfNotNull('Email', $model->companyConsumer->email);
echo Helpers::reutrnTitleAndValueIfNotNull('Phone', $model->companyConsumer->phone);
echo Helpers::reutrnTitleAndValueIfNotNull('Est. Year/Quartal of Purchase', $model->getYearQuartal());
?>
<h6>Customers</h6>
<?php $customers = $model->getCustomers();

foreach ($customers as $c) {
$label = $c->customerComment;
?>

<strong> <?= TranslitHelper::translitGost($c->lastName . ' ' . $c->firstName . ' ' . $c->middleName . ' ')?></strong> <br/>
<?= $c->getContactInfoEng(); ?>
<?php
}
?>
