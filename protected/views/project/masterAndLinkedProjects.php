<?php Yii::app()->clientScript->registerScriptFile("/js/revealActivate.js", CClientScript::POS_END); ?>
<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Projects') => array('index'),
    $masterModel->name,
);
?>

<header><h3> Проекты данной группы </h3></header>

<?php $form = $this->beginWidget('CActiveForm', array(
    'enableAjaxValidation' => true,
)); ?>

<?php

$this->widget('MGridView', array(
    'id' => 'project-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'cssFile' => false,
    'template' => '{items} {pager}',
    'columns' => array(),
    'insertColumns'=> array(array(
        'id' => 'selectedProjectsId',
        'class' => 'CCheckBoxColumn',
        'selectableRows' => '50',
    ))
));
?>

<?php echo CHtml::submitButton('Разгруппировать отмеченные проекты', array('class' => 'button'));
echo '&nbsp' . CHtml::link('Добавить проекты в данную группу', Yii::app()->createUrl('project/linkProjects', array('id' => $masterModel->idProject)), array('class' => 'button'));
?>

<?php $this->endWidget(); ?>

