<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Projects') => array('index'),
    $model->projectName => $model->getViewUrl(array('noLinkedProjects' => 1)),
    Yii::t('app', 'Update'),
); ?>
<div class="right"><?= CHtml::link("Назад к сводке &raquo;", $model->getViewUrl(), array('class' => 'button ')) ?></div>
<header><h3> <?= $model->name; ?> </h3></header>


<div class="module_content">
    <?php
    $this->renderPartial('_form', array(
        'model' => $model,
        'logistic' => $logistic,
        'contest' => $contest));
    ?>
</div>
