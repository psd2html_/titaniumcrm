<?php if (Yii::app()->user->hasFlash('success')): ?>
    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>


<?php
$this->widget('ext.slidetoggle.ESlidetoggle', array(
    'itemSelector' => 'div.more',
    'titleSelector' => 'div.more h6',
    'collapsed' => 'div.more',
    'duration' => 'fast'
));
?>

<?php Yii::app()->clientScript->registerScriptFile("/js/revealActivate.js", CClientScript::POS_END); ?>
<?php
$form = $this->beginWidget('ext.foundation.widgets.FounActiveForm', array(
    'id' => 'project-form',
    'enableAjaxValidation' => !true,
    'enableClientValidation' => !true,
        ));

echo $form->errorSummary($model);
?>



    <script>
        function refreshList() {
            $.ajax({
                url: "<?= Yii::app()->createUrl('ajax/getAllowedCoreInstrumentClasses') ?>",
                data: {'idManufacturer': $('#Project_idManufacturer').val(), 'all':0},
                type: "POST",
                success: function(e) {
                    $('#Project_idCoreInstrumentClass').html(e);
                }
            });
        }
    </script>
<div class="row">

    <div class="four columns">
        <?php echo $form->labelEx($model, 'idResponsible'); ?>
        <?php echo $form->dropDownList($model, 'idResponsible', CHtml::listData(User::model()->findAll(), 'id', 'fullname')); ?>
        <?php echo $form->error($model, 'idResponsible'); ?>
    </div>


    <div class="four columns">
        <?php
        
        
        if ($model->idManufacturer) {
            $cclRaw = $model->idManufacturer ? $model->manufacturer->allowedCoreInstrumentClasses : $manufacturers[0]->allowedCoreInstrumentClasses;
            $ccl = CHtml::listData($cclRaw, "idCoreInstrumentClass", "name");
        }
        else
        {
            $ccl = array(""=>"Выберите класс оборудования");    
        }
        
        
        
        $m[""]="Выберите производителя";
        $m=$m+CHtml::listData($manufacturers, "idManufacturer", "name")
        ?>
        <?php echo $form->labelEx($model, 'idManufacturer'); ?>
        <?php echo $form->dropDownList($model, 'idManufacturer', $m, array('onchange' => 'refreshList();', 'onkeyup' => 'refreshList();')); ?>
        <?php echo $form->error($model, 'idManufacturer'); ?>
    </div>
    <div class="four columns indent-bot-15 ">
        <?php echo $form->labelEx($model, 'idCoreInstrumentClass'); ?>
        <?php echo $form->dropDownList($model, 'idCoreInstrumentClass', $ccl); ?>
        <?php echo $form->error($model, 'idCoreInstrumentClass'); ?>
    </div>
</div>
<table>
    <thead>
    <th colspan="2"> <?php echo $form->labelEx($model, 'idCompanyConsumer'); ?> </th>

</thead>

<tbody>
    <?php foreach (array('ompanyConsumer') as $name) { ?>

    <td width="120px">
        <a href="#" class="remove button secondary small" onclick="$('#Project_idC<?= $name ?>').val('');
                $('#autocomplete_idC<?= $name ?>').val('');
                return false;">
            <img alt="Удалить" src="<?= Yii::app()->getBaseUrl() ?>/images/icn_trash.png">    
        </a>

        <a href="<?= Yii::app()->createUrl("company/create", array('returnUrl' => Yii::app()->request->getRequestUri())) ?>" class="add button secondary small foundationModalTrigger" >
            <img alt="Создать" src="<?= Yii::app()->getBaseUrl() ?>/images/new.gif">    
        </a>
    </td>
    <td>

        <?php
        echo $form->hiddenField($model, 'idC' . $name);
        $v = $model->getRelated('c' . $name);
        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            'name' => ('autocomplete_idC' . $name),
            'value' => $v ? $v->fullName : '',
            'source' => Yii::app()->createUrl('ajax/companyAjax'),
            // additional javascript options for the autocomplete plugin
            'options' => array(
                'select' => "js:function(event, ui) {
                                          $('#Project_idC" . $name . "').val(ui.item.id);
                                        }",
                'change' => "js:function(event, ui) {
                                          if (!ui.item) {
                                             $('#Project_idC" . $name . "').val('');
                                          }
                                        }",
                'size' => 120,
                'showAnim' => 'fold',
            ),
                //'htmlOptions' => array('size' => 60)
        ));
        ?>
    </td>    

<?php } ?>
</table>
<?php
echo CHtml::submitButton(Yii::t('app', 'Save'), array('class' => 'button'));
$this->endWidget();
?>
