<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<?php
$filter[''] = 'Выберите группу этапов';
$filter = ($filter + Project::getEnumStatusList());
?>
<h4>Изменение этапа</h4>
<?php
$rand=  rand(1000, 9999);
$form = $this->beginWidget('ext.foundation.widgets.FounActiveForm', array(
    'id' => 'ajax-form-1-'.$rand,
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
));
?>
<div class="row full-width">
    <div class="six columns">

        <fieldset>
            <legend>
                Этапы
            </legend>

            <?php
            $s = Project::getEnumStatusList(true);
            if ($isLogist)
                echo $form->radioButtonList($model, "enumStatus", $s,array('disabled'=>'disabled'));
            else
                echo $form->radioButtonList($model, "enumStatus", $s);
            ?>


        </fieldset>

    </div>
    <div class="six columns">
        <fieldset>
            <legend>
                Приоритеты
            </legend>

            <?php
            $s = Project::getPriorityList();
            echo $form->radioButtonList($model, "priority", $s);
            ?>
          </fieldset>
    </div>
</div>


<?php
echo CHtml::ajaxSubmitButton('Сохранить', Yii::app()->createUrl(
    'project/chooseNextWorkflow/' . $model->getPrimaryKey()
), array(
    'type' => 'POST',
    'dataType' => 'json',
    'data' => 'js:$("#ajax-form-1-'.$rand.'").serialize()', //this one
    'success' => "js:function(data){
            if(data.result==='success'){
                location.reload();
            }else{
                $('#foundationModal').html(data.msg);
            }
        }",
), array('id'=>'first-submit-'.$rand,'class' => 'button secondary small'));


?>

<?php  $this->endWidget(); ?>
