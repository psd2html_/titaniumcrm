<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>
<div class="form">
    <p class="note">
        <?php echo Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required');?>.
    </p>

    <?php
    $form=$this->beginWidget('ext.foundation.widgets.FounActiveForm', array(
    'id'=>'centre-form',
    'enableAjaxValidation'=>false,
    'enableClientValidation'=>true,
    ));

    echo $form->errorSummary($model);
    ?>
    
        
        
        
        
            <?php echo $form->labelEx($model,'name'); ?>
            <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->error($model,'name'); ?>
            <fieldset>
                <legend>Фирмы, чье оборудование есть в центре</legend>
            <?php
            
            $this->widget('ext.relation.relation', array(
                    'model' => $model,
                    'widgetType' => 0,
                    'showCreate' => false,
                    'relation' => 'manufacturers',
                    'controllerName' => 'manufacturer',
                    'fields' => 'name',
                    'actionParams' => array(),
            ));
            
            ?>
            </fieldset>
                    <?php
        echo CHtml::submitButton(Yii::t('app', 'Save'),array('class'=>'button'));
        
        if (!$model->isNewRecord) {
                    echo "&nbsp;" . CHtml::link('Удалить', Yii::app()->createUrl("system/confirmDeletion", array(
                    "mid" => deletionHelper::D_CENTRE,
                    "id" => $model->getPrimaryKey()))
                    , array('class' => 'alert button del-btn'));
        }
        $this->endWidget(); 
                    ?>    
        
            </div> <!-- form -->

