<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Centres') 
    
); ?><header> <h3> Управление  измерительными центрами</h3> </header>


<?php $this->widget('MGridView', array(
	'id' => 'centre-grid',
	'dataProvider' => $model->search(),
        'cssFile' => false,
        'filter' => $model,
	'template' => '{items} {pager}',
	'columns' => array(        
        array(
                			'name' => 'searchManufacturer',
                                        'value' => '$data->getManufacturerNames()',
                                        'filter'=>CHtml::listData(Permission::getAllowedManufacturers(),'idManufacturer','name'),
                ),
        'name',
array('class' => 'CButtonColumn',
            'template' => '{update}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options'=>array('class'=>'button secondary tiny delete')
                ),
                'update' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options'=>array('class'=>'button secondary tiny update')
                ),
            ),
        ),
        
	),
)); ?>

 <div class="footer_content">
        <a class="button" href='<?= $this->createUrl("Centre/create")?> '>Создать новый</a>
 </div>