<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>
<div class="form">
    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span
            class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <?php
    $form = $this->beginWidget('ext.foundation.widgets.FounActiveForm', array(
        'id' => 'group-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
    ));

    echo $form->errorSummary($model);
    ?>
    <p>Внимание! Если пользователь является суперадмином, то все проверки, кроме фильтров на меню и проектов считаются выполненными!</p>

    <div class="row">
        <div class="six columns">
            <?php echo $form->labelEx($model, 'name'); ?>
            <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'name'); ?>
            <?php  echo $form->checkBoxRow($model, 'isLogist', array('size' => 45, 'maxlength' => 10)); ?>

        </div>
        <div class="six columns">
            <?php  echo $form->dropDownListRow($model, 'startingTab', Project::getAllTabs()); ?>

        </div>
    </div>


    <?php if (!$model->isNewRecord) { ?>
    <?php $config = Group::config();
    $lastMod = False;
    $translate = Group::modsList();
    $translateA = Group::actionsList();
    ?>

    <?php foreach ($config as $zoneId => $zone) {
    if ($lastMod !== $zone['mod']) {
    if ($lastMod === False) {
        ?>
        </tbody>
        </table>
    <?php
    }
    $lastMod = $zone['mod'];
    $settings=$model->getRules();
    ?>
    <table>
        <thead>
        <tr>
            <td>Область</td>
            <?php if (count($lastMod) == 0) { ?>
                <td></td>
            <?php } ?>
            <?php foreach ($lastMod as $mod) { ?>
                <td><?= $translate[$mod] ?></td>
            <?php } ?>
        </tr>
        </thead>
        <tbody>
        <?php } ?>
        <tr>
            <td>
                <?= $zone['name'] ?>
            </td>
            <?php if (count($lastMod) == 0) { ?>
                <td>www</td>
            <?php } ?>
            <?php foreach ($lastMod as $mod) { ?>
                <td>
                    <?php

                    if (!empty($settings)) {
                        if (array_key_exists($zoneId, $settings) && array_key_exists($mod, $settings[$zoneId])) {
                            $allowedActions = $settings[$zoneId][$mod];
                        } else {
                            $allowedActions = array();
                        }

                        if ($zone['class'] == Group::CLASS_ZONE) {
                            foreach ($zone['actions'] as $action) {
                                echo("<label>");
                                echo(CHtml::checkBox(Group::getCheckboxName($zoneId, $mod, $action),
                                    in_array($action, $allowedActions),
                                    array('value' => $action, 'class' => 'rbac_check')));
                                echo($translateA[$action]);
                                echo("</label>");
                            }
                            echo("<br/><label>");
                            echo(CHtml::checkBox("ne",
                                count($allowedActions)==count($zone['actions']),
                                array('class' => 'check_all')));
                            echo("Отметить/снять выделение");
                            echo("</label>");
                        } elseif ($zone['class'] == Group::CLASS_PROJECT_SUBZONE) {
                            foreach ($zone['actions'] as $action) {
                                echo("<label>");
                                echo(CHtml::radioButton(Group::getRadioName($zoneId, $mod),
                                    in_array($action, $allowedActions),
                                    array('value' => $action, 'class' => 'rbac_radio')));
                                echo($translateA[$action]);
                                echo("</label>");
                            }
                        }
                    }
                    ?>
                </td>
            <?php } ?>
        </tr>

        <?php } ?>

        </tbody>
    </table>
    <script>
        function recolour(e) {
            var v = $(e).val();
            var dict = {
            <?= Group::ACTION_EDIT ?>:
            'LightGreen',
            <?= Group::ACTION_READONLY ?>:
            'yellow',
            <?= Group::ACTION_NONE ?>:
            'red',
        }
        $(e).parent().parent().css('background-color', dict[v]);
        }
        $(document).ready(function () {
            $('.rbac_radio:checked').each(
                function () {
                    recolour(this)
                }
            );
            $('.rbac_radio').click(function () {
                recolour(this)
            })

            $('.check_all').click(function() {
                var val=$(this).attr('checked')=='checked';
                if (val) {
                    $(this).parent().parent().find('input').attr('checked','checked')
                } else {
                    $(this).parent().parent().find('input').removeAttr('checked')
                }
            })

        })
    </script>
    <?php } ?>
    <!--        -->
    <!--        -->
    <!--            --><?php //echo $form->labelEx($model,'system'); ?>
    <!--            --><?php //echo $form->textField($model,'system'); ?>
    <!--            --><?php //echo $form->error($model,'system'); ?>

    <?php
    echo CHtml::submitButton(Yii::t('app', 'Save'), array('class' => 'button'));


    $this->endWidget();
    ?>        </div> <!-- form -->

