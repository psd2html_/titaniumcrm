<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Groups') => array('index'),
    Yii::t('app', 'Create'),
);?>
<header> <h3> Создать </h3> </header>

<div class="module_content">
    
<?php
$this->renderPartial('_form', array(
			'model' => $model,
			'buttons' => 'create'));

?>
</div>