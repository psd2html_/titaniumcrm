<?php $personalFilters=Yii::app()->user->user()->getPersonalFilterListData(); ?>
<div class="more">
    <h6>Фильтр <?= count($personalFilters) > 0 ? '(' . count($personalFilters) . ')' : '' ?></h6>

    <?php if (is_array($personalFilters) && (count($personalFilters) > 0)) { ?>
        <ul>
            <?php foreach ($personalFilters as $value) { ?>
                <li>
                    <?= CHtml::image('/images/Filter.png') ?> <?= $value['name'] ?> - <?= $value['class'] ?>
                    <a onclick="deleteFilter(this); return false;" class="button secondary small" href="#"
                       mid="<?= $value['idManufacturer'] ?>" cid="<?= $value['idCoreInstrumentClass'] ?>"><img
                            alt="Удалить" src="/images/icn_trash.png"/></a>
                </li>
            <?php } ?>
        </ul>
        <script>
            /**
             * Comment
             */
            function deleteFilter(m) {

                $(m).parent().remove();
                $.ajax({
                    url: "<?= Yii::app()->createUrl("personal/removeFilter") ?>",
                    data: {
                        mid: $(m).attr("mid"),
                        cid: $(m).attr("cid"),
                        '<?= Yii::app()->request->csrfTokenName ?>': '<?= Yii::app()->request->csrfToken ?>'
                    },
                    success: function () {
                        location.reload();
                    },
                    type: "POST"
                });
            }
        </script>
    <?php } ?>
    <?=
    CHtml::link('Добавить фильтр проектов', array('personal/createFilter', 'returnUrl' => Yii::app()->request->getRequestUri()), array('class' => ' button small secondary foundationModalTrigger'))
    ?>
    <br/><br/>
</div>

