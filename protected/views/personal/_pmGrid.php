<?php $form=$this->beginWidget('CActiveForm', array(
    'enableAjaxValidation'=>true,
)); ?>
<?php

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => $gridViewId,
    'dataProvider' => $dataProvider,
    'filter' => $model,
    'cssFile' => false,
    'rowCssClassExpression'=>'$data->status==0?"unread":""',
    'template' => '{items} {pager}',
    'columns' => array(
        array(
            'id' => 'markedPmId',
            'class' => 'CCheckBoxColumn',
            'selectableRows' => '50',
        ),
        array(
            'name' => 'idUser',
            'filter'=>'',
            'value' => '$data->idUser?$data->user->fullName:"CRM"',
        ),
        array(
            'name' => 'created',
            'value' => 'Yii::app()->dateFormatter->formatDateTime($data->created)',
        ),
        array(
            'name'=>'subject',
            'value' => '$data->subjectColumn',
            'filter'=>Pm::getSubjectList(),

        ),
        array(
            'name' => 'body',
            'type'=>'html'
        ),
//        array(
//            'name' => 'idProject',
//            'filter'=>'',
//            'value'=>'$data->idProject?$data->project->getNameUrl():"-"',
//            'type'=>'html'
//        ),
    )
));
?>
<button type="submit" name="submitButton" class="button secondary small" value="archSel">Архивировать отмеченные</button>
<button type="submit" name="submitButton" class="button secondary small" value="delSel">Удалить отмеченные</button>
<button type="submit" name="submitButton" class="button alert small" value="delAll">Удалить все</button>

<?php $this->endWidget(); ?>
