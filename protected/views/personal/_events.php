<?php 
$eventsType=  Event::getEventClassList();

$this->widget('MGridView', array(
    'rowCssClassExpression'=>'$data->getCustomClass()',
    'id' => $prefix.'-events-grid',
    'dataProvider' => $eventDataProvider,
    'filter' => $event,
    'cssFile' => false,
    'template' => '{items} {pager}',
    'columns' => array(
        array(
            'name' => 'eventClass',
            'value' => '$data->getType()',
            'filter' => $eventsType,
            'type'=>'html'
        ),
        array(
            'name' => 'idProject',
            'value' => '$data->project->nameUrl',
            'filter' => '',
            'type'=>'raw'
        ),
        array(
            'name' => 'date',
            'value' => 'Yii::app()->dateFormatter->formatDateTime($data->date, "full", null)',
            'filter' => '',
        ),
        array(
            'name' => 'comment',
            'value' => '$data->comment',
            'filter' => '',
            'type' => 'html'
        ),
        
        array('class' => 'CButtonColumn',
            'template' => '{update}',
            'deleteButtonUrl' => 'Yii::app()->createUrl("event/delete", array("id" => $data->getPrimaryKey()))',
            'updateButtonUrl' => 'Yii::app()->createUrl("event/update", array("id" => $data->getPrimaryKey(),"returnUrl"=>"' . Yii::app()->request->getRequestUri() . '"))',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options' => array('class' => 'button secondary tiny delete')
                ),
                'update' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options' => array('class' => 'button secondary tiny update')
                ),
            ),
        ),
    ),
));
