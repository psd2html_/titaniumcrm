<?php
Yii::app()->clientScript->registerCoreScript('jquery.ui');
$form = $this->beginWidget('ext.foundation.widgets.FounActiveForm', array(
    'id' => 'ajax-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'htmlOptions' => array(
        'onsubmit' => "sendFormAjax('#ajax-form'); return false;",
    ),
));

echo $form->errorSummary($model);
?>
<script>
    window.selectPreset = function selectPreset(val) {
        $("#CustomizableColumnsForm_changePresetTo").val(val);
        $("Input[name=selectPreset]").attr('disabled', 'disabled');
        sendFormAjax('#ajax-form');
    }

    function sendFormAjax(form) {
        var countChecked = $("input[name='CustomizableColumnsForm[columns][]']:checked").length;
        if (countChecked == 0) {
            alert("Хотя бы одина колонка должена быть отмечена птичкой. Сохранение невозможно. Выберите нужные колонки.");
            return false;
        }
        window.dwDialog.uploadForm(form, "<?= Yii::app()->createUrl("personal/customizeColumns", array('gid' => $gid)) ?>", true);
    }
</script>

<div class="row full-width">
    <div class="six columns">
        <?php echo $form->textFieldRow($model, 'presetName'); ?>
        <?php echo $form->hiddenField($model, 'currentPreset'); ?>
        <?php echo $form->hiddenField($model, 'changePresetTo'); ?>
        <fieldset>
            <legend>Фильтр колонок</legend>

            <div id="sortableFieldset">
                <?php echo $form->checkBoxListRow($model, 'columns', $columns, array('checkAll' => 'Отметить все')); ?>
            </div>
        </fieldset>
    </div>
    <div class="six columns">
        <h5>Выбрать пресет для редактирования</h5>

        <div id="presetButtons">
            <?php
            if (is_array($presets)) {
                foreach ($presets as $id => $preset) {
                    ?>
                    <div class="dot"><span class="sortable-tag"><img
                                    src="<?= Yii::app()->getBaseUrl() ?>/images/icon_sort.gif"/></span> <?php
                        echo CHtml::hiddenField("presetOrder[]", $id);
                        echo CHtml::button(CHtml::encode($preset['n'])
                                , array('name'  => 'selectPreset', 'onClick' => 'window.selectPreset("' . $id . '");',
                                        'class' => ($id == $model->currentPreset) ? 'button indent-bot-10' : 'button secondary indent-bot-10')) . '<br/>';
                        ?> </div>
                    <?php
                }
            }
            ?>
        </div>
        <br/>

        <p>Отмеченные пункты можно сортировать перетаскивая их в нужном порядке</p>

        <?php
        echo CHtml::submitButton('Cохранить и закрыть'
            , array('class' => 'button')); ?>
    </div>
</div>




<?php


/*echo CHtml::ajaxSubmitButton('Cохранить', Yii::app()->createUrl(
    "personal/customizeColumns", array('gid' => $gid)
), array(
    'type' => 'POST',
    'dataType' => 'json',
    'data' => 'js:$("#ajax-form").serialize()', //this one
    'success' => "js:function(data){
            if(data.result==='success'){
                location.reload();
            }else{
                $('#foundationModal').html(data.msg);
            }
        }",
), array('class' => 'button'));*/

$this->endWidget();
?>

<script>
    //    $('#sortableFieldset').sortable({handle: '.sortable-tag'});
    $(document).ready(function () {
        $('#sortableFieldset label').append('<span class="sortable-tag"><img src="<?= Yii::app()->getBaseUrl() ?>/images/icon_sort.gif" /></span>');
        $('#sortableFieldset').sortable();
        $('#presetButtons').sortable({handle: '.sortable-tag'});

    })

</script>