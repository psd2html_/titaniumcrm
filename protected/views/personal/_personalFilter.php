<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>

<div class="row full-width">
    <div class="twelve columns">
        <h4><?= 'Создать новый фильтр' ?></h4>

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'ajax-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
        ));

        echo $form->errorSummary($model);
        ?>

        <?php echo $form->hiddenField($model, 'idUser'); ?>
        <script>
            /**
             * refresh
             */


            function refreshList() {
                $.ajax({
                    url: "<?= Yii::app()->createUrl('ajax/getAllowedCoreInstrumentClasses') ?>",
                    data: {'idManufacturer': $('#Permission_idManufacturer').val()},
                    type: "POST",
                    success: function(e) {
                        $('#Permission_idCoreInstrumentClass').html(e);
                    }
                });
            }
        </script>

        <div class="row full-width">
            <div class="six columns">
                <?php
                
                $manufacturers = Permission::getAllowedManufacturers();
                $cclRaw =  Permission::getAllowedCoreInstrumentClasses($manufacturers[0]->idManufacturer);
                $ccl = CHtml::listData($cclRaw, "idCoreInstrumentClass", "name");
                
                ?>
                <?php echo $form->labelEx($model, 'idManufacturer'); ?>
                <?php echo $form->dropDownList($model, 'idManufacturer', CHtml::listData($manufacturers, "idManufacturer", "name"), array('onchange' => 'refreshList();', 'onkeyup' => 'refreshList();')); ?>
                <?php echo $form->error($model, 'idManufacturer'); ?>
            </div>
            <div class="six columns indent-bot-15 ">
                <?php echo $form->labelEx($model, 'idCoreInstrumentClass'); ?>
                <?php echo $form->dropDownList($model, 'idCoreInstrumentClass', $ccl); ?>
                <?php echo $form->error($model, 'idCoreInstrumentClass'); ?>
            </div>
        </div>

        <?php
        echo CHtml::ajaxSubmitButton('Создать', Yii::app()->createUrl(
                        'personal/createFilter' 
                ), array(
            'type' => 'POST',
            'dataType' => 'json',
            'data' => 'js:$("#ajax-form").serialize()', //this one
            'beforeSend'=>"js:function() {  $('#foundationModal input[type=submit]').hide() }",
            'success' => "js:function(data){
            if(data.result==='success'){
                location.reload();
            }else{
                $('#foundationModal').html(data.msg);
            }
        }",
                ), array('class' => 'button'));

        $this->endWidget();
        ?>

    </div>
</div> <!-- form -->

