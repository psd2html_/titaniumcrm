<?php
$eventsType=  Event::getEventClassList();

$this->widget('MGridView', array(
    'rowCssClassExpression'=>'$data->getCustomClass()',
    'id' => $gridViewId,
    'dataProvider' => $dataProvider,
    'readOnly'=>$readOnly,
    'filter' => $model,
    'cssFile' => false,
    'template' => '{items} {pager}',
    'columns' => array(
        array(
            'name' => 'text',
            'value' => '$data->getTemplateIcon()." ".$data->text',
            'type'=>'html'
        ),

        array(
            'name' => 'idProject',
            'value' => '$data->project?$data->project->nameUrl:"N/A"',
            'filter' => '',
            'type'=>'raw'
        ),
        array(
            'name' => 'idUser',
            'value' => '$data->user?$data->user->fullname:"N/A"',
            'filter' => '',
        ),
        array(
            'name' => 'idResponsible',
            'value' => '$data->responsible?$data->responsible->fullname:"N/A"',
            'filter' => '',
        ),
        array(
            'name' => 'calendarDate',
            'value' => 'Yii::app()->dateFormatter->formatDateTime($data->calendarDate, "full", null)',
            'filter' => '',
        ),

        array(
            'name' => 'date',
            'value' => 'Yii::app()->dateFormatter->formatDateTime($data->date, "full", null)',
            'filter' => '',
        ),



        array('class' => 'MButtonColumn',
            'template' => '{update}',
            'deleteButtonUrl' => 'Yii::app()->createUrl("sticky/delete", array("id" => $data->getPrimaryKey()))',
            'updateButtonUrl' => 'Yii::app()->createUrl("sticky/update", array("id" => $data->getPrimaryKey(),"returnUrl"=>"' . Yii::app()->request->getRequestUri() . '"))',

            'buttons' => array(
//                'check' => array
//                (
//                    'label'=>'',     //Text label of the button.
//                    'url'=>'Yii::app()->createUrl("sticky/checkAjax", array("id" => $data->getPrimaryKey(),"returnUrl"=>"' . Yii::app()->request->getRequestUri() . '"))',       //A PHP expression for generating the URL of the button.
//                    'imageUrl'=>'...',  //Image URL of the button.
//                    'options'=>array(), //HTML options for the button tag.
//                    'click'=>'...',     //A JS function to be invoked when the button is clicked.
//                    'visible'=>'...',   //A PHP expression for determining whether the button is visible.
//                ),
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options' => array('class' => 'button secondary tiny delete')
                ),
                'update' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options' => array('class' => 'button secondary tiny update dwDialogTrigger')
                ),
            ),
        ),
    ),
));
