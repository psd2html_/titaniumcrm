<?php
$this->breadcrumbs = array(
    "Личный кабинет",
);?>

<div class="row">
    <div class="twelve columns">
        <?php if (Yii::app()->user->hasFlash('success')): ?>
            <div class="alert-box success">
                <?php echo Yii::app()->user->getFlash('success'); ?>
                <a href="" class="close">&times;</a>
            </div>
        <?php endif; ?>


        <dl class="tabs">
            <?php if ($newMessagesCount>0) {  ?>
                <dd class="active"><a href="#unreadMessages">Новые уведомления (<?= $newMessagesCount?>)</a></dd>
            <?php } else  { ?>
                <dd class="active"><a href="#unreadMessages">Уведомления</a></dd>
            <?php }  ?>

            <dd><a href="#archived">Архивные сообщения</a></dd>
            <dd><a href="#personal">Персональные настройки</a></dd>
            <dd><a href="#main">Настройки учетной записи</a></dd>
            <?php if (!$model->isNewRecord) { ?>
                <dd><a href="#sign">Подпись</a></dd>
            <?php } ?>
        </dl>

        <ul class="tabs-content">
            <li id="personalTab">
                <?php
                /* @var CUserPreferencesForm $userPreferencesForm*/
                $userPreferencesForm->renderForm($this);
                ?>
            </li>
            <?php if ($newMessagesCount>0) { ?>
                <li class="active" id="unreadMessagesTab">
                    <h3>Новые уведомления</h3>
                    <?php
                        $this->mGridViewDataProvider->renderMGridView('unread-pm-grid',false);
                        PersonalMessageService::markAllAsRead();
                    ?>
                </li>
            <?php } else { ?>
                <li class="active" id="unreadMessagesTab">
                    <h3>Уведомления</h3>
                    <?php
                    $this->mGridViewDataProvider->renderMGridView('unread-pm-grid',false);
                    ?>
                </li>
            <?php } ?>
            <li id="archivedTab">
                <h3>Архив уведомлений</h3>
                <?php
                $this->mGridViewDataProvider->renderMGridView('archived-pm-grid',false);
                ?>
            </li>
            <?php if (!$model->isNewRecord) { ?>
                <li id="signTab">
                    <fieldset>
                        <?php
                        $this->widget("ext.hasImage.processingTool", array('model' => $model));
                        ?>
                        <br/>
                    </fieldset>
                </li>
            <?php } ?>
            <li id="mainTab">

                <?php $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'user-form',
                    'enableAjaxValidation' => true,
                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                ));
                ?>


                <?php echo $form->errorSummary(array($model, $profile)); ?>

                <fieldset>
                    <div class="row">
                        <div class="six columns">
                            <?php echo $form->labelEx($model, 'username'); ?>
                            <?php echo $form->textField($model, 'username', array('size' => 20, 'maxlength' => 20)); ?>
                            <?php echo $form->error($model, 'username'); ?>

                            <?php echo $form->labelEx($model, 'email'); ?>
                            <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 128)); ?>
                            <?php echo $form->error($model, 'email'); ?>

                            <label>Ваш пароль, для изменения настроек</label>
                            <?php echo $form->passwordField($model, 'password', array('size' => 60, 'maxlength' => 128)); ?>
                            <?php echo $form->error($model, 'password'); ?>
                        </div>
                        <div class="six columns">
                            <?php
                            $profileFields = $profile->getFields();
                            if ($profileFields) {
                                foreach ($profileFields as $field) {
                                    ?>

                                    <?php echo $form->labelEx($profile, $field->varname); ?>
                                    <?php
                                    if ($widgetEdit = $field->widgetEdit($profile)) {
                                        echo $widgetEdit;
                                    } elseif ($field->range) {
                                        echo $form->dropDownList($profile, $field->varname, Profile::range($field->range));
                                    } elseif ($field->field_type == "TEXT") {
                                        echo CHtml::activeTextArea($profile, $field->varname, array('rows' => 6, 'cols' => 50));
                                    } else {
                                        echo $form->textField($profile, $field->varname, array('size' => 60, 'maxlength' => (($field->field_size) ? $field->field_size : 255)));
                                    }
                                    ?>
                                    <?php echo $form->error($profile, $field->varname); ?>
                                <?php
                                }
                            }
                            ?>
                            <?php echo $form->labelEx($model, 'personalFooter'); ?>
                            <?php echo $form->textField($model, 'personalFooter'); ?>
                            <?php echo $form->error($model, 'personalFooter'); ?>
                            <?php echo $form->labelEx($model, 'personalFooter2'); ?>
                            <?php echo $form->textField($model, 'personalFooter2'); ?>
                            <?php echo $form->error($model, 'personalFooter2'); ?>
                            <?php echo $form->labelEx($model, 'personalFooter3'); ?>
                            <?php echo $form->textField($model, 'personalFooter3'); ?>
                            <?php echo $form->error($model, 'personalFooter3'); ?>
                        </div>
                    </div>
                    <?php echo CHtml::submitButton(("Сохранить"), array('class' => 'button')); ?>
                    <br/>
                    <br/>
                    <?php $this->endWidget(); ?>
                </fieldset>

                <h3>Настройки доступа</h3>

                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'profile-form',
                    'enableAjaxValidation' => true,
                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                ));
                ?>

                <?php echo $form->errorSummary($changePass); ?>
                <fieldset>

                    <?php echo $form->labelEx($changePass, 'oldPassword'); ?>
                    <?php echo $form->passwordField($changePass, 'oldPassword'); ?>
                    <?php echo $form->error($changePass, 'oldPassword'); ?>

                    <?php echo $form->labelEx($changePass, 'password'); ?>
                    <?php echo $form->passwordField($changePass, 'password'); ?>
                    <?php echo $form->error($changePass, 'password'); ?>


                    <?php echo $form->labelEx($changePass, 'verifyPassword'); ?>
                    <?php echo $form->passwordField($changePass, 'verifyPassword'); ?>
                    <?php echo $form->error($changePass, 'verifyPassword'); ?>

                    <?php echo CHtml::submitButton(("Сохранить"), array('class' => 'button')); ?>

                    <?php $this->endWidget(); ?>
                    <br/>
                    <br/>
                </fieldset>
            </li>
        </ul>
    </div>
</div>