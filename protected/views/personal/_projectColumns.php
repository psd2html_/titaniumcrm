<?php
$form = $this->beginWidget('ext.foundation.widgets.FounActiveForm', array(
    'id' => 'ajax-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
));

echo $form->errorSummary($model);
?>

    <fieldset>
        <legend>Фильтр колонок</legend>
        <?php echo $form->checkBoxListRow($model, 'columns', Project::getAllColumns(), array('checkAll' => 'Отметить все')); ?>
    </fieldset>


<?php

echo CHtml::ajaxSubmitButton('Cохранить', Yii::app()->createUrl(
    'personal/projectColumnsFilter'
), array(
    'type' => 'POST',
    'dataType' => 'json',
    'data' => 'js:$("#ajax-form").serialize()', //this one
    'success' => "js:function(data){
            if(data.result==='success'){
                location.reload();
                //$('#foundationModal').trigger('reveal:close');
            }else{
                $('#foundationModal').html(data.msg);
            }
        }",
), array('class' => 'button'));

$this->endWidget();
?>