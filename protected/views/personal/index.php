<?php
$this->breadcrumbs = array(
    "Личная сводка"
);
?>

<?php



$this->widget('application.extensions.profullcalendar.ProFullcalendarGraphWidget', array(
        //'data' => $dates,
        'ajaxUrl' => '/personal/FetchCalendarEvents',
        // 'projectFilters' => $coreInstrumentClassFilters,
        'allowQtip' => true,
        'options' => array(
             'header'=> array(
              'left'=>'prev,next today',
              'center'=> 'title',
              'right'=>'twoweek,basicWeek,month,offweek'
              ),

            'editable' => !true,
            //'eventRender' => $eventRender,
            //'defaultView' => 'twoweek',
            'defaultView'=> "js:$.cookie('fullcalendar_defaultView') || 'twoweek'",
            'viewDisplay'=> "js:function(view) { $.cookie('fullcalendar_defaultView', view.name); }",
            'contentHeight' => '350',
            //'eventAfterRender'=>'js:setFilter',
            //'eventAfterAllRender' => 'js:applyFilter'
        ),
        'htmlOptions' => array(
            'style' => 'margin: 0 auto;',
        ),
    )
);
?>



    <div class="footer_content indent-bot-20">
        <a class="button" href='<?= $this->createUrl("Project/create") ?> '>Создать новый проект</a> <br/><br/>
    </div>



    <h5>Задания, назначенные мне</h5>

    <dl class="tabs pill">
        <dd class="active"><a href="#tomeactive">Назначенные мне - активные</a></dd>
        <dd><a href="#bymeactive">Назначенные мной - активные</a></dd>
        <dd><a href="#past">Выполненные</a></dd>
    </dl>

    <ul class="tabs-content">
        <li class="active" id="tomeactiveTab">
            <?php
            $this->mGridViewDataProvider->renderMGridView('tomeactive-sticky-grid',false);
            ?>
        </li>
        <li id="bymeactiveTab">
            <?php
            $this->mGridViewDataProvider->renderMGridView('bymeactive-sticky-grid',false);
            ?>
        </li>
        <li id="pastTab">
            <?php
            $this->mGridViewDataProvider->renderMGridView('done-sticky-grid',false);
            ?>
        </li>
    </ul>





    <script>
        $(document).ready(function () {
            $("a.foundationModalTrigger").click(function (e) {
                $.ajax({
                    url: $(this).attr("href"),
                    //processData: false,
                    success: function (result) {
                        $("foundationModal").html(result);
                        $("foundationModal").reveal();
                    }
                });
                e.preventDefault();
            });
        });

    </script>

<?php
$this->widget('ext.slidetoggle.ESlidetoggle', array(
    'itemSelector' => 'div.more',
    'titleSelector' => 'div.more h6',
    'collapsed' => 'div.more',
    'duration' => 'fast'
));
?>