<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Projects') => array('project/index'),
    $model->project->name=> $model->project->getViewUrl(array( 'noLinkedProjects'=>1)),
    Yii::t('app', 'Редактировать образец'),

        ); ?>

<header> <h3> <?= $model->name; ?> </h3> </header>
<div class="module_content">
<?php
$this->renderPartial('_form', array(
			'model'=>$model));
?>
</div>