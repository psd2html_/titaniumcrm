<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Projects') => array('project/index'),
    $model->project->name=> $model->project->getViewUrl(array( 'noLinkedProjects'=>1)),
    Yii::t('app', 'Создать образец'),
);?>

<header> <h3> Создать </h3> </header>

<div class="module_content">
    
<?php
$this->renderPartial('_form', array(
			'model' => $model,
			'buttons' => 'create'));

?>
</div>