<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>
<div class="form">
    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span
            class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>


    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'sample-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
    ));

    echo $form->errorSummary($model);
    ?>

    <fieldset>

        <script>
            /**
             * refresh
             */


            function refreshList() {
                $.ajax({
                    url: "<?= Yii::app()->createUrl('ajax/getCoreInstruments') ?>",
                    data: {'idManufacturer': $('#Sample_idManufacturer').val(),
                        'idCoreInstrumentClass': <?= $model->project->idCoreInstrumentClass ?>,
                        'noCI': 1
                    },
                    type: "POST",
                    success: function (e) {
                        $('#Sample_idCoreInstrument').html(e);
                    }
                });
                $.ajax({
                    url: "<?= Yii::app()->createUrl('ajax/getCentres') ?>",
                    data: {'idManufacturer': $('#Sample_idManufacturer').val(),
                        'idCoreInstrumentClass': <?= $model->project->idCoreInstrumentClass ?>
                    },
                    type: "POST",
                    success: function (e) {
                        $('#Sample_idCentre').html(e);
                    }
                });
            }
        </script>




        <?php echo $form->hiddenField($model, 'idProject'); ?>

        <?php if (!$model->isNewRecord) { ?>
            <?php echo $form->labelEx($model, 'name'); ?>
            <?php echo $form->textField($model, 'name', array('disabled' => 'disabled', 'size' => 60, 'maxlength' => 64)); ?>
        <?php } ?>

        <?php echo $form->labelEx($model, 'innerName'); ?>
        <?php echo $form->textField($model, 'innerName', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'innerName'); ?>

        <?php echo $form->labelEx($model, 'title'); ?>
        <?php echo $form->textField($model, 'title', array('size' => 60, 'maxlength' => 64)); ?>
        <?php echo $form->error($model, 'title'); ?>

        <div class="row">
            <div class="four columns indent-bot-20">
                <?php
                $manufacturers = Manufacturer::model()->with('coreInstrumentClasses')->findAll('coreInstrumentClasses.idCoreInstrumentClass=' . $model->project->idCoreInstrumentClass);
                //$manufacturers = Manufacturer::model()->findAll();
                $manufacturersKeyValue = CHtml::listData($manufacturers, "idManufacturer", "name");
                if ($model->idManufacturer) {
                    if (!array_key_exists($model->idManufacturer, $manufacturersKeyValue)) {
                        $manufacturersKeyValue[$model->idManufacturer] = $model->manufacturer->name;
                    }

                    $ciS = CoreInstrument::model()->findAllByAttributes(array('idManufacturer' => $model->idManufacturer, 'idCoreInstrumentClass' => $model->project->idCoreInstrumentClass));
                    $ciS = CHtml::listData($ciS, 'idCoreInstrument', 'name');

                    $ciC = new Centre('search');
                    $ciC->searchManufacturer = $model->idManufacturer;
                    $aa = $ciC->search();
                    $aa->pagination = false;
                    $ciC = CHtml::listData($aa->getData(), 'idCentre', 'name');

                } else {
                    if ($manufacturers) {
                        $ciS = CoreInstrument::model()->findAllByAttributes(array('idManufacturer' => $manufacturers[0]->idManufacturer, 'idCoreInstrumentClass' => $model->project->idCoreInstrumentClass));
                        $ciS = CHtml::listData($ciS, 'idCoreInstrument', 'name');
                        $ciC = new Centre('search');
                        $ciC->searchManufacturer = $manufacturers[0]->idManufacturer;
                        $aa = $ciC->search();
                        $aa->pagination = false;
                        $ciC = CHtml::listData($aa->getData(), 'idCentre', 'name');

                    } else {
                        $ciS = array('' => ' < Выберите производителя >');
                        $ciC = array('' => ' < Выберите центр >');
                    }
                }

                ?>
                <?php echo $form->labelEx($model, 'idManufacturer'); ?>
                <?php echo $form->dropDownList($model, 'idManufacturer', $manufacturersKeyValue, array('onchange' => 'refreshList();', 'onkeyup' => 'refreshList();')); ?>
                <?php echo $form->error($model, 'idManufacturer'); ?>
            </div>
            <div class="four columns ">
                <?php echo $form->labelEx($model, 'idCoreInstrument'); ?>
                <?php echo $form->dropDownList($model, 'idCoreInstrument', $ciS); ?>
                <?php echo $form->error($model, 'idCoreInstrument'); ?>
            </div>
            <div class="four columns ">
                <?php echo $form->labelEx($model, 'idCentre'); ?>
                <?php echo $form->dropDownList($model, 'idCentre', $ciC); ?>
                <?php echo $form->error($model, 'idCentre'); ?>
            </div>

        </div>


        <div class="row">
            <div class="four columns">
                <?php echo $form->labelEx($model, 'recievedDate'); ?>
                <?php
                $this->widget('ext.proDate.proDate', array(
                    'model' => $model,
                    'attribute' => 'recievedDate',
                    'htmlOptions' => array('class' => 'foundation-date-picker')
                ));
                ?>
                <?php echo $form->error($model, 'recievedDate'); ?>
                <?php if (!$model->isNewRecord) { ?>
                    <?php echo $form->labelEx($model, 'lastChanged'); ?>
                    <?php

                    echo CHtml::textField('lastChanged__', is_object($model->lastChanged) ? '' : Yii::app()->dateFormatter->formatDateTime($model->lastChanged, 'full', null), array('disabled' => '1'));
                    ?>
                    <?php echo $form->error($model, 'lastChanged'); ?>
                <?php } ?>
            </div>

            <div class="eight columns">
                <div class="indent-bot-20">
                    <?php echo $form->labelEx($model, 'enumStatus'); ?>
                    <?php echo $form->dropDownList($model, 'enumStatus', Sample::getEnumStatusList()); ?>
                    <?php echo $form->error($model, 'enumStatus'); ?>
                </div>
                <?php echo $form->labelEx($model, 'location'); ?>
                <?php echo $form->textField($model, 'location', array('size' => 60, 'maxlength' => 128)); ?>
                <?php echo $form->error($model, 'location'); ?>

            </div>
        </div>

        <?php echo $form->labelEx($model, 'task'); ?>
        <?php echo $form->textArea($model, 'task', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'task'); ?>

        <?php echo $form->labelEx($model, 'comment'); ?>
        <?php echo $form->textArea($model, 'comment', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'comment'); ?>


    </fieldset>
    <p>Файлы можно прикрепить из <?= CHtml::link("файловой панели",$model->project->getViewUrl()."#attachments",array("target"=>'_blank')) ?></p>
    <?php
    echo CHtml::submitButton(Yii::t('app', 'Save'), array('class' => 'button'));
    if (!$model->isNewRecord) {
        echo "&nbsp;" . CHtml::link('Удалить', Yii::app()->createUrl("system/confirmDeletion", array(
                    "mid" => deletionHelper::D_SAMPLE,
                    "id" => $model->getPrimaryKey()))
                , array('class' => 'alert button del-btn'));
    }
    $this->endWidget();
    ?>

</div> <!-- form -->

