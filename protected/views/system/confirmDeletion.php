<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<h3>Удаление</h3>



<?php if (!$model->beforeDelete()) { ?>
    Вы хотели удалить: <strong><?= CHtml::link($model->name, $model->editUrl); ?></strong>, однако <br/> <br/>
    <?php
    $err = ($model->getErrors());
    if ($err) {
        echo CHtml::errorSummary($model);
    }
} else {
    ?>
    Следующая запись будет удалена: <strong><?= CHtml::link($model->name, $model->editUrl); ?></strong> <br/> <br/>
<?php } ?>

<?php
/* @var $model Customer */
$out = array();

foreach ($model->relations() as $key => $value) {

    // $key = Name of the Relation
    // $value[0] = Type of the Relation
    // $value[1] = Related Model
    // $value[2] = Related Field or Many_Many Table

    $o = array();

    //@todo remove kostyl!!!
    if ($key == 'closestTaskSorter') continue;

    $refs = $model->getRelated($key);

    if ($refs) {
        if (!is_array($refs)) {
            $refs = array($refs);
        }
        if ($model->getAttributeLabel($key) != "") {


            foreach ($refs as $ref) {
                if (is_object($ref)&&$ref->hasAttribute('name')&&$ref->hasAttribute('editUrl'))
                {
                    $o[] = CHtml::link($ref->name, $ref->editUrl);
                }
                //ActiveRecord
            }
            if (count($o)>0) {
                $out[$model->getAttributeLabel($key)] = $o;
            }
        }
    }
}

if ($out) {
    ?>

    <h5>Связанные сущности</h5>
    <p>После удаления данного объекта следующие записи могут измениться.</p>
    <?php foreach ($out as $name => $val) {
        ?>
        <h6><?= $name ?></h6>
        <ul>
            <?php
            foreach ($val as $v) {
                
                ?>
                <li><?= $v ?></li>
            <?php } ?>
        </ul>

        <?php
    }
    ?>
    <?php
}
?>
<?php if ($model->beforeDelete()) { ?>
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'deleteConfirmation',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
    ));
    ?>
    <?php
    echo CHtml::hiddenField("mid", $mid);
    echo CHtml::hiddenField("id", $id);
    echo CHtml::hiddenField("v", $validation);
    echo CHtml::submitButton("Подтверждаю удаление", array('class' => 'button alert'));
    $this->endWidget();
    ?>
    <?php
}
?>