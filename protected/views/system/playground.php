<?php
$this->angularJsDispatcher->injectDependency("angularBootstrapNavTree");
$this->angularJsDispatcher->injectDependency("dropzone");
Yii::app()->clientScript->registerScriptFile(Yii::app()->getBaseUrl() . "/js/controllers/abnController.js", CClientScript::POS_BEGIN);
$idProject = 1;
$currentUser = Yii::app()->user->id;
?>

<div ng-init="csrfToken='<?= Yii::app()->request->csrfToken ?>'; csrfName='<?= Yii::app()->request->csrfTokenName ?>'; idProject=<?= $idProject ?>; updateTree();"
    ng-controller="AbnTestController">

    <hr>
    <div class="row">
        <div class="three columns">
            <div class="panel">
                <abn-tree tree-data="fileTree" on-select="my_tree_handler(branch)" expand-level="2"></abn-tree>
            </div>
        </div>
        <div class="nine columns">
            <span ng-show="updatingFiles"><?= Helpers::getSpinnerGif() ?></span>

            <div ng-show="currentDirId>-1&&!updatingFiles">
                <div dropzone project-id="<?= $idProject ?>" upload-action="/upload/store"
                     success-callback="updateFiles()"
                     location="currentDirId"
                     csrf-name="<?= Yii::app()->request->csrfTokenName ?>"
                     csrf-token="<?= Yii::app()->request->csrfToken ?>">

                    <h4>{{selectedFolder}}</h4>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Файл</th>
                            <th>Автор</th>
                            <th>Размер</th>
                            <!--                            <th>Комментарий</th>-->
                            <th>Дата</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-show="files.length==0">
                            <td colspan="6">
                                <i>Файлов нет</i>
                            </td>
                        </tr>
                        <tr ng-repeat="f in files">
                            <td>{{f.n}}</td>
                            <td>{{f.a}}</td>
                            <td>{{f.s | bytes}}</td>
                            <!--                            <td>{{f.c}}</td>-->
                            <td>{{f.date | date}}</td>
                            <td>
                                <a class="small secondary button"
                                   ng-href="/upload/download?p=<?= $idProject ?>&f={{currentDirId}}&h={{f.uid}}&i={{f.id}}"><i
                                        class="fa fa-download"></i></a>
                                <a class="alert small button" ng-show="f.u==<?= $currentUser ?>"
                                   ng-click="deleteFile(f)"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>