<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>
<div class="form">
    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'logistic-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
            ));

    echo $form->errorSummary($model);
    ?>

    <fieldset>
        <?php echo $form->labelEx($model, 'idProject'); ?>
        <?php echo $form->dropDownList($model, 'idProject0', CHtml::listData(Project::model()->findAll(), 'idProject', 'name')); ?>
        <?php echo $form->error($model, 'idProject'); ?>
    </fieldset>

    <fieldset>
        <?php echo $form->labelEx($model, 'enumStatus'); ?>
        <?php echo $form->textField($model, 'enumStatus', array('size' => 45, 'maxlength' => 45)); ?>
        <?php echo $form->error($model, 'enumStatus'); ?>
    </fieldset>

    <fieldset>
        <?php echo $form->labelEx($model, 'lastChange'); ?>
        <?php
        
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model,
            'name' => 'Logistic[lastChange]',
            //'language'=> substr(Yii::app()->language,0,strpos(Yii::app()->language,'_')),
            'language' => 'en',
            'value' => $model->lastChange,
            'mode' => 'datetime',
            'options' => array(
                'showAnim' => 'fold', // 'show' (the default), 'slideDown', 'fadeIn', 'fold'
                'showButtonPanel' => true,
                'changeYear' => true,
                'changeMonth' => true,
                'dateFormat' => 'yy-mm-dd',
            ),
                )
        );
        ;
        ?>
<?php echo $form->error($model, 'lastChange'); ?>
    </fieldset>

    <fieldset>
        <?php echo $form->labelEx($model, 'currentLocation'); ?>
        <?php echo $form->textField($model, 'currentLocation', array('size' => 60, 'maxlength' => 128)); ?>
<?php echo $form->error($model, 'currentLocation'); ?>
    </fieldset>

    <fieldset>
        <?php echo $form->labelEx($model, 'comment'); ?>
        <?php echo $form->textArea($model, 'comment', array('rows' => 6, 'cols' => 50)); ?>
    <?php echo $form->error($model, 'comment'); ?>
    </fieldset>
    <?php
    echo CHtml::submitButton(Yii::t('app', 'Save'), array('class' => 'button'));
    $this->endWidget();
    ?>
</div> <!-- form -->

