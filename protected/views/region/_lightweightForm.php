<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>
<div class="row full-width">
    <div class="twelve columns">
        <h4><?= $model->isNewRecord ? 'Создать новое правило' : 'Редактировать правило' ?></h4>

    <?php
    $form=$this->beginWidget('CActiveForm', array(
    'id'=>'ajax-form',
    'enableAjaxValidation'=>false,
    'enableClientValidation'=>true,
    ));

    echo $form->errorSummary($model);
    ?>
    
        
            <?php echo $form->labelEx($model,'idCountry'); ?>
            <?php echo $form->textField($model,'idCountry',array('size'=>10,'maxlength'=>10)); ?>
            <?php echo $form->error($model,'idCountry'); ?>
         
        
        
            <?php echo $form->labelEx($model,'name'); ?>
            <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>64)); ?>
            <?php echo $form->error($model,'name'); ?>
         
        <?php  echo CHtml::ajaxSubmitButton($model->isNewRecord ? 'Создать' : 'Cохранить', Yii::app()->createUrl(
                    $model->isNewRecord ?  'region/create' : 'region/update/' . $model->getPrimaryKey() 
            ), array(
        'type' => 'POST',
        'dataType' => 'json',
        'data' => 'js:$("#ajax-form").serialize()', //this one
        'success' => "js:function(data){
            if(data.result==='success'){
                location.reload();
            }else{
                $('#foundationModal').html(data.msg);
            }
        }",
            ), array('class' => 'button'));
    
    $this->endWidget();
    ?>
    
    </div>
</div> <!-- form -->

