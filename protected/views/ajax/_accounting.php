<h5>Заказ</h5>
<?php
$Curr = Order::getCurrencies();
?>
<table>
    <thead>
        <tr>
            <th>Имя</th>
            <th>SID</th>
            <th>Кол-во</th>
            <th>Стоимость (за ед.)</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($features as $feature) { ?>

            <tr><td><?= $feature->nameUrl ?></td> <td><?= $feature->sid ?></td> 
                <td><?= $feature->count ?></td>
                <td><div class="editable_price" kind="<?= get_class($feature) == "Feature" ? 1 : 0 ?>" amount="<?= $feature->count ?>" currency="<?= $feature->enumCurrency ? $feature->enumCurrency : 1 ?>" value="<?= $feature->price ? $feature->price : 0 ?>" mid="<?= $feature->getPrimaryKey() ?>"><?= $feature->isPriceSettled ? (Helpers::formatPrice($feature->price) . ' ' . $Curr[$feature->enumCurrency]) : '<i>Нет цены</i>' ?></div></td>
            </tr>
        <? } ?>
    </tbody>
</table>

<small>Нажмите на поле цены у соответствующего товара для редактирования его цены. Нажмите ESC для отмены редактирования, ENTER для принятия, TAB для принятия и перехода к следующей цене.</small>

<br/><br/>

<div class="inline_editor" style="display:none">
    <div class="row">
        <div class="five columns"><?php echo CHtml::textField('newPrice', ''); ?></div>
        <div class="three columns"><?php echo CHtml::dropDownList('newPriceCurrency', '', Order::getCurrencies()) ?></div>
        <div class="two columns"><?php echo CHtml::link(CHtml::image('/images/yes_icon.gif'), "#"
                , array('class' => 'small secondary button inline_accept'));
                echo CHtml::link(CHtml::image('/images/no_icon.png'), "#"
                , array('class' => 'small secondary button inline_decline'));
        ?>
        </div>
    </div>
</div>





