<fieldset>
    <legend>Виджет для выбора городов</legend>
    <div class="row">
        <div class="four columns">


            <script>
                function refreshLocationList(entity) {

                    var ajaxAction = (entity === 0) ? 'getRegions' : 'getCities';
                    var controller = (entity === 0) ? function(e) {
                        $('#RegionFilter').html(e).trigger("liszt:updated");
                        $('#Company_idCity').html('').trigger("liszt:updated");
                    } : function(e) {
                        $('#Company_idCity').html(e).trigger("liszt:updated");
                    };

                    $.ajax({
                        url: "/ajax/" + ajaxAction,
                        data: {'idCountry': $('#CountryFilter').val(),
                            'idRegion': $('#RegionFilter').val()
                        },
                        type: "POST",
                        success: controller
                    });
                }
                $(document).ready(function() {
                    $("#CountryFilter").chosen({no_results_text: 'Ничего не найдено'});
                    $("#RegionFilter").chosen({no_results_text: 'Ничего не найдено'});
                    $("#Company_idCity").chosen({no_results_text: 'Ничего не найдено'});
                });
            </script>
            <?php
            Yii::app()->clientScript->registerCssFile(Yii::app()->getBaseUrl() . '/css/chosen.css');
            Yii::app()->clientScript->registerScriptFile(Yii::app()->getBaseUrl() . '/js/chosen/chosen.jquery.js', CClientScript::POS_HEAD);
            $city = null;

            $countries = Country::model()->findAll();
            $countries = CHtml::listData($countries, 'idCountry', 'name');

            $regions = $city ? $city->country->regions : Region::model()->findAll('idCountry=3159');
            $regions = CHtml::listData($regions, 'idRegion', 'name');

            $cities = $city ? City::model()->findAll('idRegion=' . $city->idRegion) : City::model()->findAll('idRegion=4312');
            $cities = CHtml::listData($cities, 'idCity', 'name');
            ?>
            <?php echo CHtml::dropDownList("CountryFilter", $city ? $city->idCountry : 3159, $countries, array('onchange' => 'refreshLocationList(0);')); ?>
        </div>
        <div class="four columns indent-bot-15">
            <?php echo CHtml::dropDownList("RegionFilter", $city ? $city->idRegion : 4312, $regions, array('onchange' => 'refreshLocationList(1);')); ?>
        </div>
        <div class="four columns">
            <?php echo CHtml::dropDownList("Company_idCity", $city ? $city->idCity : 4400, $cities, array('data-placeholder' => 'Выберите страну и город')); ?>
        </div>
    </div>

</fieldset>