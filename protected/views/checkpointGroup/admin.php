<?php

$this->breadcrumbs = array(
    Yii::t('app', 'Checkpoint Groups'),

);
?><header> <h3> Управление  </h3> </header>

<?php Yii::app()->clientScript->registerScriptFile("/js/revealActivate.js", CClientScript::POS_END); ?>

<?php
echo('<dl class="tabs pill">');
$first = true;

foreach ($models as $f) {
    echo($first ? '<dd class="active">' : '<dd>');
    $first = false;
    echo('<a href="#cg' . $f->idCheckpointGroup . '">' . $f->name . '</a></dd>');
}
echo($first ? '<dd class="active">' : '<dd>');
echo('<a href="#cgEmpty">Без группы</a></dd>');
echo('<dd><a class="foundationModalTrigger" href="' . $this->createUrl("CheckpointGroup/create", array('id' => $id, 'returnUrl' => Yii::app()->request->getRequestUri())) . '">+</a></dd>');
echo('</dl>');

echo('<ul class="tabs-content">');
$first = true;

foreach ($models as $f) {

    echo($first ? '<li class="active"' : '<li');
    $first = false;
    echo(" id='cg" . $f->idCheckpointGroup . "Tab'>");
    ?>
    <?php $this->renderPartial("_checkpoints", array('models' => $f->checkpoints, 'idg' => $f->idCheckpointGroup)) ?>
    </li>
<?php } ?>
<li id="cgEmptyTab" <?= $first ? 'class="active"' : '' ?> >
    <?php $this->renderPartial("_checkpoints", array('models' => $notLinkedCheckpoints, 'idg' => 0)) ?>
</li>
</ul>


<script>
    var sort = false;

    function toggle_sort(e)
    {

        sort = !sort;
        $(this).html(sort ? "Сохранить последовательность" : "Сортировать");
        if (sort)
        {
            //$('.sort-container tr td').append('<span class="sortable-tag"><img src="<?= Yii::app()->getBaseUrl() ?>/images/icon_sort.gif" /></span>');
            //$('.sort-container').sortable({handle:'.sortable-tag'});
            $('.sort-container').sortable();
            //                $('.sortable-tag').disableSelection();
            $('.sort-container').sortable('enable');
        }
        else
        {
            window.Sections = {};

            $('.sortable-row').each(function() {
                var gid = $(this).attr("id_group");
                var id = $(this).attr("id_checkpoint");
                if (!window.Sections.hasOwnProperty(gid))
                {
                    window.Sections[gid] = [];
                }
                window.Sections[gid].push(id);
            });


            //console.log(Sections);
            $.ajax({
                url: "<?= Yii::app()->createUrl("checkpointGroup/ajaxSaveOrder") ?>",
                data: {json: JSON.stringify(Sections),
                    '<?= Yii::app()->request->csrfTokenName ?>': '<?= Yii::app()->request->csrfToken ?>'
                },
                type: "POST"
            });

           
            $('.sort-container').sortable('disable');
            //$('.sortable-tag').parent().remove();
        }
        e.preventDefault();
    }

    $(document).ready(function() {
        $('.ch-sort').click(toggle_sort);
        $('.delete-button').click(function(e) {
            var mid = $(this).attr('mid');
            answer = confirm("Вы действительно хотите удалить эту контрольную точку? " + mid);
            if (answer != 0)
            {
                $.post("<?= Yii::app()->createUrl("checkpoint/deleteAjax") ?>", {mid: mid, '<?= Yii::app()->request->csrfTokenName ?>': '<?= Yii::app()->request->csrfToken ?>'},
                function(data)
                {

                    if (data.result == 1)
                    {
                        $("#checkpoint-row-" + data.mid).hide('slow');
                    }
                }

                );
            }
            e.preventDefault();
        });
        $('.delete-n-button').click(function(e) {
            var mid = $(this).attr('mid');
            answer = confirm("Вы действительно хотите удалить этот блок? " + mid);
            if (answer != 0)
            {
                $.post("<?= Yii::app()->createUrl("notice/deleteAjax") ?>", {mid: mid, '<?= Yii::app()->request->csrfTokenName ?>': '<?= Yii::app()->request->csrfToken ?>'},
                function(data)
                {
                    console.log(data.mid);
                    if (data.result == 1)
                    {
                        $("#notice-" + data.mid).hide('slow');
                    }
                }

                );
            }
            e.preventDefault();
        });
    });
</script>
<?php
Yii::app()->getClientScript()->registerCoreScript('jquery.ui');
//Yii:app()->clientScript->registerCoreScript
?>