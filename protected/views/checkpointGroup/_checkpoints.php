<table>
    <thead>

    <th>Название</th>
    <th>Появляется в</th>
    <th>Должен быть готов к</th>
    <th>Настройки</th>
</thead>
<tbody class="sort-container">
    <?php
    foreach ($models as $m) {


        echo("<tr class='sortable-row' id='checkpoint-row-$m->idCheckpoint' id_group='" . $idg . "' id_checkpoint='" . $m->idCheckpoint . "'>");
        ?>


    <td ><?= $m->name ?></td>
    <td ><?= $m->getStartWithName() ?></td>
    <td ><?= $m->getReadyStateName() ?></td>

    <?
    echo("<td>");
    echo("<a class='button small secondary foundationModalTrigger indent-bot-15' href='" . Yii::app()->createUrl("checkpoint/update", array("id" => $m->idCheckpoint, 'returnUrl' => Yii::app()->request->getRequestUri())) . "' >" . CHtml::image(Yii::app()->getBaseUrl() . '/images/icn_edit.png') . "</a> ");
    echo("<a class='button small secondary alert ' href='" . Yii::app()->createUrl("system/confirmDeletion", array(
        "mid" => deletionHelper::D_CHECKPOINT,
        "id" => $m->getPrimaryKey())) . "' mid='" . $m->idCheckpoint . "' >" . CHtml::image(Yii::app()->getBaseUrl() . '/images/icn_trash.png') . "</a>");
    echo("</td>");
}
?>
</tbody>
</table>

<div class="footer_content">
    <a class="button ch-sort" href='#'> Сортировать </a>
    <a class="button secondary foundationModalTrigger" href='<?= $this->createUrl("Checkpoint/create", array('Checkpoint[idCheckpointGroup]' => $idg, 'returnUrl' => Yii::app()->request->getRequestUri())) ?>'> Создать контрольную точку</a>
<?php
echo  CHtml::link('Удалить группу контрольных точек', Yii::app()->createUrl("system/confirmDeletion", array(
            "mid" => deletionHelper::D_CHECKPOINTGROUP,
            "id" => $idg))
        , array('class' => 'alert button del-btn'));
?>
</div>
