<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>

<div class="form">
    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'checkpoint-group-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
            ));

    echo $form->errorSummary($model);
    ?>

    <fieldset>
        <?php echo $form->labelEx($model, 'name'); ?>
        <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 99)); ?>
        <?php echo $form->error($model, 'name'); ?>
    
    </fieldset>


    <?php
    /* @var $model Checkpoint */
    $model_name = get_class($model);

    echo CHtml::ajaxSubmitButton($model->isNewRecord ? 'Создать' : 'Cохранить', Yii::app()->createUrl(
                    $model->isNewRecord ? $model_name . '/create/' . $model->enumProjectClass : $model_name . '/update/' . $model->getPrimaryKey()
            ), array(
        'type' => 'POST',
        'dataType' => 'json',
        'data' => 'js:$("#ajax-form").serialize()', //this one
        'error'=> "js:function(data){  $('#foundationModal').html(data.msg);
            }",
        'success' => "js:function(data){
        if(data.result==='success'){
            location.reload();
        }else{
        $('#foundationModal').html(data.msg);
        }
    }
    else
    {
    $('#foundationModal').html(data);
    }
   }",
            ), array('class' => 'button'));
    ?>

    <?php
    $this->endWidget();
    ?>

    