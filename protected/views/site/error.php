<?php
$this->pageTitle = Yii::app()->name . ' - Ошибка';
$this->breadcrumbs = array(
    'Ошибка',
);
?>

<h2 class="error_header">Ошибка <?php echo $code; ?></h2>

<div class="error error_message">
    <?php echo CHtml::encode($message); ?>
</div>

