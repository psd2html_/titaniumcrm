<h3>Release notes</h3>
<h4>Что нового в версии .21</h4>
<strong>Задачи #13: Доработка Контрольных Точек</strong><br/>
<strong>Задачи #12: Сортировка событий</strong><br/>
<strong>Задачи #14: Бесконечное число клиентов на проект</strong><br/>
<strong>Задачи #15: Подгрузка аттачей к CoreInstrument</strong><br/>
<strong>Задачи #16: Расширение личной сводки до 2х недель</strong><br/>
<strong>Задачи #17: Мелкие текстовые правки</strong><br/>
<strong>Задачи #18: Pop-up</strong><br/>
<strong>Задачи #19: Значения по умолчанию</strong><br/>
<strong>Задачи #22: Предпросмотр КП</strong></strong><br/>
<strong>Задачи #25: Описания на русском</strong></strong><br/>
<strong>Задачи #27: Сводный список по образцам</strong></strong><br/>