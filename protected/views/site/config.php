<?php
$this->pageTitle = Yii::app()->name . ' - Настройка';
$this->breadcrumbs = array(
    'Настройка',
);
?>

<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Общие настройки')
);
?><header> <h3> Настройки</h3> </header>



<div class="form">

    <?= CHtml::beginForm() ?> 

    <?= CHtml::hiddenField('doSave', $nonce) ?>
    <fieldset>
        <div class="row">
            <div class="six columns">
                <label>
                    Логин гугл-учетки
                </label>
                <?= CHtml::textField('googleLogin', isset($s['googleLogin'])?$s['googleLogin']:'', array('size' => 60)); ?>
            </div>
            <div class="six columns">
                <label>
                    Пароль гугл-учетки
                </label>
                <?= CHtml::passwordField('googlePass', isset($s['googlePass'])?$s['googlePass']:"", array('size' => 60)); ?>
            </div>
        </div>

    </fieldset>




    <?= CHtml::submitButton('Сохранить изменения', array('class' => 'button')) ?>
    <?= CHtml::endForm(); ?>
</div>

