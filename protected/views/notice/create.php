<?php
$this->breadcrumbs = array(
     $model->featureGroup->coreInstrument->name=>array('coreInstrument/update','id'=>$model->featureGroup->idCoreInstrument),
    Yii::t('app', 'Create Notice'),
);?>
<header> <h3> Создать </h3> </header>

<div class="module_content">
    
<?php
$this->renderPartial('_form', array(
			'model' => $model,
			'buttons' => 'create'));

?>
</div>