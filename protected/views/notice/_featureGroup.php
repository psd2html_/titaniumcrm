<h5>Отметьте те позиции, к которым нужен данное примечание</h5>
<?php
$description = ManagedBlock::getEnumRules();
//$features=array();
if (count($f->managedBlocks) > 0) {
        echo("<fieldset>");
        echo("<legend>$f->name</legend>");

        foreach ($f->managedBlocks as $m) {

            echo("<fieldset id='managedBlock-".$m->idManagedBlock."' >");
            echo("<div class='row'><div class='nine columns'>");
            if ($m->comment) {
            echo('<b>' . ($m->comment) . '</b><br/><br/>');
            }
            echo('<div class="indent-bot-15">');
            foreach ($m->features as $x) {

                $v = $x->sid . " - " . $x->name;

                switch ($m->enumRule) {
                    case ManagedBlock::SELECT_ONE:
                        echo(CHtml::radioButton("Notice[features][".$m->idManagedBlock."][]", 
                            array_key_exists($m->idManagedBlock, $features)?in_array($x->idFeature, $features[$m->idManagedBlock]):false,
                            array('value'=>$x->idFeature)) . $v);
                        break;
                    case ManagedBlock::SELECT_ONE_OR_MORE:
                    case ManagedBlock::ANY_COMBINATION:
                        echo(CHtml::checkBox("Notice[features][".$m->idManagedBlock."][]",
                                array_key_exists($m->idManagedBlock, $features)?in_array($x->idFeature, $features[$m->idManagedBlock]):false,
                                array('value'=>$x->idFeature)) . $v);
                        break;
                }
                /*
                if (!$x->hasDescription()) 
                {
                    echo(' <span class="alert round label" title="Нет файла с описанием для данной позиции">!</span>');
                }*/
                echo('<br/>');
            }
            echo('</div>');
            echo('<span class="required">*</span><i> ' . $description[$m->enumRule] . '</i><br/><br/>');


            echo("</div><div class='three columns'>");
            echo("</div>");
            echo("</div>");
            echo("</fieldset>");
        }

        echo("</fieldset>"); 
}
