<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>
<div class="form">
    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'notice-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
            ));

    echo $form->errorSummary($model);
    ?>

    <fieldset>

        <?php echo $form->labelEx($model, 'text'); ?>
        <?php
        echo $form->textArea($model,'text',array('rows'=>6, 'cols'=>50)); 
        
        ?>
        <?php echo $form->error($model, 'text'); ?>

        <?php echo $form->labelEx($model, 'mapId'); ?>
        <?php echo $form->textField($model, 'mapId'); ?>
<?php echo $form->error($model, 'mapId'); ?>

    </fieldset>

    <?php echo $form->hiddenField($model, 'idFeatureGroup'); ?>

    <?php
    if ($model->features) {
        $features = $model->features;
    } else {
        $features = array();
    }

    $this->renderPartial('_featureGroup', array('f' => $model->featureGroup, 'features' => $features));
    ?>

    <?php
    echo CHtml::submitButton(Yii::t('app', 'Save'), array('class' => 'button'));
    $this->endWidget();
    ?>
</div> <!-- form -->

