<?php
$this->breadcrumbs = array(
     $model->featureGroup->coreInstrument->name=>array('coreInstrument/update','id'=>$model->featureGroup->idCoreInstrument),
    Yii::t('app', 'Update'),
); ?>
<header> <h3> Редактировать</h3> </header>
<div class="module_content">
<?php
$this->renderPartial('_form', array(
			'model'=>$model));
?>
</div>