<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Insurance Companies') => array('index'),
    Yii::t('app', 'Update'),
); ?>
<header> <h3> <?= $model->name; ?> </h3> </header>
<div class="module_content">
<?php
$this->renderPartial('_form', array(
    'model' => $model,
    'projectDataProvider' => $projectDataProvider,
    'project' => $project));
?>
</div>