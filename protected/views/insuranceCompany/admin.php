<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Insurance Companies')

); ?>
<header><h3> Управление </h3></header>


<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'insurance-company-grid',
    'dataProvider' => $model->search(),
    'cssFile' => false,
    'template' => '{items} {pager}',
    'columns' => array(
        'name',
        'phone',
        'email',
        array('class' => 'CButtonColumn',
            'template' => '{update}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options' => array('class' => 'button secondary tiny delete')
                ),
                'update' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options' => array('class' => 'button secondary tiny update')
                ),
            ),
        ),

    ),
)); ?>

<div class="footer_content">
    <a class="button" href='<?= $this->createUrl("InsuranceCompany/create") ?> '>Создать новый</a>
</div>