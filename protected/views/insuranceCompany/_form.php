<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>
<div class="form">
    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span
            class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>
    <dl class="tabs">
        <dd class="active"><a href="#main">Настройки</a></dd>
    </dl>
    <ul class="tabs-content">
        <li class="active" id="mainTab">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'contragent-form',
                'enableAjaxValidation' => false,
                'enableClientValidation' => true,
            ));
            echo $form->errorSummary($model);
            ?>
            <div class="row">
                <div class="three columns">
                    <?php echo $form->labelEx($model, 'name'); ?>
                    <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 255)); ?>
                    <?php echo $form->error($model, 'name'); ?>
                </div>
                <div class="three columns">
                    <?php echo $form->labelEx($model, 'director'); ?>
                    <?php echo $form->textField($model, 'director', array('size' => 60, 'maxlength' => 255)); ?>
                    <?php echo $form->error($model, 'director'); ?>
                </div>
                <div class="three columns">
                    <?php echo $form->labelEx($model, 'phone'); ?>
                    <?php echo $form->textField($model, 'phone', array('size' => 60, 'maxlength' => 255)); ?>
                    <?php echo $form->error($model, 'phone'); ?>
                </div>
                <div class="three columns">
                    <?php echo $form->labelEx($model, 'email'); ?>
                    <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 255)); ?>
                    <?php echo $form->error($model, 'email'); ?>
                </div>
            </div>

            <div class="row">
                <div class="six columns">
                    <?php echo $form->labelEx($model, 'requisites'); ?>
                    <?php echo $form->textArea($model, 'requisites'); ?>
                    <?php echo $form->error($model, 'requisites'); ?>
                </div>
                <div class="six columns">
                    <?php echo $form->labelEx($model, 'address'); ?>
                    <?php echo $form->textArea($model, 'address'); ?>
                    <?php echo $form->error($model, 'address'); ?>
                </div>
            </div>

            <?php if (!$model->isNewRecord) { ?>
                <h5> Проекты с участием данной страховой компании</h5>

                <?php
                $this->widget('MGridView', array(
                    'id' => 'projects-grid',
                    'dataProvider' => $projectDataProvider,
                    'filter' => $project,
                    'cssFile' => false,
                    'template' => '{items} {pager}',
                    //'columns' => $columns
                ));
                ?>
            <?php } ?>

            <?php
            echo CHtml::submitButton(Yii::t('app', 'Save'), array('class' => 'button'));
            if (!$model->isNewRecord) {
                echo "&nbsp;" . CHtml::link('Удалить', Yii::app()->createUrl("system/confirmDeletion", array(
                            "mid" => deletionHelper::D_INSURANCE,
                            "id" => $model->getPrimaryKey()))
                        , array('class' => 'alert button del-btn'));
            }

            $this->endWidget();
            ?>
        </li>

    </ul>
</div> <!-- form -->

