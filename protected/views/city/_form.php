<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>
<div class="form">
    <p class="note">
        <?php echo Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required');?>.
    </p>

    <?php
    $form=$this->beginWidget('CActiveForm', array(
    'id'=>'city-form',
    'enableAjaxValidation'=>false,
    'enableClientValidation'=>true,
    ));

    echo $form->errorSummary($model);
    ?>
    
        
            <?php echo $form->labelEx($model,'idCountry'); ?>
            <?php echo $form->textField($model,'idCountry',array('size'=>11,'maxlength'=>11)); ?>
            <?php echo $form->error($model,'idCountry'); ?>
         
        
        
            <?php echo $form->labelEx($model,'idRegion'); ?>
            <?php echo $form->textField($model,'idRegion',array('size'=>10,'maxlength'=>10)); ?>
            <?php echo $form->error($model,'idRegion'); ?>
         
        
        
            <?php echo $form->labelEx($model,'name'); ?>
            <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>128)); ?>
            <?php echo $form->error($model,'name'); ?>
         
                    <?php
        echo CHtml::submitButton(Yii::t('app', 'Save'),array('class'=>'button'));
         
        if (!$model->isNewRecord) {
                    echo "&nbsp;" . CHtml::link('Удалить', Yii::app()->createUrl("system/confirmDeletion", array(
                    "mid" => deletionHelper::D_OLOLO,
                    "id" => $model->getPrimaryKey()))
                    , array('class' => 'alert button del-btn'));
        
        }
    $this->endWidget();
        ?>        </div> <!-- form -->

