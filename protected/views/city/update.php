<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Cities') => array('index'),
    Yii::t('app', 'Update'),
); ?>
<header> <h3> <?= $model->name; ?> </h3> </header>
<div class="module_content">
<?php
$this->renderPartial('_form', array(
			'model'=>$model));
?>
</div>