<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Cities') 
    
); ?><header> <h3> Управление  </h3> </header>


<?php $this->widget('MGridView', array(
	'id' => 'city-grid',
	'dataProvider' => $model->search(),
        'cssFile' => false,
	'template' => '{items} {pager}',
	'columns' => array(
        'idCity',
        array(
                			'name' => 'idCountry',
                                        'value' => 'isset($data->idCountry0->name)?$data->idCountry0->name:"N/A"'
                ),
        array(
                			'name' => 'idRegion',
                                        'value' => 'isset($data->idRegion0->name)?$data->idRegion0->name:"N/A"'
                ),
        'name',
array('class' => 'CButtonColumn',
            'template' => '{update}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                    'options'=>array('class'=>'button secondary tiny delete')
                ),
                'update' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options'=>array('class'=>'button secondary tiny update')
                ),
            ),
        ),
        
	),
)); ?>

 <div class="footer_content">
        <a class="button" href='<?= $this->createUrl("City/create")?> '>Создать новый</a>
 </div>