<?php
$this->breadcrumbs = array(
    Yii::t('app', 'Core Instruments') => array('index'),
    $modelCI->name => array('coreInstrument/update', 'id' => $modelCI->idCoreInstrument, '#' => 'form'),
    Yii::t('app', 'Create'),
);?>
<header> <h3> Создать </h3> </header>

<div class="module_content">
    
<?php
$this->renderPartial('_form', array(
			'model' => $model,
			'buttons' => 'create'));

?>
</div>