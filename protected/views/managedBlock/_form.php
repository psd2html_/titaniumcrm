<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>
<div class="form">
    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <?php
    $form = $this->beginWidget('foundation.widgets.FounActiveForm', array(
        'id' => 'managed-block-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
    ));

    echo $form->errorSummary($model);
    ?>


    <?php echo $form->hiddenField($model, 'idCoreInstrument'); ?>
    <fieldset>
        <legend>Группа блоков</legend>
        <?php
        $v = array();
        $v = CHtml::listData(FeatureGroup::model()->findAll('idCoreInstrument=:i', array("i" => $model->idCoreInstrument)), "idFeatureGroup", "name");
        $v['-1'] = 'Создать группу позиций';
        ?>
        <div class="indent-bot-15">
            <?php echo $form->labelEx($model, 'idFeatureGroup'); ?>
            <?php echo $form->dropDownList($model, 'idFeatureGroup', $v); ?>
            <?php echo $form->error($model, 'idFeatureGroup'); ?>
        </div>

        <?php echo CHtml::label('Название группы, если нужно создать', 'customGroup') ?>
        <?php echo CHtml::textField('customGroup') ?>
    </fieldset>

    <fieldset>
        <legend>Блок</legend>
        <?php echo $form->labelEx($model, 'comment'); ?>
        <?php echo $form->textField($model, 'comment', array('rows' => 3, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'comment'); ?>
        <div class="indent-bot-15">
            <?php echo $form->labelEx($model, 'enumRule'); ?>
            <?php echo $form->dropDownList($model, 'enumRule', ManagedBlock::getEnumRules()); ?>
            <?php echo $form->error($model, 'enumRule'); ?>
        </div>
        <?php echo $form->labelEx($model, 'notice'); ?>
        <?php
        $this->widget('ext.niceditor.nicEditorWidget', array(
            "model" => $model, // Data-Model
            "attribute" => 'notice', // Attribute in the Data-Model
            "config" => array("maxHeight" => "400"),
        ));
        ?>
        <?php echo $form->error($model, 'notice'); ?>
        <br/>
    </fieldset>  

    <?php
    $this->widget('ext.autocompleter.autocompleter', array(
        'lockManufacturer'=>$model->coreInstrument->idManufacturer,
        'verifyUrl' => 'ajax/doesFeatureExist',
        'autocompleteUrl' => 'ajax/featureAjax',
        'createUrl' => 'ajax/fetchFeatureAjax',
        'hiddenInput' => '#PersonId',
        'defaultValue' => '',
        'multiselect' => true,
        'multiselect_input' => "features[]",
        'data' => $model->features,
        'wouldCreateNew' => "Артикул не найден.",
        'found' => "Нажмите на Добавить.",
        'badFormat' => "Выберите позицию из предложенных.",
        'promt' => "Введите первые буквы артикула.",
        'allowCreate' => true
    ));
    ?>

    <?php
    echo CHtml::submitButton(Yii::t('app', 'Save'), array('class' => 'button'));
    $this->endWidget();
    ?>
</div> <!-- form -->
