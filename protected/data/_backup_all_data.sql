-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 22, 2017 at 11:52 PM
-- Server version: 5.7.13
-- PHP Version: 5.6.23

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `titaniumcrm`
--

--
-- Dumping data for table `ActiveRecordLog`
--

INSERT INTO `ActiveRecordLog` (`idActiveRecordLog`, `description`, `action`, `model`, `idModel`, `field`, `creationDate`, `idUser`, `slaveModelId`, `newValue`, `slaveModel`) VALUES
(1, 'Создан Centre[Центр-1].', '0', 'Centre', '1', '', '2017-10-21 16:44:46', '1', NULL, NULL, NULL),
(2, 'admin удалил Centre[Центр-1].', '2', 'Centre', '1', '', '2017-10-21 16:45:08', '1', NULL, NULL, NULL),
(3, 'Создан Company[Reebook].', '0', 'Company', '1', '', '2017-10-21 16:50:55', '1', NULL, NULL, NULL),
(4, 'Создан Manufacturer[Reebook].', '0', 'Manufacturer', '2', '', '2017-10-21 16:50:55', '1', NULL, NULL, NULL),
(5, 'Изменено: "Core Instrument Classes" в Manufacturer[Reebook].', '1', 'Manufacturer', '2', 'coreInstrumentClasses', '2017-10-21 17:08:37', '1', '2', 'test_class', 'Manufacturer'),
(6, ' Изменено: "Core Instrument Classes" в Manufacturer[Reebook].', '1', 'Manufacturer', '2', 'coreInstrumentClasses', '2017-10-21 17:08:37', '1', '2', 'Array', 'Manufacturer'),
(7, 'Создан Contragent[ООО Фирма].', '0', 'Contragent', '1', '', '2017-10-21 17:12:02', '1', NULL, NULL, NULL),
(8, 'Создан Custom[фирма-1].', '0', 'Custom', '1', '', '2017-10-21 17:42:55', '1', NULL, NULL, NULL),
(9, 'Создан Feature[позиция-1].', '0', 'Feature', '1', '', '2017-10-21 17:44:11', '1', NULL, NULL, NULL),
(10, 'Создан CompanyClass[ТипКомпани-1].', '0', 'CompanyClass', '1', '', '2017-10-21 18:14:57', '1', NULL, NULL, NULL),
(11, 'Создан Company[comp-1].', '0', 'Company', '2', '', '2017-10-21 18:16:45', '1', NULL, NULL, NULL),
(12, 'Создан CoreInstrument[Инструмент-1].', '0', 'CoreInstrument', '1', '', '2017-10-21 19:07:36', '1', NULL, NULL, NULL),
(13, 'Создан CoreInstrument[Инструмент-1].', '0', 'CoreInstrument', '2', '', '2017-10-21 19:17:34', '1', NULL, NULL, NULL),
(14, ' Изменено: "Описание" в CoreInstrument[Инструмент-1].', '1', 'CoreInstrument', '2', 'description', '2017-10-21 19:21:17', '1', '2', 'описание', 'CoreInstrument'),
(15, ' Изменено: "Перевод Описания" в CoreInstrument[Инструмент-1].', '1', 'CoreInstrument', '2', 'ruDescription', '2017-10-21 19:21:17', '1', '2', 'описание', 'CoreInstrument'),
(16, ' Изменено: "Описание" в CoreInstrument[Инструмент-1].', '1', 'CoreInstrument', '1', 'description', '2017-10-21 19:21:50', '1', '1', 'авыа', 'CoreInstrument'),
(17, ' Изменено: "Перевод Описания" в CoreInstrument[Инструмент-1].', '1', 'CoreInstrument', '1', 'ruDescription', '2017-10-21 19:21:50', '1', '1', 'выавыа', 'CoreInstrument'),
(18, ' Изменено: "Производитель" в CoreInstrument[Инструмент-1].', '1', 'CoreInstrument', '1', 'idManufacturer', '2017-10-21 19:22:03', '1', '1', '2', 'CoreInstrument'),
(19, ' Изменено: "Название" в CoreInstrument[Инструмент-2].', '1', 'CoreInstrument', '2', 'name', '2017-10-21 19:22:20', '1', '2', 'Инструмент-2', 'CoreInstrument'),
(20, ' Изменено: "Валюта" в CoreInstrument[Инструмент-2].', '1', 'CoreInstrument', '2', 'enumCurrency', '2017-10-21 19:22:20', '1', '2', '1', 'CoreInstrument'),
(21, 'Создан Company[comp-1].', '0', 'Company', '3', '', '2017-10-21 22:16:10', '1', NULL, NULL, NULL),
(22, ' Изменено: "Аббревиатура" в CoreInstrumentClass[test_class].', '1', 'CoreInstrumentClass', '1', 'abbr', '2017-10-21 22:23:52', '1', '1', 'abbr_class', 'CoreInstrumentClass'),
(23, ' Изменено: "Аббревиатура" в CoreInstrumentClass[test_class].', '1', 'CoreInstrumentClass', '1', 'abbr', '2017-10-21 22:24:18', '1', '1', 'dsfds', 'CoreInstrumentClass'),
(24, 'Создан Centre[Центр-12].', '0', 'Centre', '2', '', '2017-10-21 22:41:44', '1', NULL, NULL, NULL),
(25, 'Изменено: "В центре есть оборудование от следующих производителей" в Centre[Центр-12].', '1', 'Centre', '2', 'manufacturers', '2017-10-21 22:43:09', '1', '2', 'Addidas', 'Centre'),
(26, ' Изменено: "В центре есть оборудование от следующих производителей" в Centre[Центр-12].', '1', 'Centre', '2', 'manufacturers', '2017-10-21 22:43:09', '1', '2', 'Array', 'Centre'),
(27, 'Изменено: "В центре есть оборудование от следующих производителей" в Centre[Центр-12].', '1', 'Centre', '2', 'manufacturers', '2017-10-21 22:43:43', '1', '2', 'Addidas', 'Centre'),
(28, ' Изменено: "В центре есть оборудование от следующих производителей" в Centre[Центр-12].', '1', 'Centre', '2', 'manufacturers', '2017-10-21 22:43:43', '1', '2', 'Array', 'Centre'),
(29, 'Изменено: "В центре есть оборудование от следующих производителей" в Centre[Центр-12].', '1', 'Centre', '2', 'manufacturers', '2017-10-21 22:44:05', '1', '2', 'Reebook', 'Centre'),
(30, ' Изменено: "В центре есть оборудование от следующих производителей" в Centre[Центр-12].', '1', 'Centre', '2', 'manufacturers', '2017-10-21 22:44:05', '1', '2', 'Array', 'Centre'),
(31, 'Изменено: "В центре есть оборудование от следующих производителей" в Centre[Центр-12].', '1', 'Centre', '2', 'manufacturers', '2017-10-21 22:44:18', '1', '2', 'Addidas', 'Centre'),
(32, ' Изменено: "В центре есть оборудование от следующих производителей" в Centre[Центр-12].', '1', 'Centre', '2', 'manufacturers', '2017-10-21 22:44:18', '1', '2', 'Array', 'Centre'),
(33, 'Изменено: "В центре есть оборудование от следующих производителей" в Centre[Центр-12].', '1', 'Centre', '2', 'manufacturers', '2017-10-21 22:44:30', '1', '2', 'Reebook', 'Centre'),
(34, ' Изменено: "В центре есть оборудование от следующих производителей" в Centre[Центр-12].', '1', 'Centre', '2', 'manufacturers', '2017-10-21 22:44:30', '1', '2', 'Array', 'Centre'),
(35, 'Изменено: "В центре есть оборудование от следующих производителей" в Centre[Центр-12].', '1', 'Centre', '2', 'manufacturers', '2017-10-21 22:46:32', '1', '2', 'Addidas', 'Centre'),
(36, ' Изменено: "В центре есть оборудование от следующих производителей" в Centre[Центр-12].', '1', 'Centre', '2', 'manufacturers', '2017-10-21 22:46:32', '1', '2', 'Array', 'Centre'),
(37, 'Изменено: "В центре есть оборудование от следующих производителей" в Centre[Центр-12].', '1', 'Centre', '2', 'manufacturers', '2017-10-21 22:47:44', '1', '2', 'Addidas', 'Centre'),
(38, ' Изменено: "В центре есть оборудование от следующих производителей" в Centre[Центр-12].', '1', 'Centre', '2', 'manufacturers', '2017-10-21 22:47:44', '1', '2', 'Array', 'Centre'),
(39, 'Изменено: "В центре есть оборудование от следующих производителей" в Centre[Центр-12].', '1', 'Centre', '2', 'manufacturers', '2017-10-21 22:50:27', '1', '2', 'Addidas', 'Centre'),
(40, ' Изменено: "В центре есть оборудование от следующих производителей" в Centre[Центр-12].', '1', 'Centre', '2', 'manufacturers', '2017-10-21 22:50:27', '1', '2', 'Array', 'Centre'),
(41, 'Изменено: "В центре есть оборудование от следующих производителей" в Centre[Центр-12].', '1', 'Centre', '2', 'manufacturers', '2017-10-21 22:52:51', '1', '2', 'Addidas', 'Centre'),
(42, 'Создан Customer[Петров П.П. (Reebook - Директор)].', '0', 'Customer', '1', '', '2017-10-21 23:06:41', '1', NULL, NULL, NULL),
(43, 'Создан Company[орг-1].', '0', 'Company', '4', '', '2017-10-22 23:03:37', '1', NULL, NULL, NULL),
(44, 'Изменено: "Core Instrument Classes" в Manufacturer[Addidas].', '1', 'Manufacturer', '1', 'coreInstrumentClasses', '2017-10-22 23:11:47', '1', '1', 'test_class', 'Manufacturer'),
(45, ' Изменено: "Core Instrument Classes" в Manufacturer[Addidas].', '1', 'Manufacturer', '1', 'coreInstrumentClasses', '2017-10-22 23:11:47', '1', '1', 'Array', 'Manufacturer'),
(46, 'Изменено: "В центре есть оборудование от следующих производителей" в Centre[Центр-12].', '1', 'Centre', '2', 'manufacturers', '2017-10-22 23:12:47', '1', '2', 'Addidas', 'Centre'),
(47, ' Изменено: "В центре есть оборудование от следующих производителей" в Centre[Центр-12].', '1', 'Centre', '2', 'manufacturers', '2017-10-22 23:12:47', '1', '2', 'Array', 'Centre');

--
-- Dumping data for table `attachment`
--

INSERT INTO `attachment` (`idAttachment`, `filename`, `title`, `originalFilename`, `original_filename`) VALUES
(1, 'f35a63f92bd1ba4c023a186b708b48a3', 'ааывавы', 'Test_png_300x225.png', NULL);

--
-- Dumping data for table `attachment2contragent`
--

INSERT INTO `attachment2contragent` (`id`, `idContragent`, `idAttachment`) VALUES
(1, 1, 1);

--
-- Dumping data for table `centre`
--

INSERT INTO `centre` (`idCentre`, `idManufacturer`, `name`, `manufacturers`) VALUES
(2, NULL, 'Центр-12', 'Array');

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`idCity`, `idCountry`, `idRegion`, `name`) VALUES
(1, '1', '1', 'Москва'),
(2, '2', '2', 'Саратов');

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`idCompany`, `idManufacturer`, `idCompanyClass`, `fullName`, `fullNameEng`, `shortName`, `city`, `cityGenetivus`, `physicalAddress`, `physicalAddressEng`, `legalAddress`, `url`, `email`, `phone`, `addPhone`, `comment`, `enumFinanceType`, `inn`, `financeType`, `ogrn`, `bik`, `bankName`, `account`, `correspondentAccount`, `abbr`, `requisites`) VALUES
(1, 2, 14, 'Reebook', NULL, 'Reebook', '-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL),
(2, NULL, 1, 'Компания-1', NULL, 'comp-1', 'Москва', NULL, 'Мира, 20', NULL, 'Мира, 20', 'http://site.com', 'test@test.com', '12121', '21212', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '444', NULL),
(3, NULL, 1, 'Компания-1', NULL, 'comp-1', 'Москва', NULL, 'Мира, 20', NULL, 'мира 20', 'http://site.com', 'test@test.com', '12121', '12222', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', NULL),
(4, NULL, 1, 'Организация-1', 'organizations', 'орг-1', 'Москва', 'ООО Бизнес-Инфо', 'Мира, 20', 'Mira 20', 'юр адрес', 'http://site.com', 'test@test.com', '+380662536608', '+380662536608', 'комментарий', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'org', 'МФО: 121212');

--
-- Dumping data for table `companyclass`
--

INSERT INTO `companyclass` (`idCompanyClass`, `name`) VALUES
(1, 'ТипКомпани-1');

--
-- Dumping data for table `contragent`
--

INSERT INTO `contragent` (`idContragent`, `name`, `director`, `requisites`, `phone`, `email`, `address`) VALUES
(1, 'ООО Фирма', 'Иванов Иванович', '12542, МФО 1309', '123-456-789', 'test@test.com', 'Мира, 20');

--
-- Dumping data for table `coreinstrument`
--

INSERT INTO `coreinstrument` (`idCoreInstrument`, `idCoreInstrumentClass`, `idManufacturer`, `name`, `sid`, `description`, `price`, `enumCurrency`, `isPriceSettled`, `ruName`, `ruDescription`) VALUES
(1, 1, 2, 'Инструмент-1', '1', 'авыа', 111, 0, '1', 'inst-1', 'выавыа'),
(2, 1, 1, 'Инструмент-2', '2', 'описание', 111, 1, '1', 'inst-1', 'описание');

--
-- Dumping data for table `coreinstrumentclass`
--

INSERT INTO `coreinstrumentclass` (`idCoreInstrumentClass`, `name`, `abbr`) VALUES
(1, 'test_class', 'dsfds');

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`idCountry`, `name`) VALUES
(1, 'Россия'),
(2, 'Украина');

--
-- Dumping data for table `custom`
--

INSERT INTO `custom` (`idCustom`, `name`) VALUES
(1, 'фирма-1');

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`idCustomer`, `idCompany`, `firstName`, `lastName`, `middleName`, `address`, `email`, `email2`, `skype`, `icq`, `corporation`, `phone`, `phone2`, `mobilePhone`, `customerComment`, `job`, `comment`, `bothName`) VALUES
(1, 1, 'Петя', 'Петров', 'Петрович', 'Мира, 20', 'test@test.com', 'Мира 55', 'petrob', NULL, NULL, '123', '123', '123', '', 'Директор', NULL, NULL);

--
-- Dumping data for table `feature`
--

INSERT INTO `feature` (`idFeature`, `sid`, `name`, `count`, `amount`, `isPriceSettled`, `ruName`, `description`, `isFreeOfCharge`, `ruDescription`, `idManufacturer`, `price`, `enumCurrency`) VALUES
(1, 'SKU-111', 'позиция-1', 1, 1, 1, 'pos-1', 'описание', NULL, 'описание', '1', 100, 0);

--
-- Dumping data for table `group`
--

INSERT INTO `group` (`idGroup`, `name`, `jsonEncodedRules`, `system`, `isLogist`, `startingTab`) VALUES
(1, 'группа-1', '{"1":[["1","8"]],"2":[["1","8"]],"3":[["1","8"]],"4":[["1","8"]],"5":[["1","8"]],"6":[["1","8"]],"7":{"1":["2","7","3","1","8"],"0":["1","8"]},"30":[["1","8"]],"8":[["1","8"]],"9":{"1":["2","7","3","1","8"],"0":["1","8"]},"10":{"2":["2","7","3","1","8"],"0":["1","8"]},"11":{"5":["2","7","3","1","8"],"6":["2","7","3","1","8"],"2":["2","7","3","1","8"]},"13":{"5":["4"],"6":["4"],"2":["4"],"100":["6"],"120":["6"],"130":["6"],"160":["6"],"170":["6"],"200":["6"],"0":["6"]},"14":{"5":["4"],"6":["4"],"2":["4"],"100":["6"],"120":["6"],"130":["6"],"160":["6"],"170":["6"],"200":["6"],"0":["6"]},"15":{"5":["4"],"6":["4"],"2":["4"],"100":["6"],"120":["6"],"130":["6"],"160":["6"],"170":["6"],"200":["6"],"0":["6"]},"16":{"5":["4"],"6":["4"],"2":["4"],"100":["6"],"120":["6"],"130":["6"],"160":["6"],"170":["6"],"200":["6"],"0":["6"]},"17":{"5":["4"],"6":["4"],"2":["4"],"100":["6"],"120":["6"],"130":["6"],"160":["6"],"170":["6"],"200":["6"],"0":["6"]},"18":{"5":["4"],"6":["4"],"2":["4"],"100":["6"],"120":["6"],"130":["6"],"160":["6"],"170":["6"],"200":["6"],"0":["6"]},"19":{"5":["4"],"6":["4"],"2":["4"],"100":["6"],"120":["6"],"130":["6"],"160":["6"],"170":["6"],"200":["6"],"0":["6"]},"20":{"5":["4"],"6":["4"],"2":["4"],"100":["6"],"120":["6"],"130":["6"],"160":["6"],"170":["6"],"200":["6"],"0":["6"]},"21":{"5":["4"],"6":["4"],"2":["4"],"100":["6"],"120":["6"],"130":["6"],"160":["6"],"170":["6"],"200":["6"],"0":["6"]},"22":{"5":["4"],"6":["4"],"2":["4"],"100":["6"],"120":["6"],"130":["6"],"160":["6"],"170":["6"],"200":["6"],"0":["6"]}}', NULL, 0, 0);

--
-- Dumping data for table `insuranceCompany`
--

INSERT INTO `insuranceCompany` (`idInsuranceCompany`, `name`, `director`, `phone`, `email`, `requisites`, `address`) VALUES
(1, 'Страховя-1', 'Иванов Иванович', '123-456', 'test@test.com', '123123213', 'Мира, 20');

--
-- Dumping data for table `manufacturer`
--

INSERT INTO `manufacturer` (`idManufacturer`, `name`, `abbr`, `coreInstrumentClasses`, `motto`) VALUES
(1, 'Addidas', 'test', 'Array', 'test'),
(2, 'Reebook', 'rbk', 'Array', 'девиз');

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`idPermission`, `enumPermissionType`, `idUser`, `idManufacturer`, `idCoreInstrumentClass`) VALUES
(1, '2', 1, 1, 1);

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`user_id`, `lastname`, `firstname`) VALUES
(1, 'Admin', 'Administrator'),
(2, 'Demo', 'Demo');

--
-- Dumping data for table `profiles_fields`
--

INSERT INTO `profiles_fields` (`id`, `varname`, `title`, `field_type`, `field_size`, `field_size_min`, `required`, `match`, `range`, `error_message`, `other_validator`, `default`, `widget`, `widgetparams`, `position`, `visible`) VALUES
(1, 'lastname', 'Last Name', 'VARCHAR', '50', '3', 1, '', '', 'Incorrect Last Name (length between 3 and 50 characters).', '', '', '', '', 1, 3),
(2, 'firstname', 'First Name', 'VARCHAR', '50', '3', 1, '', '', 'Incorrect First Name (length between 3 and 50 characters).', '', '', '', '', 0, 3);

--
-- Dumping data for table `region`
--

INSERT INTO `region` (`idRegion`, `idCountry`, `name`) VALUES
(1, '1', 'Хабаровскйи Край');

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `activkey`, `create_at`, `lastvisit_at`, `superuser`, `status`, `personalFooter`, `abbr`, `permissions`, `group`, `idGroup`, `columns`, `personalFooter2`, `personalFooter3`, `serializedPreferences`, `serializedPersonalFilter`, `serializedPermissions`, `serializedColumns`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'webmaster@example.com', '9a24eff8c15a6a141ece27eb6947da0f', '2017-10-21 11:18:39', '2017-10-22 18:04:00', 1, 1, 'Lolol', 0, NULL, 0, 1, NULL, '123-456-789', 'director@mail.ru', '{"v":3,"drc":10,"dpc":5,"pis":1,"ces":";"}', NULL, NULL, '[]'),
(2, 'demo', 'fe01ce2a7fbac8fafaed7c982a04e229', 'demo@example.com', '099f825543f7850cc038b90aaff39fac', '2017-10-21 11:18:39', '2017-10-22 20:51:54', 0, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '{"v":3,"drc":10,"dpc":5,"pis":1,"ces":";"}', NULL, NULL, '[]');
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
