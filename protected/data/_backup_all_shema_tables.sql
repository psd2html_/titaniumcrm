-- --------------------------------------------------------

--
-- Table structure for table `ActiveRecordLog`
--

DROP TABLE IF EXISTS `ActiveRecordLog`;
CREATE TABLE IF NOT EXISTS `ActiveRecordLog` (
  `idActiveRecordLog` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `action` varchar(20) DEFAULT NULL,
  `model` varchar(45) DEFAULT NULL,
  `idModel` varchar(10) DEFAULT NULL,
  `field` varchar(45) DEFAULT NULL,
  `creationDate` varchar(255) NOT NULL,
  `idUser` varchar(45) DEFAULT NULL,
  `slaveModelId` varchar(255) DEFAULT NULL,
  `newValue` varchar(255) DEFAULT NULL,
  `slaveModel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `atomicRbac`
--

DROP TABLE IF EXISTS `atomicRbac`;
CREATE TABLE IF NOT EXISTS `atomicRbac` (
  `idAtomicRbac` int(11) NOT NULL,
  `enumRuleClass` int(11) NOT NULL,
  `enumRuleValue` int(11) NOT NULL,
  `idProject` int(11) DEFAULT NULL,
  `idUser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `attachement2logisticfragment`
--

DROP TABLE IF EXISTS `attachement2logisticfragment`;
CREATE TABLE IF NOT EXISTS `attachement2logisticfragment` (
  `id` int(11) NOT NULL,
  `idLogisticFragment` int(11) NOT NULL,
  `idAttachment` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `attachment`
--

DROP TABLE IF EXISTS `attachment`;
CREATE TABLE IF NOT EXISTS `attachment` (
  `idAttachment` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `originalFilename` varchar(255) NOT NULL,
  `original_filename` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `attachment2contest`
--

DROP TABLE IF EXISTS `attachment2contest`;
CREATE TABLE IF NOT EXISTS `attachment2contest` (
  `id` int(11) NOT NULL,
  `idContest` int(11) NOT NULL,
  `idAttachment` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `attachment2contragent`
--

DROP TABLE IF EXISTS `attachment2contragent`;
CREATE TABLE IF NOT EXISTS `attachment2contragent` (
  `id` int(11) NOT NULL,
  `idContragent` int(11) NOT NULL,
  `idAttachment` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `attachment2coreinstrument`
--

DROP TABLE IF EXISTS `attachment2coreinstrument`;
CREATE TABLE IF NOT EXISTS `attachment2coreinstrument` (
  `id` int(11) NOT NULL,
  `idCoreInstrument` int(11) NOT NULL,
  `idAttachment` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `attachment2event`
--

DROP TABLE IF EXISTS `attachment2event`;
CREATE TABLE IF NOT EXISTS `attachment2event` (
  `id` int(11) NOT NULL,
  `idEvent` int(11) NOT NULL,
  `idAttachment` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `attachment2logistic`
--

DROP TABLE IF EXISTS `attachment2logistic`;
CREATE TABLE IF NOT EXISTS `attachment2logistic` (
  `id` int(11) NOT NULL,
  `idLogistic` int(11) NOT NULL,
  `idAttachment` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `attachment2order`
--

DROP TABLE IF EXISTS `attachment2order`;
CREATE TABLE IF NOT EXISTS `attachment2order` (
  `id` int(11) NOT NULL,
  `idOrder` int(11) NOT NULL,
  `idAttachment` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `attachment2project`
--

DROP TABLE IF EXISTS `attachment2project`;
CREATE TABLE IF NOT EXISTS `attachment2project` (
  `id` int(11) NOT NULL,
  `idProject` int(11) NOT NULL,
  `idAttachment` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `attachment2sample`
--

DROP TABLE IF EXISTS `attachment2sample`;
CREATE TABLE IF NOT EXISTS `attachment2sample` (
  `id` int(11) NOT NULL,
  `idSample` int(11) NOT NULL,
  `idAttachment` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `centre`
--

DROP TABLE IF EXISTS `centre`;
CREATE TABLE IF NOT EXISTS `centre` (
  `idCentre` int(11) NOT NULL,
  `idManufacturer` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `manufacturers` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `centre2manufacturer`
--

DROP TABLE IF EXISTS `centre2manufacturer`;
CREATE TABLE IF NOT EXISTS `centre2manufacturer` (
  `id` int(11) NOT NULL,
  `idManufacturer` int(11) NOT NULL,
  `idCentre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `checkpoint`
--

DROP TABLE IF EXISTS `checkpoint`;
CREATE TABLE IF NOT EXISTS `checkpoint` (
  `idCheckpoint` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `idCheckpointGroup` int(11) DEFAULT NULL,
  `readyState` int(11) DEFAULT NULL,
  `startWith` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `checkpointGroup`
--

DROP TABLE IF EXISTS `checkpointGroup`;
CREATE TABLE IF NOT EXISTS `checkpointGroup` (
  `idCheckpointGroup` int(11) NOT NULL,
  `name` varchar(99) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
CREATE TABLE IF NOT EXISTS `city` (
  `idCity` int(11) NOT NULL,
  `idCountry` varchar(11) DEFAULT NULL,
  `idRegion` varchar(10) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
CREATE TABLE IF NOT EXISTS `company` (
  `idCompany` int(11) NOT NULL,
  `idManufacturer` int(11) DEFAULT NULL,
  `idCompanyClass` int(11) NOT NULL,
  `fullName` varchar(255) NOT NULL,
  `fullNameEng` varchar(255) DEFAULT NULL,
  `shortName` varchar(64) NOT NULL,
  `city` varchar(45) NOT NULL,
  `cityGenetivus` varchar(255) DEFAULT NULL,
  `physicalAddress` varchar(255) DEFAULT NULL,
  `physicalAddressEng` varchar(255) DEFAULT NULL,
  `legalAddress` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `addPhone` varchar(45) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `enumFinanceType` int(11) DEFAULT NULL,
  `inn` varchar(64) DEFAULT NULL,
  `financeType` varchar(255) DEFAULT NULL,
  `ogrn` varchar(64) DEFAULT NULL,
  `bik` varchar(64) DEFAULT NULL,
  `bankName` varchar(64) DEFAULT NULL,
  `account` varchar(64) DEFAULT NULL,
  `correspondentAccount` varchar(64) DEFAULT NULL,
  `abbr` varchar(255) NOT NULL,
  `requisites` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `companyclass`
--

DROP TABLE IF EXISTS `companyclass`;
CREATE TABLE IF NOT EXISTS `companyclass` (
  `idCompanyClass` int(11) NOT NULL,
  `name` varchar(99) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contest`
--

DROP TABLE IF EXISTS `contest`;
CREATE TABLE IF NOT EXISTS `contest` (
  `id` int(11) NOT NULL,
  `idProject` int(11) NOT NULL,
  `contestUrl` varchar(255) DEFAULT NULL,
  `contestSiteUrl` varchar(255) DEFAULT NULL,
  `enumStatus` int(11) DEFAULT NULL,
  `datePost` varchar(255) DEFAULT NULL,
  `dateRequest` varchar(255) DEFAULT NULL,
  `dateResponse` varchar(255) DEFAULT NULL,
  `dateContest` varchar(255) DEFAULT NULL,
  `notification` varchar(80) DEFAULT NULL,
  `costLotCurrency` int(11) DEFAULT NULL,
  `costAskCurrency` int(11) DEFAULT NULL,
  `costContractCurrency` int(11) DEFAULT NULL,
  `costLot` int(11) DEFAULT NULL,
  `costAsk` int(11) DEFAULT NULL,
  `costContract` int(11) DEFAULT NULL,
  `number` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contragent`
--

DROP TABLE IF EXISTS `contragent`;
CREATE TABLE IF NOT EXISTS `contragent` (
  `idContragent` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `director` varchar(255) DEFAULT NULL,
  `requisites` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `coreinstrument`
--

DROP TABLE IF EXISTS `coreinstrument`;
CREATE TABLE IF NOT EXISTS `coreinstrument` (
  `idCoreInstrument` int(11) NOT NULL,
  `idCoreInstrumentClass` int(11) NOT NULL,
  `idManufacturer` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sid` varchar(45) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `enumCurrency` int(4) DEFAULT NULL,
  `isPriceSettled` varchar(255) DEFAULT NULL,
  `ruName` varchar(255) DEFAULT NULL,
  `ruDescription` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `coreinstrumentclass`
--

DROP TABLE IF EXISTS `coreinstrumentclass`;
CREATE TABLE IF NOT EXISTS `coreinstrumentclass` (
  `idCoreInstrumentClass` int(11) NOT NULL,
  `name` varchar(90) NOT NULL,
  `abbr` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
CREATE TABLE IF NOT EXISTS `country` (
  `idCountry` int(11) NOT NULL,
  `name` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `custom`
--

DROP TABLE IF EXISTS `custom`;
CREATE TABLE IF NOT EXISTS `custom` (
  `idCustom` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE IF NOT EXISTS `customer` (
  `idCustomer` int(11) NOT NULL,
  `idCompany` int(11) DEFAULT NULL,
  `firstName` varchar(90) NOT NULL,
  `lastName` varchar(90) NOT NULL,
  `middleName` varchar(90) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `email2` varchar(255) DEFAULT NULL,
  `skype` varchar(255) DEFAULT NULL,
  `icq` varchar(255) DEFAULT NULL,
  `corporation` varchar(90) DEFAULT NULL,
  `phone` varchar(90) DEFAULT NULL,
  `phone2` varchar(90) DEFAULT NULL,
  `mobilePhone` varchar(255) DEFAULT NULL,
  `customerComment` varchar(255) DEFAULT NULL,
  `job` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `bothName` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer2project`
--

DROP TABLE IF EXISTS `customer2project`;
CREATE TABLE IF NOT EXISTS `customer2project` (
  `id` int(11) NOT NULL,
  `idProject` int(11) NOT NULL,
  `idCustomer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
CREATE TABLE IF NOT EXISTS `event` (
  `idEvent` int(11) NOT NULL,
  `idProject` int(11) NOT NULL,
  `enumType` int(11) NOT NULL,
  `eventClass` int(11) DEFAULT NULL,
  `title` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `idUser` int(11) DEFAULT NULL,
  `searchEnumClass` varchar(255) DEFAULT NULL,
  `idCheckpoint` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `event2logistic`
--

DROP TABLE IF EXISTS `event2logistic`;
CREATE TABLE IF NOT EXISTS `event2logistic` (
  `id` int(11) NOT NULL,
  `idLogistic` int(11) NOT NULL,
  `idEvent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `feature`
--

DROP TABLE IF EXISTS `feature`;
CREATE TABLE IF NOT EXISTS `feature` (
  `idFeature` int(11) NOT NULL,
  `sid` varchar(45) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `isPriceSettled` int(11) DEFAULT NULL,
  `ruName` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `isFreeOfCharge` varchar(255) DEFAULT NULL,
  `ruDescription` varchar(255) DEFAULT NULL,
  `idManufacturer` varchar(255) NOT NULL,
  `price` int(11) DEFAULT NULL,
  `enumCurrency` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `featuregroup`
--

DROP TABLE IF EXISTS `featuregroup`;
CREATE TABLE IF NOT EXISTS `featuregroup` (
  `idFeatureGroup` int(11) NOT NULL,
  `idCoreInstrument` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `order` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `folder`
--

DROP TABLE IF EXISTS `folder`;
CREATE TABLE IF NOT EXISTS `folder` (
  `idFolder` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `idProject` int(11) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `enumSpecialFolder` int(11) DEFAULT NULL,
  `link` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `group`
--

DROP TABLE IF EXISTS `group`;
CREATE TABLE IF NOT EXISTS `group` (
  `idGroup` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `jsonEncodedRules` text,
  `system` int(11) DEFAULT NULL,
  `isLogist` tinyint(1) DEFAULT NULL,
  `startingTab` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `insuranceCompany`
--

DROP TABLE IF EXISTS `insuranceCompany`;
CREATE TABLE IF NOT EXISTS `insuranceCompany` (
  `idInsuranceCompany` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `director` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `requisites` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `logistic`
--

DROP TABLE IF EXISTS `logistic`;
CREATE TABLE IF NOT EXISTS `logistic` (
  `idLogistic` int(11) NOT NULL,
  `idProject` int(11) NOT NULL,
  `enumStatus` varchar(45) DEFAULT NULL,
  `lastChange` varchar(255) DEFAULT NULL,
  `currentLocation` varchar(128) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `shipmentDeadline` varchar(255) DEFAULT NULL,
  `productionDeadline` varchar(255) DEFAULT NULL,
  `idContragent` int(11) DEFAULT NULL,
  `enumTransportBeforeBorder` int(11) DEFAULT NULL,
  `enumTransportAfterBorder` int(11) DEFAULT NULL,
  `idCustom` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `logisticFragment`
--

DROP TABLE IF EXISTS `logisticFragment`;
CREATE TABLE IF NOT EXISTS `logisticFragment` (
  `idLogisticFragment` int(11) NOT NULL,
  `cityFrom` varchar(255) NOT NULL,
  `cityTo` varchar(255) NOT NULL,
  `dateUnload` varchar(255) DEFAULT NULL,
  `dateUpload` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `enumTransport` int(11) NOT NULL,
  `idProject` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `logisticPosition`
--

DROP TABLE IF EXISTS `logisticPosition`;
CREATE TABLE IF NOT EXISTS `logisticPosition` (
  `idLogisticPosition` int(11) NOT NULL,
  `innerNumber` int(11) DEFAULT NULL,
  `idInsuranceCompany` int(11) DEFAULT NULL,
  `idCoreInstrument` int(11) DEFAULT NULL,
  `idFeature` int(11) DEFAULT NULL,
  `idProject` int(11) NOT NULL,
  `productionDeadline` varchar(255) DEFAULT NULL,
  `poNumber` varchar(255) DEFAULT NULL,
  `goodsNameRus` varchar(255) DEFAULT NULL,
  `goodsOrigin` varchar(255) DEFAULT NULL,
  `goodsNameEng` varchar(255) DEFAULT NULL,
  `isPurchaseViaManufacturer` int(11) DEFAULT NULL,
  `isTechnoinfoUnloadsGoods` int(11) DEFAULT NULL,
  `isExportDeclarationMade` int(11) DEFAULT NULL,
  `idPackingSheetUpload` int(11) DEFAULT NULL,
  `idDescriptionUpload` int(11) DEFAULT NULL,
  `isSpecialDeliveryRequired` int(11) DEFAULT NULL,
  `isExportDoubleApplicationLicenseRequired` int(11) DEFAULT NULL,
  `isInsurancePayed` int(11) DEFAULT NULL,
  `packagingSpecification` varchar(255) DEFAULT NULL,
  `buyIncotermsCondition` varchar(255) DEFAULT NULL,
  `sellIncotermsCondition` varchar(255) DEFAULT NULL,
  `takeoutAddress` varchar(255) DEFAULT NULL,
  `tnvedCod` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `managedblock`
--

DROP TABLE IF EXISTS `managedblock`;
CREATE TABLE IF NOT EXISTS `managedblock` (
  `idManagedBlock` int(11) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `notice` varchar(255) DEFAULT NULL,
  `enumRule` int(11) NOT NULL,
  `idCoreInstrument` varchar(255) NOT NULL,
  `idFeatureGroup` varchar(255) DEFAULT NULL,
  `save` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `managedblockfeature`
--

DROP TABLE IF EXISTS `managedblockfeature`;
CREATE TABLE IF NOT EXISTS `managedblockfeature` (
  `id` int(11) NOT NULL,
  `idFeature` int(11) NOT NULL,
  `idManagedBlock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `manufacture2coreinstrumentclass`
--

DROP TABLE IF EXISTS `manufacture2coreinstrumentclass`;
CREATE TABLE IF NOT EXISTS `manufacture2coreinstrumentclass` (
  `id` int(11) NOT NULL,
  `idManufacture` int(11) NOT NULL,
  `idCoreInstrumentClass` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer`
--

DROP TABLE IF EXISTS `manufacturer`;
CREATE TABLE IF NOT EXISTS `manufacturer` (
  `idManufacturer` int(11) NOT NULL,
  `name` varchar(99) NOT NULL,
  `abbr` varchar(10) NOT NULL,
  `coreInstrumentClasses` varchar(255) DEFAULT NULL,
  `motto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

DROP TABLE IF EXISTS `notice`;
CREATE TABLE IF NOT EXISTS `notice` (
  `idNotice` int(11) NOT NULL,
  `text` varchar(255) NOT NULL,
  `idFeatureGroup` int(11) NOT NULL,
  `mapId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `notice2link`
--

DROP TABLE IF EXISTS `notice2link`;
CREATE TABLE IF NOT EXISTS `notice2link` (
  `id` int(11) NOT NULL,
  `idLink` int(11) NOT NULL,
  `idNotice` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
CREATE TABLE IF NOT EXISTS `order` (
  `idOrder` int(11) NOT NULL,
  `enumCalculationAlgorithm` int(11) DEFAULT NULL,
  `idCoreInstrument` int(11) DEFAULT NULL,
  `idProject` int(11) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `date` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `enumOrderType` int(11) DEFAULT NULL,
  `enumPricePolicy` int(11) DEFAULT NULL,
  `idUser` int(11) NOT NULL,
  `idOrderInProject` int(11) NOT NULL,
  `idCustomer` int(11) DEFAULT NULL,
  `showCIP` int(11) DEFAULT NULL,
  `showDDP` int(11) DEFAULT NULL,
  `showEXW` int(11) DEFAULT NULL,
  `isFeaturePriceShown` tinyint(1) DEFAULT NULL,
  `isRussianDescriptionLoaded` tinyint(1) DEFAULT NULL,
  `isDeliverySpreadBetweenFeatures` tinyint(1) DEFAULT NULL,
  `destinationCity` varchar(255) DEFAULT NULL,
  `destinationCityGenetivus` varchar(255) DEFAULT NULL,
  `idCompany` int(11) DEFAULT NULL,
  `guaranteeInMonth` int(11) DEFAULT NULL,
  `guaranteeLessThan` int(11) DEFAULT NULL,
  `deliver` varchar(255) DEFAULT NULL,
  `enumPaymentCondition` int(11) DEFAULT NULL,
  `lastDate` varchar(255) DEFAULT NULL,
  `cipCurrency` int(11) DEFAULT NULL,
  `doThreeMonth` int(11) DEFAULT NULL,
  `outCurrency` int(11) DEFAULT NULL,
  `idCoreInstrumentClass` varchar(255) DEFAULT NULL,
  `idCoreInstrumentManufacturer` varchar(255) DEFAULT NULL,
  `entryId` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `cipPayment` int(11) DEFAULT NULL,
  `ddpCoeff` int(11) DEFAULT NULL,
  `saleCoeff` int(11) DEFAULT NULL,
  `overridenPrice` int(11) DEFAULT NULL,
  `eurRub` int(11) DEFAULT NULL,
  `usdRub` int(11) DEFAULT NULL,
  `gbpRub` int(11) DEFAULT NULL,
  `yenRub` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `orderfeature`
--

DROP TABLE IF EXISTS `orderfeature`;
CREATE TABLE IF NOT EXISTS `orderfeature` (
  `id` int(11) NOT NULL,
  `idOrder` int(11) NOT NULL,
  `idFeature` int(11) NOT NULL,
  `idManagedBlock` int(11) NOT NULL,
  `idLink` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `orderungroupedfeature`
--

DROP TABLE IF EXISTS `orderungroupedfeature`;
CREATE TABLE IF NOT EXISTS `orderungroupedfeature` (
  `id` int(11) NOT NULL,
  `idFeature` int(11) NOT NULL,
  `idOrder` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

DROP TABLE IF EXISTS `permission`;
CREATE TABLE IF NOT EXISTS `permission` (
  `idPermission` int(11) NOT NULL,
  `enumPermissionType` varchar(255) NOT NULL,
  `idUser` int(11) NOT NULL,
  `idManufacturer` int(11) DEFAULT NULL,
  `idCoreInstrumentClass` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pm`
--

DROP TABLE IF EXISTS `pm`;
CREATE TABLE IF NOT EXISTS `pm` (
  `idPm` int(11) NOT NULL,
  `idUser` int(11) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `idRecipient` int(11) NOT NULL,
  `body` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created` varchar(255) NOT NULL,
  `idProject` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
CREATE TABLE IF NOT EXISTS `profiles` (
  `user_id` int(11) NOT NULL,
  `lastname` varchar(50) NOT NULL DEFAULT '',
  `firstname` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `profiles_fields`
--

DROP TABLE IF EXISTS `profiles_fields`;
CREATE TABLE IF NOT EXISTS `profiles_fields` (
  `id` int(10) NOT NULL,
  `varname` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `field_type` varchar(50) NOT NULL,
  `field_size` varchar(15) NOT NULL DEFAULT '0',
  `field_size_min` varchar(15) NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL DEFAULT '0',
  `match` varchar(255) NOT NULL DEFAULT '',
  `range` varchar(255) NOT NULL DEFAULT '',
  `error_message` varchar(255) NOT NULL DEFAULT '',
  `other_validator` varchar(5000) NOT NULL DEFAULT '',
  `default` varchar(255) NOT NULL DEFAULT '',
  `widget` varchar(255) NOT NULL DEFAULT '',
  `widgetparams` varchar(5000) NOT NULL DEFAULT '',
  `position` int(3) NOT NULL DEFAULT '0',
  `visible` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
CREATE TABLE IF NOT EXISTS `project` (
  `idProject` int(11) NOT NULL,
  `overrideFilter` tinyint(1) DEFAULT NULL,
  `lastChange` timestamp NULL DEFAULT NULL,
  `lastChangeId` int(11) DEFAULT NULL,
  `contractTotalCurrency` int(11) DEFAULT NULL,
  `logisticPositionCounter` int(11) DEFAULT NULL,
  `idInsuranceCompany` int(11) DEFAULT NULL,
  `poCounter` int(11) DEFAULT NULL,
  `kpCounter` int(11) DEFAULT NULL,
  `sampleCounter` int(11) DEFAULT NULL,
  `enumStatus` int(11) DEFAULT NULL,
  `buyIncotermsCondition` int(11) DEFAULT NULL,
  `sellIncotermsCondition` int(11) DEFAULT NULL,
  `enumFinanceStatus` int(11) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `payedByUser` varchar(255) DEFAULT NULL,
  `quartal` int(11) DEFAULT NULL,
  `innerName` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `takeoutAddress` text,
  `parent` int(11) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `idCustomer` int(11) DEFAULT NULL,
  `enumProvision` int(11) DEFAULT NULL,
  `idResponsible` int(11) DEFAULT NULL,
  `idCompanyConsumer` int(11) DEFAULT NULL,
  `idCompanyBuyer` int(11) DEFAULT NULL,
  `tnvedCod` varchar(255) DEFAULT NULL,
  `idManufacturer` varchar(255) DEFAULT NULL,
  `idCoreInstrumentClass` varchar(255) DEFAULT NULL,
  `earliestSampleStatus` varchar(255) DEFAULT NULL,
  `countOfOrders` varchar(255) DEFAULT NULL,
  `isInsurancePayed` varchar(255) DEFAULT NULL,
  `isExportDeclarationMade` varchar(255) DEFAULT NULL,
  `isExportDoubleApplicationLicenseRequired` varchar(255) DEFAULT NULL,
  `isSpecialDeliveryRequired` varchar(255) DEFAULT NULL,
  `isTechnoinfoUnloadsGoods` varchar(255) DEFAULT NULL,
  `projectName` varchar(255) DEFAULT NULL,
  `packagingSpecification` varchar(255) DEFAULT NULL,
  `idCoreInstrument` varchar(255) DEFAULT NULL,
  `idContractUpload` int(11) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `setupDate` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `projectWorkflow`
--

DROP TABLE IF EXISTS `projectWorkflow`;
CREATE TABLE IF NOT EXISTS `projectWorkflow` (
  `idProjectWorkflow` int(11) NOT NULL,
  `name` varchar(90) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `enumState` int(11) NOT NULL,
  `nextStages` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `projectworkflow2projectworkflow`
--

DROP TABLE IF EXISTS `projectworkflow2projectworkflow`;
CREATE TABLE IF NOT EXISTS `projectworkflow2projectworkflow` (
  `id` int(11) NOT NULL,
  `idFrom` int(11) NOT NULL,
  `idTo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
CREATE TABLE IF NOT EXISTS `region` (
  `idRegion` int(11) NOT NULL,
  `idCountry` varchar(10) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sample`
--

DROP TABLE IF EXISTS `sample`;
CREATE TABLE IF NOT EXISTS `sample` (
  `idSample` int(11) NOT NULL,
  `idProject` int(11) NOT NULL,
  `recievedDate` varchar(255) NOT NULL,
  `lastChanged` varchar(255) DEFAULT NULL,
  `task` varchar(255) NOT NULL,
  `enumStatus` int(11) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `location` varchar(128) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `idManufacturer` int(11) NOT NULL,
  `idSampleInProject` int(11) NOT NULL,
  `idCoreInstrument` int(11) NOT NULL,
  `idCoreInstrumentClass` int(11) NOT NULL,
  `enumDeliverType` int(11) NOT NULL,
  `centre` varchar(255) NOT NULL,
  `idCentre` varchar(255) NOT NULL,
  `title` int(64) DEFAULT NULL,
  `innerName` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `spent`
--

DROP TABLE IF EXISTS `spent`;
CREATE TABLE IF NOT EXISTS `spent` (
  `idSpent` int(11) NOT NULL,
  `enumCurrency` int(11) NOT NULL,
  `enumPaymentGoal` int(11) NOT NULL,
  `isCashPayment` int(11) NOT NULL,
  `paymentType` int(11) NOT NULL,
  `idUser` varchar(255) NOT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `date` varchar(255) NOT NULL,
  `idProject` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `whoPayed` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sticky`
--

DROP TABLE IF EXISTS `sticky`;
CREATE TABLE IF NOT EXISTS `sticky` (
  `idSticky` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `isTask` int(11) DEFAULT NULL,
  `idResponsible` int(11) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `calendarDate` varchar(255) DEFAULT NULL,
  `text` varchar(255) NOT NULL,
  `postCommitComment` varchar(255) DEFAULT NULL,
  `idProject` int(11) NOT NULL,
  `showInCalendar` tinyint(1) DEFAULT NULL,
  `template` int(11) DEFAULT NULL,
  `colour` varchar(45) DEFAULT NULL,
  `isDone` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

DROP TABLE IF EXISTS `supplier`;
CREATE TABLE IF NOT EXISTS `supplier` (
  `idSupplier` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `supplier2project`
--

DROP TABLE IF EXISTS `supplier2project`;
CREATE TABLE IF NOT EXISTS `supplier2project` (
  `id` int(11) NOT NULL,
  `idSupplier` int(11) NOT NULL,
  `idProject` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `upload`
--

DROP TABLE IF EXISTS `upload`;
CREATE TABLE IF NOT EXISTS `upload` (
  `idUpload` int(11) NOT NULL,
  `idFolder` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `size` int(11) DEFAULT NULL,
  `idProject` int(11) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `idUser` int(11) NOT NULL,
  `token` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastvisit_at` varchar(128) NOT NULL,
  `superuser` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `personalFooter` varchar(255) DEFAULT NULL,
  `abbr` int(10) DEFAULT NULL,
  `permissions` varchar(255) DEFAULT NULL,
  `group` int(4) NOT NULL DEFAULT '0',
  `idGroup` int(11) DEFAULT NULL,
  `columns` varchar(255) DEFAULT NULL,
  `personalFooter2` varchar(255) DEFAULT NULL,
  `personalFooter3` varchar(255) DEFAULT NULL,
  `serializedPreferences` varchar(255) DEFAULT NULL,
  `serializedPersonalFilter` varchar(255) DEFAULT NULL,
  `serializedPermissions` varchar(255) DEFAULT NULL,
  `serializedColumns` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ActiveRecordLog`
--
ALTER TABLE `ActiveRecordLog`
  ADD PRIMARY KEY (`idActiveRecordLog`);

--
-- Indexes for table `atomicRbac`
--
ALTER TABLE `atomicRbac`
  ADD PRIMARY KEY (`idAtomicRbac`);

--
-- Indexes for table `attachement2logisticfragment`
--
ALTER TABLE `attachement2logisticfragment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attachment`
--
ALTER TABLE `attachment`
  ADD PRIMARY KEY (`idAttachment`);

--
-- Indexes for table `attachment2contest`
--
ALTER TABLE `attachment2contest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attachment2contragent`
--
ALTER TABLE `attachment2contragent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attachment2coreinstrument`
--
ALTER TABLE `attachment2coreinstrument`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attachment2event`
--
ALTER TABLE `attachment2event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attachment2logistic`
--
ALTER TABLE `attachment2logistic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attachment2order`
--
ALTER TABLE `attachment2order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attachment2project`
--
ALTER TABLE `attachment2project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attachment2sample`
--
ALTER TABLE `attachment2sample`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `centre`
--
ALTER TABLE `centre`
  ADD PRIMARY KEY (`idCentre`);

--
-- Indexes for table `centre2manufacturer`
--
ALTER TABLE `centre2manufacturer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `checkpoint`
--
ALTER TABLE `checkpoint`
  ADD PRIMARY KEY (`idCheckpoint`);

--
-- Indexes for table `checkpointGroup`
--
ALTER TABLE `checkpointGroup`
  ADD PRIMARY KEY (`idCheckpointGroup`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`idCity`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`idCompany`);

--
-- Indexes for table `companyclass`
--
ALTER TABLE `companyclass`
  ADD PRIMARY KEY (`idCompanyClass`);

--
-- Indexes for table `contest`
--
ALTER TABLE `contest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contragent`
--
ALTER TABLE `contragent`
  ADD PRIMARY KEY (`idContragent`);

--
-- Indexes for table `coreinstrument`
--
ALTER TABLE `coreinstrument`
  ADD PRIMARY KEY (`idCoreInstrument`);

--
-- Indexes for table `coreinstrumentclass`
--
ALTER TABLE `coreinstrumentclass`
  ADD PRIMARY KEY (`idCoreInstrumentClass`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`idCountry`);

--
-- Indexes for table `custom`
--
ALTER TABLE `custom`
  ADD PRIMARY KEY (`idCustom`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`idCustomer`);

--
-- Indexes for table `customer2project`
--
ALTER TABLE `customer2project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`idEvent`);

--
-- Indexes for table `event2logistic`
--
ALTER TABLE `event2logistic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feature`
--
ALTER TABLE `feature`
  ADD PRIMARY KEY (`idFeature`);

--
-- Indexes for table `featuregroup`
--
ALTER TABLE `featuregroup`
  ADD PRIMARY KEY (`idFeatureGroup`);

--
-- Indexes for table `folder`
--
ALTER TABLE `folder`
  ADD PRIMARY KEY (`idFolder`);

--
-- Indexes for table `group`
--
ALTER TABLE `group`
  ADD PRIMARY KEY (`idGroup`);

--
-- Indexes for table `insuranceCompany`
--
ALTER TABLE `insuranceCompany`
  ADD PRIMARY KEY (`idInsuranceCompany`);

--
-- Indexes for table `logistic`
--
ALTER TABLE `logistic`
  ADD PRIMARY KEY (`idLogistic`);

--
-- Indexes for table `logisticFragment`
--
ALTER TABLE `logisticFragment`
  ADD PRIMARY KEY (`idLogisticFragment`);

--
-- Indexes for table `logisticPosition`
--
ALTER TABLE `logisticPosition`
  ADD PRIMARY KEY (`idLogisticPosition`);

--
-- Indexes for table `managedblock`
--
ALTER TABLE `managedblock`
  ADD PRIMARY KEY (`idManagedBlock`);

--
-- Indexes for table `managedblockfeature`
--
ALTER TABLE `managedblockfeature`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manufacture2coreinstrumentclass`
--
ALTER TABLE `manufacture2coreinstrumentclass`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manufacturer`
--
ALTER TABLE `manufacturer`
  ADD PRIMARY KEY (`idManufacturer`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`idNotice`);

--
-- Indexes for table `notice2link`
--
ALTER TABLE `notice2link`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`idOrder`);

--
-- Indexes for table `orderfeature`
--
ALTER TABLE `orderfeature`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderungroupedfeature`
--
ALTER TABLE `orderungroupedfeature`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`idPermission`);

--
-- Indexes for table `pm`
--
ALTER TABLE `pm`
  ADD PRIMARY KEY (`idPm`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `profiles_fields`
--
ALTER TABLE `profiles_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `varname` (`varname`,`widget`,`visible`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`idProject`);

--
-- Indexes for table `projectWorkflow`
--
ALTER TABLE `projectWorkflow`
  ADD PRIMARY KEY (`idProjectWorkflow`);

--
-- Indexes for table `projectworkflow2projectworkflow`
--
ALTER TABLE `projectworkflow2projectworkflow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`idRegion`);

--
-- Indexes for table `sample`
--
ALTER TABLE `sample`
  ADD PRIMARY KEY (`idSample`);

--
-- Indexes for table `spent`
--
ALTER TABLE `spent`
  ADD PRIMARY KEY (`idSpent`);

--
-- Indexes for table `sticky`
--
ALTER TABLE `sticky`
  ADD PRIMARY KEY (`idSticky`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`idSupplier`);

--
-- Indexes for table `supplier2project`
--
ALTER TABLE `supplier2project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upload`
--
ALTER TABLE `upload`
  ADD PRIMARY KEY (`idUpload`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `status` (`status`),
  ADD KEY `superuser` (`superuser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ActiveRecordLog`
--
ALTER TABLE `ActiveRecordLog`
  MODIFY `idActiveRecordLog` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `atomicRbac`
--
ALTER TABLE `atomicRbac`
  MODIFY `idAtomicRbac` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attachement2logisticfragment`
--
ALTER TABLE `attachement2logisticfragment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attachment`
--
ALTER TABLE `attachment`
  MODIFY `idAttachment` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attachment2contest`
--
ALTER TABLE `attachment2contest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attachment2contragent`
--
ALTER TABLE `attachment2contragent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attachment2coreinstrument`
--
ALTER TABLE `attachment2coreinstrument`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attachment2event`
--
ALTER TABLE `attachment2event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attachment2logistic`
--
ALTER TABLE `attachment2logistic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attachment2order`
--
ALTER TABLE `attachment2order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attachment2project`
--
ALTER TABLE `attachment2project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attachment2sample`
--
ALTER TABLE `attachment2sample`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `centre`
--
ALTER TABLE `centre`
  MODIFY `idCentre` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `centre2manufacturer`
--
ALTER TABLE `centre2manufacturer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `checkpoint`
--
ALTER TABLE `checkpoint`
  MODIFY `idCheckpoint` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `checkpointGroup`
--
ALTER TABLE `checkpointGroup`
  MODIFY `idCheckpointGroup` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `idCity` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `idCompany` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `companyclass`
--
ALTER TABLE `companyclass`
  MODIFY `idCompanyClass` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contest`
--
ALTER TABLE `contest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contragent`
--
ALTER TABLE `contragent`
  MODIFY `idContragent` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `coreinstrument`
--
ALTER TABLE `coreinstrument`
  MODIFY `idCoreInstrument` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `coreinstrumentclass`
--
ALTER TABLE `coreinstrumentclass`
  MODIFY `idCoreInstrumentClass` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `idCountry` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `custom`
--
ALTER TABLE `custom`
  MODIFY `idCustom` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `idCustomer` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer2project`
--
ALTER TABLE `customer2project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `idEvent` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event2logistic`
--
ALTER TABLE `event2logistic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `feature`
--
ALTER TABLE `feature`
  MODIFY `idFeature` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `featuregroup`
--
ALTER TABLE `featuregroup`
  MODIFY `idFeatureGroup` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `folder`
--
ALTER TABLE `folder`
  MODIFY `idFolder` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `group`
--
ALTER TABLE `group`
  MODIFY `idGroup` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `insuranceCompany`
--
ALTER TABLE `insuranceCompany`
  MODIFY `idInsuranceCompany` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `logistic`
--
ALTER TABLE `logistic`
  MODIFY `idLogistic` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `logisticFragment`
--
ALTER TABLE `logisticFragment`
  MODIFY `idLogisticFragment` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `logisticPosition`
--
ALTER TABLE `logisticPosition`
  MODIFY `idLogisticPosition` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `managedblock`
--
ALTER TABLE `managedblock`
  MODIFY `idManagedBlock` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `managedblockfeature`
--
ALTER TABLE `managedblockfeature`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `manufacture2coreinstrumentclass`
--
ALTER TABLE `manufacture2coreinstrumentclass`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `manufacturer`
--
ALTER TABLE `manufacturer`
  MODIFY `idManufacturer` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `idNotice` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notice2link`
--
ALTER TABLE `notice2link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `idOrder` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orderfeature`
--
ALTER TABLE `orderfeature`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orderungroupedfeature`
--
ALTER TABLE `orderungroupedfeature`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `idPermission` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pm`
--
ALTER TABLE `pm`
  MODIFY `idPm` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profiles_fields`
--
ALTER TABLE `profiles_fields`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `idProject` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `projectWorkflow`
--
ALTER TABLE `projectWorkflow`
  MODIFY `idProjectWorkflow` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `projectworkflow2projectworkflow`
--
ALTER TABLE `projectworkflow2projectworkflow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `region`
--
ALTER TABLE `region`
  MODIFY `idRegion` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sample`
--
ALTER TABLE `sample`
  MODIFY `idSample` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `spent`
--
ALTER TABLE `spent`
  MODIFY `idSpent` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sticky`
--
ALTER TABLE `sticky`
  MODIFY `idSticky` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `idSupplier` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `supplier2project`
--
ALTER TABLE `supplier2project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `upload`
--
ALTER TABLE `upload`
  MODIFY `idUpload` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `user_profile_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
SET FOREIGN_KEY_CHECKS=1;