 # -*- coding: utf-8 -*-
import json
import os
import zipfile
import beanstalkc

TEMP_DIR = "./processed/%s"

testing = False
def build_zip(structure):
    filename = TEMP_DIR % structure['filename']
    try:
        os.remove(filename)
    except:
        pass;
    #with zipfile.ZipFile(filename, "w") as zipf:
    zipf=zipfile.ZipFile(filename, "w")
    for path, files in structure['structure'].items():
        for origin, name in files.items():
            zipf.write(origin, "%s/%s" % (path, name))
    zipf.close()
    pass;

if (testing) :
    structure_json = """
        {
            "structure": {
            "images": {
                        "./source/IMAG0627.jpg": "русская_хрень.jpg",
                        "./source/IMAG0637.jpg": "2.jpg"
                      },
            "music": {
                        "./source/sss.mp3": "1.mp3" ,
                        "./source/ssss.mp3": "2.mp3"
                    }},
            "filename":
                "project_ololo2.zip"
        }
    """

    structure = json.loads(structure_json)
    print(structure)
    build_zip(structure)
else:
    print ("Worker started...")
    beanstalk = beanstalkc.Connection(host='localhost', port=11300)
    beanstalk.use('default')
    while (True):
        job = beanstalk.reserve()
        if (job):
            print ("Job recieved...");
            structure = json.loads(job.body)
            job.delete()
            build_zip(structure)
            print ("Job done...");
