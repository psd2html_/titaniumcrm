<?php if(Yii::app()->user->hasFlash('loginMessage')): ?>

<div class="success">
	<?php echo Yii::app()->user->getFlash('loginMessage'); ?>
</div>

<?php endif; ?>

<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Restore");
$this->breadcrumbs=array(
	UserModule::t("Login")
);
?>
            <div id="login-box">
                <fieldset>
                <H2>Titanium CRM</H2>
                
                <?php $e=CHtml::errorSummary($model); 
                if ($e) {echo $e;}
                 ?>
                
                <?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'login-form',
		'enableAjaxValidation'=>false,
		'focus'=>array($model, 'username'),
                )); ?>
                
                <label>Логин:</label>
                <?php echo $form->textField($model,'username',array('size'=>"30",'maxlength'=>"30",'class'=>"form-login")); ?>  
                    
                <label>Пароль:</label>
                    <?php echo $form->passwordField($model,'password',array('size'=>"30",'maxlength'=>"30",'class'=>"form-login")); ?>
                
                
                <?echo $form->checkBox($model,'rememberMe'); ?> Запомнить меня <br/><br/>
                
                <input class="button blue" type="submit" value="Войти"/>
                    
                </div>
               
                
                <?php $this->endWidget(); ?>
                </fieldset>
 

           
<? echo CHtml::link(UserModule::t("Lost Password?"),Yii::app()->getModule('user')->recoveryUrl,array('style'=>'margin-left:25px;'));?><br /><br />
		
	




	
		

