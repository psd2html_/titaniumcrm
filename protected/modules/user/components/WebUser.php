<?php

class WebUser extends CWebUser
{
    public function renewStateForUser($id)
    {

    }

    public function purgeStoredIdGroupForUser($id)
    {
        $name="_user__".$id."_group";

        if (!YII_LOCAL)
         Yii::app()->cache->delete($name);
    }
    public function getIdGroup()
    {
        $name="_user__".$this->id."_group";

        if (!YII_LOCAL) {
            $g = Yii::app()->cache->get($name);
        } else {
            $g = '';
        }


        if (!$g) {
            if (Yii::app()->user->isGuest) {
                $g=0;
            } else {
            $g=Yii::app()->user->user()->idGroup;
            $g=$g?$g:1;
            }

            if (!YII_LOCAL) {
                Yii::app()->cache->set($name, $g);
            }
        }
        return $g;
    }

    public function getRole()
    {
        return $this->getState('__role');
    }
    
    public function getId()
    {
        return $this->getState('__id') ? $this->getState('__id') : 0;
    }

//    protected function beforeLogin($id, $states, $fromCookie)
//    {
//        parent::beforeLogin($id, $states, $fromCookie);
//
//        $model = new UserLoginStats();
//        $model->attributes = array(
//            'user_id' => $id,
//            'ip' => ip2long(Yii::app()->request->getUserHostAddress())
//        );
//        $model->save();
//
//        return true;
//    }

    protected function afterLogin($fromCookie)
	{
        parent::afterLogin($fromCookie);
        $this->updateSession();
	}

    public function updateSession($id=False) {
        $user = Yii::app()->getModule('user')->user($this->id);

        $userAttributes = CMap::mergeArray(array(
                                                'email'=>$user->email,
                                                'username'=>$user->username,
                                                'create_at'=>$user->create_at,
                                                'lastvisit_at'=>$user->lastvisit_at,
                                           ),$user->profile->getAttributes());
        foreach ($userAttributes as $attrName=>$attrValue) {
            $this->setState($attrName,$attrValue);
        }
    }

    public function model($id=0) {
        return Yii::app()->getModule('user')->user($id);
    }

    public function user($id=0) {
        return $this->model($id);
    }

    public function getUserByName($username) {
        return Yii::app()->getModule('user')->getUserByName($username);
    }

    public function getAdmins() {
        return Yii::app()->getModule('user')->getAdmins();
    }

    public function isAdmin() {
        return Yii::app()->getModule('user')->isAdmin();
    }

}