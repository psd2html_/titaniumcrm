<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sdd
 * Date: 12/6/13
 * Time: 1:34 PM
 * To change this template use File | Settings | File Templates.
 */
class CUserPreferences {

    const CURRENT_PREFERENCES_VERSION = 3;
    const DEFAULT_ROW_COUNT_IN_GRIDVIEW = 'drc';
    const DEFAULT_PRESET_COUNT_IN_GRIDVIEW = 'dpc';
    const CSV_EXPORT_SEPARATOR = 'ces';
    const PROJECT_IMPORTANCE_ORDERING = 'pis';

    private static $_settings = null;

    public static function setParam($setting_id)
    {
        $a=self::_getUserSettings();
        return $a[$setting_id];
    }

    /**
     * Safe user param retrieval; In case, if no such set, default is returned;
     * @param $setting_id
     * @return mixed
     */
    public static function getParam($setting_id)
    {
        $a = self::_getUserSettings();
        if (!empty($a))
        {
            if (!array_key_exists($setting_id, $a)) {
                $a[$setting_id]=self::getDefaultValueForParam($setting_id);
                self::_setUserSettings($a);
            }
            return $a[$setting_id];
        }
    }


    private static function _getDefaultUserSettings() {
        return array(
            'v'=>self::CURRENT_PREFERENCES_VERSION, //version of config
            self::DEFAULT_ROW_COUNT_IN_GRIDVIEW=>10, //amount of columns
            self::DEFAULT_PRESET_COUNT_IN_GRIDVIEW=>5, //amount of columns
            self::PROJECT_IMPORTANCE_ORDERING=>1, //amount of columns
            self::CSV_EXPORT_SEPARATOR=>';', //amount of columns
        );
    }
    private  static function _setDefaultUserSettings() {
        $array=self::_getDefaultUserSettings();
        self::_setUserSettings($array);
        return $array;
    }

    /**
     * @param $array
     */
    private static function _setUserSettings($array)
    {
        $user = Yii::app()->user->model();
        $user->serializedPreferences = CJSON::encode($array);
        $user->update(array('serializedPreferences'));

        if (!YII_LOCAL)
            Yii::app()->cache->set("PREFERENCES_USER_" . Yii::app()->user->id, $array, 24 * 60 * 60);

        self::$_settings=$array;
    }

    /**
     * @return array
     */
    private static function _getUserSettings()
    {
        if (self::$_settings===null)
        {
            self::$_settings=self::_retriveUserSettings();
        }
        return self::$_settings;
    }

    /**
     * @return array|mixed|void
     */
    private static function _retriveUserSettings()
    {
        if (!YII_LOCAL)
            $array = Yii::app()->cache->get("PREFERENCES_USER_" . Yii::app()->user->id);

        if ($array === false) {
            $user = Yii::app()->user->model();

            $var = $user->serializedPreferences;
            $array = CJSON::decode($var);

            if (($array === NULL) || (!is_array($array) || (!isset($array['v'])) || ($array['v'] != self::CURRENT_PREFERENCES_VERSION))) {
                $array = self::_setDefaultUserSettings();
            }
            if (!YII_LOCAL)
                Yii::app()->cache->set("PREFERENCES_USER_" . Yii::app()->user->id, $array, 24 * 60 * 60);
        }
        return $array;
    }

    public static function getPreferences()
    {
        return self::_getUserSettings();
    }

    public static function savePreferences($sett)
    {
        self::_setUserSettings($sett);
    }

    /**
     * Returns default value for param
     * @param $setting_id
     * @throws CHttpException
     */
    public static function getDefaultValueForParam($setting_id)
    {
        $defaultSettings=self::_getDefaultUserSettings();
        if (array_key_exists($setting_id,$defaultSettings)) {
            return $defaultSettings[$setting_id];
        } else {
            throw new CHttpException(500,"No default value for the parameter $setting_id");
        }
    }

}