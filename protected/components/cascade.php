<?php
/**
 * temporary class for presenting tree-like structure
 * @property string $title Description
 * @property f_block[] $blocks Description
 * @property Array[][] $blocksDoccy Hashmap
 */
class f_group
{
    public $title;
    public $blocks; 
    public $blocksDoccy; 
}

class f_block
{
    public $title;
    public $blocks; 
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
