<?php

class Caching {

    public static function flush()
    {
        Yii::app()->cache->flush();
    }

    public static function updateScheme() {
        Yii::app()->setGlobalState('schemeDate', date(DATE_RFC822));
    }

    public static function getListdataMarks() {
        $u = 'listdataMarks';

        if (!YII_LOCAL)
            $d = Yii::app()->cache->get($u);

        if ($d === false) {
            $d = CHtml::listData(Mark::model()->findAll('countAutos>0'), 'idMark', 'name');
            $d[''] = 'Любая марка';

            if (!YII_LOCAL)
                Yii::app()->cache->set($u, $d, 86000, new CGlobalStateCacheDependency('schemeDate'));
        }
        return $d;
    }

    public static function getImagesMarks() {
        $u = 'imgMarks';

        if (!YII_LOCAL)
            $d = Yii::app()->cache->get($u);

        $d=false;
        if ($d === false) {
        
            $mdls = Mark::model()->findAll('imgfile IS NOT NULL AND countAutos>0 ');
            //$mdls = Mark::model()->findAll(array('condition'=>'imgfile IS NOT NULL','order'=>'countAutos DESC'));
            $d = array();
            if ($mdls) {
                foreach ($mdls as $m) {
                    $v['url'] = $m->createFrontendUrl();
                    $v['name'] = $m->name;
                    $v['img'] = $m->imgfile;
                    $d[] = $v;
                }
            }

            if (!YII_LOCAL)
                Yii::app()->cache->set($u, $d, 86000, new CGlobalStateCacheDependency('schemeDate'));
        }
        return $d;
    }

    public static function getListdataTypes() {
        $u = 'listdataTypes';

        if (!YII_LOCAL)
            $d = Yii::app()->cache->get($u);

        if ($d === false) {
            $d = CHtml::listData(Type::model()->findAll(), 'idType', 'name');
            if (!YII_LOCAL)
                Yii::app()->cache->set($u, $d,86000, new CGlobalStateCacheDependency('schemeDate'));
        }
        return $d;
    }

}

?>
