<?php
/**
 * Class AngularJsDispatcher
 * provides some php-bootstrap for AngularJsDependencies;
 * namely,
 *  - dependency injection for ng-app
 */
class AngularJsDispatcher
{
    public $appName = "TIApp";
    /**
     * @var CController $_controller
     */

    private $_controller;
    private $_config;
    private $_dependencies = array();

    public function AngularJsDispatcher($p = false)
    {
        $this->_controller = $p;
        $this->_config = $this->_getConfig();
    }

    private function _getConfig()
    {
        return array(
            //'module'=>array('css'=>array(),'js'=>array())
            'ui.bootstrap' => array(
                'css' => array(),
                'js' => array('/js/bootstrap/ui-bootstrap-custom-0.10.0.js', '/js/bootstrap/ui-bootstrap-custom-tpls-0.10.0.js')
            ),'angularBootstrapNavTree' => array(
                'css' => array('/css/anb_tree.css'),
                'js' => array('/js/angular/angular-tree.js')
            ),
            'xeditable' => array(
                'css' => array('/js/editable/xeditable.css'),
                'js' => array('/js/editable/xeditable.js')
            ),
            'dropzone' => array(
                'css' => array(),
                'js' => array('/js/angular/dropzone.js')
            ),
            'textAngular' => array(
                'css' => array(),
                'js' => array('/js/textAngular/textAngular.min.js')
            ),
            'ngSanitize' => array(
                'css' => array(),
                'js' => array('/js/angular/angular-sanitize.min.js')
            )
        );
    }

    public function injectDependency($dependency)
    {
        if (is_array($dependency)) {
            foreach ($dependency as $dep) {
                $this->_injectSingleDependency($dep);
            }
        } else {
            $this->_injectSingleDependency($dependency);
        }
    }

    /**
     * @param $dependency
     */
    public function _injectSingleDependency($dependency)
    {
        if (!in_array($dependency, $this->_dependencies)) {
            $this->_dependencies[] = $dependency;
            if ($this->_controller !== false && key_exists($dependency, $this->_config)) {

                foreach ($this->_config[$dependency]['css'] as $cssFile) {
                    Yii::app()->clientScript->registerCssFile(Yii::app()->getBaseUrl() . $cssFile);
                }
                foreach ($this->_config[$dependency]['js'] as $jsFile) {
                    Yii::app()->clientScript->registerScriptFile(Yii::app()->getBaseUrl() . $jsFile, CClientScript::POS_BEGIN);
                }
            }
        }
    }

    public function getNgAppInitCode($defaultDependencies=false)
    {
        if ($defaultDependencies!==false) {
            $this->injectDependency($defaultDependencies);
        }
        $d = CJavaScript::encode($this->_dependencies);
        return "this.{$this->appName}" .
        " = angular.module('{$this->appName}', $d);";

    }

}