<?php
/**
 * new properties:
 * 1. add to _formBuilder
 * 2. add public var
 * 3. add rule
 * 4. add label
 * Class CUserPreferencesForm
 */

class CUserPreferencesForm extends CFormModel
{
    const DROPDOWN_LIST = 0;
    const TEXTFIELD = 1;
    const TEXTAREA = 2;
    const NO_GROUP = '$$NO_GROUP';

    public $sortByImportance;
    public $rowCount;
    public $presetCount;
    public $csvSeparator;

    public function fillSettings()
    {

        $groups = self::_formBuilderConfig();

        foreach ($groups as $group) {
            foreach ($group as $field) {
                $token = $field['name'];
                $this->$token = CUserPreferences::getParam($field['associatedUserPreferenceId']);

            }
        }

        return true;
    }

    private function _formBuilderConfig()
    {
        return array(
            "Настройки отображения таблиц" => array(
                array(
                    'columns' => 2,
                    'name' => 'rowCount',
                    'type' => self::DROPDOWN_LIST,
                    'list' => array(5 => '5', 10 => '10', 25 => '25', 50 => '50', 100 => '100'),
                    'associatedUserPreferenceId' => CUserPreferences::DEFAULT_ROW_COUNT_IN_GRIDVIEW
                ),
                array(
                    'columns' => 2,
                    'name' => 'sortByImportance',
                    'type' => self::DROPDOWN_LIST,
                    'list' => array('1'=>'Да','0'=>'Нет'),
                    'associatedUserPreferenceId' => CUserPreferences::PROJECT_IMPORTANCE_ORDERING
                ),
                array(
                    'columns' => 2,
                    'name' => 'presetCount',
                    'type' => self::DROPDOWN_LIST,
                    'list' => array(5 => '5', 6 => '6', 7 => '7', 8 => '8', 9 => '9', 10 => '10',11=>'11',12=>'12',13=>'13',14=>'14',15=>'15'),
                    'associatedUserPreferenceId' => CUserPreferences::DEFAULT_PRESET_COUNT_IN_GRIDVIEW
                ),
                array(
                    'columns' => 2,
                    'name' => 'csvSeparator',
                    'type' => self::DROPDOWN_LIST,
                    'list' => array(',' => 'запятая (,)', ';' => 'точка с запятой (;)'),
                    'associatedUserPreferenceId' => CUserPreferences::CSV_EXPORT_SEPARATOR
                )
            ),
        );
    }

    public function applySettings()
    {
        if (!$this->validate()) return false;
        $groups = self::_formBuilderConfig();
       $sett = CUserPreferences::getPreferences();

        foreach ($groups as $group) {
            foreach ($group as $field) {
                $token = $field['name'];

                if ($this->$token !== null) {
                    $sett[$field['associatedUserPreferenceId']] = $this->$token;
                }
            }
        }

        CUserPreferences::savePreferences($sett);
        return true;
    }

    /**
     * @param CController $controller
     */
    public function renderForm($controller, $id = null)
    {
        $groups = self::_formBuilderConfig();

        if ($id == null) {
            $id = strtolower(get_class($this)) . '-form';
        }

        $form = $controller->beginWidget('ext.foundation.widgets.FounActiveForm', array(
            'id' => $id,
            'enableAjaxValidation' => false,
            'enableClientValidation' => true));

        foreach ($groups as $name => $group) {
            echo('<fieldset>');
            if ($name !== self::NO_GROUP) {
                echo("<legend>$name</legend>");
            }
            $row = array();
            $rowCount = 0;
            $rowMax = 12;
            foreach ($group as $field) {
                $candidate = $this->renderField($form, $field);

                $cost = isset($field['columns']) ? $field['columns'] : ($rowMax - $rowCount);
                if ($rowCount + $cost < $rowMax) {
                    $row[] = array($cost, $candidate);
                } elseif ($rowCount + $cost == $rowMax) {
                    $row[] = array($cost, $candidate);
                    self::renderRow($row);
                    $row = array();
                } else {
                    self::renderRow($row);
                    $row = array(array($cost, $candidate));
                }

            }
            self::renderRow($row);
            echo('</fieldset>');
        }

        echo CHtml::submitButton(("Сохранить"), array('class' => 'button'));
        $controller->endWidget();
    }

    /**
     * @param FounActiveForm $form
     * @param array $field
     */
    private function renderField($form, $field)
    {
        $model = $this;
        $htmlOptions = isset($field['htmlOptions']) ? $field['htmlOptions'] : array();

        switch ($field['type']) {
            case self::DROPDOWN_LIST:
                return $form->dropDownListRow($model, $field['name'], $field['list'], $htmlOptions) . '<br/><br/>';
                break;
            case self::TEXTAREA:
                return $form->textAreaRow($model, $field['name'], $htmlOptions);
                break;
            case self::TEXTFIELD:
                return $form->textFieldRow($model, $field['name'], $htmlOptions);
                break;
        }
    }

    private static function renderRow($rows)
    {
        $foun_semantics = array(
            0 => 'lol',
            1 => 'one columns',
            2 => 'two columns',
            3 => 'three columns',
            4 => 'four columns',
            5 => 'five columns',
            6 => 'six columns',
            7 => 'seven columns',
            8 => 'eight columns',
            9 => 'nine columns',
            10 => 'ten columns',
            11 => 'eleven columns',
            12 => 'twelve columns',
        );

        if (is_array($rows) && count($rows) > 0) {
            $sum = 0;
            echo('<div class="row">');
            foreach ($rows as $row) {
                echo('<div class="' . $foun_semantics[$row[0]] . '">');
                echo($row[1]);
                echo('</div>');
                $sum += $row[0];
            }
            if ($sum < 12) {
                echo('<div class="' . $foun_semantics[12 - $sum] . '"></div>');
            }
            echo('</div>');
        }

    }

    public function rules()
    {
        return array(
            array('rowCount,presetCount,csvSeparator,sortByImportance', 'safe')
        );
    }

    public function attributeLabels()
    {
        return array(
            "rowCount" => "Число строк по-умолчанию",
            "csvSeparator" => "Разделитель данных в выгружаемом CSV",
            "sortByImportance" => "Проекты сортируются по важности",
            "presetCount" => "Число пресетов, отображаемых в шапке");
    }

}