<?php
class ActiveRecordLoggable extends CActiveRecordBehavior
{
    const ACTION_CHANGE = 1;
    const ACTION_CREATE = 0;
    const ACTION_DELETE = 2;
    public $afterSaveInProgress = false;
    public $skipAttributes = array();
    public $hostAfterSaveAction = null;
    public $shouldBubbleToMaster = false;
    public $masterRelation = 'project';
    public $logCreation = true;
    private $_oldattributes = array();

    public function afterSave($event)
    {
        if ($this->afterSaveInProgress) return;


        $host = $this->_getHost();
        if ($host->afterSaveInProgress) return;

        if (!Yii::app()->params['enableLog']) return;

        $model = $this->getOwner();
        $host = $this->_getHost();

        /* @var ActiveRecord $model */
        $log = null;
        if (!$model->isNewRecord) {
            $log = $this->_processModelAttributesUpdate($model, $host);
        } else {
            $log = $this->_processModelCreation($model, $host);
        }

        $this->invokeAfterSaveHook($log);
        // if some additional save would be performed, we should have actual diff
        $this->setOldAttributes($model->getAttributes());
    }

    private function _getHost()
    {
        $model = $this->getOwner();
        /* @var ActiveRecord $model */
        if (!$this->shouldBubbleToMaster) return $model;
        return $model->getRelated($this->masterRelation);

    }

    /**
     * @param ActiveRecord $model
     * @param ActiveRecord $host
     */
    private function _processModelAttributesUpdate($model, $host)
    {
        $log = null;
        // new attributes
        $newattributes = $model->getAttributes();
        $oldattributes = $this->getOldAttributes();

        // compare old and new
        foreach ($newattributes as $name => $value) {
            if (in_array($name, $this->skipAttributes)) {
                continue;
            }
            if (!empty($oldattributes)) {
                $old = $oldattributes[$name];
            } else {
                $old = '';
            }

            if ($value != $old) {
                $log = $this->logEvent(
                    ' Изменено: "' . $model->getAttributeLabel($name) . '" в '
                    . Yii::t('app', get_class($model))
                    . '[' . $model->readableName . '].',
                    self::ACTION_CHANGE, $host, $name, Yii::app()->user->id,
                    $model, $value);
            }
        }
        return $log;
    }

    public function getOldAttributes()
    {
        return $this->_oldattributes;
    }

    /**
     * logs event
     * @param string $description
     * @param string $action
     * @param ActiveRecord $model
     * @param string $field
     * @param id $userId optional
     */

    public function logEvent($description, $action, $model, $field, $userId = -1, $slaveModel = null, $newValue = null, $invokeAfterSave = false)
    {
        if ($invokeAfterSave) {
            $host = $this->_getHost();
            if ($host->afterSaveInProgress) return;
        }
        if ($userId == -1) {
            $userId = Yii::app()->user->id;
        }
        $log = new ActiveRecordLog();
        $log->description = $description;
        $log->action = $action;
        $log->model = get_class($model);
        $log->idModel = $model->getPrimaryKey();

        if ($slaveModel) {
            $log->slaveModel = get_class($slaveModel);
            $log->slaveModelId = $model->getPrimaryKey($slaveModel);
        }
        if ($newValue) {
            $log->newValue = $newValue;
        }

        $log->field = $field;
        $log->creationDate = new CDbExpression('NOW()');
        $log->idUser = $userId;
        if ($log->save()) {

            if ($invokeAfterSave) {
                $this->invokeAfterSaveHook($log);
            }
        }
        return $log;
    }

    /**
     * @param $log
     */
    public function invokeAfterSaveHook($log)
    {
        if ($log && $this->hostAfterSaveAction) {
            $host = $this->_getHost();

            $host->afterSaveInProgress=true;
            call_user_func(array($host, $this->hostAfterSaveAction), $log);
            $host->afterSaveInProgress=false;
        }
    }

    /**
     * @param $model
     * @param $host
     */
    private function _processModelCreation($model, $host)
    {
        $log = null;
        if ($this->logCreation) {
            $log = $this->logEvent('Создан ' . Yii::t('app', get_class($model))
            . '[' . $model->readableName . '].', self::ACTION_CREATE, $host, '');
        }
        return $log;
    }

    public function setOldAttributes($value)
    {
        $this->_oldattributes = $value;
    }

    public function  beforeSave($event)
    {
        $model = $this->getOwner();
        $host = $this->_getHost();
        // -------------- relations ------------------------
        foreach ($model->relations() as $key => $value) {
            switch ($value[0]) {
                case CActiveRecord::BELONGS_TO:

                    break;
                case CActiveRecord::MANY_MANY:
                    $newArr = $model->$key;
                    if (($newArr) && (is_array($newArr)) && (is_numeric($newArr[0]))) {

                        if (preg_match('/^(.+)\((.+)\s*,\s*(.+)\)$/s', $value[2], $pocks)) {
                            $oldArr = Yii::app()->db->createCommand("SELECT $pocks[3] FROM $pocks[1] WHERE $pocks[2]=" . $model->getPrimaryKey())
                                ->queryColumn();
                            if (array_diff($newArr, $oldArr) || array_diff($oldArr, $newArr)) {
                                $r = $value[1];
                                /* @var ActiveRecord $r */

                                $joined = $r::model()->findAllByPk($newArr);
                                $joined = array_map(function ($o) {
                                    return $o->readableName;
                                }, $joined);
                                $joined = implode(', ', $joined);

                                $this->logEvent('Изменено: "' . $model->getAttributeLabel($key) . '" в '
                                    . Yii::t('app', get_class($model))
                                    . '[' . $model->readableName . '].',
                                    self::ACTION_CHANGE, $host, $key, Yii::app()->user->id, $model, $joined);
                            }
                        }

                    }
                    break;
            }

        }

    }

    public function afterDelete($event)
    {
        if (!Yii::app()->params['enableLog']) return;

        $model = $this->getOwner();
        $host = $this->_getHost();

        $this->logEvent(Yii::app()->user->name
        . ' удалил ' . Yii::t('app', get_class($model))
        . '[' . $model->readableName . '].', self::ACTION_DELETE, $host, '');
    }

    public function afterFind($event)
    {
        $this->setOldAttributes($this->Owner->getAttributes());
    }
}