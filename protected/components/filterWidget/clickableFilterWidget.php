<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sdd
 * Date: 9/22/13
 * Time: 11:44 PM
 * To change this template use File | Settings | File Templates.
 */

class clickableFilterWidget extends gridViewFilterWidgetBase
{

    public function getInitJS()
    {
        return "window.mGridView.clickableFilter.init(\"".$this->_gridview->id."\"); ";
    }

    public function injectIntoFilterColumn(&$column)
    {
        $n=$this->getFilterName($column);

        // warp actual value in <a>
        if (isset($column['value'])) {
            $column['value'] = '"<a href=\"#\" class=\"mgvClF\" g=\"'.$n.'\">".(' . $column['value'] . ')."</a>"';
        }
        parent::injectIntoFilterColumn($column);
    }
}