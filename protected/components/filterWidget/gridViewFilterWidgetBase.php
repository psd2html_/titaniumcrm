<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sdd
 * Date: 9/22/13
 * Time: 11:40 PM
 * To change this template use File | Settings | File Templates.
 */

abstract class gridViewFilterWidgetBase
{
    protected $_options;
    /**
     * @var MGridView
     */
    protected $_gridview;

    abstract public function getInitJS();

    public function getFilterName(&$column) {
        return get_class($this->_gridview->filter).'['.$column['name'].']';
    }

    public function __construct(&$column,&$grid) {
        $this->_gridview=$grid;
        $this->injectIntoFilterColumn($column);
    }

    public function injectIntoFilterColumn(&$column)
    {
        if (isset($column['filterWidget'])) {
            unset($column['filterWidget']);
        }
        if (isset($column['filterWidgetOptions'])) {
            $this->_options = $column['filterWidgetOptions'];
            unset($column['filterWidgetOptions']);
        }
    }

}