<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sdd
 * Date: 9/22/13
 * Time: 11:44 PM
 * To change this template use File | Settings | File Templates.
 */

class clickableKeyValueFilterWidget extends gridViewFilterWidgetBase
{

    public function getInitJS()
    {
        return "window.mGridView.clickableKeyValueFilter.init(\"".$this->_gridview->id."\"); ";
    }

    public function injectIntoFilterColumn(&$column)
    {
        $n=$this->getFilterName($column);
        $column['type']='raw';
        // warp actual value in <a>
        if (isset($column['value'])) {
            if (is_array($column['value'])) {
                $key=key($column['value']);
                $value=reset($column['value']);
                $column['value'] = '"<a href=\"#\" class=\"mgvClKVF\" g=\"'.$n.'\" v=\"".('.$key.')."\" >".(' . $value . ')."</a>"';
            } else {
                throw new Exception("value should be key-value pair");
            }
        }
        parent::injectIntoFilterColumn($column);
    }
}