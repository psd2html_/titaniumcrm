<?php
//$jui=Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('web.js.source.jui').'\\js\\jquery-ui.min.js');
Yii::app()->clientScript->registerCoreScript('jquery.ui');
$tag_it=Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.components.widgets.tag').'/tag-it.js');
$tag_it_css=Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.components.widgets.tag').'/tag-it.css');
$cs=Yii::app()->clientScript;
$cs->registerScriptFile($tag_it);
$cs->registerCssFile($tag_it_css);

$cs->registerScript($id,'
    $(".'.$id.'").tagit({
        tags: '.$tags.',
        url: "'.$url.'",
		urlAjax: "'.$urlAjax.'",
		needAjax: "'.$needAjax.'",
		formId: "'.$formId.'"
    });
', CClientScript::POS_READY);

?>

<label for="<?php echo CHtml::encode($id);?>">Tags</label>
<ul class="<?php echo CHtml::encode($id);?>">
    <li class="tagit-new">
        <input class="tagit-input" type="text" />
    </li>
</ul>