<?php
/**
 * Searchable fields on steriods.
 * -----------------------------
 *
 * Usecases: for the problems, involved custom logic for searching particular models
 *
 * Class ActiveRecord
 */
class ActiveRecord extends SearchableActiveRecord implements ISelfManagedCrudUrl
{

    public function getReadableName()
    {
        if (($this->hasAttribute("name")) || method_exists($this, "getName")) {
            return $this->name;
        } else {
            return $this->getPrimaryKey();
        }
    }

    public function getFullNameUrl()
    {
        if (($this->hasAttribute("fullName")) || method_exists($this, "getFullName")) {
            return CHtml::link($this->fullName . ' ' . CHtml::image('/images/redirect.png'), $this->getViewUrl(), array('target' => '_blank'));
        } else {
            return $this->getNameUrl();
        }
    }

    public function getViewUrl($params = false)
    {
        if ($params == false) $params = array();
        $controller = get_class($this);
        $controller[0] = strtolower($controller[0]);

        $id = $this->getPrimaryKey();
        $params['id'] = $id;
        switch ($controller) {
            case "project":
            case "logistic":
            case "contest":
                $params['id'] = $id + 1000;
                return Yii::app()->urlManager->createUrl('project/view/', $params);
            case "customer":
                return Yii::app()->urlManager->createUrl('customer/view/', $params);
            case "company":
                return Yii::app()->urlManager->createUrl('company/view/', $params);
            default:
                return Yii::app()->urlManager->createUrl($controller . '/update/', $params);
        }
    }

    public function getNameUrl()
    {
        if (($this->hasAttribute("name")) || method_exists($this, "getName")) {
            return CHtml::link($this->name . ' ' . CHtml::image('/images/redirect.png', ''), $this->getViewUrl(), array('target' => '_blank'));
        } else {
            return CHtml::link(get_class($this), $this->getViewUrl());
        }
    }

    public function getNameUrlNoArrow()
    {
        if (($this->hasAttribute("name")) || method_exists($this, "getName")) {
            return CHtml::link($this->name, $this->getViewUrl(), array('target' => '_blank'));
        } else {
            return CHtml::link(get_class($this), $this->getViewUrl());
        }
    }

    public function getEditUrl()
    {
        // TODO: Eliminate this --> join to getUpdateUrl
        return $this->getUpdateUrl(array());
    }

    public function getUpdateUrl($params = false)
    {
        if ($params == false) $params = array();
        $controller = get_class($this);
        $controller[0] = strtolower($controller[0]);

        $id = $this->getPrimaryKey();
        $params['id'] = $id;
        switch ($controller) {
            case "project":
            case "logistic":
            case "contest":
                $params['id'] = $id + 1000;
                return Yii::app()->urlManager->createUrl('project/update/', $params);
            case "customer":
                return Yii::app()->urlManager->createUrl('customer/update/', $params);
            case "company":
                return Yii::app()->urlManager->createUrl('company/update/', $params);
            default:
                return Yii::app()->urlManager->createUrl($controller . '/update/', $params);
        }
    }

    /**
     * @return string
     */
    public function getDeleteUrl($params = false)
    {
        // TODO: Implement getDeleteUrl() method.
    }

    /**
     * @return string
     */
    public function getAdminUrl($params = false)
    {
        // TODO: Implement getAdminUrl() method.
    }
}