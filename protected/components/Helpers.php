<?php

class Helpers
{
    static public function include_all_files($root)
    {

        $d = new RecursiveDirectoryIterator(Yii::app()->getBasePath() . '/vendors/' . $root);
        foreach (new RecursiveIteratorIterator($d) as $file => $f) {
            $ext = pathinfo($f, PATHINFO_EXTENSION);
            if ($ext == 'php' || $ext == 'inc')
                require_once($file); // or require(), require_once(), include_once()
        }
    }
    static public function getYesNoImage($val)
    {
        if ($val)
            return CHtml::image(Yii::app()->getBaseUrl() . "/images/yes_icon.gif");
        else
            return CHtml::image(Yii::app()->getBaseUrl() . "/images/no_icon.png");;
    }

    static public function more($input, $size = 50, $stripTags = false)
    {

        if ($stripTags) {
            $input = strip_tags($input);
        }
        if (mb_strlen($input) > $size) {
            return (mb_substr($input, 0, $size)) . '...';
        }
        return $input;
    }

    static public function formatPrice($val)
    {
        if ($val === null) return null;
        if (!is_numeric($val)) return 0;
        return number_format($val, 2, '.', ' ');
    }

    static public function makeLinkForCity($var)
    {
        return CHtml::link($var . ' ' . CHtml::image('/images/redirect.png'), Yii::app()->createUrl("company/index", array("Company[city]" => $var)));
    }

    static public function checkDateInRange($startUnix, $endUnix, $dateArbitrary)
    {
        $user_ts = strtotime($dateArbitrary);
        return (($user_ts >= $startUnix) && ($user_ts <= $endUnix));
    }

    static public function getProcessedText($in)
    {
        $out = CHtml::encode($in);

        $out = preg_replace('/(http|https)\:\/\/[а-яА-Яa-zA-Z0-9\-\.]+\.[a-zA-Zа-яА-Я]{2,3}([-_%\w\/\.])+(?:\b)/', '<noindex><a href="$0" rel="nofollow">$0</a></noindex>', $out);

        $out = preg_replace('/^((?:&gt;.+[\n\r]+)+)(?!&gt)/m', "<span class='comment-quote'>$1</span>", $out);
        $out = preg_replace("/\*\*(.*?)\*\*/", '<b>$1</b>', $out);
        $out = preg_replace("/\*(.*?)\*/", '<i>$1</i>', $out);

        $out = preg_replace("/([\n\r]+)/", "<br/>", $out);
        return $out;
    }

    static function wordWrap($in)
    {
        $words = explode(' ', $in);
        $counter = 0;
        $out = array();
        $buff = array();
        foreach ($words as $word) {
            $counter += mb_strlen($word);
            $buff[] = $word;
            if ($counter > 15) {
                $out[] = implode(' ', $buff);
                $counter = 0;
                $buff = array();
            }
        }
        if (count($buff) > 0) {
            $out[] = implode(' ', $buff);
        }
        return implode('\n', $out);
    }

    static function prettyDate($date)
    {
        $time = strtotime($date);
        $now = time();
        $ago = $now - $time;
        if ($ago < 60) {
            $when = round($ago);
            $s = ($when == 1) ? "second" : "seconds";
            return "$when $s ago";
        } elseif ($ago < 3600) {
            $when = round($ago / 60);
            $m = ($when == 1) ? "minute" : "minutes";
            return "$when $m ago";
        } elseif ($ago >= 3600 && $ago < 86400) {
            $when = round($ago / 60 / 60);
            $h = ($when == 1) ? "hour" : "hours";
            return "$when $h ago";
        } elseif ($ago >= 86400 && $ago < 2629743.83) {
            $when = round($ago / 60 / 60 / 24);
            $d = ($when == 1) ? "day" : "days";
            return "$when $d ago";
        } elseif ($ago >= 2629743.83 && $ago < 31556926) {
            $when = round($ago / 60 / 60 / 24 / 30.4375);
            $m = ($when == 1) ? "month" : "months";
            return "$when $m ago";
        } else {
            $when = round($ago / 60 / 60 / 24 / 365);
            $y = ($when == 1) ? "year" : "years";
            return "$when $y ago";
        }
    }

    // does the first string start with the second?

    public static function trim($str)
    {
        return preg_replace("/(^[\s\n\r]+)|([\s\n\r]+$)/us", "", $str);
    }

    public static function getUserIcon()
    {
        return CHtml::image(Yii::app()->getBaseUrl() . '/images/user.png', 'Загрузка');
    }

    public static function getSpinnerGif()
    {
        return CHtml::image(Yii::app()->getBaseUrl() . '/images/loading.gif', 'Загрузка');
    }

    public static function getSemaphoreImage($bool)
    {
        return $bool ? Yii::app()->getBaseUrl() . "/images/yes_icon.gif" : Yii::app()->getBaseUrl() . "/images/no_icon.png";
    }

    public static function getPencil($promt, $route, $params, $p = false)
    {
        if ($promt) {
            $promt = '&nbsp;' . $promt;
        }

        if ($p) {
            $params['returnUrl'] = Yii::app()->request->getRequestUri();
        }
        return CHtml::link(
            CHtml::image(Yii::app()->getBaseUrl() . '/images/pencil.png', 'Загрузка') . $promt, Yii::app()->createUrl($route, $params), $p ? (array('class' => 'foundationModalTrigger')) : ''
        );
    }

    public static function makeRandomString($bits = 256)
    {
        $bytes = ceil($bits / 8);
        $return = '';
        for ($i = 0; $i < $bytes; $i++) {
            $return .= chr(mt_rand(0, 255));
        }
        return $return;
    }

    public static function excerpt($text, $words = 100, $end = '...', $limit = 300)
    {
        $text = strip_tags($text);
        // split the string by spaces into an array
        $split = explode(' ', $text);


        if (count($split) > $words) {
            // rebuild the excerpt back into a string
            $text = join(' ', array_slice($split, 0, $words));
        }


        // append the ending, limit it, and return
        //return substr($text,0,$limit-  strlen($end)).$end;
        return $text . $end;
    }

    static function russianDate($d, $includeTime = false)
    {
        $timestamp = strtotime($d);
        $date = explode(".", date("d.m.Y", $timestamp));
        switch ($date[1]) {
            case 1:
                $m = 'января';
                break;
            case 2:
                $m = 'февраля';
                break;
            case 3:
                $m = 'марта';
                break;
            case 4:
                $m = 'апреля';
                break;
            case 5:
                $m = 'мая';
                break;
            case 6:
                $m = 'июня';
                break;
            case 7:
                $m = 'июля';
                break;
            case 8:
                $m = 'августа';
                break;
            case 9:
                $m = 'сентября';
                break;
            case 10:
                $m = 'октября';
                break;
            case 11:
                $m = 'ноября';
                break;
            case 12:
                $m = 'декабря';
                break;
        }
        $out = $date[0] . '&nbsp;' . $m . '&nbsp;' . $date[2];
        if ($includeTime) {
            $out = $out . ' в ' . date("H:m", $timestamp);
        }
        return $out;
    }

    public static function normalizeArray(&$in, $num)
    {

        $headersCount = count($in);

        if ($headersCount > $num) {
            for ($i = 0; $i < -($num - $headersCount); $i++) {
                array_pop($in);
            }
        }

        if ($headersCount < $num) {
            for ($i = 0; $i < ($num - $headersCount); $i++) {
                $in[] = '';
            }
        }
    }

    public static function fetchPage($url, $proxy = false, $v = -1)
    {
        echo("Fetching $url <br/>");
        // return('!!!');
        $ch = curl_init(); /// open a cURL instance

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // tell cURL to return the data

        curl_setopt($ch, CURLOPT_URL, $url); /// set the URL to download
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:7.0.1) Gecko/20100101 Firefox/7.0.1');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); /// Follow any redirects

        curl_setopt($ch, CURLOPT_BINARYTRANSFER, false); /// tells cURL if the data is binary data or not
        if ($proxy) {
//http://spys.ru/proxys/RU/ 2,3,15,20
            $p = '5 217.76.35.238:3128	!!!!!!!!!!!!
6 188.143.232.239:80	!!!!!!!!!!!!!!
18 78.138.172.145:80	HTTP	!!!!!!!!!!!!!!!!!!!!!!
23 84.22.159.146:3128	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
11 178.210.42.30:80   !@!!!!!!!!!!!!!!!!!!
1 79.120.177.37:8080	HTTP	NOA	6.783	HU	remote.hotelkapitany.hu	29:06:12-17:54
2 79.165.191.114:3128	HTTP	NOA	4.324	RU Moscow	host-79-165-191-114.qwerty.ru	29:06:12-17:54
3 80.69.243.68:3128	HTTP	NOA	1.191	IR Rasan	80-69-243-68.pasargadnet.ir	29:06:12-17:53
4 80.74.160.66:6666	HTTP	NOA	0.36	RS Novi Sad	front.neobee.net';

            $regex = '/^[0-9]+\s([0-9\.]+):([0-9]+)\t/smuU';

            preg_match_all($regex, $p, $pl, PREG_SET_ORDER);
            //	var_dump($out);
            if ($v < 0)
                $v = rand(0, count($pl) - 1);
            //$f=explode(':',$pl[$v]);
            $proxy_ip = $pl[$v][1]; //proxy IP here
            $proxy_port = $pl[$v][2]; //proxy port from your proxy list
            echo("Using proxy $proxy_ip:$proxy_port <br/>");
            //Yii::app()->end();

            curl_setopt($ch, CURLOPT_PROXYPORT, $proxy_port);
            curl_setopt($ch, CURLOPT_PROXYTYPE, 'HTTP');
            curl_setopt($ch, CURLOPT_PROXY, $proxy_ip);
        }
        $html = curl_exec($ch); // pulls the webpage from the internet
        $phail = false;
        if (curl_errno($ch)) {
            echo 'Curl error: ' . curl_error($ch);
            $phail = !false;
        }

        curl_close($ch); /// closes the connection
        if ($phail) {
            Yii::app()->end();
        }
        return ($html);
    }

    public static function headerJson()
    {
        header('Content-type: application/json');
    }

    public static function headerUTF8()
    {
        header('content-type: text/html; charset=utf-8');
    }

    public static function cp1251toUTF8($raw)
    {
        return (iconv("cp1251", "UTF-8", $raw)); //$this->win2utf($raw);
    }

    public static function dateToString($date)
    {
        $MonthNames = array('', 'января', 'февраля', 'марта',
            'апреля', 'мая', 'июня', 'июля', 'августа',
            'сентября', 'октября', 'ноября', 'декабря');
        $NamesOfThousand0 = array('', 'тысячного', 'двухтысячного');
        $NamesOfThousand = array('', 'одна тысяча', 'две тысячи');
        $NamesOfHandreds0 = array('', 'сотого', 'двухсотого',
            'трехсотого', 'четырехсотого', 'пятисотого', 'шестисотого',
            'семисотого', 'восьмисотого', 'девятисотого');
        $NamesOfHandreds = array('', 'сто', 'двести', 'триста',
            'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');
        $NamesOfTens00 = array('', 'десятое', 'двадцатое', 'тридцатое');
        $NamesOfTens0 = array('', 'десятого', 'двадцатого', 'тридцатого',
            'сорокового', 'пятидесятого', 'шестидесятого', 'семидесятого',
            'восьмидесятого', 'девяностого');
        $NamesOfTens = array('', '', 'двадцать', 'тридцать', 'сорок', 'пятьдесят',
            'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
        $NamesOfFistTen0 = array('', 'одинадцатое', 'двенадцатое',
            'тринадцатое', 'четырнадцатое', 'пятнадцатое', 'шестнадцатое',
            'семнадцатое', 'восемьнадцатое', 'девятнадцатое');
        $NamesOfFistTen = array('', 'одинадцатого', 'двенадцатого',
            'тринадцатого', 'четырнадцатого', 'пятнадцатого', 'шестнадцатого',
            'семнадцатого', 'восемьнадцатого', 'девятнадцатого');
        $NamesOfUnits0 = array('', 'первое', 'второе', 'третье', 'четвертое',
            'пятое', 'шестое', 'седьмое', 'восьмое', 'девятое');
        $NamesOfUnits = array('', 'первого', 'второго', 'третьего', 'четвертого',
            'пятого', 'шестого', 'седьмого', 'восьмого', 'девятого');

        $date_ = explode(".", $date);
        $day = (int)$date_[0];
        $month = (int)$date_[1];
        $year = (int)$date_[2];

        // разбираем число
        $tn = floor($day / 10);
        $un = $day % 10;

        if ($un == 0) {
            $result = $NamesOfTens00[$tn];
        } elseif ($tn == 0) {
            $result = $NamesOfUnits0[$un];
        } elseif ($tn == 1) {
            $result = $NamesOfFistTen0[$un];
        } else {
            $result = $NamesOfTens[$tn] . ' ' . $NamesOfUnits0[$un];
        }

        // разбираем месяц
        $result .= ' ' . $MonthNames[$month];

        // разбираем год
        $th = floor($year / 1000);
        $hn = floor($year % 1000 / 100);
        $tn = floor($year % 1000 % 100 / 10);
        $un = $year % 1000 % 100 % 10;
        if (($hn == 0) and ($tn == 0) and ($un == 0)) {
            $result .= ' ' . $NamesOfThousand0[$th] . ' года';
            return $result;
        } else
            $result .= ' ' . $NamesOfThousand[$th];
        if ($hn > 0) {

            if (($tn == 0) and ($un == 0)) {
                $result .= ' ' . $NamesOfHandreds0[$hn] . ' года';
                return $result;
            } else
                $result .= ' ' . $NamesOfHandreds[$hn];
        }

        if ($un == 0)
            $result .= ' ' . $NamesOfTens0[$tn];
        elseif ($tn == 0)
            $result .= ' ' . $NamesOfUnits[$un]; elseif ($tn == 1)
            $result .= ' ' . $NamesOfFistTen[$un]; else
            $result .= ' ' . $NamesOfTens[$tn] . ' ' . $NamesOfUnits[$un];
        $result .= ' года';
        return $result;
    }

    /**
     * Возвращает сумму прописью
     * @author runcore
     * @uses morph(...)
     */
    public static function num2str($num, $include_num = false)
    {
        $nul = 'ноль';
        $ten = array(
            array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
            array('', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
        );
        $a20 = array('десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
        $tens = array(2 => 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
        $hundred = array('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');
        $unit = array( // Units
            array('копейка', 'копейки', 'копеек', 1),
            array('рубль', 'рубля', 'рублей', 0),
            array('тысяча', 'тысячи', 'тысяч', 1),
            array('миллион', 'миллиона', 'миллионов', 0),
            array('миллиард', 'милиарда', 'миллиардов', 0),
        );
        //
        list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($num)));
        $out = array();
        if ($include_num) {
            $out[] = round($num, 0);
        }
        if (intval($rub) > 0) {
            foreach (str_split($rub, 3) as $uk => $v) { // by 3 symbols
                if (!intval($v))
                    continue;
                $uk = sizeof($unit) - $uk - 1; // unit key
                $gender = $unit[$uk][3];
                list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2 > 1)
                    $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3]; # 20-99
                else
                    $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3];
                # 10-19 | 1-9
                // units without rub & kop
                if ($uk > 1)
                    $out[] = Helpers::morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
            } //foreach
        } else
            $out[] = $nul;

        if ($include_num) {
            $out[2] = '(' . $out[2];
            $out[count($out) - 1] = $out[count($out) - 1] . ')';
            $out[] = 'руб.'; // rub
            $out[] = $kop . ' ' . 'коп.'; // kop
        } else {
            $out[] = Helpers::morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]); // rub
            $out[] = $kop . ' ' . Helpers::morph($kop, $unit[0][0], $unit[0][1], $unit[0][2]); // kop
        }
        return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
    }

    /**
     * Склоняем словоформу
     * @ author runcore
     */
    private static function morph($n, $f1, $f2, $f5)
    {
        $n = abs(intval($n)) % 100;
        if ($n > 10 && $n < 20)
            return $f5;
        $n = $n % 10;
        if ($n > 1 && $n < 5)
            return $f2;
        if ($n == 1)
            return $f1;
        return $f5;
    }

    /**
     *
     */
    public static function reutrnTitleAndValueIfNotNull($title, $value, $case = true)
    {
        if (!$case) return '';
        if ($value) {
            return '<strong>' . $title . ':</strong> ' . $value . '<br/>';
        }
        return '';
    }

    public static function returnTitleAndValueIfNotNull($title, $value, $case = true)
    {
        if (!$case) return '';
        if ($value) {
            return '<strong>' . $title . ':</strong> ' . $value . '<br/>';
        }
        return '';
    }

    public static function getCurrentDate()
    {
        return Yii::app()->dateFormatter->format('yyyy-MM-dd', time());
    }

    public static function eventPrepare($title, $url, $date, $color, $id = '', $class = '', $projectId = 'N/A', $isModal = false)
    {
        $k = array();
        $k['projectId'] = $projectId;
        $k['title'] = $title;

        if ($url) {
            $k['qt_url'] = $url;
            if (!$isModal) {
                $k['title'] = CHtml::link($title . CHtml::image('/images/redirect.png'), $url, array('target' => '_blank'));
            } else {
                $k['title'] = CHtml::link($title . CHtml::image('/images/redirect.png'), $url, array('class' => 'dwDialogTrigger', 'target' => '_blank'));
            }
        }

        if ($id)
            $k['id'] = $id;

        if ($class)
            $k['className'] = $class;

        if ((is_array($class))) {
            $f = explode(' ', $class[1]);
            $k['filterName'] = $f[0];
        } else {
            $k['filterName'] = $class;
        }
        $k['start'] = Yii::app()->dateFormatter->format('yyyy-MM-dd', $date);
        $k['color'] = $color;
        return $k;
    }

}

