<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sdd
 * Date: 12/13/13
 * Time: 1:15 AM
 * To change this template use File | Settings | File Templates.
 */

class currencyAjax {

    const RU_BANK=0;
    const EU_BANK=1;
    const NOTHING=-1;

    public function getCoursesData() {
        if ($data=$this->_getFromRuBank())  {
            return array($data,self::RU_BANK);
        }
        if ($data=$this->_getFromEuBank())  {
            return array($data,self::EU_BANK);
        }

        return array(false,self::NOTHING);
    }

    private  function _getFromRuBank() {
        $ch = curl_init();
        $cv = curl_version();
        $user_agent = "curl";
        curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);

        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
        curl_setopt($ch, CURLOPT_URL, "http://www.cbr.ru/scripts/XML_daily.asp");
        $xml = curl_exec($ch);
        curl_close($ch);

        if ($xml===false) return false;
        $xml = new SimpleXMLElement($xml);
        $lbl = array(
            'R01239' => Order::CURRENCY_EUR,
            'R01235' => Order::CURRENCY_USD,
            'R01035' => Order::CURRENCY_GBP,
            'R01820' => Order::CURRENCY_YEN,
        );

        $out = array();
        $out[Order::CURRENCY_RUB] = 1;
        foreach ($xml->Valute as $v) {
            if ($v['ID']) {
                if (key_exists((string)$v['ID'], $lbl)) {
                    $out[$lbl[(string)$v['ID']]] = (float)str_replace(',', '.', (string)($v->Value));
                }
            }
        }
        return $out;
    }
    private function _getFromEuBank() {
        //https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml
        $ch = curl_init();
        $cv = curl_version();
        $user_agent = "curl";
        curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);

        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
        curl_setopt($ch, CURLOPT_URL, "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");
        $xml = curl_exec($ch);
        curl_close($ch);
        if ($xml===false) return false;
        $xml = new SimpleXMLElement($xml);

        $lbl = array(
            'EUR' => Order::CURRENCY_EUR,
            'USD' => Order::CURRENCY_USD,
            'GBP' => Order::CURRENCY_GBP,
            'JPY' => Order::CURRENCY_YEN,
            'RUB' => Order::CURRENCY_RUB,
        );

        $out = array();
        $out[Order::CURRENCY_EUR]=1;

        foreach ($xml->Cube->Cube->Cube as $v) {
            if ($v['currency']) {
                if (key_exists((string)$v['currency'], $lbl)) {
                    $out[$lbl[(string)$v['currency']]] = (float)str_replace(',', '.', (string)($v['rate']));
                }
            }
        }
        foreach ($out as $key=>$val)
        {
            $out[$key]=$out[Order::CURRENCY_RUB]/$out[$key];
        }
        return $out;
    }

}