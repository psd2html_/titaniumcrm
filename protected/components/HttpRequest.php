<?php

class HttpRequest extends CHttpRequest
{
    public $noCsrfValidationRoutes = array();

    public function validateCsrfToken($event)
    {
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? $_SERVER["CONTENT_TYPE"] : null;

        if (strpos($contentType,'application/json')!==false)
        {
            $d=CJSON::decode($this->getRawBody());
            if (isset($d[$this->csrfTokenName]))
            {
              $_POST[$this->csrfTokenName]=$d[$this->csrfTokenName];
            }
        }

        parent::validateCsrfToken($event);
    }

    protected function normalizeRequest()
    {
        parent::normalizeRequest();
        if ($this->enableCsrfValidation) {
            $url = Yii::app()->getUrlManager()->parseUrl($this);
            foreach ($this->noCsrfValidationRoutes as $route) {
                if (strpos($url, $route) === 0)
                    Yii::app()->detachEventHandler('onBeginRequest', array($this, 'validateCsrfToken'));
            }
        }
    }
}