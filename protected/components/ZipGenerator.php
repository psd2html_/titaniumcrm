<?php
class ZipGenerator
{
    /**
     *
     * @param Project $project
     */
    public static function  buildProjectZip($project)
    {

        $t = $project->getArchiveName();
        $original_filename = self::getArchivePath($t);

        $job = array(
            'structure' => $project->serializeFolderStructure(),
            'filename' => $t
        );

        foreach ($project->orders as $o) {
            $gen = new DocGenerator($o);
            $gen->saveDoc(self::getOrderDocPath($o->name . '.docx'));

            $job['structure']['KP'][self::getOrderDocPath($o->name . '.docx')] = $o->name . '.docx';
        }

//        var_dump($job);

        $zip = new ZipArchive();
        if ($zip->open($original_filename, ZipArchive::CREATE) !== TRUE) {
            exit("cannot open \n");
        }

        foreach ($job['structure'] as $folder_name => $folder) {
            foreach ($folder as $path => $file_filename) {
                $zip->addFile($path, $folder_name . "/" . $file_filename);
            }
        }

        ob_start();
        self::_renderOverview($project,false);
        $data=ob_get_clean();

        $zip->addFromString("aboutProject.txt",$data);

        $zip->close();

    }

    /**
     *
     * @param Project $project
     */
    private static function _renderOverview($project, $toHtml = true)
    {

        $lineBreak = $toHtml ? "<br/>" : "\r\n";
        /*
     * (Ответственный,
     *  Класс прибора,
     * Производитель,
     * Базовый прибор,
     * закупка....
     * )
     * +
     * с контактами организации и пользователей по проекту +
     * с комментариями и заданиями
    */


        self::_safeRenderModelFields($project,
            array(
                'status' => 'Этап',
                'responsible.name' => 'Ответственный',
                'manufacturer.name' => 'Производитель',
                'coreInstrumentClass.name' => 'Класс прибора',
                'coreInstrument.name' => 'Прибор',
                'yearQuartal' => '',

            ), "Проект " . $project->name, $lineBreak);

        self::_safeRenderModelFields($project->companyConsumer,
            array(
                'fullName' => '',
                'companyClass.name' => '',
                'fullNameEng' => '',
                'shortName' => '',
                'city' => '',
                'physicalAddress' => '',
                'email' => '',
                'phone' => '',
                'addPhone' => '',
                'comment' => '',
            ), "Компания-потребитель", $lineBreak);

        self::_safeRenderModelFields($project->companyBuyer,
            array(
                'fullName' => '',
                'companyClass.name' => '',
                'fullNameEng' => '',
                'shortName' => '',
                'city' => '',
                'physicalAddress' => '',
                'email' => '',
                'phone' => '',
                'addPhone' => '',
                'comment' => '',
            ), "Компания-закупщик", $lineBreak);
        foreach ($project->getCustomers() as $c) {

            self::_safeRenderModelFields($c,
                array(
                    'customerComment' => 'Роль в проекте',
                    '#div' => '',
                    'email' => '',
                    'email2' => '',
                    'skype' => '',
                    'icq' => '',
                    'phone' => '',
                    'phone2' => '',
                    'mobilePhone' => '',
                    'comment' => '',
                ), $c->name, $lineBreak);
        }
        self::_renderTitle("Активные задачи", $lineBreak);
        foreach ($project->tasksActive as $c) {

            self::_safeRenderModelFields($c,
                array(
                    'user.name' => 'Автор',
                    'responsible.name' => 'Исполнитель',
                    'text' => 'Текст задания',
                    //'postCommitComment' => '',
                    'calendarDate' => '',
                    'date' => '',

                ), "", $lineBreak);
        }
        self::_renderTitle("Завершенные задачи", $lineBreak);
        foreach ($project->tasksFinished as $c) {

            self::_safeRenderModelFields($c,
                array(
                    'user.name' => 'Автор',
                    'responsible.name' => 'Исполнитель',
                    'text' => 'Текст задания',
                    'postCommitComment' => '',
                    'calendarDate' => '',
                    'date' => '',

                ), "", $lineBreak);
            self::_renderDash($lineBreak);
        }
        self::_renderTitle("Комментарии", $lineBreak);

        foreach ($project->commentsSticky as $c) {

            self::_safeRenderModelFields($c,
                array(
                    'user.name' => 'Автор',
                    'text' => 'Текст',
                    'date' => '',

                ), "", $lineBreak);
            self::_renderDash($lineBreak);
        }

    }

    /**
     * @param $project
     * @param $fields
     */
    private static function _safeRenderModelFields($project, $fields, $title = "", $lineBreak = "<br/>")
    {
        if (!$project) {
            return;
        }
        if ($title) {
            self::_renderTitle($title,$lineBreak );
        }
        foreach ($fields as $field => $label) {
            if ($field=="#div") {
                echo($lineBreak);
                continue;
            }
            $parts = explode(".", $field);
            $p = $project;
            $prev = $project;
            $lastAttr = null;
            $failed = false;
            foreach ($parts as $part) {
                if (!isset($p->$part)) {
                    $failed = true;
                    continue;
                }
                $prev = $p;
                $lastAttr = $part;
                $p = $p->$part;
            }
            if (!$failed && $p) {
                if (!$label) {
                    $label = $prev->getAttributeLabel($lastAttr);
                }
                $processed = strip_tags($p);
                if (strpos(strtolower($lastAttr), "date") !== false) {
                    $processed = Yii::app()->dateFormatter->formatDateTime($p, 'full', null);
                }
                echo($label . ": " . $processed);
                echo($lineBreak);
            }
        }
    }

    /**
     * @param $title
     */
    private static function _renderTitle($title, $lineBreak)
    {
        echo $lineBreak . "----------------------------------------------------------" . $lineBreak . $title . $lineBreak . "----------------------------------------------------------" . $lineBreak . $lineBreak;
    }

    private static function _renderDash($lineBreak)
    {
        echo $lineBreak;
    }

    private static function getArchivePath($t)
    {
        $root = Yii::app()->getBasePath() . '/async/processed/';
        return ($root . $t);
    }

    private static function getOrderDocPath($t)
    {
        $root = Yii::app()->getBasePath() . '/async/docvault/';
        return ($root . $t);
    }

    public static function downloadProjectZip($project)
    {
        $t = $project->getArchiveName();
        $original_filename = self::getArchivePath($t);
        header("Content-Length: " . filesize($original_filename));
        header("Content-Type: application/octet-stream");
        header('Content-Disposition: attachment; filename="' . $t . '"');
        readfile($original_filename);

    }

    public static function previewAboutProject($project)
    {
        Helpers::headerUTF8();
        self::_renderOverview($project,true);

    }
}