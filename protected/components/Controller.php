<?php
/**
 * @property MGridViewDataProvider mGridViewDataProvider
 * @property AngularJsDispatcher angularJsDispatcher
 *
 */
class Controller extends CController
{
    public $angularJsDispatcher;
    public $mGridViewDataProvider;
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column1';
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();
    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();
    public $sidebarToolbox;
    public $keywords;
    public $description;

    public function  init()
    {
        $this->angularJsDispatcher = new AngularJsDispatcher($this);
        $this->mGridViewDataProvider = new MGridViewDataProvider($this);

        parent::init();
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(

            array('deny', // deny all users
                'users' => array('?'),
            ),

        );
    }

    /**
     * RendersCsvView
     * @param $masterModel
     * @param $gridClassId
     * @param $context
     * @param $filenamePrefix
     */

    public function _getCsv($gridClassId = false, $context = false, $masterModel = false, $filenamePrefix = "project")
    {
        $this->layout = false;

        header('content-type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment;filename=' . $filenamePrefix . '__' . Yii::app()->dateFormatter->format("yyyy-M-d__HH-mm-s", time()) . '.csv');
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        //$this->mGridViewDataProvider->renderMGridView($gridClassId,$masterModel,false,false,$context,true);
        $this->mGridViewDataProvider->renderCSV($gridClassId, $masterModel, $context, CUserPreferences::getParam(CUserPreferences::CSV_EXPORT_SEPARATOR));


    }

    protected function loadModel($id) {
        return False;
    }

    protected function safeLoadModel($id, $overrideAction = False, $overrideZone= False)
    {
        $mdl=$this->loadModel($id);
        $this->applyGroupPolicy($mdl,$overrideAction,$overrideZone);
        return $mdl;
    }

    protected function safeLoadModelProjectSubzone($id, $overrideAction = False, $overrideZone= False)
    {
        $mdl=$this->loadModel($id);
        $this->applyGroupPolicy($mdl->project,$overrideAction,$overrideZone);
        return $mdl;
    }

    protected function applyGroupPolicy($model = False, $overrideAction = False, $overrideZone= False)
    {
        if ($model===Null) {
            throw new CHttpException(404);
        }
        if ($overrideZone === False) {
            $z = $this->getZone();
        } else {
            $z=$overrideZone;
        }
        if ($z !== False) {
            if ($overrideAction === False) {
                $za = $this->getZoneActionsToActions();
                $a = $za['_'];
                if (array_key_exists($this->action->id, $za)) {
                    $a = $za[$this->action->id];
                } else {
                    throw new Exception ("Default zone action for controller action ".$this->action->id." not found! Quitting");
                }
            } else {
                $a = $overrideAction;
            }
            $rule = Group::getPermissionsUponZone($z, $a, $model);
            if (!$rule) {
                throw new CHttpException(403, "Доступ к данному действию запрещен. Информация для медетации: $$" . $z . "A" . $a . "U" . Yii::app()->user->id . "-G-" . Yii::app()->user->idGroup);
            }
        }
    }

    public function getZone()
    {
        return False;
    }

    public function getZoneActionsToActions()
    {
        return array(
            'index' => Group::ACTION_ADMIN,
            'view' => Group::ACTION_VIEW,
            'update' => Group::ACTION_UPDATE,
            'create' => Group::ACTION_CREATE,
            'delete' => Group::ACTION_DELETE,
            '_' => Group::ACTION_VIEW,
        );
    }
}
