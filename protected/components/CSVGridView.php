<?php
class CSVGridView extends MGridView
{
    public $separator=",";

    public function run()
    {
        $this->renderContent();
    }

    public function renderPager()
    {

    }
    public function renderKeys()
    {

    }
    /**
     * Renders the data items for the grid view.
     */
    public function renderItems()
    {
        if ($this->dataProvider->getItemCount() > 0 || $this->showTableOnEmpty) {
            ob_start();
            foreach ($this->columns as $column) {
                if ($column instanceof CDataColumn) {
                    ob_start();
                    $column->renderHeaderCell();
                    $dataCellRaw = ob_get_clean();
                    $dataCellRaw='"'.str_replace('"','""',$dataCellRaw).'"';
                    echo $dataCellRaw.$this->separator;

                }
            }
            $header = ob_get_clean();
            $header = mb_substr($header,0,mb_strlen($header)-1);

            $data = $this->dataProvider->getData();
            $n = count($data);

            $buff=$header."\n";
            if ($n > 0) {
                for ($row = 0; $row < $n; ++$row) {

                    ob_start();

                    $this->renderTableRow($row);

                    $r = ob_get_clean();
                    $r = mb_substr($r,0,mb_strlen($r)-1);
                    $buff=$buff.$r."\n";
                }
            }


            $body = str_replace("<br/>", "\n", $buff);
            $body = strip_tags($body);
            echo  $body ;
        } else
            $this->renderEmptyText();
    }

    /**
     * Renders the table header.
     */
    public function renderTableHeader()
    {


    }

    /**
     * Renders the table body.
     */
    public function renderTableBody()
    {

    }

    /**
     * Renders a table body row.
     * @param integer $row the row number (zero-based).
     */
    public function renderTableRow($row)
    {
        foreach ($this->columns as $column) {
            if ($column instanceof CDataColumn) {
                ob_start();

                $column->renderDataCell($row);

                $dataCellRaw = ob_get_clean();

                $dataCellRaw='"'.str_replace('"','""',$dataCellRaw).'"';

                echo $dataCellRaw.$this->separator;
            }
        }
    }

    /**
     * Renders the filter.
     * @since 1.1.1
     */
    public function renderFilter()
    {

    }

    /**
     * Renders the table footer.
     */
    public function renderTableFooter()
    {

    }


}