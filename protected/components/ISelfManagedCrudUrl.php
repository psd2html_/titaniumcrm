<?php
/**
 * Class ISelfManagedCrudUrl
 * Class provides functions to get explicit url for view, update and delete
 */
interface ISelfManagedCrudUrl {
    /**
     * @return string
     */
    public function getUpdateUrl($params=false);
    /**
     * @return string
     */
    public function getViewUrl($params=false);
    /**
     * @return string
     */
    public function getDeleteUrl($params=false);
    /**
     * @return string
     */
    public function getAdminUrl($params=false);


}