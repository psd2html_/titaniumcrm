<?php
class CustomizableColumnsProvider
{
    const PRESET_COUNT = 15;
    const FILTER_LOCKING_IMPRINT = 1;
    const FILTER_LOCKING_DISABLED = 0;
    const FILTER_LOCKING_ENABLED = 2;

    public static function selectPreset($modelName, $presetId)
    {
        if (Yii::app()->user->isGuest) return;
        $old = self::_getUserSettings();

        if (!key_exists($modelName, $old) || !isset($old[$modelName]['c']) || !isset($old[$modelName]['b'])) {
            $old[$modelName] = array();
            $old[$modelName]['c'] = 0;
            $old[$modelName]['b'] = array();
        }

        $old[$modelName]['c'] = $presetId;
        self::_saveUserSettings($old);
    }

    /**
     * @return array
     */
    private static function _getUserSettings()
    {
        //$array = array();

        //if (!YII_LOCAL)
            $array = Yii::app()->cache->get("COLUMNS_SETTING_USER_" . Yii::app()->user->id);

        if ($array === false) {
            $user = Yii::app()->user->model();

            $var = $user->serializedColumns;
            $array = CJSON::decode($var);

            if (($array === NULL) || (!is_array($array))) {
                $array = self::_dropUserSettings();
            }


           // if (!YII_LOCAL)
                Yii::app()->cache->set("COLUMNS_SETTING_USER_" . Yii::app()->user->id, $array, 24 * 60 * 60);
        }

        return $array;
    }

    /**
     *
     * @param $user
     * @return array
     */
    private static function _dropUserSettings()
    {
        $user = Yii::app()->user->model();
        $array = array();
        $user->serializedColumns = CJSON::encode($array);
        $user->update(array('serializedColumns'));

        //if (!YII_LOCAL)
         Yii::app()->cache->set("COLUMNS_SETTING_USER_" . Yii::app()->user->id, $array, 24 * 60 * 60);

        return $array;
    }

    /**
     * @param $array
     */
    private static function _saveUserSettings($array)
    {
        $user = Yii::app()->user->model();
        $user->serializedColumns = CJSON::encode($array);
        $user->update(array('serializedColumns'));

        //if (!YII_LOCAL)
            Yii::app()->cache->set("COLUMNS_SETTING_USER_" . Yii::app()->user->id, $array, 24 * 60 * 60);
    }

    public static function setSettings($modelName, $value, $newPresetName = null, $newSort = false)
    {
        if (Yii::app()->user->isGuest) return;
        $old = self::_getUserSettings();

        if (!key_exists($modelName, $old) || !isset($old[$modelName]['c']) || !isset($old[$modelName]['b'])) {
            $old[$modelName] = array();
            $old[$modelName]['c'] = 0;
            $old[$modelName]['b'] = array();
        }

        $currentPresetId = isset($old[$modelName]['c']) ? $old[$modelName]['c'] : 0;

        if (!key_exists($currentPresetId, $old[$modelName]['b'])) {
            $old[$modelName]['b'][$currentPresetId] = array();
            $old[$modelName]['b'][$currentPresetId]['n'] = $newPresetName ? $newPresetName : "По-умолчанию";
        }

        $old[$modelName]['b'][$currentPresetId]['v'] = $value;

        if ($newPresetName) {
            $old[$modelName]['b'][$currentPresetId]['n'] = $newPresetName;
        }

        if ($newSort!==false) {
            $reorder = array();
            $i = 0;

            if (is_array($newSort)) {
                foreach ($newSort as $newItem) {
                    $reorder[$i] = $old[$modelName]['b'][$newItem];
                    $i++;
                }
            }
            $old[$modelName]['b'] = $reorder;
        }
        self::_saveUserSettings($old);
    }

    public static function getAllColumnsDataList($modelName)
    {
        $cmb = self::_getARCustomizableColumn($modelName);
        $selected = self::getSettings($modelName);
        $dl = $cmb->getAllColumnsDataList();;
        $dl = self::sortColumns($dl, $selected);
        return $dl;
    }

    /**
     * @param string $modelName
     * @return ARCustomizableColumnsBase
     * @throws Exception
     */
    private static function _getARCustomizableColumn($modelName)
    {

        Yii::import("application.components.ARCustomizableColumns.*");
        switch ($modelName) {
            case "project":
                return new ProjectCustomizableColumns();
                break;
            case "customer":
                return new CustomerCustomizableColumns();
                break;
            case "sample":
                return new SampleCustomizableColumns();
                break;
            case "company":
                return new CompanyCustomizableColumns();
                break;
            case "pm":
                return new PmCustomizableColumns();
            default:
                throw new Exception("no model found for " . $modelName);
        }

    }

    /*
     * fill default presets
     */

    /** get Current User's active columns for Model;
     * @param string $modelName
     * @return array() of columnsID
     */
    public static function getSettings($modelName)
    {
        if (Yii::app()->user->isGuest) return array();


        $array = self::_getUserSettings();

        // migration from v1.0 to v2.0

        if (!empty($array)) {
            if (key_exists($modelName, $array)) {
                if (!isset($array[$modelName]['c']) || !isset($array[$modelName]['b'])) {
                    // old version; no presets; needs to be redone
                    self::_dropUserSettings();
                    $array = array();
                }
            }
        }

        if (!empty($array))
        {
            if (key_exists($modelName, $array)) {
                $currentPresetId = $array[$modelName]['c'];

                if (key_exists($currentPresetId, $array[$modelName]['b']) && isset($array[$modelName]['b'][$currentPresetId]['v'])) {
                    return $array[$modelName]['b'][$currentPresetId]['v'];
                } else {
                    // preset not found; setting default; rare occasion!
                    $cmb = self::_getARCustomizableColumn($modelName);

                    $array[$modelName]['b'][$currentPresetId]['v'] = $cmb->getDefaultColumns();
                    $array[$modelName]['b'][$currentPresetId]['n'] = "По-умолчанию";
                    $array[$modelName]['b'][$currentPresetId]['l'] = false;
                    $array[$modelName]['b'][$currentPresetId]['f'] = array();

                    self::_saveUserSettings($array);

                    return $array[$modelName]['b'][$currentPresetId]['v'];
                }
            } else {
                // no block for this model found

                $array[$modelName]      = array();
                $array[$modelName]['c'] = 0; //current preset

                $cmb = self::_getARCustomizableColumn($modelName);

                $defaultValue = $cmb->getDefaultColumns();

                for ($i = 0; $i < 5; $i++) {
                    $array[$modelName]['b'][$i]['v'] = $cmb->getDefaultColumns();
                    $array[$modelName]['b'][$i]['n'] = ($i == 0) ? "По-умолчанию" : ("Пресет-" . ($i + 1));
                    $array[$modelName]['b'][$i]['l'] = false;
                    $array[$modelName]['b'][$i]['f'] = array();
                }
                //  $array[$modelName] = $defaultValue;

                self::_saveUserSettings($array);

                return $defaultValue;
            }
        }
    }

    private static function sortColumns($data, $set)
    {
        $newOrder = array();
        $keys = array_keys($data);
        $set = array_intersect($set, $keys);
        $rest = array_diff($keys, $set);
        $set = array_merge($set, $rest);
        foreach ($set as $id) {
            $newOrder[$id] = $data[$id];
        }
        return $newOrder;
    }

    public static function getPresets($modelName)
    {
        if (Yii::app()->user->isGuest) return array();


        $array = self::_getUserSettings();

        // migration from v1.0 to v2.0

        if (!empty($array)) {
            if (key_exists($modelName, $array)) {
                if (!isset($array[$modelName]['c']) || !isset($array[$modelName]['b']) || !isset($array[$modelName]['b'][$array[$modelName]['c']])
                    || !isset($array[$modelName]['b'][$array[$modelName]['c']]['n'])
                ) {
                    return array();
                }

                return ($array[$modelName]['b']);
            } else {
                return array();
            }
        }
    }

    public static function getCurrentPreset($modelName)
    {
        if (Yii::app()->user->isGuest) return array();
        $array = self::_getUserSettings();

        if (!empty($array)) {
            if (key_exists($modelName, $array)) {
                if (!isset($array[$modelName]['c']) || !isset($array[$modelName]['b']) || !isset($array[$modelName]['b'][$array[$modelName]['c']])
                    || !isset($array[$modelName]['b'][$array[$modelName]['c']]['n'])
                ) {
                    return array(0, "Ошибка");
                }

                return array($array[$modelName]['c'], $array[$modelName]['b'][$array[$modelName]['c']]['n']);
            } else {
                return array(0, "Ошибка");
            }
        }
    }

    /** Proxies
     * @param CActiveRecord $model
     * @return array of columns
     */
    public static function getColumns($model, $context = false)
    {
        //self::_dropUserSettings();
        $modelName = strtolower(get_class($model));

        $a = self::getSettings($modelName);
        $cmb = self::_getARCustomizableColumn($modelName);
        $cmb->context = $context;

        return $cmb->getColumns($a);
    }

    public static function getFilters($modelName)
    {
        $a = self::getCurrentPresetData($modelName);

        if (!isset($a['l']) || !isset($a['f'])) {
            $a['l'] = self::FILTER_LOCKING_DISABLED;
            $a['f'] = array();
            self::saveCurrentPreset($modelName, $a);
        }
        return array($a['l'], $a['f']);
    }

    public static function getCurrentPresetData($modelName)
    {
        if (Yii::app()->user->isGuest) return array();
        $array = self::_getUserSettings();

        if (!empty($array)) {
            if (key_exists($modelName, $array)) {
                if (!isset($array[$modelName]['c']) || !isset($array[$modelName]['b']) || !isset($array[$modelName]['b'][$array[$modelName]['c']])
                ) {
                    $data = self::_buildConfigForModel($modelName);
                    $array[$modelName] = $data;
                    self::_saveUserSettings($array);
                    return $data['b'][0];
                }

                if (count($array[$modelName]['b']) < self::PRESET_COUNT) {
                    $data = self::_buildConfigForModel($modelName, count($array[$modelName]['b']));
                    foreach ($data['b'] as $key => $val) {
                        $array[$modelName]['b'][$key] = $val;
                    }

                    self::_saveUserSettings($array);
                } elseif (count($array[$modelName]['b']) > self::PRESET_COUNT) {
                    $data = self::_buildConfigForModel($modelName);
                    $array[$modelName] = $data;
                    self::_saveUserSettings($array);
                    return $data['b'][0];

                }
                return $array[$modelName]['b'][$array[$modelName]['c']];
            } else {
                $data = self::_buildConfigForModel($modelName);
                $array[$modelName] = $data;
                self::_saveUserSettings($array);
                return $data['b'][0];
            }
        } else {
            return false;
        }
    }

    private static function _buildConfigForModel($modelName, $from = 0)
    {
        $config = array();
        $config['c'] = 0; //current preset
        $config['b'] = array();

        $cmb = self::_getARCustomizableColumn($modelName);

        for ($i = $from; $i < self::PRESET_COUNT; $i++) {
            $config['b'][$i] = self::_buildDefaultPreset($modelName);
            $config['b'][$i]['n'] = ($i == 0) ? "По-умолчанию" : ("Пресет-" . ($i + 1));
        }
        return $config;
    }

    private static function  _buildDefaultPreset($modelName)
    {
        $cmb = self::_getARCustomizableColumn($modelName);
        $p = array();
        $p['v'] = $cmb->getDefaultColumns();
        $p['n'] = "По-умолчанию";
        $p['l'] = false;
        $p['f'] = array();
        return $p;
    }

    public static function saveCurrentPreset($modelName, $v)
    {
        if (Yii::app()->user->isGuest) return array();
        $array = self::_getUserSettings();

        if (!empty($array)) {
            if (key_exists($modelName, $array)) {
                if (!isset($array[$modelName]['c']) || !isset($array[$modelName]['b']) || !isset($array[$modelName]['b'][$array[$modelName]['c']])
                ) {
                    $data              = self::_buildConfigForModel($modelName);
                    $array[$modelName] = $data;
                }

                $array[$modelName]['b'][$array[$modelName]['c']] = $v;
                self::_saveUserSettings($array);

            } else {
                $data                                            = self::_buildConfigForModel($modelName);
                $array[$modelName]                               = $data;
                $array[$modelName]['b'][$array[$modelName]['c']] = $v;
                self::_saveUserSettings($array);
            }
        } else {
            return false;
        }

    }

    public static function saveFilters($modelName, $filters)
    {
        if (!is_array($filters)) return;

        $a = self::getCurrentPresetData($modelName);
        $a['l'] = self::FILTER_LOCKING_ENABLED;
        $a['f'] = $filters;
        self::saveCurrentPreset($modelName, $a);
    }

    public static function triggerFilterLocking($modelName)
    {
        $a = self::getCurrentPresetData($modelName);

        if (!isset($a['l']) || !isset($a['f'])) {
            $a['l'] = 1;
            $a['f'] = array();
        } else {
            if ($a['l'] == self::FILTER_LOCKING_DISABLED) {
                $a['l'] = self::FILTER_LOCKING_IMPRINT;
            } else {
                $a['l'] = self::FILTER_LOCKING_DISABLED;
            }
        }

        self::saveCurrentPreset($modelName, $a);
    }
}