<?php
/**
 * Class for  Generator Image Color Palette.  
 * This file is class for return  mamimum used colors in image, format return in HTML or RGB format. 
 *
 * PHP versions 4 and 5
 * Licensed under The GPL License
 *
 * Developed By 	Kalpeh Gamit
 * E-mail 			Kalpa.programmer@gmail.com
 *
 * @filesource
 * @copyright     Copyright 2009, InScripter Inc. (http://www.inscripter.com)
 * @link          http://www.inscripter.com
 * @package       PHP
 * @version       GetImageColor-1.0.0
 * @license       http://www.opensource.org/licenses/gpl-2.0.php The GPL License
 */
 
// start of class : Generator Image Color Palette 
class getImageColor 
{
	// get image color in RGB format function 
	public static function imageColor($imageFile_URL, $numColors, $image_granularity = 5)
	{
   		$image_granularity = max(1, abs((int)$image_granularity));
   		$colors = array();
   		//find image size
   		$size = @getimagesize($imageFile_URL);
   		if($size === false)
   		{
      		user_error("Unable to get image size data");
      		return false;
   		}
   		// open image
   		$img = @imagecreatefromjpeg($imageFile_URL);
   		if(!$img)
   		{
   	  		user_error("Unable to open image file");
   		   return false;
   		}
   		
   		// fetch color in RGB format
   		$hash=array();
   		$roughness=0x33;
   		$pixel = imagecreatetruecolor(1, 1);
   		 
   		
   		for($x = $image_granularity; $x < $size[0]; $x += $image_granularity)
   		{
      		for($y = $image_granularity; $y < $size[1]; $y += $image_granularity)
      		{
      			imagecopyresampled($pixel, $img, 0, 0, $x-$image_granularity, $y-$image_granularity, 1, 1, $image_granularity,$image_granularity);
         		//$thisColor = imagecolorat($img, $x, $y);
      			$thisColor  = imagecolorat($pixel, 0, 0);
      			$rgb = imagecolorsforindex($img, $thisColor);
        		$a[0]= round(round(($rgb['red'] / $roughness)) * $roughness);
         		$a[1]= round(round(($rgb['green'] / $roughness)) * $roughness);
         		$a[2]= round(round(($rgb['blue'] / $roughness)) * $roughness);
         		$thisRGB = sprintf('%02X%02X%02X', $a[0], $a[1], $a[2]);
         		$a[3]=$thisRGB;
         		$hash[$thisRGB]=$a;
         		if(array_key_exists($thisRGB, $colors))
         		{
           			 $colors[$thisRGB]++;
         		}
         		else
         		{
           			 $colors[$thisRGB] = 1;
         		}
      		}
   		}
   		arsort($colors);
   		$top_colors=array_slice(($colors), 0, $numColors,true);
   		$ret=array();
   		foreach ($top_colors as $t=>$v) {
   			$ret[]=$hash[$t];
   		}
   		// returns maximum used color of image format like #C0C0C0.
   		return $ret;
	}

	// html color to convert in RGB format color like R(255) G(255) B(255)  
	public static function getHtml2Rgb($str_color)
	{
		

        	$r = substr($str_color, 0,2);
        	$g = substr($str_color, 2,2);
        	$b = substr($str_color, 4,2);
    	//echo($str_color[0].$str_color[1]);
    	$r = hexdec($r); $g = hexdec($g); $b = hexdec($b);
    	$arr_rgb = array($r, $g, $b);
		// Return colors format liek R(255) G(255) B(255)  
    	return $arr_rgb;
	}

// end of class here : Generator Image Color Palette  
}
?>