<?php
/**
 * Searchable fields on steriods.
 * -----------------------------
 * 0. set SearchableActiveRecord as parent
 * if model->init() is overriden, make sure to parent::init();
 * 
 * 1. set public function searchableFields()
 * 2. in search() function call:
 * $criteria= $model->buildSearchCriteria(); based on config
 * return new CActiveDataProvider($model, array(
        'criteria' => $criteria));
 *
 */


class SearchableActiveRecord extends CActiveRecord
{

    /**
     * Returns CActiveDataProvider based on search config
     * @return CActiveDataProvider
     */

    public function  search() {
        $criteria=$this->buildSearchCriteria();
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria
        ));
    }

    private $_wasSearchableFieldsInitialized = false;
    private $_searchableFields = array();
    private $_searchableFieldRules = array();
    private $_searchableFieldLabels = array();
    private $_searchableFieldSort = array();

    /** Inject our synthetic labels for searchable fields
     * Returns the text label for the specified attribute.
     * @param string $attribute the attribute name
     * @return string the attribute label
     * @see generateAttributeLabel
     * @see attributeLabels
     */
    public function getAttributeLabel($attribute)
    {

        $labels = $this->attributeLabels();
        if (isset($labels[$attribute]))
        {

            return $labels[$attribute];
        }
        else if (isset($this->_searchableFieldLabels[$attribute])) {

            return $this->_searchableFieldLabels[$attribute];
        }
        else
            return $this->generateAttributeLabel($attribute);
    }

    /** Inject our synthetic rules for searchable fields
     * Creates validator objects based on the specification in {@link rules}.
     * This method is mainly used internally.
     * @throws CException if current class has an invalid validation rule
     * @return CList validators built based on {@link rules()}.
     */
    public function createValidators()
    {
        $validators = new CList;
        $mixedRules = array_merge($this->rules(), $this->_searchableFieldRules);

        foreach ($mixedRules as $rule) {
            if (isset($rule[0], $rule[1])) // attributes, validator name
            $validators->add(CValidator::createValidator($rule[1], $this, $rule[0], array_slice($rule, 2)));
            else
                throw new CException(Yii::t('yii', '{class} has an invalid validation rule. The rule must specify attributes to be validated and the validator name.',
                    array('{class}' => get_class($this))));
        }
        return $validators;
    }

    /**
     * @param string $name
     * @return mixed|void
     */

    public function __get($name)
    {

        if (key_exists($name, $this->_searchableFields))
            return $this->_searchableFields[$name];
        else
            return parent::__get($name);
    }

    public function __set($name, $value)
    {
        if ($this->setSearchableField($name, $value) === false) {
            parent::__set($name, $value);
        }
    }

    public function setSearchableField($name, $value)
    {
        if (key_exists($name, $this->_searchableFields)) {
            $this->_searchableFields[$name] = $value;
            return true;
        }
        return false;
    }

    public function init()
    {

        $this->_initSearchableFields();
        parent::init();
    }

    /**
     * return sort for dataprovider
     * @return array
     */

    public function getSortAttributes()
    {
        $s=$this->_searchableFieldSort;
        $s[]='*';
        return $s;
    }

    private function _initSearchableFields()
    {
        $this->_wasSearchableFieldsInitialized = true;
        foreach ($this->searchableFields() as $searchableField) {

            if (isset($searchableField[0], $searchableField[1])) // attributes, validator name
            {

                $out = CSearchableField::getSyntheticField($searchableField, $this);
                if ($out) {
                    $this->_searchableFields = $this->_searchableFields + $out;
                }

                $out = CSearchableField::getLabelsForSearchableField($searchableField, $this);

                if ($out) {
                    $this->_searchableFieldLabels = $this->_searchableFieldLabels + $out;
                }

                $out = CSearchableField::getSortForSearchableField($searchableField, $this);
                if ($out) {
                    $this->_searchableFieldSort = $this->_searchableFieldSort + $out;
                }

                $out = CSearchableField::getRulesForSearchableField($searchableField, $this);

                if ($out) {
                    $this->_searchableFieldRules[] = ($out);
                }
            } else
                throw new CException(Yii::t('yii', '{class} has an invalid searchable field.',
                    array('{class}' => get_class($this))));
        }
    }

    /**
     * !Override this!
     * Searchable Fields config
     * format:
     * array(
     *      array('field1, field2, relationName.field1, relationName.field2', 'applied generator name', options),
     * )
     * refer to /searchableField/* to example and use cases
     * @return array
     */
    public function searchableFields()
    {
        return array();
    }

    /**
     * @return CDbCriteria
     */
    public function buildSearchCriteria()
    {
        if (!$this->_wasSearchableFieldsInitialized) {
            throw new Exception("Searchable Fields was not initialized properly. Missing parent::init() in model's init() override?");
        }

        $a = new CDbCriteria();

        foreach ($this->searchableFields() as $searchableField) {
            if (isset($searchableField[0], $searchableField[1])) // attributes, validator name
            CSearchableField::applySearchFieldsOnCriteria($a, $searchableField, $this);
            else
                throw new CException(Yii::t('yii', '{class} has an invalid searchable field.',
                    array('{class}' => get_class($this))));
        }
        $a->together=true;
        return $a;
    }
}