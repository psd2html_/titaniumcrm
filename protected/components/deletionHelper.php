<?php

class deletionHelper {
    
    const D_PROJECT=0;
    
    const D_CHECKPOINT=1;
    const D_CHECKPOINTGROUP=2;
    const D_COMPANYCLASS=4;
    const D_CONTRAGENT=5;
        
    const D_COREINSTRUMENT=6;
    const D_COREINSTRUMENTCLASS=7;
    
    const D_CUSTOM=8;
        
        const D_FEATURE=10;
        const D_FEATUREGROUP=11;
    
    
    const D_CUSTOMER=9;
    const D_COMPANY=3;
    const D_MANUFACTURER=12;
    const D_ORDER=13;
    const D_SAMPLE=14;
    const D_CENTRE=15;
    const D_WORKFLOW=16;
    
    const D_SPENT=17;
    const D_INSURANCE=18;
    const D_STICKY=19;
    const D_LOGISTICFRAGMENT=20;

    public static function getIdToModel($modelType,$id) {
        
        switch ($modelType)
        {
            case self::D_LOGISTICFRAGMENT:
                return LogisticFragment::model()->findByPk($id);
            case self::D_STICKY:
                return Sticky::model()->findByPk($id);

            case self::D_INSURANCE:
                return InsuranceCompany::model()->findByPk($id);
            case self::D_SPENT:
                return Spent::model()->findByPk($id);
            case self::D_WORKFLOW:
                return ProjectWorkflow::model()->findByPk($id);
            case self::D_CENTRE:
                return Centre::model()->findByPk($id);
            case self::D_FEATURE:
                return Feature::model()->findByPk($id);
            case self::D_FEATUREGROUP:
                return FeatureGroup::model()->findByPk($id);
            case self::D_PROJECT:
                return Project::model()->findByPk($id);
             case self::D_CONTRAGENT:
                return Contragent::model()->findByPk($id);
            case self::D_CUSTOM:
                return Custom::model()->findByPk($id);
            case self::D_ORDER:
                return Order::model()->findByPk($id);
            case self::D_CHECKPOINT:
                return Checkpoint::model()->findByPk($id);
            case self::D_CHECKPOINTGROUP:
                return CheckpointGroup::model()->findByPk($id);
            case self::D_SAMPLE:
                return Sample::model()->findByPk($id);
            case self::D_MANUFACTURER:
                return Manufacturer::model()->findByPk($id);
            break;
            case self::D_COMPANY:
                return Company::model()->findByPk($id);
            break;
            case self::D_COMPANYCLASS:
                return CompanyClass::model()->findByPk($id);
            break;
            case self::D_CUSTOMER:
                return Customer::model()->findByPk($id);
            break;
            case self::D_COREINSTRUMENT:
                return CoreInstrument::model()->findByPk($id);
            break;
            case self::D_COREINSTRUMENTCLASS:
                return CoreInstrumentClass::model()->findByPk($id);
            break;
        }
        
        return null;
    } 
}

?>
