<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sdd
 * Date: 9/2/13
 * Time: 11:58 PM
 * To change this template use File | Settings | File Templates.
 */

class MGridViewFilterWidget
{


    public static function getFilterWidget($name, &$column, &$gridView)
    {
        Yii::import("application.components.filterWidget.*");
        switch ($name) {
            case "clickableFilter":
                $a= new clickableFilterWidget($column,$gridView);
                return $a;
                break;
            case "clickableKeyValueFilter":
                $a= new clickableKeyValueFilterWidget($column,$gridView);
                return $a;
                break;
            default;
                throw new CException("Unknown filter: ".$name);
                return null;
        }

    }


}