<?php

class MLinkPager extends CLinkPager {
    protected function createPageButtons() {
        $buttons = parent::createPageButtons();
        if ($buttons) {
            $p = $_GET;
            $p['showAll'] = 1;

            $buttons[] = '<li class="page">' . CHtml::link("Показать все", Yii::app()->createUrl(Yii::app()->controller->id . "/".Yii::app()->controller->action->id , $p)) . '</li>';
        }
        return $buttons;
    }

    public function run() {
        parent::run();

        $pageSize=$this->pages->pageVar.'Size';
        $sizes=array(10,25,50,100);
        $sizeLinks=array();
        $params=$_GET;
        $controller=$this->getController();

        foreach ($sizes as $s) {
            $params[$pageSize]=$s;

            $sizeLinks[]= CHtml::link($s, $controller->createUrl($this->pages->route,($params)));
        }

        echo '<div class="right">'.
            implode(" | ",$sizeLinks).
            " | ".
            CHtml::link("Пермалинк", Yii::app()->request->url) .

        '</div>';
    }

    public function init() {


        parent::init();
    }
}


