<?php

Yii::import('zii.widgets.grid.CGridView');


class MGridView extends CGridView
{
    const SEARCH_VALUE_ISNULL="!";
    public $filterLocking=CustomizableColumnsProvider::FILTER_LOCKING_DISABLED;
    public $noPermalink = false;
    public $noCustomizableColumns = false;
    public $insertColumns = null;
    public $extended = false;
    public $lockableFilters = true;
    public $downloadCSV = false;
    public $context = false;
    public $readOnly = false;

    private $afterHook;

    public function init()
    {
        // $this->ajaxType="POST";
        $gridViewFilterWidget = array();

        $pageSizeVar=$this->dataProvider->pagination->pageVar.'Size';
        if (isset($_GET[$pageSizeVar])) {
            $this->dataProvider->pagination->pageSize=$_GET[$pageSizeVar];
        } else {
            $this->dataProvider->pagination->pageSize=CUserPreferences::getParam(CUserPreferences::DEFAULT_ROW_COUNT_IN_GRIDVIEW);
        }

        if (isset($_GET['showAll'])) {
            $this->dataProvider->pagination = false;
        }

        if (!$this->noPermalink) {
            $this->pager = array('class' => 'MLinkPager');
        }

        if (!$this->noCustomizableColumns) {
            if ($this->filter instanceof IProvidesCustomizableColumns) {
                $f = CustomizableColumnsProvider::getColumns($this->filter, $this->context);

                if ($this->lockableFilters) {
                    $m=strtolower(get_class($this->filter));
                    list($filterLockingStatus,$filters) = CustomizableColumnsProvider::getFilters($m);
                    $this->filterLocking=$filterLockingStatus;
                }

                // test for custom filtering widgets
                foreach ($f as $key => $value) {
                    if (isset($value['filter']) && (is_array($value['filter']))) {
                        $f[$key]['filter'][self::SEARCH_VALUE_ISNULL]="< Не установлен >";
                    }
                    if (isset($value['filterWidget'])) {
                        $fw = $value['filterWidget'];
                        $fwm = MGridViewFilterWidget::getFilterWidget($fw, $value, $this);
                        $f[$key] = $value;
                        if (!key_exists($fw, $gridViewFilterWidget)) {
                            $gridViewFilterWidget[$fw] = $fwm;
                        }
                    }

                    if (!isset($value['class']) && $this->lockableFilters) {

                        $f[$key]['isLocked']=($filterLockingStatus>0)
                            &&
                            (isset($filters[$value['name']])) &&
                                ($filters[$value['name']]!='');
                    }

                    if (isset($value['class']) && $value['class']=="MButtonColumn" && $this->lockableFilters) {

                        $filterLockState='<a href="#" class="'.(
                            ($filterLockingStatus==CustomizableColumnsProvider::FILTER_LOCKING_DISABLED)?"filterUnlocked":"filterLocked"
                            ).' button secondary small" mode="'.$filterLockingStatus.'" onclick="window.dwDialog.triggerFilterLock'.
                        "('$m','#{$this->id}',this); return false;\">";


                        $f[$key]['filterCell']=$filterLockState;

                    }
                }
                $this->columns = $f;

                if ($this->insertColumns) {
                    $this->columns = $columns = array_merge($this->insertColumns, $this->columns);

                }
            }
            $afterHook = 'window.dwDialog.bindTriggers();' . "\n";
            $afterHook .= 'window.mGridView.bindClearInput("'.$this->id.'");' . "\n";
            foreach ($gridViewFilterWidget as $g) {
                $afterHook .= $g->getInitJS() . "\n";
            }

            $this->afterAjaxUpdate .= "function() {" . $afterHook . "}";
            $s = Yii::app()->clientScript;
            $s->registerScript($this->id . "_afterHook", $afterHook, CClientScript::POS_READY);
        }
        if ($this->readOnly) {
            foreach ($this->columns  as $key => $value) {
                if (isset($value['class']) && $value['class']=="MButtonColumn") {
                    $this->columns[$key]['template']="";
                }
            }
        }
        if ($this->dataProvider->getSort() !== false) {
            if ($this->filter instanceof ActiveRecord) {
                $this->dataProvider->sort->attributes = $this->filter->getSortAttributes();


            }
        }
        //$this->enableHistory=true;
        parent::init();
    }

    public function run()
    {
        if ($this->extended) {
            $countMax=CUserPreferences::getParam(CUserPreferences::DEFAULT_PRESET_COUNT_IN_GRIDVIEW);
            $this->ajaxUpdate = $this->id."-csv-download"; // ajaxUpdate mixed? myass mixed!

            $params = array_merge($_GET, array('downloadCSV' => '1'));
            echo("<div class='panel'>");
            $this->widget('ext.columnsPresetSelector.columnsPresetSelector', array(
                    'model' => $this->filter,
                    'visiblePresetCount' => $countMax
                )
            );

            if ($this->downloadCSV) {
                echo '<div id="'.$this->id.'-csv-download" class="indent-bot-15">'
                    . CHtml::link("Скачать CSV", Yii::app()->createUrl(Yii::app()->request->pathInfo, $params), array('target' => '_blank')) .
                    '</div>';
            } else {
                echo '<div id="'.$this->id.'-csv-download" class="indent-bot-15">&nbsp;</div>';
            }

            echo("</div>");
        }
        parent::run();
    }

    /**
     * Creates column objects and initializes them.
     */
    protected function initColumns()
    {
        if($this->columns===array())
        {
            if($this->dataProvider instanceof CActiveDataProvider)
                $this->columns=$this->dataProvider->model->attributeNames();
            elseif($this->dataProvider instanceof IDataProvider)
            {
                // use the keys of the first row of data as the default columns
                $data=$this->dataProvider->getData();
                if(isset($data[0]) && is_array($data[0]))
                    $this->columns=array_keys($data[0]);
            }
        }
        $id=$this->getId();
        foreach($this->columns as $i=>$column)
        {
            if(is_string($column))
                $column=$this->createDataColumn($column);
            else
            {
                if(!isset($column['class']))
                    $column['class']='MDataColumn';

                $column=Yii::createComponent($column, $this);


            }
            if(!$column->visible)
            {
                unset($this->columns[$i]);
                continue;
            }
            if($column->id===null)
                $column->id=$id.'_c'.$i;
            $this->columns[$i]=$column;
        }

        foreach($this->columns as $column)
            $column->init();
    }
}