<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sdd
 * Date: 9/2/13
 * Time: 11:58 PM
 * To change this template use File | Settings | File Templates.
 */

class CSearchableField
{
    public static function getSyntheticField($options, $model) {
        list($v, $fieldNames) = self::_getSearchableFieldGenerator($options, $model);
        return $v->getSyntheticField($fieldNames,$options);
    }

    public static function getSortForSearchableField($options, $model) {
        list($v, $fieldNames) = self::_getSearchableFieldGenerator($options, $model);
        return $v->getSortForSearchableField($fieldNames,$options);
    }

    public static function getRulesForSearchableField($options, $model) {
        list($v, $fieldNames) = self::_getSearchableFieldGenerator($options, $model);
        return $v->getRulesForSearchableField($fieldNames,$options);
    }

    public static function getLabelsForSearchableField($options, $model) {
        list($v, $fieldNames) = self::_getSearchableFieldGenerator($options, $model);

        return $v->getLabelsForSearchableField($fieldNames,$options);
    }


    public static function applySearchFieldsOnCriteria($criteria, $options, $model)
    {
        list($v, $fieldNames) = self::_getSearchableFieldGenerator($options, $model);
        $v->applyGenerator($criteria,$fieldNames,$options);
    }

    /**
     * @param $name
     * @return searchableFieldGeneratorBase
     */
    private static function _buildSearchableFieldGenerator($name, $model)
    {
        Yii::import("application.components.searchableField.*");
        switch ($name) {
            case "aggregate":
                return new searchableFieldGeneratorAggregate($model);
                break;
            case "exact":
                return new searchableFieldGeneratorExact($model);
                break;
            case "partial":
                return new searchableFieldGeneratorPartial($model);
                break;
            case "dummy":
                return new searchableFieldGeneratorDummy($model);
                break;
            case "relation":
            case "related":
                return new searchableFieldGeneratorRelation($model);
                break;
            default;
                return false;
        }
    }

    /**
     * @param $options
     * @param $model
     * @return array
     * @throws CException
     */
    public static function _getSearchableFieldGenerator($options, $model)
    {
        $v = self::_buildSearchableFieldGenerator($options[1], $model);

        if ($v === false) {
            throw new CException("no field found");
        }
        $fieldNames = explode(",", $options[0]);

        $fieldNames = array_map(function ($f) {
            return trim($f);
        }, $fieldNames);

        $fieldNames = array_filter($fieldNames, function ($f) {
            return $f!="";
        });
        return array($v, $fieldNames);
    }
}