<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sdd
 * Date: 9/1/13
 * Time: 12:33 AM
 * To change this template use File | Settings | File Templates.
 *
 * Model Interface to show MGrivView that the model provides customizable columns
 */

interface IProvidesCustomizableColumns {
    /**
     * @return ARCustomizableColumnsBase
     */
    public function getCustomizableColumns();
}