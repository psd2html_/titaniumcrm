<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sdd
 * Date: 9/3/13
 * Time: 12:17 AM
 * To change this template use File | Settings | File Templates.
 */

class searchableFieldGeneratorDummy extends searchableFieldGeneratorBase {

    public function applyGenerator($criteria, $fieldNames, $options)
    {
        if (isset($options['from'])) {
            $this->_processFrom($criteria, $options['from']);
        }
        // yeap!
    }

    /**
     * @param array $fieldNames
     * @param array $options
     * @return mixed
     */
    public function getSyntheticField($fieldNames, $options)
    {
        return null;
    }


    public function getRulesForSearchableField($fieldNames, $options)
    {
        return array(implode($fieldNames,', '), 'safe', 'on' => 'search');
    }

    public function getLabelsForSearchableField($fieldNames, $options)
    {
        return null;
    }

}