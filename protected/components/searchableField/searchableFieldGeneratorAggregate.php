<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sdd
 * Date: 9/2/13
 * Time: 11:44 PM
 * To change this template use File | Settings | File Templates.
 */

class searchableFieldGeneratorAggregate extends searchableFieldGeneratorBase
{

    /**
     * Applies this generator on Criteria
     * @param CDbCriteria $criteria
     * @param array $fieldNames
     * @param array $options
     * @return none
     */

    public function applyGenerator($criteria, $fieldNames, $options)
    {
        if (!isset($options['name'])) {
            throw new CException("Name of synthetic field is NOT set. Bad dog");
        }
        if (isset($options['from'])) {
            $this->_processFrom($criteria, $options['from']);
        }

        $sf = $options['name'];
        $val = $this->_model->$sf;
        $exact = isset($options['exact']) ? $options['exact'] : false;

        if ($val) {
            if ($val == self::SEARCH_VALUE_ISNULL) {
                $c = array_map(function ($f) use ($sf) {
                    return $this->addHomeTable($f) . " IS NULL OR ". $this->addHomeTable($f) . " = ''";
                }, $fieldNames);
            } else {
                if (!$exact) {
                    $criteria->params['ycs_' . $sf] = '%' . $val . '%';
                    $c = array_map(function ($f) use ($sf) {
                        return $this->addHomeTable($f) . " LIKE :ycs_" . $sf;
                    }, $fieldNames);
                } else {
                    $criteria->params['ycs_' . $sf] = $val;
                    $c = array_map(function ($f) use ($sf) {
                        return $this->addHomeTable($f) . " = :ycs_" . $sf;
                    }, $fieldNames);
                }
            }
            $criteria->addCondition(implode($c, " OR "));
        }
    }

    /**
     * Gets synthetic field to be added to collection
     * @param array $fieldNames
     * @param array $options
     * @return array() or null
     */
    public function getSyntheticField($fieldNames, $options)
    {
        if (!isset($options['name'])) {
            throw new CException("Name of synthetic field is NOT set. Bad dog");
        }
        $sf = $options['name'];
        return array($sf => null);
    }

    public function getRulesForSearchableField($fieldNames, $options)
    {
        if (!isset($options['name'])) {
            throw new CException("Name of synthetic field is NOT set. Bad dog");
        }
        $sf = $options['name'];

        return array($sf, 'safe', 'on' => 'search');
    }

    public function getLabelsForSearchableField($fieldNames, $options)
    {
        if (!isset($options['name'])) {
            throw new CException("Name of synthetic field is NOT set. Bad dog");
        }

        $sf = $options['name'];
        $sl = isset($options['label']) ? $options['label'] : $options['name'];

        return array($sf => $sl);
    }


}