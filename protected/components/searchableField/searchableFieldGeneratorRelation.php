<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sdd
 * Date: 9/3/13
 * Time: 12:17 AM
 * To change this template use File | Settings | File Templates.
 */

class searchableFieldGeneratorRelation extends searchableFieldGeneratorBase
{

    //  array('logisticRelated.idContragent', 'relation', 'exact' => true, 'from' => 'logisticRelated', 'name' => 'searchContragent', 'label' => 'Контрагент'),

    public function applyGenerator($criteria, $fieldNames, $options)
    {
        $this->_checkOptions($options);
        $this->_processFrom($criteria, $options['from']);

        $sf = $options['name'];

        $exact = isset($options['exact']) ? $options['exact'] : false;

        foreach ($fieldNames as $f) {
            if ($this->_model->$sf == self::SEARCH_VALUE_ISNULL) {
                $criteria->addCondition($f." IS NULL OR ". $f . " = ''");
            } else {
            $criteria->compare($f, $this->_model->$sf, !$exact);
            }
        }
    }

    private function _checkOptions($options)
    {
        if (!isset($options['name'])) throw new Exception("No name specified");
        if (!isset($options['from'])) throw new Exception("No from specified");
    }

    /**
     * @param array $fieldNames
     * @param array $options
     * @return mixed
     */
    public function getSyntheticField($fieldNames, $options)
    {
        $this->_checkOptions($options);

        $sf = $options['name'];
        return array($sf => null);
    }

    public function getSortForSearchableField($fieldNames, $options)
    {
        $this->_checkOptions($options);
        /* 'asc'=>'author.username',
           'desc'=>'author.username DESC',*/
        $sf = $options['name'];

        if (isset($options['sort']) && isset($options['sortDesc'])) {
            return array($sf => array('asc' => $options['sort'],
                                      'desc' => $options['sortDesc']));
        } else {
            if (!$fieldNames) return false;
            return array($sf => array('asc' => $fieldNames[0],
                'desc' => $fieldNames[0] . ' DESC'));
        }
    }

    public function getRulesForSearchableField($fieldNames, $options)
    {
        $this->_checkOptions($options);

        $sf = $options['name'];
        return array($sf, 'safe', 'on' => 'search');
    }

    public function getLabelsForSearchableField($fieldNames, $options)
    {
        $this->_checkOptions($options);
        $sf = $options['name'];
        $sl = isset($options['label']) ? $options['label'] : $options['name'];
        return array($sf => $sl);
    }


}