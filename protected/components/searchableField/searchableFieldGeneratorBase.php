<?php

abstract class searchableFieldGeneratorBase {
    const SEARCH_VALUE_ISNULL="!";
    protected $_model;

    /**
     * @param ActiveRecord $model
     */
    public function __construct($model)
    {
        $this->_model=$model;
    }

    /**
     * Applies this generator on Criteria
     * @param CDbCriteria $criteria
     * @param array $fieldNames
     * @param array $options
     * @return none
     */
    abstract public function applyGenerator($criteria, $fieldNames, $options);

    /**
     * Gets synthetic field to be added to collection
     * @param array $fieldNames
     * @param array $options
     * @return array() or null
     */

    abstract public function getSyntheticField($fieldNames, $options);
    abstract public function getRulesForSearchableField($fieldNames, $options);
    abstract public function getLabelsForSearchableField($fieldNames, $options);

    public function getSortForSearchableField($fieldNames, $options)
    {
        if (isset($options['name'])&&isset($options['sort']) && isset($options['sortDesc']))
        {
        $sf = $options['name'];

            return array($sf => array('asc' => $options['sort'],
                'desc' => $options['sortDesc']));
        } else {
            return null;
        }
    }

    protected function  addHomeTable($f)
    {
        if (strstr($f,'.')===false) return 't.'.$f;
        return $f;
    }
    /**
     * @param CDbCriteria $criteria
     * @param string $from
     */
    protected function _processFrom($criteria, $from)
    {
        $froms = explode(",", $from);

        if (!$criteria->with) {
            $criteria->with = array();
        }

        foreach ($froms as $from) {
            $w = trim($from);
            if (!in_array($w, $criteria->with)) {
                $criteria->with[] = $w;
            }
        }
    }
}