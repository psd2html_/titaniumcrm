<?php
class f1 extends CDbMigration
{
    public function safeUp()
    {
        $this->addColumn("company","fullNameEng","varchar(255)");
        $this->addColumn("company","physicalAddressEng","varchar(255)");
        $this->addColumn("project","takeoutAddress","text");
    }

    public function safeDown()
    {

    }
}