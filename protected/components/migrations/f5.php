<?php
class f5 extends CDbMigration
{
    public function safeUp()
    {
        //$this->alterColumn("project","sellIncotermsCondition","VARCHAR(255)");
        $this->addColumn("project","idContractUpload","INT(11)");
        $this->addColumn("logisticPosition","idPackingSheetUpload","int(11)");
        $this->addColumn("logisticPosition","idDescriptionUpload","int(11)");

        $this->addForeignKey("fk_p_contract","project","idContractUpload","upload","idUpload","SET NULL","SET NULL");
        $this->addForeignKey("fk_p_contract2","logisticPosition","idPackingSheetUpload","upload","idUpload","SET NULL","SET NULL");
        $this->addForeignKey("fk_p_contract3","logisticPosition","idDescriptionUpload","upload","idUpload","SET NULL","SET NULL");
    }

    public function safeDown()
    {

    }
}