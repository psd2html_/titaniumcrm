<?php
class f8 extends CDbMigration
{
    public function safeUp()
    {
//        $this->createTable("group", array(
//            'idGroup' => 'pk',
//            'name' => 'VARCHAR(255) NOT NULL',
//            'jsonEncodedRules' => 'text',
//            'system' => 'int(2) NOT NULL DEFAULT 1',
//        ));
//
//        $this->addColumn("group", "isLogist", "tinyint(1) NOT NULL DEFAULT 0");
//        $this->addColumn("group", "startingTab", "VARCHAR(30)");


        $this->addColumn("users", "idGroup", "INT(11)");

        $this->addForeignKey("fk_users_group", "users", "idGroup", "group", "idGroup", "SET NULL", "SET NULL");
    }

    public function safeDown()
    {

    }
}