<?php
class f6 extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable("atomicRbac", array(
            'idAtomicRbac' => 'pk',
            'enumRuleClass' => 'int(4) NOT NULL',
            'enumRuleValue' => 'int(4) NOT NULL',
            'idProject' => 'int(11)',
            'idUser' => 'int(11) NOT NULL',
        ));

        $this->addForeignKey("fk_rbac_project", "atomicRbac", "idProject", "project", "idProject", "CASCADE", "CASCADE");
        $this->addForeignKey("fk_rbac_user", "atomicRbac", "idUser", "users", "id", "CASCADE", "CASCADE"); // ??????????
        $this->alterColumn("project", "payedByUser", "VARCHAR(255) DEFAULT NULL");
        Yii::app()->db->createCommand("UPDATE project SET payedByUser=NULL WHERE payedByUser='Не оплачено'")->execute();
        $this->addColumn("sticky", "postCommitComment", "text");
        $this->addColumn("project", "virtualCoreInstrument", "varchar(255)");
    }

    public function safeDown()
    {

    }
}