<?php
class f10 extends CDbMigration
{
    public function safeUp()
    {
        //$this->alterColumn("project","sellIncotermsCondition","VARCHAR(255)");
        $this->addColumn("project","idRoomMeasurementUpload","INT(11)");
        $this->addColumn("project","idSetupReportUpload","INT(11)");
        $this->addColumn("project","setupComment","text");
        $this->addColumn("project","setupDate","date");

        $this->addForeignKey("fk_p_setup","project","idRoomMeasurementUpload","upload","idUpload","SET NULL","SET NULL");
        $this->addForeignKey("fk_p2_setup","project","idSetupReportUpload","upload","idUpload","SET NULL","SET NULL");

    }

    public function safeDown()
    {

    }
}