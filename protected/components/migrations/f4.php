<?php
class f4 extends CDbMigration
{
    public function safeUp()
    {
        $this->alterColumn("project","sellIncotermsCondition","VARCHAR(255)");
        $this->addColumn("project","payedByUser","VARCHAR(255) DEFAULT 'Не оплачено'");
        $this->addColumn("contest","costLotCurrency","int(4) DEFAULT 0");
        $this->addColumn("contest","costAskCurrency","int(4) DEFAULT 0");
        $this->addColumn("contest","costContractCurrency","int(4) DEFAULT 0");

        $this->addColumn("project","contractTotalCurrency","int(4) DEFAULT 0");
        $this->addColumn("project","logisticPositionCounter","int(4) DEFAULT 0");

        $this->createTable("logisticPosition",array(
            'idLogisticPosition' => 'pk',
            'innerNumber' => 'int(4)',

            'idInsuranceCompany' => 'int(11)',
            'idCoreInstrument' => 'int(11)',
            'idFeature' => 'int(11)',
            'idProject' => 'int(11)',

            'productionDeadline' => 'date',
            'poNumber' => 'string',
            'goodsNameRus' => 'string',
            'goodsOrigin' => 'string',
            'goodsNameEng' => 'string',

            'isPurchaseViaManufacturer' => 'int(4) DEFAULT 2',
            'isTechnoinfoUnloadsGoods' => 'int(4) DEFAULT 2',
            'isExportDeclarationMade' => 'int(4) DEFAULT 2',
            'isSpecialDeliveryRequired' => 'int(4) DEFAULT 2',
            'isExportDoubleApplicationLicenseRequired' => 'int(4) DEFAULT 2',
            'isInsurancePayed' => 'int(4) DEFAULT 2',

            'packagingSpecification' => 'text',
            'buyIncotermsCondition' => 'string',
            'sellIncotermsCondition' => 'string',
            'takeoutAddress'=>'text',
            'tnvedCod'=>'string',
        ));

        $this->addForeignKey("fk_lp_insurance","logisticPosition","idInsuranceCompany","insuranceCompany","idInsuranceCompany","CASCADE","CASCADE");
        $this->addForeignKey("fk_lp_feature","logisticPosition","idFeature","feature","idFeature","CASCADE","CASCADE");
        $this->addForeignKey("fk_lp_coreinstrument","logisticPosition","idCoreInstrument","coreinstrument","idCoreinstrument","CASCADE","CASCADE");
        $this->addForeignKey("fk_lp_project","logisticPosition","idProject","project","idProject","CASCADE","CASCADE");
    }

    public function safeDown()
    {

    }
}