<?php
class DocGenerator
{

    private $_order = null;
    private $_temp = array();

    public function __construct($order)
    {
        $this->_order = $order;
    }

    public function downloadDoc()
    {
        $order=$this->_order;
        $file_name = ($order->name . '.docx');
        $TBS = $this->makeTBS($order);
        $TBS->Show(OPENTBS_DOWNLOAD, $file_name);
        $this->_clearTemp();
    }
    public function saveDoc($file_name)
    {
        $order=$this->_order;
        $TBS = $this->makeTBS($order);
        $TBS->Show(OPENTBS_FILE, $file_name);
        $this->_clearTemp();
    }

    private function makeTBS($order)
    {
        $this->_importDependencies();

        $TBS = new clsTinyButStrong; // new instance of TBS
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN); // load OpenTBS plugin
        $debug = 0;

        if (!$order->coreInstrument) {
            $template = Yii::app()->getBasePath() . '/docs/new_template_v1_no_ci.docx';
        } else {
            $template = Yii::app()->getBasePath() . '/docs/new_template_v1.docx';
        }

        $x = pathinfo($template);

        $template_ext = $x['extension'];

        if (!file_exists($template))
            exit("File does not exist.");


        if ($debug == 2) { // debug mode 2
            $TBS->Plugin(OPENTBS_DEBUG_XML_CURRENT);
            exit;
        } elseif ($debug == 1) { // debug mode 1
            $TBS->Plugin(OPENTBS_DEBUG_INFO);
            exit;
        }
        $templateBlock_coreInstrument = array();
        $templateBlock_colspan = array();
//-------------------------------------------------------------------------------------------------
        // Load constants
        $conversion = $order->getMoneyConversionArray();
        $distinct = $order->getDistinctFeatures();
        $rusCurrenciesName = Order::getRusCurrencies();
        $rusCurrenciesName[Order::CURRENCY_EMPTY] = '';

        if ($order->coreInstrument) {
            //   $this->_addFeatureOrCoreInstrumentRow($order, $order->coreInstrument, $conversion, $rusCurrenciesName, $templateBlock_coreInstrument);
            array_unshift($distinct, $order->coreInstrument);
        }

//-------------------------------------------------------------------------------------------------
        // Get calculation

        list($sum, $exw, $cip, $ddp, $discount) = $order->calculateOrder();

//-------------------------------------------------------------------------------------------------
        //prepare vars
        $stuff = array();

        if ($order->coreInstrument)
            $stuff[] = $order->coreInstrument;

        $destinationCity = $order->destinationCity ? $order->destinationCity :
            $order->project->companyConsumer->city;

        $destinationCityGenetivus = $order->destinationCityGenetivus ? $order->destinationCityGenetivus :
            ($order->project->companyConsumer->cityGenetivus ? $order->project->companyConsumer->cityGenetivus : $order->project->companyConsumer->city);

        $typeOfContracts = $order->getCalculationAlgorithms();
        $typeOfContract = $typeOfContracts[$order->enumCalculationAlgorithm];

//-------------------------------------------------------------------------------------------------

        $sign = tempnam(sys_get_temp_dir(), "_sgn") . ".jpg";
        $coreinstrumentImage = tempnam(sys_get_temp_dir(), "_ci") . ".jpg";

        $this->_temp[] = $sign;

        copy($order->user->getRawThumbnail(), $sign);

        if ($order->isDeliverySpreadBetweenFeatures) {
            $order->showEXW = false;
        }

        if ($order->coreInstrument) {
            $this->_temp[] = $coreinstrumentImage;
            copy($order->coreInstrument->getRawThumbnail(), $coreinstrumentImage);
        }

        $templateSettings = array(
            array(
                'userSign' => $sign,
                'address' => $order->project->companyConsumer->physicalAddress,
                'phrase' => $order->manufacturer->motto,
                'coreinstrumentname' => $order->coreInstrument ? $order->coreInstrument->name : "-",
                'coreinstrumentImage' => $coreinstrumentImage,
                'currency' => $rusCurrenciesName[$order->outCurrency],
                'poName' => $order->name,
                'date' => Yii::app()->dateFormatter->formatDateTime($order->date, 'medium', null),
                'companyType' => $order->project->companyConsumer->companyClass->name,
                'company' => $order->project->companyConsumer->fullName,
                'person' => '-',
                'email' => '-',
                'phone' => '-',
                'italic' => $order->outCurrency != Order::CURRENCY_RUB ? '* При курсе не более ' . $conversion[$order->outCurrency] . ' руб. за 1 ' . $rusCurrenciesName[$order->outCurrency] : '',
                'noBold' => 0,
                'noColspan' => 0,
                'noRed' => 0,
                'noFooter' => 0,
                'showManufacturerImage' => 0,
                'manufacturerImage' => $order->manufacturer->getRawThumbnail(false, false),
                'userName' => $order->user->fullName,
                'personalFooter' => ((mb_ereg_replace("([\n\r])+", "", $order->user->personalFooter, "mp"))), //$this->prepareDocx2(mb_ereg_replace("([\n\r])+","",$order->user->personalFooter,"mp")),
                'personalFooter2' => ((mb_ereg_replace("([\n\r])+", "", $order->user->personalFooter2, "mp"))), //$this->prepareDocx2(mb_ereg_replace("([\n\r])+","",$order->user->personalFooter,"mp")),
                'personalFooter3' => ((mb_ereg_replace("([\n\r])+", "", $order->user->personalFooter3, "mp"))) //$this->prepareDocx2(mb_ereg_replace("([\n\r])+","",$order->user->personalFooter,"mp")),
            ));

        if ($m = $order->manufacturer->getRawThumbnail(false, true)) {
            // if word fucks up the Template,
            // embed this line into w:drawing
            // <w:r><w:t>[onshow;block=w:drawing;when [options.showManufacturerImage]=1]</w:t></w:r>

            $templateSettings[0]['manufacturerImage'] = $m;
            $templateSettings[0]['showManufacturerImage'] = 1;
        }


        if ($order->customer) {
            $templateSettings[0] ['person'] = $order->customer->lastName . ' ' . $order->customer->firstName . ' ' . $order->customer->middleName;
            $phones = array($order->customer->phone, $order->customer->phone2, $order->customer->mobilePhone);
            $pp = array();
            foreach ($phones as $phone) {
                if (($phone)) {
                    $pp[] = $phone;
                }
            }
            if (($order->customer->email)) {
                $templateSettings[0] ['email'] = $order->customer->email;
            }
            if ($pp) {
                $templateSettings[0] ['phone'] = implode($pp, ', ');
            }
        }

        if ($order->cipPayment != 0) {
            $deliveryPriceText = "";
            $deliveryPrice = $this->_normalizePrice($order->cipPayment, $order->cipCurrency, $order->outCurrency, $conversion);
            if ($order->isDeliverySpreadBetweenFeatures) {
                foreach ($distinct as $feature) {
                    $percentage = $this->_normalizePrice($feature->price, $feature->enumCurrency, $order->outCurrency, $conversion) * $feature->amount
                        / $exw;
                    $feature->addToPrice = $deliveryPrice * $percentage;
                }

            } else {

                if (!$order->showEXW and !$order->showCIP and $order->isFeaturePriceShown) {
                    $deliveryPrice = $this->_normalizePrice($order->cipPayment * $order->ddpCoeff, $order->cipCurrency, $order->outCurrency, $conversion);
                }

                $deliveryPriceText = $order->isFeaturePriceShown ?
                    $this->formatPrice(
                        $deliveryPrice, Order::CURRENCY_EMPTY, $rusCurrenciesName) : "";

            }

            $templateBlock_colspan = array(array(
                'text' => 'Страховка и доставка до ' . $destinationCityGenetivus,
                'amount' => '1',
                'delivery' => $deliveryPriceText
            ));
        }

        $templateBlock_redLines = array();

        if ($order->enumPricePolicy == Order::PRICEPOLICY_SALE) {
            $templateBlock_redLines = array(
                array(
                    'text' => 'Скидка для ' . $order->project->companyConsumer->fullName . ', ' . ($order->saleCoeff) . '%',
                    'minus' => '-' . $this->formatPrice($discount, $order->outCurrency, $rusCurrenciesName),
                )
            );
        } else if ($order->enumPricePolicy == Order::PRICEPOLICY_OVERRIDENPRICE) {
            if ($discount > 0) {
//                $redLines = array(
//                    array(
//                        'text' => 'Сумма для ' . $order->project->companyConsumer->fullName,
//                        'minus' => $this->formatPrice($order->overridenPrice, $order->outCurrency, $c),
//                    )
//                );
            } else {
                $templateBlock_redLines = array(
                    array(
                        'text' => 'Скидка для ' . $order->project->companyConsumer->fullName . ', ' . (number_format(-100 * $discount / (-$discount + $sum), 2)) . '%',
                        'minus' => '-' . $this->formatPrice($discount, $order->outCurrency, $rusCurrenciesName),
                    )
                );
            }
        }

        $templateBlock_footer = array();
        $templateBlock_bold = array();

        if (($order->enumCalculationAlgorithm > Order::ALGORITHM_EXW) && ($order->showEXW)) {
            $templateBlock_footer[] = array('text' => 'Цена товаров и услуг, EXW ' . $destinationCity,
                'sum' => $this->formatPrice($exw, $order->outCurrency, $rusCurrenciesName));
        }

        if (($order->enumCalculationAlgorithm > Order::ALGORITHM_CIP) && ($order->showCIP)) {
            $templateBlock_footer[] = array('text' => 'Цена товаров и услуг, CIP ' . $destinationCity,
                'sum' => $this->formatPrice($cip, $order->outCurrency, $rusCurrenciesName));
        }
        if (($order->enumCalculationAlgorithm > Order::ALGORITHM_DDP ||
                ($order->enumCalculationAlgorithm == Order::ALGORITHM_DDP &&
                    ($order->enumPricePolicy == Order::PRICEPOLICY_SALE ||
                        (($order->enumPricePolicy == Order::PRICEPOLICY_OVERRIDENPRICE) && ($discount < 0))
                    )
                ))
            &&
            ($order->showDDP)
        ) {
            $templateBlock_footer[] = array('text' => 'Цена товаров и услуг, DDP ' . $destinationCity,
                'sum' => $this->formatPrice($ddp, $order->outCurrency, $rusCurrenciesName));
        }

        $templateBlock_bold[] = array('text' => 'ИТОГО товаров и услуг, ' . $typeOfContract . ' ' . $destinationCity . ':',
            'sum' => $this->formatPrice($sum, $order->outCurrency, $rusCurrenciesName));

        $TBS->SetOption('charset', 'UTF-8');
        $TBS->LoadTemplate($template);
        $TBS->SetOption('charset', 'UTF-8');

        $templateBlock_featuresTable = array();
        $multiplyCoefficient = 1;

        if (!$order->showEXW and !$order->showCIP and $order->isFeaturePriceShown) {
            $multiplyCoefficient = $order->ddpCoeff;
        }

        foreach ($distinct as $feature) {
            $this->_addFeatureOrCoreInstrumentRow($order, $feature, $conversion, $rusCurrenciesName, $multiplyCoefficient, $templateBlock_featuresTable);
        }

        /* Условия договора - вынести в модель */

        $arguments = array();
        if ($order->doThreeMonth) {
            $arguments[] = array('name' => 'Данное предложение действительно три месяца.');
        } else {
            $arguments[] = array('name' => 'Данное предложение действительно до ' . Yii::app()->dateFormatter->formatDateTime($order->lastDate, 'medium', null) . '.');
        }

        if ($order->deliver) {
            $arguments[] = array('name' => 'Доставка: ' . $order->deliver . ' мес. ');
        }

        if ($order->guaranteeInMonth) {
            $oo = 'Гарантия: ' . $order->guaranteeInMonth . ' мес.';

            if ($order->guaranteeLessThan) {
                $oo .= ', но не более ' . $order->guaranteeLessThan . ' мес.';
            }

            $arguments[] = array('name' => $oo);
        }

        $arguments[] = array('name' => 'Условия поставки: ' . $typeOfContract . ' ' . ($order->project->companyConsumer->city) . '.');

        $currencyLiteralFull = Order::getRusCurrenciesLong();
        $arguments[] = array('name' => 'Валюта платежа: ' . mb_strtolower($currencyLiteralFull[$order->outCurrency], 'utf8') . '.');

        $pc = Order::getPaymentConditionList();
        if (isset($pc[$order->enumPaymentCondition])) {
            $arguments[] = array('name' => 'Условия платежа: ' . mb_strtolower($pc[$order->enumPaymentCondition], 'utf8') . '.');
        }

        if (isset($templateBlock_featuresTable))
            $TBS->MergeBlock('a', ($templateBlock_featuresTable));

        if (!isset($templateBlock_colspan)) {
            $templateBlock_colspan = array(array(
                'text' => 'Стоимость доставки',
                'amount' => '',
                'delivery' => ''));
        }


        $TBS->MergeBlock('options', ($templateSettings));
        if (isset($templateBlock_redLines))
            $TBS->MergeBlock('red', ($templateBlock_redLines));
        if (isset($templateBlock_footer))
            $TBS->MergeBlock('footer', ($templateBlock_footer));
        if (isset($templateBlock_colspan))
            $TBS->MergeBlock('colspan', ($templateBlock_colspan));
        if (isset($templateBlock_bold))
            $TBS->MergeBlock('bold', ($templateBlock_bold));


        $TBS->MergeBlock('arguments', $arguments);
        $TBS->MergeBlock('coreinstrument', $templateBlock_coreInstrument);
        $TBS->MergeBlock('feature', $templateBlock_featuresTable);

        $TBS->LoadTemplate('#word/footer1.xml');
        $TBS->MergeBlock('options', ($templateSettings));

        return $TBS;
    }

    private function _importDependencies()
    {
        Yii::import('application.vendors.*');

        require_once 'tbs/tbs_plugin_opentbs.php';
        require_once 'tbs/tbs_class.php';
        require_once 'simplehtmldom/simple_html_dom.php';
        require_once 'phpword/PHPWord/Writer/Word2007/WriterPart.php';
        require_once 'phpword/PHPWord/Writer/Word2007/Base.php';
        require_once 'phpword/PHPWord/Writer/Word2007/Section.php';
        require_once 'phpword/PHPWord/Section.php';
        require_once 'phpword/PHPWord/DocumentProperties.php';
        require_once 'phpword/PHPWord/Style.php';
        require_once 'phpword/PHPWord/Media.php';
        require_once 'phpword/PHPWord/TOC.php';

        $this->_includeAllFilesFromFolder('phpword/PHPWord/Section');
        $this->_includeAllFilesFromFolder('phpword/PHPWord/Shared');
        $this->_includeAllFilesFromFolder('phpword/PHPWord/Style');

        require_once 'phpword/PHPWord.php';
        require_once 'htmltodocx_converter/h2d_htmlconverter.php';
    }

    public function _includeAllFilesFromFolder($root)
    {

        $d = new RecursiveDirectoryIterator(Yii::app()->getBasePath() . '/vendors/' . $root);
        foreach (new RecursiveIteratorIterator($d) as $file => $f) {
            $ext = pathinfo($f, PATHINFO_EXTENSION);
            if ($ext == 'php' || $ext == 'inc') {
                require_once($file); // or require(), require_once(), include_once()
            }
        }
    }

    /**
     * @param $value
     * @param $from
     * @param $to
     * @param $curr
     * @return float
     */
    private function _normalizePrice($val, $from, $to, $curr)
    {
        return $val * $curr[$from] / $curr[$to];
    }

    private function formatPrice($val, $curr, $a)
    {
        return number_format($val, 2, '.', '`') . ' ' . $a[$curr];
    }

    /**
     * @param $order
     * @param $feature
     * @param $conversion
     * @param $c
     * @param $options
     */

    private function _addFeatureOrCoreInstrumentRow($order, $feature, $conversion, $c, $multiply, &$options)
    {
        $price = $feature->isPriceSettled ?
            $this->formatPrice(
                $multiply * (
                    $feature->addToPrice +
                    $this->_normalizePrice($feature->price, $feature->enumCurrency, $order->outCurrency, $conversion) * $feature->amount)
                , Order::CURRENCY_EMPTY, $c
            ) : '';

        if (!$order->isFeaturePriceShown) {
            $price = '';
        }

        if ($order->isRussianDescriptionLoaded) {
            $name = $feature->ruName ? $feature->ruName : $feature->name;
            $description = strip_tags($feature->ruDescription) ? $feature->ruDescription : $feature->description;
        } else {
            $name = $feature->name;
            $description = $feature->description;
        }

        $options[] = array('SID' => $feature->sid, 'name' => $name, 'amount' => $feature->amount,
            'price' => $price,
            'description' => $this->prepareDocx2($description));
    }

    private function prepareDocx2($raw)
    {
        $html = new simple_html_dom();

        // delete empty <p>

        $raw = tidy_repair_string(
            $raw,
            array(
                'show-body-only' => true,
                'doctype' => '-//W3C//DTD XHTML 1.0 Transitional//EN',
                'output-xhtml' => true
            ), 'utf8'

        );

        $raw = preg_replace("/<p>([\r\n\s])*(&nbsp;)+([\r\n\s])*<\/p>/umxs", "", $raw);

        $raw = '<p>' . $raw . '</p>';
        $html->load($raw);

        $child = $html->childNodes();
        $section = new PHPWord_Section(1, array());
        $setup = array();

        $htmltodocx_insert_html_recursive = htmltodocx_insert_html($section, $child);
        $docWriter = new PHPWord_Writer_Word2007_Section();
        $baked = $docWriter->writeSection($section);
        $baked = str_replace('<w:p/>', "", $baked);
        //var_dump($baked);

        return ('</w:t></w:r></w:p>' . $baked . '<w:p><w:r><w:rPr></w:rPr><w:t>');
    }

    private function _clearTemp()
    {

        foreach ($this->_temp as $t) {
            unlink($t);
        }

        $this->_temp = [];
    }

}