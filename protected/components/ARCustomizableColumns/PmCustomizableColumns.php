<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sdd
 * Date: 9/2/13
 * Time: 12:12 AM
 * To change this template use File | Settings | File Templates.
 */


class PmCustomizableColumns extends ARCustomizableColumnsBase
{
    /* consts */

    public function getDefaultColumns()
    {
        return array(0, 1, 2,3);
    }

    /**
     * @return CActiveRecord
     */
    protected function _getCorrespondingModel()
    {
        return new Pm();
    }

    /**
     * @return array
     */
    protected function _getAllColumns()
    {

        $columns = array(
            0 => array(
                'name' => 'created',
                'value' => 'Yii::app()->dateFormatter->formatDateTime($data->created)',
            ),
            1 => array(
                'name'=>'subject'
            ),
            2 => array(
                'name' => 'body',
            ),
            3 => array(
                'name' => 'idProject',
                'value'=>'$data->idProject?$data->project->getNameUrl():"-"'
            ),
       );
        return $columns;
    }
}