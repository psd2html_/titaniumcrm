<?php

class CustomerCustomizableColumns extends ARCustomizableColumnsBase
{
    /* consts */


    const COLUMN_NAME = 0;
    const COLUMN_COMPANY = 1;
    const COLUMN_JOB = 2;
    const COLUMN_PHONES = 3;
    const COLUMN_EMAILS = 4;
    const COLUMN_SKYPE = 5;
    const COLUMN_ADDRESS = 6;
    const COLUMN_COMMENT = 7;
    const COLUMN_CITY = 8;
    const COLUMN_FIRST_NAME = 9;
    const COLUMN_LAST_NAME = 10;
    const COLUMN_MIDDLE_NAME = 11;
    const COLUMN_CORE_INSTRUMENT_CLASS = 12;
    const COLUMN_PREVIEW = 13;

    public function getDefaultColumns()
    {
        return array(0, 1, 2);
    }

    /**
     * @return CActiveRecord
     */
    protected function _getCorrespondingModel()
    {
        return new Customer();
    }

    /**
     * @return array
     */
    protected function _getAllColumns()
    {

        $columns = array(
            self::COLUMN_PREVIEW=> array(
                'name'=>'previewColumn',
                'value'=>'$data->previewColumn()',
                'type'=>'raw',
                'filter'=>False
            ),
            self::COLUMN_FIRST_NAME => array(
                'name' => 'firstName',
            ),
            self::COLUMN_LAST_NAME => array(
                'name' => 'lastName',
            ),
            self::COLUMN_MIDDLE_NAME=> array(
                'name' => 'middleName',
            ),
            self::COLUMN_COMMENT => array(
                'name' => 'comment',
                'type' => 'html'
            ),
            self::COLUMN_ADDRESS => array(
                'name' => 'address',
                'type' => 'html'
            ),
            self::COLUMN_NAME => array(
                'name' => 'searchFullName',
                'value' => '$data->getBothNames()',
                'header' => 'ФИО',
            ),
            self::COLUMN_COMPANY => array(
                'name' => 'companyFilter',
                'value' => '$data->company?($data->company->nameUrl):("Не определена")',
                'type' => 'html'
            ),
            self::COLUMN_JOB =>
            array(
                'name' => 'job',
            ),
            self::COLUMN_PHONES =>
            array(
                'name' => 'searchPhones',
                'value' => '$data->getPhones()',
                'type' => 'raw'
            ),
            self::COLUMN_EMAILS =>
            array(
                'name' => 'searchEmails',
                'value' => '$data->getEmails()',
                'type' => 'raw'
            ),
            self::COLUMN_SKYPE =>
            array(
                'name' => 'skype',
                'type' => 'raw'
            ),
            self::COLUMN_CITY => array(
                'name' => 'companyCity',
                'value' => '$data->company?$data->company->city:"Нет компании"',
                'type' => 'raw',
                'filterWidget' => 'clickableFilter'
            ),
            self::COLUMN_CORE_INSTRUMENT_CLASS => array(
                'name' => 'searchCoreInstrumentClass',
                'value' => '$data->searchCoreInstrumentClassColumn()',
                'filter'=>CHtml::listData(CoreInstrumentClass::model()->findAll(),"idCoreInstrumentClass","name"),
                'type' => 'raw',
                //'filterWidget' => 'clickableFilter'
            )
        );
        return $columns;
    }
}