<?php

class SampleCustomizableColumns extends ARCustomizableColumnsBase
{
    /* consts */


    const COLUMNS_NAME = 0;
    const COLUMNS_CENTRE = 1;
    const COLUMNS_STATUS = 2;
    const COLUMNS_PROJECT = 3;
    const COLUMNS_COMMENT = 4;
    const COLUMNS_RECEIVED_DATE = 5;
    const COLUMNS_MANUFACTURER = 6;
    const COLUMNS_COREINSTRUMENT = 7;
    const COLUMNS_TITLE = 8;
    const COLUMNS_TASK = 9;
    const COLUMNS_LOCATION = 10;
    const COLUMNS_LASTCHANGE = 11;
    const COLUMNS_INNERNAME = 12;

    public function getDefaultColumns()
    {
        return array(0, 1, 2);
    }

    /**
     * @return CActiveRecord
     */
    protected function _getCorrespondingModel()
    {
        return new Sample();
    }

    /**
     * @return array
     */
    protected function _getAllColumns()
    {

        $columns = array(
            self::COLUMNS_TASK =>
            array(
                'name' => 'task'),

            self::COLUMNS_INNERNAME =>
            array(
                'name' => 'innerName'),

            self::COLUMNS_TITLE =>
            array(
                'name' => 'title'),
            self::COLUMNS_LOCATION =>
            array(
                'name' => 'location'),
            self::COLUMNS_LASTCHANGE =>
            array(
                'name' => 'lastChanged'),
            self::COLUMNS_NAME =>
            array(
                'name' => 'name',
                'filter' => ''),
            self::COLUMNS_CENTRE =>
            array(
                'name' => 'idCentre',
                'value' => "\$data->idCentre?\$data->relatedCentre->name:'N/A'",
                'filter' => CHtml::listData(Centre::model()->findAll(), "idCentre", "name"),
            ),
            self::COLUMNS_STATUS =>
            array(
                'name' => 'enumStatus',
                'value' => '$data->status',
                'filter' => Sample::getEnumStatusList(),
            ),
            self::COLUMNS_PROJECT =>
            array(
                'name' => 'idProject',
                'value' => 'isset($data->project->name)?$data->project->nameUrl:"N/A"',
                'type' => 'html',
                'filter' => "",
            ),
            self::COLUMNS_COMMENT =>
            array('name' => 'comment', 'filter'=> ""),

            self::COLUMNS_RECEIVED_DATE =>
            array('name' => 'recievedDate',
                'value' => 'Yii::app()->dateFormatter->formatDateTime($data->recievedDate,"long",null)',
                'filter'=> ""),

            self::COLUMNS_MANUFACTURER =>
            array('name' => 'idManufacturer',
                'value' => '$data->manufacturer?$data->manufacturer->name:"N/A"',
                'filter' => CHtml::listData(Manufacturer::model()->findAll(), "idManufacturer", "name"),
            ),
            self::COLUMNS_COREINSTRUMENT =>
            array('name' => 'searchCoreInstrumentClass',
                'value' => '$data->coreInstrument?$data->coreInstrument->name:"N/A"',
                'filter' => CHtml::listData(CoreInstrumentClass::model()->findAll(), "idCoreInstrumentClass", "name"),),

        );
        return $columns;
    }
}