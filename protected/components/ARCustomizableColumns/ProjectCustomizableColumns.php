<?php

class ProjectCustomizableColumns extends ARCustomizableColumnsBase
{
    /* consts */

    const COLUMN_NAME = 0;
    const COLUMN_COREINSTRUMENTCLASS = 1;
    const COLUMN_MANUFACTURER = 2;
    const COLUMN_COREINSTRUMENT = 3;
    const COLUMN_COMPANY = 4;
    const COLUMN_TOWN = 5;
    const COLUMN_RESPONSIBLE = 6;
    const COLUMN_WORKFLOW = 7;
    const COLUMN_EVENTCLOSEST = 8;
    const COLUMN_INNERNAME = 9;
    const COLUMN_COMMENT = 10;
    const COLUMN_LASTACTION = 11;
    const COLUMN_CONTRAGENT = 12;
    const COLUMN_YEARQUARTAL = 13;
    const COLUMN_STATUS = 14;
    const COLUMN_DEADLINE_BUILD = 15;
    const COLUMN_DEADLINE_DELIVER = 16;
    const COLUMN_LASTACTION_DETAILS = 17;
    // ---------  newly added  ---------------
    const COLUMN_COMPANY_BUYER = 18;
    const COLUMN_COMPANYCLASS = 19;
    const COLUMN_CLOSESTTASK = 20;
    const COLUMN_CONTRACT_PRICE = 21;
    const COLUMN_CONTEST_DEADLINE = 22;
    const COLUMN_CONTRACT_DATE = 23;
    const COLUMN_CONTRACT_NUMBER = 24;
    const COLUMN_SAMPLES_STATUS = 25;
    const COLUMN_INSURANCE = 26;
    const COLUMN_ORDERS = 27;
    const COLUMN_CONTRACT_PROVISION = 28;
    const COLUMN_CREATED = 29;
    const COLUMN_CUSTOMERS = 30;
    const COLUMN_PO_NUMBER = 31;
    const COLUMN_PRIORITY = 32;
    const COLUMN_YEAR = 33;
    const COLUMN_QUARTAL = 34;
    const COLUMN_COMPANY_FULL_ENGLISH_NAME = 35;
    const COLUMN_COMPANY_FULL_ENGLISH_ADDRESS = 36;
    const COLUMN_ENG_PREVIEW = 37;

    public function getDefaultColumns()
    {
        return array(0, 1);
    }

    /**
     * @return CActiveRecord
     */
    protected function _getCorrespondingModel()
    {
        return new Project();
    }

    /**
     * @return array
     */
    protected function _getAllColumns()
    {
        $statusFilter=0;

        if ($this->context=="active-projects") {
            $statusFilter=1;
        } elseif ($this->context=="closed-projects") {
            $statusFilter=2;
        }

        $dateFilter = "";
        $hierarchyImg = CHtml::image("/images/hierarchy.gif", "Содержит связаные проекты");
        $slaveImg = CHtml::image("/images/Chains.gif", "Присоединенный проект");
        $fireImg = CHtml::image("/images/Fire.png", "Повышенный приоритет");

        $naming =
            '$data->childrenProjects?\'' . $hierarchyImg . '\'.$data->name:($data->parent?\'' . $slaveImg . '\'.$data->name:$data->name)';

        $columns = array(
            self::COLUMN_ENG_PREVIEW=> array(
                'name'=>'engPreview',
                'value'=>'$data->engPreviewColumn()',
                'type'=>'raw',
                'filter'=>False
            ),
            self::COLUMN_COMPANY_FULL_ENGLISH_NAME=> array(
                'name'=>'companyFullEnglishName',
                'value'=>'$data->companyConsumer?$data->companyConsumer->fullNameEng:"-"'
            ),
            self::COLUMN_COMPANY_FULL_ENGLISH_ADDRESS=> array(
                'name'=>'companyFullEnglishAddress',
                'value'=>'$data->companyConsumer?$data->companyConsumer->physicalAddressEng:"-"'
            ),
            self::COLUMN_PO_NUMBER => array(
                    'name'=>'poNumber'
                ),
            self::COLUMN_PRIORITY => array(
                    'name'=>'priority',
                    'filter'=>Project::getPriorityList(),
                    'value'=>'$data->getPriorityColumn()',
                    //'type'=>'html',
                ),
            self::COLUMN_CUSTOMERS => array(
                'name'=>'customersColumn',
             //   'header'=>'Конечный ползователь',
                'value'=>'$data->getCustomersColumn()',
                'filter' => '',
                'type'=>'html'
            ),
            self::COLUMN_COMPANY_BUYER => array(
                'name' => 'searchCompanyBuyerByName',
                'header'=>'Компания-поставщик',
                'value' => '$data->companyBuyer?$data->companyBuyer->nameUrl:"Отсутствует"',
                'type'=>'html'
            ),
            self::COLUMN_COMPANYCLASS => array(
                'name' => 'searchCompanyClass',
                'header'=>'Тип организации',
                'value' => '$data->companyConsumer?$data->companyConsumer->companyClass->name:"Неизвестно"',
                'filter' => CHtml::listData(CompanyClass::model()->findAll(),"idCompanyClass","name"),
            ),
            self::COLUMN_CLOSESTTASK => array(
                'name' => 'closestTask',
                'header' => 'Ближайшая задача',
                'value' => '$data->closestTask2?$data->closestTask2->getTaskHtmlFormatted():"Неизвестно"',
                'filter' => '',
                'type'=>'html'
            ),
            self::COLUMN_CONTRACT_PRICE => array(
                'name' => 'contractTotal',
                'value' => '$data->contractTotal? $data->contractTotalFormatted :"Неизвестно"',
                'filter' => '',
            ),
            self::COLUMN_CONTRACT_PROVISION => array(
                'name' => 'enumProvision',
                'value' => '$data->provision',
                'filter' => Project::getEnumProvisionList(),
            ),
            self::COLUMN_CONTEST_DEADLINE => array(
                'name' => 'contestDeadline',
                'header' => 'Заявка - deadline',
                'value' => '$data->contest?($data->contest->dateRequest?Yii::app()->dateFormatter->formatDateTime($data->contest->dateRequest,"medium",null):"Неизвестно"):"Неизвестно"',
                'filter' => '',
            ),

            self::COLUMN_CONTRACT_DATE => array(
                'name' => 'contractDate',
                'value' => '$data->contractDate?Yii::app()->dateFormatter->formatDateTime($data->contractDate,"medium",null):"Неизвестно"',
                'filter' => '',
            ),
            self::COLUMN_CREATED => array(
                'name' => 'created',
                'value' => 'Yii::app()->dateFormatter->formatDateTime($data->created,"medium",null)',
                'filter' => '',
            ),

            self::COLUMN_CONTRACT_NUMBER => array(
                'name' => 'contractNumber',
                'value' => '$data->contractNumber?$data->contractNumber:"Неизвестно"',
            ),
            self::COLUMN_SAMPLES_STATUS => array(
                'name' => 'earliestSampleStatus',
                'value' => '$data->getSamplesStatusColumn()',
                'filter' => '',
                'type' => 'html'
            ),
            self::COLUMN_ORDERS => array(
                'name' => 'countOfOrders',
                'value' => '$data->getOrdersColumn()',
                'filter' => array("0"=>"Нет КП",">0"=>'Есть КП'),
                'type' => 'raw'
            ),
            self::COLUMN_INSURANCE => array(
                'name' => 'isInsurancePayed',
                'value' => 'Project::getFuzzyBooleanList($data->isInsurancePayed)',
                'filter' => Project::getFuzzyBooleanList(),
            ),
            // ----------------------------------------
            self::COLUMN_LASTACTION_DETAILS => array(
                'name' => 'lastActionLog',
                'value' => '$data->lastActionLog?$data->lastActionLog->description:"Неизвестно"',
                'filter' => '',
            ),

            self::COLUMN_DEADLINE_BUILD => array(
                'name' => 'productionDeadline',
                'value' => '$data->productionDeadline?Yii::app()->dateFormatter->formatDateTime($data->productionDeadline,"medium",null):"Неизвестно"',
                'filter' => $dateFilter,
            ),

            self::COLUMN_DEADLINE_DELIVER => array(
                'name' => 'shipmentDeadline',
                'value' => '$data->shipmentDeadline?Yii::app()->dateFormatter->formatDateTime($data->shipmentDeadline,"medium",null):"Неизвестно"',
                'filter' => '',
            ),
//            self::COLUMN_WORKFLOW => array(
//                'name' => 'enumStatus',
//                'value' => '$data->getStatus()',
//                'filter' => Project::getEnumStatusList(),
//            ),
//
            self::COLUMN_STATUS => array(
                'name' => 'enumStatus',
                'value' => '$data->getStatusBox()." ".$data->getStatus()',
                'filter' => Project::getEnumStatusList(false,$statusFilter),
                'type'=>'html'
            ),
            self::COLUMN_YEAR => array(
                'name' => 'year',
                'value' => '$data->yearQuartal?$data->year:"Неизвестно"',
            ),
            self::COLUMN_QUARTAL => array(
                'name' => 'quartal',
                'value' => '$data->yearQuartal?$data->quartal:"Неизвестно"',
            ),

//            self::COLUMN_YEARQUARTAL => array(
//                'name' => 'searchYear',
//                'header' => 'Год/квартал',
//                'value' => '$data->yearQuartal?$data->yearQuartal:"Неизвестно"',
//            ),

            self::COLUMN_CONTRAGENT => array(
                'name' => 'searchContragent',
                'value' => '($data->logistic&&$data->logistic->idContragent)?$data->logistic->contragent->name:"Неизвестно"',
                'filter' => CHtml::listData(Contragent::model()->findAll(), 'idContragent', 'name')
            ),
            self::COLUMN_LASTACTION => array(
                'name' => 'lastChange',
                'value' => '$data->lastChange>0?Yii::app()->dateFormatter->formatDateTime($data->lastChange):"Неизвестно"'
            ),

            self::COLUMN_COMMENT => array(
                'name' => 'comment',
                'value' => '$data->commentColumn',
                'type' => 'html',
                'filter' => "",
            ),
            self::COLUMN_INNERNAME => array(
                'name' => 'innerName',
            ),
//            self::COLUMN_EVENTCLOSEST => array(
//                'name' => 'eventClosest',
//                'value' => '$data->getClosestEventOrSticky()?$data->getClosestEventOrSticky()->nameUrl :"Нет ближайших событий"',
//                'type' => 'html',
//                'filter' => "",
//            ),
            self::COLUMN_NAME => array(
                'name' => 'projectName',
                'value' => 'CHtml::link($data->getNameColumn(),$data->getViewUrl())',
                'type' => 'html',
                //'filter' => "",
            ),
            self::COLUMN_COREINSTRUMENTCLASS =>
            array('name' => 'idCoreInstrumentClass',
                'value' => '$data->coreInstrumentClass->name',
                'filter' => CHtml::listData(CoreInstrumentClass::model()->findAll(), "idCoreInstrumentClass", "name"),
            ),

            self::COLUMN_MANUFACTURER =>
            array(
                'name' => 'searchManufacturer',
                'value' => '$data->manufacturer?$data->manufacturer->name:"?"',
                'type' => 'raw',
                'filterWidget' => 'clickableFilter'
            ),

            self::COLUMN_COREINSTRUMENT =>
            array(
                'name' => 'searchCoreInstrument',
                'value' => '$data->coreInstrument?$data->coreInstrument->name:($data->virtualCoreInstrument?$data->virtualCoreInstrument:"Не определен")',
                'type' => 'raw',
                'filterWidget' => 'clickableFilter'
            ),
            self::COLUMN_COMPANY =>
            array(
                'name' => 'searchCompanyConsumer',
                'value' => '$data->companyConsumer?$data->companyConsumer->getFullNameUrl():"N/A"',
                'type' => 'html'
            ),
            self::COLUMN_TOWN =>
            array(
                'name' => 'searchTownCompany',
                'value' => '$data->companyConsumer?$data->companyConsumer->city:"N/A"',
                'type' => 'raw',
                'filterWidget' => 'clickableFilter'
            ),
            self::COLUMN_RESPONSIBLE =>
            array(
                'name' => 'idResponsible',
                'value' => array('$data->idResponsible'=>'$data->responsible->fullname'),
                'filter' => CHtml::listData(User::model()->findAll(), "id", "fullname"),
                'filterWidget' => 'clickableKeyValueFilter'
            ),
          );
        return $columns;
    }
}