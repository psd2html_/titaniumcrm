<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sdd
 * Date: 9/2/13
 * Time: 12:12 AM
 * To change this template use File | Settings | File Templates.
 */


class CompanyCustomizableColumns extends ARCustomizableColumnsBase
{
    /* consts */


    const COLUMN_NAME = 0;
    const COLUMN_NAME_SHORT = 1;
    const COLUMN_NAME_ENG = 4;
    const COLUMN_ADDRESS = 5;
    const COLUMN_ADDRESS_ENG = 6;
    const COLUMN_CITY = 3;
    const COLUMN_COMPANYCLASS = 2;

    public function getDefaultColumns()
    {
        return array(0, 1, 2,3);
    }

    /**
     * @return CActiveRecord
     */
    protected function _getCorrespondingModel()
    {
        return new Company();
    }

    /**
     * @return array
     */
    protected function _getAllColumns()
    {

        $columns = array(

            self::COLUMN_NAME => array(
                'name' => 'fullName',
            ),
            self::COLUMN_NAME_ENG => array(
                'name' => 'fullNameEng',
            ),
            self::COLUMN_ADDRESS=> array(
                'name' => 'physicalAddress',
            ),
            self::COLUMN_ADDRESS_ENG => array(
                'name' => 'physicalAddressEng',
            ),

            self::COLUMN_NAME_SHORT => array(
                'name' => 'shortName',
            ),
            self::COLUMN_COMPANYCLASS =>
            array('name' => 'idCompanyClass',
                'value' => '$data->companyClass->name',
                'filter' => CHtml::listData(CompanyClass::model()->findAll(), "idCompanyClass", "name"),
            ),
            self::COLUMN_CITY =>
            array(
                'name' => 'city',
            ),
        );
        return $columns;
    }
}