<?php
// base abstract class for ARCustomizableColumns

abstract class ARCustomizableColumnsBase
{

    public $context="";

    /** Returns columns to CGridView based on filter
     * @param $filter
     * @return array
     */

    public function getColumns($filter)
    {

        $columns = $this->_getAllColumns();
        $filtered = array();

        if (!is_array($filter)) {
            $filter=array();
        }

        foreach ($filter as $id) {

            if (array_key_exists($id,$columns)) {
                $filtered[] = $columns[$id];
            }
        }

        $filtered[] = $this->_getButtonColumn();

        return $filtered;
    }

    /**
     * @return array
     */
    abstract protected function _getAllColumns();

    protected function  _getButtonColumn()
    {
        $m=strtolower(get_class($this->_getCorrespondingModel()));

        return array('class' => 'MButtonColumn',
            'template' => '{view}',
            'filterCell' => "",
            'header' => '<a href="'. Yii::app()->createUrl("personal/customizeColumns", array('gid'=>$m,'returnUrl' => Yii::app()->request->getRequestUri())) .'" class="button secondary small" onclick="window.dwDialog.loadAjax($(this).attr(\'href\'), true);
            return false;">'.
            '<img src="/images/cog.png" alt="Настроить столбцы" title="Настроить столбцы" />'.'</a>',
            'viewButtonUrl' => $this->_getEditAction(),
            'buttons' => array(
                'view' => array(
                    'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                    'options' => array('class' => 'button secondary tiny view')
                ),
            ),
        );
    }

    /**
     * Override this to change edit button action
     * @return string
     */

    protected function _getEditAction()
    {
        $model = $this->_getCorrespondingModel();
        if ($model instanceof ISelfManagedCrudUrl) {
            return '$data->getViewUrl(array("returnUrl"=>"' . Yii::app()->request->getRequestUri() . '"))';
        } else
        return 'Yii::app()->createUrl("' . strtolower(get_class($model)) . '/view", array("id" => $data->getPrimaryKey(),"returnUrl"=>"' . Yii::app()->request->getRequestUri() . '"))';
    }

    /**
     * @return CActiveRecord
     */

    abstract protected function _getCorrespondingModel();

    public function getAllColumnsDataList()
    {
        $model = $this->_getCorrespondingModel();
        $columns = $this->_getAllColumns();
        $out = array();
        ksort($columns);
        if (is_array($columns)) {
            foreach ($columns as $key => $column) {
                $out[$key] = $model->getAttributeLabel($column['name']);
            }
        } else
            $out = array();

        return $out;
    }

    /**
     * @return array
     */
    abstract public function getDefaultColumns();
}