<?php
/*
 * Usage 
 * 'filter' => array(
                        'class' => 'ApacheLikeLog',                        
                    ),
 */
class ApacheLikeLog extends CLogFilter {

    public function filter(&$logs) {
        $shouldTrim = array('exception.CHttpException.404',
            'exception.CHttpException.403',
            'exception.CHttpException.400'        );
        // unset categories marked as "ignored"
        if ($logs)
            for ($i = 0; $i < count($logs); $i++) {
                if (in_array($logs[$i][2], $shouldTrim)) {
                    //$logs[$i][0] = preg_replace('/Stack trace:\n(#\d+.*?\n)+/us', '', $logs[$i][0]);
                    //'logVars' => array('$_SERVER[HTTP_USER_AGENT]','REMOTE_ADDR','REMOTE_HOST','HTTP_REFERER','HTTP_USER_AGENT'),
                    $logs[$i][0] = sprintf('%s | %s | %s | %s', $_SERVER['REQUEST_URI'], $_SERVER['REMOTE_ADDR'], isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '-', $_SERVER['HTTP_USER_AGENT']
                    );
                }
            }

        $this->format($logs);

        return $logs;
    }

}