<?php

class MGridViewDataProvider
{
    public $context = false;
    private $_controller;

    public function __construct($controller)
    {
        $this->_controller = $controller;
    }

    public function renderMGridView($mGridViewId, $masterModel, $arguments = false, $toVar = false, $context = false, $toCsv=false)
    {
        return $this->_processNode($mGridViewId, $masterModel, $arguments, $toVar, $context, false);
    }

    public function renderMGridViewReadOnlyMode($mGridViewId, $masterModel, $arguments = false, $toVar = false, $context = false, $toCsv=false)
    {
        return $this->_processNode($mGridViewId, $masterModel, $arguments, $toVar, $context, false, true);
    }

    private function _processNode($mGridViewId, $masterModel, $arguments = false, $toVar = false, $context = false, $overrideView=false, $readOnlyMode=false)
    {
        $config = self::_getConfig($mGridViewId);

        if ($this->context !== false)
            self::_applyContext($config, $this->context);

        if ($context !== false)
            self::_applyContext($config, $context);

        if ($overrideView) {
            $config['partialView']=$overrideView;
        }

        $modelTag = $config['modelTag'];
        $attributes = $config['attributes'];
        $attributesFromMasterModel = $config['attributesFromMasterModel'];
        $defaultOrder = $config['defaultOrder'];
        $partialView = $config['partialView'];

        $model = self::_getModelByTag($modelTag);
        $model->unsetAttributes();


        if ($model instanceof IProvidesCustomizableColumns) {
            $modelName = strtolower(get_class($model));
            list($filterStatus, $forceFilter) = CustomizableColumnsProvider::getFilters($modelName);
            if ($filterStatus == CustomizableColumnsProvider::FILTER_LOCKING_ENABLED) {
                //gracefully merge
                $userFilter=Yii::app()->getRequest()->getQuery($modelTag);

                foreach ($forceFilter as $key=>$value)
                {
                    if ($value) {
                        $userFilter[$key]=$value;
                    }
                }

                $model->setAttributes($userFilter);
            } else {
                if ($filterStatus == CustomizableColumnsProvider::FILTER_LOCKING_IMPRINT) {
                    $imprintFilter = Yii::app()->getRequest()->getQuery($modelTag);
                    CustomizableColumnsProvider::saveFilters($modelName, $imprintFilter);
                }
                $model->setAttributes(Yii::app()->getRequest()->getQuery($modelTag));
             }
        } else {

            $model->setAttributes(Yii::app()->getRequest()->getQuery($modelTag));
        }

        $this->_applyAttributes($masterModel, $attributes, $attributesFromMasterModel, $model);

        $dataProvider = $model->search();
        $this->_applyDefaultOrder($defaultOrder, $dataProvider);
        $args = array('dataProvider' => $dataProvider, 'model' => $model, 'gridViewId' => $mGridViewId, 'context'=>$context);

        if ($arguments !== false && is_array($arguments)) {
            $args += $arguments;
        }

        $args['readOnly']=$readOnlyMode;


        if ($toVar)
            return $this->_controller->renderPartial($partialView, $args, $toVar);
        else
            $this->_controller->renderPartial($partialView, $args, $toVar);

        return true;
    }
    const GRID_TEMPLATE_WITH_PRESETS_AND_CSV="/shared/_fullGrid";
    const GRID_TEMPLATE_WITH_PRESETS="/shared/_extGrid";
    const GRID_TEMPLATE_PLAIN="/shared/_genericGrid";

    const GRID_TEMPLATE_CSV_COMMA="/shared/_csvCommaGrid";
    const GRID_TEMPLATE_CSV_SEMICOLON="/shared/_csvSemicolonGrid";

    private static function _getConfig($tag)
    {
        /*
         * - Config -
         * 'modelTag' - model to resolve
         * 'contexts' - keyvalue map, with overriding some elements
         * 'defaultOrder'
         * 'partialView'
         * 'attributes' - directly set filter attributes
         * 'attributesFromMasterModel' - derived attributed
         *
         * ['parent'] = set parent config with some overrides
         */
        $a = array(
            'admin-samples-grid' => array(
                'modelTag' => 'Sample',
                'contexts' => array(),
                'attributes' => array(),
                'attributesFromMasterModel' => array(),
                'defaultOrder' => false,
                'partialView' => self::GRID_TEMPLATE_WITH_PRESETS,
            ),
            'unread-pm-grid' => array(
                'modelTag' => 'Pm',
                'contexts' => array(),
                'attributes' => array('status' =>'<=1','idRecipient' => Yii::app()->user->id),
                'attributesFromMasterModel' => array(),
                'defaultOrder' => "status ASC, created DESC",
                'partialView' => '/personal/_pmGrid',
            ),
            'archived-pm-grid' => array(
                'modelTag' => 'Pm',
                'contexts' => array(),
                'attributes' => array('status'=>'2','idRecipient' => Yii::app()->user->id),
                'attributesFromMasterModel' => array(),
                'defaultOrder' => "status ASC, created DESC",
                'partialView' => '/personal/_pmGrid',
            ),
            // default project admin view
            'project-grid-admin' => array(
                'modelTag' => 'Project',
                'contexts' => array(
                    'active-projects'=>array('attributes' => array('searchModificatorOnlyPersonalProjects'=>True,'activeProjectFilter'=>1)),
                    'closed-projects'=>array('attributes' => array('searchModificatorOnlyPersonalProjects'=>True, 'activeProjectFilter'=>2)),
                    'all-projects'=>array(),
                ),
                'attributes' => array(),
                'attributesFromMasterModel' => array(),
                'defaultOrder' => CUserPreferences::getParam(CUserPreferences::PROJECT_IMPORTANCE_ORDERING)? "t.priority DESC, t.lastChange DESC" : "t.lastChange DESC",
                'partialView' => self::GRID_TEMPLATE_WITH_PRESETS_AND_CSV,
            ),
            'project-grid' => array(
                'modelTag' => 'Project',
                'contexts' => array(
                    'customer' => array(
                        'attributesFromMasterModel' => array('searchIdCustomer' => 'idCustomer'),
                    ),
                    'manufacturer' => array(
                       // 'attributesFromMasterModel' => array('idManufacturer' => 'idManufacturer'),
                        'attributesFromMasterModel' => array(),
                        'attributes'=>array('idManufacturer'=>1),
                    ),
                    'linked-projects' => array (
                        'attributesFromMasterModel' => array('searchAllLinkedProjects'=>'idProject')
                    ),
                    'linked-projects-slave' => array (
                        'attributesFromMasterModel' => array('searchAllLinkedProjects'=>'parent')
                    )
                ),
                'attributes' => array(),
                'attributesFromMasterModel' => array('searchIdCompany' => 'idCompany'),
                'defaultOrder' => false,
                'partialView' => self::GRID_TEMPLATE_WITH_PRESETS,
            ),
            'company-view-project-grid' => array(
                'modelTag' => 'Project',
                'contexts' => array(),
                'attributes' => array('searchModificatorSkipUglySql'=>True),
                'attributesFromMasterModel' => array('searchIdCompany' => 'idCompany'),
                'defaultOrder' => false,
                'partialView' => self::GRID_TEMPLATE_WITH_PRESETS,
            ),
            'project-grid-manufacturer' => array(
                'modelTag' => 'Project',
                'contexts' => array(),
                'attributes' => array(),
                'attributesFromMasterModel' => array('idManufacturer' => 'idManufacturer'),
                'defaultOrder' => false,
                'partialView' => self::GRID_TEMPLATE_PLAIN,
            ),

            'customer-grid' => array(
                'modelTag' => 'Customer',
                'contexts' => array(),
                'attributes' => array(),
                'attributesFromMasterModel' => array('idCompany' => 'idCompany'),
                'defaultOrder' => false,
                'partialView' => self::GRID_TEMPLATE_WITH_PRESETS_AND_CSV,
            ),
            'customer-admin' => array(
                'modelTag' => 'Customer',
                'contexts' => array(),
                'attributes' => array(),
                'attributesFromMasterModel' => array(),
                'defaultOrder' => false,
                'partialView' => self::GRID_TEMPLATE_WITH_PRESETS_AND_CSV,
            ),
            'sticky-grid' => array(
                'modelTag' => 'Sticky',
                'contexts' => array(),
                'attributes' => array('isTask' => 1, 'isDone'=>'0'),
                'attributesFromMasterModel' => array('idProject' => 'idProject'),
                'defaultOrder' => ('date DESC'),
                'partialView' => '/project/mgridViewTemplates/_notes',
            ),
            'sticky-grid-past' => array(
                'modelTag' => 'Sticky',
                'contexts' => array(),
                'attributes' => array('isTask' => 1, 'isDone'=>'1'),
                'attributesFromMasterModel' => array('idProject' => 'idProject'),
                'defaultOrder' => ('date DESC'),
                'partialView' => '/personal/_sticky',
            ),
            'atomic-project-permissions' => array(
                'modelTag' => 'AtomicRbac',
                'contexts' => array(),
                'attributes' => array('enumRuleClass' => AtomicRbac::RBAC_PROJECT),
                'attributesFromMasterModel' => array('idProject' => 'idProject'),
                'defaultOrder' => false,
                'partialView' => '/project/mgridViewTemplates/_atomicRbac',
            ),
            'tomeactive-sticky-grid' => array(
                'parent' => 'sticky-grid',
                'attributes' => array('isTask' => 1, 'isDone' => 0, 'idResponsible' => Yii::app()->user->id),
                'attributesFromMasterModel' => array(),
                'defaultOrder' => 'calendarDate ASC',
                'partialView' => '/personal/_sticky',
            ),
            'bymeactive-sticky-grid' => array(
                'parent' => 'sticky-grid',
                'attributes' => array('isTask' => 1, 'isDone' => 0, 'notAssignedToAuthor' => true, 'idUser' => Yii::app()->user->id),
                'attributesFromMasterModel' => array(),
                'defaultOrder' => 'calendarDate ASC',
                'partialView' => '/personal/_sticky',
            ),
            'done-sticky-grid' => array(
                'parent' => 'sticky-grid',
                'attributes' => array('isTask' => 1, 'isDone' => 1, 'searchUser' => Yii::app()->user->id),
                'attributesFromMasterModel' => array(),
                'defaultOrder' => 'calendarDate ASC',
                'partialView' => '/personal/_sticky',
            ),

            'lf-grid' => array(
                'modelTag' => 'LogisticFragment',
                'contexts' => array(),
                'attributes' => array(),
                'attributesFromMasterModel' => array('idProject' => 'idProject'),
                'defaultOrder' => "idLogisticFragment DESC",
                'partialView' => '/project/mgridViewTemplates/_logisticFragment',
            ),
            // brief project view, logistic tab
            'logisticPosition-brief-grid' => array(
                'modelTag' => 'LogisticPosition',
                'contexts' => array(),
                'attributes' => array(),
                'attributesFromMasterModel' => array('idProject' => 'idProject'),
                'defaultOrder' => "innerNumber ASC",
                'partialView' => '/project/mgridViewTemplates/_logisticPosition',
            ),
            'log-grid' => array(
                'modelTag' => 'ActiveRecordLog',
                'contexts' => array(),
                'attributes' => array('model' => 'project'),
                'attributesFromMasterModel' => array('idModel' => 'idProject'),
                'defaultOrder' => "creationDate DESC",
                'partialView' => '/project/mgridViewTemplates/_log',
            ),
            'samples-grid' => array(
                'modelTag' => 'Sample',
                'contexts' => array(),
                'attributes' => array(),
                'attributesFromMasterModel' => array('idProject' => 'idProject'),
                'defaultOrder' => false,
                'partialView' =>  self::GRID_TEMPLATE_PLAIN,
            ),
            'spent-grid' => array(
                'modelTag' => 'Spent',
                'contexts' => array(),
                'attributes' => array(),
                'attributesFromMasterModel' => array('idProject' => 'idProject'),
                'defaultOrder' => false,
                'partialView' => '/project/mgridViewTemplates/_spent',
            ),
        );

        if (array_key_exists($tag, $a)) {
            $config = $a[$tag];
            if (array_key_exists("parent", $config)) {
                $parent = $a[$config['parent']];
                foreach ($config as $key => $value) {
                    if ($key != 'parent') {
                        $parent[$key] = $value;
                    }
                }

                return $parent;
            } else

                return $config;
        }
        throw new CException("Can't resolve config for grid-view named " . $tag);
    }

    private function _applyContext(&$config, $context)
    {
        if ($context === false) return;
        if (!array_key_exists($context, $config['contexts'])) throw new CException("Context $context does not extist");
        foreach ($config['contexts'][$context] as $key => $value) {
            if ($key != 'contexts') {
                $config[$key] = $value;
            }
        }


    }

    private static function _getModelByTag($tag)
    {
        switch ($tag) {
            case "Spent":
                return new Spent('search');
            case "Sample":
                return new Sample('search');
            case "ActiveRecordLog":
                return new ActiveRecordLog('search');
            case "LogisticFragment":
                return new LogisticFragment('search');
            case "Sticky":
                return new Sticky('search');
            case "Project":
                return new Project('search');
            case "Pm":
                return new Pm('search');
            case "Customer":
                return new Customer('search');
            case "LogisticPosition":
                return new LogisticPosition('search');
            case "AtomicRbac":
                return new AtomicRbac('search');
            default:
                throw new CException("Can't resolve model " . $tag);
        }
    }

    /**
     * @param $masterModel
     * @param $attributes
     * @param $attributesFromMasterModel
     * @param $model
     */
    private function _applyAttributes($masterModel, $attributes, $attributesFromMasterModel, $model)
    {
        //$model->setAttributes($attributes);
        foreach ($attributes as $attributeToSet => $value) {
            $model->$attributeToSet = $value;
        }

        foreach ($attributesFromMasterModel as $attributeToSet => $attributeToGetFromMasterModel) {

            $model->$attributeToSet = $masterModel->getAttribute($attributeToGetFromMasterModel);
        }
    }

    /**
     * @param $defaultOrder
     * @param $dataProvider
     */
    private function _applyDefaultOrder($defaultOrder, $dataProvider)
    {
        if ($defaultOrder !== false) {
            $dataProvider->sort->defaultOrder = $defaultOrder;
        }
    }

    /**
     *
     * @param array() $mGridViewIds
     * @param ActiveRecord $masterModel
     */

    public function renderPartialGridViewIfRequired($mGridViewIds, $masterModel)
    {
        if (Yii::app()->request->isAjaxRequest) {
            $gridViewIdToRefresh = Yii::app()->getRequest()->getQuery('ajax', false);
            if (in_array($gridViewIdToRefresh, $mGridViewIds)) {
                $this->_processNode($gridViewIdToRefresh, $masterModel);
                return true;
            }
        }
        return false;
    }

    /**
     * Renders CSV
     * @param $gridClassId
     * @param $masterModel
     * @param $context
     * @param $csvSeparator
     * @return bool
     */
    public function renderCSV($gridClassId, $masterModel, $context, $csvSeparator)
    {
        return $this->_processNode($gridClassId, $masterModel, false, false, $context, $csvSeparator==","?self::GRID_TEMPLATE_CSV_COMMA:self::GRID_TEMPLATE_CSV_SEMICOLON);
    }


}