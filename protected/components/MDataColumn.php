<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sdd
 * Date: 11/14/13
 * Time: 9:29 PM
 * To change this template use File | Settings | File Templates.
 */
class MDataColumn extends CDataColumn {

    public $isLocked=false;
    /**
     * Renders the header cell content.
     * This method will render a link that can trigger the sorting if the column is sortable.
     */
    protected function renderHeaderCellContent()
    {
        if($this->name!==null)
        {
            $name=$this->header===null?$this->name:$this->header;

            if($this->grid->dataProvider instanceof CActiveDataProvider)
            {
                $name=$this->grid->dataProvider->model->getAttributeLabel($this->name);
            }

            if($this->grid->enableSorting && $this->sortable && $name!==null)
                echo $this->grid->dataProvider->getSort()->link($this->name,$name,array('class'=>'sort-link'));
            else
                echo CHtml::encode($name);
        }
        else
            parent::renderHeaderCellContent();
    }

    /**
     * Renders the filter cell content.
     * This method will render the {@link filter} as is if it is a string.
     * If {@link filter} is an array, it is assumed to be a list of options, and a dropdown selector will be rendered.
     * Otherwise if {@link filter} is not false, a text field is rendered.
     * @since 1.1.1
     */
    protected function renderFilterCellContent()
    {

        if(is_string($this->filter))
            echo $this->filter;
        elseif($this->filter!==false && $this->grid->filter!==null && $this->name!==null && strpos($this->name,'.')===false)
        {
            if(is_array($this->filter)) {
                if ($this->isLocked)
                    echo CHtml::activeDropDownList($this->grid->filter, $this->name, $this->filter, array('id'=>false,'prompt'=>'','disabled'=>'disabled'));
                else
                   echo CHtml::activeDropDownList($this->grid->filter, $this->name, $this->filter, array('id'=>false,'prompt'=>''));
            }
            elseif($this->filter===null)
            {
                if ($this->isLocked)
                    echo CHtml::activeTextField($this->grid->filter, $this->name, array('id'=>false,'disabled'=>'disabled'));
                else
                    echo CHtml::activeTextField($this->grid->filter, $this->name, array('id'=>false));
            }
        }
        else
            parent::renderFilterCellContent();
    }
}