<?php
class PersonalMessageService
{
    public static function archiveSelectedPm($array) {
        if(!is_array($array)) $array=array($array);
        $currentUser = Yii::app()->user->id;

        foreach ($array as $id)
        {
            Yii::app()->db->createCommand("UPDATE pm SET status=".Pm::PM_ARCHIVED." WHERE idRecipient=$currentUser and idPm=:id")
                ->bindParam("id",$id)
                ->execute();
        }
    }

    public static function deleteSelectedPm($array) {
        if(!is_array($array)) $array=array($array);
        $currentUser = Yii::app()->user->id;

        foreach ($array as $id)
        {
            Yii::app()->db->createCommand("DELETE FROM pm WHERE idRecipient=$currentUser and idPm=:id")
                ->bindParam("id",$id)
                ->execute();
        }
    }

    public static function deleteAllPm() {
        $currentUser = Yii::app()->user->id;
        Yii::app()->db->createCommand("DELETE FROM pm WHERE idRecipient=$currentUser")->execute();
    }

    public static function markAllAsRead() {
        $currentUser = Yii::app()->user->id;
        Yii::app()->db->createCommand("UPDATE pm SET status=1 WHERE idRecipient=$currentUser")->execute();
    }

    public static function getMessage($id)
    {
        $m = Pm::model()->findByPk($id);
        if (!$m) return false;
        if ($m->idRecipient != Yii::app()->user->id) {
            return false;
        }
        return $m;
    }

    public static function getActiveDataProviderForMessages($page = 0)
    {
        $currentUser = Yii::app()->user->id;

        $c = new CDbCriteria();
        $c->compare("idRecipient", $currentUser);
        $a = new CActiveDataProvider("Pm", array('condition' => $c, 'pagination' => array('pageSize' => 10, 'currentPage' => $page,
            'sort' => array('orderBy' => 'status ASC, created DESC'))));
        return $a;
    }

    public static function getAmountOfUnreadMessages()
    {
        if (Yii::app()->user->isGuest) return 0;
        $currentUser = Yii::app()->user->id;
        $u = Yii::app()->db->createCommand("SELECT COUNT(1) FROM pm WHERE idRecipient=$currentUser AND status=" . Pm::PM_UNREAD)->queryColumn();
        return $u[0];
    }
    /**
     * @param Project $project
     */
    public static function notifyAboutAssignedProject($project) {
        $currentUser=Yii::app()->user->user();
        $body="Вы - ответственный по проекту ". $project->nameUrl;
        self::notifyUser($currentUser->id,$project->idResponsible,Pm::SUBJECT_PROJECT_ASSIGNED,$body,$project->idProject);
    }

    /**
     * @param Project $project
     */
    public static function notifyAboutCoassignedProject($project, $user) {
        $currentUser=Yii::app()->user->user();
        $body="Вы - добавлены в ответственные по проекту ". $project->nameUrl;
        self::notifyUser($currentUser->id,$user->id,Pm::SUBJECT_PROJECT_ASSIGNED,$body,$project->idProject);
    }

    /**
     * @param Sticky $task
     */
    public static function notifyAboutFinishedTask($task) {
        $currentUser=Yii::app()->user->user();
        if (strip_tags($task->postCommitComment)) {
            $body=$task->nameUrl." выполнено. Комментарий:".strip_tags($task->postCommitComment);
        } else {
            $body=$task->nameUrl." выполнено.";
        }
        self::notifyUser($currentUser->id,$task->idUser,Pm::SUBJECT_TASK_DONE,$body,$task->idProject);
    }

    public static function notifyAboutNewTask($task) {
        $body= $task->nameUrl." к сроку ".$task->calendarDate;

        self::notifyUser($task->idUser,$task->idResponsible,Pm::SUBJECT_NEW_TASK_ASSIGNED,$body,$task->idProject);
    }

    public static function notifyUser($author, $recipient, $subject, $body,$project)
    {
        $m=new Pm();

        $m->idUser=$author;
        $m->idRecipient=$recipient;
        $m->status=Pm::PM_UNREAD;
        $m->subject=$subject;
        $m->body=$body;
        $m->idProject=$project;

        $m->save();

    }

    public static function notifyAboutLogisticRelatedStatusOfProject($project)
    {
        $logists=User::getAllLogists();

        $currentUser=Yii::app()->user->user();
        $statuses=Project::getEnumStatusList(true);

        $body="Проект ". $project->nameUrl." перешел на стадию ".$statuses[$project->enumStatus];
        foreach ($logists as $logist) {
            self::notifyUser($currentUser->id,$logist->id,Pm::SUBJECT_PROJECT_LOGISTIC_STATUS,$body,$project->idProject);
        }
    }


}