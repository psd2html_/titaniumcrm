<?php

/**
 * This is the model class for table "event".
 *
 * The followings are the available columns in table 'event':
 * @property integer $idEvent
 * @property integer $idProject
 * @property integer $enumType
 * @property integer $eventClass
 * @property integer $title
 * @property date $date
 * @property string $comment
 * @property integer $idUser
 *
 * The followings are the available model relations:
 * @property Project $project
 * @property Users $user
 * @property Attachment[] $attachments
 */
class Event extends ActiveRecord
{

    const TASK_EVENT = 3;
    const CHECKPOINT_EVENT = 2;
    const LOGISTIC_EVENT = 1;
    const PROJECT_EVENT = 0;
    public $attachment1, $attachment2, $attachment3;

    public static function getEventClassList()
    {
        return array(
            self::PROJECT_EVENT => 'Общение с заказчиком',
            self::LOGISTIC_EVENT => 'Финансы и логистика',
            self::CHECKPOINT_EVENT => 'Выполнение контрольных точек',
            self::TASK_EVENT => 'Задачи',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Event the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    function behaviors()
    {
        return array(
            'loggable' => array(
                'class' => 'ActiveRecordLoggable',
                'logCreation' => true,
                'shouldBubbleToMaster' => true
            )
        );
    }

    public function beforeValidate()
    {

        return parent::beforeValidate();
    }

    public function getName()
    {
        $out = $this->getType();

        return $out;
    }

    public function getType($addTitle = true)
    {
        $array = $this->getEnumTypeList();
        $out = $array[$this->enumType];
        $this->title=CHtml::encode(strip_tags($this->title));
        if (($this->title) && $addTitle) {

            if ($this->eventClass == self::PROJECT_EVENT) {
                $img = self::getIconForType($this->enumType);
                if ($img) $img=CHtml::image($img,$out);
                $out= $img.$this->title ;
            } else {
                $out .= ' (' . $this->title . ')';
            }
        }
        return $out;
    }

    public function getEnumTypeList()
    {
        if ($this->eventClass === null) {
            return $this->getFilterListBox();
        }
        $out = self::getAllEnumTypesList();


        return $out[$this->eventClass];
    }

    public static function getFilterListBox()
    {
        $out = self::getAllEnumTypesList();
        $out2 = array('Общение с заказчиком' => $out[self::PROJECT_EVENT],
            'Логистика и финансы' => $out[self::LOGISTIC_EVENT],
        );
        return $out2;
    }

    public static function getAllEnumTypesList()
    {
        return array(
            self::TASK_EVENT => array(10 => 'Персональное задание', 11 => 'Задание выполено'
            ),
            self::CHECKPOINT_EVENT => array(9 => 'Выполнено'),
            self::LOGISTIC_EVENT => array(5 => 'Движение',
                6 => 'Финансирование',
                7 => 'Страховка',
                8 => 'Прочее',
            ),
            self::PROJECT_EVENT => array(0 => 'Встреча',
                1 => 'Телефонный звонок',
                2 => 'Email',
                3 => 'Экскурсия на завод',
                4 => 'Прочее',
            ),
        );
    }

    public static function getIconForType($type)
    {
        /*0 => 'Встреча',
                1 => 'Телефонный звонок',
                2 => 'Email',
                3 => 'Экскурсия на завод',
                4 => 'Прочее',*/
        $k =
            array(
                0 => '/images/actions/meeting.png',
                1 => '/images/actions/call.png',
                2 => '/images/actions/email.png',
                3 => '/images/actions/factory.png',
            );
        return (isset($k[$type])) ? $k[$type] : null;


    }

    public function getCustomClass()
    {
        $ts = strtotime($this->date);

        if ($ts >= strtotime("midnight", time()))
            return 'newRow';
        return null;
    }

    public function getTitle()
    {
        $array = $this->getEnumTypeList();
        return $array[$this->enumType] . ' ' . Yii::app()->dateFormatter->formatDateTime($this->date, 'medium', null);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'event';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('idProject, date, enumType', 'required'),
            array('attachment1, attachment2, attachment3', 'file', 'types' => Yii::app()->params['allowedFileTypes'], 'allowEmpty' => 'true'),
            array('idProject, enumType, idUser', 'numerical', 'integerOnly' => true),
            array('searchEnumClass, idCheckpoint, comment, title, eventClass, enumType, idUser', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idEvent, idProject, eventClass, enumType, comment, idUser', 'safe', 'on' => 'search'),
        );
    }

    public function beforeDelete()
    {

        if (Yii::app()->user->isGuest) {
            return false;
        }
        if (Yii::app()->user->isAdmin()) {
            return true;
        }

        if (!$this->project->isSubzoneWritable(Group::SUBZONE_PROJECT_TASK)) {
            $this->addError('', 'У вас нет необходимых прав для совершения данного действия');
            return false;
        }

        return parent::beforeDelete();
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'project' => array(self::BELONGS_TO, 'Project', 'idProject'),
            'user' => array(self::BELONGS_TO, 'User', 'idUser'),
            'checkpoint' => array(self::BELONGS_TO, 'Checkpoint', 'idCheckpoint'),
            'attachments' => array(self::MANY_MANY, 'Attachment', 'attachment2event(idEvent, idAttachment)'),
            'attachmentsCount' => array(self::STAT, 'Attachment', 'attachment2event(idEvent, idAttachment)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'title' => 'Заголовок',
            'idEvent' => 'Id Event',
            'idProject' => 'Проект',
            'comment' => 'Комментарий',
            'idUser' => 'Сотрудник',
            'Title' => 'Заголовок',
            'attachments' => 'Прикрепить файлы',
            'date' => 'Дата',
            'enumType' => 'Тип',
            'attachmentsCount' => 'Число файлов',
            'eventClass' => 'События'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search($sort = null, $pagination = null)
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idEvent', $this->idEvent);
        $criteria->compare('idProject', $this->idProject);
        $criteria->compare('eventClass', $this->eventClass);
        $criteria->compare('enumType', $this->enumType);

        $criteria->compare('date', $this->date);

        $criteria->compare('comment', $this->comment, true);
        $criteria->compare('idUser', $this->idUser);

        if ($sort) {
            $criteria->order = $sort;
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => $pagination,
            'sort' => array(
                'defaultOrder' => 'date DESC',
            )
        ));
    }

}