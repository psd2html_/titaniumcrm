<?php

/**
 * This is the model class for table 'supplier2project'.
 *
 * The followings are the available columns in table 'supplier2project':
 * @property integer $idSupplier
* @property integer $idProject
                                 
 */
class supplier2project extends ActiveRecord
{
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'supplier2project';
    }

    public function rules() {
        return array(
            array('idSupplier, idProject', 'required'),
            array('idSupplier, idProject', 'numeric'),            
            array('idSupplier, idProject', 'safe', 'on'=>'search'),
        );
    }
    
    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idSupplier' => 'idSupplier',
'idProject' => 'idProject',

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idSupplier',$this->idSupplier);
$criteria->compare('idProject',$this->idProject);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
