<?php

/**
 * This is the model class for table 'customer2project'.
 *
 * The followings are the available columns in table 'customer2project':
 * @property integer $idProject
* @property integer $idCustomer
                                 
 */
class customer2project extends ActiveRecord
{
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'customer2project';
    }

    public function rules() {
        return array(
            array('idProject, idCustomer', 'required'),
            array('idProject, idCustomer', 'numeric'),            
            array('idProject, idCustomer', 'safe', 'on'=>'search'),
        );
    }
    
    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idProject' => 'idProject',
'idCustomer' => 'idCustomer',

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idProject',$this->idProject);
$criteria->compare('idCustomer',$this->idCustomer);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
