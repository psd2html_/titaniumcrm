<?php

/**
 * This is the model class for table "coreinstrument".
 *
 * The followings are the available columns in table 'coreinstrument':
 * @property integer $idCoreInstrument
 * @property integer $idCoreInstrumentClass
 * @property integer $idManufacturer
 * @property string $name
 * @property string $sid
 * @property string $description
 *
 * todo Не определено свойство "CoreInstrument.ruDescription"
 * @property string $ruDescription
 *
 * todo Не определено свойство "CoreInstrument.ruName".
 * @property string $ruName
 *
 * The followings are the available model relations:
 * @property Ownage[] $ownages
 */
class CoreInstrument extends ActiveRecord
{

    const HAS_DESCRIPTION = 2;
    const NO_DESCRIPTION = 1;
    const descPath = "/../descriptions/";
    public $hasPrice;
    public $amount = 1;
    public $count = 1;
    public $addToPrice = 0;
    public $multiplyPrice = 1;

    public $attached_image;
    public $attachments = array(); // Не удалось присвоить небезопасный атрибут "attachments" класса "CoreInstrument".
    public $attachment1;
    public $attachment2;
    public $attachment3;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CoreInstrument the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getAbbr()
    {
        return TranslitHelper::translit_it($this->name);
    }

    public function getHasPrice()
    { // виртуальный property, важно!

        if (($this->isPriceSettled)) {
            return self::HAS_DESCRIPTION;
        }

        return self::NO_DESCRIPTION;
    }

    public function getHasPriceLiteral()
    {
        if (($this->price))
            return CHtml::image(Yii::app()->getBaseUrl() . "/images/yes_icon.gif");
        return CHtml::image(Yii::app()->getBaseUrl() . "/images/no_icon.png");
    }

    /**
     *  Get FileName that stored in folder
     * @return string filename
     */
    public function getSanitizedFilename()
    {
        $str = $this->sid;
        $tr = array("/" => "-", '…' => '', "'" => '', '"' => '', '.' => '', '*' => '', '?' => '');
        $str = strtr($str, $tr) . '.txt';

        return $str;
    }

    public function obtainDescriptionBySid()
    {
        if ($this->sid) {
            $f = Feature::model()->find('sid=:s', array('s' => $this->sid));
            if ($f) {
                if ($f->description) {
                    $this->description = $f->description;
                }
                if ($f->ruDescription) {
                    $this->ruDescription = $f->ruDescription;
                }
                if ($f->ruName) {
                    $this->ruName = $f->ruName;
                }
            }
            $this->save();
        }
    }


    /**
     * has Description of any means - in folder or in db
     * @return boolean hasDescription
     */
    public function hasDescription()
    {
        return $this->description != null;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'coreinstrument';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        $rules = array(
            array('attached_image', 'file', 'types' => 'jpg, gif, png', 'allowEmpty' => 'true'),
            array('attachment1, attachment2, attachment3', 'file', 'types' => Yii::app()->params['allowedFileTypes'], 'allowEmpty' => 'true'),
            array('name, sid, idManufacturer, idCoreInstrumentClass', 'required'),
            array('name', 'length', 'max' => 255),
            array('sid', 'length', 'max' => 45),
            array('sid', 'unique', 'allowEmpty' => false),
            array('price', 'numerical', 'min' => 0),
            array('enumCurrency', 'numerical', 'min' => 0, 'max' => 4, 'integerOnly' => true),

            array('isPriceSettled, hasPrice', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idCoreInstrument,enumCurrency, price,  name, sid', 'safe', 'on' => 'search'),

            array('attachments', 'safe'),
        );

        if (!Permission::isLoggedUserSuperadmin() and !$this->isNewRecord) {
            $rules[]=array('idManufacturer', 'unsafe' , 'on'=>'update');
            if (Permission::isManufacturerAllowed($this->idManufacturer)) {
                $rules[]=array('description, ruDescription, ruName', 'safe', 'on'=>'update');
            } else {
                $rules[]=array('description, ruDescription, ruName', 'unsafe', 'on'=>'update');
            }
        } else {
            $rules[]=array('idManufacturer, description, ruDescription, ruName', 'safe');
        }
        return $rules;
    }

    public function beforeDelete()
    {

        if (Yii::app()->user->isGuest) {
            return false;
        }

        if (count($this->projects)) {
            $this->addError("", "Нельзя удалить базовый прибор, так как он участвует в проектах!");
            return false;
        }

        if (count($this->orders)) {
            $this->addError("", "Нельзя удалить базовый прибор, так как он участвует в проектах!");
            return false;
        }

        if (!Group::getPermissionsUponZone(Group::ZONE_COREINSTRUMENT,Group::ACTION_DELETE,$this)) {
            return !true;
        }

        return parent::beforeDelete();
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'managedBlocks' => array(self::HAS_MANY, 'ManagedBlock', 'idCoreInstrument', 'order' => '`order`'),
            'featureGroups' => array(self::HAS_MANY, 'FeatureGroup', 'idCoreInstrument', 'order' => '`order`'),
            'coreInstrumentClass' => array(self::BELONGS_TO, 'CoreInstrumentClass', 'idCoreInstrumentClass'),
            'manufacturer' => array(self::BELONGS_TO, 'Manufacturer', 'idManufacturer'),
            'attachments' => array(self::MANY_MANY, 'Attachment', 'attachment2coreinstrument(idCoreInstrument, idAttachment)'),
            'orders' => array(self::HAS_MANY, 'Order', 'idCoreInstrument'),
            'projects' => array(self::HAS_MANY, 'Project', 'idCoreInstrument'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'price' => 'Цена',
            'enumCurrency' => 'Валюта',
            'managedBlocks' => '',
            'idCoreInstrument' => 'Id Core Instrument',
            'idCoreInstrumentClass' => 'Класс оборудования',
            'idManufacturer' => 'Производитель',
            'name' => 'Название',
            'ruName' => 'Перевод Названия',
            'sid' => 'Sid',
            'description' => 'Описание',
            'ruDescription' => 'Перевод Описания',
            'attachments' => 'Загрузить файлы:',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idCoreInstrument', $this->idCoreInstrument);
        $criteria->compare('idCoreInstrumentClass', $this->idCoreInstrumentClass);
        $criteria->compare('idManufacturer', $this->idManufacturer);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('sid', $this->sid, true);

        if ($this->hasPrice !== NULL) {
            if ($this->hasPrice == self::HAS_DESCRIPTION) {
                $criteria->addCondition('price is not null');
            } else if ($this->hasPrice == self::NO_DESCRIPTION) {
                $criteria->addCondition('price IS null');
            }
        }

        list($outUglySql, $additionalWithes) = Group::getUglySql(Group::ZONE_COREINSTRUMENT, True);

        $criteria->addCondition($outUglySql);

        foreach ($additionalWithes as $aw) {
            $criteria->with[]=$aw;
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getSerializedOptionsSettings()
    {
        $model = $this;
        return array_map(
            function ($a) {
                $m = $a->managedBlocks ?
                    array_map(
                        function ($a) {
                            $m = $a->features ?
                                array_map(
                                    function ($a) {
                                        return array('id' => $a->idFeature, 'name' => $a->fullNameUrl, 'sid' => $a->sid);
                                    },
                                    $a->features
                                ) : array();


                            return array('deleted' => array(), 'features' => $m, 'id' => $a->idManagedBlock, 'name' => $a->comment, 'comment' => $a->notice, 'enumRule' => $a->enumRule);
                        },
                        $a->managedBlocks
                    ) : array();
                return array('deleted' => array(), 'managedBlocks' => $m, 'id' => $a->idFeatureGroup, 'name' => $a->name, 'opened' => false, 'img' => $a->getThumbnail(false, true));
            },
            $model->featureGroups);
    }

    function behaviors()
    {
        return array('loggable' => array(
            'class' => 'ActiveRecordLoggable',
            'logCreation' => true,
            'shouldBubbleToMaster' => !true
        ),
            'img' => array(
                'class' => 'ext.hasImage.hasImage',
                'file_prefix' => "coreInstrument",
                'thumbnail_width' => 427,
                'thumbnail_height' => 569,
                'crop_width' => 200,
                'crop_height' => 267,
                'doResize' => true,
                'no_image' => 'coreInstrumentDefault.jpg'
            ),);
    }
}