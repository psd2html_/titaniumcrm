<?php

/**
 * This is the model class for table "company".
 *
 * The followings are the available columns in table 'company':
 * @property integer $idCompany
 * @property integer $idManufacturer
 * @property Manufacturer $manufacturer
 *
 * @property integer $idCompanyClass
 * @property string $fullName
 * @property string $fullNameEng
 * @property string $shortName
 * @property string $city
 * @property string $cityGenetivus
 * @property string $physicalAddress
 * @property string $physicalAddressEng
 * @property string $legalAddress
 * @property string $url
 * @property string $email
 * @property string $phone
 * @property string $addPhone
 * @property string $comment
 * @property integer $enumFinanceType
 * @property string $inn
 * @property string $financeType
 * @property string $ogrn
 * @property string $bik
 * @property string $bankName
 * @property string $account
 * @property string $correspondentAccount
 *
 * @property Customer[] $customers
 */
class Company extends ActiveRecord implements IProvidesCustomizableColumns
{

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Company the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getIsManufacturerDescription()
    {
        return ($this->idManufacturer !== null);
    }

    function behaviors()
    {
        return array(
            'loggable' => array(
                'class' => 'ActiveRecordLoggable',
                'logCreation' => true,
                'shouldBubbleToMaster' => !true
            )
        );
    }

    public function getName()
    {
        return $this->shortName;
    }

    public function saveCustomers($new)
    {

        foreach ($this->customers as $customer) {
            $customer->idCompany = null;
            $customer->save();
        }

        foreach ($new as $customerId) {
            $customer = Customer::model()->findByPk($customerId);

            if ($customer) {

                $customer->idCompany = $this->idCompany;
                $customer->save();
            }
        }
    }

    public function getFinanceType()
    {
        $a = $this->getEnumFinanceTypeList();
        return $a[$this->enumFinanceType];
    }

    public static function getEnumFinanceTypeList()
    {
        return array(0 => 'Коммерческая стурктура',
            1 => 'Телефонный звонок',
            2 => 'Проведена первоначальная встреча',
            3 => 'Экскурсия на завод',
        );
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'company';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('abbr, fullName, idCompanyClass, shortName, city', 'required'),
            array('abbr', 'length', 'max' => 255),
            array('abbr', 'unique', 'allowEmpty' => false),
            array('enumFinanceType', 'numerical', 'integerOnly' => true),
            array('shortName,email, inn, ogrn, bik, bankName, account, correspondentAccount', 'length', 'max' => 64),
            array(' city, phone, addPhone', 'length', 'max' => 45),
            array('fullName, fullNameEng,  physicalAddressEng,  physicalAddress, legalAddress, url', 'length', 'max' => 255),
            array('url', 'url', 'defaultScheme' => 'http'),
            array('comment, fullNameEng,  physicalAddressEng, cityGenetivus, requisites', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idCompany, abbr, fullName, shortName, city, physicalAddress, legalAddress, url, email, phone, addPhone, comment, enumFinanceType, inn, ogrn, bik, bankName, account, correspondentAccount', 'safe', 'on' => 'search'),
        );
    }

    public function beforeDelete()
    {

        if (Yii::app()->user->isGuest) {
            return false;
        }


        if (($this->manufacturer)) {
            $this->addError("", "Нельзя удалить, так как это описание производителя!");
            return false;
        }

        if (count($this->projectsAsBuyer) || (count($this->projectsAsConsumer))) {
            $this->addError("", "Нельзя удалить, так как используется в проектах!");
            return false;
        }


        if (!Group::getPermissionsUponZone(Group::ZONE_COMPANY,Group::ACTION_DELETE,$this)) {
            return !true;
        }

        return parent::beforeDelete();
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'customers' => array(self::HAS_MANY, 'Customer', 'idCompany'),
            'companyClass' => array(self::BELONGS_TO, 'CompanyClass', 'idCompanyClass'),
            'manufacturer' => array(self::BELONGS_TO, 'Manufacturer', 'idManufacturer'),
            'projectsAsBuyer' => array(self::HAS_MANY, 'Project', 'idCompanyBuyer'),
            'projectsAsConsumer' => array(self::HAS_MANY, 'Project', 'idCompanyConsumer'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'requisites' => 'Банковские реквизиты',
            'projectsAsConsumer' => 'Участие в пректах в качестве конечного пользователя',
            'projectsAsBuyer' => 'Участие в пректах в качестве поставщика',
            'companyClass' => 'Тип организации',
            'abbr' => 'Аббревиатура на латинском, для именования КП',
            'idCompany' => 'Id Company',
            'idCompanyClass' => 'Тип организации',
            'companyClass' => 'Тип организации',
            'fullName' => 'Полное наименование',
            'fullNameEng' => 'Полное наименование(на англ.)',
            'shortName' => 'Сокращенное наимаименование',
            'city' => 'Город',
            'cityGenetivus' => 'Город, в родительном падеже (доставка до Москвы)',
            'physicalAddress' => 'Физический адрес',
            'physicalAddressEng' => 'Физический адрес(на англ.)',
            'legalAddress' => 'Юридический адрес',
            'url' => 'Веб-сайт',
            'email' => 'Email',
            'phone' => 'Телефон',
            'addPhone' => 'Доп.телефон',
            'comment' => 'Комментарий',
            'enumFinanceType' => 'Тип финансирования',
            'inn' => 'ИНН',
            'ogrn' => 'ОГРН',
            'bik' => 'БИК',
            'bankName' => 'Название банка',
            'account' => 'Расчетный счет',
            'correspondentAccount' => 'Корреспондентский счет',
            'customers' => 'Список клиентов, сотрудников данной огранизации',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idCompany', $this->idCompany);
        $criteria->compare('idCompanyClass', $this->idCompanyClass);

        $criteria->compare('fullNameEng', $this->fullNameEng, true);

        $criteria->compare('fullName', $this->fullName, true);
        $criteria->compare('fullName', $this->shortName, true, 'OR');

        $criteria->compare('shortName', $this->fullName, true, 'OR');
        $criteria->compare('shortName', $this->shortName, true, 'OR');


        $criteria->compare('city', $this->city, true);

        $criteria->compare('physicalAddress', $this->physicalAddress, true);
        $criteria->compare('physicalAddressEng', $this->physicalAddressEng, true);
        $criteria->compare('legalAddress', $this->legalAddress, true);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('addPhone', $this->addPhone, true);
        $criteria->compare('comment', $this->comment, true);
        $criteria->compare('enumFinanceType', $this->enumFinanceType);
        $criteria->compare('inn', $this->inn, true);
        $criteria->compare('ogrn', $this->ogrn, true);
        $criteria->compare('abbr', $this->abbr, true);
        $criteria->compare('bik', $this->bik, true);
        $criteria->compare('bankName', $this->bankName, true);
        $criteria->compare('account', $this->account, true);
        $criteria->compare('correspondentAccount', $this->correspondentAccount, true);

        if ($this->idCompanyClass==CompanyClass::MANUFACTURE_COMPANY_CLASS) {
            list($outUglySql, $additionalWithes) = Group::getUglySql(Group::ZONE_MANUFACTURER, True);
            $criteria->addCondition($outUglySql);
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * @return ARCustomizableColumnsBase
     */
    public function getCustomizableColumns()
    {
        return new CompanyCustomizableColumns();
    }
}