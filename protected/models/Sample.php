<?php

/**
 * This is the model class for table "sample".
 *
 * The followings are the available columns in table 'sample':
 * @property integer $idSample
 * @property integer $idProject
 * @property string $recievedDate
 * @property string $lastChanged
 * @property string $task
 * @property integer $enumStatus
 * @property string $comment
 * @property string $location
 * @property string $name
 * 
 * @property integer $idManufacturer
 * @property integer $idSampleInProject
 * @property integer $idCoreInstrument
 * @property integer $idCoreInstrumentClass
 * @property integer $enumDeliverType
 * @property string $centre
 *
 * The followings are the available model relations:
 * @property Attachment[] $attachments
 * @property Project $project
 */
class Sample extends ActiveRecord implements IProvidesCustomizableColumns{
      public function setSampleCounter($doSave) {
        if (($this->project)&&(!($this->idSampleInProject))) {
                $this->idSampleInProject=$this->project->sampleCounter+1;
                $this->project->saveCounters(array('sampleCounter'=>1));
                if ($doSave)
                    $this->save();
        }
    }
    function behaviors()
    {
        return array(
            'CAdvancedArBehavior' => array(
                'class' => 'ext.Many2Many.CAdvancedArBehavior'),
            'loggable' => array(
                'class' => 'ActiveRecordLoggable',
                'logCreation' => true,
                'shouldBubbleToMaster' => true,
                'hostAfterSaveAction'=>'updateLastChange',
                'skipAttributes'=>array('lastChanged'),
            ));
    }
    public function afterSave() {
        if ($this->project) {
            $this->project->recacheEarliestSampleStatus();
            if ($this->isNewRecord) {
                $this->project->buildFolderForSample($this);
            }
        }
        parent::afterSave();
    }
    public function afterDelete() {
        if ($this->project) {
            $this->project->recacheEarliestSampleStatus();
        }
        parent::afterDelete();
    }
    public function beforeSave() {
        parent::beforeSave();
      
        if ($this->isNewRecord) {
            $this->setSampleCounter(false);
        }
        
        return true;
    }
    
    public function getName() {
        $name = '';

        if (!$this->project)
            return 'ORPHANED-KP';
        $name.= "TI" . str_pad(1000 + $this->idProject, 4, "0", STR_PAD_LEFT);


        if ($this->idSampleInProject) {
            $name.='-S' . str_pad($this->idSampleInProject, 2, "0", STR_PAD_LEFT);
        } else {
            $name.='-SF' . $this->idSample;
        }

       // $name.=$this->user->abbr;
        if ($this->coreInstrument) {
            $name.='-' . $this->coreInstrument->abbr;
        }

        $name.='-' . $this->project->companyConsumer->abbr;

        return $name;
    }

    public function beforeValidate() {
        //check permissions
        if (!$this->project->isSubzoneWritable(Group::SUBZONE_PROJECT_SAMPLES)) {
            $this->addError('', 'У вас нет необходимых прав для совершения данного действия');
            return false;
        }

        if ($this->idCentre == "") {
            $this->idCentre = null;
        }
        if ($this->idCoreInstrument == "") {
            $this->idCoreInstrument = null;
        }
        return parent::beforeValidate();
    }

    public function getStatus() {
        $array = Sample::getEnumStatusList();
        return $array[$this->enumStatus];
    }

    public static function getEnumStatusList() {

        return array(0 => 'Договорённость',
            1 => 'В Technoinfo',
            2 => 'На измерениях',
            3 => 'Ждём результаты',
            4 => 'Завершен',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Sample the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'sample';
    }

    public function beforeDelete() {

        if (Yii::app()->user->isGuest) {
            return false;
        }
        if (Yii::app()->user->isAdmin()) {
            return true;
        }

        if (!$this->project->isSubzoneWritable(Group::SUBZONE_PROJECT_SAMPLES)) {
            $this->addError('', 'У вас нет необходимых прав для совершения данного действия');
            return false;
        }

        return parent::beforeDelete();
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('idProject, recievedDate, task, enumStatus, idCentre,idCoreInstrument,idManufacturer', 'required'),
            array('idProject, enumStatus', 'numerical', 'integerOnly' => true),
            array('idCentre,idCoreInstrument,idManufacturer', 'safe'),
            array('location', 'length', 'max' => 128),
            array('title', 'length', 'max' => 64),
            array('innerName', 'length', 'max' => 255),
            array('lastChanged, location, comment, innerName', 'safe'),
            array('lastChanged', 'default',
                'value' => new CDbExpression('NOW()'),
                'setOnEmpty' => false, 'on' => 'update'),
            array('lastChanged', 'default',
                'value' => new CDbExpression('NOW()'),
                'setOnEmpty' => false, 'on' => 'insert'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idSample, idProject, recievedDate, lastChanged, task, enumStatus, comment, location, title', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'attachments' => array(self::MANY_MANY, 'Attachment', 'attachment2sample(idSample, idAttachment)'),
            'project' => array(self::BELONGS_TO, 'Project', 'idProject'),
            'coreInstrument' => array(self::BELONGS_TO, 'CoreInstrument', 'idCoreInstrument'),
            'coreInstrumentClass' => array(self::BELONGS_TO, 'CoreInstrumentClass', 'idCoreInstrumentClass'),
            'manufacturer' => array(self::BELONGS_TO, 'Manufacturer', 'idManufacturer'),
            'relatedCentre' => array(self::BELONGS_TO, 'Centre', 'idCentre'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'idSample' => 'Id Sample',
            'idProject' => 'Проект',
            'idManufacturer' => 'Производитель измерительного оборудования',
            'recievedDate' => 'Дата получения',
            'idManfucaturer' => 'Производитель изм. оборудования',
            'idCoreInstrument' => 'На чем проводятся измерения',
            'searchCoreInstrumentClass' => 'На чем проводятся измерения',
            'idCentre' => 'Измерительный центр',
            'lastChanged' => 'Дата последнего изменения',
            'task' => 'Задача',
            'enumStatus' => 'Статус',
            'comment' => 'Комментарий',
            'location' => 'Текущее местонахождение',
            'title' => 'Образец',
            'id' => 'Имя',
            'name' => 'Название',
            'innerName' => 'Внутреннее имя центра измерений',
        );
    }



    public function searchableFields()
    {
        return array(
        array('project.idCoreInstrumentClass', 'relation', 'exact' => true, 'from' => 'project', 'name' => 'searchCoreInstrumentClass', 'label' => 'На чем проводятся измерения'),
        //exact
            array('idSample, idCentre, idProject, idCoreInstrument, idManufacturer, enumStatus', 'exact'),
        //partial
            array('recievedDate, lastChanged, task, comment, location', 'partial'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria= $this->buildSearchCriteria();

//        $criteria->compare('idSample', $this->idSample);
//        $criteria->compare('idCentre', $this->idCentre);
//        $criteria->compare('idProject', $this->idProject);
//        $criteria->compare('recievedDate', $this->recievedDate, true);
//        $criteria->compare('lastChanged', $this->lastChanged, true);
//        $criteria->compare('idCoreInstrument', $this->idCoreInstrument);
//        $criteria->compare('idManufacturer', $this->idManufacturer);
//        $criteria->compare('task', $this->task, true);
//        $criteria->compare('enumStatus', $this->enumStatus);
//        $criteria->compare('comment', $this->comment, true);
//        $criteria->compare('location', $this->location, true);
        //$criteria->compare('name', $this->name, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'recievedDate DESC',
            )
        ));
    }

    /**
     * @return ARCustomizableColumnsBase
     */
    public function getCustomizableColumns()
    {
        return new SampleCustomizableColumns();
    }
}