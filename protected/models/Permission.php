<?php

/**
 * This is the model class for table "permission".
 *
 * The followings are the available columns in table 'permission':
 * @property integer $idPermission
 * @property string $enumPermissionType ===> FULL ACCESS since f8..f9
 * @property integer $idUser
 * @property integer $idManufacturer
 * @property integer $idCoreInstrumentClass
 *
 * The followings are the available model relations:
 * @property Users $idUser0
 * @property Manufacturer $idManufacturer0 
 * @property Coreinstrumentclass $idCoreInstrumentClass0
 */
class Permission extends ActiveRecord {

    const PERMISSION_CHECKPOINT = 10;
    const PERMISSION_WORKFLOW = 10; // sic! 
    
    const PERMISSION_CONTRAGENT = 11;
    const PERMISSION_CUSTOM = 12;
    const PERMISSION_CUSTOMER = 19;
    const PERMISSION_COMPANY = 20;
    const PERMISSION_COMPANYCLASS = 13;
    const PERMISSION_MANUFACTURER = 14;
    const PERMISSION_COREINSTRUMENTCLASS = 15;
    const PERMISSION_COREINSTRUMENT_DELETE = 18;
    const PERMISSION_USER = 16;
    const PERMISSION_FEATURE = 22;
    const PERMISSION_CENTRE = 13;
    const PERMISSION_SPENT = 26; // sic! 
    const PERMISSION_LOGIST = 26;
    const PERMISSION_INSURANCE = 27;
    const PERMISSION_DONT_APPLY_LIMITATION = 99;

    public static function flushPermissions() {
        if (Yii::app()->user->isGuest)
            return false;

        if (!YII_LOCAL)
            Yii::app()->cache->delete("LOCALPERMISSIONS_" . Yii::app()->user->id);

        $y = Manufacturer::model()->findAll();

        foreach ($y as $m) {
            if (!YII_LOCAL)
                Yii::app()->cache->delete("SYNTHP_" . Yii::app()->user->id . "_" . $m->idManufacturer);
        }
    }

    public static function isLoggedUserLogist() {

        if (Yii::app()->user->isGuest)
            return false;
        // ------------------------------------
        return (Yii::app()->user->user()->relatedGroup?
            Yii::app()->user->user()->relatedGroup->isLogist:False);

    }

    public static function isLoggedUserSuperadmin()
    {
        return (Yii::app()->user->isAdmin());
    }

    protected function afterSave() {

        self::getSynthesiedRules($this->idUser, $this->idManufacturer, true);

        return parent::afterSave();
    }

    /**
     * Gets synth rules
     * @param type $userId
     * @param type $manufacturerId
     * @param type $forceUpdate
     * @return Array coreInstrumentClass=>permission
     */
    private static function getSynthesiedRules($userId, $manufacturerId, $forceUpdate = false)
    {
        if (!YII_LOCAL)
            $grant = Yii::app()->cache->get("SYNTHP_" . $userId . "_" . $manufacturerId);

        if (($grant == false) || ($forceUpdate)) {
            $permissions = Permission::model()->findAll(array('condition' => 'idUser=:uid and idManufacturer=:mid', 'order' => 'enumPermissionType DESC', 'params' =>
                array('uid' => $userId, 'mid' => $manufacturerId)));

            $grant = array();
            $whole = false;

            $keys = Yii::app()->db->createCommand(
                            "SELECT j.idCoreInstrumentClass FROM manufacture2coreinstrumentclass as j WHERE j.idManufacture=:mid")
                    ->bindParam('mid', $manufacturerId)
                    ->queryColumn();

            foreach ($permissions as $perm) {
                if ($perm->enumPermissionType == Permission::PERMISSION_PREVENT) {
                    if ($perm->idCoreInstrumentClass == null) {
                        $out = array();
                        break; // everypony is forbidden
                    } else {
                        //$grant[] = $perm->idCoreInstrumentClass;
                        unset($grant[$perm->idCoreInstrumentClass]);
                    }
                } else {
                    if ($perm->idCoreInstrumentClass == null) {
                        // 0 over 1 over 2 
                        foreach ($keys as $key) {
                            $grant[$key] = $perm->enumPermissionType;
                        }
                    } else {
                        $grant[$perm->idCoreInstrumentClass] = $perm->enumPermissionType;
                    }
                }
            }

            if (!YII_LOCAL)
                Yii::app()->cache->set("SYNTHP_" . $userId . "_" . $manufacturerId, serialize($grant), 24 * 60 * 60 * 60);
        } else {
            //loaded from cache
            $grant = unserialize($grant);
        }
        //cache
        return $grant;
    }

    public static function getAllowedManufacturersSql() {
        if (Yii::app()->user->isGuest) {
            return 't.idManufacturer=-1';
        }
        $myId = Yii::app()->user->id;
        //$permissions = Permission::model()->findAllByAttributes(array('idUser' => $myId,'' ));
        // ----------------
        if (False) {
            $m = Yii::app()->db->createCommand(
                            "SELECT idManufacturer FROM permission where idUser=" . $myId . " and enumPermissionType=" . Permission::PERMISSION_READWRITE)
                    ->queryColumn();
            if (!$m) {
                return 't.idManufacturer=-1';
            } else {
                return 't.idManufacturer IN (' . implode(',', $m) . ')';
            }
            //$manufacturers = Manufacturer::model()->findAllBySql("SELECT * from manufacturer as t JOIN permission as p ON p.idManufacturer=t.idManufacturer WHERE p.idUser=:uid AND p.enumPermissionType=" . Permission::PERMISSION_READWRITE, array('uid' => $myId));
            return $manufacturers;
        } else {
            return null;
        }
    }

    public static function isManufacturerAllowed($id) {
        $myId = Yii::app()->user->id;
        $manufacturers = Yii::app()->db->
            createCommand(
           "SELECT COUNT(1) FROM permission as p WHERE p.idManufacturer=:man AND p.idUser=:uid AND p.enumPermissionType=" . Permission::PERMISSION_READWRITE.';')
            ->bindParam('uid' , $myId)
            ->bindParam('man' , $id)
            ->queryScalar();
        return $manufacturers>0;
    }

    public static function isManufacturerAndClassAllowed($id, $idClass) {
        $myId = Yii::app()->user->id;
        $manufacturers = Yii::app()->db->
            createCommand(
           "SELECT COUNT(1) FROM permission as p WHERE p.idManufacturer=:man
           AND p.idUser=:uid
           AND p.enumPermissionType=" . Permission::PERMISSION_READWRITE.'
           AND (p.idCoreInstrumentClass IS NULL or p.idCoreInstrumentClass=:cls)
           ;')
            ->bindParam('uid' , $myId)
            ->bindParam('man' , $id)
            ->bindParam('cls' , $idClass)
            ->queryScalar();
        return $manufacturers>0;
    }

    public static function getAllowedManufacturers() {
        if (Yii::app()->user->isGuest) {
            return array();
        }
        $myId = Yii::app()->user->id;
        //$permissions = Permission::model()->findAllByAttributes(array('idUser' => $myId,'' ));
        // ----------------
        if (!Yii::app()->user->isAdmin()) {
            $manufacturers = Manufacturer::model()->findAllBySql("SELECT * from manufacturer as t JOIN permission as p ON p.idManufacturer=t.idManufacturer WHERE p.idUser=:uid AND p.enumPermissionType=" . Permission::PERMISSION_READWRITE, array('uid' => $myId));
            return $manufacturers;
        } else {
            return Manufacturer::model()->findAll();
        }
    }

    public static function getAllowedCoreInstrumentClasses($mId) {
        if (Yii::app()->user->isGuest) {
            return array();
        }

        $myId = Yii::app()->user->id;


        // ----------------

        if (!Yii::app()->user->isAdmin()) {
            $permissions = Permission::model()->findAll(array('condition' => 'idUser=:uid and idManufacturer=:mid', 'order' => 'enumPermissionType DESC', 'params' =>
                array('uid' => $myId, 'mid' => $mId)));

            $not = array();
            $is = array();
            $whole = false;
            foreach ($permissions as $perm) {
                if ($perm->enumPermissionType == Permission::PERMISSION_PREVENT) {
                    if ($perm->idCoreInstrumentClass == null) {
                        return array();
                    } else {
                        $not[] = $perm->idCoreInstrumentClass;
                    }
                } else if ($perm->enumPermissionType == Permission::PERMISSION_READWRITE) {
                    if ($perm->idCoreInstrumentClass == null) {
                        $whole = true;
                    } else {
                        $is[] = $perm->idCoreInstrumentClass;
                    }
                }
            }


            if ($whole) {
                // everything, except some

                $not = (count($not) > 0) ? " AND t.idCoreInstrumentClass NOT IN (" . implode(", ", $not) . ")" : "";
                $c = CoreInstrumentClass::model()->findAllBySql(
                        "SELECT * FROM coreinstrumentclass as t JOIN manufacture2coreinstrumentclass as j ON j.idCoreInstrumentClass=t.idCoreInstrumentClass WHERE j.idManufacture=:mid" . $not, array('mid' => $mId));
                return $c;
            } else {
                // something, except (probably) some

                $is = array_diff($is, $not);
                if (count($is) > 0) {
                    return CoreInstrumentClass::model()->findAllByPk($is);
                } else {
                    return array();
                }
            }
        } else {
            $c = CoreInstrumentClass::model()->findAllBySql(
                    "SELECT * FROM coreinstrumentclass as t JOIN manufacture2coreinstrumentclass as j ON j.idCoreInstrumentClass=t.idCoreInstrumentClass WHERE j.idManufacture=:mid", array('mid' => $mId));
            return $c;
        }
    }

    public static function getManufacturerCoreInstrumentClassPermission($idManufacturer,$CoreInstrumentClass) {
        $myId = Yii::app()->user->id;
        if (Permission::isLoggedUserSuperadmin()) return Permission::PERMISSION_READWRITE;
        $permissions = Permission::model()->find(array('idUser' => $myId,'idManufacturer'=>$idManufacturer));
        if (!$permissions) {
            return Permission::PERMISSION_PREVENT;
        }
        $UNDECIDED=666;
        $perm=$UNDECIDED;

        foreach ($permissions as $p) {

            if ($p->idCoreInstrumentClass != null) {
                if ($p->idCoreInstrumentClass == $CoreInstrumentClass) {
                    if ($perm>=$p->enumPermissionType)
                    {
                        $perm=$p->enumPermissionType;
                    }
                }
            } else {
                if ($perm>=$p->enumPermissionType)
                {
                    $perm=$p->enumPermissionType;
                }
            }
        }
        return $perm==$UNDECIDED?Permission::PERMISSION_PREVENT:$perm;
    }

    public static function getUglySql($includeIdResponsible,$ciOnly=false) {
        $myId = Yii::app()->user->id;
        $permissions = Permission::model()->findAllByAttributes(array('idUser' => $myId));
        $isLogist=  self::isLoggedUserLogist();
        $uglySql = array();

        if (($isLogist))
        {
            return 't.enumStatus>='.Project::STATUS_CONTEST;
        }

        if (!$permissions) {
            return 't.idManufacturer=-1';
        }

        if ($includeIdResponsible) {
        //    $uglySql[] = 't.idResponsible = ' . $myId;
        }

        $uglySqlNot = array();

        foreach ($permissions as $p) {
            $sql = 't.idManufacturer = ' . $p->idManufacturer;
            if ($p->idCoreInstrumentClass != null) {
                $sql.= ' and t.idCoreInstrumentClass = ' . $p->idCoreInstrumentClass;
            }

            if ($p->enumPermissionType == Permission::PERMISSION_PREVENT) {
                $sql = 'NOT (' . $sql . ')';
                $uglySqlNot[] = $sql;
            } else {
                $sql = '(' . $sql . ')';
                $uglySql[] = $sql;
            }
        }

        $outUglySql = (implode($uglySql, ' OR '));

        if ($uglySqlNot) {
            $uglySqlNot[] = '(' . $outUglySql . ')';
            $outUglySql = (implode($uglySqlNot, ' AND '));
        }
        
        if (($isLogist)&&(!$ciOnly))
        {
            return 't.enumStatus>='.Project::STATUS_CONTEST.' OR '.$outUglySql;
        }

        return $outUglySql;
    }

    const NOTHING = 0;
    const READ = 1;
    const READ_WRITE = 2;

    const PERMISSION_READ = 1;
    const PERMISSION_READWRITE = 2;
    const PERMISSION_PREVENT = 0;

    public function getPermissionType() {
        $a = Permission::enumPermissionTypeList();
        return $a[$this->enumPermissionType];
    }

    public static function enumPermissionTypeList() {
        return array(
            self::PERMISSION_READ => "Разрешить чтение",
            self::PERMISSION_READWRITE => "Разрешить чтение и запись",
            self::PERMISSION_PREVENT => "Запретить",
        );
    }

    public static function permissionList() {
        return array(
           // self::PERMISSION_LOGIST => "Пользователь получает доступ ко всем проектам на этапах Логистика, а также может править траты",
            //self::PERMISSION_SPENT => "Редактирование доп.трат",
            self::PERMISSION_DONT_APPLY_LIMITATION  => "Не приминять ограничения по производителям для отображения проектов и позиций",
            self::PERMISSION_CHECKPOINT => "Редактирование контрольных точек и workflow проекта",
            self::PERMISSION_CONTRAGENT => "Редактирование котрагентов",
            self::PERMISSION_CUSTOM => "Редактирование таможень",
            self::PERMISSION_CUSTOMER => "Редактирование клиентов",
            self::PERMISSION_COMPANY => "Редактирование компании",
            self::PERMISSION_COMPANYCLASS => "Редактирование типов организаций",
            self::PERMISSION_MANUFACTURER => "Редактирование производителей",
            self::PERMISSION_COREINSTRUMENTCLASS => "Редактирование классов оборудования",
            self::PERMISSION_USER => "Редактирование пользователей и прав",
            self::PERMISSION_COREINSTRUMENT_DELETE => "Удаление Ключевых Инструментов(!)",
            self::PERMISSION_FEATURE => "Удаление позиций и групп позиций",
            self::PERMISSION_CENTRE => "Редактирование центров обработки измерений",
            self::PERMISSION_INSURANCE => "Редактирование страховых компаний",
        );
    }

    function behaviors()
    {
        return array(
            'CAdvancedArBehavior' => array(
                'class' => 'ext.Many2Many.CAdvancedArBehavior'),
            'loggable' => array(
                'class' => 'ActiveRecordLoggable',
                'logCreation' => true,
                'shouldBubbleToMaster' => !true

            ));
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Permission the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function beforeValidate() {
        if ($this->idCoreInstrumentClass == 0)
            $this->idCoreInstrumentClass = null;
        $this->enumPermissionType=self::PERMISSION_READWRITE;
        return parent::beforeValidate();
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'permission';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('idUser,enumPermissionType', 'required'),
            array('idPermission, enumPermissionType, idUser, idManufacturer, idCoreInstrumentClass', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idPermission, enumPermissionType, idUser, idManufacturer, idCoreInstrumentClass', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            //  'idUser0' => array(self::BELONGS_TO, 'Users', 'idUser'),
            'manufacturer' => array(self::BELONGS_TO, 'Manufacturer', 'idManufacturer'),
            'coreInstrumentClass' => array(self::BELONGS_TO, 'CoreInstrumentClass', 'idCoreInstrumentClass'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'idPermission' => 'Id Permission',
            'enumPermissionType' => 'Тип правила',
            'idUser' => 'Id User',
            'idManufacturer' => 'Производитель',
            'idCoreInstrumentClass' => 'Класс оборудования',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idPermission', $this->idPermission);
        $criteria->compare('enumPermissionType', $this->enumPermissionType, true);
        $criteria->compare('idUser', $this->idUser);
        $criteria->compare('idManufacturer', $this->idManufacturer);
        $criteria->compare('idCoreInstrumentClass', $this->idCoreInstrumentClass);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
