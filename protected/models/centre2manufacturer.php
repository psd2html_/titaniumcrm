<?php

/**
 * This is the model class for table 'centre2manufacturer'.
 *
 * The followings are the available columns in table 'centre2manufacturer':
 * @property integer $idManufacturer
* @property integer $idCentre
                                 
 */
class centre2manufacturer extends ActiveRecord
{
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'centre2manufacturer';
    }

    public function rules() {
        return array(
            array('idManufacturer, idCentre', 'required'),
            array('idManufacturer, idCentre', 'numeric'),            
            array('idManufacturer, idCentre', 'safe', 'on'=>'search'),
        );
    }
    
    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idManufacturer' => 'idManufacturer',
'idCentre' => 'idCentre',

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idManufacturer',$this->idManufacturer);
$criteria->compare('idCentre',$this->idCentre);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
