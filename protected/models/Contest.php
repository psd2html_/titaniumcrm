<?php

/**
 * This is the model class for table "contest".
 *
 * The followings are the available columns in table 'contest':
 * @property integer $idProject
 * @property string $contestUrl
 * @property string $contestSiteUrl
 * @property double $costLot
 * @property double $costAsk
 * @property double $costContract
 * @property integer $enumStatus
 * @property string $datePost
 * @property string $dateRequest
 * @property string $dateResponse
 * @property string $dateContest
 * @property string $notification
 *
 * The followings are the available model relations:
 * @property Project $idProject0
 */
class Contest extends ActiveRecord
{

    public $attachment1;
    public $attachment2;
    public $attachment3;

    public static function getEnumStatusList()
    {
        return array(0 => 'Получена договоренность',
            1 => 'Телефонный звонок',
            2 => 'Проведена первоначальная встреча',
            3 => 'Экскурсия на завод',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Contest the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getName()
    {
        return $this->project->name;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'contest';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('attachment1, attachment2, attachment3', 'file', 'types' => Yii::app()->params['allowedFileTypes'], 'allowEmpty' => 'true'),
            array('idProject', 'required'),
            array('costLotCurrency, costContractCurrency, costAskCurrency, costContractCurrency, costContractCurrency, idProject, enumStatus', 'numerical', 'integerOnly' => true),
            array('costLot, costAsk, costContract', 'numerical'),
            array('contestUrl, number, contestSiteUrl', 'length', 'max' => 255),
            array('contestUrl, contestSiteUrl','url','defaultScheme' => 'http'),
            array('notification', 'length', 'max' => 80),
            array('datePost, number, dateRequest, dateResponse, dateContest', 'safe'),
            // Костыль для установки NULL в дату, если она пуста - иначе она станет 1899 годом :c
            array('datePost, dateRequest, dateResponse, dateContest', 'default','setOnEmpty'=>'true','value'=>null),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idProject, contestUrl, contestSiteUrl, costLot, costAsk, costContract, enumStatus, datePost, dateRequest, dateResponse, dateContest, notification', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'project' => array(self::BELONGS_TO, 'Project', 'idProject'),
            'attachments' => array(self::MANY_MANY, 'Attachment', 'attachment2contest(idContest, idAttachment)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'number' => 'Номер извещения',
            'idContest' => 'Id Contest',
            'idProject' => 'Id Project',
            'contestUrl' => 'Сайт Конкурса',
            'contestSiteUrl' => 'Конкурсная площадка',

            'costLot' => 'Цена контракта',
            'costLotCurrency' => 'Валюта',
            'costAsk' => 'Размер обеспечения заявки',
            'costAskCurrency' => 'Валюта',
            'costContract' => 'Размер обеспечения контракта',
            'costContractCurrency' => 'Валюта',

            'enumStatus' => 'Статус',
            'datePost' => 'Размещение конкурса',
            'dateRequest' => 'Deadline заявки',
            'dateResponse' => 'Рассмотрение заявок',
            'dateContest' => 'Аукцион',
            'notification' => 'Номер извещения',
            'attachments' => 'Загрузить файлы'
        );
    }

    function behaviors()
    {
        return array(
            'CAdvancedArBehavior' => array(
                'class' => 'ext.Many2Many.CAdvancedArBehavior'),
            'loggable' => array(
                'class' => 'ActiveRecordLoggable',
                'shouldBubbleToMaster' => true,
                'hostAfterSaveAction'=>'updateLastChange',
                'logCreation' => false,
            )
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idContest', $this->idContest);
        $criteria->compare('idProject', $this->idProject);
        $criteria->compare('contestUrl', $this->contestUrl, true);
        $criteria->compare('contestSiteUrl', $this->contestSiteUrl, true);
        $criteria->compare('costLot', $this->costLot);
        $criteria->compare('costAsk', $this->costAsk);
        $criteria->compare('costContract', $this->costContract);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}