<?php
/**
 * Class UploadForm
 * @property Upload oldUpload
 */
class UploadForm extends CFormModel
{
    public $attachment;
    public $oldUpload;
    public $doRemove;
    public $idProject;
    public $idLogisticPosition;
    public $enumField;

    const CONTRACT=0;
    const PACKING_SHEET=1;
    const DESCRIPTION=2;
    const ROOM=4;
    const SETUP=5;

    public function getTitle() {
        $a=array(
            self::CONTRACT=>'Контракт с покупателем',
            self::PACKING_SHEET=>'Упаковочный',
            self::DESCRIPTION=>'Описание',
            self::ROOM=>'Замер помещения',
            self::SETUP=>'АКТ об установке',
        );
        return $a[$this->enumField];
    }

    public function rules()
    {
        return array(
            array('idProject, enumField', 'required'),
            array('doRemove', 'safe')
        );
    }

    public function attributeLabels()
    {
        return array("doRemove" => "УДАЛИТЬ загрузку",'attachment'=>'Загрузить файл');
    }
}