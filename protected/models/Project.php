<?php

/**
 * This is the model class for table "project".
 *
 * The followings are the available columns in table 'project':
 * @property boolean $overrideFilter
 * @property datetime $lastChange
 * @property integer $lastChangeId
 * @property integer $contractTotalCurrency
 * @property integer idRoomMeasurementUpload
 * @property integer idSetupReportUpload
 * @property string setupComment
 * @property DateTime setupDate
 * @property integer $logisticPositionCounter
 * @property integer $idProject
 * @property integer $idInsuranceCompany
 * @property integer $poCounter
 * @property integer $kpCounter
 * @property integer $sampleCounter
 * @property integer $enumStatus
 * @property integer $buyIncotermsCondition
 * @property integer $sellIncotermsCondition
 * @property integer $enumFinanceStatus
 * @property integer $priority
 * @property string $year
 * @property string $payedByUser
 * @property integer $quartal
 * @property string $innerName
 * @property string $name
 * @property string $takeoutAddress
 * @property int $parent
 * @property string $comment
 * @property integer $idCustomer
 * @property integer $enumProvision
 * @property integer $idResponsible
 * @property integer $idCompanyConsumer
 * @property integer $idCompanyBuyer
 * @property enum $permission
 *
 * // todo Не определено свойство "Project.created".
 * @property datetime $created
 *
 * The followings are the available model relations:
 * @property Attachment[] $attachments
 * @property Upload $roomMeasurementUpload
 * @property Upload $setupReportUpload
 * @property Contest[] $contest
 * @property LogisticFragment[] $logisticFragments
 * @property CoreInstrument $coreInstrument
 *
 * @property Event[] $events // DEPRECATED
 * @property Event $eventClosest // DEPRECATED
 *
 * @property Logistic[] $logistic
 * @property LogisticPosition[] $logisticPositions
 * @property Users $responsible
 * @property Sample[] $samples
 * @property Sticky[] $sticky
 * @property Supplier[] $suppliers
 *
 * @property Customer $customer
 * @property Customer $customerB
 * @property Customer $customerC
 *
 * @property Company $companyBuyer
 * @property Company $companyConsumer
 * @property ActiveRecordLog $lastActionLog
 */
class Project extends ProtoProject implements IProvidesCustomizableColumns
{
    const ARE_EVENTS_DEPRECATED = true;
    public $searchModificatorSkipUglySql = false;
    public $searchModificatorOnlyPersonalProjects = false;
    public $pageSize;
    public $formIdCustomer, $formIdCustomerB, $formIdCustomerC, $formIdCompanyBuyer, $formIdCompanyConsumer;
    public $rootOnly, $skipIdProject, $searchIdCustomer;
    public $overrideFilter = false;
    public $activeProjectFilter = null;
    private $nameSearch;
    private $_oldProjectIdResponsible;
    private $_oldEnumStatus;


    // todo: добавил нз нуно ли
    public $consigneeAddress;
    public $goodsNameRus;
    public $goodsOrigin;
    public $goodsNameEng;
    public $poNumber;
    public $contractDate;
    public $contractTotal;
    public $contractNumber;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function getAllTabs()
    {
        $conf0 = self::_getTabConf();
        $out = [];
        foreach ($conf0 as $tabName => $conf) {
            $out[$tabName] = $conf['label'];
        }
        return $out;
    }

    private static function _getTabConf($model = False)
    {
        if ($model === False) {
            $model = new Project();
        }
        return
            array(
                "orders" => array(
                    "subzone" => Group::SUBZONE_PROJECT_ORDER,
                    "label" => "Комм.предложение" . ($model->orders ? '(' . count($model->orders) . ')' : '')),
                "events" => array(
                    "subzone" => Group::SUBZONE_PROJECT_LOG,
                    "label" => "История изменений"),
                "archivedSticky" => array(
                    "subzone" => Group::SUBZONE_PROJECT_TASK,
                    "label" => "Архив задач"),
                "attachments" => array(
                    "subzone" => Group::SUBZONE_PROJECT_FILES,
                    "isInCommonFolder" => true,
                    "label" => "Файлы" . ($model->uploadsCount ? "(" . $model->uploadsCount . ")" : "")),
                "samples" => array(
                    "subzone" => Group::SUBZONE_PROJECT_SAMPLES,
                    "label" => "Образцы" . ($model->samples ? '(' . count($model->samples) . ')' : '')),
                "contest" => array(
                    "subzone" => Group::SUBZONE_PROJECT_CONTEST,
                    "label" => "Конкурс"),
                "logistic" => array(
                    "subzone" => Group::SUBZONE_PROJECT_LOGISTIC,
                    "label" => "Логистика"),
                "spents" => array(
                    "subzone" => Group::SUBZONE_PROJECT_SPENTS,
                    "label" => "Доп.траты" . ($model->spents ? '(' . count($model->spents) . ')' : '')),
                "relatedProjects" => array(
                    "subzone" => True,
                    "label" => "Группа связанных проектов" . ($model->relatedProjects ? '(' . count($model->relatedProjects) . ')' : '')),
                "setup" => array(
                    "subzone" => Group::SUBZONE_PROJECT_INSTALLATION,
                    "label" => "Установка",
                ));
    }

    public function isSubzoneWritable($subzone = Group::SUBZONE_PROJECT_ORDER)
    {
        return Group::getPermissionsUponZone($subzone, Group::ACTION_EDIT, $this);
    }

    public function isVisibleSubzone($zone = Group::SUBZONE_PROJECT_ORDER)
    {
        return !Group::getPermissionsUponZone($zone, Group::ACTION_NONE, $this);
    }

    public function getTabs()
    {
        $confs = self::_getTabConf($this);

        $subfolder = array(
            Group::ACTION_NONE => '',
            Group::ACTION_READONLY => '_ro',
            Group::ACTION_EDIT => '_rw',
        );

        foreach ($confs as $key => $conf) {
            if ($conf['subzone'] === True) {
                $confs[$key]['visible'] = True;
                $confs[$key]['mod'] = "_rw";
                $confs[$key]['readonly'] = (False);
            } else {
                $results = Group::getAllowedActions($this, $conf['subzone']);

                if (count($results) > 0) {
                    $result = $results[0];
                } else {
                    $result = Group::ACTION_NONE;
                }
                if ($result == Group::ACTION_NONE) {
                    unset($confs[$key]);
                } else {
                    $confs[$key]['visible'] = ($result == (Group::ACTION_NONE));
                    $confs[$key]['mod'] = $subfolder[$result];
                    if (isset($conf['isInCommonFolder'])) {
                        $confs[$key]['mod'] = "_common";
                    }
                    $confs[$key]['readonly'] = ($result == (Group::ACTION_READONLY));
                }
            }
        }

        return $confs;
    }

    public function engPreviewColumn()
    {
        return CHtml::link("<i class='fa fa-search'></i>", Yii::app()->createUrl("project/engOverview", array('id' => $this->idProject)), array('clickToClose' => 1, 'class' => 'foundationModalTrigger button secondary'));
    }

    public function getArchiveName()
    {
        return 'project_' . $this->idProject . '.zip';
    }

    public function serializeFolderStructure()
    {
        $c = $this->topLevelFolders;

        return $this->_treeWalker($this->topLevelFolders);
    }

    /**
     * @param Folder[] $folders
     */
    private function _treeWalker($folders, $path = "")
    {
        if ($folders) {
            $out = array();
            foreach ($folders as $folder) {
                $data = array();
                $folderNameSanitized = $folder->name;
                $folderNameSanitized = TranslitHelper::translit_it($folderNameSanitized);
                foreach ($folder->uploads as $upload) {
                    $file = realpath($upload->getFilePath());
                    if (file_exists($file))
                        $data[$file] = $upload->name;
                }
                if ($data) {
                    $out[$path . $folderNameSanitized] = $data;
                }
                if ($folder->folders) {
                    $out += $this->_treeWalker($folder->folders, $path . $folderNameSanitized . "/");
                }
            }
            return $out;
        } else {
            return array();
        }
    }

    /**
     * @param Sample $sample
     */
    public function buildFolderForSample($sample)
    {
        $t = Folder::model()->find("t.enumSpecialFolder = " . Folder::SAMPLES_FOLDER . " AND t.idProject=" . $this->idProject);
        if (!$t) {
            $this->buildFolderStructure();
            $t = Folder::model()->find("t.enumSpecialFolder = " . Folder::SAMPLES_FOLDER . " AND t.idProject=" . $this->idProject);
        }

        $f = new Folder();
        $f->name = $sample->name;
        $f->enumSpecialFolder = Folder::SAMPLE_FOLDER;
        $f->idProject = $this->idProject;
        $f->parent = $t->idFolder;

        $f->save();
    }

    public function buildFolderStructure()
    {
        $samples = array();
        foreach ($this->samples as $sample) {
            $s['n'] = $sample->name;
            $s['t'] = Folder::SAMPLE_FOLDER;
            $s['l'] = $sample->idSample;
            $samples[] = $s;
        }
        $def = array(
            array('n' => "Измерения", 't' => Folder::SAMPLES_FOLDER, 'c' => $samples),
            array('n' => "Конкурсная документация", 't' => Folder::SYSTEM_FOLDER),
            array('n' => "Доставка и таможня", 't' => Folder::SYSTEM_FOLDER, 'c' => array(
                array('n' => "Описание", 't' => Folder::SYSTEM_FOLDER),
                array('n' => "Упаковочные", 't' => Folder::SYSTEM_FOLDER),
                array('n' => "Контракты", 't' => Folder::SYSTEM_FOLDER)
            )),
            array('n' => "Установка", 't' => Folder::SYSTEM_FOLDER),
            array('n' => "Прочее", 't' => Folder::SYSTEM_FOLDER),
        );
        $this->_reccurentBuilder($this->idProject, $def, null);
    }

    private function _reccurentBuilder($id, $array, $parent = null)
    {
        if (count($array) == 0) {
            return;
        }
        foreach ($array as $val) {
            $f = new Folder();
            $f->name = $val['n'];
            if (isset($val['t']))
                $f->enumSpecialFolder = $val['t'];
            if (isset($val['l']))
                $f->link = $val['l'];

            $f->idProject = $id;
            $f->parent = $parent;

            if (!$f->save()) {
                throw new CHttpException(500, "Can't save folder");
            }

            if (isset($val['c']) && (count($val['c'] > 0))) {
                $this->_reccurentBuilder($id, $val['c'], $f->idFolder);
            }
        }
    }

    public function getNameColumn()
    {
        $hierarchyImg = CHtml::image("/images/hierarchy.gif", "Содержит связаные проекты");
        $slaveImg = CHtml::image("/images/Chains.gif", "Присоединенный проект");
        $fireImg = CHtml::image("/images/fire.png", "Повышенный приоритет");

        $naming = $this->childrenProjects ? $hierarchyImg . $this->name
            :
            ($this->parent ? $slaveImg . $this->name : $this->name);
        if ($this->priority > 0) {
            $naming = $naming . $fireImg;
        }
        return $naming;
    }

    public function getCustomersColumn()
    {
        $names = [];
        foreach ($this->customers as $c) {
            $names[] = $c->getFullNameUrl() . '<br/>' . $c->getContactInfo(false);
        }
        return implode("<br/> ", $names);
    }

    public function getProvision()
    {
        $a = self::getEnumProvisionList();
        return isset($a[$this->enumProvision]) ? $a[$this->enumProvision] : 'N/A';
    }

    public function getOrdersColumn()
    {
        $out = array_map(function ($data) {
            return CHtml::link($data->idOrderInProject,
                Yii::app()->createUrl("order/overview", array("id" => $data->idOrder)),
                array('class' => 'foundationModalTrigger', 'clickToClose' => 'true')
            );
        }, $this->orders);
        return ($out ? implode("; ", $out) : "КП нет");
    }

    public function getSamplesStatusColumn()
    {
        $out = array_map(function ($sample) {
                return $sample->nameUrl . ': ' . $sample->status;
            },
            $this->samples);

        return ($out ? implode("; ", $out) : "Измерений нет");
    }

    public function getClosestEventOrSticky()
    {
        if (!self::ARE_EVENTS_DEPRECATED)
            $e = $this->eventClosest;
        else
            $e = null;

        $s = $this->stickyClosest;

        if ($s) {
            if ($s->isTask || $s->showInCalendar) {

            } else {
                $s = null;
            }
        }
        if (!$s && !$e) return null;
        if (!$s && $e) return $e;
        if ($s && !$e) return $s;


        if (strtotime($e->date) < strtotime($s->calendarDate)) {
            return $e;
        } else return $s;
    }

    public function getShipmentDeadline()
    {
        return $this->_logisticAdapter('shipmentDeadline');
    }

    private function _logisticAdapter($attr)
    {
        if ($this->logistic) {
            return $this->logistic->getAttribute($attr);
        } else {
            return null;
        }
    }

    public function getDeadlinePayment()
    {
        return $this->_logisticAdapter('paymentDeadline');
        // return $this->_contestAdapter('shipmentDeadline');
    }

    public function getProductionDeadline()
    {
        return $this->_logisticAdapter('productionDeadline');
    }

    public function getCBOX()
    {
        return $this->getStatusBox();
    }

    public function init()
    {
        $this->year = null; // Yii::app()->dateFormatter->format("yyyy", time());
        $this->quartal = null;
        parent::init();
    }

    /**
     * Returns list of towns (links for HTML)
     * @return type
     */
    public function getTownCompanies()
    {
        $u = "";

        if ($this->companyConsumer)
            $u .= Helpers::makeLinkForCity($this->companyConsumer->city);

        return $u;
    }

    public function getContractTotalFormatted()
    {

        return Helpers::formatPrice($this->contractTotal) . ' ' . Order::resolveCurrencyToText($this->contractTotalCurrency);
    }

    public function getCompanies()
    {
        $u = "";

        if ($this->companyConsumer)
            $u .= $this->companyConsumer->nameUrl;

        return $u;
    }

    /**
     * Gets array of events
     *
     * @return array of Events to be passed to ProFullCalendar
     */

    public function getCalendarEvents($addProjectName = false, $returnUrl = '', $startUnix = -1, $endUnix = -1)
    {

        $dontCheckRange = !(($startUnix > 0) && ($endUnix > 0));

        $dates = array();

        $colors = array(Event::PROJECT_EVENT => '#3A87AD',
            Event::LOGISTIC_EVENT => 'green',
            Event::CHECKPOINT_EVENT => 'darkorange',
            Event::TASK_EVENT => 'navy',
        );

        $fcolors = array(Event::PROJECT_EVENT => 'black',
            Event::LOGISTIC_EVENT => 'white',
            Event::CHECKPOINT_EVENT => 'white',
            Event::TASK_EVENT => 'white',
        );

        $tagsName = array(Event::PROJECT_EVENT => 'eventProjectTag',
            Event::LOGISTIC_EVENT => 'logisticTag',
            Event::CHECKPOINT_EVENT => 'checkpointTag',
            Event::TASK_EVENT => 'taskTag',
        );

        if (!self::ARE_EVENTS_DEPRECATED) {
            foreach ($this->events as $e) {
                if ($dontCheckRange || Helpers::checkDateInRange($startUnix, $endUnix, $e->date)) {
                    $dates[] = Helpers::eventPrepare($e->getType(), Yii::app()->createUrl('event/update', array('returnUrl' => $returnUrl, 'id' => $e->idEvent)), $e->date, $colors[$e->eventClass], '', array('eventTag', $tagsName[$e->eventClass]), $this->idCoreInstrumentClass, true);
                }
            }
        }

        $userID = Yii::app()->user->id;
        foreach ($this->sticky as $e) {
            if ($e->isTask || $e->showInCalendar) {

                if ($dontCheckRange || Helpers::checkDateInRange($startUnix, $endUnix, $e->calendarDate)) {
                    $class = 'stickyTag';
                    if (($e->isTask) && ($e->isDone)) {
                        $class .= ' doneTag';
                    }

                    if ($e->idResponsible == $userID) {
                        $stickySubclass = 'taskToMeTag';
                    } else if ($e->idUser == $userID) {
                        $stickySubclass = 'taskByMeTag';
                    } else {
                        $stickySubclass = 'otherTaskTag';
                    }

                    //  $class .= ' '.$stickySubclass;
                    $dates[] = Helpers::eventPrepare($e->templateIcon . strip_tags($e->text), Yii::app()->createUrl('sticky/update', array('returnUrl' => $returnUrl,
                            'id' => $e->idSticky)),
                        $e->calendarDate,

                        $colors[Event::PROJECT_EVENT],

                        '',
                        array($class,
                            $stickySubclass),
                        $this->idCoreInstrumentClass,
                        true);
                }
            }
        }

        foreach ($this->logisticFragments as $f) {
            if ($dontCheckRange || Helpers::checkDateInRange($startUnix, $endUnix, $f->dateUnload)) {
                $dates[] = Helpers::eventPrepare('Отправлен из ' . ($f->cityFrom).". ".$f->comment, null, $f->dateUnload, 'LightSeaGreen', '', ('logisticTag'), $this->idCoreInstrumentClass);
            }
            if ($dontCheckRange || Helpers::checkDateInRange($startUnix, $endUnix, $f->dateUpload)) {
                $dates[] = Helpers::eventPrepare('Прибыл в ' . ($f->cityTo). ". ".$f->comment, null, $f->dateUpload, 'LightSeaGreen', '', ('logisticTag'), $this->idCoreInstrumentClass);
            }
        }

        if (($this->enumStatus >= self::STATUS_CONTEST)) {
            if ($this->logistic->shipmentDeadline) {
                if ($dontCheckRange || Helpers::checkDateInRange($startUnix, $endUnix, $this->logistic->shipmentDeadline)) {
                    $dates[] = Helpers::eventPrepare('Deadline поставки', null, $this->logistic->shipmentDeadline, 'red', '', 'logisticTag', $this->idCoreInstrumentClass);
                }
            }
            // deprecated --> moved to logisticPostitions

//            if ($this->logistic->productionDeadline) {
//                if ($dontCheckRange || Helpers::checkDateInRange($startUnix, $endUnix, $this->logistic->productionDeadline)) {
//                    $dates[] = Helpers::eventPrepare('Deadline производства', null, $this->logistic->productionDeadline, 'red', '', 'logisticTag', $this->idCoreInstrumentClass);
//                }
//            }

            foreach ($this->logisticPositions as $lp) {
                if ($dontCheckRange || Helpers::checkDateInRange($startUnix, $endUnix, $lp->productionDeadline)) {
                    $dates[] = Helpers::eventPrepare('Deadline производства (' . ($lp->getName()) . ')', null, $lp->productionDeadline, 'red', '', ('logisticTag'), $this->idCoreInstrumentClass);
                }
            }
        }

        if ($this->contest->dateRequest) {
            if ($dontCheckRange || Helpers::checkDateInRange($startUnix, $endUnix, $this->contest->dateRequest)) {
                $dates[] = Helpers::eventPrepare($this->contest->getAttributeLabel('dateRequest'), $this->contest->contestUrl, $this->contest->dateRequest, 'darkred', '', 'contestTag', $this->idCoreInstrumentClass);
            }
        }

        if ($this->contest->datePost) {
            if ($dontCheckRange || Helpers::checkDateInRange($startUnix, $endUnix, $this->contest->datePost)) {
                $dates[] = Helpers::eventPrepare($this->contest->getAttributeLabel('datePost'), $this->contest->contestUrl, $this->contest->datePost, 'darkred', '', 'contestTag', $this->idCoreInstrumentClass);
            }
        }

        if ($this->contest->dateContest) {
            if ($dontCheckRange || Helpers::checkDateInRange($startUnix, $endUnix, $this->contest->dateContest)) {
                $dates[] = Helpers::eventPrepare($this->contest->getAttributeLabel('dateContest'), $this->contest->contestUrl, $this->contest->dateContest, 'darkred', '', 'contestTag', $this->idCoreInstrumentClass);
            }
        }

        if ($this->contest->dateResponse) {
            if ($dontCheckRange || Helpers::checkDateInRange($startUnix, $endUnix, $this->contest->dateResponse)) {
                $dates[] = Helpers::eventPrepare($this->contest->getAttributeLabel('dateResponse'), $this->contest->contestUrl, $this->contest->dateResponse, 'darkred', '', 'contestTag', $this->idCoreInstrumentClass);
            }
        }


        if ($addProjectName) {
            for ($i = 0; $i < count($dates); $i++) {
                $dates[$i]['title'] = CHtml::link($this->name, $this->getViewUrl(), array('class' => 'prj'))
                    . "<br/>" . $dates[$i]['title'];
            }
        }

        foreach ($this->orders as $o) {
            if ($dontCheckRange || Helpers::checkDateInRange($startUnix, $endUnix, $o->date)) {
                $dates[] = Helpers::eventPrepare('К.П. (' . $o->name . ')', Yii::app()->createUrl('order/update', array('returnUrl' => $returnUrl, 'id' => $o->idOrder)), $o->date, 'purple', '', 'poTag', $this->idCoreInstrumentClass);
            }
        }

        return $dates;
    }

    public function beforeValidate()
    {
        //check permissions
        if (!$this->isWritable()) {
            $this->addError('', 'У вас нет необходимых прав для совершения данного действия');
            return false;
        }

        if ($this->idInsuranceCompany == 0) {
            $this->idInsuranceCompany = null;
        }
        if ($this->idCoreInstrument == 0) {
            $this->idCoreInstrument = null;
        }

        if ($this->isNewRecord) {
            $this->created = Yii::app()->dateFormatter->format("yyyy-M-d", time());
        }

        return parent::beforeValidate();
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Project the static model class
     */
    public function isWritable($zone = Group::ZONE_PROJECT)
    {
        return Group::getPermissionsUponZone($zone, Group::ACTION_UPDATE, $this);
    }

    public function getStatus()
    {
        $array = $this->getEnumStatusList();
        return $array[$this->enumStatus];
    }

    public function getFinanceStatus()
    {
        $array = $this->getEnumFinanceStatusList();
        return $array[$this->enumFinanceStatus];
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'project';
    }

    /**
     * Virtual parameter for both Year And Quartal
     * @param $value
     */
    public function  setYearQuartal($value)
    {
        if (!$value) {
            $this->year = null;
            $this->quartal = null;
            return;
        }
        $matches = array();
        if (!preg_match('/^(\d\d\d\d)-(\d)$/', $value, $matches)) {
            $this->addErrors['yearQuartal'] = "Неверный ввод";
        }

        $this->year = $matches[1];
        $this->quartal = $matches[2];
    }

    public function getYearQuartal()
    {
        if (($this->year > 0) && ($this->quartal)) {
            return $this->year . '-' . $this->quartal;
        }
        return null;
    }

    public function  getYearQuartalVariants()
    {
        return $this->getYearQuatralList();

        $currentYear = date("Y");
        $startYear = $currentYear;
        $depth = 5;
        $options = array();
        $romans = array('I', 'II', 'III', 'IV');

        $options[''] = 'Неизвестен';
        for ($y = $startYear; $y < $startYear + $depth; $y++) {
            for ($q = 1; $q <= 4; $q++) {
                $options[$y . '-' . $q] = $y . ' / ' . $romans[$q - 1] . ' квартал';
            }
        }
        return $options;
    }

    public function getYearQuatralList()
    {

        $currentYear = date("Y");
        $startYear = $currentYear;
        $endQuartal = 4;
        $depth = 5;
        $endYear = $startYear + $depth;

        if ($this->enumStatus >= self::STATUS_SUPPLY) {
            $startYear = 2013;
            $endYear = $currentYear;
            $curMonth = date("m", time());
            $endQuartal = ceil($curMonth / 3);
        }

        $options = array();
        $options[''] = 'Неизвестен';
        $romans = array('I', 'II', 'III', 'IV');

        for ($y = $startYear; $y <= $endYear; $y++) {
            $q_max = ($y != $endYear) ? 4 : $endQuartal;
            for ($q = 1; $q <= $q_max; $q++) {
                $options[$y . '-' . $q] = $y . ' / ' . $romans[$q - 1] . ' квартал';
            }
        }
        return $options;
    }

    public function getCommentColumn()
    {
//         if ($this->comment) {
//             $out = CHtml::link("Ред.", Yii::app()->createUrl("project/updateCommentAjax", array("id" => $this->getPrimaryKey())), array('class' => 'right button secondary tiny update foundationModalTrigger'));
//             $out .= $this->comment."<hr/>";
//        } else {
//             $out = CHtml::link("Добавить комментарий", Yii::app()->createUrl("project/updateCommentAjax", array("id" => $this->getPrimaryKey())), array('class' => 'button secondary tiny update foundationModalTrigger'))."<br/><br/>";
//         }
        $out = "";
        $s = array();
        if ($this->sticky) {
            foreach ($this->sticky as $sticky) {
                if (!$sticky->isTask)
                    $s[] = $sticky->getStickyHtml();
            }
        }
        //$out.=implode('<hr/>',$s);
        $out .= implode('<hr/>', $s);
        return $out;
    }

    public function setName($value)
    {
        $nameSearch = $value;
    }

    public function recacheEarliestSampleStatus()
    {
        if ($this->isNewRecord) return;

        $myId = $this->idProject;
        Yii::app()->db->createCommand("UPDATE project set earliestSampleStatus=(SELECT min(enumStatus) from `sample` where idProject=$myId) where idProject=$myId;")->execute();
    }

    public function recacheOrderCount()
    {
        if ($this->isNewRecord) return;

        $myId = $this->idProject;
        Yii::app()->db->createCommand("UPDATE project set countOfOrders=(SELECT count(1) from `order` where idProject=$myId) where idProject=$myId;")->execute();
    }

    public function getContest()
    {
        if ($this->contestRelated) {
            return $this->contestRelated;
        } else {
            $model = new Contest;
            $model->idProject = $this->idProject;
            return $model;
        }
    }

    public function getLogistic()
    {
        if ($this->logisticRelated) {
            return $this->logisticRelated;
        } else {
            $model = new Logistic();
            $model->idProject = $this->idProject;
            return $model;
        }
    }

    public function getCheckpointEvents()
    {
        $a = Event::model()->findAll('eventClass=' . Event::CHECKPOINT_EVENT . ' AND idProject=' . $this->idProject);
        $array = array();
        foreach ($a as $o) {
            $array[$o->idCheckpoint] = $o;
        }
        return $array;
    }

    public function getGrouppedCheckpoints()
    {
        $condition = 'startWith<=:me';
        $models = Checkpoint::model()->findAll(array(
            'condition' => $condition,
            'order' => 'idCheckpointGroup, `order`',
            'params' => array('me' => $this->enumStatus)));

        $out = array();

        foreach ($models as $model) {
            $out[(int)($model->idCheckpointGroup)][] = $model;
        }
        return $out;
    }

    public function getCheckpoints()
    {
        $sel = Yii::app()->db->createCommand('SELECT t.idCheckpoint FROM event as t WHERE t.eventClass=' . Event::CHECKPOINT_EVENT . ' AND t.idProject=' . $this->idProject)
            ->queryColumn();

        $checked = $sel;
        $postfix = '';
        if ($checked) {
            $postfix = 'startWith<=:me AND readyState>=:me OR readyState<=:me AND idCheckpoint NOT IN (' . implode($checked, ', ') . ') ';
        } else {
            $postfix = 'startWith<=:me';
        }
        $a = Checkpoint::model()->findAll($postfix, array('me' => $this->enumStatus));

        return $a;
    }

    /**
     *
     * @param Array $id
     * @param Array $comments
     */
    public function updateCustomers($id, $comments)
    {
        if (count($id) == count($comments)) {

            $ready = Yii::app()->db->createCommand("SELECT idCustomer FROM customer2project WHERE idProject=" . $this->idProject)
                ->queryColumn();
            if (array_diff($id, $ready) || array_diff($ready, $id)) {

                $joined = Customer::model()->findAllByPk($id);
                $joined = array_map(function ($o) {
                    return $o->readableName;
                }, $joined);
                $joined = implode(', ', $joined);

                $a = Yii::app()->db->createCommand("DELETE FROM customer2project WHERE idProject=$this->idProject")->execute();

                for ($i = 0; $i < count($id); $i++) {
                    Yii::app()->db->createCommand("INSERT INTO customer2project (idProject,idCustomer,comment) VALUES ($this->idProject,:ic,:cc)")
                        ->bindParam("ic", $id[$i])
                        ->bindParam("cc", $comments[$i])
                        ->execute();
                }

                $this->logEvent(Yii::app()->user->name
                . ' изменил клиентов у ' . get_class($this)
                . '[' . $this->readableName . '].', ActiveRecordLoggable::ACTION_CHANGE, $this, 'customers', -1, $this, $joined, true);
            } else {
                // maybe comments has changed
                for ($i = 0; $i < count($id); $i++) {
                    Yii::app()->db->createCommand("UPDATE customer2project SET comment = :cc WHERE idProject={$this->idProject} AND idCustomer=:ic;")
                        ->bindParam("ic", $id[$i])
                        ->bindParam("cc", $comments[$i])
                        ->execute();
                }
            }
        }
    }

    public function isProjectDelegatedToUser($idUser)
    {
        $lol = Yii::app()->db->createCommand("SELECT COUNT(1) FROM atomicRbac WHERE idProject={$this->idProject} AND idUser=:iu AND enumRuleClass=" . AtomicRbac::RBAC_PROJECT . ";")
            ->bindParam('iu', $idUser)
            ->queryScalar();

        return $lol > 0;
    }

    public function getCustomers()
    {
        if ($this->isNewRecord)
            return array();
        $a = Yii::app()->db->createCommand(
            "SELECT idCustomer as id, comment as comment FROM customer2project WHERE idProject=$this->idProject")->queryAll();
        $out = array();
        foreach ($a as $record) {
            if ($c = Customer::model()->findByPk($record['id'])) {
                $c->customerComment = $record['comment'];
                $out[] = $c;
            }
        }
        return $out;
    }

    public function beforeDelete()
    {

        if (Yii::app()->user->isGuest) {
            return false;
        }
        if (Yii::app()->user->isAdmin()) {
            return true;
        }

        if (!Group::getPermissionsUponZone(Group::ZONE_PROJECT, Group::ACTION_DELETE, $this)) {
            $this->addError("", "У вас нет необходимых прав");
            return false;
        }

        return parent::beforeDelete();
    }

    public function getParentProject2()
    {
        if ($this->parent) {
            return $this->parentProjectRaw;
        }
        if ($this->childrenProjects) {
            return $this;
        }
        return null;
    }

    public function getRelatedProjects()
    {
        if ($this->parent) {
            return $this->parentProject->childrenProjects;
        }
        if ($this->childrenProjects) {
            return $this->childrenProjects;
        }
        return null;
    }

    public function search($forceFilter = false, $skipPersonalFilter = false)
    {
        $model = $this;

        $criteria = $model->buildSearchCriteria();

        if ($model->searchIdCustomer) {
            $criteria->join = 'JOIN customer2project as c2p on c2p.idProject=t.idProject';
            $criteria->addCondition('c2p.idCustomer= :icr');
            $criteria->params['icr'] = $model->searchIdCustomer;
        }


        if ($model->idProject) {
            if (preg_match("/(?:GT)*([0-9]+)(.*)/", $model->idProject, $m)) {
                $criteria->compare('t.idProject', (int)$m[1] - 1000);
            }
        }

        if ($model->rootOnly) {
            $criteria->addCondition('t.parent IS NULL');
        }

        if ($model->skipIdProject) {
            $criteria->addCondition('t.idProject != :sidp');
            $criteria->addCondition('t.parent IS NULL OR t.parent != :sidp');
            $criteria->params['sidp'] = $model->skipIdProject;
        }

        if ($model->activeProjectFilter !== null) {
            if ($model->activeProjectFilter == Project::ACTIVE_PROJECT_FILTER) {
                $criteria->addCondition('t.enumStatus < ' . Project::STATUS_FINAL);
            } else if ($model->activeProjectFilter == Project::ENDED_PROJECT_FILTER) {
                $criteria->addCondition('t.enumStatus >= ' . Project::STATUS_FINAL);
            }
        }

        if (!$this->searchModificatorSkipUglySql) {

            if ($this->searchModificatorOnlyPersonalProjects) {
                list($outUglySql, $additionalWithes) = Group::getUglySql(Group::ZONE_PROJECT, False, 't', true);
            } else {
                list($outUglySql, $additionalWithes) = Group::getUglySql();
            }

            $criteria->addCondition($outUglySql);

            foreach ($additionalWithes as $aw) {
                $criteria->with[] = $aw;
            }
        }

        return new CActiveDataProvider($model, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.lastChange DESC, t.idProject DESC',
            )
        ));
    }

    /**
     * Post Update Hook: update last change date
     * @param $activeRecordLog
     */
    public function updateLastChange($activeRecordLog)
    {
        $this->lastChange = new CDbExpression('NOW()');
        $this->lastChangeId = $activeRecordLog->idActiveRecordLog;
        $this->afterSaveInProgress = true;
        Yii::app()->db->createCommand("UPDATE project SET lastChange=NOW(), lastChangeId=$this->lastChangeId WHERE idProject={$this->idProject}")->execute();
        $this->afterSaveInProgress = false;
    }

    public function afterSave()
    {
        if ($this->isNewRecord) {
            $name = $this->getName();
            Yii::app()->db->createCommand("UPDATE project SET projectName=:p, name=:name WHERE idProject={$this->idProject}")
                ->bindParam("p", $name)
                ->bindParam("name", $name)
                ->execute();
        }
        if ($this->_oldProjectIdResponsible != $this->idResponsible) {
            PersonalMessageService::notifyAboutAssignedProject($this);
        }
        if ($this->_oldEnumStatus != $this->enumStatus) {
            $logisticStatuses = array(self::STATUS_CONTEST, self::STATUS_SUPPLY, self::STATUS_SUPPORT);
            if (in_array($this->enumStatus, $logisticStatuses)) {
                PersonalMessageService::notifyAboutLogisticRelatedStatusOfProject($this);
            }
        }
        parent::afterSave();
    }

    public function getName()
    {
        $out = "";
        if ($this->idProject) {

            $out = "GT" . str_pad(1000 + $this->idProject, 4, "0", STR_PAD_LEFT);

            if ($this->coreInstrumentClass)
                $out .= '-' . $this->coreInstrumentClass->abbr;

            if ($this->companyConsumer)
                $out .= '-' . $this->companyConsumer->abbr;

        }
        return $out;
    }

    function behaviors()
    {
        return array(
            'CAdvancedArBehavior' => array(
                'class' => 'ext.Many2Many.CAdvancedArBehavior'),
            'loggable' => array(
                'skipAttributes' => array('lastChange', 'lastChangeId', 'projectName'),
                'class' => 'ActiveRecordLoggable',
                'hostAfterSaveAction' => 'updateLastChange',
            )
        );
    }

    public function getCustomizableColumns()
    {
        return new ProjectCustomizableColumns();
    }

    public function afterFind()
    {
        // used to check, whether or not should we send notifications
        $this->_oldProjectIdResponsible = $this->idResponsible;
        $this->_oldEnumStatus = $this->enumStatus;
        parent::afterFind();
    }


}
