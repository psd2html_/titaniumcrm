<?php

/**
 * This is the model class for table 'attachment2coreinstrument'.
 *
 * The followings are the available columns in table 'attachment2coreinstrument':
 * @property integer $idCoreInstrument
* @property integer $idAttachment
                                 
 */
class attachment2coreinstrument extends ActiveRecord
{
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'attachment2coreinstrument';
    }

    public function rules() {
        return array(
            array('idCoreInstrument, idAttachment', 'required'),
            array('idCoreInstrument, idAttachment', 'numeric'),            
            array('idCoreInstrument, idAttachment', 'safe', 'on'=>'search'),
        );
    }
    
    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCoreInstrument' => 'idCoreInstrument',
'idAttachment' => 'idAttachment',

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idCoreInstrument',$this->idCoreInstrument);
$criteria->compare('idAttachment',$this->idAttachment);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
