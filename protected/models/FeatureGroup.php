<?php

/**
 * This is the model class for table "featuregroup".
 *
 * The followings are the available columns in table 'featuregroup':
 * @property integer $idFeatureGroup
 * @property integer $idCoreInstrument
 * @property string $name
 * @property integer $parent
 *
 * @property string $order
 *
 * The followings are the available model relations:
 * @property FeatureGroup $parent0
 * @property FeatureGroup[] $featuregroups
 * @property Ownage[] $ownages
 */
class FeatureGroup extends ActiveRecord {

    function behaviors() {
        return array(
            'img' => array(
                'class' => 'ext.hasImage.hasImage',
                'file_prefix' => "featureGroup",
                'thumbnail_width' => 500,
                'thumbnail_height' => 10000,
                'doResize' => true,
            ),
            'loggable' => array(
                'class' => 'ActiveRecordLoggable',
                'logCreation' => true,
                'shouldBubbleToMaster' => true,
                'masterRelation'=>'coreInstrument'
            ));
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return FeatureGroup the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'featuregroup';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, idCoreInstrument', 'required'),
            array('parent', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idFeatureGroup, name, parent', 'safe', 'on' => 'search'),
        );
    }
    
    public function beforeDelete() {

        if (Yii::app()->user->isGuest) {
            return false;
        }
        
        if (count($this->managedBlocks)) 
        {
            $this->addError("", "Нельзя удалить, так как у группы есть блоки, которые могут быть удалены.");
            return false;
        }


        if (!Group::getPermissionsUponZone(Group::ZONE_COREINSTRUMENT,Group::ACTION_UPDATE,$this->coreInstrument)) {
            return !true;
        }
        return parent::beforeDelete();
    }
    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            //'parent' => array(self::BELONGS_TO, 'FeatureGroup', 'parent'),
            'coreInstrument' => array(self::BELONGS_TO, 'CoreInstrument', 'idCoreInstrument'),
            //'children' => array(self::HAS_MANY, 'FeatureGroup', 'parent'),
            
            'managedBlocks' => array(self::HAS_MANY, 'ManagedBlock', 'idFeatureGroup','order'=>'`order` ASC'),
            'notices' => array(self::HAS_MANY, 'Notice', 'idFeatureGroup'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'coreInstrument'=>'Базовый прибор',
            'managedBlocks'=>'Блоки',
            'notices'=>'Примечания',
            'idFeatureGroup' => 'Id Feature Group',
            'name' => 'Название группы',
            'parent' => 'Parent',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idFeatureGroup', $this->idFeatureGroup);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('parent', $this->parent);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

}