<?php

/**
 * This is the model class for table "manufacturer".
 *
 * The followings are the available columns in table 'manufacturer':
 * @property integer $idManufacturer
 * @property string $name
 * @property string $abbr
 *
 * The followings are the available model relations:
 * @property Coreinstrument[] $coreinstruments
 * @property CoreInstrumentClass[] $coreInsturmentClasses
 */
class Manufacturer extends ActiveRecord
{
    public static function findAllWhoHasCoreInstrumentClass($id)
    {
        $id = Yii::app()->db->createCommand(
            "SELECT DISTINCT idManufacturer FROM coreinstrument WHERE idCoreInstrumentClass=:id")
            ->bindParam('id', $id)->queryColumn();


        return Manufacturer::model()->findAllByPk($id);
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Manufacturer the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getProjectUrlFilter()
    {
        return CHtml::link($this->name, array("project/index", "Project[searchManufacturer]" => $this->name), array('target' => '_blank'));
    }

    public function afterDelete()
    {
        // clear corresponding description;
        parent::afterDelete();
    }

    public function afterSave()
    {
        if ($this->isNewRecord) {
            $m                 = new Company();
            $m->idCompanyClass = CompanyClass::MANUFACTURE_COMPANY_CLASS;
            $m->fullName       = $this->name;
            $m->shortName      = $this->name;
            $m->abbr           = $this->abbr;
            $m->city           = "-";
            $m->idManufacturer = $this->idManufacturer;
            $m->save();
        }
        parent::afterSave();
    }


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'manufacturer';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, abbr', 'required'),
            array('abbr', 'length', 'max' => 10),
            array('abbr', 'unique', 'allowEmpty' => false),
            array('coreInstrumentClasses, motto', 'safe'),
            array('name', 'length', 'max' => 99),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idManufacturer,   name', 'safe', 'on' => 'search'),
        );
    }

    function behaviors()
    {
        return array(
            'loggable'            => array(
                'class'                => 'ActiveRecordLoggable',
                'logCreation'          => true,
                'shouldBubbleToMaster' => !true
            ),
            'img'                 => array(
                'class'            => 'ext.hasImage.hasImage',
                'file_prefix'      => "manufacturer",
                'thumbnail_width'  => 420,
                'thumbnail_height' => 156,
                'crop_width'       => 210,
                'crop_height'      => 78,
                'doResize'         => true,
                'no_image'         => 'empty.png'
            ),
            'CAdvancedArBehavior' => array(
                'class' => 'ext.Many2Many.CAdvancedArBehavior')
        );
    }

    public function beforeDelete()
    {

        if (Yii::app()->user->isGuest) {
            return false;
        }

        if (count($this->coreInstruments)) {
            $this->addError("", "Нельзя удалить производителя, так как у него есть Базовые приборы");

            return false;
        }

        if (!Group::getPermissionsUponZone(Group::ZONE_MANUFACTURER, Group::ACTION_DELETE, $this)) {
            return !true;
        }

        return parent::beforeDelete();
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'coreInstruments'       => array(self::HAS_MANY, 'CoreInstrument', 'idManufacturer'),
            'coreInstrumentClasses' => array(self::MANY_MANY, 'CoreInstrumentClass', 'manufacture2coreinstrumentclass(idManufacture, idCoreInstrumentClass)'),
            'orders'                => array(self::HAS_MANY, 'Order', 'idManufacturer'),
            'projects'              => array(self::HAS_MANY, 'Project', 'idManufacturer'),
            //'centres' => array(self::HAS_MANY, 'Centre', 'idManufacturer'),
            'centres'               => array(self::MANY_MANY, 'Centre', 'centre2manufacturer(idManufacturer, idCentre)'),
            'company'               => array(self::HAS_ONE, 'Company', 'idManufacturer'),
            'features'              => array(self::HAS_MANY, 'Feature', 'idManufacturer'),
            //'allowedCoreInstrumentClasses' => array(self::MANY_MANY, 'CoreInstrumentClass', 'manufacture2coreinstrumentclass(idManufacture, idCoreInstrumentClass)'),
        );
    }

    public function getAllowedCoreInstrumentClasses()
    {
        if (Yii::app()->user->isGuest) {
            return array();
        }
        $myId = Yii::app()->user->id;

        // ----------------

        return Permission::getAllowedCoreInstrumentClasses($this->idManufacturer);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'abbr'           => 'Аббревиатура',
            'idManufacturer' => 'Id Manufacturer',
            'name'           => 'Название',
            'motto'          => 'Девиз, напр. "Техноинфо Лтд. – официальный представитель компании FEI в России и СНГ"',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idManufacturer', $this->idManufacturer);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('abbr', $this->abbr, true);

        list($outUglySql, $additionalWithes) = Group::getUglySql(Group::ZONE_MANUFACTURER, True);
        $criteria->addCondition($outUglySql);

        foreach ($additionalWithes as $aw) {
            $criteria->with[] = $aw;
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}