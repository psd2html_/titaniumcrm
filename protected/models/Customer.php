<?php

/**
 * This is the model class for table "customer".
 *
 * The followings are the available columns in table 'customer':
 * @property integer $idCustomer
 * @property integer $idCompany
 * @property string $firstName
 * @property string $lastName
 * @property string $middleName
 * @property string $address
 * @property string $email
 * @property string $email2
 * @property string $skype
 * @property string $icq
 * @property string $corporation
 * @property string $phone
 * @property string $phone2
 * @property string $mobilePhone
 * @property string $customerComment
 *
 * @property Order $orders Description
 */
class Customer extends ActiveRecord implements IProvidesCustomizableColumns
{

    public function previewColumn()
    {
        return CHtml::link("<i class='fa fa-search'></i>", Yii::app()->createUrl("customer/overview", array('id' => $this->idCustomer)), array('clickToClose' => 1, 'class' => 'foundationModalTrigger button secondary'));
    }

    public function getContactInfo($showJob=true) {
        $out='';

        if ($showJob)
            $out.=Helpers::reutrnTitleAndValueIfNotNull('Должность', $this->job);

        $out.=Helpers::reutrnTitleAndValueIfNotNull('Телефон', $this->phone);
        $out.=Helpers::reutrnTitleAndValueIfNotNull('Доп. телефон', $this->phone2);
        $out.=Helpers::reutrnTitleAndValueIfNotNull('Моб. телефон', $this->mobilePhone);
        $out.=Helpers::reutrnTitleAndValueIfNotNull('Skype', $this->skype);
        $out.=Helpers::reutrnTitleAndValueIfNotNull('Email', $this->email);
        $out.=Helpers::reutrnTitleAndValueIfNotNull('ICQ', $this->icq);
        $out.=Helpers::reutrnTitleAndValueIfNotNull('Адрес', $this->address);
        return $out;
    }
    public function getContactInfoEng($showJob=true) {
        $out='';

        if ($showJob)
            $out.=Helpers::reutrnTitleAndValueIfNotNull('Position', TranslitHelper::translitGost($this->job));

        $out.=Helpers::reutrnTitleAndValueIfNotNull('Phone', TranslitHelper::translitGost($this->phone));
        $out.=Helpers::reutrnTitleAndValueIfNotNull('Add. Phone', TranslitHelper::translitGost($this->phone2));
        $out.=Helpers::reutrnTitleAndValueIfNotNull('Mobile', TranslitHelper::translitGost($this->mobilePhone));
        $out.=Helpers::reutrnTitleAndValueIfNotNull('Skype', TranslitHelper::translitGost($this->skype));
        $out.=Helpers::reutrnTitleAndValueIfNotNull('Email', TranslitHelper::translitGost($this->email));
        $out.=Helpers::reutrnTitleAndValueIfNotNull('ICQ', TranslitHelper::translitGost($this->icq));
        $out.=Helpers::reutrnTitleAndValueIfNotNull('Address', TranslitHelper::translitGost($this->address));
        return $out;
    }

    public $attached_image;
    public $customerComment = "";
    public $companyFilter;
    public $searchPhones;
    public $searchEmails;

    public function getPhones() {
        $out = '';
        $out.= $this->phone?('<b>Тел.</b> '.$this->phone.'<br/>'): '';
        $out.= $this->phone2?('<b>Доп.</b> '.$this->phone2.'<br/>'): '';
        $out.= $this->mobilePhone?('<b>Моб.</b> '.$this->mobilePhone.'<br/>'): '';
        return $out;
    }
    public function getEmails() {
        $out = '';
        $out.= $this->email?(CHtml::link($this->email,'mailto:'.$this->email).'<br/>'): '';
        $out.= $this->email2?(CHtml::link($this->email2,'mailto:'.$this->email2).'<br/>'): '';
        return $out;
    }
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    function behaviors()
    {
        return array(

            'loggable' => array(
                'class' => 'ActiveRecordLoggable',
                'logCreation' => true,
                'shouldBubbleToMaster' => !true
            ),


            'img' => array(
                'class' => 'ext.hasImage.hasImage',
                'file_prefix' => "customer",
                'thumbnail_width' => 10000,
                'thumbnail_height' => 150,
                'doResize' => true,
            ),);
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Customer the static model class
     */
    public function getBothNames()
    {
        if ($this->middleName) {
            $name = $this->lastName . ' ' . $this->firstName . ' ' . $this->middleName;
        } else {
            $name = $this->lastName . ' ' . $this->firstName;
        }
        return $name;
    }

    public function getName()
    {
        return $this->getFullName();
    }

    public function getFullName()
    {

        $name = $this->getShortName();
        if ($this->company) {
            $name = $name . ' (' . $this->company->shortName . ' - ' . $this->job . ')';
        }
        return $name;
    }

    public function getShortName()
    {

        $name = $this->lastName . ' ' . mb_substr($this->firstName, 0, 1, 'utf8') . '.';
        if ($this->middleName) {
            $name = $name . mb_substr($this->middleName, 0, 1, 'utf8') . '.';
        }

        return $name;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'customer';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('attached_image', 'file', 'types' => 'jpg, gif, png', 'allowEmpty' => 'true'),
            array('firstName, lastName', 'required'),
            array('firstName, lastName, middleName, corporation, phone, phone2', 'length', 'max' => 90),
            array('address', 'length', 'max' => 255),
            array('idCompany, job, comment, phone, address, email, email2, skype, icq, mobilePhone, bothName, companyFilter', 'safe'),
            //  array('inn, ogrn, shortCorporation, legalAddress, postAddress, bank, bik, account, accountCorrespondent', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('searchPhones, searchEmails, idCustomer, firstName, lastName, middleName, address, corporation, phone, phone2', 'safe', 'on' => 'search'),
        );
    }

    public function beforeDelete()
    {

        if (Yii::app()->user->isGuest) {
            return false;
        }

        if (count($this->projects)) {
            $this->addError("", "Нельзя удалить человека, так как он используется в проектах");
            return false;
        }

        if (!Group::getPermissionsUponZone(Group::ZONE_CLIENT,Group::ACTION_DELETE,$this)) {
            return !true;
        }
        return parent::beforeDelete();
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'company' => array(self::BELONGS_TO, 'Company', 'idCompany'),
            'projects' => array(self::MANY_MANY, 'Project', 'customer2project(idCustomer, idProject)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        $p = parent::attributeLabels();
        return $p + array(
            'previewColumn' => "Карточка клиента",
            'idCustomer' => 'Id Customer',
            'firstName' => 'Имя',
            'lastName' => 'Фамилия',
            'middleName' => 'Отчество',
            'bothName' => 'Имя',
            'projects' => 'Проекты',
            'address' => 'Физический адрес',
            'corporation' => 'Наименование',
            'phone' => 'Телефон',
            'phone2' => 'Дополнительный телефон',
            'mobilePhone' => 'Мобильный телефон',
            'comment' => 'Комментарий',
            'job' => 'Должность',
            'idCompany' => 'Компания',
            'company' => 'Компания',
            'email2' => 'Доп. почтовый адрес',
            'companyFilter' => 'Компания',
            'searchPhones' => 'Телефоны',
            'searchEmails' => 'Почта',
        );
        var_dump($p);
    }

    public function searchableFields()
    {

        return array(
//            array('year', 'dummy', 'sort' => '(quartal>0) DESC, year, quartal', 'sortDesc' => '(quartal>0) DESC, year DESC, quartal DESC', 'name' => 'year'),
//            array('quartal', 'dummy', 'sort' => '(quartal>0) DESC, year, quartal', 'sortDesc' => '(quartal>0) DESC, year DESC, quartal DESC', 'name' => 'quartal'),
//            array('earliestSampleStatus', 'dummy', 'sort' => 'ISNULL(earliestSampleStatus) ASC, earliestSampleStatus', 'sortDesc' => 'ISNULL(earliestSampleStatus) ASC, earliestSampleStatus DESC', 'name' => 'earliestSampleStatus'),
//            array('productionDeadline', 'dummy', 'from' => 'logisticRelated', 'sort' => 'ISNULL(logisticRelated.productionDeadline) ASC, logisticRelated.productionDeadline', 'sortDesc' => 'ISNULL(logisticRelated.productionDeadline) ASC, logisticRelated.productionDeadline DESC', 'name' => 'productionDeadline'),
//            array('shipmentDeadline', 'dummy', 'from' => 'logisticRelated', 'sort' => 'ISNULL(logisticRelated.shipmentDeadline) ASC, logisticRelated.shipmentDeadline', 'sortDesc' => 'ISNULL(logisticRelated.shipmentDeadline) ASC, logisticRelated.shipmentDeadline DESC', 'name' => 'shipmentDeadline'),
//            array('contestDeadline', 'dummy', 'from' => 'contestRelated', 'sort' => 'ISNULL(contestRelated.dateRequest) ASC, contestRelated.dateRequest', 'sortDesc' => 'ISNULL(contestRelated.dateRequest) ASC, contestRelated.dateRequest DESC', 'name' => 'contestDeadline'),
//
//            array('manufacturer.name', 'relation', 'from' => 'manufacturer', 'name' => 'searchManufacturer', 'label' => 'Производитель'),
//            array('coreInstrument.name', 'relation', 'from' => 'coreInstrument', 'name' => 'searchCoreInstrument', 'label' => 'Базовый прибор'),
//            array('companyBuyer.fullName', 'relation', 'from' => 'companyBuyer',
//                'sort' => 'ISNULL(idCompanyBuyer) ASC, companyBuyer.fullName', 'sortDesc' => 'ISNULL(idCompanyBuyer) ASC, companyBuyer.fullName DESC',
//                'name' => 'searchCompanyBuyerByName', 'label' => 'Компания-поставщик'),
//
//
//            //array('coreInstrument.idCoreInstrumentClass', 'relation', 'exact' => true, 'from' => 'coreInstrument', 'name' => 'idCoreInstrumentClass', 'label' => 'Базовый прибор'),
//            array('logisticRelated.idContragent', 'relation', 'exact' => true, 'from' => 'logisticRelated', 'name' => 'searchContragent', 'label' => 'Контрагент'),
//            array('companyConsumer.idCompanyClass', 'relation', 'exact' => true, 'from' => 'companyConsumer', 'name' => 'searchCompanyClass', 'label' => 'Тип организации'),
//
//            array('idCompanyBuyer, idCompanyConsumer', 'aggregate', 'exact' => true, 'name' => 'searchIdCompany', 'label' => 'Компания'),
//            array('parent, idProject', 'aggregate', 'exact' => true, 'name' => 'searchAllLinkedProjects', 'label' => 'Компания'),
//
//            array('companyBuyer.fullName, companyBuyer.shortName, companyConsumer.fullName, companyConsumer.shortName', 'aggregate', 'from' => 'companyConsumer, companyBuyer', 'name' => 'searchCompany', 'label' => 'Компания'),
//            array('companyBuyer.city, companyConsumer.city', 'aggregate', 'from' => 'companyConsumer, companyBuyer', 'name' => 'searchTownCompany', 'label' => 'Компания'),
//            array('companyConsumer.fullName, companyConsumer.shortName', 'aggregate', 'from' => 'companyConsumer', 'name' => 'searchCompanyConsumer', 'label' => 'Организация-потребитель'),
//
//            array('tnvedCod, year, innerName, name, comment', 'partial'),
//            array('closestTask', 'relation', 'from' => 'closestTaskSorter', 'sort' => 'isTaskFree, closestTaskDate', 'sortDesc' => 'isTaskFree, closestTaskDate DESC', 'name' => 'closestTask'),
//            array('enumStatus, idInsuranceCompany, idCoreInstrumentClass, idResponsible, quartal, idCompanyConsumer', 'exact'),
//
//            array('earliestSampleStatus, countOfOrders, priority, enumProvision, isInsurancePayed, isExportDeclarationMade, isExportDoubleApplicationLicenseRequired, isSpecialDeliveryRequired, isTechnoinfoUnloadsGoods', 'exact'),

            array('company.city', 'relation', 'from' => 'company',
                'sort' => 'company.city', 'sortDesc' => 'company.city DESC',
                'name' => 'companyCity', 'label' => 'Город'),

            array('projects.idCoreInstrumentClass', 'relation', 'from' => 'projects', 'name' => 'searchCoreInstrumentClass', 'label' => 'Направления проектов участия'),
            array('firstName, lastName, job, skype, middleName, address, phone, phone2', 'partial'),
            array('idCustomer, idCompany', 'exact'),
            array('phone, phone2, mobilePhone', 'aggregate', 'exact' => false, 'name' => 'searchPhones', 'label' => 'Телефон'),
            array('email, email2', 'aggregate', 'exact' => false, 'name' => 'searchEmails', 'label' => 'Email'),
            array('firstName, lastName', 'aggregate', 'exact' => false, 'name' => 'searchFullName', 'label' => 'ФИО'),
            array('company.fullName, company.shortName', 'aggregate', 'from' => 'company', 'name' => 'companyFilter', 'label' => 'Компания'),

        );
    }
    function  searchCoreInstrumentClassColumn() {
        $a=Yii::app()->db->createCommand("SELECT name FROM coreinstrumentclass WHERE idCoreInstrumentClass IN (
        SELECT DISTINCT p.idCoreInstrumentClass FROM project as p JOIN customer2project ON customer2project.idProject=p.idProject AND customer2project.idCustomer={$this->idCustomer});")->queryColumn();
        return implode(", ",$a);
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=$this->buildSearchCriteria();

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * @return ARCustomizableColumnsBase
     */
    public function getCustomizableColumns()
    {
        return new CustomerCustomizableColumns();
    }
}