<?php

/**
 * This is the model class for table 'notice2link'.
 *
 * The followings are the available columns in table 'notice2link':
 * @property integer $idLink
* @property integer $idNotice
                                 
 */
class notice2link extends ActiveRecord
{
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'notice2link';
    }

    public function rules() {
        return array(
            array('idLink, idNotice', 'required'),
            array('idLink, idNotice', 'numeric'),            
            array('idLink, idNotice', 'safe', 'on'=>'search'),
        );
    }
    
    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idLink' => 'idLink',
'idNotice' => 'idNotice',

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idLink',$this->idLink);
$criteria->compare('idNotice',$this->idNotice);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
