<?php

/**
 * This is the model class for table "insuranceCompany".
 *
 * The followings are the available columns in table 'insuranceCompany':
 * @property integer $idInsuranceCompany
 * @property string $name
 * @property string $director
 * @property string $phone
 * @property string $email
 * @property string $requisites
 * @property string $address
 *
 * The followings are the available model relations:
 * @property Project[] $projects
 */
class InsuranceCompany extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InsuranceCompany the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'insuranceCompany';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('name, director, phone, email', 'length', 'max'=>255),
			array('requisites, address', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idInsuranceCompany, name, director, phone, email, requisites, address', 'safe', 'on'=>'search'),
		);
	}

    public function beforeDelete()
    {

        if (Yii::app()->user->isGuest) {
            return false;
        }

        if (count($this->projects)) {
            $this->addError("", "Нельзя удалить страховую компанию, так как она используется в проектах");
            return false;
        }
        return parent::beforeDelete();
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'projects' => array(self::HAS_MANY, 'Project', 'idInsuranceCompany'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */

    public function attributeLabels()
    {
        return array(
            'logistics'=>'Проекты',
            'phone'=>'Телефон',
            'email'=>'Email',
            'address'=>'Адрес',
            'idInsuranceCompany' => 'Id Insurance Company',
            'name' => 'Страховая компания',
            'director' => 'ФИО Директора',
            'requisites' => 'Реквизиты',
        );
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idInsuranceCompany',$this->idInsuranceCompany);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('director',$this->director,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('requisites',$this->requisites,true);
		$criteria->compare('address',$this->address,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}