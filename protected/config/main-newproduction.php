<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'defaultController' => 'site', 
     'language'=>'ru',
    'theme'=>'classic',
    'sourceLanguage'=>'en_US',
    'name' => 'Titanium-CRM',  
    // preloading 'log' component
    'preload' => array('log','foundation'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.models.proto.*',
        'application.models.advancedSearcher.*',
        'application.components.*',
        'application.extensions.awegen.components.*',
        'application.modules.user.models.*',
        'application.modules.user.components.*',
    ),
    'modules' => array(
        // uncomment the following to enable the Gii tool

        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'lol',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('93.175.9.177', '::1'),
            'generatorPaths' => array(
                'ext.awegen',
            ),
        ),
        'user' => array(
            # encrypting method (php hash function)
            'disableRegistration' => true,
            'hash' => 'md5',
            # send activation email
            'sendActivationMail' => !true,
            # allow access for non-activated users
            'loginNotActiv' => false,
            # activate user on registration (only sendActivationMail = false)
            'activeAfterRegister' => !false,
            # automatically login from registration
            'autoLogin' => true,
            # registration path
            'registrationUrl' => array('/user/registration'),
            # recovery password path
            'recoveryUrl' => array('/user/recovery'),
            # login form path
            'loginUrl' => array('/user/login'),
            # page after login
            'returnUrl' => array('/personal/index'),
            # page after logout
            'returnLogoutUrl' => array('/user/login'),
        ),
    ),
    // application components
    
    'components' => array(
        'foundation' => array("class" => "ext.foundation.components.Foundation"),
        
        'swiftMailer' => array(
            'class' => 'ext.swiftMailer.SwiftMailer',
        ),
        'user' => array(
            // enable cookie-based authentication

            'class' => 'WebUser',
            'allowAutoLogin' => true,
            'loginUrl' => array('/user/login'),
        ),
        'ih' => array(
            'class' => 'CImageHandler',
        ),
        'clientScript' => array(
            'class' => 'application.components.NLSClientScript',
        //'includePattern'=>'/\/scripts/', //javacsript regexp, if set, only the matched urls will be filtered
        //'excludePattern'=>'/\/raw/'      //javacsript regexp, if set, the matched urls won't be filtered
        ),
        // uncomment the following to enable URLs in path-format

        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                '<controller:\w+>/<action:\w+>/<id:\d+>-<name:\w+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        // uncomment the following to use a MySQL database

        'db' => array(
            //'connectionString' => 'mysql:host=localhost;dbname=pum',
            'connectionString' => 'mysql:host=localhost;dbname=ti',
            'username' => 'ti',
            'password' => 'qaAjtChHMRP12lbbkt3z',
            'emulatePrepare' => true,
            //'username' => 'root',
            //'password' => 'qsc',
            'charset' => 'utf8',
            'tablePrefix' => '',
            'schemaCachingDuration' =>  1, //24 hrs
            //'enableProfiling' => true,
            //'enableParamLogging' => true,
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'cache' => array(
            'class' => 'system.caching.CApcCache',
        ),
        'settings' => array(
            'class' => 'ext.cmsSettings.CmsSettings',
            'cacheComponentId' => 'cache',
            'cacheId' => 'global_website_settings',
            'cacheTime' => 84000,
            'tableName' => 'ti_settings',
            'dbComponentId' => 'db',
            'createTable' => !true,
            'dbEngine' => 'InnoDB',
        ),
        'request' => array(
            'class'=>'HttpRequest',
            'enableCsrfValidation' => true,
            'noCsrfValidationRoutes'=>array('ajax/'),
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                    'filter' => array(
                        'class' => 'ApacheLikeLog',                        
                    ),
                ),
                array(
                    'class' => 'CEmailLogRoute',
                    'levels' => 'error, warning',
                    'except'=>  array('exception.CHttpException.404','exception.CHttpException.403','exception.CHttpException.400'),
                    'emails' => 'sddeath@gmail.com',
                ),
                  /*
                 array(
                    'class' => 'ext.db_profiler.DbProfileLogRoute',   
                    'countLimit' => 1, // How many times the same query should be executed to be considered inefficient
                    'slowQueryMin' => 0.01, // Minimum time for the query to be slow
                    //	'class'=>'CFileLogRoute',
                    'levels' => 'error, warning',
                ),*/
            // uncomment the following to show log messages on web pages
            
              //array(
              //'class'=>'CWebLogRoute',
                //          'levels' => 'error, warning',
              //),
            
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'webmaster@example.com',
        'version' =>'0.41&alpha;',
        'defaultPageSize'=>20,
        'enableLog' => true,
    ),
);
