<?php
class CentreController extends Controller {
    public function getZone( ) {
        return Group::ZONE_MEASUREMENT_CENTRE;
    }


    
    public $layout='column1';
    private $_allowAjax=true;
    
    public function actionIndex() {
        $this->applyGroupPolicy();
       $model = new Centre('search');
        $model->unsetAttributes();

        if (isset($_GET['Centre']))
                $model->setAttributes($_GET['Centre']);

        $this->render('admin', array(
                'model' => $model,
        ));
    }
        
  
        
    public function actionCreate() {
        $this->applyGroupPolicy();
        $model = new Centre;
                if (isset($_POST['Centre'])) {
            $model->setAttributes($_POST['Centre']);                
                try {
                    if($model->save()) {
                    
                        if (($this->_allowAjax)&&(Yii::app()->request->isAjaxRequest)) {
                            exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                        }
                    
                        Yii::app()->user->setFlash('success', "Запись создана");
                        if (isset($_GET['returnUrl'])) {
                                $this->redirect($_GET['returnUrl']);
                        } else {
                                $this->redirect(array('index'));
                        }
                    }  else {
                        if (($this->_allowAjax)&&(Yii::app()->request->isAjaxRequest)) {
                            exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_lightweightForm', array(
                                            'model' => $model), true))));
                        }
                    }
                    
                    
                } catch (Exception $e) {
                        $model->addError('', $e->getMessage());
                }
        } elseif(isset($_GET['Centre'])) {
                        $model->attributes = $_GET['Centre'];
        }
        
        if (($this->_allowAjax)&&(Yii::app()->request->isAjaxRequest)) {
            $this->renderPartial('_lightweightForm', array('model' => $model), false, true);
        } else {
        $this->render('create',array( 'model'=>$model));
        }
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $this->applyGroupPolicy($model);

        if(isset($_POST['Centre'])) {
            $model->setAttributes($_POST['Centre']);
                try {
                    if($model->save()) {
                    
                    if (($this->_allowAjax)&&(Yii::app()->request->isAjaxRequest)) {
                            exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                    }
                        
                    Yii::app()->user->setFlash('success', "Данные обновлены");
                        if (isset($_GET['returnUrl'])) {
                                $this->redirect($_GET['returnUrl']);
                        } else {
                                $this->redirect(array('centre/update/'.$model->getPrimaryKey()));
                        }
                    }   else {
                        if (($this->_allowAjax)&&(Yii::app()->request->isAjaxRequest)) {
                            exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_lightweightForm', array(
                                            'model' => $model), true))));
                        }
                    }
                } catch (Exception $e) {
                        $model->addError('', $e->getMessage());
                }

            }
           if (($this->_allowAjax)&&(Yii::app()->request->isAjaxRequest)) {
            $this->renderPartial('_lightweightForm', array('model' => $model), false, true);
        } else {
        $this->render('update',array(
                'model'=>$model,
                ));
                }
    }

    
    
    public function loadModel($id) {
            $model=Centre::model()->findByPk($id);
            if($model===null)
                    throw new CHttpException(404,Yii::t('app', 'The requested page does not exist.'));
            return $model;
    }

}