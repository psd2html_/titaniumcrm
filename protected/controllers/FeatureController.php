<?php

class FeatureController extends Controller {

    public $layout = 'column1';

    public function getZone( ) {
        return Group::ZONE_FEATURE;
    }


    public function actionIndex() {
        $this->applyGroupPolicy();
        $model = new Feature('search');
        $model->unsetAttributes();

        if (isset($_GET['Feature']))
            $model->setAttributes($_GET['Feature']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionCreate() {
        $this->applyGroupPolicy();
        $model = new Feature;
        if (isset($_POST['Feature'])) {
            $model->setAttributes($_POST['Feature']);
            $model->attached_image = CUploadedFile::getInstance($model, 'attached_image');

            try {
                if ($model->save()) {
                    if ($model->attached_image) {
                        $model->useLoadedFile($model->attached_image->getTempName());
                    }
                    Yii::app()->user->setFlash('success', "Запись создана");
                    if (isset($_GET['returnUrl'])) {
                        $this->redirect($_GET['returnUrl']);
                    } else {
                        $this->redirect(array('feature/update/'.$model->idFeature));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        } elseif (isset($_GET['Feature'])) {
            $model->attributes = $_GET['Feature'];
        }

        $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id) {
        $model = $this->safeLoadModel($id);

        if (isset($_POST['Feature'])) {
            $model->setAttributes($_POST['Feature']);
            $model->attached_image = CUploadedFile::getInstance($model, 'attached_image');
            
            
            try {
                if ($model->save()) {
                    if ($model->attached_image) {
                        $model->useLoadedFile($model->attached_image->getTempName());
                    }
                    Yii::app()->user->setFlash('success', "Данные обновлены");
                    if (isset($_GET['returnUrl'])) {
                        $this->redirect($_GET['returnUrl']);
                    } else {
                        //$this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function loadModel($id) {
        $model = Feature::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        return $model;
    }

}