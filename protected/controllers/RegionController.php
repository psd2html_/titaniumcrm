<?php
class RegionController extends Controller {

    public $layout='column1';
    private $_allowAjax=true;
    
    public function actionIndex() {
       $model = new Region('search');
        $model->unsetAttributes();

        if (isset($_GET['Region']))
                $model->setAttributes($_GET['Region']);

        $this->render('admin', array(
                'model' => $model,
        ));
    }
        
  
        
    public function actionCreate() {
        $model = new Region;
                if (isset($_POST['Region'])) {
            $model->setAttributes($_POST['Region']);                
                try {
                    if($model->save()) {
                    
                        if (($this->_allowAjax)&&(Yii::app()->request->isAjaxRequest)) {
                            exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                        }
                    
                        Yii::app()->user->setFlash('success', "Запись создана");
                        if (isset($_GET['returnUrl'])) {
                                $this->redirect($_GET['returnUrl']);
                        } else {
                                $this->redirect(array('index'));
                        }
                    }  else {
                        if (($this->_allowAjax)&&(Yii::app()->request->isAjaxRequest)) {
                            exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_lightweightForm', array(
                                            'model' => $model), true))));
                        }
                    }
                    
                    
                } catch (Exception $e) {
                        $model->addError('', $e->getMessage());
                }
        } elseif(isset($_GET['Region'])) {
                        $model->attributes = $_GET['Region'];
        }
        
        if (($this->_allowAjax)&&(Yii::app()->request->isAjaxRequest)) {
            $this->renderPartial('_lightweightForm', array('model' => $model), false, true);
        } else {
        $this->render('create',array( 'model'=>$model));
        }
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        
        if(isset($_POST['Region'])) {
            $model->setAttributes($_POST['Region']);
                try {
                    if($model->save()) {
                    
                    if (($this->_allowAjax)&&(Yii::app()->request->isAjaxRequest)) {
                            exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                    }
                        
                    Yii::app()->user->setFlash('success', "Данные обновлены");
                        if (isset($_GET['returnUrl'])) {
                                $this->redirect($_GET['returnUrl']);
                        } else {
                                $this->redirect(array('update/'.$model->getPrimaryKey()));
                        }
                    }   else {
                        if (($this->_allowAjax)&&(Yii::app()->request->isAjaxRequest)) {
                            exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_lightweightForm', array(
                                            'model' => $model), true))));
                        }
                    }
                } catch (Exception $e) {
                        $model->addError('', $e->getMessage());
                }

            }
           if (($this->_allowAjax)&&(Yii::app()->request->isAjaxRequest)) {
            $this->renderPartial('_lightweightForm', array('model' => $model), false, true);
        } else {
        $this->render('update',array(
                'model'=>$model,
                ));
                }
    }
                
    public function actionDeleteAjax() {
    return false;
        if (($this->_allowAjax)&&(Yii::app()->request->isAjaxRequest)) {
         $id = Yii::app()->request->getPost('mid', 0);
            try {
                $this->loadModel($id)->delete();
            } catch (Exception $e) {
                    throw new CHttpException(500,$e->getMessage());
            }

            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                            $this->redirect(array('index'));
            }
            
            Helpers::headerJson();
            echo(CJSON::encode(array('result' => 1, 'mid' => $id)));
        }
        else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request.'));
    }      

    public function actionDelete($id) {
        if(Yii::app()->request->isPostRequest) {    
            try {
                $this->loadModel($id)->delete();
            } catch (Exception $e) {
                    throw new CHttpException(500,$e->getMessage());
            }

            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                            $this->redirect(array('index'));
            }
        }
        else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request.'));
    }
                
  
    
    
    public function loadModel($id) {
            $model=Region::model()->findByPk($id);
            if($model===null)
                    throw new CHttpException(404,Yii::t('app', 'The requested page does not exist.'));
            return $model;
    }

}