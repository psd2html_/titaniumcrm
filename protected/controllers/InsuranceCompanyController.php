<?php
class InsuranceCompanyController extends Controller
{
    public function getZone( ) {
        return Group::ZONE_INSURANCE_COMPANY;
    }

    public $layout = 'column1';
    private $_allowAjax = false;

    public function actionIndex()
    {
        $this->applyGroupPolicy();
        $model = new InsuranceCompany('search');
        $model->unsetAttributes();

        if (isset($_GET['InsuranceCompany']))
            $model->setAttributes($_GET['InsuranceCompany']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionCreate()
    {
        $this->applyGroupPolicy();
        $model = new InsuranceCompany;
        if (isset($_POST['InsuranceCompany'])) {
            $model->setAttributes($_POST['InsuranceCompany']);
            try {
                if ($model->save()) {

                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                    }

                    Yii::app()->user->setFlash('success', "Запись создана");
                    if (isset($_GET['returnUrl'])) {
                        $this->redirect($_GET['returnUrl']);
                    } else {
                        $this->redirect(array('index'));
                    }
                } else {
                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_lightweightForm', array(
                            'model' => $model), true))));
                    }
                }


            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        } elseif (isset($_GET['InsuranceCompany'])) {
            $model->attributes = $_GET['InsuranceCompany'];
        }

        if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
            $this->renderPartial('_lightweightForm', array('model' => $model), false, true);
        } else {
            $this->render('create', array('model' => $model));
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $this->applyGroupPolicy($model);
        if (isset($_POST['InsuranceCompany'])) {
            $model->setAttributes($_POST['InsuranceCompany']);
            try {
                if ($model->save()) {

                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                    }

                    Yii::app()->user->setFlash('success', "Данные обновлены");

                } else {
                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_lightweightForm', array(
                            'model' => $model), true))));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }

        }

        $project = new Project();
        $project->unsetAttributes();
        if (isset($_GET['Project'])) {
            $project->setAttributes($_GET['Project']);
        }
        $project->idInsuranceCompany = $model->idInsuranceCompany;

        $this->render('update', array(
            'model' => $model,
            'project' => $project,
            'projectDataProvider' => $project->search(true, true)
        ));
    }

    public function loadModel($id)
    {
        $model = InsuranceCompany::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        return $model;
    }



}