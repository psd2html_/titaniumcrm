<?php

class FeatureGroupController extends Controller {

    public $layout = 'column1';

    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('deny', // deny all users
                'users' => array('?'),
            ),
        );
    }

    public function actionIndex() {
        $model = new FeatureGroup('search');
        $model->unsetAttributes();

        if (isset($_GET['FeatureGroup']))
            $model->setAttributes($_GET['FeatureGroup']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionCreateAjax() {
        $id = (Yii::app()->request->getPost('id', '0'));
        $m = CoreInstrument::model()->findAllByPk($id);
        if (!$m) {
            throw new CHttpException(404, 'Not present');
        }

        $model = new FeatureGroup;
        if (isset($_POST['FeatureGroup'])) {
            $model->setAttributes($_POST['FeatureGroup']);
            $model->idCoreInstrument = $m->idCoreInstrument;

            try {
                if ($model->save()) {
                    return;
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        }

        $this->renderPartial('create', array('model' => $model));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        if (isset($_POST['FeatureGroup'])) {
            
            try {
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', "Данные обновлены");
                    if (isset($_GET['returnUrl'])) {
                        $this->redirect($_GET['returnUrl']);
                    } else {
                        $this->redirect(array('/coreInstrument/index'));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            try {
                $this->loadModel($id)->delete();
            } catch (Exception $e) {
                throw new CHttpException(500, $e->getMessage());
            }

            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(array('index'));
            }
        }
        else
            throw new CHttpException(400,
                    Yii::t('app', 'Invalid request.'));
    }

    public function loadModel($id) {
        $model = FeatureGroup::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        return $model;
    }

}