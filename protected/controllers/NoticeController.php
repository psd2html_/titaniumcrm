<?php

class NoticeController extends Controller {

    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('deny', // deny all users
                'users' => array('?'),
            ),
        );
    }

    public $layout = 'column1';

    public function actionIndex() {
        
        $model = new Notice('search');
        $model->unsetAttributes();

        if (isset($_GET['Notice']))
            $model->setAttributes($_GET['Notice']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionCreate() {
        if (!isset($_POST['Notice'])) {
            if (!isset($_GET['idFeatureGroup'])) {
                throw new CHttpException(400, 'Не задан id группы');
                return;
            } else {
                $modelFG = FeatureGroup::model()->findByPk($_GET['idFeatureGroup']);

                if (!$modelFG) {
                    var_dump($modelFG);
                    return;
                    throw new CHttpException(400, 'Не задан id установки');
                    return;
                }
            }
            $model = new Notice;
            $model->idFeatureGroup = $_GET['idFeatureGroup'];
            $model->featureGroup = $modelFG;
        } else {
            $model = new Notice;
        }







        if (isset($_POST['Notice'])) {
            $model->setAttributes($_POST['Notice']);

            if (isset($_POST['Notice']['features'])) {
                $model->save_features = $_POST['Notice']['features'];
            } else {
                $model->save_features = array();
            }
            $model->update_features = true;


            try {
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', "Запись создана");

                    $this->redirect(Yii::app()->createUrl("coreInstrument/update", array('id' => $model->featureGroup->idCoreInstrument)));
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        }

        $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        if (isset($_POST['Notice'])) {
            $model->setAttributes($_POST['Notice']);

            if (isset($_POST['Notice']['features'])) {
                $model->save_features = $_POST['Notice']['features'];
            } else {
                $model->save_features = array();
            }
            $model->update_features = true;

            try {
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', "Данные обновлены");
                    $this->redirect(Yii::app()->createUrl("coreInstrument/update", array('id' => $model->featureGroup->idCoreInstrument)));
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDeleteAjax() {
        if (Yii::app()->request->isPostRequest) {
            $id = Yii::app()->request->getPost('mid', 0);

            try {
                $this->loadModel($id)->delete();
            } catch (Exception $e) {
                throw new CHttpException(500, $e->getMessage());
            }

            Helpers::headerJson();
            echo(CJSON::encode(array('result' => 1, 'mid' => $id)));
        }
        else
            throw new CHttpException(400, Yii::t('app', 'Invalid request.'));
    }

    public function loadModel($id) {
        $model = Notice::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        return $model;
    }

}