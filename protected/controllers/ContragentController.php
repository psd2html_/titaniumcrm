<?php
class ContragentController extends Controller
{
    public function getZone( ) {
        return Group::ZONE_CONTRAGENT;
    }

    public $layout = 'column1';



    public function actionIndex()
    {
        $this->applyGroupPolicy();
        $model = new Contragent('search');
        $model->unsetAttributes();

        if (isset($_GET['Contragent']))
            $model->setAttributes($_GET['Contragent']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionCreate()
    {
        $this->applyGroupPolicy();
        $model = new Contragent;
        if (isset($_POST['Contragent'])) {
            $model->setAttributes($_POST['Contragent']);


            try {
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', "Запись создана");
                    if (isset($_GET['returnUrl'])) {
                        $this->redirect($_GET['returnUrl']);
                    } else {
                        $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        } elseif (isset($_GET['Contragent'])) {
            $model->attributes = $_GET['Contragent'];
        }

        $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id)
    {
        $this->applyGroupPolicy();
        $model = $this->loadModel($id);

        if (isset($_POST['Contragent'])) {
            $model->setAttributes($_POST['Contragent']);
            try {
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', "Данные обновлены");
                    if (isset($_GET['returnUrl'])) {
                        $this->redirect($_GET['returnUrl']);
                    } else {
                        $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }

        }
        //$m=new CDbCriteria();
        //$m->with[]="logisticRaw";
        //$m->compare("logisticRaw.idContragent", $model->idContragent);
        $project = new Project();
        $project->unsetAttributes();
        if (isset($_GET['Project'])) {
            $project->setAttributes($_GET['Project']);
        }
        $project->searchContragent = $model->idContragent;
        $this->render('update', array(
            'model' => $model,
            'project'=>$project,
            'projectDataProvider' => $project->search(true,true)
        ));
    }

    public function loadModel($id)
    {
        $model = Contragent::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        return $model;
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            try {
                $this->safeLoadModel($id)->delete();
            } catch (Exception $e) {
                throw new CHttpException(500, $e->getMessage());
            }

            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(array('index'));
            }
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request.'));
    }

}