<?php

class CoreInstrumentController extends Controller
{
    public function getZone( ) {
        return Group::ZONE_COREINSTRUMENT;
    }

    public function actionDescriptionUpdate($id)
    {
        $model = $this->loadModel($id);
        $this->applyGroupPolicy($model,Group::ACTION_UPDATE);

        if (isset($_POST['CoreInstrument'])) {
            $model->setAttributes($_POST['CoreInstrument']);
            try {
                if ($model->save()) {

                    if ((Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                    }
                    Yii::app()->user->setFlash('success', "Данные обновлены");

                } else {
                    if ((Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_descriptionEditor', array(
                            'model' => $model), true))));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }

        }
        if ((Yii::app()->request->isAjaxRequest)) {
            $this->renderPartial('_descriptionEditor', array('model' => $model), false, true);
        } else {


        }
    }

    public $layout = 'column1';

    public function actionGetFeatureGroupConfig($id)
    {
        $model=$this->loadModel($id);
        $this->applyGroupPolicy($model,Group::ACTION_UPDATE);

        $nggroups = $model->getSerializedOptionsSettings();

        Helpers::headerJson();
        echo (CJSON::encode($nggroups));
    }

    public function actionDeleteFeatureGroupImage()
    {

        $json=CJSON::decode(Yii::app()->request->getRawBody());
        $id=isset($json['gid'])?$json['gid']:0;

        $model=FeatureGroup::model()->findByPk($id);
        $this->applyGroupPolicy($model->coreInstrument,Group::ACTION_UPDATE);
        Helpers::headerJson();
        $this->layout=null;

        if (($model))
        {
            $model->deleteImage();
            echo CJSON::encode(array('r'=>1));
            return;
        }

        echo CJSON::encode(array('r'=>-1));
    }

    public function actionUploadFeatureGroupImage()
    {

        $var=CUploadedFile::getInstanceByName("image");
        $id=Yii::app()->request->getParam('gid',0);
        $model=FeatureGroup::model()->findByPk($id);
        $this->applyGroupPolicy($model->coreInstrument,Group::ACTION_UPDATE);
        Helpers::headerJson();
        $this->layout=null;

        if (($var)&&($model))
        {
            $allowed_mimes = array('image/png', 'image/jpg', 'image/jpeg', 'image/gif');
            if (!in_array($var->type, $allowed_mimes)) {
                echo CJSON::encode(array('r'=>-1,'msg'=>"Файл " . $var->name . " не является картинкой, или поврежден. "));
                return;
            }

            try {
                Yii::app()->ih->load($var->getTempName());
            } catch (Exception $e) {
                echo CJSON::encode(array('r'=>-1,'msg'=>"Файл " . $var->name . " не является картинкой, или поврежден. "));
                return;
            }

            $model->useLoadedFile($var->getTempName());
            echo CJSON::encode(array('r'=>1,'fn'=>$model->getThumbnail(false, true)));
            return;
        }

        echo CJSON::encode(array('r'=>-1));

    }

    public function actionSavegroups($id)
    {
        //api endpoint;
        $k = CJSON::decode(Yii::app()->request->getRawBody());

        $this->applyGroupPolicy($this->loadModel($id),Group::ACTION_UPDATE);


        if ($k) {
            foreach ($k['d'] as $deleteId) {
                if ($deleteId > 0) {

                    $m = FeatureGroup::model()->findByPk($deleteId);

                    if ($m && $m->idCoreInstrument == $id) {

                        foreach ($m->managedBlocks as $mb)
                        {
                            $mb->delete();
                        }
                        $m->managedBlocks=array();
                        $m->delete();

                    }
                }
            }
            $fgOrder=0;
            foreach ($k['g'] as $group) {
                if ($group['id'] > 0) {
                    $m = FeatureGroup::model()->findByPk($group['id']);
                } else {
                    $m = new FeatureGroup();
                    $m->idCoreInstrument=$id;
                }
                if ($m && $m->idCoreInstrument == $id) {
                    $m->order=$fgOrder;
                    $fgOrder++;
                    $m->name=isset($group['name'])?htmlspecialchars_decode($group['name']):'';
                    $m->save();


                    // - managedBlocks -

                    foreach ($group['deleted'] as $deleteId) {
                        if ($deleteId > 0) {
                            $l = ManagedBlock::model()->findByPk($deleteId);
                            if ($l && $l->idFeatureGroup == $m->idFeatureGroup) {
                                $l->delete();
                            }
                        }
                    }
                    $mbOrder=0;
                    foreach ($group['managedBlocks'] as $block) {

                        if ($block['id'] > 0) {
                            $b = ManagedBlock::model()->findByPk($block['id']);
                        } else {
                            $b = new ManagedBlock();
                            $b->idCoreInstrument=$id;
                            $b->idFeatureGroup=$m->idFeatureGroup;
                        }

                        if ($b && $b->idFeatureGroup==$m->idFeatureGroup) {

                            $b->order=$mbOrder;
                            $mbOrder++;

                            $b->comment=isset($block['name'])?htmlspecialchars_decode($block['name']):'';
                            $b->notice=htmlspecialchars_decode($block['comment']);
                            $b->enumRule=$block['enumRule'];
                            $b->save_features=array_map(function($o) { return $o['id']; },$block['features']);

                            $b->save();

                            // - managedBlocks -
                        }

                    }

                }

            }

            $model=$this->loadModel($id);
            Helpers::headerJson();
            echo CJSON::encode($model->getSerializedOptionsSettings());
        }
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('deny', // deny all users
                'users' => array('?'),
            ),
        );
    }

    public function actionIndex()
    {
        $this->applyGroupPolicy();

        $model = new CoreInstrument('search');
        $model->unsetAttributes();

        if (isset($_GET['CoreInstrument']))
            $model->setAttributes($_GET['CoreInstrument']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionCreate()
    {
        $this->applyGroupPolicy();

        $model = new CoreInstrument;
        if (isset($_POST['CoreInstrument'])) {
            $model->setAttributes($_POST['CoreInstrument']);

            $model->attached_image = CUploadedFile::getInstance($model, 'attached_image');

            try {
                if ($model->save()) {
                    if ($model->attached_image) {
                        $model->useLoadedFile($model->attached_image->getTempName());
                    }
                    $this->trySavingAttaches($model);
                    $model->obtainDescriptionBySid();
                    Yii::app()->user->setFlash('success', "Запись создана");
                    if (isset($_GET['returnUrl'])) {
                        $this->redirect($_GET['returnUrl']);
                    } else {
                        $this->redirect(Yii::app()->createAbsoluteUrl('coreInstrument/update', array('id' => $model->idCoreInstrument)));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        } elseif (isset($_GET['CoreInstrument'])) {
            $model->attributes = $_GET['CoreInstrument'];
        }

        $this->render('create', array('model' => $model));
    }

    private function trySavingAttaches($model)
    {

        for ($i = 1; $i <= 3; $i++) {


            $file = CUploadedFile::getInstance($model, 'attachment' . $i);

            if ($file) {

                $k = new Attachment();
                $k->attached_file = $file;
                $k->belongId = $model->idCoreInstrument;
                $k->belongType = Attachment::BELONGS_TO_COREINSTRUMENT;

                if (!$k->save()) {
                    die();
                }

                $linkTableName = 'attachment2coreinstrument';
                $columnName = 'idCoreInstrument';

                Yii::app()->db->createCommand('insert into ' . $linkTableName . ' (' . $columnName . ', idAttachment) values (' . $model->getPrimaryKey() . ', ' . $k->getPrimaryKey() . ')')
                    ->execute();
            }
        }
    }

    public function actionUpdate($id)
    {

        // var_dump($_POST);
        // die();
        $model = $this->loadModel($id);
        $this->applyGroupPolicy($model,Group::ACTION_UPDATE);

        if (isset($_POST['CoreInstrument'])) {
            $model->setAttributes($_POST['CoreInstrument']);
            $model->attached_image = CUploadedFile::getInstance($model, 'attached_image');
            try {
                if ($model->save()) {
                    if ($model->attached_image) {
                        //die();
                        $model->useLoadedFile($model->attached_image->getTempName());
                    }
                    $this->trySavingAttaches($model);
                    if (isset($_POST['managedBlockOrder'])) {
                        foreach ($_POST['managedBlockOrder'] as $i => $id) {
                            Yii::app()->db->createCommand('update managedblock SET `order`=:o WHERE idManagedBlock=:id')
                                ->bindParam('o', $i)
                                ->bindParam('id', $id)
                                ->execute();
                        }
                    }
                    Yii::app()->user->setFlash('success', "Данные обновлены");
                    if (isset($_GET['returnUrl'])) {
                        $this->redirect($_GET['returnUrl']);
                    } else {
                        //$this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function loadModel($id)
    {
        $model = CoreInstrument::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        return $model;
    }





}