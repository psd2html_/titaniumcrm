<?php

class ImgProcessController extends Controller
{

    /*
       *  Open Image Processing DialogBox
     *  i = id, m = model, that have hasImage behaviour
   *  p = perfix, to manipulate
   *
   */

    public function actionDo()
    {
        if ((!isset($_GET['i'])) || (!isset($_GET['m']))) {
            return $this->_ajaxError("Некорректный запрос");
        }

        $id = Yii::app()->request->getParam('i', 0);
        $modelToken = Yii::app()->request->getParam('m', "");

        if ($t = $this->_lookupModel($modelToken, $id)) {

            return $this->render('_imgProcess', array('url' => $t['url'], 'thurl' => $t['thurl'],
                'minW' => $t['minW'], 'minH' => $t['minH'],
                'id' => $id, 'cropUrl' => 'ajax/DoAdjustCrop',
                'modelToken' => $modelToken), false, true);


        } else {
            echo $this->_ajaxError("Некорректные параметры");
            return;
        }
    }

    private function _ajaxError($message)
    {
        Helpers::headerJson();
        echo CJSON::encode(array('r' => -1, 'm' => $message));
    }

    private function _lookupModel($token, $id)
    {
        $out = null;
        $t = null;

        switch ($token) {
            case "feature":
                $t = new Feature();
                break;
            case "userSign":
                $t = new User();
                break;
            case "coreInstrument":
                $t = new CoreInstrument();
                break;
            case "manufacturer":
                $t = new Manufacturer();
                break;
            case "stamp":
                $t = new User();
                break;
            default:
                return null;
        }

        $t = $t::model()->findByPk($id);
        if (!$t) return null;

        if ($token == 'feed') {
            $minW = $t->getWidth();
            $minH = $t->getHeight();
        } else {
            $minW = $t->img->crop_width;
            $minH = $t->img->crop_height;
        }

        $url = $t->getImage();
        $thurl = $t->getThumbnail(false, true);
        if ($minW > 5000) $minW = null;
        if ($minH > 5000) $minH = null;
        return array(
            'model' => $t,
            'url' => $url,
            'thurl' => $thurl,
            'minW' => $minW,
            'minH' => $minH
        );
    }

    public function actionUpload()
    {

        $s = $_POST;

        if ((!isset($s['i'])) || (!isset($s['m']))) {
            return $this->_ajaxError("Некорректный запрос");
        }

        $id = $s['i'];
        $modelToken = $s['m'];

        if ($t = $this->_lookupModel($modelToken, $id)) {

            $var = CUploadedFile::getInstanceByName("image");
            $allowed_mimes = array('image/png', 'image/jpg', 'image/jpeg', 'image/gif');
            if (!in_array($var->type, $allowed_mimes)) {
                return $this->_ajaxError("Файл " . $var->name . " не является картинкой, или поврежден. ");
            }

            try {
                Yii::app()->ih->load($var->getTempName());
            } catch (Exception $e) {
                return $this->_ajaxError("Файл " . $var->name . " не является картинкой, или поврежден. ");
            }

            Helpers::headerJson();

            $t['model']->useLoadedFile($var->getTempName());
            echo CJSON::encode(array('r' => 1
            , 'url' => $t['model']->getImage()
            , 'thurl' => $t['model']->getThumbnail(false, true)));

            return;
        } else {
            $this->_ajaxError("Некорректные параметры");
            return;
        }
    }

    public function actionDelete()
    {

        $s = CJSON::decode(Yii::app()->request->getRawBody());

        if ((!isset($s['i'])) || (!isset($s['m']))) {
            return $this->_ajaxError("Некорректный запрос");
        }

        $id = $s['i'];
        $modelToken = $s['m'];

        if ($t = $this->_lookupModel($modelToken, $id)) {
            $t['model']->deleteImage();
            Helpers::headerJson();
            echo CJSON::encode(array('r' => 1));
        } else {
            echo $this->_ajaxError("Некорректные параметры");
            return;
        }
    }

    public function actionApply()
    {

        $s = CJSON::decode(Yii::app()->request->getRawBody());

        if ((!isset($s['i'])) || (!isset($s['m'])) || (!isset($s['c']['x']))) {
            return $this->_ajaxError("Некорректный запрос");
        }

        $id = $s['i'];
        $modelToken = $s['m'];

        if ($t = $this->_lookupModel($modelToken, $id)) {
            $t['model']->cropThumbnail($s['c']['x'], $s['c']['y'], $s['c']['w'], $s['c']['h'], $t['minW'], $t['minH'], '', $id);
            Helpers::headerJson();
            echo CJSON::encode(array('r' => 1, 'thurl' => $t['model']->getThumbnail(false, true)));
        } else {
            echo $this->_ajaxError("Некорректные параметры");
            return;
        }
    }
}