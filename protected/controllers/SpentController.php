<?php

class SpentController extends Controller {
    public function getZone( ) {
        return Group::SUBZONE_PROJECT_SPENTS;
    }

    public $layout = 'column1';
    private $_allowAjax = true;


    public function actionCreate() {
        $this->applyGroupPolicy(False, Group::ACTION_EDIT, Group::SUBZONE_PROJECT_SPENTS);
        $model = new Spent;
        if (isset($_GET['Spent']))
            $model->setAttributes($_GET['Spent']);

       
        if (isset($_POST['Spent'])) {
            $model->setAttributes($_POST['Spent']);
            try {
                if ($model->save()) {

                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                    }

                    Yii::app()->user->setFlash('success', "Запись создана");
                    if (isset($_GET['returnUrl'])) {
                        $this->redirect($_GET['returnUrl']);
                    } else {
                        $this->redirect($model->project->getViewUrl(array( 'noLinkedProjects'=>1, '#' => 'spents')));
                    }
                } else {
                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_lightweightForm', array(
                                'model' => $model), true))));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        }

        if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
            $this->renderPartial('_lightweightForm', array('model' => $model), false, true);
        } else {
            $this->render('create', array('model' => $model));
        }
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $this->applyGroupPolicy($model->project, Group::ACTION_EDIT, Group::SUBZONE_PROJECT_SPENTS);


        if (isset($_POST['Spent'])) {
            $model->setAttributes($_POST['Spent']);
            try {
                if ($model->save()) {

                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                    }

                    Yii::app()->user->setFlash('success', "Данные обновлены");
                    if (isset($_GET['returnUrl'])) {
                        $this->redirect($_GET['returnUrl']);
                    } else {
                        $this->redirect($model->project->getViewUrl(array( 'noLinkedProjects'=>1,  '#' => 'spents')));
                    }
                } else {
                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_lightweightForm', array(
                                'model' => $model), true))));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        }
        if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
            $this->renderPartial('_lightweightForm', array('model' => $model), false, true);
        } else {
            $this->render('update', array(
                'model' => $model,
            ));
        }
    }


    public function actionDelete($id) {
         
        if (Yii::app()->request->isPostRequest) {
            try {
                
                $m=$this->safeLoadModelProjectSubzone($id);
                $idProject=$m->idProject;
                $m->delete();
            } catch (Exception $e) {
                throw new CHttpException(500, $e->getMessage());
            }

        }
        else
            throw new CHttpException(400, Yii::t('app', 'Invalid request.'));
    }

    public function loadModel($id) {
        $model = Spent::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        return $model;
    }

}