<?php

class CustomerController extends Controller
{

    public function getZone( ) {
        return Group::ZONE_CLIENT;
    }

    public $layout = 'column1';
    private $_allowAjax = true;



    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->applyGroupPolicy($model);

        $this->mGridViewDataProvider->context="customer";
        if ($this->mGridViewDataProvider->renderPartialGridViewIfRequired(array('project-grid'), $model)) {
            return;
        }
        $this->render("view", array('model' => $model,

        ));

    }

    public function actionOverview($id)
    {
        $model = $this->loadModel($id);
        $this->applyGroupPolicy($model,Group::ACTION_VIEW);

        $this->renderPartial("_overview", array('model' => $model));

    }

    public function loadModel($id)
    {
        $model = Customer::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        return $model;
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('deny', // deny all users
                'users' => array('?'),
            ),
        );
    }

    public function actionIndex()
    {
        $this->applyGroupPolicy();
        if (isset($_GET['downloadCSV'])) {
            $this->_getCsv("customer-admin", false,false,"customers");
            return;
        }

        $model = new Customer();
        $model->unsetAttributes();


        if (isset($_GET['Customer']))
            $model->setAttributes($_GET['Customer']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionCreate()
    {
        $this->applyGroupPolicy();
        $model = new Customer;


        if (isset($_POST['Customer'])) {
            $model->setAttributes($_POST['Customer']);
            $model->attached_image = CUploadedFile::getInstance($model, 'attached_image');

            try {
                if ($model->save()) {
                    if ($model->attached_image) {
                        $model->useLoadedFile($model->attached_image->getTempName());
                    }

                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        Helpers::headerJson();
                        exit(json_encode(array('result' => 'success', 'name' => $model->nameUrl, 'idCustomer' => $model->idCustomer)));
                    }

                    Yii::app()->user->setFlash('success', "Запись создана");

                    if (isset($_GET['returnUrl'])) {
                        $this->redirect($_GET['returnUrl']);
                    } else {
                        $this->redirect(array('index'));
                    }
                } else {
                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        Helpers::headerJson();
                        exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_lightweightForm', array(
                            'model' => $model), true))));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        } elseif (isset($_GET['Customer'])) {
            $model->attributes = $_GET['Customer'];
        }
        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('_lightweightForm', array(
                'model' => $model,
            ), false, true);
        } else {
            $this->render('create', array('model' => $model));
        }
    }

    public function actionUpdate($id)
    {
        $this->applyGroupPolicy();
        $model = $this->loadModel($id);

        if (isset($_POST['Customer'])) {
            $model->setAttributes($_POST['Customer']);
            $model->attached_image = CUploadedFile::getInstance($model, 'attached_image');
            try {
                if ($model->save()) {
                    if ($model->attached_image) {
                        $model->useLoadedFile($model->attached_image->getTempName());
                    }
                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                    }

                    Yii::app()->user->setFlash('success', "Данные обновлены");

                    if (isset($_GET['returnUrl'])) {
                        $this->redirect($_GET['returnUrl']);
                    } else {
                        $this->redirect(array('index'));
                    }
                } else {
                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_lightweightForm', array(
                            'model' => $model), true))));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        }
        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('_lightweightForm', array(
                'model' => $model,
            ), false, true);
        } else {
            $this->render('update', array(
                'model' => $model,
            ));
        }
    }

    public function actionDelete($id)
    {
        $this->applyGroupPolicy();
        if (Yii::app()->request->isPostRequest) {
            try {
                $this->loadModel($id)->delete();
            } catch (Exception $e) {
                throw new CHttpException(500, $e->getMessage());
            }

            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(array('index'));
            }
        } else
            throw new CHttpException(400, Yii::t('app', 'Invalid request.'));
    }

}