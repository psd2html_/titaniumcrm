<?php
class GroupController extends Controller
{

    public function getZone( ) {
        return Group::ZONE_USER_MANAGEMENT;
    }

    public $layout = 'column1';
    private $_allowAjax = true;

    public function actionIndex()
    {
        $this->applyGroupPolicy();
        $model = new Group('search');
        $model->unsetAttributes();

        if (isset($_GET['Group']))
            $model->setAttributes($_GET['Group']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionCreate()
    {
        $this->applyGroupPolicy();
        $model = new Group;
        if (isset($_POST['Group'])) {
            $model->setAttributes($_POST['Group']);
            try {
                if ($model->save()) {


                    Yii::app()->user->setFlash('success', "Запись создана");
                    $this->redirect(array('index'));

                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        } elseif (isset($_GET['Group'])) {
            $model->attributes = $_GET['Group'];
        }

        $this->render('create', array('model' => $model));

    }

    public function actionUpdate($id)
    {
        $this->applyGroupPolicy();
        $model = $this->loadModel($id);

        if (isset($_POST['Group'])) {
            $model->setAttributes($_POST['Group']);
            try {
                if (isset($_POST['rbac'])) {
                    $model->setRules($_POST['rbac']);
                }
                if ($model->save()) {


                    Yii::app()->user->setFlash('success', "Данные обновлены");

                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }

        }

        $this->render('update', array(
            'model' => $model,
        ));

    }

    public function loadModel($id)
    {
        $model = Group::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        return $model;
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            try {
                $this->loadModel($id)->delete();
            } catch (Exception $e) {
                throw new CHttpException(500, $e->getMessage());
            }

            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(array('index'));
            }
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request.'));
    }

}