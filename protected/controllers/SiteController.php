<?php

class SiteController extends Controller
{
     public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('deny', // deny all users
                'users' => array('?'),
            ),
        );
    }
     private function bunchSettings($section, $action) {
        if (isset($_POST['doSave'])) {
            if ($_POST['doSave'] == Yii::app()->session['nonce_' . $section]) {
                Yii::app()->cache->flush();
                foreach ($_POST as $k => $v) {
                    if (($k != 'doSave') && ($k != 'yt0')) {
                        Yii::app()->settings->set($section, $k, $v, true);
                    }
                }
            }
        }

        $n = md5(Helpers::makeRandomString());
        Yii::app()->session['nonce_' . $section] = $n;
        $s = Yii::app()->settings->get($section);
        $this->render($action, array('nonce' => $n, 's' => $s));
    }
	/**
	 * Declares class-based actions.
	 */
	
        public function actionRn()
	{
            
            $this->render('rn');
		
	}
        /**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionConfig()
	{
            if (Yii::app()->user->isGuest)
            $this->redirect(array('user/login'));
                //Yii::app()->getGlobalState();
            $this->bunchSettings('general','config');
		
	}
        
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
            if (Yii::app()->user->isGuest)
            $this->redirect(array('user/login'));
                else
            $this->redirect(array('personal/index'));
		//$this->redirect(array('coreInstrument/index'));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
                    {
                
                        
	        	$this->render('error', $error);
                    }
	    }
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}