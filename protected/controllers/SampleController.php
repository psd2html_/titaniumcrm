<?php

class SampleController extends Controller
{
    public function getZone( ) {
        return Group::SUBZONE_PROJECT_ORDER;
    }

    public $layout = 'column1';

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('deny', // deny all users
                'users' => array('?'),
            ),
        );
    }

    public function actionIndex()
    {
        // ?????

        $model = new Sample('search');
        $model->unsetAttributes();

        if (isset($_GET['Sample']))
            $model->setAttributes($_GET['Sample']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionCreate()
    {
        $model = new Sample;
        if (isset($_POST['Sample'])) {
            $model->setAttributes($_POST['Sample']);

            if ($model->project) {
                $this->applyGroupPolicy($model->project, Group::ACTION_EDIT, Group::SUBZONE_PROJECT_SAMPLES);
            } else {
                throw new CHttpException(400, "Не определен проект");
            }
            try {
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', "Запись создана");
                    if (isset($_GET['returnUrl'])) {
                        $this->redirect($_GET['returnUrl']);
                    } else {
                        $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        } elseif (isset($_GET['Sample'])) {
            $model->attributes = $_GET['Sample'];
        }

        $this->render('create', array('model' => $model));
    }

    public function actionView($id)
    {
        $this->actionUpdate($id);
    }

    public function actionUpdate($id)
    {
        $model = $this->safeLoadModelProjectSubzone($id);

        if (isset($_POST['Sample'])) {
            $model->setAttributes($_POST['Sample']);
//			$model->attachments = $_POST['Sample']['attachments'];
//			$model->project = $_POST['Sample']['project'];
            try {
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', "Данные обновлены");
                    $this->redirect(array('sample/update/' . $model->idSample));
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function loadModel($id)
    {
        $model = Sample::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        return $model;
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            try {
                $this->safeLoadModelProjectSubzone($id)->delete();
            } catch (Exception $e) {
                throw new CHttpException(500, $e->getMessage());
            }

            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(array('index'));
            }
        } else
            throw new CHttpException(400, Yii::t('app', 'Invalid request.'));
    }

}