<?php

class ProjectController extends Controller
{
    public $layout = 'column1';

    public function getZone()
    {
        return Group::ZONE_PROJECT;
    }

    public function actionGetZip($id)
    {
        return; // obsolete, but async...
        $p = Project::model()->findByPk($id);

        if (!$p) {
            throw new CHttpException(403);
            return;
        }

        $this->applyGroupPolicy($p, Group::ACTION_VIEW);

        $t = $p->getArchiveName();
        $original_filename = Upload::getArchivePath($t);

        if (file_exists($original_filename)) {
            if (Yii::app()->request->isAjaxRequest) {
                Helpers::headerJson();
                echo CJSON::encode(array('r' => 1,
                    'c' => CHtml::link("Скачать архив проекта", array("/project/getZip", "id" => $id))
                ));
            } else {

                header("Content-Length: " . filesize($original_filename));
                header("Content-Type: application/octet-stream");
                header('Content-Disposition: attachment; filename="' . $t . '"');
                readfile($original_filename);
            }
        } else {
            Helpers::headerJson();
            echo CJSON::encode(array('r' => -1));
        }
    }

    public function actionDownloadArchive($id)
    {
        $p = Project::model()->findByPk($id);

        if (!$p) {
            throw new CHttpException(403);
            return;
        }

        $this->applyGroupPolicy($p, Group::ACTION_READONLY, Group::SUBZONE_PROJECT_FILES);


        ZipGenerator::buildProjectZip($p);
        ZipGenerator::downloadProjectZip($p);

    }

    public function actionManageLogisticUpload($id, $idL, $type)
    {
        $model = Project::model()->findByPk($id);

        if (!$model) throw new CHttpException(404);

        $logistic = array(UploadForm::PACKING_SHEET, UploadForm::DESCRIPTION);
        $setup = array(UploadForm::ROOM, UploadForm::SETUP);
        if (in_array($type, $logistic)) {
            $model2 = LogisticPosition::model()->findByPk($idL);
            if (!$model2) throw new CHttpException(404);
            if ($model2->idProject != $model->idProject) throw new CHttpException(404, $model2->idProject . " vs " . $model->idProject);
            $this->applyGroupPolicy($model, Group::ACTION_EDIT, Group::SUBZONE_PROJECT_LOGISTIC);
        } elseif ($type == UploadForm::CONTRACT) {
            $this->applyGroupPolicy($model, Group::ACTION_EDIT, Group::SUBZONE_PROJECT_LOGISTIC);
        } elseif (in_array($type, $setup)) {
            $this->applyGroupPolicy($model, Group::ACTION_EDIT, Group::SUBZONE_PROJECT_INSTALLATION);
        }

        switch ($type) {
            case UploadForm::CONTRACT:
                $name = 'Контракты';
                $modelToSave = $model;
                $attr = "idContractUpload";
                $attrUpload = "contractUpload";
                break;
            case UploadForm::SETUP:
                $name = 'Установка';
                $modelToSave = $model;
                $attr = "idSetupReportUpload";
                $attrUpload = "setupReportUpload";
                break;
            case UploadForm::ROOM:
                $name = 'Установка';
                $modelToSave = $model;
                $attr = "idRoomMeasurementUpload";
                $attrUpload = "roomMeasurementUpload";
                break;
            case UploadForm::DESCRIPTION:
                $name = 'Описание';
                $modelToSave = $model2;
                $attr = "idDescriptionUpload";
                $attrUpload = "descriptionUpload";
                break;
            case UploadForm::PACKING_SHEET:
                $name = 'Упаковочные';
                $modelToSave = $model2;
                $attr = "idPackingSheetUpload";
                $attrUpload = "packingSheetUpload";
                break;
            default:
                throw new CHttpException(403);
        }
        // TODO -- translate MAGIC FUCKING STRINGS to enumSystemFolder!!!!

        $folder = Folder::model()->findByAttributes(array("idProject" => $model->idProject
        , "name" => $name,
            "enumSpecialFolder" => Folder::SYSTEM_FOLDER
        ));
        if (!$folder) throw new CHttpException(404);

        $form = new UploadForm();
        $form->unsetAttributes();
        $form->enumField = $type;
        $form->idProject = $id;
        $form->idLogisticPosition = $idL;
        $form->doRemove = false;
        $form->oldUpload = $modelToSave->$attrUpload;

        if (isset($_POST['UploadForm'])) {
            $form->setAttributes($_POST['UploadForm']);
            $form->idProject = $id;

            $file = CUploadedFile::getInstance($form, 'attachment');
            /** @var $old Upload */
            $old = $modelToSave->$attrUpload;

            if ($file) {
                $new = Upload::useUploadedFileAndSave($model, $folder, $file->getTempName(), $file->name);
                if ($old) {
                    $old->delete();
                }
                $modelToSave->$attr = $new->idUpload;
                if ($modelToSave->save()) {
                    exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                } else {
                    var_dump($modelToSave->errors);
                }

            } else {
                if ($form->doRemove) {
                    if ($old) {
                        $old->delete();
                    }
                    $modelToSave->$attr = null;
                    $modelToSave->save();
                }
                exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
            }


        }

        $this->renderPartial('_uploadPopup', array('model' => $form), false, true);

    }

    public function actionDeleteLogisticPosition($id)
    {
        if (Yii::app()->request->isPostRequest) {
            try {
                $model = LogisticPosition::model()->findByPk($id);
                $this->applyGroupPolicy($model->project, Group::ACTION_UPDATE);
                $model->delete();
            } catch (Exception $e) {
                throw new CHttpException(500, $e->getMessage());
            }
        } else
            throw new CHttpException(400, Yii::t('app', 'Invalid request.'));
    }

    public function actionSetAttrJson()
    {
        $data = Yii::app()->request->getRawBody();

        $data_raw = CJSON::decode($data);

        $required = array('mdl', 'attr', 'id', 'value');
        foreach ($required as $field) {
            if (!array_key_exists($field, $data_raw)) {
                Helpers::headerJson();
                echo CJSON::encode(array('r' => -4,
                        'e' => "Устанавливаемое значение неверно")
                );
                return;
            }
        }
        $id = $data_raw['id'];
        $mdl = $data_raw['mdl'];
        switch ($mdl) {
            case "Project":
                $model = Project::model()->findByPk($id);
                $this->applyGroupPolicy($model, Group::ACTION_UPDATE);

                break;
            case "Contest":
                $model = Project::model()->findByPk($id);
                if ($model)
                    $this->applyGroupPolicy($model, Group::ACTION_UPDATE);
                $model = $model ? $model->contest : $model;

                break;
            case "LogisticPosition":
                $model = LogisticPosition::model()->findByPk($id);
                $this->applyGroupPolicy($model->project, Group::ACTION_UPDATE);

                break;
            case "Logistic":
                $model = Project::model()->findByPk($id);
                $model = $model ? $model->logistic : $model;
                break;
            default:
                throw new CHttpException(400);
        }
        if (!$model) {
            Helpers::headerJson();
            echo CJSON::encode(array('r' => -4,
                    'e' => "Модель не найдена")
            );
            return;
        }
        /* @var CActiveRecord $model */
        $attr = $data_raw['attr'];
        $attrs = array($attr => $data_raw['value']);
        $model->setAttributes($attrs);

        Helpers::headerJson();

        if (!$model->validate()) {
            Helpers::headerJson();
            echo CJSON::encode(array('r' => -2,
                'e' => implode(";", $model->getErrors($data_raw['attr']))
            ));
            return;
        }
        $model->save(true);

        Helpers::headerJson();
        echo CJSON::encode(array('r' => 1));
    }

    public function actionCreateLogisticPosition($id)
    {
        $project = Project::model()->findByPk($id);
        $this->applyGroupPolicy($project, Group::ACTION_UPDATE);

        $model = new LogisticPosition();
        $model->idProject = $id;

        if (isset($_POST['LogisticPosition'])) {
            $model->setAttributes($_POST['LogisticPosition']);
            $model->idProject = $id;

            try {
                if ($model->save()) {

                    exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));

                } else {
//                    var_dump($model->errors);
//                    return;
                    exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_createLogisticPositionPopup', array(
                        'model' => $model), true, true))));
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        }

        $this->renderPartial('_createLogisticPositionPopup', array('model' => $model), false, true);
    }

    public function actionDeleteRbac($id)
    {
        $model = AtomicRbac::model()->findByPk($id);

        if ($model && $model->enumRuleClass == AtomicRbac::RBAC_PROJECT) {
            $this->applyGroupPolicy($model->project, Group::ACTION_UPDATE);

            if (!Permission::isLoggedUserSuperadmin() and $model->project->idResponsible != Yii::app()->user->id) {
                throw new CHttpException(403, "У вас нет прав для совершения данного действия");
            }
            if ($model->delete()) {

                exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
            }
        }
    }

    public function actionUpdateRbac($id)
    {
        $model = AtomicRbac::model()->findByPk($id);

        if ($model && $model->enumRuleClass == AtomicRbac::RBAC_PROJECT) {
            $this->applyGroupPolicy($model->project, Group::ACTION_UPDATE);

            if (!Permission::isLoggedUserSuperadmin() and $model->project->idResponsible != Yii::app()->user->id) {
                throw new CHttpException(403, "У вас нет прав для совершения данного действия");
            }
            if (isset($_POST['AtomicRbac'])) {

                $model->setAttributes($_POST['AtomicRbac']);
                $model->enumRuleClass = AtomicRbac::RBAC_PROJECT;

                if ($model->save()) {

                    exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                }
            }
            $this->renderPartial('_atomicRbacPopup', array('model' => $model), false, true);
        }
    }

    public function actionCreateRbac($id)
    {

        /** @var $project Project */
        $project = Project::model()->findByPk($id);

        $this->applyGroupPolicy($project, Group::ACTION_UPDATE);

        if (!Permission::isLoggedUserSuperadmin() and $project->idResponsible != Yii::app()->user->id) {
            throw new CHttpException(403, "У вас нет прав для совершения данного действия");
        }

        if ($project) {
            $model = new AtomicRbac;

            $model->enumRuleClass = AtomicRbac::RBAC_PROJECT;
            $model->enumRuleValue = AtomicRbac::ALLOW;
            $model->idProject = $project->idProject;

            if (isset($_POST['AtomicRbac'])) {

                $model->setAttributes($_POST['AtomicRbac']);

                $model->enumRuleClass = AtomicRbac::RBAC_PROJECT;
                $model->idProject = $project->idProject;

                if ($model->save()) {

                    exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                }
                //$this->renderPartial('_atomicRbacPopup', array('model' => $project), false, true);
            }
            $this->renderPartial('_atomicRbacPopup', array('model' => $model), false, true);
        }
    }

    public function actionChooseNextWorkflow($id)
    {
        $project = Project::model()->findByPk($id);
        $isLogist = Permission::isLoggedUserLogist();
        $this->applyGroupPolicy($project, Group::ACTION_UPDATE);

        if ($project) {
            if ((Yii::app()->request->isAjaxRequest)) {
                if (isset($_POST['Project'])) {

                    if ($isLogist) {
                        if (isset($_POST['Project']['enumStatus'])) {
                            unset($_POST['Project']['enumStatus']);
                        }
                    }

                    $project->setAttributes($_POST['Project']);
                    if ($project->save()) {
                        exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                    } else {
                        var_dump($project->errors);
                    }
                }
                $this->renderPartial('stateSelector', array('model' => $project, 'isLogist' => $isLogist), false, true);
            }
        }
    }

    public function actionView($id)
    {
        $id = $id - 1000;
        $project = $this->loadModel($id);

        $this->applyGroupPolicy($project);

        if ($this->mGridViewDataProvider->renderPartialGridViewIfRequired(array('sticky-grid', 'lf-grid', 'log-grid', 'samples-grid', 'spent-grid'), $project)) {
            return;
        }

        $this->render('personal', array('model' => $project));
    }

    public function loadModel($id)
    {
        $model = Project::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        return $model;
    }

    /**
     * JSON encode
     * @param $id
     */

    public function  actionGetProjectComments($id)
    {
        $this->applyGroupPolicy($this->loadModel($id), Group::ACTION_VIEW);
        $comments = Sticky::model()->findAll(
            array(
                'condition' => "isTask=0 and idProject=:idp",
                'params' => array("idp" => $id),
                'order' => 'date DESC, idSticky DESC'
            ));

        $commentsJson = array_map(function ($o) {
                /* @var Sticky $o */

                return (array(
                    'd' => strtotime($o->date) * 1000,
                    't' => $o->text,
                    'id' => $o->idSticky
                ));
            }
            , $comments);

        Helpers::headerJson();
        echo CJSON::encode(
            array('r' => 1, 'p' => $commentsJson)
        );
    }

    public function actionMasterAndLinkedProjects($id)
    {

        $masterModel = $this->loadModel($id);
        $this->applyGroupPolicy($masterModel, Group::ACTION_VIEW);

        if (count($masterModel->childrenProjects) == 0) {
            $this->redirect('/project/view/' . $id);
        }

        $model = new Project('search');
        $model->unsetAttributes();

        if (isset($_POST['selectedProjectsId'])) {

            $slaveModels = Project::model()->findAllByPk($_POST['selectedProjectsId']);

            foreach ($slaveModels as $p) {
                $this->applyGroupPolicy($p, Group::ACTION_UPDATE);
                $p->parent = null;
                if (!$p->save()) {
                    throw new CHttpException(500, 'Saving failed');
                }
            }
            $this->refresh(true);
        }

        if (isset($_GET['Project']))
            $model->setAttributes($_GET['Project']);

        //$model->skipIdProject=$masterModel->idProject;
        $model->searchAllLinkedProjects = $masterModel->idProject;

        $this->render('masterAndLinkedProjects', array(
            'model' => $model,
            'masterModel' => $masterModel
        ));
    }

    public function actionLinkProjects($id)
    {

        $masterModel = $this->loadModel($id);
        $this->applyGroupPolicy($masterModel, Group::ACTION_VIEW);

        $model = new Project('search');
        $model->unsetAttributes();

        if (isset($_POST['selectedProjectsId'])) {


            $slaveModels = Project::model()->findAllByPk($_POST['selectedProjectsId']);
            foreach ($slaveModels as $p) {
                if ($p->idProject == $masterModel->idProject) {
                    continue;
                }

                $this->applyGroupPolicy($p, Group::ACTION_UPDATE);

                if ($p->childrenProjects) { //we allow only 2 levels of depth
                    foreach ($p->childrenProjects as $c) {
                        $c->parent = $masterModel->idProject;
                        if (!$c->save()) {
                            throw new CHttpException(500, 'Saving parent failed');
                        }
                    }
                }

                $p->parent = $masterModel->idProject;
                if (!$p->save()) {
                    throw new CHttpException(500, 'Saving failed');
                }
            }
            $this->redirect('/project/masterAndLinkedProjects/' . $masterModel->idProject);
        }
        if (isset($_GET['Project']))
            $model->setAttributes($_GET['Project']);

        $model->skipIdProject = $masterModel->idProject;
        $model->idCompanyConsumer = $masterModel->idCompanyConsumer;

        $this->render('linkProjects', array(
            'model' => $model,
            'masterModel' => $masterModel
        ));
    }

    public function actionClosedProjects()
    {
        $this->applyGroupPolicy(False, Group::ACTION_ADMIN);

        if (isset($_GET['downloadCSV'])) {
            $this->_getCsv("project-grid-admin", "closed-projects", false, "closed-projects");
            return;
        }

        $this->render('adminClosed', array( //'model' => $model,
        ));
    }

    public function actionAllProjects()
    {
        $this->applyGroupPolicy(False, Group::ACTION_ADMIN);

        if (isset($_GET['downloadCSV'])) {
            $this->_getCsv("project-grid-admin", "closed-projects", false, "closed-projects");
            return;
        }

        $this->render('adminAll', array());
    }

    public function actionIndex()
    {
        $this->applyGroupPolicy();
        if (isset($_GET['downloadCSV'])) {
            $this->_getCsv("project-grid-admin", "active-projects", false, "active-projects");
            return;
        }

        $this->render('admin', array());
    }

    public function actionEngOverview($id)
    {
        $model = $this->loadModel($id);
        $this->applyGroupPolicy($model, Group::ACTION_VIEW);
        $this->renderPartial('_engPreview', array("model" => $model));
    }

    public function actionCreate()
    {
        $this->applyGroupPolicy();

        $model = new Project;
        $model->idResponsible = Yii::app()->user->id;
        if (isset($_POST['Project'])) {
            $model->setAttributes($_POST['Project']);
            //$model->name = $model->projectName;
            try {
                if ($model->save()) {

                    if (isset($_POST['Customers']) && isset($_POST['customerComment'])) {
                        $model->updateCustomers($_POST['Customers'], $_POST['customerComment']);
                    }

                    if (isset($_POST['Contest'])) {
                        $related = $model->contest;
                        $related->setAttributes($_POST['Contest']);
                        $related->save();
                        $this->trySavingAttaches($related);
                    }
                    if (isset($_POST['Logistic'])) {
                        $related = $model->logistic;
                        $related->setAttributes($_POST['Logistic']);
                        $related->save();
                    }
                    Yii::app()->user->setFlash('success', "Запись создана");
                    if (isset($_GET['returnUrl'])) {
                        $this->redirect($_GET['returnUrl']);
                    } else {
                        $this->redirect($model->getUpdateUrl());
                    }
                } else {
                    print_r($model->getErrors());
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        } elseif (isset($_GET['Project'])) {
            $model->attributes = $_GET['Project'];
        }

        $this->render('create', array('model' => $model));
    }

    public function actionUpdateCommentAjax($id)
    {

        if (!Yii::app()->request->isAjaxRequest)
            throw new CHttpException(400, "Not ajax");

        $model = $this->loadModel($id);
        $this->applyGroupPolicy($model, Group::ACTION_UPDATE);

        if (isset($_POST['Project'])) {
            $model->setAttributes($_POST['Project']);
            if ($model->save()) {
                exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
            } else {
                exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_commentPopup', array(
                    'model' => $model), true))));
            }
        }

        $this->renderPartial('_commentPopup', array('model' => $model), false, true);
    }

    public function actionUpdate($id)
    {
        $id = $id - 1000;
        $model = $this->loadModel($id);

        $this->applyGroupPolicy($model, Group::ACTION_UPDATE);


        $contest = $model->contest;
        $logistic = $model->logistic;

        if ($model->idCoreInstrument === null) {
            $model->idCoreInstrument = 0;
        }

        if (isset($_POST['Contest'])) {
            $contest->setAttributes($_POST['Contest']);
            $contest->save();

        }
        if (isset($_POST['Logistic'])) {

            $logistic->setAttributes($_POST['Logistic']);
            $logistic->save();
        }

        if (isset($_POST['Project'])) {
            $model->setAttributes($_POST['Project']);


            if (!$model->errors) {
                try {
                    if ($model->save()) {

                        if (isset($_POST['Customers']) && isset($_POST['customerComment'])) {
                            $model->updateCustomers($_POST['Customers'], $_POST['customerComment']);
                        } else {
                            $model->updateCustomers(array(), array());
                        }
                        Yii::app()->user->setFlash('success', "Данные обновлены");
                        if (isset($_GET['returnUrl'])) {
                            $this->redirect($_GET['returnUrl']);
                        } else {
                            $this->redirect($model->getUpdateUrl());
                        }
                    }
                } catch (Exception $e) {
                    $model->addError('', $e->getMessage());
                }
            }
        }

        $this->render('update', array(
            'model' => $model,
            'contest' => $contest,
            'logistic' => $logistic,
        ));
    }


}