<?php
class ManufacturerController extends Controller
{
    public function getZone( ) {
        return Group::ZONE_MANUFACTURER;
    }

    public $layout = 'column1';
    private $_allowAjax = true;


    public function actionIndex()
    {
        $this->applyGroupPolicy();
        $model = new Manufacturer('search');
        $model->unsetAttributes();

        if (isset($_GET['Manufacturer']))
            $model->setAttributes($_GET['Manufacturer']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionCreate()
    {
        $this->applyGroupPolicy();
        $model = new Manufacturer;
        if (isset($_POST['Manufacturer'])) {
            $model->setAttributes($_POST['Manufacturer']);
            try {
                if ($model->save()) {

                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                    }

                    Yii::app()->user->setFlash('success', "Запись создана");
                    $this->redirect(array('manufacturer/update/' . $model->idManufacturer));
                } else {
                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_lightweightForm', array(
                            'model' => $model), true))));
                    }
                }


            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        } elseif (isset($_GET['Manufacturer'])) {
            $model->attributes = $_GET['Manufacturer'];
        }

        if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
            $this->renderPartial('_lightweightForm', array('model' => $model), false, true);
        } else {
            $this->render('create', array('model' => $model));
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $this->applyGroupPolicy($model);

        if (isset($_POST['Manufacturer'])) {

            $model->setAttributes($_POST['Manufacturer']);

            try {
                if ($model->save()) {

                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                    }

                    Yii::app()->user->setFlash('success', "Данные обновлены");
                    $this->redirect(array('manufacturer/update/' . $model->idManufacturer));
                } else {
                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_lightweightForm', array(
                            'model' => $model), true))));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }

        }
        if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
            $this->renderPartial('_lightweightForm', array('model' => $model), false, true);
        } else {
            $this->render('update', array(
                'model' => $model,
            ));
        }
    }

    public function loadModel($id)
    {
        $model = Manufacturer::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        return $model;
    }

}