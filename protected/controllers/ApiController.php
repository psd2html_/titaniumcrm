<?php

class ApiController extends Controller
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('deny', // deny all users
                'users' => array('?'),
            ),
        );
    }

    // ==============================================================================

    public function actionGetFeature()
    {
        $this->_ajax(new Feature('search'), array('sid', 'name'), 'fullNameUrl');
    }

    public function actionGetCompany()
    {
        $this->_ajax(new Company('search'), array('fullName', 'shortName'), 'fullNameUrl');
    }

    // ==============================================================================
    public function actionPutFeature()
    {
        $data=CJSON::decode(Yii::app()->request->getRawBody());

        Helpers::headerJson();

        if (isset($data['sid'])&&isset($data['n'])&&isset($data['m']))
        {
            $sid=trim($data['sid']);
            $m=Feature::model()->find('sid=:s',array('s'=>$sid));
            if ($m)
            {
                echo CJSON::encode(array('r'=>-1,'m'=>'Опция с SID '.$m->sid.' уже существует - '.$m->name));
                Yii::app()->end();
            }
            $man=Manufacturer::model()->findByPk($data['m']);
            if (!$man)
            {
                echo CJSON::encode(array('r'=>-1,'m'=>'Нет такого производителя'));
                Yii::app()->end();
            }
            $m=new Feature();
            $m->sid=$sid;
            $m->name=$data['n'];
            $m->idManufacturer=$man->idManufacturer;

            if (!$m->save())
            {
                echo CJSON::encode(array('r'=>-1,'m'=>'Не удалось сохранить'));
                Yii::app()->end();
            }

            echo CJSON::encode(array('r'=>1,'m'=>'-', 'v'=>$this->_encodeFeature($m)));
        } else
        {
            echo CJSON::encode(array('r'=>-1,'m'=>'Неправильные параметры'));
        }
    }
    private  function _encodeFeature($f)
    {
        return array('id'=>$f->idFeature,'name'=>$f->fullNameUrl);
    }

    /**
     *
     * @param CActiveRecord $m Search Model, i.e. Company('search')
     * @param mixed $attr Searchable attribute
     * @param type $name Value for each model
     */
    private function _ajax($m, $attr, $name)
    {

        if (isset($_GET['q'])) {
            $arr = array();

            $m->unsetAttributes();
            $criteria=new CDbCriteria();
            if (is_array($attr)) {
                foreach ($attr as $a) {
                    $criteria->compare($a, $_GET['q'],true,'OR');
                }
            } else {
                $criteria->compare($attr, $_GET['q'],true,'OR');
            }
            $ac=new CActiveDataProvider($m, array(
                'criteria' => $criteria,
            ));
            //$ac = $m->search();
            $ac->pagination->pageSize = 5;

            $models = $ac->getData();

            $ret = array();

            foreach ($models as $model) {
                $ret[$model->getPrimaryKey()] = $model->$name;
            }
            Helpers::headerJson();
            echo CJSON::encode($ret);
        }
    }
}