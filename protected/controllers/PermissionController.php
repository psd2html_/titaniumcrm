<?php

class PermissionController extends Controller {


    public function getZone( ) {
        return Group::ZONE_USER_MANAGEMENT;
    }


    public $layout = 'column1';
    private $_allowAjax = true;

    public function actionIndex() {
        $this->applyGroupPolicy();
        $model = new User('search');
        $model->unsetAttributes();

        if (isset($_GET['User']))
            $model->setAttributes($_GET['User']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    private function gridViewRefresh($id) {
        if (Yii::app()->request->isAjaxRequest) {
            switch (Yii::app()->getRequest()->getQuery('ajax')) {
                case 'rel-Relation_permissionRules-grid':


                    $m = new Permission();
                    $m->unsetAttributes();
                    $m->setAttributes(Yii::app()->getRequest()->getQuery('Permission'));

                    $m->idUser = $id;
                    $dp = $m->search();
                    $dp->pagination->route = Yii::app()->request->getPathInfo();
                    $this->renderPartial('_rules', array('dataProvider' => $dp, 'model' => $m));
                    return true;
                    break;
                default:
                    break;
            }
        }
        return false;
    }

    public function actionCreate() {
        $this->applyGroupPolicy();
        //if (gridViewRefresh($id)) return;

        $model = new User;
        if (isset($_POST['User'])) {
            $model->setAttributes($_POST['User']);

            if (isset($_POST['User']['permissions']) && ($_POST['User']['permissions'] == '')) {
                $model->permissions = array();
            }

            try {
                if ($model->save()) {

                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                    }

                    Yii::app()->user->setFlash('success', "Запись создана");
                    if (isset($_GET['returnUrl'])) {
                        $this->redirect($_GET['returnUrl']);
                    } else {
                        $this->redirect(array('index'));
                    }
                } else {
                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_lightweightForm', array(
                                'model' => $model), true))));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        } elseif (isset($_GET['User'])) {
            $model->attributes = $_GET['User'];
        }

        if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
            $this->renderPartial('_lightweightForm', array('model' => $model), false, true);
        } else {
            $this->render('create', array('model' => $model));
        }
    }

    public function actionUpdate($id) {
        $this->applyGroupPolicy();

        if ($this->gridViewRefresh($id))
            return;

        $model = $this->loadModel($id);

        if (isset($_POST['User'])) {
            $model->setAttributes($_POST['User']);


            if (isset($_POST['User']['permissions']) && ($_POST['User']['permissions'] == '')) {
                $model->permissions = array();
            }

            try {
                if ($model->save()) {

                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                    }
                    Permission::flushPermissions();
                    Yii::app()->user->setFlash('success', "Данные обновлены");
                    if (isset($_GET['returnUrl'])) {
                        $this->redirect($_GET['returnUrl']);
                    } else {
                        $this->redirect(array('index'));
                    }
                } else {
                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_lightweightForm', array(
                                'model' => $model), true))));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        }
        if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
            $this->renderPartial('_lightweightForm', array('model' => $model), false, true);
        } else {
            $this->render('update', array(
                'model' => $model,
            ));
        }
    }


    public function loadModel($id) {
        $model = User::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        return $model;
    }

}