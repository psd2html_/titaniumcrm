<?php

class PersonalController extends Controller
{
    public function actionP94438223() {
        $r=Project::model()->findAll();
        foreach ($r as $p) {
            $p->recacheOrderCount();
        }
    }

    public  function  actionTriggerFilterLocking($m)
    {
        CustomizableColumnsProvider::triggerFilterLocking($m);
    }

    public function actionRemoveFilter()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        $cid = Yii::app()->request->getParam('cid', -1);
        $mid = Yii::app()->request->getParam('mid', -1);

        if ($m = Yii::app()->user->user()->getPersonalFilter()) {
            $out = array();

            foreach ($m as $u) {
                if (($u['m'] != $mid) || ($u['c'] != $cid)) {
                    $out[] = $u;
                }
            }
            Yii::app()->user->user()->setPersonalFilter($out);
            Yii::app()->user->user()->save();
        }
    }

    public function actionFetchCalendarEvents()
    {
        $myId=Yii::app()->user->id;

        $startUnix = Yii::app()->request->getParam('start', -1);
        $endUnix = Yii::app()->request->getParam('end', -1);

        //$project = new Project();
        //$project->unsetAttributes();
        $c=new CDbCriteria();
        $outUglySql = Permission::getUglySql(true);

        $c->addCondition($outUglySql);
        $c->addCondition("atomicRbacs.idUser=".$myId ,"OR");
        $c->with=array('companyConsumer','sticky','logisticRelated','contestRelated','logisticFragments','orders','atomicRbacs');
        $projectDataProvider = new CActiveDataProvider("Project", array('criteria'=>$c,'pagination'=>false));

        $projects = $projectDataProvider->getData();
        $dates = array();
        foreach ($projects as $p) {
            $arr = $p->getCalendarEvents(true, Yii::app()->createUrl('/personal/index'), $startUnix, $endUnix);
            $dates = array_merge($dates, $arr);

        }
        Helpers::headerJson();
        echo CJSON::encode($dates);
    }

    public function actionCreateFilter()
    {

        $model = new Permission;

        if (isset($_POST['Permission'])) {

            $model->setAttributes($_POST['Permission']);

            if (($model->manufacturer) && ($model->coreInstrumentClass)) {
                $m = Yii::app()->user->user()->getPersonalFilter();
                $m[] = array('m' => $model->idManufacturer, 'c' => $model->idCoreInstrumentClass);
                Yii::app()->user->user()->setPersonalFilter($m);
                if (Yii::app()->user->user()->save()) {
                    exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                }
            }
        } elseif (isset($_GET['Permission'])) {
            $model->attributes = $_GET['Permission'];

        }

        if (true || (Yii::app()->request->isAjaxRequest)) {
            $this->renderPartial('_personalFilter', array('model' => $model), false, true);
        }


    }

    public function actionSwitchPreset() {
        if ((!Yii::app()->request->isAjaxRequest)) {
            throw new CHttpException(400, "Does not compute");
        }

        $model = Yii::app()->user->user();

        $gid=Yii::app()->request->getParam("gid",null);
        $newPresetId=Yii::app()->request->getParam("preset",null);

        if (($newPresetId!==null)&&($gid!==null)) {
            CustomizableColumnsProvider::selectPreset($gid, $newPresetId);
        }


    }

    public function actionCustomizeColumns($gid)
    {
        if ((!Yii::app()->request->isAjaxRequest)) {
            throw new CHttpException(400, "Does not compute");
        }

        $model = Yii::app()->user->user();

        if (isset($_POST['CustomizableColumnsForm'])) {
            list($oldPresetId, $oldPresetName) = CustomizableColumnsProvider::getCurrentPreset($gid);
            $newPresetId = isset($_POST['CustomizableColumnsForm']['changePresetTo'])?$_POST['CustomizableColumnsForm']['changePresetTo']:null;
            if (($newPresetId!==null)&&($newPresetId!=="")) {
                CustomizableColumnsProvider::setSettings($gid, $_POST['CustomizableColumnsForm']['columns'], $_POST['CustomizableColumnsForm']['presetName'], $_POST['presetOrder']);
                $newPresetIdAfterResort=
                    array_search($newPresetId,$_POST['presetOrder'] );
                if ($newPresetIdAfterResort===false) {
                    $newPresetIdAfterResort=$newPresetId;
                }
                CustomizableColumnsProvider::selectPreset($gid, $newPresetId);
                $model = new CustomizableColumnsForm();
                $model->columns = CustomizableColumnsProvider::getSettings($gid);
                list($model->currentPreset, $model->presetName) = CustomizableColumnsProvider::getCurrentPreset($gid);
                $presets = CustomizableColumnsProvider::getPresets($gid);

                $val = $this->renderPartial('_customizeColumns', array(
                    'model' => $model,
                    'columns' => CustomizableColumnsProvider::getAllColumnsDataList($gid),
                    'gid' => $gid,
                    'presets' => $presets,
                ), true, true);
                Helpers::headerJson();
                exit(CJSON::encode(array('result' => 'reload', 'msg' => $val)));

            } else {
                CustomizableColumnsProvider::setSettings($gid, $_POST['CustomizableColumnsForm']['columns'], $_POST['CustomizableColumnsForm']['presetName'], $_POST['presetOrder']);
            }
            Helpers::headerJson();
            exit(CJSON::encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
        }

        $model = new CustomizableColumnsForm();
        $model->columns = CustomizableColumnsProvider::getSettings($gid);
        list($model->currentPreset, $model->presetName) = CustomizableColumnsProvider::getCurrentPreset($gid);
        $presets = CustomizableColumnsProvider::getPresets($gid);

        $this->renderPartial('_customizeColumns', array(
            'model' => $model,
            'columns' => CustomizableColumnsProvider::getAllColumnsDataList($gid),
            'gid' => $gid,
            'presets' => $presets,
        ), false, true);

    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('deny', // deny all users
                'users' => array('?'),
            ),
        );
    }

    public function actionAccount()
    {
        $CUserPreferencesForm=new CUserPreferencesForm();

        if (isset($_POST['CUserPreferencesForm'])) {
            $CUserPreferencesForm->setAttributes($_POST['CUserPreferencesForm']);
            if ($CUserPreferencesForm->applySettings()) {
                Yii::app()->user->setFlash('success', "Предпочтения изменены");
            }
        } else {
            $CUserPreferencesForm->fillSettings();
        }

        if (isset($_POST['submitButton'])) {
            if ($_POST['submitButton']=='delAll') {
                PersonalMessageService::deleteAllPm();
            } elseif ($_POST['submitButton']=='delSel') {
                PersonalMessageService::deleteSelectedPm(isset($_POST['markedPmId'])?$_POST['markedPmId']:array());
            } elseif ($_POST['submitButton']=='archSel') {
                PersonalMessageService::archiveSelectedPm(isset($_POST['markedPmId'])?$_POST['markedPmId']:array());
            }
        }
        $changePass = new UserChangePassword;


        if (Yii::app()->user->id) {
            $u = Yii::app()->user->model();

            $profile = $u->profile;

            if (isset($_POST['User'])) {
                $vPass = isset($_POST['User']['password']) ? $_POST['User']['password'] : 'ololo';
                if (isset($_POST['User']['password'])) {
                    unset($_POST['User']['password']);
                }

                $u->attributes = $_POST['User'];
                $profile->attributes = $_POST['Profile'];

                if (User::model()->notsafe()->findByPk(Yii::app()->user->id)->password != Yii::app()->getModule('user')->encrypting($vPass)) {
                    $u->addError("password", "Пароль неверен");
                } else {
                    //$u->password=null;
                    if ($u->validate() && $profile->validate()) {
                        Yii::app()->user->setFlash('success', "Профиль изменен");
                        $u->save();
                        $profile->save();
                    } else $profile->validate();
                }
            }

            if (isset($_POST['UserChangePassword'])) {
                $changePass->attributes = $_POST['UserChangePassword'];
                if ($changePass->validate()) {
                    $new_password = User::model()->notsafe()->findbyPk(Yii::app()->user->id);
                    $new_password->password = UserModule::encrypting($changePass->password);
                    $new_password->activkey = UserModule::encrypting(microtime() . $changePass->password);

                    $new_password->save();
                    Yii::app()->user->setFlash('success', "Пароль изменен");
                }
            }
            $newMessagesCount = PersonalMessageService::getAmountOfUnreadMessages();


            $this->render('account', array('newMessagesCount' => $newMessagesCount, 'changePass' => $changePass, 'model' => $u,
                'profile' => $profile, 'userPreferencesForm'=>$CUserPreferencesForm));
        }
    }

    public function actionIndex()
    {
        $project = new Project();
        $project->unsetAttributes();
        $project->overrideFilter = true;

        if ($this->mGridViewDataProvider->renderPartialGridViewIfRequired(array('tomeactive-sticky-grid', 'bymeactive-sticky-grid', 'done-sticky-grid'), $project)) {
            return;
        }

        $projectDataProvider = $project->search(true);

        $this->render('index', array(
            'projectDataProvider' => $projectDataProvider,
        ));
    }

    protected function getDataFormatted($data)
    {
        foreach ($data as $k => $person) {
            $personFormatted[$k] = $this->formatData($person);
            $parents = null;
            if (isset($person['parents'])) {
                $parents = $this->getDataFormatted($person['parents']);
                $personFormatted[$k]['children'] = $parents;
            }
        }
        return $personFormatted;
    }

    protected function formatData($person)
    {
        return array(
            'text' => $person['name'],
            'id' => $person['id'],
            'hasChildren' => isset($person['parents']));
    }

    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */
}