<?php
class LogisticFragmentController extends Controller
{

    private function trySavingAttaches($model) {

        for ($i = 1; $i <= 3; $i++) {


            $file = CUploadedFile::getInstance($model, 'attachment' . $i);

            if ($file) {

                $k = new Attachment();
                $k->attached_file = $file;
                $k->belongId = $model->getPrimaryKey();
                $k->belongType = Attachment::BELONGS_TO_LOGISTICFRAGMENT;
                if (!$k->save()) {
                    die();
                }
                $linkTableName = 'attachement2logisticfragment';
                $columnName = 'idLogisticFragment';

                Yii::app()->db->createCommand('insert into ' . $linkTableName . ' (' . $columnName . ', idAttachment) values ('.$model->getPrimaryKey().', '.$k->getPrimaryKey().')')
                    ->execute();
            }
        }
    }

    public $layout = 'column1';
    private $_allowAjax = true;

    public function actionIndex()
    {
        $model = new LogisticFragment('search');
        $model->unsetAttributes();

        if (isset($_GET['LogisticFragment']))
            $model->setAttributes($_GET['LogisticFragment']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionCreate($id)
    {
        $model = new LogisticFragment;
        $model->idProject = $id;

        if (isset($_POST['LogisticFragment'])) {
            $model->setAttributes($_POST['LogisticFragment']);
            try {
                if ($model->save()) {
                    $this->trySavingAttaches($model);
                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                    }

                    Yii::app()->user->setFlash('success', "Запись создана");

                    $this->redirect(array('logisticFragment/update/'.$model->getPrimaryKey()));

                } else {
                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_lightweightForm', array(
                            'model' => $model), true, true))));
                    }
                }


            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        } elseif (isset($_GET['LogisticFragment'])) {
            $model->attributes = $_GET['LogisticFragment'];
        }

        if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
            $this->renderPartial('_lightweightForm', array('model' => $model), false, true);
        } else {
            $this->render('create', array('model' => $model));
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (isset($_POST['LogisticFragment'])) {
            $model->setAttributes($_POST['LogisticFragment']);
            try {
                if ($model->save()) {
                    $this->trySavingAttaches($model);
                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                    }

                    Yii::app()->user->setFlash('success', "Данные обновлены");

                    $this->redirect(array('logisticFragment/update/' . $model->getPrimaryKey()));

                } else {
                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_lightweightForm', array(
                            'model' => $model), true, true))));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }

        }
        if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
            $this->renderPartial('_lightweightForm', array('model' => $model), false, true);
        } else {
            $this->render('update', array(
                'model' => $model,
            ));
        }
    }

    public function loadModel($id)
    {
        $model = LogisticFragment::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        return $model;
    }

    public function actionDeleteAjax()
    {
        return false;
        if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
            $id = Yii::app()->request->getPost('mid', 0);
            try {
                $this->loadModel($id)->delete();
            } catch (Exception $e) {
                throw new CHttpException(500, $e->getMessage());
            }

            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(array('index'));
            }

            Helpers::headerJson();
            echo(CJSON::encode(array('result' => 1, 'mid' => $id)));
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request.'));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            try {
                $this->loadModel($id)->delete();
            } catch (Exception $e) {
                throw new CHttpException(500, $e->getMessage());
            }

            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(array('index'));
            }
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request.'));
    }

}