<?php

class ManagedBlockController extends Controller {
    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('deny', // deny all users
                'users' => array('?'),
            ),
        );
    }
    public $layout = 'column1';

    public function actionIndex($id) {
        $modelCI = CoreInstrument::model()->findByPk($id);
        if (!$modelCI) {
            throw new CHttpException(400, 'Не задан id установки');
            return;
        }

        $model = new ManagedBlock('search');
        $model->unsetAttributes();
        $model->idCoreInstrument = $modelCI->idCoreInstrument;

        if (isset($_GET['ManagedBlock']))
            $model->setAttributes($_GET['ManagedBlock']);

        $this->render('admin', array(
            'modelCI' => $modelCI,
            'model' => $model,
        ));
    }

    private function syncFeatures() {
        /* array (size=6)
          'ManagedBlock' =>
          array (size=3)
          'idFeatureGroup' => string '-1' (length=2)
          'comment' => string 'ууу' (length=6)
          'enumRule' => string '2' (length=1)
          'customGroup' => string 'ололо' (length=10)
          'multiselect_autocompleter' => string '-1' (length=2)
          'Autocomplete' => string 'FP 6597/05' (length=10)
          'theatres' =>
          array (size=2)
          0 => string '1' (length=1)
          1 => string '2' (length=1)
          'yt1' => string 'Save' (length=4)
         */
    }

    public function actionCreate() {
        if (!isset($_POST['ManagedBlock'])) {
            if (!isset($_GET['idCoreInstrument'])) {
                throw new CHttpException(400, 'Не задан id установки');
                return;
            } else {
                $modelCI = CoreInstrument::model()->findByPk($_GET['idCoreInstrument']);
                if (!$modelCI) {
                    throw new CHttpException(400, 'Не задан id установки');
                    return;
                }
            }
            $model = new ManagedBlock;
            $model->idCoreInstrument = $_GET['idCoreInstrument'];
        } else {
            $model = new ManagedBlock;
            $model->setAttributes($_POST['ManagedBlock']);
            $modelCI = CoreInstrument::model()->findByPk($model->idCoreInstrument);
        }


        if (isset($_POST['ManagedBlock'])) {
            //var_dump($_POST);
            $model->setAttributes($_POST['ManagedBlock']);
            if ($model->idFeatureGroup == -1) {
                if (!isset($_POST['customGroup'])) {
                    $model->addError('', 'Пожалуйста заполните поле с именем для создания группы');
                    $this->render('create', array('model' => $model, 'modelCI' => $modelCI));
                    return;
                }
                $fg = new FeatureGroup;
                $fg->parent = null;
                $fg->name = $_POST['customGroup'];
                $fg->idCoreInstrument = $model->idCoreInstrument;
                if (!$fg->save()) {
                    $model->addError('', 'Не удалось создать группу');
                    $this->render('create', array('model' => $model, 'modelCI' => $modelCI));
                    return;
                }
                $model->idFeatureGroup = $fg->idFeatureGroup;
            }

            try {
                $model->save_features = isset($_POST['features']) ? $_POST['features'] : array();
                if ($model->save()) {
                    /* foreach ($_POST['features'] as $f) {
                      $lnk = new ManagedBlockFeature();
                      $lnk->idFeature = $f;
                      $lnk->idManagedBlock = $model->idManagedBlock;
                      $lnk->save();
                      } */
                    Yii::app()->user->setFlash('success', "Запись создана");

                    $this->redirect(Yii::app()->createUrl("coreInstrument/update", array('id' => $model->idCoreInstrument,'#'=>'form')));
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        } elseif (isset($_GET['ManagedBlock'])) {
            $model->attributes = $_GET['ManagedBlock'];
        }

        $this->render('create', array('model' => $model, 'modelCI' => $modelCI));
    }

    public function actionUpdate($id) {

        $model = $this->loadModel($id);

        if (isset($_POST['ManagedBlock'])) {
            $model->setAttributes($_POST['ManagedBlock']);



            if ($model->idFeatureGroup == -1) {
                if (!isset($_POST['customGroup'])) {
                    $model->addError('', 'Пожалуйста заполните поле с именем для создания группы');
                    $this->render('update', array(
                        'model' => $model,
                    ));
                    return;
                }
                $fg = new FeatureGroup;
                $fg->parent = null;
                $fg->name = $_POST['customGroup'];
                $fg->idCoreInstrument = $model->idCoreInstrument;
                if (!$fg->save()) {
                    $model->addError('', 'Не удалось создать группу');
                    $this->render('update', array(
                        'model' => $model,
                    ));

                    return;
                }
                $model->idFeatureGroup = $fg->idFeatureGroup;
            }


            try {
                $model->save_features = isset($_POST['features']) ? $_POST['features'] : array();
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', "Данные обновлены");
                    $this->redirect(Yii::app()->createUrl("coreInstrument/update", array('id' => $model->idCoreInstrument,'#'=>'form')));
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            try {
                $this->loadModel($id)->delete();
            } catch (Exception $e) {
                throw new CHttpException(500, $e->getMessage());
            }

            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(array('index'));
            }
        }
        else
            throw new CHttpException(400,
                    Yii::t('app', 'Invalid request.'));
    }

    public function actionDeleteAjax() {
        if (Yii::app()->request->isPostRequest) {
            $id = Yii::app()->request->getPost('mid', 0);

            try {
                 $this->loadModel($id)->delete();
            } catch (Exception $e) {
                throw new CHttpException(500, $e->getMessage());
            }


            Helpers::headerJson();
            echo(CJSON::encode(array('result' => 1, 'mid' => $id)));
        }
        else
            throw new CHttpException(400,
                    Yii::t('app', 'Invalid request.'));
    }

    public function loadModel($id) {
        $model = ManagedBlock::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        return $model;
    }

}