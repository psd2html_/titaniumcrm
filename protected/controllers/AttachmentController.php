<?php

class AttachmentController extends Controller {
     public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('deny', // deny all users
                'users' => array('?'),
            ),
        );
    }
    
    public $layout = 'column1';

    public function actionDownload($id) {
        
        $m=$this->loadModel($id);
        
        $original_filename = $m->getAttachmentsFolder().$m->filename;
        if (file_exists($original_filename)) {
        $new_filename = $m->originalFilename;

// headers to send your file
        //header("Content-Type: application/jpeg");
        header("Content-Length: " . filesize($original_filename));
        header('Content-Disposition: attachment; filename="' . $new_filename . '"');

// upload the file to the user and quit
        readfile($original_filename);
        }
        else
        {
            throw new CHttpException(404,'Attachment not found.');
        }
    }

    public function actionIndex() {
        $model = new Attachment('search');
        $model->unsetAttributes();

        if (isset($_GET['Attachment']))
            $model->setAttributes($_GET['Attachment']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionCreate() {
        //@todo: Refactor this shit!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        $model = new Attachment;

        if (isset($_POST['Attachment'])) {
            $model->setAttributes($_POST['Attachment']);

            $file = CUploadedFile::getInstance($model, 'attached_file');
            $model->attached_file = $file;
            switch ($model->belongType) {
                case Attachment::BELONGS_TO_CONTRAGENT:
                    $mr = Contragent::model()->findByPk($model->belongId);
                    if ($mr) {
                        $linkTableName = 'attachment2contragent';
                        $columnName='idContragent';
                    }
                    break;
                case Attachment::BELONGS_TO_EVENT:
                    $mr = Event::model()->findByPk($model->belongId);
                    if ($mr) {
                        $linkTableName = 'attachment2event';
                        $columnName='idEvent';
                    }
                    break;

                case Attachment::BELONGS_TO_PROJECT:
                    $mr = Project::model()->findByPk($model->belongId);
                    if ($mr) {
                        $linkTableName = 'attachment2project';
                        $columnName='idProject';
                    }
                    break;
                case Attachment::BELONGS_TO_SAMPLE:
                    $mr = Sample::model()->findByPk($model->belongId);
                    if ($mr) {
                        $linkTableName = 'attachment2sample';
                        $columnName='idSample';
                    }
                    break;
                default:
                    die();
            }

            /*
              var_dump($file);
              if (!$file)
              {
              echo($file->name.'<br/>');
              echo($file->tempName.'<br/>');

              }

              die();
             */

            try {
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', "Запись создана");

                    if ($mr) {
                        Yii::app()->db->createCommand('insert into ' . $linkTableName . ' ('.$columnName.', idAttachment) values ('.$mr->getPrimaryKey().','.$model->getPrimaryKey().')')
                                ->execute();
                    }

                    if (isset($_GET['returnUrl'])) {
                        $this->redirect($_GET['returnUrl']);
                    } else {
                        $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        } elseif (isset($_GET['Attachment'])) {
            $model->attributes = $_GET['Attachment'];
        }

        $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        if (isset($_POST['Attachment'])) {
            $model->setAttributes($_POST['Attachment']);
            try {
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', "Данные обновлены");
                    if (isset($_GET['returnUrl'])) {
                        $this->redirect($_GET['returnUrl']);
                    } else {
                        $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            try {
                $this->loadModel($id)->delete();
            } catch (Exception $e) {
                throw new CHttpException(500, $e->getMessage());
            }

            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(array('index'));
            }
        }
        else
            throw new CHttpException(400,
                    Yii::t('app', 'Invalid request.'));
    }

    public function loadModel($id) {
        $model = Attachment::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        return $model;
    }

}