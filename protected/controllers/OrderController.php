<?php

class OrderController extends Controller {
    public function getZone( ) {
        return Group::SUBZONE_PROJECT_ORDER;
    }

    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('deny', // deny all users
                'users' => array('?'),
            ),
        );
    }

    public $layout = 'column1';


    public function actionGetFeaturesAjax() {
        if (Yii::app()->request->isAjaxRequest) {
            $id = (Yii::app()->request->getParam('idCoreInstrument', -1));
            if ($id<=0) {
                $this->renderPartial('_featuresForNoCoreInstrument', array(), false, true);
                return;
            }
            $model = CoreInstrument::model()->with('featureGroups')->findByPk($id);

            if ($model) {
                $this->renderPartial('_features', array('model' => $model, 'features' => array()), false, true);
            }
        }
    }

    public function actionCreate() {
        $model = new Order;

        if (isset($_GET['Order'])) {
            $model->setAttributes($_GET['Order']);
            if ($model->project) {
                if ($model->project->idCoreInstrumentClass) {
                    $model->idCoreInstrumentClass = $model->project->idCoreInstrumentClass;
                }
                if ($model->project->idManufacturer) {
                    $model->idManufacturer = $model->project->idManufacturer;
                }

                if ($model->project->idCoreInstrument) {
                    $model->idCoreInstrument = $model->project->idCoreInstrument;
                } else {
                    $model->idCoreInstrument = 0;
                }

                if ($model->project->companyConsumer) {
                    $model->destinationCity=$model->project->companyConsumer->city;
                    $model->destinationCityGenetivus=$model->project->companyConsumer->cityGenetivus?$model->project->companyConsumer->cityGenetivus:$model->project->companyConsumer->city;
                }
                $this->applyGroupPolicy($model->project, Group::ACTION_EDIT, Group::SUBZONE_PROJECT_ORDER);
            } else {
                throw new CHttpException(400, "Не определен проект");
            }
        }  else {
            throw new CHttpException(400, "Не определен проект");
        }
        $model->idUser = Yii::app()->user->id;
        if (isset($_POST['Order'])) {
            $model->setAttributes($_POST['Order']);
            $model->idUser = Yii::app()->user->id;
                try {
                    if ($model->save()) {
                        if (isset($_POST['Order']['features']) && isset($_POST['Order']['featuresAmount'])) {
                            $model->saveFeatures($_POST['Order']['features'], $_POST['Order']['featuresAmount']);
                        } else {
                            $model->saveFeatures(array(), array());
                        }

                        if (isset($_POST['ungroupedFeatures']) && isset($_POST['Amount'])) {
                            $model->saveUngroupedFeatures($_POST['ungroupedFeatures'], $_POST['Amount']);
                        } else {
                            $model->saveUngroupedFeatures(array(), array());
                        }

                        Yii::app()->user->setFlash('success', "Запись создана");

                            $this->redirect(array('order/update/' . $model->idOrder));

                    }
                } catch (Exception $e) {
                    $model->addError('', $e->getMessage());
                }
        }

        $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id) {

        $model = $this->safeLoadModelProjectSubzone($id);

        if (isset($_POST['Order'])) {
            $model->setAttributes($_POST['Order']);

            if (isset($_POST['saveAsReversion']))
            {
                $model->isNewRecord=true;
                $model->idOrder=null;
                $model->idUser = Yii::app()->user->id;
                $model->idOrderInProject=null;
                $model->date = Yii::app()->dateFormatter->format("yyyy-M-d", time());
            }

            try {
                if ($model->save()) {
                    if (isset($_POST['Order']['features']) && isset($_POST['Order']['featuresAmount'])) {
                        $model->saveFeatures($_POST['Order']['features'], $_POST['Order']['featuresAmount']);
                    } else {
                        $model->saveFeatures(array(), array());
                    }

                    if (isset($_POST['ungroupedFeatures']) && isset($_POST['Amount'])) {
                        $model->saveUngroupedFeatures($_POST['ungroupedFeatures'], $_POST['Amount']);
                    } else {
                        $model->saveUngroupedFeatures(array(), array());
                    }

                    Yii::app()->user->setFlash('success', "Данные обновлены");
                    $this->redirect(array('order/update/' . $model->idOrder));
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        }

        if ($model->idCoreInstrument === null) {$model->idCoreInstrument = 0;}
        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionOverview($id) {
        $model = $this->safeLoadModelProjectSubzone($id,Group::ACTION_VIEW);
        $this->renderPartial('_overview', array('model' => $model));
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            try {
                $this->safeLoadModelProjectSubzone($id)->delete();
            } catch (Exception $e) {
                throw new CHttpException(500, $e->getMessage());
            }

            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(array('index'));
            }
        }
        else
            throw new CHttpException(400, Yii::t('app', 'Invalid request.'));
    }

    public function loadModel($id) {
        $model = Order::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        return $model;
    }

}