<?php

class AjaxController extends Controller
{


    public function actionGetAccounting()
    {
        Helpers::headerJson();

        if (!isset($_POST['json'])) {
            throw new CHttpException(400, 'lol');
        }
        $data = CJSON::decode($_POST['json']);


        if (!isset($data['ci'])) {
            return;
        }

        $f = array();

        if (isset($data['fg'])) {
            $f = (array_merge($f,$data['fg']));
        }

        if (isset($data['fug'])) {
            $f = (array_merge($f, $data['fug']));
        }


        $amount = array();
        $orderMap = array();

        // $f['i']=id
        // $f['a']=amount
        foreach ($f as $fs) {
            //var_dump($fs); return;
            if (key_exists((int)$fs['i'], $amount)) {
                $amount[(int)$fs['i']] = $amount[(int)$fs['i']] + $fs['a'];
            } else {
                $amount[(int)$fs['i']] = $fs['a'];
                $orderMap[]=(int)$fs['i'];

            }
        }

        if ($data['ci'] != 0) {
            $ci = CoreInstrument::model()->findByPk($data['ci']);

            if (!$ci)
                return;
        }
        if (count($orderMap)>0) {
            $features = Feature::model()->findAll("idFeature IN (".implode(", ", $orderMap).") ORDER BY FIELD (idFeature, ".implode(", ", $orderMap).")");
        } else {
            $features=array();
        }

        $sum = 0;

        foreach ($features as $feature) {
            $feature->count = $amount[$feature->idFeature];
            if ($feature->price) {
                $sum = $sum + $feature->price * $feature->count;
            }
        }

        $stuff = array();
        if ($data['ci'] != 0) {
            $stuff[] = $ci;
        }

        $stuff = array_merge($stuff, $features);

        $html = $this->renderPartial("_accounting", array('features' => $stuff), true);

        echo CJSON::encode(array('html' => $html, 'sum' => $sum));
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('deny', // deny all users
                'users' => array('?'),
            ),
        );
    }

    public function actionCity()
    {
        $this->render('_city');
    }

    public function actionGetManufacturers()
    {
        if (!isset($_POST['idCoreInstrumentClass'])) {
            return;
        }
        $idCoreInstrumentClass = $_POST['idCoreInstrumentClass'];
        $data_raw = Manufacturer::findAllWhoHasCoreInstrumentClass($idCoreInstrumentClass);
        $data = CHtml::listData($data_raw, 'idManufacturer', 'name');

        $outMan = '';
        $outCI = '';
        if (count($data) > 0) {

            $outMan .= CHtml::tag('option', array('value' => '', 'selected' => 'selected'), 'Выберите производетеля', true);


            $outCI .= CHtml::tag('option', array('value' => ''), 'Выберите базовый прибор', true);;
            $outCI .= CHtml::tag('option', array('value' => '0'), 'Без базового прибора', true);;

        } else {
            $outMan .= CHtml::tag('option', array('value' => ''), 'Производителей оборудования данного класса не найдено', true);
            $outCI .= CHtml::tag('option', array('value' => ''), 'Оборудования данного класса не найдено', true);;
            $outCI .= CHtml::tag('option', array('value' => '0'), 'Без базового прибора', true);;
        }
        foreach ($data as $value => $name) {
            $outMan .= CHtml::tag('option', array('value' => $value), CHtml::encode($name), true);
        }
        Helpers::headerJson();
        echo CJSON::encode(array('manufacturers' => $outMan, 'coreinstruments' => $outCI));
    }

    public function actionGetRegions()
    {

        $m = new Region();
        $m->unsetAttributes();

        if (isset($_POST['idCountry'])) {
            $m->idCountry = $_POST['idCountry'];
        } else {
            return;
        }

        $ac = $m->search();
        $ac->pagination = false;
        $data = $ac->getData();

        $data = CHtml::listData($data, 'idRegion', 'name');

        echo CHtml::tag('option', array('value' => '-1', 'selected' => 'selected'), 'Выберите регион', true);
        foreach ($data as $value => $name) {
            echo CHtml::tag('option', array('value' => $value), CHtml::encode($name), true);
        }
    }

    public function actionGetCities()
    {

        $m = new City();
        $m->unsetAttributes();

        if (isset($_POST['idRegion'])) {
            $m->idRegion = $_POST['idRegion'];
        } else {
            return;
        }

        $ac = $m->search();

        $ac->pagination = false;
        $data = $ac->getData();

        $data = CHtml::listData($data, 'idCity', 'name');

        foreach ($data as $value => $name) {
            echo CHtml::tag('option', array('value' => $value), CHtml::encode($name), true);
        }
    }

    // ----------------------------------

    public function actionGetCoreInstruments()
    {

        $m = new CoreInstrument();
        $m->unsetAttributes();

        if (isset($_POST['idManufacturer'])) {
            $m->idManufacturer = $_POST['idManufacturer'];
        }
        if (isset($_POST['idCoreInstrumentClass'])) {
            $m->idCoreInstrumentClass = $_POST['idCoreInstrumentClass'];
        }
        $ac = $m->search();
        $ac->pagination = false;
        $data = $ac->getData();

        $data = CHtml::listData($data, 'idCoreInstrument', 'name');

        if (count($data) > 1) {
            echo CHtml::tag('option', array('value' => '', 'selected' => 'selected'), 'Выберите базовый прибор', true);
        } else if (count($data) == 0) {
            echo CHtml::tag('option', array('value' => ''), 'Сочетаний не найдено', true);
        }
        foreach ($data as $value => $name) {
            echo CHtml::tag('option', array('value' => $value), CHtml::encode($name), true);
        }

        if (!isset($_POST['noCI']))
            echo CHtml::tag('option', array('value' => '0'), 'Без базового прибора', true);;
    }

    // ----------------------------------

    public function actionGetCentres()
    {

        $m = new Centre();
        $m->unsetAttributes();

        if (isset($_POST['idManufacturer'])) {
            $m->searchManufacturer = $_POST['idManufacturer'];
        }

        $ac = $m->search();
        $ac->pagination = false;
        $data = $ac->getData();

        $data = CHtml::listData($data, 'idCentre', 'name');

        if (count($data) > 1) {
            echo CHtml::tag('option', array('value' => '', 'selected' => 'selected'), 'Выберите центр', true);
        } else if (count($data) == 0) {
            echo CHtml::tag('option', array('value' => ''), 'Сочетаний не найдено', true);
        }

        foreach ($data as $value => $name) {
            echo CHtml::tag('option', array('value' => $value), CHtml::encode($name), true);
        }
    }

    // ----------------------------------


    public function actionGetCoreInstrumentClasses()
    {
        if (isset($_POST['idManufacturer'])) {
            $m = Manufacturer::model()->findByPk($_POST['idManufacturer']);

            if ($m) {

                $dataRaw = $m->coreInstrumentClasses;
                $data = array();
                if (isset($_POST['all']) && ($_POST['all'] > 0)) {
                    $data[0] = "Все";
                }
                $data = $data + CHtml::listData($dataRaw, 'idCoreInstrumentClass', 'name');


                foreach ($data as $value => $name) {
                    echo CHtml::tag('option', array('value' => $value), CHtml::encode($name), true);
                }
            }
        }
    }

    public function actionGetAllowedCoreInstrumentClasses()
    {

        if (isset($_POST['idManufacturer'])) {
            $m = Manufacturer::model()->findByPk($_POST['idManufacturer']);

            if ($m) {

                $dataRaw = $m->allowedCoreInstrumentClasses;
                $data = array();
                if (isset($_POST['all']) && ($_POST['all'] > 0)) {
                    $data[0] = "Все";
                }
                $data = $data + CHtml::listData($dataRaw, 'idCoreInstrumentClass', 'name');

                if (count($data) == 0) {
                    echo CHtml::tag('option', array('value' => ''), 'Разрешенных классов не найдено', true);
                } else {
                    foreach ($data as $value => $name) {
                        echo CHtml::tag('option', array('value' => $value), CHtml::encode($name), true);
                    }
                }
            }
        }
    }

    public function actionCurrencyCourseBriefAjaxFallback()
    {
        $a=new currencyAjax();
        list($d,$type)=$a->getCoursesData();
        Helpers::headerJson();
        echo CJSON::encode(array('V' => $d,'R'=>$type));
    }
    public function actionCurrencyCourseBriefAjax()
    {
        $ch = curl_init();
        $cv = curl_version();
        $user_agent = "curl";
        curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);

        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
        curl_setopt($ch, CURLOPT_URL, "http://www.cbr.ru/scripts/XML_daily.asp");
        $xml = curl_exec($ch);
        curl_close($ch);

        Helpers::headerUTF8();

        $xml = new SimpleXMLElement($xml);
        $lbl = array(
            'R01239' => Order::CURRENCY_EUR,
            'R01235' => Order::CURRENCY_USD,
            'R01035' => Order::CURRENCY_GBP,
            'R01820' => Order::CURRENCY_YEN,
        );

        $out = array();
        $out[Order::CURRENCY_RUB] = 1;
        foreach ($xml->Valute as $v) {
            if ($v['ID']) {


                if (key_exists((string)$v['ID'], $lbl)) {
                    $out[$lbl[(string)$v['ID']]] = (float)str_replace(',', '.', (string)($v->Value));
                }
            }
        }
        Helpers::headerUTF8();
        echo CJSON::encode(array('V' => $out));
    }

    public function actionCurrencyCourseAjax()
    {
        //phpinfo(); die();

        $ch = curl_init();
        $cv = curl_version();
        $user_agent = "curl";
        curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);

        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
        curl_setopt($ch, CURLOPT_URL, "http://212.40.192.49/scripts/XML_daily.asp");
        $xml = curl_exec($ch);


        Helpers::headerUTF8();
        if ($xml===false) {
            var_dump(curl_error ($ch )); die();
        }
        curl_close($ch);
        //echo(CHtml::encode($xml));
        
        $xml = new SimpleXMLElement($xml);
        foreach ($xml->Valute as $v) {

            echo("$v->Nominal $v->Name = $v->Value деревянных (" . $v['ID'] . ")  <br/>");
        }
    }

    public function actionCompanyAjaxQ()
    {
        if (isset($_GET['q'])) {
            $arr = array();

            $m = new Company('search');
            $m->unsetAttributes();
            //$m->firstName = $_GET['term'];
            $m->fullName = $_GET['q'];
            $m->shortName = $_GET['q'];
            $ac = $m->search();
            $ac->pagination->pageSize = 5;
            $models = $ac->getData();

            $ret = '';

            foreach ($models as $model) {
                $ret[$model->idCompany] = $model->fullName;
            }

            echo CJSON::encode($ret);
        }
    }

    public function actionCompanyAjax()
    {
        if (isset($_GET['term'])) {
            $c = new CDbCriteria();
            $c->addCondition("t.fullName LIKE :q OR t.shortName LIKE :q");
            $c->addCondition("t.idManufacturer IS NULL");
            $c->params['q']='%'.$_GET['term'].'%';
                $c->limit = 5;
                $c->select = "t.idCompany, t.fullName";

                $ac = new CActiveDataProvider('Company',
                    array(
                        'criteria' => $c,
                    ));


            $arr = array();


            $models = $ac->getData();
            foreach ($models as $model) {
                $arr[] = array(
                    'label' => $model->fullName, // value for input field
                    'value' => $model->fullName, // return value from autocomplete
                    'id' => $model->idCompany, // return value from autocomplete
                );
            }
            echo CJSON::encode($arr);
        }
    }

    public function actionCustomerAjax()
    {
        if (isset($_GET['term'])) {
            $arr = array();
            $m = new Customer('search');
            $m->unsetAttributes();
            //$m->firstName = $_GET['term'];
            $m->lastName = $_GET['term'];

            $ac = $m->search();
            //$ac->pagination=false;  
            $models = $ac->getData();
            foreach ($models as $model) {
                $arr[] = array(
                    'label' => $model->fullName, // value for input field
                    'value' => $model->fullName, // return value from autocomplete
                    'id' => $model->idCustomer, // return value from autocomplete
                );
            }
            echo CJSON::encode($arr);
        }
    }

    public function actionDoesFeatureExist()
    {
        header('Content-type: application/json');

        if (isset($_REQUEST['name'])) {
            $n = $_POST['name'];

            $g = Feature::model()->find("`sid`=:sid", array(':sid' => $n));
            if ($g) {
                echo CJSON::encode(array('responce' => 1, 'name' => $g->name));
                return;
            } else {
                echo CJSON::encode(array('responce' => 0, 'name' => ''));
                return;
            }
        }
        return;
    }

    public function actionFeatureAjax()
    {
        if (isset($_GET['term'])) {
            $arr = array();
            $m = new Feature('search');
            $m->unsetAttributes();
            $m->sid = $_GET['term'];

            $ac = $m->search();
            //$ac->pagination=false;
            $models = $ac->getData();

            foreach ($models as $model) {
                $arr[] = array(
                    //'label'=>$model->some_attr,  // label for dropdown list
                    'label' => $model->sid . '-' . $model->name, // value for input field
                    'value' => $model->sid, // return value from autocomplete
                    'feature' => $model->name,
                );
            }
            echo CJSON::encode($arr);
        }
    }

    public function actionFetchFeatureAjax()
    {
        if (isset($_REQUEST['sid']) && isset($_REQUEST['name']) && isset($_REQUEST['manufacturer'])) {
            header('Content-type: application/json');
            $n = $_POST['sid'];

            $g = Feature::model()->find("`sid`=:sid", array(':sid' => $n));
            if (!$g) {
                $g = new Feature();
                $g->name = ($_REQUEST['name']);
                $g->sid = ($_REQUEST['sid']);
                $g->idManufacturer = ($_REQUEST['manufacturer']);
                if (!$g->save()) {
                    throw new CHttpException(500, 'Wont create new');
                }
            }
            if ($g) {
                echo CJSON::encode(array('name' => $g->name, 'id' => $g->idFeature, 'sid' => $g->sid, 'manufacturer' => $g->manufacturer->name));
                return;
            } else {
                throw new CHttpException(500, 'Not found');
                return;
            }
        }
    }

}