<?php
class StickyController extends Controller
{
    public function getZone( ) {
        return Group::SUBZONE_PROJECT_TASK;
    }
    public $layout = 'column1';
    private $_allowAjax = true;

    public function actionCreate()
    {
        $this->applyGroupPolicy(False, Group::ACTION_EDIT, Group::SUBZONE_PROJECT_TASK);
        $model = new Sticky;
        if (isset($_POST['Sticky'])) {
            $model->setAttributes($_POST['Sticky']);
            $model->idUser = Yii::app()->user->id;

            try {
                if ($model->save()) {

                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                    }

                    Yii::app()->user->setFlash('success', "Запись создана");
                    if (isset($_GET['returnUrl'])) {
                        $this->redirect($_GET['returnUrl']);
                    } else {
                        $this->redirect($model->project->getViewUrl(array( 'noLinkedProjects'=>1, '#' => 'sticky')));
                    }
                } else {
                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_lightweightForm', array(
                            'model' => $model), true))));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        } elseif (isset($_GET['Sticky'])) {
            $model->attributes = $_GET['Sticky'];
        }

        if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
            $this->renderPartial('_lightweightForm', array('model' => $model), false, true);
        } else {
            $this->render('create', array('model' => $model));
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $this->applyGroupPolicy($model->project, Group::ACTION_EDIT, Group::SUBZONE_PROJECT_TASK);
        if (isset($_POST['Sticky'])) {
            $model->setAttributes($_POST['Sticky']);
            try {
                if ($model->save()) {

                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                    }

                    Yii::app()->user->setFlash('success', "Данные обновлены");
                    if (isset($_GET['returnUrl'])) {
                        $this->redirect($_GET['returnUrl']);
                    } else {
                        $this->redirect($model->project->getViewUrl(array( 'noLinkedProjects'=>1,  '#' => 'sticky')));
                    }
                } else {
                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_lightweightForm', array(
                            'model' => $model), true))));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }

        }
        if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
            $this->renderPartial('_lightweightForm', array('model' => $model), false, true);
        } else {
            $this->render('update', array('model' => $model));

        }
    }

    public function loadModel($id)
    {
        $model = Sticky::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        return $model;
    }



    public function actionDelete($id)
    {
        if ((!Yii::app()->request->isAjaxRequest)) {
            return;
        }
        if (Yii::app()->request->isPostRequest) {
            try {
                $this->safeLoadModelProjectSubzone($id)->delete();
            } catch (Exception $e) {
                throw new CHttpException(500, $e->getMessage());
            }

            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(array('index'));
            }
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request.'));
    }

}