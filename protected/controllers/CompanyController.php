<?php

class CompanyController extends Controller {
    public function getZone( ) {
        return Group::ZONE_COMPANY;
    }
    private $_allowAjax = true;
     public function filters() {
        return array(
            'accessControl',
        );
    } 

    public function accessRules() {
        return array(
            array('deny', // deny all users
                'users' => array('?'),
            ),
        );
    }
    public $layout = 'column1';

    public function actionIndex() {
        $model = new Company('search');
        $model->unsetAttributes();

        if (isset($_GET['Company']))
            $model->setAttributes($_GET['Company']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionIndexManufacturers() {
        $model = new Company('search');
        $model->unsetAttributes();

        if (isset($_GET['Company']))
            $model->setAttributes($_GET['Company']);

        //$model->idCompanyClass = CompanyClass::MANUFACTURE_COMPANY_CLASS;


        $this->render('adminManufacturers', array(
            'model' => $model,
        ));
    }
    
    public function actionView($id) {
        $model = $this->loadModel($id);

        if ($this->mGridViewDataProvider->renderPartialGridViewIfRequired(array('project-grid', 'customer-grid','project-grid-manufacturer'), $model)) {
            return;
        }

        $this->render("view",array('model'=>$model,
                                  ));
        
    }
    
    public function actionCreate() {
        $model = new Company;
        if (isset($_POST['Company'])) {
            $model->setAttributes($_POST['Company']);
            try {
                if ($model->save()) {
             //       $model->saveCustomers(isset($_POST['Company']['customers']) ? $_POST['Company']['customers'] : array());

                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        Helpers::headerJson();
                        exit(json_encode(array('result' => 'success', 'name' => $model->nameUrl, 'idCompany' => $model->idCompany)));
                    }

                    Yii::app()->user->setFlash('success', "Запись создана");
                    if (isset($_GET['returnUrl'])) {
                        $this->redirect($_GET['returnUrl']);
                    } else {
                        $this->redirect(array('index'));
                    }
                } else {
                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        Helpers::headerJson();
                        exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_lightweightForm', array(
                            'model' => $model), true))));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        } elseif (isset($_GET['Company'])) {
            $model->attributes = $_GET['Company'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('_lightweightForm', array(
                'model' => $model,
                    ), false, true);
        } else {
            $this->render('create', array('model' => $model));
        }
    }

    //the controller action that handles the ajax request
    public function actionCreateAjax() {
        if (isset($_POST['Company'])) {
            $model = new Company();
            $model->unsetAttributes();
            $model->setAttributes($_POST['Company']);
            if (!$model->save())
                exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_lightweightForm', array(
                'model' => $model,
                    ), true)
                    
                    )));
            else
                exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
        }else {
            exit(json_encode(array('result' => 'error', 'msg' => 'No input data has been passed.')));
        }
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        if (isset($_POST['Company'])) {
            $model->setAttributes($_POST['Company']);
            try {
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', "Данные обновлены");
                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        Helpers::headerJson();
                        exit(json_encode(array('result' => 'success')));
                    }

                    if (isset($_GET['returnUrl'])) {
                        $this->redirect($_GET['returnUrl']);
                    } else {
                        $this->redirect($model->getViewUrl());
                    }
                } else {
                    if (($this->_allowAjax) && (Yii::app()->request->isAjaxRequest)) {
                        Helpers::headerJson();
                        exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_lightweightForm', array(
                            'model' => $model), true))));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('_lightweightForm', array(
                'model' => $model,
            ), false, true);
        } else {
            $this->render('update', array('model' => $model));
        }
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            try {
                $this->loadModel($id)->delete();
            } catch (Exception $e) {
                throw new CHttpException(500, $e->getMessage());
            }

            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(array('index'));
            }
        }
        else
            throw new CHttpException(400,
                    Yii::t('app', 'Invalid request.'));
    }

    public function loadModel($id) {
        $model = Company::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        return $model;
    }

}