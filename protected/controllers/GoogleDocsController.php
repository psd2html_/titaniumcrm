<?php

class GoogleDocsController extends Controller
{

    private $_temp = array();

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('deny', // deny all users
                'users' => array('?'),
            ),
        );
    }

    public function behaviors()
    {
        return array(
            'doccy' => array(
                'class' => 'ext.doccy.Doccy',
                'options' => array('templatePath' => Yii::app()->basePath . '/docs/templates',
                    'outputPath' => Yii::app()->basePath . '/docs/',
                    'temp' => Yii::app()->basePath . '/docs/temp/',
                )
            ),
        );
    }

    public function actionDownload($id)
    {
        $order = Order::model()->findByPk($id);
        $this->applyGroupPolicy($order->project, Group::ACTION_READONLY, Group::SUBZONE_PROJECT_ORDER);


        /* @var $order Order */
        if ((!$order)) {
            exit("No order found");
        }
        $TBS = $this->makeTBS($order);

        // todo: где-то не сохраняет $order->name
        //$file_name = ($order->name . '.docx');
        $file_name = ('order-'.$order->idOrder . '.docx');
        $TBS->Show(OPENTBS_DOWNLOAD, $file_name);
        $this->_clearTemp();
    }

    /**
     *
     * @param Order $order
     * @return \clsTinyButStrong
     */
    private function makeTBS($order)
    {
        $this->importDependencies();

        $TBS = new clsTinyButStrong; // new instance of TBS
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN); // load OpenTBS plugin
        $debug = 0;

        if (!$order->coreInstrument) {
            $template = Yii::app()->getBasePath() . '/docs/new_template_v1_no_ci.docx';
        } else {
            $template = Yii::app()->getBasePath() . '/docs/new_template_v1.docx';
        }
//        $template = Yii::app()->getBasePath() . '/docs/new_template.docx';

        $x = pathinfo($template);

        $template_ext = $x['extension'];
        if (!file_exists($template))
            exit("File does not exist.");


        if ($debug == 2) { // debug mode 2
            $TBS->Plugin(OPENTBS_DEBUG_XML_CURRENT);
            exit;
        } elseif ($debug == 1) { // debug mode 1
            $TBS->Plugin(OPENTBS_DEBUG_INFO);
            exit;
        }
        $templateBlock_coreInstrument = array();
        $templateBlock_colspan = array();
//-------------------------------------------------------------------------------------------------
        // Load constants
        $conversion = $order->getMoneyConversionArray();
        $distinct = $order->getDistinctFeatures();
        $rusCurrenciesName = Order::getRusCurrencies();
        $rusCurrenciesName[Order::CURRENCY_EMPTY] = '';

        if ($order->coreInstrument) {
            //   $this->_addFeatureOrCoreInstrumentRow($order, $order->coreInstrument, $conversion, $rusCurrenciesName, $templateBlock_coreInstrument);
            array_unshift($distinct, $order->coreInstrument);
        }

//-------------------------------------------------------------------------------------------------
        // Get calculation

        list($sum, $exw, $cip, $ddp, $discount) = $order->calculateOrder();

//-------------------------------------------------------------------------------------------------
        //prepare vars
        $stuff = array();

        if ($order->coreInstrument)
            $stuff[] = $order->coreInstrument;

        $destinationCity = $order->destinationCity ? $order->destinationCity :
            $order->project->companyConsumer->city;

        $destinationCityGenetivus = $order->destinationCityGenetivus ? $order->destinationCityGenetivus :
            ($order->project->companyConsumer->cityGenetivus ? $order->project->companyConsumer->cityGenetivus : $order->project->companyConsumer->city);

        $typeOfContracts = $order->getCalculationAlgorithms();
        $typeOfContract = $typeOfContracts[$order->enumCalculationAlgorithm];

//-------------------------------------------------------------------------------------------------

        $sign = tempnam(sys_get_temp_dir(), "_sgn") . ".jpg";
        $coreinstrumentImage = tempnam(sys_get_temp_dir(), "_ci") . ".jpg";

        $this->_temp[] = $sign;

        copy($order->user->getRawThumbnail(), $sign);

        if ($order->isDeliverySpreadBetweenFeatures) {
            $order->showEXW = false;
        }

        if ($order->coreInstrument) {
            $this->_temp[] = $coreinstrumentImage;
            copy($order->coreInstrument->getRawThumbnail(), $coreinstrumentImage);
        }

        $templateSettings = array(
            array(
                'userSign' => $sign,
                'address' => $order->project->companyConsumer->physicalAddress,
                'phrase' => $order->manufacturer->motto,
                'coreinstrumentname' => $order->coreInstrument ? $order->coreInstrument->name : "-",
                'coreinstrumentImage' => $coreinstrumentImage,
                'currency' => $rusCurrenciesName[$order->outCurrency],
                'poName' => $order->name,
                'date' => Yii::app()->dateFormatter->formatDateTime($order->date, 'medium', null),
                'companyType' => $order->project->companyConsumer->companyClass->name,
                'company' => $order->project->companyConsumer->fullName,
                'person' => '-',
                'email' => '-',
                'phone' => '-',
                'italic' => $order->outCurrency != Order::CURRENCY_RUB ? '* При курсе не более ' . $conversion[$order->outCurrency] . ' руб. за 1 ' . $rusCurrenciesName[$order->outCurrency] : '',
                'noBold' => 0,
                'noColspan' => 0,
                'noRed' => 0,
                'noFooter' => 0,
                'showManufacturerImage' => 0,
                'manufacturerImage' => $order->manufacturer->getRawThumbnail(false, false),
                'userName' => $order->user->fullName,
                'personalFooter' => ((mb_ereg_replace("([\n\r])+", "", $order->user->personalFooter, "mp"))), //$this->prepareDocx2(mb_ereg_replace("([\n\r])+","",$order->user->personalFooter,"mp")),
                'personalFooter2' => ((mb_ereg_replace("([\n\r])+", "", $order->user->personalFooter2, "mp"))), //$this->prepareDocx2(mb_ereg_replace("([\n\r])+","",$order->user->personalFooter,"mp")),
                'personalFooter3' => ((mb_ereg_replace("([\n\r])+", "", $order->user->personalFooter3, "mp"))) //$this->prepareDocx2(mb_ereg_replace("([\n\r])+","",$order->user->personalFooter,"mp")),
            ));

        if ($m = $order->manufacturer->getRawThumbnail(false, true)) {
            // if word fucks up the Template,
            // embed this line into w:drawing
            // <w:r><w:t>[onshow;block=w:drawing;when [options.showManufacturerImage]=1]</w:t></w:r>

            $templateSettings[0]['manufacturerImage'] = $m;
            $templateSettings[0]['showManufacturerImage'] = 1;
        }


        if ($order->customer) {
            $templateSettings[0] ['person'] = $order->customer->lastName . ' ' . $order->customer->firstName . ' ' . $order->customer->middleName;
            $phones = array($order->customer->phone, $order->customer->phone2, $order->customer->mobilePhone);
            $pp = array();
            foreach ($phones as $phone) {
                if (($phone)) {
                    $pp[] = $phone;
                }
            }
            if (($order->customer->email)) {
                $templateSettings[0] ['email'] = $order->customer->email;
            }
            if ($pp) {
                $templateSettings[0] ['phone'] = implode($pp, ', ');
            }
        }

        if ($order->cipPayment != 0) {
            $deliveryPriceText = "";
            $deliveryPrice = $this->normalizePrice($order->cipPayment, $order->cipCurrency, $order->outCurrency, $conversion);
            if ($order->isDeliverySpreadBetweenFeatures) {
                foreach ($distinct as $feature) {
                    $percentage = $this->normalizePrice($feature->price, $feature->enumCurrency, $order->outCurrency, $conversion) * $feature->amount
                        / $exw;
                    $feature->addToPrice = $deliveryPrice * $percentage;
                }

            } else {

                if (!$order->showEXW and !$order->showCIP and $order->isFeaturePriceShown) {
                    $deliveryPrice = $this->normalizePrice($order->cipPayment * $order->ddpCoeff, $order->cipCurrency, $order->outCurrency, $conversion);
                }

                $deliveryPriceText = $order->isFeaturePriceShown ?
                    $this->formatPrice(
                        $deliveryPrice, Order::CURRENCY_EMPTY, $rusCurrenciesName) : "";

            }

            $templateBlock_colspan = array(array(
                'text' => 'Страховка и доставка до ' . $destinationCityGenetivus,
                'amount' => '1',
                'delivery' => $deliveryPriceText
            ));
        }

        $templateBlock_redLines = array();

        if ($order->enumPricePolicy == Order::PRICEPOLICY_SALE) {
            $templateBlock_redLines = array(
                array(
                    'text' => 'Скидка для ' . $order->project->companyConsumer->fullName . ', ' . ($order->saleCoeff) . '%',
                    'minus' => '-' . $this->formatPrice($discount, $order->outCurrency, $rusCurrenciesName),
                )
            );
        } else if ($order->enumPricePolicy == Order::PRICEPOLICY_OVERRIDENPRICE) {
            if ($discount > 0) {
//                $redLines = array(
//                    array(
//                        'text' => 'Сумма для ' . $order->project->companyConsumer->fullName,
//                        'minus' => $this->formatPrice($order->overridenPrice, $order->outCurrency, $c),
//                    )
//                );
            } else {
                $templateBlock_redLines = array(
                    array(
                        'text' => 'Скидка для ' . $order->project->companyConsumer->fullName . ', ' . (number_format(-100 * $discount / (-$discount + $sum), 2)) . '%',
                        'minus' => '-' . $this->formatPrice($discount, $order->outCurrency, $rusCurrenciesName),
                    )
                );
            }
        }

        $templateBlock_footer = array();
        $templateBlock_bold = array();

        if (($order->enumCalculationAlgorithm > Order::ALGORITHM_EXW) && ($order->showEXW)) {
            $templateBlock_footer[] = array('text' => 'Цена товаров и услуг, EXW ' . $destinationCity,
                'sum' => $this->formatPrice($exw, $order->outCurrency, $rusCurrenciesName));
        }

        if (($order->enumCalculationAlgorithm > Order::ALGORITHM_CIP) && ($order->showCIP)) {
            $templateBlock_footer[] = array('text' => 'Цена товаров и услуг, CIP ' . $destinationCity,
                'sum' => $this->formatPrice($cip, $order->outCurrency, $rusCurrenciesName));
        }
        if (($order->enumCalculationAlgorithm > Order::ALGORITHM_DDP ||
                ($order->enumCalculationAlgorithm == Order::ALGORITHM_DDP &&
                    ($order->enumPricePolicy == Order::PRICEPOLICY_SALE ||
                        (($order->enumPricePolicy == Order::PRICEPOLICY_OVERRIDENPRICE) && ($discount < 0))
                    )
                ))
            &&
            ($order->showDDP)
        ) {
            $templateBlock_footer[] = array('text' => 'Цена товаров и услуг, DDP ' . $destinationCity,
                'sum' => $this->formatPrice($ddp, $order->outCurrency, $rusCurrenciesName));
        }

        $templateBlock_bold[] = array('text' => 'ИТОГО товаров и услуг, ' . $typeOfContract . ' ' . $destinationCity . ':',
            'sum' => $this->formatPrice($sum, $order->outCurrency, $rusCurrenciesName));

        $TBS->SetOption('charset', 'UTF-8');
        $TBS->LoadTemplate($template);
        $TBS->SetOption('charset', 'UTF-8');

        $templateBlock_featuresTable = array();
        $multiplyCoefficient = 1;

        if (!$order->showEXW and !$order->showCIP and $order->isFeaturePriceShown) {
            $multiplyCoefficient = $order->ddpCoeff;
        }

        foreach ($distinct as $feature) {
            $this->_addFeatureOrCoreInstrumentRow($order, $feature, $conversion, $rusCurrenciesName, $multiplyCoefficient, $templateBlock_featuresTable);
        }

        /* Условия договора - вынести в модель */

        $arguments = array();
        if ($order->doThreeMonth) {
            $arguments[] = array('name' => 'Данное предложение действительно три месяца.');
        } else {
            $arguments[] = array('name' => 'Данное предложение действительно до ' . Yii::app()->dateFormatter->formatDateTime($order->lastDate, 'medium', null) . '.');
        }

        if ($order->deliver) {
            $arguments[] = array('name' => 'Доставка: ' . $order->deliver . ' мес. ');
        }

        if ($order->guaranteeInMonth) {
            $oo = 'Гарантия: ' . $order->guaranteeInMonth . ' мес.';

            if ($order->guaranteeLessThan) {
                $oo .= ', но не более ' . $order->guaranteeLessThan . ' мес.';
            }

            $arguments[] = array('name' => $oo);
        }

        $arguments[] = array('name' => 'Условия поставки: ' . $typeOfContract . ' ' . ($order->project->companyConsumer->city) . '.');

        $currencyLiteralFull = Order::getRusCurrenciesLong();
        $arguments[] = array('name' => 'Валюта платежа: ' . mb_strtolower($currencyLiteralFull[$order->outCurrency], 'utf8') . '.');

        $pc = Order::getPaymentConditionList();
        if (isset($pc[$order->enumPaymentCondition])) {
            $arguments[] = array('name' => 'Условия платежа: ' . mb_strtolower($pc[$order->enumPaymentCondition], 'utf8') . '.');
        }

        if (isset($templateBlock_featuresTable))
            $TBS->MergeBlock('a', ($templateBlock_featuresTable));

        if (!isset($templateBlock_colspan)) {
            $templateBlock_colspan = array(array(
                'text' => 'Стоимость доставки',
                'amount' => '',
                'delivery' => ''));
        }


        $TBS->MergeBlock('options', ($templateSettings));
        if (isset($templateBlock_redLines))
            $TBS->MergeBlock('red', ($templateBlock_redLines));
        if (isset($templateBlock_footer))
            $TBS->MergeBlock('footer', ($templateBlock_footer));
        if (isset($templateBlock_colspan))
            $TBS->MergeBlock('colspan', ($templateBlock_colspan));
        if (isset($templateBlock_bold))
            $TBS->MergeBlock('bold', ($templateBlock_bold));


        $TBS->MergeBlock('arguments', $arguments);
        $TBS->MergeBlock('coreinstrument', $templateBlock_coreInstrument);
        $TBS->MergeBlock('feature', $templateBlock_featuresTable);

        $TBS->LoadTemplate('#word/footer1.xml');
        $TBS->MergeBlock('options', ($templateSettings));


        return $TBS;
    }

    private function importDependencies()
    {
        Yii::import('application.vendors.*');
        require_once 'tbs/tbs_plugin_opentbs.php';
        require_once 'tbs/tbs_class.php';
        Yii::import('application.vendors.*');
        require_once 'simplehtmldom/simple_html_dom.php';
        require_once 'phpword/PHPWord/Writer/Word2007/WriterPart.php';
        require_once 'phpword/PHPWord/Writer/Word2007/Base.php';
        require_once 'phpword/PHPWord/Writer/Word2007/Section.php';
        require_once 'phpword/PHPWord/Section.php';
        require_once 'phpword/PHPWord/DocumentProperties.php';
        require_once 'phpword/PHPWord/Style.php';
        require_once 'phpword/PHPWord/Media.php';
        require_once 'phpword/PHPWord/TOC.php';

        $this->include_all_files('phpword/PHPWord/Section');
        $this->include_all_files('phpword/PHPWord/Shared');
        $this->include_all_files('phpword/PHPWord/Style');

        require_once 'phpword/PHPWord.php';
        require_once 'htmltodocx_converter/h2d_htmlconverter.php';
    }

    public function include_all_files($root)
    {

        $d = new RecursiveDirectoryIterator(Yii::app()->getBasePath() . '/vendors/' . $root);
        foreach (new RecursiveIteratorIterator($d) as $file => $f) {
            $ext = pathinfo($f, PATHINFO_EXTENSION);
            if ($ext == 'php' || $ext == 'inc') {
                require_once($file); // or require(), require_once(), include_once()
            }
        }
    }

    private function normalizePrice($val, $from, $to, $curr)
    {
        return $val * $curr[$from] / $curr[$to];
    }

    private function formatPrice($val, $curr, $a)
    {
        return number_format($val, 2, '.', '`') . ' ' . $a[$curr];
    }

    /**
     * @param $order
     * @param $feature
     * @param $conversion
     * @param $c
     * @param $options
     */

    private function _addFeatureOrCoreInstrumentRow($order, $feature, $conversion, $c, $multiply, &$options)
    {
        $price = $feature->isPriceSettled ?
            $this->formatPrice(
                $multiply * (
                    $feature->addToPrice +
                    $this->normalizePrice($feature->price, $feature->enumCurrency, $order->outCurrency, $conversion) * $feature->amount)
                , Order::CURRENCY_EMPTY, $c
            ) : '';

        if (!$order->isFeaturePriceShown) {
            $price = '';
        }

        if ($order->isRussianDescriptionLoaded) {
            $name = $feature->ruName ? $feature->ruName : $feature->name;
            $description = strip_tags($feature->ruDescription) ? $feature->ruDescription : $feature->description;
        } else {
            $name = $feature->name;
            $description = $feature->description;
        }

        $options[] = array('SID' => $feature->sid, 'name' => $name, 'amount' => $feature->amount,
            'price' => $price,
            'description' => $this->prepareDocx2($description));
    }

    private function prepareDocx2($raw)
    {
        $html = new simple_html_dom();

        // delete empty <p>

        $raw = tidy_repair_string(
            $raw,
            array(
                'show-body-only' => true,
                'doctype' => '-//W3C//DTD XHTML 1.0 Transitional//EN',
                'output-xhtml' => true
            ), 'utf8'

        );

        $raw = preg_replace("/<p>([\r\n\s])*(&nbsp;)+([\r\n\s])*<\/p>/umxs", "", $raw);

        $raw = '<p>' . $raw . '</p>';
        $html->load($raw);

        $child = $html->childNodes();
        $section = new PHPWord_Section(1, array());
        $setup = array();

        $htmltodocx_insert_html_recursive = htmltodocx_insert_html($section, $child);
        $docWriter = new PHPWord_Writer_Word2007_Section();
        $baked = $docWriter->writeSection($section);
        $baked = str_replace('<w:p/>', "", $baked);
        //var_dump($baked);

        return ('</w:t></w:r></w:p>' . $baked . '<w:p><w:r><w:rPr></w:rPr><w:t>');
    }

    private function _clearTemp()
    {

        foreach ($this->_temp as $t) {
            unlink($t);
        }

        $this->_temp = [];
    }

    public function actionDownloadPDF($id)
    {
        return;
        ///usr/share/unoconv/unoconv -vvv -i FilterOptions=76 -e FilterOptions=76 -e RestrictPermissions=true -e PermissionPassword=test -e EnableCopyingOfContent=false -f doc index.html
        $name = 'd_' . $id . '_' . date('Y-m-d');
        $bake_path = Yii::app()->getBasePath() . '/../docs/bake/';
        $pdf_path = Yii::app()->getBasePath() . '/../docs/ready/';

        $order = Order::model()->findByPk($id);
        /* @var $order Order */
        if ((!$order)) {
            exit("No order found");
        }
        $TBS = $this->makeTBS($order);


        $TBS->Show(OPENTBS_FILE, $bake_path . $name . '.docx');


        $out = array();
        $u = -2;
//        echo exec("whoami");esrt
//echo exec ("/usr/share/unoconv/unoconv -vvv -i FilterOptions=76 -e FilterOptions=76 -e RestrictPermissions=true -e PermissionPassword=test -e EnableCopyingOfContent=false -f pdf ".$bake_path.$name." 2>&1",$out,$u);
        exec(Yii::app()->getBasePath() . "/vendors/unoconv/unoconv  -vvv -i FilterOptions=76 -e FilterOptions=76 -e RestrictPermissions=true -e PermissionPassword=test -e EnableCopyingOfContent=false -o " . $pdf_path . " -f pdf " . $bake_path . $name . ".docx 2>&1", $out, $u);
        //var_dump($u);
        //var_dump($out);
        //echo("<code>".implode("<br/>",$out)."</code>");
        echo("This is nothing more than a test. Do not take it serious. If markup is corrupted, blame OpenOffice ");
        echo CHtml::link("Converted PDF", "/docs/ready/" . $name . '.pdf');
        return;
    }

    public function actionUploadToGoogleDocs($id)
    {
        return;
        // Копролиты ahead!
        $o = Order::model()->findByPk($id);


        //Helpers::headerUTF8();
        Yii::import('application.vendors.*');
        require_once 'Zend/Gdata/Docs.php';
        require_once 'Zend/Gdata/ClientLogin.php';
        //echo('<code>');
        //echo("Connecting Google Docs...<br/>");
        if (!Yii::app()->settings->get('general', 'googleLogin')) {
            Helpers::headerUTF8();
            echo("Google account is not specified. Please, set vaild account in " . CHtml::link("config", Yii::app()->createUrl(("/site/config"))) . ". Quitting. <br/>");
            return;
        }
        $service = Zend_Gdata_Docs::AUTH_SERVICE_NAME;
        try {
            $client = Zend_Gdata_ClientLogin::getHttpClient(Yii::app()->settings->get('general', 'googleLogin'), Yii::app()->settings->get('general', 'googlePass'), $service);
        } catch (Exception $e) {
            Helpers::headerUTF8();
            echo($e->getMessage() . ". Please, set vaild account in " . CHtml::link("config", Yii::app()->createUrl(("/site/config"))) . ". Quitting. <br/>");
            return;
        }
        $docs = new Zend_Gdata_Docs($client);
        $feed = $docs->getDocumentListFeed();
        $fn = $id . "_" . Yii::app()->dateFormatter->formatDateTime($o->date, 'medium', null) . '_' . $o->coreInstrument->name . ".docx";


        if ($o->entryId) {
            foreach ($feed as $entry) {
                if ($entry->getId() == $o->entryId) {
                    $alternateLink = '';
                    foreach ($entry->link as $link) {
                        if ($link->getRel() === 'alternate') {
                            $alternateLink = $link->getHref();
                        }
                    }
// Make the title link to the document on docs.google.com.
                    //echo "Already stored in GoogleDocs <a href=\"$alternateLink\">" . $entry->title . "</a>\n";
                    $this->redirect($alternateLink);
                    return;
                }
            }
            $o->entryId = null;
            $o->save();
        }
        $this->makeDocx($id, 'template.docx');
        $this->renderDocx("OrderId-" . $id . ".docx", !true); // use $forceDownload=false in order to (just) store file in the outputPath folder.

        $entry = $this->uploadDocument($docs, true, $fn, Yii::app()->getBasePath() . "/docs/OrderId-" . $id . ".docx");
        if ($entry) {
            $o->entryId = $entry->getId();
            $o->save();
            foreach ($entry->link as $link) {
                if ($link->getRel() === 'alternate') {
                    $alternateLink = $link->getHref();
                }
            }
            $this->redirect($alternateLink);
        }

        // echo('</code>');
    }

    /**
     * Upload the specified document
     *
     * @param Zend_Gdata_Docs $docs The service object to use for communicating
     *     with the Google Documents server.
     * @param boolean $html True if output should be formatted for display in a
     *     web browser.
     * @param string $originalFileName The name of the file to be uploaded. The
     *     MIME type of the file is determined from the extension on this file
     *     name. For example, test.csv is uploaded as a comma separated volume
     *     and converted into a spreadsheet.
     * @param string $temporaryFileLocation (optional) The file in which the
     *     data for the document is stored. This is used when the file has been
     *     uploaded from the client's machine to the server and is stored in
     *     a temporary file which does not have an extension. If this parameter
     *     is null, the file is read from the originalFileName.
     */
    function uploadDocument($docs, $html, $originalFileName, $temporaryFileLocation)
    {
        $fileToUpload = $originalFileName;
        if ($temporaryFileLocation) {
            $fileToUpload = $temporaryFileLocation;
        }

// Upload the file and convert it into a Google Document. The original
// file name is used as the title of the document and the MIME type
// is determined based on the extension on the original file name.
        $newDocumentEntry = $docs->uploadFile($fileToUpload, $originalFileName, null, Zend_Gdata_Docs::DOCUMENTS_LIST_FEED_URI);

        //echo "New Document Title: ";

        if ($html) {
// Find the URL of the HTML view of this document.
            $alternateLink = '';
            foreach ($newDocumentEntry->link as $link) {
                if ($link->getRel() === 'alternate') {
                    $alternateLink = $link->getHref();
                }
            }
// Make the title link to the document on docs.google.com.
            //echo "<a href=\"$alternateLink\">\n";
        }
        // echo $newDocumentEntry->title . "\n";
        if ($html) {
            //   echo "</a>\n";
        }
        return $newDocumentEntry;
    }

    public function actionRefresh()
    {
        return;
    }

    public function actionOlolo()
    {
        $this->importDependencies();
        $f = CoreInstrument::model()->findByPk(5);

        echo($this->prepareDocx2($f->description));
    }

    private function normalizeAndFormatPrice($val, $from, $to, $curr, $a)
    {
        $val = $this->normalizePrice($val, $from, $to, $curr);
        return $this->formatPrice($val, $to, $a);
    }

    private function prepareDocx($raw)
    {
        $html = new simple_html_dom();
        $raw = str_replace("<p> </p>", "", $raw);
        //echo(CHtml::encode($raw));

        $html->load($raw);
        //$child = $html->getElementByTagName('body')->childNodes();
        $child = $html->childNodes();
        $section = new PHPWord_Section(1, array());
        $setup = array();
        $htmltodocx_insert_html_recursive = htmltodocx_insert_html($section, $child);
        $docWriter = new PHPWord_Writer_Word2007_Section();
        $baked = $docWriter->writeSection($section);


        $baked = str_replace("<w:p>", '<w:p><w:pPr><w:ind w:left="426"/></w:pPr>', $baked);
        //   var_dump(CHtml::encode($baked).'<br/>');
        return ('__delme__</w:t></w:r></w:p>' . $baked . '<w:p><w:r><w:rPr></w:rPr><w:t>__delme__');
        //return ('</w:t></w:r></w:p>'.$baked.'<w:p><w:r><w:rPr></w:rPr><w:t>');
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//AIzaSyDU7YFuCYtU160Cli6FajhcXZbw3xkSAD0
?>
