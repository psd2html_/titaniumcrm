<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sdd
 * Date: 12/13/13
 * Time: 8:58 PM
 * To change this template use File | Settings | File Templates.
 */
class UploadController extends Controller
{

    public function actionRenameFile()
    {
        if (Yii::app()->request->isAjaxRequest) throw new CHttpException(400, "Boo");
        //$i,$h,$f
        $raw = Yii::app()->request->getRawBody();
        $a = CJSON::decode($raw);
        $i = isset($a['i']) ? $a['i'] : -1;
        $h = isset($a['h']) ? $a['h'] : -1;
        $f = isset($a['f']) ? $a['f'] : -1;
        $name = isset($a['n']) ? $a['n'] : false;

        $file = Upload::model()->with("project")->findByPk($i);
        if (!$file) throw new CHttpException(404, "Attachment not found");
        if ($file->idFolder != $f) throw new CHttpException(404, "Attachment not found");
        if (substr($file->token, 0, 10) != $h) throw new CHttpException(404, "Attachment not found");
        $this->applyGroupPolicy($file->project, Group::ACTION_EDIT, Group::SUBZONE_PROJECT_FILES);

        $file->comment = CHtml::encode($name);

        Helpers::headerJson();
        echo CJSON::encode(array('r' => $file->save() ? 1 : -1));

    }

    public function actionRenameFolder()
    {
        if (Yii::app()->request->isAjaxRequest) throw new CHttpException(400, "Boo");

        $raw = Yii::app()->request->getRawBody();
        $a = CJSON::decode($raw);

        $p = isset($a['p']) ? $a['p'] : -1;
        $name = isset($a['n']) ? $a['n'] : false;

        $f = isset($a['f']) ? $a['f'] : -1;
        $folder = Folder::model()->findByPk($f);

        if (!$folder) throw new CHttpException(404, "Folder not found");
        if ($p != $folder->idProject) throw new CHttpException(404, "Attachment not found");
        if ($name === false) throw new CHttpException(404, "Attachment not found");
        if ($folder->enumSpecialFolder != Folder::USER_FOLDER) throw new CHttpException(403, "Not allowed");

        $this->applyGroupPolicy($folder->project, Group::ACTION_EDIT, Group::SUBZONE_PROJECT_FILES);

        $folder->name = CHtml::encode($name);

        Helpers::headerJson();
        if ($folder->save()) {
            echo CJSON::encode(array('r' => 1));
            return;
        }
        echo CJSON::encode(array('r' => -1));

    }

    public function actionDeleteFolder()
    {
        if (Yii::app()->request->isAjaxRequest) throw new CHttpException(400, "Boo");

        $raw = Yii::app()->request->getRawBody();
        $a = CJSON::decode($raw);

        $p = isset($a['p']) ? $a['p'] : -1;

        $f = isset($a['f']) ? $a['f'] : -1;
        $folder = Folder::model()->findByPk($f);

        if (!$folder) throw new CHttpException(404, "Folder not found");
        if ($p != $folder->idProject) throw new CHttpException(404, "Attachment not found");
        if ($folder->enumSpecialFolder != Folder::USER_FOLDER) throw new CHttpException(403, "Not allowed");

        $this->applyGroupPolicy($folder->project, Group::ACTION_EDIT, Group::SUBZONE_PROJECT_FILES);


        Helpers::headerJson();
        if ($folder->gracefullyDelete()) {
            echo CJSON::encode(array('r' => 1));
            return;
        }
        echo CJSON::encode(array('r' => -1));
    }

    public function actionCreateFolder()
    {
        if (Yii::app()->request->isAjaxRequest) throw new CHttpException(400, "Boo");

        $raw = Yii::app()->request->getRawBody();
        $a = CJSON::decode($raw);

        $p = isset($a['p']) ? $a['p'] : -1;
        $parentId = isset($a['parent']) ? $a['parent'] : false;
        $name = isset($a['n']) ? $a['n'] : false;
        $project = Project::model()->findByPk($p);

        if (!$project) throw new CHttpException(404, "Attachment not found");
        if ($name === false) throw new CHttpException(404, "Attachment not found");

        $this->applyGroupPolicy($project, Group::ACTION_EDIT, Group::SUBZONE_PROJECT_FILES);

        $folder = new Folder();
        $folder->idProject = $project->idProject;

        if ($parentId) {
            $parentFolder = Folder::model()->findByPk($parentId);
            if (!$parentFolder) {
                throw new CHttpException(403, "Bad parent folder");
            }
            $folder->parent = $parentId;
        }

        $folder->name = CHtml::encode($name);
        $folder->enumSpecialFolder = Folder::USER_FOLDER;
        Helpers::headerJson();
        if ($folder->save()) {
            echo CJSON::encode(array('r' => 1));
            return;
        }
        echo CJSON::encode(array('r' => -1));
    }

    public function actionMoveFile()
    {

        if (Yii::app()->request->isAjaxRequest) throw new CHttpException(400, "Boo");
        //$i,$h,$f
        $raw = Yii::app()->request->getRawBody();
        $a = CJSON::decode($raw);
        $i = isset($a['i']) ? $a['i'] : -1;
        $h = isset($a['h']) ? $a['h'] : -1;
        $f = isset($a['f']) ? $a['f'] : -1;
        $p = isset($a['p']) ? $a['p'] : -1;
        $file = Upload::model()->with("project")->findByPk($i);
        $folder = Folder::model()->findByPk($f);

        if (!$file) throw new CHttpException(404, "Attachment not found");
        if (!$folder) throw new CHttpException(404, "Folder not found");

        if ($file->idProject != $p) throw new CHttpException(404, "Project issue");
        if ($folder->idProject != $p) throw new CHttpException(404, "Project issue 2");

        if (substr($file->token, 0, 10) != $h) throw new CHttpException(404, "Attachment not found");
        $this->applyGroupPolicy($file->project, Group::ACTION_EDIT, Group::SUBZONE_PROJECT_FILES);

        $file->idFolder = $folder->idFolder;
        $file->save();
        Helpers::headerJson();
        echo CJSON::encode(array('r' => 1));

    }

    public function actionDeleteFile()
    {

        if (Yii::app()->request->isAjaxRequest) throw new CHttpException(400, "Boo");
        //$i,$h,$f
        $raw = Yii::app()->request->getRawBody();
        $a = CJSON::decode($raw);
        $i = isset($a['i']) ? $a['i'] : -1;
        $h = isset($a['h']) ? $a['h'] : -1;
        $f = isset($a['f']) ? $a['f'] : -1;
        $file = Upload::model()->with("project")->findByPk($i);
        if (!$file) throw new CHttpException(404, "Attachment not found");
        if ($file->idFolder != $f) throw new CHttpException(404, "Attachment not found");
        if (substr($file->token, 0, 10) != $h) throw new CHttpException(404, "Attachment not found");
        $this->applyGroupPolicy($file->project, Group::ACTION_EDIT, Group::SUBZONE_PROJECT_FILES);

        $original_filename = $file->getFilePath();
        if (file_exists($original_filename)) {
            unlink($original_filename);
        }
        $file->delete();
        Helpers::headerJson();
        echo CJSON::encode(array('r' => 1));

    }

    public function actionDownload($i, $p, $h, $f)
    {
        $file = Upload::model()->with("project")->findByPk($i);
        if (!$file) throw new CHttpException(404, "Attachment not found");
        if ($file->idProject != $p) throw new CHttpException(404, "Attachment not found");
        if ($file->idFolder != $f) throw new CHttpException(404, "Attachment not found");
        if (substr($file->token, 0, 10) != $h) throw new CHttpException(404, "Attachment not found");

        $this->applyGroupPolicy($file->project, Group::ACTION_READONLY, Group::SUBZONE_PROJECT_FILES);

        $original_filename = $file->getFilePath();

        if (file_exists($original_filename)) {
            $new_filename = $file->name;
            header("Content-Length: " . filesize($original_filename));
            header("Content-Type: application/octet-stream");
            header('Content-Disposition: attachment; filename="' . $new_filename . '"');
            readfile($original_filename);
        } else {
            throw new CHttpException(404, 'Attachment not found.');
        }
    }

    public function actionGetFiles($id)
    {
        $f = Folder::model()->findByPk($id);
        if (!$f) {
            echo CJSON::encode(array('r' => 1,
                'f' => array()));
        }

        $files = array();
        foreach ($f->uploads as $file) {
            $files[] = array('uid' => substr($file->token, 0, 10), 'u' => $file->idUser, 'id' => $file->idUpload, 'n' => ($file->name), 'c' => $file->comment, 's' => $file->size, 'desc' => $file->comment, 'a' => $file->user->name, 'date' => $file->date);
        }

        Helpers::headerJson();
        echo CJSON::encode(array('r' => 1,
            'f' => $files));
    }

    public function actionGetFolders($id)
    {
        $project = Project::model()->findByPk($id);
        if (!$project) return false;

        $folders = $project->topLevelFolders;

        if (count($folders) == 0) {
            $this->_buildDefaultFolderStructure($id);
            $project = Project::model()->findByPk($id);
            $folders = $project->topLevelFolders;
        }

        Helpers::headerJson();
        echo CJSON::encode(array('r' => 1,
            'f' => $this->_transcode($folders)));
    }

    private function _buildDefaultFolderStructure($id)
    {
        $project = Project::model()->findByPk($id);
        if (!$project) return false;

        $project->buildFolderStructure();
    }

    /**
     * @param Folder[] $folders
     * @return array
     */
    private function _transcode($folders)
    {
        $sum = array();
        $children = array();
        foreach ($folders as $folder) {
            $n = $folder->name;
            if ($folder->uploadsCount) {
                $n .= "(" . $folder->uploadsCount . ")";
            }
            $m = array('label' => $n,
                'data' => array('id' => $folder->idFolder, 'type' => $folder->enumSpecialFolder));

            if ($folder->folders) {
                $m['children'] = $this->_transcode($folder->folders);
            }

            $sum[] = $m;
        }

        return $sum;
    }

    public function actionStore()
    {
        $s = $_POST;
        if ((!isset($s['p'])) || (!isset($s['l']))) {
            return $this->_ajaxError("Некорректный запрос");
        }

        $projectId = $s['p'];
        $location = $s['l'];

        $project = Project::model()->findByPk($projectId);
        if (!$project) return $this->_ajaxError("Некорректный запрос");

        $folder = Folder::model()->findByPk($location);
        if (!$folder || $folder->idProject != $projectId) return $this->_ajaxError("Некорректный запрос");

        $var = CUploadedFile::getInstanceByName("file");

//        $allowed_mimes = Upload::getAllowedMimes();
//
//
//        //$var->getTempName()
//        if (!in_array($var->type, $allowed_mimes)) {
//            return $this->_ajaxError("Тип файла " . $var->name . " не разрешен к загрузке ($var->type)  ");
//        }

        if ($var->size > Upload::MAX_SIZE) {
            return $this->_ajaxError("Размер файла " . $var->name . " больше разшененного предела " . Upload::MAX_SIZE . " байт");
        }

        if (Upload::useFileAndSave($project, $folder, $var)) {
            Helpers::headerJson();
            echo CJSON::encode(array('r' => 1));
        } else {
            $this->_ajaxError("Не удалось");
        }

        return;

    }

    private function _ajaxError($message)
    {
        Helpers::headerJson();
        echo CJSON::encode(array('r' => -1, 'm' => $message));
    }

}