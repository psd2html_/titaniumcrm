<?php
class CustomController extends Controller {
    public function getZone( ) {
        return Group::ZONE_CUSTOM;
    }

    
    public $layout='column1';
    
    public function actionIndex() {
        $this->applyGroupPolicy();
       $model = new Custom('search');
        $model->unsetAttributes();

        if (isset($_GET['Custom']))
                $model->setAttributes($_GET['Custom']);

        $this->render('admin', array(
                'model' => $model,
        ));
    }
        
  
        
    public function actionCreate() {
        $this->applyGroupPolicy();
        $model = new Custom;
                if (isset($_POST['Custom'])) {
            $model->setAttributes($_POST['Custom']);

                
                try {
                    if($model->save()) {
                    Yii::app()->user->setFlash('success', "Запись создана");
                    if (isset($_GET['returnUrl'])) {
                            $this->redirect($_GET['returnUrl']);
                    } else {
                            $this->redirect(array('index'));
                    }
                }
                } catch (Exception $e) {
                        $model->addError('', $e->getMessage());
                }
        } elseif(isset($_GET['Custom'])) {
                        $model->attributes = $_GET['Custom'];
        }

        $this->render('create',array( 'model'=>$model));
    }

    public function actionUpdate($id) {
        $model = $this->safeLoadModel($id);
        
        if(isset($_POST['Custom'])) {
            $model->setAttributes($_POST['Custom']);
                try {
                    if($model->save()) {
                    Yii::app()->user->setFlash('success', "Данные обновлены");
                        if (isset($_GET['returnUrl'])) {
                                $this->redirect($_GET['returnUrl']);
                        } else {
                                $this->redirect(array('index'));
                        }
                    }
                } catch (Exception $e) {
                        $model->addError('', $e->getMessage());
                }

            }

        $this->render('update',array(
                'model'=>$model,
                ));
    }
                
               

    public function actionDelete($id) {
        if(Yii::app()->request->isPostRequest) {    
            try {
                $this->safeLoadModel($id)->delete();
            } catch (Exception $e) {
                    throw new CHttpException(500,$e->getMessage());
            }

            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                            $this->redirect(array('index'));
            }
        }
        else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request.'));
    }
                
  
    
    
    public function loadModel($id) {
            $model=Custom::model()->findByPk($id);
            if($model===null)
                    throw new CHttpException(404,Yii::t('app', 'The requested page does not exist.'));
            return $model;
    }

}