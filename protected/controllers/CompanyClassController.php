<?php
class CompanyClassController extends Controller {

    public function getZone( ) {
        return Group::ZONE_COMPANY_CLASS;
    }

    
    public $layout='column1';
    private $_allowAjax=true;
    
    public function actionIndex() {
        $this->applyGroupPolicy();
        $model = new CompanyClass('search');
        $model->unsetAttributes();

        if (isset($_GET['CompanyClass']))
                $model->setAttributes($_GET['CompanyClass']);

        $this->render('admin', array(
                'model' => $model,
        ));
    }
        
  
        
    public function actionCreate() {
        $this->applyGroupPolicy();
        $model = new CompanyClass;
                if (isset($_POST['CompanyClass'])) {
            $model->setAttributes($_POST['CompanyClass']);                
                try {
                    if($model->save()) {
                    
                        if (($this->_allowAjax)&&(Yii::app()->request->isAjaxRequest)) {
                            exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                        }
                    
                        Yii::app()->user->setFlash('success', "Запись создана");
                        if (isset($_GET['returnUrl'])) {
                                $this->redirect($_GET['returnUrl']);
                        } else {
                                $this->redirect(array('index'));
                        }
                    }  else {
                        if (($this->_allowAjax)&&(Yii::app()->request->isAjaxRequest)) {
                            exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_lightweightForm', array(
                                            'model' => $model), true))));
                        }
                    }
                    
                    
                } catch (Exception $e) {
                        $model->addError('', $e->getMessage());
                }
        } elseif(isset($_GET['CompanyClass'])) {
                        $model->attributes = $_GET['CompanyClass'];
        }
        
        if (($this->_allowAjax)&&(Yii::app()->request->isAjaxRequest)) {
            $this->renderPartial('_lightweightForm', array('model' => $model), false, true);
        } else {
        $this->render('create',array( 'model'=>$model));
        }
    }

    public function actionUpdate($id) {
        $this->applyGroupPolicy();
        $model = $this->loadModel($id);
        
        if(isset($_POST['CompanyClass'])) {
            $model->setAttributes($_POST['CompanyClass']);
                try {
                    if($model->save()) {
                    
                    if (($this->_allowAjax)&&(Yii::app()->request->isAjaxRequest)) {
                            exit(json_encode(array('result' => 'success', 'msg' => 'Your data has been successfully saved')));
                    }
                        
                    Yii::app()->user->setFlash('success', "Данные обновлены");
                        if (isset($_GET['returnUrl'])) {
                                $this->redirect($_GET['returnUrl']);
                        } else {
                                $this->redirect(array('index'));
                        }
                    }   else {
                        if (($this->_allowAjax)&&(Yii::app()->request->isAjaxRequest)) {
                            exit(json_encode(array('result' => 'error', 'msg' => $this->renderPartial('_lightweightForm', array(
                                            'model' => $model), true))));
                        }
                    }
                } catch (Exception $e) {
                        $model->addError('', $e->getMessage());
                }

            }
           if (($this->_allowAjax)&&(Yii::app()->request->isAjaxRequest)) {
            $this->renderPartial('_lightweightForm', array('model' => $model), false, true);
        } else {
        $this->render('update',array(
                'model'=>$model,
                ));
                }
    }


    
    public function loadModel($id) {
            $model=CompanyClass::model()->findByPk($id);
            if($model===null)
                    throw new CHttpException(404,Yii::t('app', 'The requested page does not exist.'));
            return $model;
    }

}