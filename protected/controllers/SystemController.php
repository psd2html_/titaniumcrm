<?php

class SystemController extends Controller
{
    public function actionTtt()
    {
        $project=Project::model()->findByPk(247);
        ZipGenerator::previewAboutProject($project);
    }
    public function actionTtt2()
    {
        var_dump(Group::getUglySql());
    }

    public function actionFlushCache()
    {
        Yii::app()->cache->flush();
        apc_clear_cache();
        apc_clear_cache('user');
        echo("Cache flushed");
    }

    public function actionF10Migration()
    {

        Yii::import("application.components.migrations.f10");
        $f = new f10();
        $f->safeUp();


        $g0=new Group();
        $g0->name="Установщик";
        $g0->isLogist=False;
        $g0->save();
        self::_addNewFodler(); return;
        return;
    }

    public function actionUpdatePriceAjax()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            throw new CHttpException(400, "not");
        }

        $kind = Yii::app()->request->getPost("kind", 1);
        $id = Yii::app()->request->getPost("id", -1);
        $value = Yii::app()->request->getPost("value", -1);
        $currency = Yii::app()->request->getPost("currency", -1);

        if ($kind == 0) {
            $m = CoreInstrument::model()->findByPk($id);
            $this->applyGroupPolicy($m,Group::ACTION_UPDATE, Group::ZONE_COREINSTRUMENT);
        } else {
            $m = Feature::model()->findByPk($id);
            $this->applyGroupPolicy($m,Group::ACTION_UPDATE, Group::ZONE_FEATURE);
        }

        if ($m) {
            $m->price = $value;
            $m->isPriceSettled = 1;
            $m->enumCurrency = $currency;
            $m->priceLastUpdated = Yii::app()->dateFormatter->format("yyyy-M-d H:m:s", time());

            if (!$m->save()) {
                //var_dump($m->getErrors());
                //die();
                throw new CHttpException(400, "not successed");
            }
        } else {
            throw new CHttpException(404, "not found");
        }

    }

    public function actionConfirmDeletion()
    {
        $mid = Yii::app()->request->getParam("mid", -1);
        $id = Yii::app()->request->getParam("id", -1);
        $m = deletionHelper::getIdToModel($mid, $id);
        if (!$m) {
            throw new CHttpException(403, "Запрещено");
        }

        if (isset($_POST['v']) && isset($_POST['id']) && isset($_POST['mid'])) {
            $mid = Yii::app()->request->getParam('mid');
            $id = Yii::app()->request->getParam('id');
            $v = Yii::app()->request->getParam('v');

            if ($v == $this->getValidation($mid, $id)) {
                $m->delete();
                if (isset($_GET['returnUrl'])) {
                    $this->redirect($_GET['returnUrl']);
                } else {
                    $this->redirect(array('/personal/index'));
                }
                //echo('CORRRECT');
                //die();
            }
        }
        $this->render("confirmDeletion", array(
            "model" => $m,
            "mid" => $mid,
            "id" => $id,
            "validation" => $this->getValidation($mid, $id)
        ));
    }

    private function getValidation($mid, $id)
    {
        return md5($mid . "-" . $id . "DXJJEdsk33211");
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('deny', // deny all users
                'users' => array('?'),
            ),
        );
    }



    private function _addNewFodler()
    {
        $ff=Project::model()->findAll();
        foreach ($ff as $p) {
            $fo=new Folder();
            $fo->idProject=$p->idProject;
            $fo->parent=Null;
            $fo->name="Установка";
            $fo->enumSpecialFolder=Folder::SYSTEM_FOLDER;
            if (!$fo->save()) {
                var_dump("!!!!");
                die();
            }
            var_dump("done");
        }

    }


}
