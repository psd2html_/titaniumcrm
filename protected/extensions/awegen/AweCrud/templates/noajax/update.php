<?php
echo "<?php\n";
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs = array(
    Yii::t('app', '$label') => array('index'),
    Yii::t('app', 'Update'),
); ?>";
?>

<header> <h3> <?= '<?= $model->name; ?>' ?> </h3> </header>
<div class="module_content">
<?php echo "<?php\n"; ?>
$this->renderPartial('_form', array(
			'model'=>$model));
?>
</div>