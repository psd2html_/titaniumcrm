<?php echo <<<EOD
<?php if (Yii::app()->user->hasFlash('success')): ?>

    <div class="alert-box success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
        <a href="" class="close">&times;</a>
    </div>
<?php endif; ?>
EOD;
?>

<div class="row full-width">
    <div class="twelve columns">
        <h4><?php echo '<?='; ?> $model->isNewRecord ? 'Создать новое правило' : 'Редактировать правило' ?></h4>

    <?php echo '<?php'; ?>

    $form=$this->beginWidget('CActiveForm', array(
    'id'=>'ajax-form',
    'enableAjaxValidation'=><?php echo $this->validation == 1 || $this->validation == 3 ? 'true' : 'false'; ?>,
    'enableClientValidation'=><?php echo $this->validation == 2 || $this->validation == 3 ? 'true' : 'false'; ?>,
    ));

    echo $form->errorSummary($model);
    ?>
    <?php
    foreach ($this->tableSchema->columns as $column) {
        //continue if it is an auto-increment field or if it's a timestamp kinda' stuff
        if ($column->autoIncrement || in_array($column->name, array_merge($this->create_time, $this->update_time)))
            continue;

        //skip many to many relations, they are rendered below, this allows handling of nm relationships
        foreach ($this->getRelations() as $relation) {
            if ($relation[2] == $column->name && $relation[0] == 'CManyManyRelation')
                continue 2;
        }
        ?>

        
            <?php echo "<?php echo " . $this->generateActiveLabel($this->modelClass, $column) . "; ?>\n"; ?>
            <?php echo "<?php " . $this->generateField($column, $this->modelClass) . "; ?>\n"; ?>
            <?php echo "<?php echo \$form->error(\$model,'{$column->name}'); ?>\n"; ?>
         
        <?php
    }

    echo("<?php ") ?> echo CHtml::ajaxSubmitButton($model->isNewRecord ? 'Создать' : 'Cохранить', Yii::app()->createUrl(
                    $model->isNewRecord ?  '<?=$this->controllerId?>/create' : '<?=$this->controllerId?>/update/' . $model->getPrimaryKey() 
            ), array(
        'type' => 'POST',
        'dataType' => 'json',
        'data' => 'js:$("#ajax-form").serialize()', //this one
        'success' => "js:function(data){
            if(data.result==='success'){
                location.reload();
            }else{
                $('#foundationModal').html(data.msg);
            }
        }",
            ), array('class' => 'button'));
    
    $this->endWidget();
    ?>
    
    </div>
</div> <!-- form -->

