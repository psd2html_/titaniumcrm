<?php
/**
 * This is the template for generating a controller class file for CRUD feature.
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
class <?php echo $this->controllerClass; ?> extends <?php echo $this->baseControllerClass." {\n"; ?>

<?php 
        $authpath = 'ext.awegen.AweCrud.templates.default.auth.';
	Yii::app()->controller->renderPartial($authpath . $this->authtype);
?>
    
	public function actionRCH($id)
    {
    	$theatre=$this->loadModel($id);
    	if ($theatre) {
    		$this->renderPartial("_rch",array('model'=>$theatre),false,true);
    	}
    }
    
    public function actionIndex() {
        $model = new <?php echo $this->modelClass; ?>('search');
        $model->unsetAttributes();

        if (isset($_GET['<?php echo $this->modelClass; ?>']))
                $model->setAttributes($_GET['<?php echo $this->modelClass; ?>']);

        $this->render('index', array(
                'model0' => $model,
        ));
    }
        
   
       /*--------------------------------*/

    public function actionDeleteAjax($id) {
    
    	try {
    		$m=$this->loadModel($id);
    		$m->delete();
    	} catch (Exception $e) {
    		throw new CHttpException(500,$e->getMessage());
    	}
    	
    	$model = new <?php echo $this->modelClass; ?>;
    
    	return CJSON::encode("ok");
    
    	$this->renderPartial('_formAjax',array(
    			'model'=>$model,
    			'url'=>Yii::app()->createUrl(self::controller."/createAjax"),
    			'refresh'=>true,
    	),false,true);
    
    }
    
    public function actionCreateAjax() {
    	$model = new <?php echo $this->modelClass; ?>; //<< REFRESH!
    	
    	if(isset($_GET["<?php echo $this->modelClass; ?>"])) 
    		$model->setAttributes($_GET["<?php echo $this->modelClass; ?>"]);
    	
    	if(isset($_POST["<?php echo $this->modelClass; ?>"])) {
    		$model->setAttributes($_POST["<?php echo $this->modelClass; ?>"]);
		    
    		try {
    
    			if($model->save()) {
    
    
    				$id=$model->getPrimaryKey();
    				Yii::app()->user->setFlash('success', "Data saved!");
    				$model0=new <?php echo $this->modelClass; ?>;  
    				
    				
    				$this->renderPartial('_formAjax',array(
    						'model'=>$model0,
    						'url'=>Yii::app()->createUrl("<?php echo $this->modelClass; ?>"."/createAjax"),
    						'refresh'=>true,
    				),false,true);
    
    				return;
    
    			}
    		} catch (Exception $e) {
    			$model->addError('idPerson', $e->getMessage());
    		}
    	}
    
    	$this->renderPartial('_formAjax',array(
    			'model'=>$model,
    			'url'=>Yii::app()->createUrl("<?php echo $this->modelClass; ?>"."/createAjax"),
    
    	),false,true);
    
    }
    
    
    
    public function actionUpdateAjax($id) {
    
    	$model = $this->loadModel($id);
    	 
    	if(isset($_POST["<?php echo $this->modelClass; ?>"])) {
    		$model->setAttributes($_POST["<?php echo $this->modelClass; ?>"]);
    		
    			
    		try {
    
    			if($model->save()) {
    
    				Yii::app()->user->setFlash('success', "Data saved!");
    
    
    
    				$this->renderPartial('_formAjax',array(
    						'model'=>$model,
    						'url'=>Yii::app()->createUrl("<?php echo $this->modelClass; ?>"."/updateAjax/".$model->getPrimaryKey()),
    						'refresh'=>true,
    				),false,true);
    					
    				return;
    			}
    		} catch (Exception $e) {
    
    			$model->addError('idRoleTemplate', $e->getMessage());
    		}
    	}
    
    
    	$this->renderPartial('_formAjax',array(
    			'model'=>$model,
    			'url'=>Yii::app()->createUrl("<?php echo $this->modelClass; ?>"."/updateAjax/".$id)
    	),false,true);
    
    
    }
    /*--------------------------------*/
   
            
    <?php
    if ($this->hasBooleanColumns($this->tableSchema->columns) && $this->isJToggleColumnEnabled){
        ?>
    public function actionToggle($id, $attribute, $model) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $model = $this->loadModel($id, $model);
            //loadModel($id, $model) from giix
            ($model->$attribute == 1) ? $model->$attribute = 0 : $model->$attribute = 1;
            $model->save();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }
    <?php
    }
    
    
    ?>

    public function loadModel($id) {
            $model=<?php echo $this->modelClass; ?>::model()->findByPk($id);
            if($model===null)
                    throw new CHttpException(404,Yii::t('app', 'The requested page does not exist.'));
            return $model;
    }

}