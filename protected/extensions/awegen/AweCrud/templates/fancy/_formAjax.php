<? echo "<?php\n" ?>
foreach(Yii::app()->user->getFlashes() as $key => $message) {
	echo '<div class="info flash-' . $key . '">' . $message . "</div>\n";
}
 
Yii::app()->clientScript->registerScript(
		'myHideEffect',
		'
		$(".info").animate({opacity: 1.0}, 3000).fadeOut("slow");',
		CClientScript::POS_READY
);


if (isset($refresh)) {
Yii::app()->clientScript->registerScript('updateOnce', "
		
		$.fn.yiiGridView.update('person-grid');

"); }
<? echo "?>\n" ?>


<div class="form">
    

    <? echo "<?php\n" ?>
    $form=$this->beginWidget('CActiveForm', array(
    		'action'=>$url,
    'id'=>'performance-form',
    'enableAjaxValidation'=>false,
    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
    'enableClientValidation'=>true,
    ));

    echo $form->errorSummary($model);
    <? echo "?>\n" ?>
    	<h3> <? echo "<?php\n" ?> echo ($model->isNewRecord ? 'Создать новую категорию' : $model->name ); <? echo "?>\n" ?> </h3>
    	
    	<? echo "<?php\n" ?>
    	$this->renderPartial('_form',array('model'=>$model,'form'=>$form));
    	<? echo "?>\n" ?>
    	
        <? echo "<?php\n" ?>
		
			 echo CHtml::button($model->isNewRecord ? 'Создать':'Сохранить',array('onclick'=>
        		" 
        		if(typeof CKEDITOR!='undefined'){
        		for ( instance in CKEDITOR.instances ) {
        		CKEDITOR.instances[instance].updateElement();
        		delete CKEDITOR.instances[instance];
        		console.log('ee'+instance);
				}
        		}
        		$.ajax({type:'POST',
						url: '$url',
        				data: $(this).parent().serialize(),
						success: function(data){
											$('body').off();
											$('.ajax_container').html(data);
											},
						'error':function(data) {
						alert(data);
        				}});
        		"
        		));
		
		<? echo "?>\n" ?>




<? echo "<?php\n" ?>
            
            $this->endWidget(); <? echo "?>\n" ?>
</div> <!-- form -->