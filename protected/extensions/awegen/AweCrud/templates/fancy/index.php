<?php
$label = $this->pluralize($this->class2name($this->modelClass));

echo "<?php\n";
echo "\$this->breadcrumbs = array(
    Yii::t('app', '$label')
);";
echo "?>\n";
?>

<h1><?php echo $label; ?></h1>

<?php echo "<?php"; ?>

$this->widget('ext.splitEdit.splitEdit', array(
		'defaultOrder' => 'name ASC',
		'controller' => '<?php echo $this->modelClass;?>',
		'cols' => array('name'),
		//'argsForCreate'=> array("idPerformance"=>$model0->performance->idPerformance),
		'model0' => $model0,
	//	'rightColumnUrl'=>'rch',
		'action'=>0
)); 

<?php echo "?>\n"; ?>
