<?php
/**
 * SplitEdit
*
* @version 0.1
* @author SD
* @link http://darkwater.ru
*/
class SplitEdit extends CWidget {

	/**
	 * Html ID
	 * @var string
	 */

	public $defaultOrder='name ASC';
	public $controller='roletemplate';
	public $rightColumnUrl='';
	public $model0;
	public $action=0;
	public $cols=array('name');
	public $argsForCreate=array();
	/**
	 * Initial tags
	 * @var array
	 */
	public $formId;
	public $tags;

	/**
	 * The url to get json data
	 * @var string
	 */
	public $url;
	public function init()
	{
		// this method is called by CController::beginWidget()
	}

	public function run()
	{
	switch ($this->action) {
		case 1:
			$this->render('formHeader', array(
					'url' => $this->url,
			));
		break;
		case 2:
			$this->render('formSubmit', array(
					'url' => $this->url,
			));
		break;
		default:
			$this->render('splitView', array(
					'defaultOrder' => $this->defaultOrder,
					'controller' => $this->controller,
					'model0' => $this->model0,
					'cols' => $this->cols,
					'argsForCreate' => $this->argsForCreate,
					'rightColumnUrl'=> $this->rightColumnUrl
			));
		break;
	}
		
	}
}
?>