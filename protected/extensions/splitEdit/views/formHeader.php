<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
	echo '<div class="info flash-' . $key . '">' . $message . "</div>\n";
}
 
Yii::app()->clientScript->registerScript(
		'myHideEffect',
		'$(".info").animate({opacity: 1.0}, 3000).fadeOut("slow");',
		CClientScript::POS_READY
);
?>

<?php if (isset($refresh)) {
Yii::app()->clientScript->registerScript('updateOnce', "
		
		$.fn.yiiGridView.update('person-grid');

"); }
?>

