<?php

class ProDate extends CWidget {

    private $template = 'basic';
    public $format = "%Y-%m-%d";
    public $model;
    public $attribute;
    
    private $scriptFile;
    private $scriptUrl;
    private $themeUrl;
    private $cssFile;
    private $scriptFile_dev = array('datepicker.js','lang/en.js');
    private $cssFile_dev = array('datepicker.css');
    private $scriptFile_production = array('datepicker.js','lang/en.js');
    private $cssFile_production = array('datepicker.css');
    private $forcePublishOnDebug = true;
    public $data = array();
    public $options = array();
    public $htmlOptions = array();

    public function init() {
        if (YII_DEBUG) {
            $this->scriptFile = $this->scriptFile_dev;
            $this->cssFile = $this->cssFile_dev;
        } else {
            $this->scriptFile = $this->scriptFile_production;
            $this->cssFile = $this->cssFile_production;
        }
        $this->resolvePackagePath();
        $this->registerCoreScripts();
        parent::init();
    }

    protected function resolvePackagePath() {

        $basePath = Yii::getPathOfAlias('application.extensions.proDate.assets');
        $baseUrl = Yii::app()->getAssetManager()->publish($basePath, false, -1, YII_DEBUG && $this->forcePublishOnDebug);
        if ($this->scriptUrl === null)
            $this->scriptUrl = $baseUrl . '';
        if ($this->themeUrl === null)
            $this->themeUrl = $baseUrl . '';
    }

    protected function registerCoreScripts() {

        $cs = Yii::app()->getClientScript();
        if (is_string($this->cssFile))
            $this->registerCssFile($this->cssFile);
        else if (is_array($this->cssFile)) {
            foreach ($this->cssFile as $cssFile)
                $this->registerCssFile($cssFile);
        }

        $cs->registerCoreScript('jquery');

        if (is_string($this->scriptFile))
            $this->registerScriptFile($this->scriptFile);
        else if (is_array($this->scriptFile)) {
            foreach ($this->scriptFile as $scriptFile)
                $this->registerScriptFile($scriptFile);
        }
    }

    protected function registerScriptFile($fileName, $position = CClientScript::POS_HEAD) {
        Yii::app()->clientScript->registerScriptFile($this->scriptUrl . '/' . $fileName, $position);
    }

    protected function registerCssFile($fileName) {
        Yii::app()->clientScript->registerCssFile($this->themeUrl . '/' . $fileName);
    }

    /**
     * @var string the name of the container element that contains the progress bar. Defaults to 'div'.
     */
    public $tagName = 'div';

    /**
     * Run this widget.
     * This method registers necessary javascript and renders the needed HTML code.
     */
    public function run() {
        $id = $this->getId();
        $this->htmlOptions['id'] = (CHtml::activeId($this->model, $this->attribute));
        if (YII_DEBUG) 
        {
            $this->options['debug'] =1;
        }
        $this->options['formElements'] =array((CHtml::activeId($this->model, $this->attribute))=>$this->format);
        //$this->options['id'] =(CHtml::activeId($this->model, $this->attribute));
        //$this->options['format']= $this->format;
        $encodeoptions = CJavaScript::encode($this->options);

        $this->render($this->template, array(
            'model' => $this->model,
            'attribute' => $this->attribute,
            'tagName' => $this->tagName,
            'htmlOptions' => $this->htmlOptions,
            'encodeOptions' => $encodeoptions,
                )
        );
    }

}