<div class="row">
    <div class="ten columns">
        <?php
        echo CHtml::openTag($this->tagName, $this->htmlOptions);
        echo CHtml::closeTag($this->tagName);
        ?>

    </div>
    <div class="two columns">
        <?= $this->render("_filter", array('filters' => $filters), false, true); ?>
        <?php
        if ($projectFilters) {
            echo ($this->render("_projectFilter", array('filters' => $projectFilters), false, true));
        }
        ?>
    </div>
</div>

<?php
$id = $this->getId();
Yii::app()->getClientScript()->registerScript(__CLASS__ . '#' . $id, "$('#$id').fullCalendar($encodeOptions);");
?>

<?php $this->render("_scripts", array('filters' => $filters,'isAjax'=>true,'isProjectFiltered'=>true, 'id'=>$id), false, true) ?>

