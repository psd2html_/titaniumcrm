<?php
/*
 * @var $filters array()
 */
?>

<table>
    <thead>
        <th colspan="2"><a href="#" class="ajaxLink" onclick="$('#personalFilter').toggle(); return false;">Фильтр событий</a></th>
    </thead>
    <tbody id="personalFilter" style="display:none">
        <?php
        foreach ($filters as $t => $v) {
            ?>
            <tr>
                <td width="32px"><?= (CHtml::checkBox('calendarFilter_' . $t . '', true, array('class' => 'calendarFilter', 'f_val' => $t))); ?></td>
                <td> <?= (CHtml::label($v, 'calendarFilter_' . $t . '')); ?></td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>


