<?php
$defaultFilters = $filters;
foreach ($defaultFilters as $v => $vv) {
    $defaultFilters[$v] = 1;
}
?>

<script>
    var cookieConfig = new Object();
    var disabledProjects = [];

    function initFilter() {
        var cookieConfigSerialized = $.cookie('filterStatus')

        if (cookieConfigSerialized === undefined) {
            cookieConfig =<?= CJSON::encode($defaultFilters); ?>;
            $.cookie("filterStatus", JSON.stringify(cookieConfig));
        }
        else {
            cookieConfig = JSON.parse(cookieConfigSerialized);

            for (var propertyName in cookieConfig) {
                if (cookieConfig[propertyName] == 0) {
                    $('input[f_val=' + propertyName + ']').attr('checked', false);
                }
            }
        }

        var disabledProjectsSerialized = $.cookie('disabledProjects')
        if (disabledProjectsSerialized === undefined) {
            disabledProjects = [];
            $.cookie("disabledProjects", JSON.stringify(disabledProjects));
        }
        else {
            disabledProjects = JSON.parse(disabledProjectsSerialized);

            for (i = 0; i < disabledProjects.length; i++) {
                $('#calendarProjectFilter_' + disabledProjects[i]).attr('checked', false);
            }
        }
    }

    initFilter();

    function userHasFilteredOut(e) {
        console.log(e.filterName);
        <?php if (isset($isProjectFiltered)) { ?>

        return (cookieConfig[e.filterName] == 0) || (disabledProjects.indexOf(e.projectId) >= 0);
        <?php } else { ?>
        return (cookieConfig[e.filterName] == 0);
        <?php } ?>
    }

    function applyFilter(view) {
        for (var propertyName in cookieConfig) {
            if (cookieConfig[propertyName] == 0) {
                $('.' + propertyName).hide();
            }
        }

        window.dwDialog.bindTriggers();
    }

    $('.calendarProjectFilter').click(function () {
        var t = $(this).attr('pid');
        if ($(this).is(":checked")) {
            $('a[pid=' + t + ']').show();
            var index = disabledProjects.indexOf(t);
            disabledProjects.splice(index, 1);

        }
        else {
            $('a[pid=' + t + ']').hide();
            disabledProjects.push(t);
        }

        <?php
        if (isset($isAjax)) {
            echo("$('#$id').fullCalendar('refetchEvents');");
        }
        ?>

        $.cookie("disabledProjects", JSON.stringify(disabledProjects));
    });

    $('.calendarFilter').click(function () {
            var t = $(this).attr('f_val');
            if ($(this).is(":checked")) {
                $('.' + t).show();
                cookieConfig[t] = 1;
            }
            else {
                $('.' + t).hide();
                cookieConfig[t] = 0;
            }
            <?php
            if (isset($isAjax)) {
                echo("$('#$id').fullCalendar('refetchEvents');");
            }
            ?>

            $.cookie("filterStatus", JSON.stringify(cookieConfig));
        }
    );
</script>