<div>
    <?php
    echo CHtml::openTag($this->tagName, $this->htmlOptions);
    echo CHtml::closeTag($this->tagName);
    ?>
</div> 

<div class="more">
    <h6>Фильтр событий</h6>
    <?= $this->render("_filterUl", array('filters' => $filters), false, true); ?>
</div>
<?php
$id = $this->getId();
Yii::app()->getClientScript()->registerScript(__CLASS__ . '#' . $id, "$('#$id').fullCalendar($encodeOptions);");
?>

<?php $this->render("_scripts", array('filters' => $filters), false, true) ?>