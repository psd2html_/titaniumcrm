<?php
/*
 * @var $filters array()
 */
?>


<?php
foreach ($filters as $t => $v) {
    ?>
    <div class="row">
        <div class="one column"><?= (CHtml::checkBox('calendarFilter_' . $t . '', true, array('class' => 'calendarFilter', 'f_val' => $t))); ?></div>
        <div class="eleven column"> <?= (CHtml::label($v, 'calendarFilter_' . $t . '')); ?></div>
    </div>

    <?php
}
?>


