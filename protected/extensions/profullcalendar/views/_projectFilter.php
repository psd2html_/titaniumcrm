<?php
/*
 * @var $filters array()
 */
?>
<table>
    <thead>
        <th width="32px"><a href="#" class="small secondary button" onclick='$("#filterProjectsBody").toggle("fast"); return false;'><img src="/images/down.gif"/> </a> </th><th >Фильтр проектов</th>
    </thead>
    <tbody id="filterProjectsBody" style="display: none">
        <?php
        foreach ($filters as $p) {
            ?>
            <tr>
                <td width="32px"><?= (CHtml::checkBox('calendarProjectFilter_' . $p->idCoreInstrumentClass . '', true, array('class' => 'calendarProjectFilter', 'pid' => $p->idCoreInstrumentClass))); ?></td>
                <td> <?= (CHtml::label($p->name, 'calendarProjectFilter_' . $p->idCoreInstrumentClass . '')); ?></td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>



