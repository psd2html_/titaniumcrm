<?php

class ProFullcalendarGraphWidget extends CWidget {

    public $allowQtip = false;
    public $filters;
    public $projectFilters = array();
    public $template = 'week';
    public $ajaxUrl = null;
    public $scriptUrl;
    public $themeUrl;
    public $theme = 'base';
    public $scriptFile = array('jquery-ui-custom.js', 'fullcalendar.js', 'jquery.cookie.js');
    public $cssFile = array('fullcalendar.css');
    public $data = array();
    public $options = array();
    public $htmlOptions = array();

    public function init() {
        if (!$this->filters) {
            $this->filters = array(
             //  'eventProjectTag' => 'Заметки',
                'logisticTag' => 'Логистика',
                'poTag' => 'Коммерческие предложения',
                'contestTag' => 'События конкурса',
             //   'checkpointTag' => 'Выполнение контрольных точек',
                'taskToMeTag' => 'Задачи мне',
                'taskByMeTag' => 'Мои задачи',
                'otherTaskTag' => 'Прочие задачи',
            );
        }

        if ($this->allowQtip) {
            $this->scriptFile[] = 'jquery.qtip.js';
            $this->cssFile[] = 'jquery.qtip.css';
        }

        $this->resolvePackagePath();
        $this->registerCoreScripts();
        parent::init();
    }

    protected function resolvePackagePath() {
        if ($this->scriptUrl === null || $this->themeUrl === null) {
            $basePath = Yii::getPathOfAlias('application.extensions.profullcalendar.assets');
            $baseUrl = Yii::app()->getAssetManager()->publish($basePath, false, -1, !true);
            if ($this->scriptUrl === null)
                $this->scriptUrl = $baseUrl . '';
            if ($this->themeUrl === null)
                $this->themeUrl = $baseUrl . '';
        }
    }

    protected function registerCoreScripts() {
        $cs = Yii::app()->getClientScript();
        if (is_string($this->cssFile))
            $this->registerCssFile($this->cssFile);
        else if (is_array($this->cssFile)) {
            foreach ($this->cssFile as $cssFile)
                $this->registerCssFile($cssFile);
        }

        $cs->registerCoreScript('jquery');
        if (is_string($this->scriptFile))
            $this->registerScriptFile($this->scriptFile);
        else if (is_array($this->scriptFile)) {
            foreach ($this->scriptFile as $scriptFile)
                $this->registerScriptFile($scriptFile);
        }
    }

    protected function registerScriptFile($fileName, $position = CClientScript::POS_HEAD) {
        Yii::app()->clientScript->registerScriptFile($this->scriptUrl . '/' . $fileName, $position);
    }

    protected function registerCssFile($fileName) {
        Yii::app()->clientScript->registerCssFile($this->themeUrl . '/' . $fileName);
    }

    /**
     * @var string the name of the container element that contains the progress bar. Defaults to 'div'.
     */
    public $tagName = 'div';

    /**
     * Run this widget.
     * This method registers necessary javascript and renders the needed HTML code.
     */
    public function run() {
        $id = $this->getId();
        $this->htmlOptions['id'] = $id;


        if ($this->allowQtip) {
            $this->options['eventRender'] = $this->getEventRender();
        }
        $this->options['eventAfterAllRender'] = 'js:applyFilter';
        if ($this->ajaxUrl) {
            $eventsAjax = array('url' => $this->ajaxUrl,
                                'success'=> $this->getSuccessFilter()); //success
            
            $encodeoptions = CJavaScript::encode($this->options + array(
                'loading' => 'js:function(bool) {
                                if (bool) 
                                    $("#'.$id.'").addClass("loading-control"); 
                                else 
                                    $("#'.$id.'").removeClass("loading-control"); 
                                 }',
                'events' => ($eventsAjax)
                ));
         
       } else {
            $encodeoptions = CJavaScript::encode($this->options + array('events' => ($this->data)));
        }
        $this->render($this->template, array(
            'tagName' => $this->tagName,
            'htmlOptions' => $this->htmlOptions,
            'encodeOptions' => $encodeoptions,
            'filters' => $this->filters,
            'projectFilters' => $this->projectFilters,
                )
        );
    }
    private function getSuccessFilter() {
        $s= <<< END
js:function(events) {
   events =  $.map(events, function (e) {
      if (userHasFilteredOut(e))
        return null;
      else
        return e;
    });
    return events;
    
  }
END;
        return $s;
    }
    private function getEventRender() {
        $spinner = Helpers::getSpinnerGif();
        $eventRender = <<< END
js:function(event, element) {
if (event.projectId)
    {
    $(element).attr('pid',event.projectId);
    }
 window.dwDialog.bindTriggers();
if (event.qt_url && $(element).hasClass('eventTag')) {
        var url=event.qt_url;

        var title=$(element).find('.fc-event-title').html();
        var myUrl=url.replace("update","shortView");


        $(element).qtip(
            {
                content: {
                    text: '$spinner',
                    ajax: {
                        url: myUrl // Use the rel attribute of each element for the url to load
                    },
                    title: {
                        text: title, // Give the tooltip a title using each elements text
                        button: false
                    }
                },
                position: {
                    target: 'mouse',
                    my: 'top center',
                    adjust: {
                        // screen: true,
                       // x: 15,
                        y: 20
                    }
                },
                show: {
                    event: 'mouseenter',
                    effect: true,
                    delay: 140,
                    solo: true // Only show one tooltip at a time
                },
                
                style: {
                    //name: 'light'
                    classes: 'qtip-bootstrap'
                }
            });     
            }
            }
END;
        return $eventRender;
    }

}