<script type="text/javascript">

    function ajaxRelationController($scope, $http, $timeout) {

        $scope.variants = [];
        $scope.loading = !true;
        $scope.sm = 0; // 0 - idle, 1 - waiting for deferred, 2 - loading
        var deferred;

        function ajaxConvert(inputObject) {
            var o = [];
            for (var key in inputObject) {
                o.unshift({id: key, name: inputObject[key]});
            }
            return o;
        }

        $scope.$watch('filter', function () {
            if (($scope.filter) && (!$scope.loading)) {

                if (deferred) {
                    $timeout.cancel(deferred);
                }

                $scope.sm = 1;

                deferred = $timeout(function () {
                    $scope.sm = 2;
                    //console.log('Yzy');
                    $scope.loading = true;
                    $http.get($scope.ajaxUrl + "?q=" + $scope.filter).then(
                        function (data) {
                            $scope.sm = 0;

                            $scope.loading = false;
                            $scope.variants = ajaxConvert(data.data);
                        },
                        function (err) {
                            $scope.sm = 0;
                        }
                    );
                }, 500);
            }
        });

        $scope.removeItem = function (item) {
            var id = $scope.data.indexOf(item);
            if (id != -1) {
                $scope.data.splice(id, 1);
            }
        }
        $scope.addItem = function (d) {
            if ($scope.data.indexOf(d) == -1) {
                $scope.data.unshift(d);
            }
            return;
        }
    }

</script>

<div ng-controller="ajaxRelationController" ng-init="data=<?= str_replace( array("<", "\""),array("&lt;","&quot;") ,CJavaScript::encode($ng_relations)) ?>; ajaxUrl='<?= $ajax_url ?>';" >
    <fieldset>

        <legend><?= $formName ?>  </legend>

        <div class="row">
            <div class="five columns">
                <div class="row collapse">
                    <div class="eleven columns"><input type="text" ng-model="filter" autocomplete=false
                                                       name="search-filter"
                                                       placeholder="Введите первые буквы для поиска.."/></div>
                    <div class="one column"><span class="postfix">&#128269;</span></div>
                </div>

                <div class="indent-bot-10" ng-show="!filter&&(sm==0)">
                    Введите первые буквы имени, и здесь будут варианты для добавления
                </div>

                <div class="indent-bot-10" ng-show="filter&&(sm==1)">
                    Поиск "{{ filter }}" ...
                </div>

                <div class="indent-bot-10" ng-show="filter&&(sm==2)">
                    Получение ответа ...
                </div>

                <div class="indent-bot-10" ng-show="(variants.length<1)&&(filter)&&(sm==0)">
                    По запросу ничего не найдено
                </div>

                <ul class="indent-bot-10">
                    <li ng-repeat="d in variants">

                        <input type="button" value="+" ng-click="addItem( d )">
                        <span ng-bind-html="d.name"></span>
                    </li>
                </ul>

            </div>
            <div class="six columns span-1">

                <h5>Текущий выбор</h5>

                <input type="hidden" value="" name="{{data.length==0?'<?= $this->name ?>':'__emptyFix' }}"/>

                <div ng-show="data.length==0">
                    <i>Ничего не выбрано</i>
                </div>

                <ul class="indent-bot-10">
                    <li ng-repeat="d in data">
                        <input type="hidden" value="{{ d.id }}" id="<?= $this->id ?>" name="<?= $this->name ?>[]"/>
                        <input type="button" value="X" ng-click="removeItem( d )">
                        <span ng-bind-html="d.name"></span>
                    </li>
                </ul>

            </div>
        </div>


    </fieldset>
</div>
