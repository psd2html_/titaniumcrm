<script type="text/javascript">

    function browseRelationController($scope, $http, $timeout) {
        $scope.data=<?= CJavaScript::encode($value) ?>;
        $scope.ajaxUrl='<?= $ajaxUrl ?>';
        $scope.variants = [];
        $scope.loading = !true;
        $scope.sm = 0; // 0 - idle, 1 - waiting for deferred, 2 - loading
        var deferred;

        function ajaxConvert(inputObject) {
            var o = [];
            for (var key in inputObject) {
                o.unshift({id: key, name: inputObject[key]});
            }
            return o;
        }

        $scope.$watch('filter', function () {
            if (($scope.filter) && (!$scope.loading)) {

                if (deferred) {
                    $timeout.cancel(deferred);
                }

                $scope.sm = 1;

                deferred = $timeout(function () {
                    $scope.sm = 2;
                    //console.log('Yzy');
                    $scope.loading = true;
                    $http.get($scope.ajaxUrl + "?q=" + $scope.filter).then(
                        function (data) {
                            $scope.sm = 0;
                            // console.log(data);
                            $scope.loading = false;
                            $scope.variants = ajaxConvert(data.data);
                        },
                        function (err) {
                            $scope.sm = 0;
                        }
                    );
                }, 500);
            }
        });
        $scope.removeItem = function (item) {

            $scope.data = null;

        }
        $scope.addItem = function (d) {
            $scope.data = d;
            return;
        }
    }

</script>

<div ng-controller="browseRelationController"
     ng-init="allowEmpty='<?= $allowEmpty ?>'; emptyText='<?= $emptyText ?>';  ">
    <fieldset>

        <legend><?= $formName ?>  </legend>

        <div class="row">
            <div class="five columns">
                <div class="row collapse">
                    <div class="eleven columns"><input type="text" autocomplete=false ng-model="filter"
                                                       name="search-filter"
                                                       placeholder="Введите первые буквы для поиска.."/></div>
                    <div class="one column"><span class="postfix">&#128269;</span></div>
                </div>

                <div class="indent-bot-10" ng-show="!filter&&(sm==0)">
                    Введите первые буквы имени, и здесь будут варианты для добавления
                </div>

                <div class="indent-bot-10" ng-show="filter&&(sm==1)">
                    Поиск "{{ filter }}" ...
                </div>

                <div class="indent-bot-10" ng-show="filter&&(sm==2)">
                    Получение ответа ...
                </div>

                <div class="indent-bot-10" ng-show="(variants.length<1)&&(filter)&&(sm==0)">
                    По запросу ничего не найдено
                </div>

                <ul class="indent-bot-10">
                    <li ng-repeat="d in variants">

                        <input type="button" value=">" ng-click="addItem( d )">
                        <span ng-bind-html="d.name"></span>
                    </li>
                </ul>

            </div>
            <div class="six columns span-1">

                <h5>Текущий выбор</h5>

                <input type="hidden" value="{{ data?data.id:'' }}" id="<?= $this->id ?>" name="<?= $this->name ?>"/>

                <div ng-show="data">
                    <input ng-show="allowEmpty" type="button" value="X" ng-click="removeItem( )">
                    <span ng-bind-html="data.name"></span>
                </div>

                <div ng-show="!data">
                    <i>Ничего не выбрано</i>
                </div>

            </div>
        </div>


    </fieldset>
</div>
