<script type="text/javascript">

    function relationController($scope) {

        $scope.removeItem = function (item) {
            var id = $scope.data.indexOf(item);
            if (id != -1) {
                $scope.data.splice(id, 1);
            }
        }
        $scope.addCurrentItem = function () {

            for (i = 0; i < $scope.variants.length; i++) {
                if ($scope.variants[i].id == $scope.selected_item) {
                    if ($scope.data.indexOf($scope.variants[i]) == -1) {
                        $scope.data.unshift($scope.variants[i]);
                    }
                    return;
                }
            }

        }
    }

</script>

<div ng-controller="relationController" ng-init="data=<?= str_replace( array("<", "\""),array("&lt;","&quot;") ,CJavaScript::encode($ng_relations)) ?>; variants= <?= str_replace( array("<", "\""),array("&lt;","&quot;") ,CJavaScript::encode($ng_options)) ?>;">
    <fieldset>

        <legend><?= $formName ?>  </legend>

        <div class="row">
            <div class="five columns">
                <div class="row indent-bot-10 ">
                    <div class="eleven columns">

                        <select id="selector" class="selector" name="selector" ng-model="selected_item">
                            <option ng-repeat="d in variants" value="{{ d.id }}" ng-bind-html="d.name">
                            </option>
                        </select>
                    </div>
                    <div class="one columns">
                        <input type="button" value="Добавить" ng-click="addCurrentItem()">
                    </div>
                </div>
            </div>
            <div class="six columns span-1">
                <h5>Текущий выбор</h5>

                <input type="hidden" value="" name="{{data.length==0?'<?=$this->name?>':'__emptyFix' }}" />

                <div ng-show="data.length==0">
                    <i>Ничего не выбрано</i>
                </div>

                <ul class="indent-bot-10">
                    <li ng-repeat="d in data">
                        <input type="hidden" value="{{ d.id }}" id="<?= $this->id ?>" name="<?= $fieldName ?>"/>
                        <input type="button" value="X" ng-click="removeItem( d )">
                        <span ng-bind-html="d.name"></span>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
</div>
