<?php

class Single extends CInputWidget
{
 //   public $model;
 //   public $attribute;
    public $allowEmpty;
    public $relation;
    public $fields;
    public $ajaxUrl;
    public $emptyText='Отсутствует';

    public function run()
    {
        $v = null;
        $relName=$this->relation;

        if ($r=$this->model->$relName) {

            $f=$this->fields;

            $name=$r->$f;

            $v = array('id' => $this->model->getAttribute($this->attribute),
                'name' => $name);
        }
        list($this->name,$this->id)=$this::resolveNameID();
        $this->render('browse', array(
            'value' => $v,
            'ajaxUrl' => $this->ajaxUrl,
            'emptyText' => $this->emptyText,
            'allowEmpty' => $this->allowEmpty,
            'formName' => $this->model->getAttributeLabel($this->relation),
            'fieldName' => $this->name,
        ));

    }

}
