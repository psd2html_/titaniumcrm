<?php

class Single extends CInputWidget
{
    //   public $model;
    //   public $attribute;
    public $allowEmpty;
    public $relation;
    public $fields;
    public $ajaxUrl;
    public $emptyText = 'Отсутствует';

    public function run()
    {
        $v = null;
        $n = $this->name;
        $r = $this->relation;
        $v = array_map(function ($o) { $n=$this->name;
            return array('id' => $o->getPrimaryKey(), 'name' => $o->$n);}, $this->model->$r);

        list($this->name, $this->id) = $this::resolveNameID();
        $this->render('sortable', array(
            'value' => $v,
            'formName' => $this->model->getAttributeLabel($this->relation),
            'fieldName' => $this->name,
        ));

    }

}
