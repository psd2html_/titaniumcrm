<?php

class Relation extends CInputWidget
{

    public $model;
    public $relation;
    public $ajaxUrl;
    public $fields;
    // override this for complicated MANY_MANY relations:
    protected $_model;
    protected $_relatedModel;
    private $_relationType;

    public static function retrieveValues($data, $field)
    {
        $returnArray = array();

        foreach ($data as $key => $value) {
            if (strpos($key, 'rel') !== false) {
                if ($value[$field] != "")
                    $returnArray[] = $value[$field];
            }
        }

        return $returnArray;
    }

    // Check if model-value contains '.' and generate -> directives:

    public function run()
    {
        $this->attribute = $this->relation;
        $globalName = 'rel-' . get_class($this->_model) . '_' . $this->relation;

        $rel = $this->relation;
        $data = $this->model->$rel;

        $ng_options = array();
        $ng_relations = array();

        $ff = $this->fields;


        foreach ($data as $r) {
            $ng_relations[] = array('id' => $r->getPrimaryKey(), 'name' => $r->$ff);
        }
        list($this->name, $this->id) = $this::resolveNameID();
        if ($this->ajaxUrl) {
            $this->render('listAjax', array(
                'ng_relations' => $ng_relations,
                'ajax_url' => $this->ajaxUrl,
                'formName' => $this->model->getAttributeLabel($this->relation),
                'globalName' => $globalName,
                'fieldName' => $this->name . '[]',
            ));
        } else {

            $m=$this->getRelatedModel();
            $relatedModels = $m::model()->findAll();

            $ff = $this->fields;

            foreach ($relatedModels as $r) {
                $ng_options[] = array('id' => $r->getPrimaryKey(), 'name' => $r->$ff);
            }
            $this->render('list', array(
                'ng_relations' => $ng_relations,
                'ng_options' => $ng_options,
                'formName' => $this->model->getAttributeLabel($this->relation),
                'globalName' => $globalName,
                'fieldName' => $this->name . '[]',
            ));
        }


    }

    private function getRelatedModel()
    {
        // Instantiate Model and related Model
        foreach ($this->model->relations() as $key => $value) {
            if (strcmp($this->relation, $key) == 0) {
                // $key = Name of the Relation
                // $value[0] = Type of the Relation
                // $value[1] = Related Model
                // $value[2] = Related Field or Many_Many Table

                return new $value[1];
            }
        }
    }


}
