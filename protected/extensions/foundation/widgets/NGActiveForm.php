<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sdd
 * Date: 2/15/14
 * Time: 1:58 PM
 * To change this template use File | Settings | File Templates.
 */
Yii::import("ext.foundation.widgets.*");
class NGActiveForm extends FounActiveForm
{
    public $ngModelNames = array();
    public $ngOptions = array();
    public $defaultNoEntry = "Нет данных";

    public function run()
    {
        // parent::run();
        echo CHtml::closeTag("div");
        Yii::app()->clientScript->registerScript("ng-" . $this->_getNGControllerName(), $this->_generateNGController(), CClientScript::POS_BEGIN);
    }

    private function _getNGControllerName()
    {
        return str_replace(array(":", "-"), "", $this->id) . "ActiveFormController";
    }

    private function _generateNGController()
    {
        $out = "";
        foreach ($this->ngModelNames as $name => $attrs) {
            $out .= '$scope.' . $name . '=' . CJavaScript::encode($attrs) . ';';
        }
        foreach ($this->ngOptions as $name => $attrs) {
            $out .= '$scope.' . $name . '=' . CJavaScript::encode($attrs) . ';';
        }

        $csrf = Yii::app()->request->csrfTokenName;
        $csrfVal = '"' . Yii::app()->request->csrfToken . '"';
        $out .= <<< EOT
\$scope.updateModel = function(data, model, mname, attr) {
console.log(data, model.\$\$pk, mname, attr);
return \$http.post('$this->action', {id: model.\$\$pk, mdl: mname, attr: attr, value: data, $csrf: $csrfVal}).then(
function(data) {

    if (data.data.r>0) {return true;}
    return data.data.e;
});
}
EOT;
        $out = "function " . $this->_getNGControllerName() . '($scope, $http) {' . $out . "}";

        return $out;
    }

    public function init()
    {

        if (!isset($this->htmlOptions['class'])) {
            $this->htmlOptions['class'] = $this->type;
        } else {
            $this->htmlOptions['class'] .= $this->type;
        }

        $this->controller->angularJsDispatcher->injectDependency('xeditable');
        echo CHtml::openTag("div", array("ng-controller" => $this->_getNGControllerName()));
//        ob_start();
//        parent::init();
//        $o= ob_get_clean();;
//       // echo $o;
//        echo (str_replace("<form", "<form editable-form",$o));
    }

    public function uploadField($model, $uploadIdAttribute, $upload, $uploadPopupUrl)
    {
        if ($model->$upload) {
            /** @var $upload Upload */
            $upload=$model->$upload;

            return CHtml::link("<i class='fa fa-download'></i> Скачать ".$upload->name,$upload->getDownloadUrl(),array("class"=>" small button  "))
                 .'&nbsp;'.CHtml::link("<i class='fa fa-pencil'></i> Редактировать",$uploadPopupUrl,array("class"=>"dwDialogTrigger small button secondary"))
                 ;
        } else {
            return CHtml::link("Нет файла",$uploadPopupUrl,array("class"=>"dwDialogTrigger editable-empty editable editable-click"));
        }

    }
    /**
     * Renders a text field for a model attribute.
     * This method is a wrapper of {@link CHtml::activeTextField}.
     * Please check {@link CHtml::activeTextField} for detailed information
     * about the parameters for this method.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes.
     * @return string the generated input field
     */
    public function textField($model, $attribute, $htmlOptions=array(), $additionalFilter=false)
    {
        //<a href="#" editable-text="user.name">{{ user.name || 'empty' }}</a>
        $scopeVariable = $this->_addModelAttributeToScope($model, $attribute);
        $attrName = "\"". get_class($model) . "\", \"" . $attribute . "\"";
        $s=$scopeVariable;
        if ($additionalFilter!==false) {
            $s="(".$scopeVariable." | ".$additionalFilter.")";
        }
        return CHtml::link("{{ " . $s . " || '" . $this->defaultNoEntry . "' }}", "#", array('editable-text' => $scopeVariable,
            'onBeforeSave' => 'updateModel($data,' . $this->_getNGModelName($model) . ' , ' . $attrName . ')'));
        //CHtml::activeTextField($model,$attribute,$htmlOptions);
    }


    public function dateFieldRow($model, $attribute, $htmlOptions = array())
    {
        return $this->labelEx($model,$attribute).' '.$this->dateField($model,$attribute);
    }

    public function textFieldRow($model, $attribute, $htmlOptions=array(), $additionalFilter=false)
    {
        return $this->labelEx($model,$attribute).' '.$this->textField($model,$attribute,$htmlOptions, $additionalFilter);
    }

    public function dateField($model, $attribute, $htmlOptions = array())
    {
        /*
         * <a href="#" editable-bsdate="date" e-datepicker-popup="yyyy-MM-dd">
                        {{ (date | date:"MMMM dd, yyyy") || 'empty' }}
                    </a>
         */
        $this->controller->angularJsDispatcher->injectDependency('ui.bootstrap');
        $scopeVariable = $this->_addModelAttributeToScope($model, $attribute);
        $attrName = "\"". get_class($model) . "\", \"" . $attribute . "\"";

        return CHtml::link("{{ (" . $scopeVariable . " | date:\"MMMM dd, yyyy\") || '" . $this->defaultNoEntry . "' }}", "#", array('editable-bsdate' => $scopeVariable,
            'e-datepicker-popup'=>'yyyy-MM-dd',
            'onBeforeSave' => 'updateModel($data,' . $this->_getNGModelName($model) . ' , ' . $attrName . ')'));
        //CHtml::activeTextField($model,$attribute,$htmlOptions);
    }

    public function textArea($model, $attribute, $htmlOptions = array())
    {
        //<a href="#" editable-text="user.name">{{ user.name || 'empty' }}</a>
        $scopeVariable = $this->_addModelAttributeToScope($model, $attribute);
        $attrName = "\"". get_class($model) . "\", \"" . $attribute . "\"";

        return CHtml::link("<pre>{{ " . $scopeVariable . " || '" . $this->defaultNoEntry . "' }}</pre>", "#", array('editable-textarea' => $scopeVariable,
            'onBeforeSave' => 'updateModel($data,' . $this->_getNGModelName($model) . ' , ' . $attrName . ')'));
        //CHtml::activeTextField($model,$attribute,$htmlOptions);
    }

    public function dropDownList($model,$attribute,$data,$htmlOptions=array())
    {
        // <a href="#" editable-select="status" e-ng-options="key as value for (key, value) in statuses">
        //{{ statuses[status] }}
        //</a>
        $scopeVariable = $this->_addModelAttributeToScope($model, $attribute);
        $scopeOptions = $this->_addOptionsToScope($model, $attribute,$data);
        $attrName = "\"". get_class($model) . "\", \"" . $attribute . "\"";
        $options=array('editable-select' => $scopeVariable,
            'e-ng-options'=>"key as value for (key, value) in ".$scopeOptions,
            'onBeforeSave' => 'updateModel($data,' . $this->_getNGModelName($model) . ' , ' . $attrName . ')');

        if (isset($htmlOptions['emptyOption'])) {
            $options['ng-class']=$scopeVariable."==".$htmlOptions['emptyOption']."?'editable-empty':''";
        }

        return CHtml::link("{{ " . $scopeOptions.'['.$scopeVariable.']' . " || '" . $this->defaultNoEntry . "' }}",
            "#",
            $options);

    }

    private function _addOptionsToScope($model, $attribute, $options)
    {
        $name = $this->_getNGModelOptionsName($model);

        if (!array_key_exists($name, $this->ngOptions)) {
            $this->ngOptions[$name] = array();
        }
        $this->ngOptions[$name][$attribute] = $options;
        return $name . "." . $attribute;
    }

    private function _addModelAttributeToScope($model, $attribute)
    {
        $name = $this->_getNGModelName($model);
        if (!array_key_exists($name, $this->ngModelNames)) {
            $this->ngModelNames[$name] = array('$$pk' => $model->getPrimaryKey());

        }
        $this->ngModelNames[$name][$attribute] = $model->$attribute;
        return $name . "." . $attribute;
    }

    /**
     * @param $model
     * @return string
     */
    private function _getNGModelName($model)
    {
        $name = "ng" . get_class($model);
        return $name;
    }

    private function _getNGModelOptionsName($model)
    {
        $name = "ngOptions" . get_class($model);
        return $name;
    }
}
