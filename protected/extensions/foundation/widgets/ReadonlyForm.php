<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sdd
 * Date: 2/15/14
 * Time: 1:58 PM
 * To change this template use File | Settings | File Templates.
 */
Yii::import("ext.foundation.widgets.*");
class ReadonlyForm extends FounActiveForm
{
    public $ngModelNames = array();
    public $ngOptions = array();
    public $defaultNoEntry = "Нет данных";

    public function run()
    {
        // parent::run();

    }


    public function init()
    {

        if (!isset($this->htmlOptions['class'])) {
            $this->htmlOptions['class'] = $this->type;
        } else {
            $this->htmlOptions['class'] .= $this->type;
        }

    }

    public function uploadField($model, $uploadIdAttribute, $upload, $uploadPopupUrl)
    {
        if ($model->$upload) {
            /** @var $upload Upload */
            $upload=$model->$upload;

            return CHtml::link("<i class='fa fa-download'></i> Скачать ".$upload->name,$upload->getDownloadUrl(),array("class"=>" small button  "));
        } else {
            return "Нет файла";
        }

    }
    /**
     * Renders a text field for a model attribute.
     * This method is a wrapper of {@link CHtml::activeTextField}.
     * Please check {@link CHtml::activeTextField} for detailed information
     * about the parameters for this method.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes.
     * @return string the generated input field
     */
    public function textField($model, $attribute, $htmlOptions=array(), $additionalFilter=false)
    {
        return  $model->$attribute? $model->$attribute: $this->defaultNoEntry;
    }


    public function dateFieldRow($model, $attribute, $htmlOptions = array())
    {
        return $this->labelEx($model,$attribute).' '.$this->dateField($model,$attribute);
    }

    public function textFieldRow($model, $attribute, $htmlOptions=array(), $additionalFilter=false)
    {
        return $this->labelEx($model,$attribute).' '.$this->textField($model,$attribute,$htmlOptions, $additionalFilter);
    }

    public function dateField($model, $attribute, $htmlOptions = array())
    {
        return  $model->$attribute? Yii::app()->dateFormatter->formatDateTime($model->$attribute,"long",Null): $this->defaultNoEntry;
    }

    public function textArea($model, $attribute, $htmlOptions = array())
    {

        return  "<pre>".($model->$attribute? $model->$attribute: $this->defaultNoEntry)."</pre";
    }

    public function dropDownList($model,$attribute,$data,$htmlOptions=array())
    {
        return array_key_exists($model->$attribute,$data)?$data[$model->$attribute]:$model->$attribute;
    }

}
