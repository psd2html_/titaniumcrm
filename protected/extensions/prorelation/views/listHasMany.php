
<div id="<?= $globalName ?>">  

    
    <fieldset>

        <div class="multiselect">
            <ul class="no-bullet">
                <?php
                if ($data) {
                    foreach ($data as $k => $v) {
                        $name = $v->getAttribute(($nameField));

                        if (!$name) {
                            $name = $v->$nameField; //dumb fix for virtual attribute
                        }

                        $this->buildOptionWithUpdate($k, $name, $fieldName, $updateLink);
                    }
                } else {
                    
                }
                ?>
            </ul>
        </div>
    </fieldset>
    <?php
        if ($showCreate) {
        echo ('<div class="indent-bot-15">');
        echo CHtml::link("Создать новый", $newLink, array("class" => "button small"));
        echo('</div>');
        }
    
    ?>

</div>
<?php 
$deleteRoute=$deleteLink[0];
unset($deleteLink[0]);
$deleteLink['id']='SUBSTITUE';
$deleteUrl=Yii::app()->createUrl($deleteRoute,$deleteLink) ;
?>

<script>
   
    $(document).ready(function() {
       
        
        $('#<?= $globalName ?> .autocompleter-button.remove').click(
        function (e) {
            e.preventDefault();
            
            var sid=$(this).attr('sid');
            var deleteUrl="<?= $deleteUrl ?>";
            var r=confirm("Вы действительно хотите удалить эту запись?");
            
            if (r==true)
            {
                //alert(deleteUrl.replace("SUBSTITUE", sid));
                //return;
                $.ajax({
                    type: 'post',
                    url:  deleteUrl.replace("SUBSTITUE", sid),
                    processData: false,
                    //data: xmlDocument
                    //success: handleResponse
                });
                
                var parent=$(this).parent();
                parent.remove();
            }
              
            
    		
            
        });
    });

</script>
