
<div id="<?= $globalName ?>">  

        <?php

        $this->widget('MGridView', array(
            'id' => 'project-events-grid',
            'dataProvider' => $dataProvider,
            'cssFile' => false,
            'template' => '{items} {pager}',
            
            'columns' => array_merge(
                $fields,
                array(array('class' => 'CButtonColumn',
                    'template' => '{update}{delete}',
                    'deleteButtonUrl' => 'Yii::app()->createUrl("'.$deleteRoute.'", array("id" => $data->getPrimaryKey()))',
                    'updateButtonUrl' => 'Yii::app()->createUrl("'.$updateRoute.'", array("id" => $data->getPrimaryKey(),"returnUrl"=>"' . $returnUrl . '"))',
                    'buttons' => array(
                        'delete' => array(
                            'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                            'options' => array('class' => 'button secondary tiny delete')
                        ),
                        'update' => array(
                            'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                            'options' => array('class' => 'button secondary tiny update')
                        ),
                    ),
                ))
            ),
        ));
        ?>

        <div class="indent-bot-15">
            <?php
            echo CHtml::link("Создать новый", $newLink, array("class" => "button small"));
            ?>
        </div>


</div>