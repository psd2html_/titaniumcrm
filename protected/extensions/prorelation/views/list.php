<div id="<?= $globalName ?>">  

    <?php if ($showSelection) { ?>
        <div class="row indent-bot-10">
            <div class="nine columns">
                <?php
                /* $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                  'name' => $acName,
                  'value' => $defaultValue,
                  'source' => Yii::app()->createUrl($autocompleteUrl),
                  // additional javascript options for the autocomplete plugin
                  'options' => array(
                  'size' => 120,
                  'showAnim' => 'fold',
                  ),
                  'htmlOptions' => array('size' => 60)
                  )); */

                echo(CHtml::dropDownList('selector', 0, $options, array('class' => 'selector')));
                ?>
            </div>

            <div class="three columns">
                <?php
                echo CHtml::link("Добавить", '#', array("class" => "add-button button small indent-bot-15"));
                ?> &nbsp;
                <?php
                echo CHtml::link("Создать новый", $newLink, array("class" => "button small"));
                ?>
            </div>
        </div>
    <?php } ?>

    <fieldset>

        <div class="multiselect">
            <ul class="no-bullet">
                <?php
                if ($data) {
                    foreach ($data as $k => $v) {
                        $name = $v->getAttribute(($nameField));

                        if (!$name) {
                            $name = $v->$nameField; //dumb fix for virtual attribute
                        }

                        $this->buildOption($k, $name, $fieldName);
                    }
                } else {
                    
                }
                ?>
            </ul>
        </div>
    </fieldset>
    <?php
    if (!$showSelection) {
        echo ('<div class="indent-bot-15">');
        echo CHtml::link("Создать новый", $newLink, array("class" => "button small"));
        echo('</div>');
    }
    ?>

</div>
<script>
    function remove_func()
    {
        var parent=$(this).parent();
        parent.remove();
    }
    
    function add_func(sid,id,name)
    {
        var existing=$('input[name=<?= $fieldName ?>][value='+sid+']').val();	
        if (existing==sid) {
            alert('Данная запись уже выбрана. Добавление невозможно'); return;
        }
        
        var li='<?php $this->buildOption("'+sid+'", "'+name+'", $fieldName) ?>';
        $(''+id+' .multiselect ul').append(li);
        $(''+id+' .autocompleter-button.remove').unbind().click(remove_func);
        
    }
    $(document).ready(function() {
        $('#<?= $globalName ?> .autocompleter-button.remove').click(remove_func);
        $('#<?= $globalName ?> .add-button').click(
        function (e) {
            e.preventDefault();
            
            var sid=$('#<?= $globalName ?> .selector').val();
            var name=$('#<?= $globalName ?> .selector option:selected').text();
              
            var existing=$('input[name=<?= $fieldName ?>][value='+sid+']').val();	
            if (existing==sid) {
                alert('Данная запись уже выбрана. Добавление невозможно'); return;
            }
        
            var li='<?php $this->buildOption("'+sid+'", "'+name+'", $fieldName) ?>';
            $('#<?= $globalName ?> .multiselect ul').append(li);
            $('#<?= $globalName ?> .autocompleter-button.remove').unbind().click(remove_func);
            
    		
            
        });
    });

</script>
