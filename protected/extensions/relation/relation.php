<?php

class Relation extends CWidget {

    // this Variable holds an instance of the Objec
    
    protected $_model;
    // this Variable holds an instance of the related Object
    protected $_relatedModel;
    // draw the relation of which model?
    public $showSelection = true;
    public $gridViewFields = array();
    public $widgetType = self::CLIST;
    public $dataProvider;
    public $overrideController;
    public $filter = null;
    public $miscOptions = null;

    const CLIST = 0;
    const CLIST_NOSELECT = 2;
    const CGRIDVIEW = 1;
    const BELONGS_TO = 6;
    const CGRIDVIEW_WITHAJAX = 5;

    public $showCreate = true;
    public $showDelete=true;
    
    public $createUrl = null;
    public $foundationModalTrigger = false;
    public $controllerName = null;
    public $actionParams = array();
    public $model;
    // which relation should be rendered?
    public $relation;
    private $_relationType;
    public $field;
    // the Primary Key of the foreign Model
    public $relatedPk;
    // a field or an array of fields that determine which field values
    // should be rendered in the selection
    public $fields;
    // if this is set, the User is able to select no related model
    // if this is set to a string, this string will be presented
    public $allowEmpty = 0;
    // disable this to hide the Add Button
    // set this to a string to set the String to be displayed
    public $showAddButton = true;
    // use this to set the link where the user should return to after
    // clicking the add Button
    public $returnLink;
    
    // how should multiple fields be delimited
    public $delimiter = " | ";
    // style of the selection Widget
    public $style = "dropDownList";
    public $createAction = "create";
    public $deleteAction = "delete";
    public $updateAction = "update";
    public $htmlOptions = array();
    public $parentObjects = 0;
    public $orderParentsBy = 0;
    public $groupParentsBy = 0;
    // override this for complicated MANY_MANY relations:
    public $manyManyTable = '';
    public $manyManyTableLeft = '';
    public $manyManyTableRight = '';

    public function init() {
        if ($this->widgetType == self::CGRIDVIEW_WITHAJAX) {
            if ($this->dataProvider == null) {
                throw new CException(
                Yii::t('yii', 'dataProvider should be present'));
            }
            return;
        }
        if (!is_object($this->model)) {
            if (!$this->_model = new $this->model)
                throw new CException(
                Yii::t('yii', 'Relation widget is not able to instantiate the given Model'));
        }
        else {
            $this->_model = $this->model;
        }

        // Instantiate Model and related Model
        foreach ($this->_model->relations() as $key => $value) {
            if (strcmp($this->relation, $key) == 0) {
                // $key = Name of the Relation
                // $value[0] = Type of the Relation
                // $value[1] = Related Model
                // $value[2] = Related Field or Many_Many Table
                $this->_relationType = $value[0];
                switch ($value[0]) {
                    case 'CBelongsToRelation':
                    case 'CHasOneRelation':

                        $this->_relatedModel = new $value[1];
                        if (!isset($this->field)) {
                            $this->field = $value[2];
                        }
                        break;
                    case 'CHasManyRelation':
                        $this->_relatedModel = new $value[1];
                        if (!isset($this->field)) {
                            $this->field = $value[2];
                        }
                        $this->manyManyTable = '!!!';
                        break;
                    case 'CManyManyRelation':
                        preg_match_all('/^.*\(/', $value[2], $matches);
                        $this->manyManyTable = substr($matches[0][0], 0, strlen($matches[0][0]) - 1);
                        preg_match_all('/\(.*,/', $value[2], $matches);
                        $this->manyManyTableLeft = substr($matches[0][0], 1, strlen($matches[0][0]) - 2);
                        preg_match_all('/,.*\)/', $value[2], $matches);
                        $this->manyManyTableRight = substr($matches[0][0], 2, strlen($matches[0][0]) - 3);

                        $this->_relatedModel = new $value[1];
                        break;
                }
            }
        }

        if (!is_object($this->_relatedModel))
            throw new CException(
            Yii::t('yii', 'Relation widget cannot find the given Relation(' . $this->relation . ')'));

        if (!isset($this->relatedPk) || $this->relatedPk == "") {
            $this->relatedPk = $this->_relatedModel->tableSchema->primaryKey;
        }

        if (!isset($this->fields) || $this->fields == "" || $this->fields == array())
            throw new CException(Yii::t('yii', 'Widget "Relation" has been run without fields Option(string or array)'));
    }

    // Check if model-value contains '.' and generate -> directives:
    public function getModelData($model, $field) {
        if (strstr($field, '.')) {
            $data = explode('.', $field);
            $value = $model->getRelated($data[0])->$data[1];
        }
        else
            $value = $model->$field;

        return $value;
    }

    /**
     * This function fetches all needed data of the related Object and returns them
     * in an array that is prepared for use in ListData.
     */
    public function getRelatedData() {
        /* At first we determine, if we want to display all parent Objects, or
         * if the User supplied an list of Objects */
        if (is_object($this->parentObjects)) { // a single Element
            $parentobjects = array($this->parentObjects);
        } else if (is_array($this->parentObjects)) {  // Only show this elements
            $parentobjects = $this->parentObjects;
        } else {
            // Show all Parent elements
            $parentobjects = CActiveRecord::model(get_class($this->_relatedModel))->findAll();
        }

        if ($this->allowEmpty)
            if (is_string($this->allowEmpty))
                $dataArray[0] = $this->allowEmpty;
            else
                $dataArray[0] = Yii::t('app', 'Не выбрано');

        foreach ($parentobjects as $obj) {
            if (is_string($this->fields)) {
                // Display only 1 field:

                $value = $this->getModelData($obj, $this->fields);
            } else if (is_array($this->fields)) {
                // Display more than 1 field:

                $value = '';
                foreach ($this->fields as $field) {
                    $value .= $this->getModelData($obj, $field) . $this->delimiter;
                }
            }

            if ($this->groupParentsBy != '') {
                $dataArray[$obj->{$this->groupParentsBy}][$obj->{$this->relatedPk}] = $value;
            } else {
                $dataArray[$obj->{$this->relatedPk}] = $value;
            }
        }
        if (!isset($dataArray) || !is_array($dataArray))
            $dataArray = array();

        return $dataArray;
    }

    public function buildOption($key, $value, $field) {
        echo '<li>';
        echo '<a class="autocompleter-button remove button secondary small"><img alt="Удалить" src="' . Yii::app()->getBaseUrl() . '/images/icn_trash.png"></a>&nbsp;';
        echo '<input type="hidden" value="' . $key . '" id="' . $field . '" name="' . $field . '" />';
        echo '<span class="multiselect-label">' . $value . '</span>';
        echo '</li>';
    }

    public function buildOptionWithUpdate($key, $value, $field, $update, $showDelete=true) {

        echo '<li>';
        if ($showDelete) {
            echo '<a href="#" sid="' . $key . '" class="autocompleter-button remove button secondary small"><img alt="Удалить" src="' . Yii::app()->getBaseUrl() . '/images/icn_trash.png"></a>';
        }
        
        $update = array_merge($update, array('id' => $key));

        echo CHtml::link('<img alt="Редактировать" src="' . Yii::app()->getBaseUrl() . '/images/icn_edit.png">', $update, array("class" => "autocompleter-button edit button secondary small"));
        echo ('&nbsp;');

        echo '<input type="hidden" value="' . $key . '" id="' . $field . '" name="' . $field . '" />';
        echo '<span class="multiselect-label">' . $value . '</span>';

        echo '</li>';
    }

    /**
     * Retrieves the Assigned Objects of the MANY_MANY related Table
     */
    public function getAssignedObjects() {
        if (!$this->_model->getPrimaryKey())
            return array();

        //$result = $this->_model->getAttributes(array($this->relation));
        $res = $this->_model->getRelated($this->relation);
        //var_dump($this->_model->getRelated($this->relation));
        //die();
        $out = array();
        if ($res) {
            foreach ($res as $foreignObject) {
                $out[$foreignObject->getPrimaryKey()] = $foreignObject;
            }
        }
        return $out;
        /*
          return array();
          if ($this->_relationType=='CManyManyRelation') {
          $sql = sprintf("select * from %s where %s = %s", $this->manyManyTable, $this->manyManyTableLeft, $this->_model->{$this->_model->tableSchema->primaryKey});

          $result = Yii::app()->db->createCommand($sql)->queryAll();

          foreach ($result as $foreignObject) {
          $id = $foreignObject[$this->manyManyTableRight];
          $objects[$id] = $this->_relatedModel->findByPk($id);
          }

          return isset($objects) ? $objects : array();
          }
          elseif ($this->_relationType=='CHasManyRelation')
          {
          $relatedObjects=$this->_model->getAttributes(array($this->relation));

          }

          return array(); */
    }

    /**
     * Retrieves the not Assigned Objects of the MANY_MANY related Table
     * This is used in the two-pane style view.
     */
    public function getNotAssignedObjects() {
        foreach ($this->getRelatedData() as $key => $value) {
            if (!array_key_exists($key, $this->getAssignedObjects())) {
                $objects[$key] = $this->_relatedModel->findByPk($key);
            }
        }

        return $objects ? $objects : array();
    }

    /**
     * Gets the Values of the given Object or Objects depending on the
     * $this->fields the widget requests
     */
    public function getObjectValues($objects) {
        if (is_array($objects)) {
            foreach ($objects as $object) {
                $attributeValues[$object->primaryKey] = $object->{$this->fields};
            }
        } else if (is_object($objects)) {
            $attributeValues[$object->primaryKey] = $objects->{$this->fields};
        }

        return isset($attributeValues) ? $attributeValues : array();
    }

    /*
     * How will the Listbox of the MANY_MANY Assignment be called? 
     */

    public function getListBoxName($ajax = false) {
        if ($ajax) {
            return sprintf('%s_%s', get_class($this->_model), ($this->relation)
            );
        } else {
            return sprintf('%s[%s]', get_class($this->_model), ($this->relation)
            );
        }
    }

    public function renderBelongsToSelection() {
        if (strcasecmp($this->style, "dropDownList") == 0)
            echo CHtml::ActiveDropDownList($this->_model, $this->field, $this->getRelatedData(), $this->htmlOptions);
        else if (strcasecmp($this->style, "listbox") == 0)
            echo CHtml::ActiveListBox($this->_model, $this->field, $this->getRelatedData(), $this->htmlOptions);
        else if (strcasecmp($this->style, "checkbox") == 0)
            echo CHtml::ActiveCheckBoxList($this->_model, $this->field, $this->getRelatedData(), $this->htmlOptions);
    }

    /*
      public function renderManyManySelection() {
      if (strcasecmp($this->style, 'twopane') == 0)
      $this->renderTwoPaneSelection();
      else if (strcasecmp($this->style, 'checkbox') == 0)
      $this->renderCheckBoxListSelection();
      else if (strcasecmp($this->style, 'dropDownList') == 0)
      $this->renderManyManyDropDownListSelection();
      else
      $this->renderOnePaneSelection();
      }
     */
    /*
     * Renders one dropDownList per selectable related Element.
     * The users can add additional entries with the + and remove entries
     * with the - Button .
     */

    public function renderManyManyDropDownListSelection() {
        if (!$this->actionParams) {
            $this->actionParams = array();
        }
        $globalName = 'rel-' . get_class($this->_model) . '_' . $this->relation;
        if (!$this->controllerName) {
            $this->controllerName = $this->model->tableSchema->name;
        }

        switch ($this->widgetType) {
            case self::BELONGS_TO:

                $this->renderBelongsToSelection();
                break;
            case self::CGRIDVIEW_WITHAJAX:
                //$res = $this->_model->getRelated($this->relation);
                //$pk = $this->_model->tableSchema->primaryKey;
                //$dataProvider = new CArrayDataProvider($rawData = $res, array('keyField' => $pk));

                $this->render('gridView2', array(
                    'globalName' => $globalName,
                    'dataProvider' => $this->dataProvider,
                    'fields' => $this->fields,
                    'filter' => $this->filter,
                    'foundationModalTrigger' => $this->foundationModalTrigger,
                    'miscOptions' => $this->miscOptions,
                    'newLink' => array_merge(
                            array($this->controllerName . "/" . $this->createAction), $this->actionParams),
                    'deleteRoute' =>
                    ($this->controllerName . "/" . $this->deleteAction),
                    'updateRoute' =>
                    ($this->controllerName . "/" . $this->updateAction),
                    'returnUrl' => $this->actionParams['returnUrl'],
                    'showCreate' => $this->showCreate,
                ));
                break;

            case self::CGRIDVIEW:
                $res = $this->_model->getRelated($this->relation);
                $pk = $this->_model->tableSchema->primaryKey;
                $dataProvider = new CArrayDataProvider($rawData = $res, array('keyField' => $pk));

                $this->render('gridView', array(
                    'globalName' => $globalName,
                    'dataProvider' => $dataProvider,
                    'fields' => $this->fields,
                    'foundationModalTrigger' => $this->foundationModalTrigger,
                    'newLink' => array_merge(
                            array($this->_relatedModel->tableSchema->name . "/" . $this->createAction), $this->actionParams),
                    'deleteRoute' =>
                    ($this->_relatedModel->tableSchema->name . "/" . $this->deleteAction),
                    'updateRoute' =>
                    ($this->_relatedModel->tableSchema->name . "/" . $this->updateAction),
                    'returnUrl' => $this->actionParams['returnUrl'],
                    'showCreate' => $this->showCreate,
                ));
                break;


            case self::CLIST:
                $options = CHtml::listData($this->_relatedModel->findAll(), $this->relatedPk, $this->fields);
                $this->render('list', array('globalName' => $globalName,
                    'options' => $options,
                    'fieldName' => $this->getListBoxName() . '[]',
                    'data' => ($this->getAssignedObjects()),
                    'nameField' => $this->fields,
                    'showSelection' => $this->showSelection,
                    'showCreate' => $this->showCreate,
                    'newLink' => $this->createUrl ? $this->createUrl :
                            array_merge(
                                    array($this->_relatedModel->tableSchema->name . "/" . $this->createAction), $this->actionParams
                )));
                break;
            case self::CLIST_NOSELECT:
                $options = CHtml::listData($this->_relatedModel->findAll(), $this->relatedPk, $this->fields);
                $this->render('listHasMany', array(
                    'globalName' => $globalName,
                    'options' => $options,
                    'showCreate' => $this->showCreate,
                    'showDelete'=>$this->showDelete,
                    'fieldName' => $this->getListBoxName() . '[]',
                    'data' => ($this->getAssignedObjects()),
                    'nameField' => $this->fields,
                    'showSelection' => $this->showSelection,
                    
                    'newLink' => array_merge(
                            array($this->controllerName . "/" . $this->createAction), $this->actionParams),
                    'deleteLink' => array_merge(
                            array($this->controllerName . "/" . $this->deleteAction), $this->actionParams),
                    'updateLink' => array_merge(
                            array($this->controllerName . "/" . $this->updateAction), $this->actionParams),
                ));
                break;
        }
    }

    public function isAssigned($id) {
        return in_array($id, array_keys($this->getAssignedObjects()));
    }

    public static function retrieveValues($data, $field) {
        $returnArray = array();

        foreach ($data as $key => $value) {
            if (strpos($key, 'rel') !== false) {
                if ($value[$field] != "")
                    $returnArray[] = $value[$field];
            }
        }

        return $returnArray;
    }

    /*
      public function renderCheckBoxListSelection() {
      $keys = array_keys($this->getAssignedObjects());

      echo CHtml::CheckBoxList($this->getListBoxName(), $keys, $this->getRelatedData(), $this->htmlOptions);
      }

      public function renderOnePaneSelection() {
      $keys = array_keys($this->getAssignedObjects());

      echo CHtml::ListBox($this->getListBoxName(), $keys, $this->getRelatedData(), array('multiple' => 'multiple'));
      }

      public function handleAjaxRequest($_POST) {
      print_r($_POST);
      }

      public function renderTwoPaneSelection() {
      echo CHtml::ListBox($this->getListBoxName(), array(), $this->getObjectValues($this->getAssignedObjects()), array('multiple' => 'multiple'));

      $ajax =
      array(
      'type' => 'POST',
      'data' => array('yeah'),
      'update' => '#' . $this->getListBoxName(true),
      );

      echo CHtml::ajaxSubmitButton('<<', array('assign'), $ajax
      );

      $ajax =
      array(
      'type' => 'POST',
      'update' => '#not_' . $this->getListBoxName(true)
      );

      echo CHtml::ajaxSubmitButton('>>', array('assign', 'revoke' => 1), $ajax); //,
      //$data['revoke']);


      echo CHtml::ListBox('not_' . $this->getListBoxName(), array(), $this->getObjectValues($this->getNotAssignedObjects()), array('multiple' => 'multiple'));
      }
     */

    public function run() {

        $this->renderManyManyDropDownListSelection();

        /* if ($this->manyManyTable != '')
          $this->renderManyManySelection();
          else
          $this->renderBelongsToSelection(); */
    }

}
