
<div id="<?= $globalName ?>">  

    <?php
    //$dataProvider = new CArrayDataProvider($rawData = $model->events, array('keyField' => 'idEvent'));
    //$returnUrl = Yii::app()->createUrl('project/update', array('id' => $model->idProject));
    $fa = '';
    
    if (isset($foundationModalTrigger)&&($foundationModalTrigger)) {
        $fa = 'foundationModalTrigger';
    }
    
    
    $this->widget('MGridView', array(
        'id' => 'project-events-grid',
        'dataProvider' => $dataProvider,
        'cssFile' => false,
        'template' => '{items} {pager}',
        'columns' => array_merge(
                $fields, array(array('class' => 'CButtonColumn',
                'template' => '{update}{delete}',
                'deleteButtonUrl' => 'Yii::app()->createUrl("' . $deleteRoute . '", array("id" => $data->getPrimaryKey()))',
                'updateButtonUrl' => 'Yii::app()->createUrl("' . $updateRoute . '", array("id" => $data->getPrimaryKey(),"returnUrl"=>"' . $returnUrl . '"))',
                'buttons' => array(
                    'delete' => array(
                        'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                        'options' => array('class' => 'button secondary tiny delete')
                    ),
                    'update' => array(
                        'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                        'options' => array('class' => 'button secondary tiny update' . $fa)
                    ),
                ),
            ))
        ),
    ));
    ?>

    <div class="indent-bot-15">
<?php
echo CHtml::link("Создать новый", $newLink, array("class" => "button small ".$fa ));
?>
    </div>


</div>