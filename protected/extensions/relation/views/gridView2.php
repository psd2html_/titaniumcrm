
<div id="<?= $globalName ?>">  

    <?php
     $fa = '';
    
    if (isset($foundationModalTrigger)&&($foundationModalTrigger)) {
        $fa = ' foundationModalTrigger';
    }
    
    //$dataProvider = new CArrayDataProvider($rawData = $model->events, array('keyField' => 'idEvent'));
    //$returnUrl = Yii::app()->createUrl('project/update', array('id' => $model->idProject));
    
    $options = array(
        'id' => $globalName . '-grid',
        'dataProvider' => $dataProvider,
        'cssFile' => false,
        'template' => '{items} {pager}',
        'filter' => $filter,
        'columns' => array_merge(
                $fields, array(array('class' => 'CButtonColumn',
                'template' => '{update}{delete}',
                'deleteButtonUrl' => 'Yii::app()->createUrl("' . $deleteRoute . '", array("id" => $data->getPrimaryKey()))',
                'updateButtonUrl' => 'Yii::app()->createUrl("' . $updateRoute . '", array("id" => $data->getPrimaryKey(),"returnUrl"=>"' . $returnUrl . '"))',
                'buttons' => array(
                    'delete' => array(
                        'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_trash.png',
                        'options' => array('class' => 'button secondary tiny delete')
                    ),
                    'update' => array(
                        'imageUrl' => Yii::app()->getBaseUrl() . '/images/icn_edit.png',
                        'options' => array('class' => 'button secondary tiny update'. $fa)
                    ),
                ),
                ))
        ),
    );
    if ($miscOptions) {
        $options = $options + $miscOptions;
    }
    //miscOptions
    $this->widget('MGridView', $options);
    ?>
    <?php if ($showCreate) { ?>
        <div class="indent-bot-15">
            <?php
            echo CHtml::link("Создать новый", $newLink, array("class" => "button small secondary ".$fa ));
            ?>
        </div>
    <?php } ?>

</div>