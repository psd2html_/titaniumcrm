<?php
/**
 * SplitEdit
*
* @version 0.1
* @author SD
* @link http://darkwater.ru
*/
class Autocompleter extends CWidget {

	/**
	 * Html ID
	 * @var string
	 */
        public $lockManufacturer=0;
        public $showAmount=false;
	public $verifyUrl='performance/doesPersonExist';
	public $autocompleteUrl='performance/actorAjax';
	public $createUrl=null;
	public $hiddenInput='#Person';
	public $defaultValue="";
	public $id="autocompleter";
	public $allowCreate=true;
	public $acName="Autocomplete";
	public $multiselect=false,$forbidCreation=false;
	public $multiselect_input="data[]";
	public $data=array();
	public $wouldCreateNew="Такой персоналии в базе нет. Она будет создана, но проверьте на опечатки.", $found="Персоналия успешно загружена.", $badFormat="Требуемый формат: Фамилия Имя", $promt="Введите первые буквы фамилии";
	
	public function init()
	{
		// this method is called by CController::beginWidget()
	}

	public function run()
	{
		if (!$this->createUrl) {$this->allowCreate=false;}
	//	if (!$this->multiselect) {
	//		$this->allowCreate=true;
	//	}
		
			$this->render('autocompleter', array(
                                                'lockManufacturer'=>$this->lockManufacturer,
						'verifyUrl'=>$this->verifyUrl,
						'autocompleteUrl'=>$this->autocompleteUrl,
						'createUrl'=>$this->createUrl,
						'allowCreate'=>$this->allowCreate,
						'hiddenInput'=>$this->hiddenInput,
						'defaultValue'=>$this->defaultValue,
						'id'=>$this->id,
						'acName'=>$this->acName,
						'multiselect'=>$this->multiselect,
						'multiselect_input'=>$this->multiselect_input,
						'data'=>$this->data,
						'wouldCreateNew'=>$this->wouldCreateNew,
						'found'=>$this->found,
						'badFormat'=>$this->badFormat,
						'promt'=>$this->promt,
						'fc'=>$this->forbidCreation,
                                                'showAmount'=>$this->showAmount,
					));
	}
}
?>