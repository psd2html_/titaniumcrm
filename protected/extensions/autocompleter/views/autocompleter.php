
<div id="<?php echo $id; ?>">

    <?php
    $empty = '<i>Элементов нет</i>';

    if (!$multiselect) {
        $multiselect_text = 'false';
    } else {
        $multiselect_text = 'true';
    };
    if (!$allowCreate) {
        $allowCreate_text = 'false';
    } else {
        $allowCreate_text = 'true';
    };


    if ($multiselect) {
        echo CHtml::hiddenField("multiselect_" . $id, "-1");
        ?>

        <?php
    }
    ?>
    <fieldset>
        <legend>Добавить позицию</legend>

        <div class="row indent-bot-10">
            <div class="four columns">
                <label>Артикул</label>
                <?php
                $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                    'name' => $acName,
                    'value' => $defaultValue,
                    'source' => Yii::app()->createUrl($autocompleteUrl),
                    // additional javascript options for the autocomplete plugin
                    'options' => array(
                        'size' => 120,
                        'showAnim' => 'fold',
                    ),
                    'htmlOptions' => array('size' => 60)
                ));
                ?>
            </div>
            <div class="four columns">
                <label>Название</label>
                <input type="text" id="multiselect2_<?= $id ?>" value=""/>
            </div>
            <div class="four columns">
                <div id="man_choose">
                    <label>Производитель</label>    
                    <?php
                    $mList[] = "Выберите производителя";
                    $mList = $mList + CHtml::listData(Manufacturer::model()->findAll(), "idManufacturer", "name");
                    echo CHtml::dropDownList('manufacturer_selector', $lockManufacturer, $mList);
                    ?>
                </div>
            </div>
        </div>
        <div class="indent-bot-15">

            <?php
            echo CHtml::button("Добавить", array("class" => "autocompleter-add button small indent-bot-15", "disabled" => "false"));
            ?>
            &nbsp;&nbsp;&nbsp;
            <span class="comment"><?php echo $promt; ?></span> 
        </div>
    </fieldset>

    <fieldset>
        <legend>Выбранные позиции</legend>
        <div class="multiselect">
            <ul class="no-bullet">
                <?php
                if ($data) {
                    foreach ($data as $k) {
                        echo '<li>';
                        echo '<a class="autocompleter-button remove button secondary small"><img alt="Удалить" src="' . Yii::app()->getBaseUrl() . '/images/icn_trash.png"></a>&nbsp;';
                        echo CHtml::hiddenField($multiselect_input, $k->idFeature);
                        echo '<span class="multiselect-label">' . $k->sid . '</span>' . ' - ' . $k->name . ' (' . $mList[$k->idManufacturer] . ')';
                        if ($showAmount) {
                            echo('; Количество:' . CHtml::textField("Amount[" . $k->idFeature . "]", $k->amount, array('class' => 'input-inline')));
                        }
                        echo '</li>';
                    }
                } else {
                    
                }
                ?>
            </ul>
        </div>
        <?php
        echo CHtml::button("Cортировать", array("class" => "autocompleter-sort button small secondary indent-bot-15"));
        ?>
        <br/> <br/>
    </fieldset>
    <br/>

</div>
<?php
Yii::app()->getClientScript()->registerCoreScript('jquery.ui');
//Yii:app()->clientScript->registerCoreScript
?>
<script>
    var DISABLED_MODE = 0;
    var CREATION_MODE = 1;
    var INSERTION_MODE = 2;

    var sort = false;
    var mode = DISABLED_MODE;

    function toggle_sort()
    {
        sort = !sort;
        if (sort)
        {
            $('.autocompleter-sort').val('Сохранить порядок');

            $('.multiselect ul li').append('<span class="sortable-tag"><img src="<?= Yii::app()->getBaseUrl() ?>/images/icon_sort.gif" /></span>');
            $('.multiselect ul').sortable({handle: '.sortable-tag'});
            //                $('.sortable-tag').disableSelection();
            $('.multiselect ul').sortable('enable');
        }
        else
        {
            $('.autocompleter-sort').val('Сортировать');
            $('.multiselect ul').sortable('disable');
            $('.sortable-tag').remove();
        }
    }

    function remove_func()
    {
        var parent = $(this).parent();
        parent.remove();
    }

    function add_func(sid, id, name, manufacturer)
    {
        var existing = $('input[name="<?= $multiselect_input ?>"][value=' + id + ']').val();


        if (existing == id) {
            alert('Данная запись уже выбрана. Добавление невозможно');
            return;
        }

        var li = '<li><a class="autocompleter-button remove button secondary small"><img alt="Удалить" src="<?= Yii::app()->getBaseUrl() ?>/images/icn_trash.png"></a>';
        li = li + '<input type=\"hidden\" name=\"<?= $multiselect_input ?>\" value=\"' + id + '\">&nbsp;';
        li = li + '<span class=\"multiselect-label\">' + sid + '</span> - ' + name + ' (' + manufacturer + ")" ;
        
        <?php if ($showAmount) { ?>
            li = li + '; Количество: <input id="Amount_'+id+'" class="input-inline" type="text" name="Amount[' + id +']" value="1">';
        <?php } ?>
            
        li=li+ '</li>';
        $('#<?= $id ?> .multiselect ul').append(li);
        $('#<?= $id ?> .autocompleter-button.remove').unbind().click(remove_func);
    }

    $(document).ready(function() {
        $('#<?= $id ?> .autocompleter-button.remove').click(remove_func);
        $('#<?= $id ?> .autocompleter-sort').click(toggle_sort);
        $('#man_choose').hide();
        //$('.multiselect ul').sortable();
        //$('.multiselect ul').disableSelection();
        $('#<?= $id ?> .autocompleter-add').click(
        function() {
            var sid = $('#<?= $id ?> #<?= $acName ?>').val();
            var name = $('#<?= $id ?> #multiselect2_<?= $id ?>').val();
            var manufacturer = parseInt($('#manufacturer_selector').val());

            if (mode == CREATION_MODE) {
                if (isNaN(manufacturer) || manufacturer == 0) {
                    alert('Пожалуйста, выберите производителя');
                    return;
                }
            }
            if (isNaN(manufacturer))
            {
                manufacturer = 0;
            }

            $.ajax({url: '<?= Yii::app()->createUrl($createUrl) ?>',
                type: 'POST',
                data: {sid: sid,
                    '<?= Yii::app()->request->csrfTokenName ?>': '<?= Yii::app()->request->csrfToken ?>',
                    name: name,
                    manufacturer: manufacturer
                },
                success: function(data) {
                    var val = $('#<?= $id ?> #<?= $acName ?>').val();
                    console.log(data);
                    add_func(data.sid, data.id, data.name, data.manufacturer);
                }});

            return;
        }

    );
        /**
         * Comment
         */
        function setMode(_mode) {
            mode = _mode;
            if (_mode==INSERTION_MODE) {
                $('#man_choose').hide();
                $('#<?= $id ?> .comment').html('<?= $found ?>');
                $('#<?= $id ?> .autocompleter-notice').removeClass('search bad found new');
                $('#<?= $id ?> .autocompleter-notice').addClass('found');
                
                if (<?= $multiselect_text ?>) {
                    $('#<?= $id ?> .autocompleter-add').removeAttr('disabled');
                }
                $('#<?= $id ?> #multiselect2_<?= $id ?>').attr('disabled', 'disabled');
                $('#<?= $id ?> .autocompleter-add').val('Добавить');
                
            }
            else if (_mode==CREATION_MODE)
            {
                $('#man_choose').show();
                $('#<?= $id ?> .autocompleter-add').val('Создать');
                $('#<?= $id ?> .comment').html('<?= $wouldCreateNew ?>');
                $('#<?= $id ?> .autocompleter-notice').addClass('new');
                $('#<?= $id ?> #multiselect2_<?= $id ?>').removeAttr('disabled');
                if (!<?= $allowCreate_text ?>) {
                    $('#<?= $id ?> .autocompleter-add').attr('disabled', 'disabled');
                }
                else {
                    $('#<?= $id ?> .autocompleter-add').removeAttr('disabled');
                }
            }
        }
        $('#<?= $acName ?>').each(function() {
            //$(this).unbind();
            //$(this).off();

            var autoCompelteElement = this;
            var s_id = -1;
            $(this).keyup(function(event) {
                if ((event.keyCode == 32) || (event.keyCode == 8) || (event.keyCode > 48))
                {

                    $.ajax({url: '<?= Yii::app()->createUrl($verifyUrl) ?>',
                        type: 'POST',
                        data: {name: $(this).val(),
                            '<?= Yii::app()->request->csrfTokenName ?>': '<?= Yii::app()->request->csrfToken ?>'
                        },
                        success: function(data) {
                            if (data.responce == 1) {
                                $('#man_choose').hide();
                                
                                $('#<?= $id ?> #multiselect2_<?= $id ?>').val(data.name);
                                setMode(INSERTION_MODE);
                            }
                            else
                            {
                                setMode(CREATION_MODE);
                            }
                        }
                    });
                }
            });

            $(this).autocomplete({
                select: function(event, ui) {
                    setMode(INSERTION_MODE);
                    
                    var selectedObj = ui.item;
                    
                    $(autoCompelteElement).val(selectedObj.label);
                    $('#<?= $id ?> #multiselect2_<?= $id ?>').val(selectedObj.feature);
                    $(this).val(selectedObj.value);
                    s_id = selectedObj.value;
                    
                    return false;
                }

            });
        });
    });

</script>

