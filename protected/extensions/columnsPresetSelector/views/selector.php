<div class="right">

<?php
foreach ($presets as $id => $preset) {
    echo CHtml::link(CHtml::encode($preset['n']),'#'
            , array('name' => 'selectPreset', 'onClick' => ($id == $currentPreset) ? 'return false;' : 'window.mGridView.switchPreset("' . $gid . '", "' . $id . '"); return false;',
                'class' => ($id == $currentPreset) ? 'label' : 'label secondary')).' ' ;
}
?>

</div>
