<?php

class columnsPresetSelector extends CWidget {

    public $model;
    public $relation;
    public $visiblePresetCount=false;

    public function init()
    {

    }

    public function run()
    {
        $gid = strtolower(get_class($this->model));
        $presets = CustomizableColumnsProvider::getPresets($gid);

        list($currentPreset, $presetName) = CustomizableColumnsProvider::getCurrentPreset($gid);

        if (!empty($presets)) {
            if ($this->visiblePresetCount !== false) {
                $presets = array_slice($presets, 0, $this->visiblePresetCount);
            }
        }

        if ($presets) {
            $this->render("selector", array('presets'=>$presets, 'gid'=>$gid, 'currentPreset'=>$currentPreset));
        }
    }

}
