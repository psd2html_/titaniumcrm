<?php

// $doResize=true == aka fit in the box;

class hasImage extends CActiveRecordBehavior
{

    public $modelAttribute = "attached_image";
    public $file_prefix = "avatar";
    public $thumbnail_width = 150;
    public $thumbnail_height = 1000;
    public $crop_width = null;
    public $crop_height = null;
    public $no_image = 'no_image';
    public $thumbnail_width_array = array('' => '150');
    public $thumbnail_height_array = array('' => '1000');
    public $doResize_array = array('' => true);
    public $doResize = true;
    public $no_image_array = array('' => 'no_image.png');
    public $borrowFromGallery = false;

    public function afterSave($event)
    {


        if ($this->getOwner()->getAttribute($this->modelAttribute)) {

            $this->useLoadedFile($this->getOwner()->getAttribute($this->modelAttribute)->getTempName());

        }
        parent::afterSave($event);
    }

    public function useLoadedFile($path0, $id = null)
    {
        if (!$id) {
            $id = $this->getOwner()->getPrimaryKey();
        }

        $path = $this->route(false, '', $id);

        Yii::app()->ih
            ->load($path0)
            ->save($path, 2, 90);

        $this->buildThumbnail($id);
        return true;
    }

    private function route($isThumbnail, $type = '', $id = null, $relative = false)
    {

        if (!$id) {
            $id = $this->getOwner()->getPrimaryKey();
        }

        $root = Yii::app()->getBasePath() . '/../';
        $prefix = $this->file_prefix;

        if (!$isThumbnail) {
            $type = '_o';
        } else {
            if (!$type) {
                $type = '_t';
            }
        }

        // uploads/prefix/_o
        // uploads/prefix/_t
        // uploads/prefix/$type

        if (!is_dir($root . '/uploads')) {
            mkdir($root . '/uploads');
        }
        if (!is_dir($root . "/uploads/$prefix")) {
            mkdir($root . "/uploads/$prefix");
        }
        if (!is_dir($root . "/uploads/$prefix/$type")) {
            mkdir($root . "/uploads/$prefix/$type");
        }

        $new = "/uploads/$prefix/$type/$id.jpg";

        if (!$relative) {
            return $root . $new;
        } else {
            return $new;
        }
    }

    public function buildThumbnail($id = -1, $t = '')
    {
        if ($id == -1) {
            $id = $this->getOwner()->getPrimaryKey();
        }

        $o_path = $this->route(false, '', $id);
        $t_path = $this->route(true, $t, $id);

        $width = $this->thumbnail_width;
        $height = $this->thumbnail_height;
        $doResize = $this->doResize;

        if (($t != '') && (isset($this->doResize_array[$t]))) {
            $width = $this->thumbnail_width_array[$t];
            $height = $this->thumbnail_height_array[$t];
            $doResize = $this->doResize_array[$t];
        }
        if ($doResize) {
            Yii::app()->ih
                ->load($o_path)
                ->resize($width, $height)
                ->save($t_path, 2, 90);
        } else {
            Yii::app()->ih
                ->load($o_path)
                ->adaptiveThumb($width, $height)
                ->save($t_path);
        }
    }

    public function getcrop_width()
    {
        return $this->crop_width;
    }

    public function deleteImage($id = -1, $t = '')
    {
        if ($id == -1) {
            $id = $this->getOwner()->getPrimaryKey();
        }
        $file = $this->route(false);
        $file2 = $this->route(true);
        if (file_exists($file)
        ) {
            unlink($file);
        }
        if (file_exists($file2)
        ) {
            unlink($file2);
        }
    }

    public function saveImage($image)
    {
        if ($image) {
            $file = $this->route(false);

            if (!$image->saveAs($file)) {
                throw new CHttpException(400, 'Только не это дерьмо опять.');
            } else {
                $this->buildThumbnail();
            }
        }
    }

    public function saveFromStream($id)
    {
        $or_f = Yii::app()->params['uploadsDir'];
        $input = fopen("php://input", "r");
        $tf = tempnam($or_f . '/temp', 'str');
        $temp = fopen($tf, "w");
        $realSize = stream_copy_to_stream($input, $temp);
        fclose($input);
        fclose($temp);

        if ($realSize != $_SERVER["CONTENT_LENGTH"]) {
            return false;
        }

        $path = $this->route(false, '', $id);
        Yii::app()->ih
            ->load($tf)
            ->save($path, 2, 90);

        $this->buildThumbnail($id);

        unlink($tf);

        return true;
    }

    public function getRawThumbnail($force = false, $nullInsteadOfDefault = false, $t = '')
    {
        $id = $this->getOwner()->getPrimaryKey();

        if (!$this->hasThumbnail($t) || ($force)) {
            $o_path = $this->route(false);

            if (file_exists($o_path)) {
                $this->buildThumbnail($id, $t);
            } else {
                if ($nullInsteadOfDefault) {
                    return null;
                } else {

                    if (!$t) {
                        return Yii::app()->getBasePath() . '/../images/' . $this->no_image;
                    } else
                        return Yii::app()->getBasePath() . '/../images/' . $this->no_image_array[$t];
                }
            }
        }
        $t_path = $this->route(true, $t, $id, false);

        return $t_path;
    }

    public function hasThumbnail($t = '')
    {
        $path = $this->route(true, $t);
        return file_exists($path);
    }

    public function getThumbnail($force = false, $nullInsteadOfDefault = false, $t = '')
    {
        $id = $this->getOwner()->getPrimaryKey();

        if (!$this->hasThumbnail($t) || ($force)) {
            $o_path = $this->route(false);

            if (file_exists($o_path)) {
                $this->buildThumbnail($id, $t);
            } else {
                if ($nullInsteadOfDefault) {
                    return null;
                } else {

                    if (!$t) {
                        return Yii::app()->request->baseUrl . '/images/' . $this->no_image;
                    } else
                        return Yii::app()->request->baseUrl . '/images/' . $this->no_image_array[$t];
                }
            }
        }
        $t_path = $this->route(true, $t, $id, true);
        return $t_path;
    }

    public function getRawImage($id = null)
    {
        $t_path = $this->route(false, null, $id, false);

        if (file_exists($t_path))
            return $t_path;

        return null;
    }

    public function getImage($id = null)
    {
        $o_path = $this->route(false, null, $id, true);
        $t_path = $this->route(false, null, $id, false);

        if (file_exists($t_path))
            return $o_path;

        return null;
    }

    public function hasBigImage()
    {
        $o_path = $this->route(false);

        return (file_exists($o_path));

    }

    public function cropThumbnail($x = 0, $y = 0, $w = 300, $h = 300, $w2 = 300, $h2 = 300, $t = '', $id = null)
    {
        //var_dump($x,$y,$w,$h,$w2,$h2);

        if (!$id) {
            $id = $this->getOwner()->getPrimaryKey();
        }

        $o_path = $this->route(false, '', $id);
        $t_path = $this->route(true, $t, $id);

        if ($this->doResize) {
            $width = $this->thumbnail_width;
            $height = $this->thumbnail_height;
            Yii::app()->ih
                ->load($o_path)
                ->crop((int)$w, (int)$h, (int)$x, (int)$y)
                ->resize($width, $height)
                ->save($t_path, 2, 90);
        } else {
            Yii::app()->ih
                ->load($o_path)
                ->crop((int)$w, (int)$h, (int)$x, (int)$y)
                ->resize($w2, $h2)
                ->save($t_path, 2, 90);
        }
    }

}

?>