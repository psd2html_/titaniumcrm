<?php
/**
 * SplitEdit
*
* @version 0.1
* @author SD
* @link http://darkwater.ru
*/
class gallery extends CWidget {

	/**
	 * Html ID
	 * @var string
	 */

	public $subject=null;
        public $subjectType=null;
        
        public $label='Галерея';
	public $saveAction='ajax/storeGalleryImage';
        public $deleteAction='ajax/deleteGalleryImage';
        public $updateAction='ajax/updateGalleryImage';
        
        public function init()
	{
		// this method is called by CController::beginWidget()
	}

	public function run()
	{
            if(($this->subject)&&($this->subjectType)) {
                
                $models=  GalleryImage::model()->findAll('subject=:s and subjectType=:st',array('s'=>$this->subject,'st'=>$this->subjectType));
                
			$this->render('_gallery', array(
                                        'models'=>$models,
                                        'subject'=>$this->subject,
                                        'subjectType'=>$this->subjectType,
                                        'label'=>$this->label,
                                        'saveAction'=>$this->saveAction,
                                        'deleteAction'=>$this->deleteAction,
                                        'updateAction'=>$this->updateAction,
					));
            }
            else 
            {
                echo('Model is NULL');
            }
	}
}
?>
