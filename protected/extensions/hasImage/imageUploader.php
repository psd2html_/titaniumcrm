<?php
/**
 * SplitEdit
 *
 * @version 0.1
 * @author SD
 * @link http://darkwater.ru
 */
class ImageUploader extends CWidget
{

    /**
     * Html ID
     * @var string
     */

    public $model = null;
    public $allowCrop = false;
    public $showImage = true;
    public $label = 'Картинка';
    public $saveAction = 'ajax/storeImage';
    public $showCropAction = 'ajax/showCropAction';
    public $cropClass = '';
    public $layout = '';

    public function init()
    {
        // this method is called by CController::beginWidget()
    }

    public function run()
    {
        if ($this->model) {
            $this->render('form', array(
                'allowCrop' => $this->allowCrop,
                'model' => $this->model,
                'storeUrl' => $this->saveAction,
                'showCropUrl' => $this->showCropAction,
                'cropClass' => $this->cropClass,
                'label' => $this->label,
            ));
        } else {
            echo('Model is NULL');
        }
    }
}

?>