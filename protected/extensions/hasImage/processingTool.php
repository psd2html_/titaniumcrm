<?php
/**
 * SplitEdit
 *
 * @version 0.1
 * @author SD
 * @link http://darkwater.ru
 */

class processingTool extends CWidget
{
    public $model = null;
    public $cropAction = '/imgProcess/apply';
    public $uploadAction = '/imgProcess/upload';
    public $deleteAction = '/imgProcess/delete';

    public function init()
    {
    }

    public function run()
    {
        $t = $this->model;
        if ($t->isNewRecord) {return;}

        if (strtolower(get_class($this->model)) == "feed") {
            $minW = $t->getWidth();
            $minH = $t->getHeight();
        } else {
            $minW = $t->img->crop_width;
            $minH = $t->img->crop_height;
        }

        $url = $t->getImage();
        $thurl = $t->getThumbnail(false, true);

        return $this->render('_img', array(
            'cropAction'=>$this->cropAction,
            'uploadAction'=>$this->uploadAction,
            'deleteAction'=>$this->deleteAction,
            'url' => $url, 'thurl' => $thurl,
            'minW' => $minW, 'minH' => $minH,
            'id' => $t->getPrimaryKey(),
            'modelToken' => $t->img->file_prefix), false, true);
    }
}

?>
