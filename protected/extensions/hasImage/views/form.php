<?php
$u = Yii::app()->createUrl($showCropUrl, array('id' => $model->getPrimaryKey(), 'prefix' => $model->img->file_prefix, 'cropClass' => $cropClass));
$img = $model->getThumbnail(false, true);
$url = $model->getImage();
$minW = $model->img->crop_width;
$minH = $model->img->crop_height;
$baseUrl = Yii::app()->baseUrl;
$mId = $model->getPrimaryKey();
?>


<label><?= $label ?></label>


<script>
        
</script>
<div class="img_place">
    <?php
    if ($img) {
        echo CHtml::image($img . "?v=" . rand(0, 5000), 'Картинка', array('class' => 'img-box img-indent-bot'));
    }
    ?>
</div>
<div class="indent-bot">

    <?php
    Yii::app()->clientScript->registerScript('dialogBoxes', "
            function openDialog() {
                    $('#jobDialog').dialog('open'); console.log('open'); return;
            };
            function closeDialog() {
                    $('#jobDialog').dialog('close'); console.log('close'); return;
            };
            function refreshThumbnail() {
                    console.log('rt');
                    var rnd=Math.floor(Math.random()*10000);
                    $('.img_place').html('<img class=\"img-box img-indent-bot\" alt=\"Картинка\" src=\"http://uploads.chekhoved.net/uploads/{$model->img->file_prefix}/_t/" . $mId . ".jpg?v='+rnd+'\">');
                    
            };
            ", CClientScript::POS_HEAD);

    if ($allowCrop) {
        $dc = "$.ajax({url:'$u',
               success: function(msg) { $('#edit_preview').html(msg); console.log('open'); $('#jobDialog').dialog('open'); }});";
    } else {
        $dc = "";
    }
    if ($allowCrop) {
        echo CHtml::ajaxButton("Обрезать", Yii::app()->createUrl($showCropUrl, array('id' => $model->getPrimaryKey(), 'prefix' => $model->img->file_prefix, 'cropClass' => $cropClass)), array(
            'success' => "function(msg) { $('#edit_preview').html(msg); console.log('enter'); openDialog(); }",
                ), array(
            'class' => 'small button indent-bot-10',
            'id' => 'open-crop-Dialog-' . rand(0, 5000)));

        $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
            'id' => 'jobDialog',
            'options' => array(
                'title' => Yii::t('job', 'Обрезать'),
                'autoOpen' => !true,
                'modal' => 'true',
                'width' => 'auto',
                'height' => 'auto',
            ),
        ));
        echo ("<div id='edit_preview'></div>");
        $this->endWidget('zii.widgets.jui.CJuiDialog');
    }

    $this->widget('ext.EAjaxUpload.EAjaxUpload', array(
        'id' => 'uploadFile',
        'config' => array(
            'action' => Yii::app()->createUrl($storeUrl, array('id' => $model->getPrimaryKey(), 'prefix' => $model->img->file_prefix)),
            'allowedExtensions' => array("jpg", "jpeg", "gif", "png"), //array("jpg","jpeg","gif","exe","mov" and etc...
            'sizeLimit' => 10 * 1024 * 1024, // maximum file size in bytes
            'maxSizeLimit' => 10 * 1024 * 1024, // minimum file size in bytes
            'onComplete' => "js:function(id, fileName, responseJSON,html){ 
                                 $('.img_place').html();
                                 refreshThumbnail();
                                 $dc
                                }",
        )
    ));
    ?>

</div>

