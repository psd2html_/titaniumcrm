<?php
Yii::app()->clientScript->registerCssFile('/css/jquery.Jcrop.min.css');
Yii::app()->clientScript->registerScriptFile('/js/jquery.Jcrop.min.js');
Yii::app()->clientScript->registerScriptFile('/js/angular/angular.imgCropped.js', CClientScript::POS_BEGIN);
$this->controller->angularJsDispatcher->injectDependency("dw.imgCropped");

?>

<script>
    //this.myApp = angular.module('uploaderApp', []);

    // -------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    <!-- thurl; url; state; progress; progressVisible; -->

    function CropController($scope, $http) {

        $scope.crop = [];
        $scope.state = 1;
        $scope.files = [];
        $scope.progressVisible = false;
        $scope.progress = 0;
        $scope.send = false;

        var xhr;

        $scope.setState = function (f) {
            $scope.state = f;
        }

        $scope.doSubmit = function () {

            if ($scope.setCrop) {
                alert('Для того, чтобы обрезать картинку, выделите желаемую область мышью.');
                return;
            }

            $scope.send = true;
            $http.post($scope.cropAction,
                {
                    "<?= Yii::app()->request->csrfTokenName?>": "<?= Yii::app()->request->csrfToken ?>",
                    c: $scope.crop,
                    i: $scope.id,
                    m: $scope.mt
                }).then(function (data) {
                    if (data.data.r > 0) {
                        $scope.thurl = data.data.thurl + '?_t=' + Math.floor((Math.random() * 10000) + 1);
                    }
                    $scope.send = false;
                    $scope.state = 1;
                },
                function (data) {
                    $scope.send = false;
                    alert('Не удалось');
                })
        }

        $scope.uploadFile = function () {
            var fd = new FormData()
            for (var i in $scope.files) {
                fd.append("image", $scope.files[i])
            }
            fd.append("<?= Yii::app()->request->csrfTokenName ?>", "<?= Yii::app()->request->csrfToken ?>");
            fd.append("i", $scope.id);
            fd.append("m", $scope.mt);

            xhr = new XMLHttpRequest()
            xhr.upload.addEventListener("progress", uploadProgress, false)
            xhr.addEventListener("load", uploadComplete, false)
            xhr.open("POST", $scope.uploadAction)
            $scope.progressVisible = true
            xhr.send(fd)
        }

        function uploadComplete(evt) {
            $scope.$apply(function () {
                $scope.progressVisible = false;
                if (xhr.status == 200) {
                    var result = JSON.parse(xhr.response);
                    if (result.r > 0) {
                        $scope.url = result.url + '?_t=' + Math.floor((Math.random() * 10000) + 1);
                        $scope.thurl = result.thurl + '?_t=' + Math.floor((Math.random() * 10000) + 1);
                        $scope.state = 2;
                    } else {
                        alert("Не удалось загрузить. " + result.m);
                    }
                }
            });
        }

        $scope.deleteImage = function () {
            $http.post($scope.deleteAction, {
                '<?= Yii::app()->request->csrfTokenName ?>': '<?= Yii::app()->request->csrfToken ?>',
                i: $scope.id,
                m: $scope.mt
            }).then(
                function (data) {
                    if (data.data.r <= 0) {
                        alert(data.data.m);
                        return;
                    }
                    $scope.url = null;
                    $scope.thurl = null;
                    $scope.state = 1;
                },
                function (err) {
                    console.log(err);
                }
            );
        }

        $scope.setFiles = function (element) {
            $scope.$apply(function () {
                console.log('files:', element.files);
                // Turn the FileList object into an Array
                $scope.files = []
                for (var i = 0; i < element.files.length; i++) {
                    $scope.files.push(element.files[i])
                }
                $scope.progressVisible = false
            });
        };

        function uploadProgress(evt) {
            $scope.$apply(function () {
                if (evt.lengthComputable) {
                    $scope.progress = Math.round(evt.loaded * 100 / evt.total);
                } else {
                    $scope.progress = 'unable to compute'
                }
            })
        }

        $scope.selected = function (x) {
            if(!$scope.$$phase) {
            $scope.$apply(function () {
                $scope.crop = x;
                $scope.setCrop = false;
            });
            }
        };
    }
</script>

<div ng-controller="CropController"
     ng-init="cropAction='<?= $cropAction ?>'; deleteAction='<?= $deleteAction ?>'; uploadAction='<?= $uploadAction ?>'; thurl='<?= $thurl ?>'; url='<?= $url ?>'; mt=<?= CJavaScript::encode($modelToken) ?>; id=<?= $id ?>; ">
    <dl class="tabs pill">
        <dd ng-click="setState(1);" ng-class="state==1?'active':''"><a href="#pillTab1">Картинка</a></dd>
        <dd ng-show="url" ng-click="setState(2);" ng-class="state==2?'active':''"><a href="#pillTab2">Обработка</a>
        </dd>
    </dl>
    <div ng-show="state==1">
        <a href="#" class="th indent-bot-15" ng-show="thurl">
            <img ng-src="{{ thurl }}"/>
        </a>

        <form class="indent-bot-15">
            <label ng-show="progressVisible"> Загрузка {{progress}}%...</label>
            <input type="file" name="image" ng-model="photo" accept="image/*"
                   onchange="angular.element(this).scope().setFiles(this)"/> <br/><br/>

            <div class="footer" ng-show="!send">

                <input type="button" value="Загрузить" ng-disabled="files.length==0" class="button"
                       ng-click="uploadFile()"/>

                <input type="button" class="button alert" ng-show="url" ng-click="deleteImage();"
                       value="Удалить фото"/>

            </div>
        </form>
    </div>
    <div ng-show="state==2">

        <figure class="indent-bot-15" ng-show="thurl">
            <img-cropped
                settings='{"src": "{{ url }}", "minW": <?= CJavaScript::encode($minW) ?>, "minH": <?= CJavaScript::encode($minH) ?>}'
                selected='selected(cords)'>
        </figure>

        <div class="footer indent-bot-15" ng-show="!send">
            <p ng-show="setCrop">Если необходима обрезка, выделите желаемую область картинки мышью и нажмите
                обрезать.</p>
            <input type="button" class="button" ng-disabled="setCrop" ng-click="doSubmit();" value="Обрезать"/>
            <input type="button alert" class="button alert" ng-click="deleteImage();" value="Удалить фото"/>

        </div>
    </div>
</div>


