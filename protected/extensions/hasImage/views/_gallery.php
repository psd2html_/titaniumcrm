<script>

</script>
<legend> <?= $label ?></legend>
<?php
$random = rand(0, 5000);
$baseUrl = Yii::app()->baseUrl;


Yii::app()->clientScript->registerScript('gallery', <<< EOT
   function deleteImg(e)
       {
       
       $.ajax({
            type: 'POST',
            url: '$baseUrl/$deleteAction',
            data: {pid:$(this).attr('pid')},
            
        })
        $(this).parent().remove();
        e.preventDefault();
       }
       
   function saveImg(e)
       {
       var myPid=$(this).attr('pid');
      console.log($('.c_label[pid='+myPid+']').val());
      
       $.ajax({
            type: 'POST',
            url: '$baseUrl/$updateAction',
            data: {pid:myPid,
                   t:$('.t_label[pid='+myPid+']').val(),
                   c:$('.c_label[pid='+myPid+']').val(),
                   cs:$('.c_show[pid='+myPid+']').is(':checked')
            },
            success: function(ee) {
                $('.ajaxResult[pid='+myPid+']').html(ee);
                
            }
        })
        $('.ajaxResult[pid='+myPid+']').html('...');
        e.preventDefault();
       }
       
   function joinScripts()
       {
            $('.deleteButton').unbind().click(deleteImg);
            $('.saveButton').unbind().click(saveImg);
       }
   joinScripts();
EOT
        , CClientScript::POS_READY);
?>

    <div class="container">
        <span id="gallery_spot"> </span>
        <?php
        foreach ($models as $i) {
            $this->render('_dsc',array('i'=>$i));
        }
        ?>
    </div>  </fieldset>
    <?php
$this->widget('ext.EAjaxUpload.EAjaxUpload', array(
    'id' => 'uploadFileIntoGallery',
    'config' => array(
        'action' => Yii::app()->createUrl($saveAction, array('s'=>$subject,'st'=>$subjectType)),
        'allowedExtensions' => array("jpg", "jpeg", "gif", "png"), //array("jpg","jpeg","gif","exe","mov" and etc...
        'sizeLimit' => 10 * 1024 * 1024, // maximum file size in bytes
        'maxSizeLimit' => 10 * 1024 * 1024, // minimum file size in bytes
        'onComplete' => "js:function(id, fileName, responseJSON){ 
                        console.log(responseJSON);
                        if (responseJSON.success) {
                        $('#gallery_spot').append('<div class=\"thumbnail_gallery\">Файл успешно загружен. Перезагрузите страницу, чтобы отредактировать его</div>'); 
                            }
                        $('.thumbnail_gallery a').unbind().click(
       function (e)
       {
       
       $.ajax({
            type: 'POST',
            url: '$baseUrl/image/ajaxDelete',
            data: {pid:$(this).attr('pid')},
            //success: success,
            //dataType: dataType
        })
        $(this).parent().remove();
        e.preventDefault();
       }
        
        );
                        }",
    )
));
?>



