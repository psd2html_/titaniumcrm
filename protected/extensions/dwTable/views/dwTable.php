<div class="row">

	<div class="span-7 ">
	
		<?php
		$hotFix="if(typeof CKEDITOR!='undefined'){
					console.log('enter');
   					for ( instance in CKEDITOR.instances ) 
   						{
   						console.log('del'+instance);
    						delete CKEDITOR.instances[instance];}		
   						}
				";
		$rch='';
		$rchL='';
		$makeLoad="$(\".rch-block\").html(' <img src=\"".Yii::app()->getBaseUrl()."/images/loading.gif\" > <br/>'); 
				   $(\".ajax_container\").html(' <img src=\"".Yii::app()->getBaseUrl()."/images/loading.gif\" > <br/> ');";
		
		if ($rightColumnUrl) {
			$rch='$.ajax({
			url: "'.Yii::app()->createUrl($controller."/".$rightColumnUrl).'/"+selectedRow,
			success: function(data) {
			
			$(".rch-block").html(data); $(".rch-block").show();
		}});';
		}
		if (isset($_GET['createNew']))
		{
			Yii::app()->clientScript->registerScript('load2', '$("body").off();
				'.$hotFix.'
				'.$makeLoad.'
				$.ajax({
				url: "'.Yii::app()->createUrl($controller."/createAjax",$argsForCreate).'",
				success: function(data) {
				$(".ajax_container").html(data); $(".rch-block").html(); $(".rch-block").hide();
				}});
					');
		}
		if (isset($_GET['idSE']))
		{
			
			if (is_numeric($_GET['idSE'])) {
				if ($rightColumnUrl) {
				$rchL='$.ajax({
				url: "'.Yii::app()->createUrl($controller."/".$rightColumnUrl).'/"+'.$_GET['idSE'].',
				success: function(data) {
				
				$(".rch-block").html(data); $(".rch-block").show();
				}});';
				}
			Yii::app()->clientScript->registerScript('load', '
			$("body").off();
			'.$makeLoad.'
			$.ajax({
				url: "'.Yii::app()->createUrl($controller."/updateAjax").'/"+'.$_GET['idSE'].',
				success: function(data) {
					$(".ajax_container").html(data);
					
				}
			});
			'.$rchL);
			}
		}
		
		Yii::app()->clientScript->registerScript('search', '
				$(".create-new").click(function(){
				$("body").off();
				'.$hotFix.'
				'.$makeLoad.'
				$.ajax({
				url: "'.Yii::app()->createUrl($controller."/createAjax",$argsForCreate).'",
				success: function(data) {
				$(".ajax_container").html(data); $(".rch-block").html(); $(".rch-block").hide();
				}});
				return false;
				});
				');
		
		$dp= $model0->search();
		$dp->sort->defaultOrder=$defaultOrder;
		$dp->pagination->pageSize=15;
	
		
		$this->widget('zii.widgets.grid.CGridView', array(

				'id' => 'person-grid',

				'selectableRows'=>1,
				'selectionChanged'=>'
				function(id)
				{
				var selectedRow=$.fn.yiiGridView.getSelection(id) ;
				if (selectedRow=="") {return;}
				$("body").off();
				'.$hotFix.'
				'.$makeLoad.'
				$.ajax({
				url: "'.Yii::app()->createUrl($controller."/updateAjax").'/"+selectedRow,
				success: function(data) {
				$(".ajax_container").html(data);
				}});
				'.$rch.'
				} ',
				'cssFile'=>Yii::app()->baseUrl.'/css/strictTable.css',


				'dataProvider' => $dp,
				'template'=>"{items}\n<div class=\"row\">{pager}</div>",
				'summaryText'=>'{start}/{end}; Всего: {count}',
				'filter' => $model0,
				'columns' => ($cols+array(
						
						'CButtonColumn'=>
						array	
						(
								'class'=>'CButtonColumn',
								'deleteButtonUrl' => 'Yii::app()->createUrl("'.$controller.'/deleteAjax",array("id"=>$data->primaryKey))',
								'template'=>'{delete}',
								'htmlOptions' => array('width' => '5%'),
						))
						
				),
		));
		
		echo (CHtml::link("<i class=\"icon-plus-sign\"></i> Создать новый элемент","#",array('class'=>"btn btn-big create-new")));
		
		?>
		<br/>
	<div class="rch-block" style="display:none"></div>
	</div>
		
	<div class="last push-1 ajax_container">
            <div class="flash-notice">	
			Для редактирования записи, выберите в таблице слева соответствующий
			заголовок. Для создания новой, нажмите на соответствующий пункт меню.
			<br /> Для удаления записи, нажмите на соответствующий пункт напротив
			записи.
            </div>
		
	</div>
		
</div>
