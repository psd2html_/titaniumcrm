<?php

/**
 * SplitEdit
 *
 * @version 0.1
 * @author SD
 * @link http://darkwater.ru
 */
class dwTable extends CWidget {

    /**
     * Html ID
     * @var string
     */
    public $columnCount = 6;
    public $model;
    
    public function init() {
        // this method is called by CController::beginWidget()
    }

    public function obtainHeaders( $columns)
    {
       
        return $headers;
        
    }


    public function run() {
        $this->render('dwTable', array(
            'defaultOrder' => $this->defaultOrder,
            'controller' => $this->controller,
            'model0' => $this->model0,
            'cols' => $this->cols,
            'argsForCreate' => $this->argsForCreate,
            'rightColumnUrl' => $this->rightColumnUrl
        ));
    }

}

?>