<?php

/**
 * EChosen class file.
 * 
 * @author Andrius Marcinkevicius <andrew.web@ifdattic.com>
 * @copyright Copyright &copy; 2011 Andrius Marcinkevicius
 * @license Licensed under MIT license. http://ifdattic.com/MIT-license.txt
 * @version 1.5.1
 */

/**
 * EChosen makes select boxes much more user-friendly.
 * 
 * @author Andrius Marcinkevicius <andrew.web@ifdattic.com>
 */
class EChosen extends CInputWidget {

    /**
     * @var string apply chosen plugin to these elements.
     */
    public $target = '.chzn-select';

    /**
     * @var boolean use jQuery plugin, otherwise use Prototype plugin.
     */
    public $useJQuery = true;

    /**
     * @var boolean include un-minified plugin then debuging.
     */
    public $debug = false;

    /**
     * @var array native Chosen plugin options.
     */
    public $options = array();
    public $chosenOptions = array();

    /**
     * @var int script registration position.
     */
    public $scriptPosition = CClientScript::POS_END;

    /**
     * @var boolean add empty element 
     */
    public $allowEmpty = false;
    public $emptyText = "Не задан";

    /**
     * @var boolean if attribute value is set, what to display
     */
    public $relatedAttribute = 'name';
    public $related = 'name';

    /**
     * Apply Chosen plugin to select boxes.
     */
    public function run() {

        list($this->name, $this->id) = $this->resolveNameId();
        $ajaxDataFunction = "function (data) {
    var results = [];";
                
        
       $ajaxDataFunction.="
    $.each(data, function (i, val) {
    
        results.push({ value: i, text: val });
    });";
       
        
        $ajaxDataFunction.="
    return results;
}";

        $v = $this->model->getAttribute($this->attribute);
        $set = array();
        
        if ($v) {
            $set = array($v => $this->model->getRelated($this->related)->getAttribute($this->relatedAttribute));
        }
        $set[''] = $this->allowEmpty?$this->emptyText:'';
        
        echo CHtml::activeDropDownList($this->model, $this->attribute, $set);
        //echo CHtml::textField($this->name.'_visible');
        // Publish extension assets
        $assets = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias(
                        'ext.EChosen') . '/assets');

        // Register extension assets
        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile($assets . '/chosen.css');

        // Get extension for JavaScript file
        $ext = '.min.js';
        if ($this->debug)
            $ext = '.js';


        // Register jQuery scripts
        $this->options['placeholder']=$this->emptyText;
        $this->options['allowEmpty']=$this->allowEmpty;
        
        $chosenOptions = CJavaScript::encode($this->chosenOptions);
        $options = CJavaScript::encode($this->options);
        $this->chosenOptions['placeholder']=$this->emptyText;
        
        $cs->registerScriptFile($assets . '/chosen.jquery' . $ext, $this->scriptPosition);
        $cs->registerScriptFile($assets . '/ajax-chosen' . $ext, $this->scriptPosition);
        $cs->registerScript('ajaxChosen', "$('#{$this->id}' ).ajaxChosen({$options},$ajaxDataFunction,{$chosenOptions});", CClientScript::POS_READY);
    }

}

?>