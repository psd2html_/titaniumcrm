<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="en"/>

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/font-awesome.min.css"/>


    <!--        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getBaseUrl(); ?>/css/zurb/grid.css" />-->
    <!--[if lt IE 8]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0"
                 height="42" width="820"
                 alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/>
        </a>
    </div>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="js/html5.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ie.css">
    <![endif]-->

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<?php
$messagesNotification = "";
$allowedMenu = Group::getTopMenu();

if ($a = PersonalMessageService::getAmountOfUnreadMessages())
    $messagesNotification = '<span class="round label">' . $a . '</span>';

Yii::app()->clientScript->registerScriptFile("/js/revealActivate.js", CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/angular/angular.min.js', CClientScript::POS_HEAD);
$menu_items = array(
    array('label' => "Организации", 'url' => array("/company/index"), 'visible' => $allowedMenu[Group::ZONE_COMPANY]),
    array('label' => "Клиенты", 'url' => array("/customer/index"), 'visible' => $allowedMenu[Group::ZONE_CLIENT]),
    array('label' => "Измерения", 'url' => array("/sample/index"), 'visible' => $allowedMenu[Group::ZONE_PROJECT]),
    array('label' => "Производители", 'url' => array("/company/indexManufacturers"), 'visible' => $allowedMenu[Group::ZONE_MANUFACTURER]),
    //array('label' => "Клиенты", 'url' => array("customer/index")),
);
$divider = 'divider';
$menu_items_pro = array(
    array('label' => "Оборудование", 'url' => array("/coreInstrument"), 'flyout' => array(
        array('label' => "Базовые приборы", 'url' => array("/coreInstrument/index"), 'visible' => $allowedMenu[Group::ZONE_COREINSTRUMENT]),
        array('label' => "Позиции", 'url' => array("/feature/index"), 'visible' => $allowedMenu[Group::ZONE_FEATURE]),
    )),
    array('label' => "Настройки", 'url' => "#",
        'flyout' => array(
            array('label' => "Контрагенты", 'url' => array("/contragent/index"), 'visible' => $allowedMenu[Group::ZONE_CONTRAGENT]),
            array('label' => "Страховые компании", 'url' => array("/insuranceCompany/index"), 'visible' => $allowedMenu[Group::ZONE_INSURANCE_COMPANY]), // 'visible' => Permission::getPermissionsUponAction(Permission::PERMISSION_INSURANCE)),
            array('label' => "Типы организаций", 'url' => array("/companyClass/index"), 'visible' => $allowedMenu[Group::ZONE_COMPANY_CLASS]), // 'visible' => Permission::getPermissionsUponAction(Permission::PERMISSION_COMPANYCLASS)),
            array('label' => "Производители оборудования", 'url' => array("/manufacturer/index"), 'visible' => $allowedMenu[Group::ZONE_MANUFACTURER]), // 'visible' => Permission::getPermissionsUponAction(Permission::PERMISSION_MANUFACTURER)),
            array('label' => "Класс оборудования", 'url' => array("/coreInstrumentClass/index"), 'visible' => $allowedMenu[Group::ZONE_COREINTRUMENT_CLASS]), // 'visible' => Permission::getPermissionsUponAction(Permission::PERMISSION_COREINSTRUMENTCLASS)),
            array('label' => "Пользователи CRM", 'url' => array("/user/admin"), 'visible' => $allowedMenu[Group::ZONE_USER_MANAGEMENT]), // 'visible' => Permission::getPermissionsUponAction(Permission::PERMISSION_USER)),
            array('label' => "Права доступа", 'url' => array("/permission/index"), 'visible' => $allowedMenu[Group::ZONE_USER_MANAGEMENT]), // 'visible' => Permission::getPermissionsUponAction(Permission::PERMISSION_USER)),
            array('label' => "Группы", 'url' => array("/group/index"), 'visible' => $allowedMenu[Group::ZONE_USER_MANAGEMENT]), // 'visible' => Permission::getPermissionsUponAction(Permission::PERMISSION_USER)),
            array('label' => "Таможни", 'url' => array("/custom/index"), 'visible' => $allowedMenu[Group::ZONE_CUSTOM]), // 'visible' => Permission::getPermissionsUponAction(Permission::PERMISSION_USER)),
            array('label' => "Измерительные центры", 'url' => array("/centre/index"), 'visible' => $allowedMenu[Group::ZONE_MEASUREMENT_CENTRE]), //'visible' => Permission::getPermissionsUponAction(Permission::PERMISSION_CENTRE)),

            array('label' => "/Получить курсы валют/", 'url' => array("/ajax/CurrencyCourseAjax")),
            //      array('label' => "/Виджет городов (демо)/", 'url' => array("/ajax/City")),

        )
    ),

    //  array('label' => "Release notes", 'url' => array("/site/rn")),
    // array('label' => "Проект-1", 'url' => array("project/view/1")),
);

$menu_first = array(
    array('label' => "Моя сводка", 'url' => array("/personal/index")),
    array('label' => "Проекты", 'url' => "/project/index", 'flyout' => array(
        array('label' => "Активные проекты по моим направлениям", 'url' => array("/project/index")),
        array('label' => "Завершенные проекты по моим направлениям", 'url' => array("/project/closedProjects")),
        array('label' => "Все доступные проекты", 'url' => array("/project/allProjects")),
    )),
);

$top_menu = array();
$top_menu[] = $divider;
$top_menu = array_merge($top_menu, $menu_first);
$top_menu[] = $divider;
$top_menu = array_merge($top_menu, $menu_items);
$top_menu[] = $divider;
$top_menu = array_merge($top_menu, $menu_items_pro);
?>

<body ng-app="<?= $this->angularJsDispatcher->appName ?>">
<script>
    <?= $this->angularJsDispatcher->getNgAppInitCode(array("ngSanitize")) ?>
</script>
<?php

Yii::app()->clientScript->registerCoreScript('jquery');
?>

<!-- ZURBar -->
<nav class="top-bar sticky">
    <ul>
        <!-- Title Area -->
        <li class="name">
        </li>
        <li class="toggle-topbar"><a href="#"></a></li>
    </ul>

    <? if (!Yii::app()->user->isGuest) { ?>
        <section>
            <!-- Left Nav Section -->
            <?php
            $this->widget('ext.foundation.widgets.FounTopBar', array('class' => 'left',
                'items' => $top_menu));
            ?>


            <!-- Right Nav Section -->
            <ul class="right vertical">
                <li class="divider show-for-medium-and-up"></li>
                <li>
                    <a href="<?= Yii::app()->createUrl('personal/account') ?>">Личный
                        кабинет <?= $messagesNotification ?></a>
                </li>
                <li>
                    <a href="<?= Yii::app()->createUrl('user/logout') ?>">Выход (<?= Yii::app()->user->username ?>:<?= Yii::app()->user->idGroup ?>)</a>
                </li>
            </ul>
        </section>
    <? } ?>
</nav>
<div class="placeholder"></div>
<? if (!Yii::app()->user->isGuest) { ?>
    <!-- /ZURBar -->
<div class="row almost-full-width">
    <div class="twelve columns full-width-print">
        <section class="row">
            <div class="twelve columns">
                <!--                <div class="alert-box alert"><em>Внимание</em><br/>CRM работает в режиме только для чтения в связи с переездом на новый сервер </div>-->
                <?php $this->widget('ext.foundation.widgets.FounBreadcrumbs', array('links' => $this->breadcrumbs)) ?>
            </div>
        </section>
        <?php } ?>
        <section class="row">
            <?php echo $content; ?>

        </section>

        <? if (!Yii::app()->user->isGuest) { ?>
    </div>
</div>
    <div id="dwModal" class="dwModal">
    </div>
    <div id="dwModalPopup" class="dwModalPopup">
        <div id="dwModalPopupHeader">
            <button class="button secondary small">X</button>
        </div>
        <div id="dwModalPopupInner">
        </div>
    </div>
<?php } ?>
</body>
</html>